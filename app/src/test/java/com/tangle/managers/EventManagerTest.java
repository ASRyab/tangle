package com.tangle.managers;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.observers.TestObserver;
import io.reactivex.schedulers.TestScheduler;

public class EventManagerTest {

    private EventManager manager;

    @Before
    public void setUp() throws Exception {
//         manager = new EventManager();
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getEventById() {
//        TestObserver observer = new TestObserver();
//        manager.getAll().subscribe(observer);
//        observer.assertComplete();
    }

    @Test
    public void timeTest() {
        String serverTime = "2018-09-17 10:14:00";
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date parse = null;
        try {
            parse = format.parse(serverTime);

        } catch (ParseException e) {
            e.printStackTrace();
        }
        format = new SimpleDateFormat("h:mm a zX", Locale.US);
//        format.setTimeZone(TimeZone.getTimeZone("Europe/London"));
        String format1 = format.format(parse);
        System.out.println(format1);
//        Log.d("ppp", "tokenTest1: "+format1);
    }
    @Test
    public void tokenTest() {
        TestScheduler testScheduler = new TestScheduler();
        ConnectableObservable<String> tokenObservable = Observable
                .fromCallable(this::getToken)
                .map(this::updateToken)
                .subscribeOn(testScheduler)
                .replay(1);

        TestObserver<String> observer1 = new TestObserver<>();
        TestObserver<String> observer2 = new TestObserver<>();

        tokenObservable.subscribeWith(observer1);
        tokenObservable.connect();

        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS);

        tokenObservable.subscribeWith(observer2);

        testScheduler.advanceTimeBy(1000, TimeUnit.MILLISECONDS);


        observer1.assertValues("new");
        observer2.assertValues("new");
    }


    String token = "old";

    String getToken() {
        return token;
    }

    String updateToken(String token) throws Exception {
        System.out.println("update token " + Thread.currentThread());
        this.token = "new";
        return this.token;
    }
}