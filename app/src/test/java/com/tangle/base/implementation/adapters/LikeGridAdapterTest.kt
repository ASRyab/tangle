package com.tangle.base.implementation.adapters

import android.content.Context
import com.bumptech.glide.request.RequestOptions
import com.nhaarman.mockitokotlin2.mock
import com.tangle.base.implementation.adapters.LikeConnectionsAdapter.Companion.TYPE_LIKED_ME
import com.tangle.base.implementation.adapters.LikeConnectionsAdapter.Companion.TYPE_LIKED_ME_HEADER
import com.tangle.base.implementation.adapters.LikeConnectionsAdapter.Companion.TYPE_LIKED_OTHER
import com.tangle.base.implementation.adapters.LikeConnectionsAdapter.Companion.TYPE_LIKED_OTHER_HEADER
import com.tangle.base.implementation.adapters.LikeConnectionsAdapter.Companion.TYPE_NONE
import com.tangle.model.profile_data.ProfileData
import org.junit.Test


class LikeGridAdapterTest {

    private val profDataLiked: ProfileData = ProfileData.mock()

    private var emptyProfDataList: ArrayList<ProfileData> = ArrayList()
    private var threeItemsDataList: ArrayList<ProfileData> = ArrayList()
    private var nineItemsDataList: ArrayList<ProfileData> = ArrayList()
    private var sixItemsDataList: ArrayList<ProfileData> = ArrayList()
    private val ctx: Context = mock()
    private val requestOptions: RequestOptions = mock()
    private val adapter = LikeConnectionsAdapter(ctx, requestOptions, null)

    init {
        for (i in 0 until 3) {
            threeItemsDataList.add(profDataLiked)
        }

        for (i in 0 until 9) {
            nineItemsDataList.add(profDataLiked)
        }

        for (i in 0 until 6) {
            sixItemsDataList.add(profDataLiked)
        }
    }

    @Test
    fun testEmpty() {
        adapter.setLikedItems(emptyProfDataList, emptyProfDataList)
        assert(adapter.itemCount == 0)
    }

    @Test
    fun testFirstEmptySecondFilled() {
        adapter.setLikedItems(emptyProfDataList, nineItemsDataList)
        assert(adapter.getItemViewType(0) == TYPE_LIKED_OTHER_HEADER)
        assert(adapter.getItemViewType(1) == TYPE_LIKED_OTHER)
        assert(adapter.getItemViewType(6) == TYPE_LIKED_OTHER)
        assert(adapter.getItemViewType(7) == TYPE_NONE)
    }

    @Test
    fun testFirstFilledSecondEmpty() {
        adapter.setLikedItems(nineItemsDataList, emptyProfDataList)
        assert(adapter.getItemViewType(0) == TYPE_LIKED_ME_HEADER)
        assert(adapter.getItemViewType(1) == TYPE_LIKED_ME)
        assert(adapter.getItemViewType(6) == TYPE_LIKED_ME)
        assert(adapter.getItemViewType(7) == TYPE_NONE)
    }

    @Test
    fun testAllFilledSix() {
        adapter.setLikedItems(sixItemsDataList, sixItemsDataList)
        assert(adapter.getItemViewType(0) == TYPE_LIKED_ME_HEADER)
        assert(adapter.getItemViewType(1) == TYPE_LIKED_ME)
        assert(adapter.getItemViewType(6) == TYPE_LIKED_ME)
        assert(adapter.getItemViewType(7) == TYPE_LIKED_OTHER_HEADER)
        assert(adapter.getItemViewType(8) == TYPE_LIKED_OTHER)
        assert(adapter.getItemViewType(13) == TYPE_LIKED_OTHER)
        assert(adapter.getItemViewType(14) == TYPE_NONE)
    }

    @Test
    fun testAllFilledSmallSix() {
        adapter.setLikedItems(threeItemsDataList, threeItemsDataList)
        assert(adapter.getItemViewType(0) == TYPE_LIKED_ME_HEADER)
        assert(adapter.getItemViewType(1) == TYPE_LIKED_ME)
        assert(adapter.getItemViewType(3) == TYPE_LIKED_ME)
        assert(adapter.getItemViewType(4) == TYPE_LIKED_OTHER_HEADER)
        assert(adapter.getItemViewType(6) == TYPE_LIKED_OTHER)
        assert(adapter.getItemViewType(7) == TYPE_LIKED_OTHER)
        assert(adapter.getItemViewType(8) == TYPE_NONE)
    }

}