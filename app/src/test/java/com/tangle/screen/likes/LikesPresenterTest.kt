package com.tangle.screen.likes

import android.os.Bundle
import com.nhaarman.mockitokotlin2.verify
import com.tangle.managers.PhotoManager
import com.tangle.model.profile_data.ProfileData
import com.tangle.screen.activities.likes.LikesContract
import com.tangle.screen.activities.likes.LikesPresenter
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import java.util.*
import kotlin.collections.ArrayList


class LikesPresenterTest {
    private lateinit var presenter: LikesPresenter

    @Mock
    private lateinit var view: LikesContract.View
    @Mock
    private lateinit var model: LikesContract.Model

    @Before
    @Throws(Exception::class)
    fun init() {
        MockitoAnnotations.initMocks(this)
        presenter = LikesPresenter(model)

        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }

    }

    @Test
    fun updateDataset_add_matched_user() {
        val profDataFirstList = ArrayList<ProfileData>()
        for (i in 1..10) {
            val newUser = getNewUser()
            newUser.id = i.toString()
            if (i == 5) {
                newUser.isMatchedUser = true
            }
            profDataFirstList.add(newUser)
        }
        presenter.updateDataset(profDataFirstList)
        assert(presenter.users.size == 9)
    }

    @Test
    fun attachToView_oneLikeMeUserShownTest() {
        val testList = ArrayList<ProfileData>()
        val newUser = getNewUser()
        newUser.isLikedMeUser = true
        newUser.isYouLikedUser = false
        newUser.isMatchedUser = false
        testList.add(newUser)

        `when`(presenter.model.getProfilesWithUpdates())
                .thenReturn(Observable.just(testList))
        `when`(presenter.model.getCheckPhotoObservable(view))
                .thenReturn(Single.just(PhotoManager.PhotoState.HAS_PHOTO))
        presenter.attachToView(view, Bundle())
        verify(view).setLikedUserList(ArrayList(Collections.singletonList(newUser)), ArrayList(Collections.emptyList()))
        verify(view).hideEmptyListError()
    }

    @Test
    fun attachToView_oneLikeOtherUserShownTest() {
        val testList = ArrayList<ProfileData>()
        val newUser = getNewUser()
        newUser.isLikedMeUser = false
        newUser.isYouLikedUser = true
        newUser.isMatchedUser = false
        testList.add(newUser)

        `when`(presenter.model.getProfilesWithUpdates())
                .thenReturn(Observable.just(testList))
        `when`(presenter.model.getCheckPhotoObservable(view))
                .thenReturn(Single.just(PhotoManager.PhotoState.HAS_PHOTO))
        presenter.attachToView(view, Bundle())
        verify(view).setLikedUserList(ArrayList(Collections.emptyList()), ArrayList(Collections.singletonList(newUser)))
        verify(view).hideEmptyListError()
    }

    @Test
    fun attachToView_emptyListShownTest() {
        val testList = ArrayList<ProfileData>()
        `when`(presenter.model.getProfilesWithUpdates())
                .thenReturn(Observable.just(testList))
        `when`(presenter.model.getCheckPhotoObservable(view))
                .thenReturn(Single.just(PhotoManager.PhotoState.HAS_PHOTO))
        presenter.attachToView(view, Bundle())
        verify(view).showEmptyListError()

        presenter.getUpdates().test()
                .assertNoErrors()
                .assertComplete()
    }


    @Test
    fun updateDataset_add_last_item() {
        val profDataFirstList = ArrayList<ProfileData>()
        for (i in 1..5) {
            val newUser = getNewUser()
            newUser.id = i.toString()
            profDataFirstList.add(newUser)
        }
        presenter.updateDataset(profDataFirstList)
        val updUser = getNewUser()
        presenter.updateDataset(Collections.singletonList(updUser))
        assert(presenter.users.size == 6)
        assert(presenter.users.contains(updUser))
        assert(presenter.users[presenter.users.size - 1] == updUser)
    }

    @Test
    fun updateDataset_add_same_item() {
        val profDataFirstList = ArrayList<ProfileData>()
        for (i in 1..5) {
            val newUser = getNewUser()
            newUser.id = i.toString()
            profDataFirstList.add(newUser)
        }
        presenter.updateDataset(profDataFirstList)
        val updUser = getNewUser()
        presenter.updateDataset(Collections.singletonList(updUser))
        presenter.updateDataset(Collections.singletonList(updUser))
        assert(presenter.users.size == 6)
        assert(presenter.users.contains(updUser))
        assert(presenter.users[presenter.users.size - 1] == updUser)
    }

    @Test
    fun updateDataset_remove_if_match_same_user() {
        val updUser = getNewUser()
        presenter.updateDataset(Collections.singletonList(updUser))
        updUser.isMatchedUser = true
        presenter.updateDataset(Collections.singletonList(updUser))
        assert(presenter.users.isEmpty())
        assert(!presenter.users.contains(updUser))
    }

    @Test
    fun getUpdates_one_like_me_user() {
        val newUser = getNewUser()
        newUser.isMatchedUser = false
        newUser.isLikedMeUser = true
        newUser.isYouLikedUser = false

        `when`(presenter.model.getProfilesWithUpdates())
                .thenReturn(Observable.just(ArrayList(Collections.singletonList(newUser))))
        presenter.getUpdates()
                .test()
                .assertValue {
                    it.first.size == 1 && it.first.contains(newUser)
                            && it.second.isEmpty()
                }
    }

    @Test
    fun getUpdates_one_like_you_user() {
        val newUser = getNewUser()
        newUser.isMatchedUser = false
        newUser.isLikedMeUser = false
        newUser.isYouLikedUser = true

        `when`(presenter.model.getProfilesWithUpdates())
                .thenReturn(Observable.just(ArrayList(Collections.singletonList(newUser))))
        presenter.getUpdates()
                .test()
                .assertValue {
                    it.second.size == 1 && it.second.contains(newUser)
                            && it.first.isEmpty()
                }

    }

    @Test
    fun getUpdates_one_matched_user() {
        val newUser = getNewUser()
        newUser.isMatchedUser = true
        newUser.isLikedMeUser = true
        newUser.isYouLikedUser = true

        `when`(presenter.model.getProfilesWithUpdates())
                .thenReturn(Observable.just(ArrayList(Collections.singletonList(newUser))))
        presenter.getUpdates()
                .test()
                .assertValue {
                    it.first.isEmpty() && it.second.isEmpty()
                }
    }

    @Test
    fun getUpdates_one_like_me_one_liked_you() {
        val likeMeUser = getNewUser()
        likeMeUser.isLikedMeUser = true
        likeMeUser.isYouLikedUser = false

        val likedYouUser = getNewUser()
        likedYouUser.isLikedMeUser = false
        likedYouUser.isYouLikedUser = true

        val testList = ArrayList<ProfileData>()
        testList.add(likeMeUser)
        testList.add(likedYouUser)

        `when`(presenter.model.getProfilesWithUpdates())
                .thenReturn(Observable.just(testList))
        presenter.getUpdates()
                .test()
                .assertValue {
                    it.first.size == 1 && it.first[0].isLikedMeUser
                }.assertValue {
                    it.second.size == 1 && it.second[0].isYouLikedUser
                }
    }

    @Test
    fun getUpdates_sort_liked_me_by_date() {
        val testList = createDateSortDataSet()
        `when`(presenter.model.getProfilesWithUpdates())
                .thenReturn(Observable.just(testList))

        presenter.getUpdates()
                .test()
                .assertValue {
                    it.first.size == 5 && it.second.size == 5
                }
                .assertValue {
                    var isAllDatesAreSorted = true
                    it.first.forEachIndexed { index, element ->
                        if (index < it.first.size - 1) {
                            val date1 = it.first[index].getLastUserActivityDate()
                            val date2 = it.first[index + 1].getLastUserActivityDate()

                            isAllDatesAreSorted = date1.compareTo(date2) == 1
                            if (!isAllDatesAreSorted) return@assertValue false
                        }
                    }
                    return@assertValue isAllDatesAreSorted
                }
    }

    @Test
    fun getUpdates_sort_liked_other_by_date() {
        val testList = createDateSortDataSet()

        `when`(presenter.model.getProfilesWithUpdates())
                .thenReturn(Observable.just(testList))
        presenter.getUpdates()
                .test()
                .assertValue {
                    var isAllDatesAreSorted = true
                    it.second.forEachIndexed { index, element ->
                        if (index < it.second.size - 1) {
                            val date1 = it.second[index].getLastUserActivityDate()
                            val date2 = it.second[index + 1].getLastUserActivityDate()
                            isAllDatesAreSorted = date1.compareTo(date2) == 1
                            if (!isAllDatesAreSorted) return@assertValue false
                        }
                    }
                    return@assertValue isAllDatesAreSorted
                }
    }

    @Test
    fun getUpdates_sort_liked_me_by_date_and_new() {
        val testList = createDateSortDataSetWithNew()
        `when`(presenter.model.getProfilesWithUpdates())
                .thenReturn(Observable.just(testList))
        presenter.getUpdates()
                .test()
                .assertValue {
                    it.first[0].isNew == true
                }
                .assertValue {
                    it.first[1].lastUserActivityDate.compareTo(it.first[0].lastUserActivityDate) == 1
                }
    }

    private fun createDateSortDataSet(): ArrayList<ProfileData> {
        val testList = ArrayList<ProfileData>()
        val dateTemplate = "2019-02-06T10:01:"
        for (i in 1..10) {
            val date = dateTemplate + String.format("%02d", i)
            val newUser = getNewUser()
            newUser.id = i.toString()
            newUser.lastUserActivity = date
            if (i % 2 == 0) {
                newUser.isYouLikedUser = true
            } else newUser.isLikedMeUser = true
            testList.add(newUser)
        }
        return testList
    }

    private fun createDateSortDataSetWithNew(): ArrayList<ProfileData> {
        val testList = ArrayList<ProfileData>()
        val dateTemplate = "2019-02-06T10:01:"
        for (i in 1..3) {
            val date = dateTemplate + String.format("%02d", i)
            val newUser = getNewUser()
            newUser.id = i.toString()
            newUser.lastUserActivity = date
            newUser.isLikedMeUser = true
            if (i % 2 == 0) newUser.isNew = true
            testList.add(newUser)
        }
        return testList
    }

    private fun getNewUser() = ProfileData.mock()

}