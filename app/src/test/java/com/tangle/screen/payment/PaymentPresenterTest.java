package com.tangle.screen.payment;

import android.os.Bundle;

import com.tangle.model.payment.PaymentActionResult;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.TestScheduler;

import static com.tangle.screen.payment.PaymentPresenter.PERIOD_EXPIRED_TICKET;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PaymentPresenterTest {
    @Mock
    private PaymentContract.View paymentView;

    @Mock
    private PaymentContract.Model paymentModel;

    private PaymentPresenter paymentPresenter;
    private TestScheduler testScheduler;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        paymentPresenter = new PaymentPresenter(paymentModel);
        paymentPresenter.attachToView(paymentView, new Bundle());
        testScheduler = new TestScheduler();
        // set calls to Schedulers.computation() to use our test scheduler
        RxJavaPlugins.setComputationSchedulerHandler(ignore -> testScheduler);
        when(paymentModel.getTimeOfOpen()).thenReturn(0L);
    }

    @After
    public void tearDown() throws Exception {
        // reset it
        RxJavaPlugins.setComputationSchedulerHandler(null);
    }


    @Test
    public void checkCloseInvoice_ok() {
        when(paymentModel.undoInvoice(null)).thenReturn(Observable.just(new PaymentActionResult()));
        paymentPresenter.onBackPressed();
        verify(paymentModel).undoInvoice(null);
        verify(paymentView,never()).expiredTicket();
        verify(paymentView).close();
        verify(paymentModel).setTimeOfOpen(0);
    }

    @Test
    public void checkTimer_ok() {
        TestObserver observer = new TestObserver<>();
        Single observable = paymentPresenter.startCount();
        observable.subscribe(observer);
        testScheduler.advanceTimeBy(paymentPresenter.getPeriod(), TimeUnit.MINUTES);
        observer.assertNoErrors();
        observer.assertValueCount(1);
        verify(paymentView).expiredTicket();
        verify(paymentView).close();
        verify(paymentModel).setTimeOfOpen(0);
    }

    @Test
    public void checkTimer_more() {
        TestObserver observer = new TestObserver<>();
        Single observable = paymentPresenter.startCount();
        observable.subscribe(observer);
        testScheduler.advanceTimeBy(paymentPresenter.getPeriod() + 1, TimeUnit.MINUTES);
        observer.assertNoErrors();
        observer.assertValueCount(1);
        verify(paymentView).expiredTicket();
        verify(paymentView).close();
        verify(paymentModel).setTimeOfOpen(0);
    }

    @Test
    public void checkTimer_less() {
        TestObserver observer = new TestObserver<>();
        Single observable = paymentPresenter.startCount();
        observable.subscribe(observer);
        testScheduler.advanceTimeBy(paymentPresenter.getPeriod() - 1, TimeUnit.MINUTES);
        observer.assertNoErrors();
        observer.assertValueCount(0);
        verify(paymentView, never()).expiredTicket();
        verify(paymentView, never()).close();
        verify(paymentModel, never()).setTimeOfOpen(0);
    }

    @Test
    public void checkTimer_after() {
        long time = 2;
        when(paymentModel.getTimeOfOpen()).thenReturn(time);
        TestObserver observer = new TestObserver<>();
        Single observable = paymentPresenter.startCount();
        observable.subscribe(observer);
        testScheduler.advanceTimeBy(PERIOD_EXPIRED_TICKET - time, TimeUnit.MINUTES);
        observer.assertNoErrors();
        observer.assertValueCount(1);
        verify(paymentView).expiredTicket();
        verify(paymentView).close();
        verify(paymentModel).setTimeOfOpen(0);
    }

    @Test
    public void checkTimer_after_20() {
        long time = 20;
        when(paymentModel.getTimeOfOpen()).thenReturn(time);
        TestObserver observer = new TestObserver<>();
        Single observable = paymentPresenter.startCount();
        observable.subscribe(observer);
        testScheduler.triggerActions();
        observer.assertNoErrors();
        observer.assertValueCount(1);
        verify(paymentView).expiredTicket();
        verify(paymentView).close();
        verify(paymentModel).setTimeOfOpen(0);
    }

    @Test
    public void checkTimer_close() {
        when(paymentModel.undoInvoice(null)).thenReturn(Observable.just(new PaymentActionResult()));
        paymentPresenter.onBackPressed();
        TestObserver observer = new TestObserver<>();
        Single observable = paymentPresenter.startCount();
        observable.subscribe(observer);
        testScheduler.advanceTimeBy(PERIOD_EXPIRED_TICKET / 2, TimeUnit.MINUTES);
        observer.assertNoErrors();
        observer.assertValueCount(0);
        verify(paymentView, never()).expiredTicket();
        verify(paymentView).close();
        verify(paymentModel).setTimeOfOpen(0);
    }
}