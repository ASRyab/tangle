package com.tangle.screen.payment

import android.os.Bundle
import com.tangle.model.event.EventInfo
import com.tangle.model.payment.InvoiceInfo
import com.tangle.model.payment.PaymentData
import com.tangle.model.payment.TicketResponse
import com.tangle.model.profile_data.ProfileData
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.*
import org.mockito.MockitoAnnotations
import java.util.*

class PaymentPresenterBaseTest {

    companion object {
        const val CURRENT_USER_ID:String = "123"
    }
    @Mock
    private lateinit var paymentView: PaymentContract.View

    @Mock
    private lateinit var paymentModel: PaymentContract.Model

    private val paymentData: PaymentData = Mockito.mock(PaymentData::class.java)

    private val inviteData: InvoiceInfo = Mockito.mock(InvoiceInfo::class.java)

    private lateinit var paymentPresenter: PaymentPresenter

    @Before
    @Throws(Exception::class)
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        paymentPresenter = PaymentPresenter(paymentModel)
        paymentPresenter.attachToView(paymentView, Bundle())
        paymentPresenter.setData(paymentData)
        `when`(paymentModel.currentUserId).thenReturn(CURRENT_USER_ID)
    }


    @Test
    fun setEvent_non_ticket() {
        val eventInfo = EventInfo()
        eventInfo.ticketStatus = 0
        eventInfo.maxBookTickets = 10
        val ticketResponse = Mockito.mock(TicketResponse::class.java)
        `when`(ticketResponse.userId).thenReturn(CURRENT_USER_ID)
        `when`(inviteData.tickets).thenReturn(Collections.singletonList(ticketResponse))
        paymentPresenter.showEvent(eventInfo, Collections.singletonList(inviteData))
        verify(paymentView).setOwnData(paymentData, inviteData)
    }

    @Test
    fun setEvent_has_ticket() {
        val eventInfo = EventInfo()
        eventInfo.ticketStatus = 1
        eventInfo.maxBookTickets = 10
        val ticketResponse = Mockito.mock(TicketResponse::class.java)
        `when`(ticketResponse.userId).thenReturn(CURRENT_USER_ID)
        `when`(inviteData.tickets).thenReturn(Collections.singletonList(ticketResponse))
        paymentPresenter.showEvent(eventInfo, Collections.singletonList(inviteData))
        verify(paymentView).setOwnData(paymentData, inviteData)
    }

    @Test
    fun setEvent_has_ticket_max() {
        val eventInfo = EventInfo()
        eventInfo.ticketStatus = 1
        eventInfo.maxBookTickets = 0
        paymentPresenter.showEvent(eventInfo, Collections.singletonList(inviteData))
        verify(paymentView, never()).setOwnData(paymentData, inviteData)
    }

    @Test
    fun setEvent_non_ticket_invite() {
        val eventInfo = EventInfo()
        eventInfo.ticketStatus = 0
        eventInfo.maxBookTickets = 10
        `when`(paymentData.inviteUserId).thenReturn("")
        val ticketResponse = Mockito.mock(TicketResponse::class.java)
        `when`(ticketResponse.userId).thenReturn(CURRENT_USER_ID)
        `when`(inviteData.tickets).thenReturn(Collections.singletonList(ticketResponse))

        paymentPresenter.showEvent(eventInfo, Collections.singletonList(inviteData))
        verify(paymentView).setOwnData(paymentData, inviteData)
    }

    @Test
    fun setEvent_non_ticket_invite_max() {
        val eventInfo = EventInfo()
        eventInfo.ticketStatus = 0
        eventInfo.maxBookTickets = 0
        `when`(paymentData.inviteUserId).thenReturn("")
        paymentPresenter.showEvent(eventInfo, Collections.singletonList(inviteData))
        verify(paymentView, never()).setOwnData(paymentData, inviteData)
    }

    @Test
    fun setEvent_has_ticket_invite() {
        val eventInfo = EventInfo()
        eventInfo.ticketStatus = 1
        eventInfo.maxBookTickets = 10
        `when`(paymentData.inviteUserId).thenReturn("")
        paymentPresenter.showEvent(eventInfo, Collections.singletonList(inviteData))
        verify(paymentView, never()).setOwnData(paymentData, inviteData)
    }

    @Test
    fun setInvitedUser_non_invite() {
        paymentPresenter.showInvitedUser(ProfileData.EMPTY_STATE, Collections.singletonList(inviteData))
        verify(paymentView, never()).setOtherUser(ProfileData(),paymentData, inviteData)
    }

    @Test
    fun setInvitedUser_invite() {
        val profileData = ProfileData()
        paymentPresenter.showInvitedUser(profileData, Collections.singletonList(inviteData))
        verify(paymentView).setOtherUser(profileData,paymentData, inviteData)
    }
}