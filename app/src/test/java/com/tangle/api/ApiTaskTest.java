package com.tangle.api;

import org.junit.Test;

import io.reactivex.Observable;
import io.reactivex.observers.TestObserver;
import io.reactivex.subjects.AsyncSubject;

public class ApiTaskTest {
    @Test
    public void autoRefresh() {
        AsyncSubject<Object> subject = null;


        Observable.concat(subject = getSubject(subject), Observable.just(1)).doOnNext(o -> System.out.println(o)).subscribe();
        Observable.concat(getSubject(subject), Observable.just(2)).doOnNext(o -> System.out.println(o)).subscribe();
        Observable.concat(getSubject(subject), Observable.just(3)).doOnNext(o -> System.out.println(o)).subscribe();

//        getSubject(subject).onNext(3333);
        getSubject(subject).onComplete();

        TestObserver<Object> test = Observable.concat(subject = getSubject(subject), Observable.just(4)).doOnNext(o -> System.out.println(o)).test();
        Observable.concat(getSubject(subject), Observable.just(5)).doOnError(o -> System.out.println(o)).doOnNext(o -> System.out.println(o)).subscribe(o -> {},throwable ->  System.out.println(throwable));
        getSubject(subject).onNext(4444);
        getSubject(subject).onError(new Throwable("ppp"));
        test.assertErrorMessage("ppp");
//        getSubject(subject).onComplete();
//        TestObserver<Object> to1 = subject.test();
//
//        to1.assertEmpty();
//
//        subject.onNext(1);
//
//        // AsyncSubject only emits when onComplete was called.
//        to1.assertEmpty();
//
//        subject.onNext(2);
//        subject.onComplete();
//
//        // onComplete triggers the emission of the last cached item and the onComplete event.
//        to1.assertResult(2);
//
//        TestObserver<Object> to2 = subject.test();
//
//        // late Observers receive the last cached item too
//        to2.assertResult(2);
//
//        subject.onNext(3);
//        to2.assertNever(3);
//        TestObserver<Object> to3 = subject.test();
//        subject.onComplete();
//        to3.assertResult(2);
//        to3.assertNotComplete();
    }

    private AsyncSubject getSubject(AsyncSubject<Object> subject) {
        if (subject == null || subject.hasComplete()) {
            subject = AsyncSubject.create();
        }
        return subject;
    }
}
