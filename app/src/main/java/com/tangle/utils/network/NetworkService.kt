package com.tangle.utils.network

import android.app.Service
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager.CONNECTIVITY_ACTION
import android.os.Binder
import android.os.IBinder
import com.tangle.ApplicationLoader
import com.tangle.utils.Debug

class NetworkService : Service(), ConnectivityReceiver.ConnectivityReceiverListener {
    override fun onBind(p0: Intent?): IBinder = Binder()

    private var mConnectivityReceiver: ConnectivityReceiver? = null

    override fun onCreate() {
        super.onCreate()
        Debug.logI(TAG, "Service created")
        mConnectivityReceiver = ConnectivityReceiver(this)
        registerReceiver(mConnectivityReceiver, IntentFilter(CONNECTIVITY_ACTION))
    }

    override fun onDestroy() {
        super.onDestroy()
        Debug.logI(TAG, "Service destroy")
        unregisterReceiver(mConnectivityReceiver)
        mConnectivityReceiver = null
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Debug.logI(TAG, "onStartCommand")
        return START_NOT_STICKY
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        ApplicationLoader.getApplicationInstance().mediaManager.updateNetworkState(isConnected)
        Debug.logI(TAG, if (isConnected) "Good! Connected to Internet" else "Sorry! Not connected to internet")

    }

    companion object {
        val TAG: String = NetworkService::class.java.simpleName
    }
}