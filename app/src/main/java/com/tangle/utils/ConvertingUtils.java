package com.tangle.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;

import com.tangle.R;
import com.tangle.model.chat.MailMessagePhoenix;
import com.tangle.model.profile_data.ProfileData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class ConvertingUtils {
    public static final String DATE_FORMAT_YMD_HMS = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT_YMD_HMS_T = "yyyy-MM-dd'T'HH:mm:ss";
    public static final String DATE_FORMAT_HM = "HH:mm";
    public static final String DATE_FORMAT_DMY = "dd.MM.yyyy";
    public static final String DATE_FORMAT_CHAT_SEPARATOR = "EEEE dd.MM.yyyy";
    public static final String DATE_FORMAT_TICKET_CREATED_TIME = "dd.MM.yyyy HH:mm";

    public static Date getBasicDateFromString(String dateString) {
        return getDateFromString(dateString, DATE_FORMAT_YMD_HMS);
    }

    @SuppressLint("SimpleDateFormat")
    public static Date getDateFromString(String dateString, String pattern) {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        try {
            return format.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String getStringDateByPattern(Date date, String pattern) {
        return new SimpleDateFormat(pattern).format(date);
    }

    public static String formatDateToInviteStyle(Date date, Context context) {
        long incomeTimeMillis = date.getTime();
        long currentTimeMillis = System.currentTimeMillis();
        long timeDifferenceInMillis = currentTimeMillis - incomeTimeMillis;
        if (DateUtils.isToday(incomeTimeMillis)) {
            if (timeDifferenceInMillis < DateUtils.HOUR_IN_MILLIS) {
                return String.format(context.getString(R.string.format_date_with_minutes),
                        (timeDifferenceInMillis / DateUtils.MINUTE_IN_MILLIS));
            } else {
                return String.format(context.getString(R.string.format_date_with_hours),
                        (timeDifferenceInMillis / DateUtils.HOUR_IN_MILLIS));
            }
        } else {
            if (isYesterday(date)) {
                return context.getString(R.string.yesterday);
            } else {
                if (isInLastSevenDays(date)) {
                    return getUKShortDayName(date);
                } else {
                    return getStringDateFromDateWithPattern(date, "MMM dd");
                }
            }
        }
    }

    private static boolean isYesterday(Date date) {
        return DateUtils.isToday(date.getTime() + DateUtils.DAY_IN_MILLIS);
    }

    private static boolean isInLastSevenDays(Date date) {
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DAY_OF_YEAR, -7);
        return date.getTime() >= cal.getTime().getTime();
    }

    private static String getUKShortDayName(Date date) {
        return new SimpleDateFormat("EEE").format(date);
    }

    private static String getStringDateFromDateWithPattern(Date date, String pattern) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.ENGLISH);
        return simpleDateFormat.format(date);
    }

    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static float convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static CharSequence trimTrailingWhitespace(CharSequence source) {

        if (source == null)
            return "";

        int i = source.length();

        // loop back to the first non-whitespace character
        while (--i >= 0 && Character.isWhitespace(source.charAt(i))) {
        }

        return source.subSequence(0, i + 1);
    }

    /**
     * @param value
     * @return if value = "0" - false  else true.
     */
    public static boolean stringToBool(String value) {
        return value.equals("1");
    }

    public static String getLastMessageText(Resources res, ProfileData profile, MailMessagePhoenix message) {
        String result;
        if (message.isChatImg()) {
            result = message.isMyMsg() ? res.getString(R.string.matches_item_last_my_msg_is_photo)
                    : String.format(res.getString(R.string.matches_item_last_user_msg_is_photo), profile.login);
        } else {
            result = message.message;
        }
        return result;
    }
}