package com.tangle.utils;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tangle.api.utils.DeviceUtils;
import com.tangle.screen.events.gallery.GalleryAdapter;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.Arrays;

import static com.tangle.ApplicationLoader.getContext;

public class GalleryUtils {

    public static boolean isTopItem(int position) {
        return position < 2;
    }

    public static boolean isEndItem(int position) {
        return Arrays.asList(1, 2, 5, 7, 11).contains(position % 12);
    }

    public static boolean isStartItem(int position) {
        return Arrays.asList(0, 3, 6, 8, 9).contains(position % 12);
    }

    public static boolean isHugeItem(int position) {
        return position % 12 == 0 || position % 12 == 7;
    }

    public static GalleryAdapter setupListAndGetAdapter(RecyclerView rvGallery, Context context) {
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 2, GridLayoutManager.HORIZONTAL, false);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return position % 3 == 0 ? 2 : 1;
            }
        });
        rvGallery.setLayoutManager(layoutManager);

        final int itemOffset = (int) PlatformUtils.Dimensions.dipToPixels(context, 12);
        final int galleryPadding = (int) PlatformUtils.Dimensions.dipToPixels(context, 24);
        final int extraItemWidth = (int) PlatformUtils.Dimensions.dipToPixels(context, 16);
        final int itemWidth = (DeviceUtils.getDeviceWidth(context) - galleryPadding - 2 * itemOffset - extraItemWidth) / 2;


        GalleryAdapter adapter = new GalleryAdapter(RequestOptionsFactory.getSmallRoundedCornersOptions(context), itemWidth);
        rvGallery.setAdapter(adapter);
        rvGallery.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                final int itemCount = state.getItemCount();
                final int itemPosition = parent.getChildAdapterPosition(view);
                outRect.left = itemPosition % 3 == 0 && itemPosition != 0 ? itemOffset : 0;
                outRect.right = itemPosition % 3 == 0 && itemPosition != itemCount - 1 ? itemOffset : 0;
                outRect.bottom = itemPosition % 3 == 1 ? itemOffset / 2 : 0;
                outRect.top = itemPosition % 3 == 2 ? itemOffset / 2 : 0;
            }
        });

        return adapter;
    }
}
