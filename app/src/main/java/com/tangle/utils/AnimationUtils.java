package com.tangle.utils;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.view.animation.FastOutSlowInInterpolator;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;

import java.io.Serializable;

public class AnimationUtils {
    public interface AnimationFinishedListener {
        void onAnimationFinished();
    }

    public static int getMediumDuration(Context context) {
        return context.getResources().getInteger(android.R.integer.config_mediumAnimTime);
    }

    private static void registerCircularRevealAnimation(final Context context, final View view, final RevealAnimationSetting revealSettings, final int startColor, final int endColor, final AnimationFinishedListener listener) {
        view.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @TargetApi(Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                v.removeOnLayoutChangeListener(this);

                int cx = revealSettings.getCenterX();
                int cy = revealSettings.getCenterY();
                int width = revealSettings.getWidth();
                int height = revealSettings.getHeight();

                float finalRadius = (float) Math.sqrt(width * width + height * height);
                Animator anim = ViewAnimationUtils.createCircularReveal(v, cx, cy, 0, finalRadius);
                anim.setDuration(getMediumDuration(context));
                anim.setInterpolator(new FastOutSlowInInterpolator());
                anim.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (listener != null) {
                            listener.onAnimationFinished();
                        }
                    }
                });
                anim.start();
                startBackgroundColorAnimation(view, startColor, endColor, getMediumDuration(context));
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private static void startCircularRevealExitAnimation(Context context, final View view, RevealAnimationSetting revealSettings, int startColor, int endColor, final AnimationFinishedListener listener) {
        int cx = revealSettings.getCenterX();
        int cy = revealSettings.getCenterY();
        int width = revealSettings.getWidth();
        int height = revealSettings.getHeight();

        float initRadius = (float) Math.sqrt(width * width + height * height);
        Animator anim = ViewAnimationUtils.createCircularReveal(view, cx, cy, initRadius, 0);
        anim.setDuration(getMediumDuration(context));
        anim.setInterpolator(new FastOutSlowInInterpolator());
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                view.setVisibility(View.GONE);
                if (listener != null) {
                    listener.onAnimationFinished();
                }
            }
        });
        anim.start();
        startBackgroundColorAnimation(view, startColor, endColor, getMediumDuration(context));
    }

    private static void startBackgroundColorAnimation(final View view, int startColor, int endColor, int duration) {
        ValueAnimator anim = new ValueAnimator();
        anim.setIntValues(startColor, endColor);
        anim.setEvaluator(new ArgbEvaluator());
        anim.setDuration(duration);
        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                view.setBackgroundColor((Integer) valueAnimator.getAnimatedValue());
            }
        });
        anim.start();
    }

    public static void registerCreateShareLinkCircularRevealAnimation(Context context, View view, RevealAnimationSetting revealSettings, int colorFrom, int colorTo, AnimationFinishedListener listener) {
        registerCircularRevealAnimation(context, view, revealSettings, colorFrom, colorTo, listener);
    }

    public static void startCreateShareLinkCircularRevealExitAnimation(Context context, View view, RevealAnimationSetting revealSettings, int colorFrom, int colorTo, AnimationFinishedListener listener) {
        startCircularRevealExitAnimation(context, view, revealSettings, colorFrom, colorTo, listener);
    }

    public static class RevealAnimationSetting implements Serializable {

        private int centerX, centerY, width, height;

        public static RevealAnimationSetting with(int centerX, int centerY, int width, int height) {
            RevealAnimationSetting setting = new RevealAnimationSetting();
            setting.centerX = centerX;
            setting.centerY = centerY;
            setting.width = width;
            setting.height = height;
            return setting;
        }

        public int getCenterX() {
            return centerX;
        }

        public int getCenterY() {
            return centerY;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }
    }

    public interface Dismissible {
        interface OnDismissedListener {
            void onDismissed();
        }

        void dismiss(OnDismissedListener listener);
    }

    public static AlphaAnimation getAlphaAnimation(long startOffset, Runnable animationStartAction, Runnable animationEndAction) {
        AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
        animation.setDuration(1000);
        animation.setStartOffset(startOffset);
        animation.setFillAfter(true);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (animationStartAction != null) {
                    animationStartAction.run();
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (animationEndAction != null) {
                    animationEndAction.run();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        return animation;
    }
}