package com.tangle.utils.own_user_photo;

import java.io.File;

public interface OwnPhotoView {
    void setError(String error);

    void setCurrentImage(File photoFile);

    void uploadAvailablePhoto(File photoFile);

    void showUploadDialog(OwnPhotoPresenter presenter);
}