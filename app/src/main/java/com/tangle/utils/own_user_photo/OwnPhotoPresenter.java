package com.tangle.utils.own_user_photo;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.TextUtils;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.utils.PlatformUtils;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class OwnPhotoPresenter {
    private static final int TAKE_PICTURE_CAMERA = 123;
    private static final int TAKE_PICTURE_GALLERY = 124;

    private Uri photoUri;
    private File photoFile;
    private Uri tempPhotoUri;
    private File tempPhotoFile;

    private OwnPhotoView ownPhotoView;

    public OwnPhotoPresenter(OwnPhotoView view) {
        this.ownPhotoView = view;
    }

    public void onAddPhotoClicked() {
        ownPhotoView.showUploadDialog(this);
    }

    public void onGalleryPickClicked(Fragment fragment) {
        requestReadPermission(fragment);
    }

    public void onTakePictureClicked(Fragment fragment) {
        try {
            tempPhotoUri = createImageFile(fragment.getActivity().getApplicationContext());
            openCamera(tempPhotoUri, fragment);
        } catch (IOException e) {
            e.printStackTrace();
            getView().setError(fragment.getActivity().getApplicationContext().getResources().getString(R.string.error_internal));
        }
    }

    private Uri createImageFile(Context context) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.UK).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        tempPhotoFile = File.createTempFile(imageFileName, ".jpg", storageDir);
        return FileProvider.getUriForFile(context,
                "com.tangle.fileprovider",
                tempPhotoFile);
    }

    private void openCamera(Uri imageUri, Fragment fragment) {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (cameraIntent.resolveActivity(fragment.getActivity().getPackageManager()) != null) {
            cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
            fragment.startActivityForResult(cameraIntent, TAKE_PICTURE_CAMERA);
        }
    }

    private void openGallery(Fragment fragment) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        fragment.startActivityForResult(Intent.createChooser(intent,
                fragment.getResources().getString(R.string.funnel_avatar_select_photo)),
                TAKE_PICTURE_GALLERY);
    }

    private void requestReadPermission(Fragment fragment) {
        new RxPermissions(fragment.getActivity()).request(Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(granted -> {
                    if (granted) {
                        openGallery(fragment);
                    }
                });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != Activity.RESULT_OK) return;
        switch (requestCode) {
            case TAKE_PICTURE_CAMERA:
                onCameraPhotoReady();
                break;
            case TAKE_PICTURE_GALLERY:
                onGalleryPhotoReady(data.getData());
                break;
        }
    }

    private void onCameraPhotoReady() {
        if (isNewPhotoAvailable()) {
            photoUri = tempPhotoUri;
            photoFile = tempPhotoFile;
            getView().setError(null);
            getView().setCurrentImage(photoFile);
        }
    }

    public void onPrivateChatFooterImgsChoose(String data) {
        if (!TextUtils.isEmpty(data)) {
            photoFile = new File(data);
        }
    }

    private void onGalleryPhotoReady(Uri uri) {
        photoUri = uri;
        photoFile = new File(PlatformUtils.StorageAccess.getPath(ApplicationLoader.getContext(), uri));
        getView().setError(null);
        getView().setCurrentImage(photoFile);
    }

    private boolean isNewPhotoAvailable() {
        return tempPhotoUri != null && tempPhotoFile.exists() && tempPhotoFile.length() > 0;
    }

    private boolean isLastPhotoAvailable() {
        return photoUri != null && photoFile.exists() && photoFile.length() > 0;
    }

    public void onNextClickedHandler() {
        getView().uploadAvailablePhoto((isLastPhotoAvailable()) ? photoFile : null);
    }

    private OwnPhotoView getView() {
        return ownPhotoView;
    }

    public void sendPhotoChat() {
        if (photoFile != null) {
            getView().setCurrentImage(photoFile);
        }
    }
}