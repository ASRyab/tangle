package com.tangle.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Patterns {
    public static final Pattern EMAIL_ADDRESS = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );

    /**
     * This pattern is intended for searching for things that look like they
     * might be phone numbers in arbitrary text, not for validating whether
     * something is in fact a phone number. It will miss many things that
     * are legitimate phone numbers.
     * <p/>
     * The pattern matches the following:
     * <ul>
     * <li>Optionally, a + sign followed immediately by one or more digits. Spaces, dots, or dashes may follow.
     * <li>Optionally, sets of digits in parentheses, separated by spaces, dots, or dashes.
     * <li>A string starting and ending with a digit, containing digits, spaces, dots, and/or dashes.
     * </ul>
     */
    public static final Pattern PHONE = Pattern.compile( // sdd = space, dot, or dash
            "(\\+[0-9]+[\\- \\.]*)?" // +<digits><sdd>*
                    + "(\\([0-9]+\\)[\\- \\.]*)?" // (<digits>)<sdd>*
                    + "([0-9][0-9\\- \\.][0-9\\- \\.]+[0-9])"); // <digit><digit|sdd>+<digit>

    public static Pattern CHARACTERS_PATTERN = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);

    public static SimpleDateFormat DATE_FORMAT_LONG = new SimpleDateFormat("EEEE, d MMM", Locale.US);
    public static SimpleDateFormat DATE_FORMAT_SHORT = new SimpleDateFormat("d MMM", Locale.US);
    public static SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("h:mm a z", Locale.US);
    public static SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("MMM d, h:mm", Locale.US);
    public static SimpleDateFormat DATE_TIME_FORMAT_LONG = new SimpleDateFormat("EEEE, d MMM, h:mm a z", Locale.US);
    public static SimpleDateFormat DATE_FORMAT_LINKED_DIALOG = new SimpleDateFormat("dd MMM yyyy 'at' HH:mm", Locale.US);

    public static List<String> extractUrlsFromText(String text) {
        List<String> containedUrls = new ArrayList<String>();
        String urlRegex = "((https?|ftp|gopher|telnet|file):((//)|(\\\\))+[\\w\\d:#@%/;$()~_?\\+-=\\\\\\.&]*)";
        Pattern pattern = Pattern.compile(urlRegex, Pattern.CASE_INSENSITIVE);
        Matcher urlMatcher = pattern.matcher(text);
        while (urlMatcher.find()) {
            containedUrls.add(text.substring(urlMatcher.start(0),
                    urlMatcher.end(0)));
        }
        return containedUrls;
    }
}