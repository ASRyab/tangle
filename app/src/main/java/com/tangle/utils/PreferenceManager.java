package com.tangle.utils;

import android.content.Context;
import android.text.TextUtils;

import com.google.common.collect.Sets;
import com.google.gson.reflect.TypeToken;
import com.tangle.api.Api;
import com.tangle.api.ServerSession;
import com.tangle.model.event.EventInfo;
import com.tangle.model.pojos.Gender;
import com.tangle.screen.likebook.filter.AgeRange;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Set;

public class PreferenceManager extends PreferenceHelper {
    private static final String TERMS_ACCEPTED = "terms accepted";
    private static final String PAYMENT_TIME = "payment time";
    private static final String FREE_EVENTS_LIST = "free event list";
    private static final String WELCOME_SCREEN = "welcome screen";
    private static final String AD_ID = "ad id";
    private static final String EMULATE_IP = "emulate ip";
    public final String TAG = this.getClass().getSimpleName();

    private static PreferenceManager instance;

    /**
     * Value who not need USER_ID
     */
    // AuthData
    private static final String REFRESH_TOKEN = "refresh_token";
    private static final String IS_TRACK_INSTALL = "isTrackInstall";
    private static final String INSTALL_REFERRER = "install_referrer";
    private static final String REFRESH_TOKEN_LAST_UPDATE_TIME = "refresh_token_last_update_time";

    /**
     * Value who need USER_ID
     */
    private static final String CURRENT_USER_ID = "current_user_id";
    private static final String SESSION = "session";

    /**
     * Likebook filters
     */
    private static final String GENDER = "gender";
    private static final String AGE_RANGE = "age_range";

    /**
     * Brand Blog
     */
    private static final String IS_BRAND_BLOG_PROMO_SHOWN = "brand_blog_promo_video";

    private static final String TARGET_COUNTRY = "target_country";

    private static final Set<String> USER_SESSION_KEYS = Sets.newHashSet(REFRESH_TOKEN, REFRESH_TOKEN_LAST_UPDATE_TIME,
            CURRENT_USER_ID, SESSION, GENDER, AGE_RANGE, FREE_EVENTS_LIST, IS_BRAND_BLOG_PROMO_SHOWN, TARGET_COUNTRY);

    public static PreferenceManager getInstance(Context context) {
        if (instance == null) {
            instance = new PreferenceManager(context.getApplicationContext());
        }
        return instance;
    }

    protected PreferenceManager(Context context) {
        super(context);
    }

    public String getRefreshToken() {
        return getStringValue(REFRESH_TOKEN, true);
    }

    public void saveRefreshToken(String refresh_token) {
        storeValue(REFRESH_TOKEN, refresh_token, true);
        saveRefreshTokenLastUpdateTime();
    }

    public boolean isInstallTracked() {
        return getBooleanValue(IS_TRACK_INSTALL, true, false);
    }

    public void setInstallTracked(boolean isTrackInstall) {
        storeValue(IS_TRACK_INSTALL, isTrackInstall, true);
    }

    public String getKeyReferrer() {
        return getStringValue(INSTALL_REFERRER, true);
    }

    public void setKeyReferrer(String referrer) {
        storeValue(INSTALL_REFERRER, referrer, true);
    }

    public long getRefreshTokenLastUpdateTime() {
        return getLongValue(REFRESH_TOKEN_LAST_UPDATE_TIME, false);
    }

    public void saveRefreshTokenLastUpdateTime() {
        storeValue(REFRESH_TOKEN_LAST_UPDATE_TIME, System.currentTimeMillis(), false);
    }

    public String getCurrentUserId() {
        return getStringValue(CURRENT_USER_ID, false);
    }

    public void saveCurrentUserId(String id) {
        storeValue(CURRENT_USER_ID, id, false);
    }

    public Integer getTargetCountry() {
        return getIntValue(TARGET_COUNTRY, false);
    }

    public void saveTargetCountry(Integer targetCountry) {
        storeValue(TARGET_COUNTRY, targetCountry, false);
    }

    public void saveSession(ServerSession session) {
        storeValue(SESSION, Api.getInst().getGson().toJson(session), true);
    }

    public ServerSession loadSession() {
        String stringValue = getStringValue(SESSION, true);
        if (TextUtils.isEmpty(stringValue)) {
            return null;
        } else {
            return Api.getInst().getGson().fromJson(stringValue, ServerSession.class);
        }
    }

    public void saveFreeEvents(HashMap<String, EventInfo> eventInfoMap) {
        Type mapType = new TypeToken<HashMap<String, EventInfo>>() {
        }.getType();
        String value = Api.getInst().getGson().toJson(eventInfoMap, mapType);
        storeValue(FREE_EVENTS_LIST, value, true);
    }

    public HashMap<String, EventInfo> loadFreeEvents() {
        String stringValue = getStringValue(FREE_EVENTS_LIST, true);
        if (TextUtils.isEmpty(stringValue)) {
            return new HashMap<>();
        } else {
            Type mapType = new TypeToken<HashMap<String, EventInfo>>() {
            }.getType();
            return Api.getInst().getGson().fromJson(stringValue, mapType);
        }
    }

    public void clearSession() {
        removeValues(USER_SESSION_KEYS);
    }

    public void saveFilterGender(Gender gender) {
        if (gender == null) {
            storeValue(GENDER, -1, false);
        } else {
            storeValue(GENDER, gender.ordinal(), false);
        }
    }

    public Gender getFilterGender() {
        int index = getIntValue(GENDER, false);
        if (index > -1) {
            return Gender.values()[index];
        } else {
            return null;
        }
    }

    public void saveFilterAgeRange(AgeRange range) {
        storeValue(AGE_RANGE, range != null ? range.name() : "", false);
    }

    public AgeRange getFilterAgeRange() {
        return AgeRange.ALL;
    }

    public void saveTermsAccepted(boolean b) {
        storeValue(TERMS_ACCEPTED, b, false);
    }

    public boolean isTermsAccepted() {
        return getBooleanValue(TERMS_ACCEPTED, false);
    }

    public long getLastPaymentTime() {
        return getLongValue(PAYMENT_TIME, false);
    }

    public void setLastPaymentTime(long time) {
        storeValue(PAYMENT_TIME, time, false);
    }

    public void saveWelcomeAccepted() {
        storeValue(WELCOME_SCREEN, true, false);
    }

    public boolean isWelcomeAccepted() {
        return getBooleanValue(WELCOME_SCREEN, false);
    }

    public void saveBrandBlogPromoShowed() {
        storeValue(IS_BRAND_BLOG_PROMO_SHOWN, true, false);
    }

    public boolean isBrandBlogPromoShown() {
        return getBooleanValue(IS_BRAND_BLOG_PROMO_SHOWN, false);
    }

    public String getAdId() {
        return getStringValue(AD_ID, false);
    }

    public void setAdId(String adId) {
        storeValue(AD_ID, adId, false);
    }

    public String getEmulateIP() {
        return getStringValue(EMULATE_IP, false);
    }

    public void setEmulateIP(String ip) {
        storeValue(EMULATE_IP, ip,false);
    }

}