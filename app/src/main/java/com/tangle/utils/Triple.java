package com.tangle.utils;

import android.support.v4.util.ObjectsCompat;

public class Triple<A, B, C> {
    public final A a;
    public final B b;
    public final C c;

    public Triple(A a, B b, C c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        } else if (!(obj instanceof Triple)) {
            return false;
        } else {
            Triple<?, ?, ?> other = (Triple)obj;
            return ObjectsCompat.equals(this.a, other.a) && ObjectsCompat.equals(this.b, other.b) && ObjectsCompat.equals(this.c, other.c);
        }
    }

    @Override
    public int hashCode() {
        return (a == null ? 0 : a.hashCode()) ^ (b == null ? 0 : b.hashCode()) ^ (c == null ? 0 : c.hashCode());
    }

    @Override
    public String toString() {
        return String.format("Triple{%s, %s, %s}", this.a, this.b, this.c);
    }

    public static <A, B, C> Triple<A, B, C> create(A a, B b, C c) {
        return new Triple<>(a, b, c);
    }
}
