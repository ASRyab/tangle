package com.tangle.utils.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewTreeObserver;

public class HideViewOnScrollListener extends RecyclerView.OnScrollListener {

    private float alpha;
    private float scale;
    private final float MAX_SCALE = 3f;
    private float scrolly = 0.f;

    private int heightViewToHide;
    private final View viewToHide;

    public HideViewOnScrollListener(View viewToHide) {
        this.viewToHide = viewToHide;

        heightViewToHide = viewToHide.getHeight();
        if (heightViewToHide == 0) {
            ViewTreeObserver viewTreeObserver = viewToHide.getViewTreeObserver();
            viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {

                    heightViewToHide = viewToHide.getHeight();

                    if (heightViewToHide > 0)
                        viewToHide.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
            });
        }
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        if (heightViewToHide > 0) {
            scrolly += dy;
            alpha = ((heightViewToHide / 2) - scrolly) / (heightViewToHide / 2);
            scale = ((heightViewToHide - scrolly) / heightViewToHide) * MAX_SCALE;

            if (alpha < 0.f) alpha = 0.f;
            if (alpha > 1.0f) alpha = 1.f;

            if (dy < 0 && scrolly > heightViewToHide) {
                alpha = 0.f;
            }
            viewToHide.setY(-scrolly / 2);
            viewToHide.setScaleX(scale);
            viewToHide.setScaleY(scale);
            viewToHide.setAlpha(alpha);
        }
    }
}

