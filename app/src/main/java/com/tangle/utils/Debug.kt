package com.tangle.utils

import android.util.Log
import com.tangle.ApplicationLoader

import com.tangle.BuildConfig
import com.tangle.R

object Debug {

    val TAG = "Debug"

    @JvmField
    var isEnabled = false
    private var isLiveServer = true

    private val isLogEnabled: Boolean
        get() = isEnabled || !isLiveServer

    @JvmStatic
    fun logD(TAG: String, msg: String) {
        if (isLogEnabled) {
            Log.d(TAG, msg)
        }
    }

    @JvmStatic
    fun logI(TAG: String, msg: String) {
        if (isLogEnabled) {
            Log.i(TAG, msg)
        }
    }

    @JvmStatic
    fun logW(TAG: String, msg: String) {
        if (isLogEnabled) {
            Log.w(TAG, msg)
        }
    }

    @JvmStatic
    fun logE(TAG: String, msg: String, e: Throwable? = null) {
        if (isLogEnabled) {
            Log.e(TAG, msg)
        }
    }

    @JvmStatic
    fun logE(TAG: String, msg: String) {
        if (isLogEnabled) {
            Log.e(TAG, msg)
        }
    }

    fun initDebug(isReleaseServer: Boolean) {
        isEnabled = BuildConfig.DEBUG || ApplicationLoader.getContext().resources.getBoolean(R.bool.is_logging)
        Debug.isLiveServer = isReleaseServer
    }

    fun logException(e: Exception?) {
        if (isLogEnabled && e != null) {
            Log.e("Exception", e.toString())
        }
    }
}
