package com.tangle.utils.glide;

import android.content.Context;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.tangle.R;
import com.tangle.model.pojos.Gender;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.PlatformUtils;
import com.tangle.utils.glide.blur.BlurTransformation;

public class RequestOptionsFactory {

    private static RequestOptions getCornersOptionsWithCenterCrop(Context context, int dp) {
        return RequestOptions.bitmapTransform(new MultiTransformation<>(
                new CenterCrop(),
                new RoundedCorners((int) PlatformUtils.Dimensions.dipToPixels(context, dp))));
    }

    public static RequestOptions getSmallRoundedCornersOptions(Context context) {
        return getCornersOptionsWithCenterCrop(context, 3);
    }

    public static RequestOptions getMediumRoundedCornersOptions(Context context) {
        return getCornersOptionsWithCenterCrop(context, 9);
    }

    public static RequestOptions getCustomizedBlurOptions(Context context, int dp, int sampling) {
        return RequestOptions.bitmapTransform(
                new BlurTransformation(context,
                        (int) PlatformUtils.Dimensions.dipToPixels(context, dp), sampling));
    }

    public static RequestOptions getDefaultBlurOptions(Context context) {
        return getCustomizedBlurOptions(context, 8, 2);
    }

    public static int getNonAvatar(ProfileData profileData) {
        //todo it can't be null! will fixed
        if (profileData == null) return R.drawable.placeholder_man;
        int res;
        Gender gender = profileData.gender;
        switch (gender) {
            case MALE:
                res = R.drawable.placeholder_man;
                break;
            case FEMALE:
                res = R.drawable.placeholder_woman;
                break;
            default:
                res = R.drawable.placeholder_man;
                break;
        }
        return res;
    }
}
