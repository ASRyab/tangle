package com.tangle.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public abstract class PreferenceHelper {
    private SharedPreferences prefs;

    protected PreferenceHelper(Context context) {
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
    }

    protected String getStringValue(String key, boolean useDecryption) {
        String value = prefs.getString(key, null);
        if (useDecryption && value != null) {
            return SecurityHelper.decodeString(value);
        } else {
            return value;
        }
    }

    protected String getStringValue(String key, boolean useDecryption, String defaultValue) {
        String value = getStringValue(key, useDecryption);
        if (value == null) {
            value = defaultValue;
        }
        return value;
    }

    protected long getLongValue(String key, boolean useDecryption) {
        String stringValue = getStringValue(key, useDecryption);
        if (stringValue == null) {
            return 0L;
        } else {
            return Long.valueOf(stringValue);
        }
    }

    protected int getIntValue(String key, boolean useDecryption) {
        String stringValue = getStringValue(key, useDecryption);
        if (stringValue == null) {
            return -1;
        } else {
            return Integer.valueOf(stringValue);
        }
    }

    protected boolean getBooleanValue(String key, boolean useDecryption) {
        return getBooleanValue(key, useDecryption, false);
    }

    protected boolean getBooleanValue(String key, boolean useDecryption, boolean defaultValue) {
        String stringValue = getStringValue(key, useDecryption);
        if (stringValue == null) {
            return defaultValue;
        } else {
            return Boolean.valueOf(stringValue);
        }
    }

    protected <U> void storeValue(String key, U value, boolean useEncryption) {
        String resultValue;
        if (useEncryption) {
            resultValue = SecurityHelper.encodeString(String.valueOf(value));
        } else {
            resultValue = String.valueOf(value);
        }
        prefs.edit().putString(key, resultValue).apply();
    }

    protected <U> void removeValue(String key) {
        prefs.edit().remove(key).apply();
    }

    protected void removeValues(Iterable<String> keys) {
        SharedPreferences.Editor editor = prefs.edit();
        for (String key : keys) {
            editor.remove(key);
        }
        editor.apply();
    }

    protected boolean containsValue(String key) {
        return prefs.contains(key);
    }
}