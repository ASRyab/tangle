package com.tangle.utils;

import android.text.TextUtils;

import java.util.Map;

public class CollectionUtils {

    public static void putStringSafe(Map<String, String> map, String key, String value) {
        if (!TextUtils.isEmpty(value)) {
            map.put(key, value);
        }
    }

}
