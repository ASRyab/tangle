package com.tangle.utils;


/**
 * 0 - user country not in target group
 * 1 - user country is target country
 * 2 - user country in target group (white list)
 */

public class UserCheckUtils {

    public static boolean isNotSupportedCountry(int isTargetCountry) {
        return isTargetCountry == 0;
    }

    public static boolean isTargetCountry(int isTargetCountry) {
        return isTargetCountry == 1;
    }

    public static boolean isCountryInWhiteList(int isTargetCountry) {
        return isTargetCountry == 2;
    }
}
