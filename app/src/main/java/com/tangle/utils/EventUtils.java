package com.tangle.utils;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.model.event.EventInfo;
import com.tangle.model.pojos.DictionaryItem;

import java.util.ArrayList;
import java.util.List;

public class EventUtils {

    public static List<DictionaryItem> initEventBadges(Context context) {
        List<DictionaryItem> dictionaryItems = new ArrayList<>();
        dictionaryItems.add(new DictionaryItem(1, context.getString(R.string.limited_space)));
        return dictionaryItems;
    }

    public static void updateEventStatus(TextView tvTicketStatus, EventInfo eventInfo) {
        if (eventInfo.hasTicket()) {
            tvTicketStatus.setBackgroundResource(R.drawable.own_profile_date_background);
            tvTicketStatus.setText(R.string.booked);
            tvTicketStatus.setVisibility(View.VISIBLE);
        } else if (eventInfo.isReserved()) {
            tvTicketStatus.setText(R.string.reserved);
            tvTicketStatus.setBackgroundResource(R.drawable.reserved_label_background);
            tvTicketStatus.setVisibility(View.VISIBLE);
        } else tvTicketStatus.setVisibility(View.GONE);
    }
}
