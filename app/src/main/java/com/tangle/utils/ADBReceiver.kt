package com.tangle.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.tangle.ApplicationLoader

import com.tangle.api.Api
import com.tangle.screen.auth.AuthorizationModel


class ADBReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        Debug.logD(LOG_TAG, "Received intent " + intent.action!!)
        when (intent.action) {
            ACTION_CHANGE_IP_ADDRESS ->
                if (intent.hasExtra(EXTRA_KEY_IP_ADDRESS)) {
                    val ip = intent.getStringExtra(EXTRA_KEY_IP_ADDRESS)
                    Debug.logD(LOG_TAG, "ip=$ip")
                    ApplicationLoader.getApplicationInstance().preferenceManager.emulateIP = ip
                } else {
                    Debug.logE(LOG_TAG, "Missed extra \"$EXTRA_KEY_IP_ADDRESS\"")
                }
            ACTION_DO_AUTO_LOGIN ->
                if (intent.hasExtra(EXTRA_KEY_AUTO_LOGIN)) {
                    val userId = intent.getStringExtra(EXTRA_KEY_AUTO_LOGIN)
                    Debug.logD(LOG_TAG, "$EXTRA_KEY_AUTO_LOGIN = $userId")
                    val model = AuthorizationModel()
                    Api.getInst().session.updateRefreshToken(userId)

                    model.autoLogin()
                            .subscribe({
                                Debug.logD(LOG_TAG, "auto login done")
                                ApplicationLoader.getApplicationInstance().navigationManager.showMainScreen()
                            }, { error -> Debug.logE(LOG_TAG, "auto login", error as Throwable) })
                } else {
                    Debug.logE(LOG_TAG, "Missed extra \"$EXTRA_KEY_AUTO_LOGIN\"")
                }
            ACTION_SET_LOGGING ->
                if (intent.hasExtra(EXTRA_KEY_DEBUG)) {
                    val debug = intent.getBooleanExtra(EXTRA_KEY_DEBUG, false)
                    Debug.logD(LOG_TAG, "debug=$debug")
                    Debug.isEnabled = debug
                    Api.getInst().resetRetrofit()
                } else {
                    Debug.logE(LOG_TAG, "Missed extra \"$EXTRA_KEY_DEBUG\"")
                }
            ACTION_OPEN_USER ->
                if (intent.hasExtra(EXTRA_KEY_USER_ID)) {
                    val userId = intent.getStringExtra(EXTRA_KEY_USER_ID)
                    Debug.logD(LOG_TAG, "ip=$userId")

                    ApplicationLoader.getApplicationInstance().navigationManager.showUserProfile(userId)

                } else {
                    Debug.logE(LOG_TAG, "Missed extra \"$EXTRA_KEY_USER_ID\"")
                }

            else -> Debug.logE(LOG_TAG, "Unknown action \"" + intent.action + "\"")
        }

    }

    companion object {
        const val ACTION_CHANGE_IP_ADDRESS = "com.tangle.CHANGE_IP_ADDRESS"
        const val ACTION_DO_AUTO_LOGIN = "com.tangle.DO_AUTO_LOGIN"
        const val ACTION_SET_LOGGING = "com.tangle.SET_LOGGING"
        const val ACTION_OPEN_USER = "com.tangle.OPEN_USER"


        const val EXTRA_KEY_IP_ADDRESS = "ip_address"
        const val EXTRA_KEY_AUTO_LOGIN = "user_auto_login"
        const val EXTRA_KEY_USER_ID = "user_id"
        const val EXTRA_KEY_DEBUG = "is_debug"

        private const val LOG_TAG = "ADB"
    }
}
