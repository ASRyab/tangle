package com.tangle.model.contact_us;

import java.io.Serializable;
import java.util.Map;

public class ContactUsData implements Serializable {
    public ContactData contactData;
    public Map<String, ContactUsCategoryData> categories;
}
