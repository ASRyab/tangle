package com.tangle.model.contact_us;

import java.io.Serializable;

public class ContactUsSubjectData implements Serializable {
    public String title;
    public String message;
}
