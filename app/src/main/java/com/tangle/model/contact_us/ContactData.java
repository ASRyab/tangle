package com.tangle.model.contact_us;

import java.io.Serializable;

public class ContactData implements Serializable {
    public String phoneNumber;
    public String companyOperatorAddressCountry;
    public String companyOperatorAddressString;
    public String email;
}