package com.tangle.model.contact_us;

import java.io.Serializable;
import java.util.Map;

public class ContactUsCategoryData implements Serializable {
    public String title;
    public String message;
    public Map<String, ContactUsSubjectData> subjects;
}
