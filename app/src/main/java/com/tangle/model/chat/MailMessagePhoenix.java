package com.tangle.model.chat;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.arch.persistence.room.TypeConverters;
import android.support.annotation.NonNull;

import com.tangle.ApplicationLoader;
import com.tangle.api.rpc.rpc_actions.chat.MsgType;
import com.tangle.utils.ConvertingUtils;

import java.io.Serializable;
import java.util.Date;

@Entity(tableName = "MailMessagePhoenix")
public class MailMessagePhoenix implements Serializable, Comparable<MailMessagePhoenix> {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    public String id;
    @Embedded
    public Sender from;
    @ColumnInfo(name = "to")
    public String to;
    @ColumnInfo(name = "time")
    public String time;
    @ColumnInfo(name = "message")
    public String message;
    @Ignore
    public int offset;
    @ColumnInfo(name = "msgType")
    public String msgType;
    @ColumnInfo(name = "isRead")
    public boolean read;
    @Ignore
    public boolean isSeparator;
    @Ignore
    public String text;
    @Ignore
    public Date dateTime;
    @TypeConverters(Converter.class)
    public ImbImageResources resources;

    public MailMessagePhoenix() {
    }

    public MailMessagePhoenix(Date dateTime, boolean isSeparator) {
        this.dateTime = dateTime;
        this.isSeparator = isSeparator;
    }

    public String getSenderId() {
        return from.id;
    }

    public Date getDateTime() {
        return ConvertingUtils.getDateFromString(time, ConvertingUtils.DATE_FORMAT_YMD_HMS_T);
    }

    public MailMessagePhoenix unify(String chatMate) {
        this.message = text;
        this.from = new Sender(ApplicationLoader.getApplicationInstance().getUserManager().getCurrentUserId());
        this.to = chatMate;

        return this;
    }

    public boolean isChatImg() {
        return msgType.equals(MsgType.IMG_MSG.value)
                && resources != null && resources.getImage() != null;
    }

    public boolean isMyMsg() {
        return ApplicationLoader.getApplicationInstance().getUserManager().isCurrentUser(from.id);
    }

    public boolean isSystemMsg() {
        return msgType.equals(MsgType.SYSTEM_MSG.value);
    }

    @Override
    public int compareTo(@NonNull MailMessagePhoenix o) {
        return o.getDateTime().compareTo(getDateTime());
    }

    public int hashCode() {
        return id.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj instanceof MailMessagePhoenix) {
            MailMessagePhoenix pp = (MailMessagePhoenix) obj;
            if (isChatImg()) {
                return pp.message.equals(this.message);
            } else
                return (pp.id.equals(this.id));
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "MailMessagePhoenix{" +
                "id='" + id + '\'' +
                ", from=" + from +
                ", to='" + to + '\'' +
                ", time='" + time + '\'' +
                ", message='" + message + '\'' +
                ", read=" + read +
                ", isSeparator=" + isSeparator +
                ", dateTime=" + dateTime +
                ", msgType=" + msgType +
                '}';
    }
}