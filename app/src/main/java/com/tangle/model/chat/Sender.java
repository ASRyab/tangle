package com.tangle.model.chat;

import android.arch.persistence.room.ColumnInfo;

public class Sender {
    @ColumnInfo(name = "sender_id")
    public String id;

    public Sender(String id) {
        this.id = id;
    }
}