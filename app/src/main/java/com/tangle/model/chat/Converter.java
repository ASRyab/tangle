package com.tangle.model.chat;

import android.arch.persistence.room.TypeConverter;

import com.tangle.api.Api;

public class Converter {

    @TypeConverter
    public String from(ImbImageResources hobbies) {
        return Api.getInst().getGson().toJson(hobbies);
    }

    @TypeConverter
    public ImbImageResources to(String data) {
        return Api.getInst().getGson().fromJson(data, ImbImageResources.class);
    }

}