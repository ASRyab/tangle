package com.tangle.model.chat;

import java.util.ArrayList;
import java.util.List;

public class ImbImageResources {
    public List<ImbImage> imbImage = new ArrayList<>();

    public ImbImage getImage() {
        if (imbImage.size() > 0) {
            return imbImage.get(0);
        } else {
            return null;
        }
    }
}