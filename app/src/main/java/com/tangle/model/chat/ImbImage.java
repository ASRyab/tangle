package com.tangle.model.chat;

public class ImbImage {
    private static final int PHOTO_WAS_NOT_IN_MESSENGER_APPROVE_QUEUE = 0;
    private static final int PHOTO_FROM_MESSENGER_APPROVED = 1;
    private static final int PHOTO_FROM_MESSENGER_DECLINED = 2;
    public static final int PHOTO_FROM_MESSENGER_PENDING = 3;

    private static final int PHOTO_LEVEL_NORMAL = 0;
    private static final int PHOTO_LEVEL_SEXY = 1;
    private static final int PHOTO_LEVEL_HARD = 2;

    public String avatar;
    public String photosendMobileAvatar;
    public String fullSize;
    public Attributes attributes;

    public int getPhotoApproveStatus() {
        return attributes != null ? attributes.isApprovedFromMessenger : 0;
    }

    class Attributes {
        public int isApprovedFromMessenger;
        public int level;
    }

    public boolean isPhotoApprovePending() {
        return attributes != null && attributes.isApprovedFromMessenger == PHOTO_FROM_MESSENGER_PENDING;
    }

    public boolean isPhotoApproveDeclined() {
        return attributes != null && attributes.isApprovedFromMessenger == PHOTO_FROM_MESSENGER_DECLINED;
    }

    public boolean isPhotoLevelNormal() {
        return attributes != null && attributes.level == PHOTO_LEVEL_NORMAL;
    }

}