package com.tangle.model.chat;

import com.tangle.model.BaseModelResponse;

import java.util.List;

public class ChatHistoryData extends BaseModelResponse {
    public List<MailMessagePhoenix> messages;
    public int offset;
    public String groupId;
}