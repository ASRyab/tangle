package com.tangle.model.payment;

import com.tangle.model.BaseModelResponse;

public class PaymentActionResult extends BaseModelResponse {
    public boolean result;
}
