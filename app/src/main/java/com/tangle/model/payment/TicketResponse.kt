package com.tangle.model.payment

import com.google.gson.annotations.SerializedName

data class TicketResponse(
        @SerializedName("ticketId")
        val ticketId: String?,
        @SerializedName("eventId")
        val eventId: String?,
        @SerializedName("userId")
        val userId: String?,
        @SerializedName("invitedBy")
        val invitedBy: String?,
        @SerializedName("inviteId")
        val inviteId: String?,
        @SerializedName("ticketStatus")
        val ticketStatus: String?,
        @SerializedName("visitStatus")
        val visitStatus: String?,
        @SerializedName("type")
        val type: String?,
        @SerializedName("reasonId")
        val reasonId: String?,
        @SerializedName("updatedAt")
        val updatedAt: String?,
        @SerializedName("createdAt")
        val createdAt: String?,
        @SerializedName("isDonated")
        val isDonated: Boolean?,
        @SerializedName("isOwn")
        val isOwn: Boolean?,
        @SerializedName("amount")
        val amount: Int?
)