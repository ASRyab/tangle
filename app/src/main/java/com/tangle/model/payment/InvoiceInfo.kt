package com.tangle.model.payment;

import com.google.gson.annotations.SerializedName;
import java.util.*

data class InvoiceInfo(
        @SerializedName("invoiceId")
        val invoiceId: String?,
        @SerializedName("parentInvoiceId")
        val parentInvoiceId: String?,
        @SerializedName("transactionId")
        val transactionId: String?,
        @SerializedName("eventId")
        val eventId: String?,
        @SerializedName("userId")
        val userId: String?,
        @SerializedName("transactionType")
        val transactionType: EventInvoiceTransactionType?,
        @SerializedName("transactionStatus")
        val transactionStatus: EventInvoiceTransactionStatus?,
        @SerializedName("amount")
        val amount: Int?,
        @SerializedName("currency")
        val currency: String?,
        @SerializedName("updatedAt")
        val updatedAt: Date?,
        @SerializedName("createdAt")
        val createdAt: String?,
        @SerializedName("count")
        val count: Int?,
        @SerializedName("price")
        val price: Int?,
        @SerializedName("currencySymbol")
        val currencySymbol: String?,
        @SerializedName("tickets")
        val tickets: List<TicketResponse>
)



enum class EventInvoiceTransactionType(val code: kotlin.Int) {
    @SerializedName("0")
    None(0),

    @SerializedName("1")
    Charge(1), // INVOICE_TYPE_INSTANT

    @SerializedName("2")
    Hold(2), // INVOICE_TYPE_HOLD

    @SerializedName("3")
    Rebook(3), // INVOICE_TYPE_REBOOK

    @SerializedName("4")
    Free(4), // INVOICE_TYPE_FREE
}

enum class EventInvoiceTransactionStatus() {
    @SerializedName("0")
    None,

    @SerializedName("1")
    Charged, // INVOICE_STATUS_CHARGED

    @SerializedName("2",alternate = ["3","4","5","6"])
    Canceled, // all of INVOICE_STATUS_CANCEL_BY_*

    @SerializedName("7")
    Retry, // INVOICE_STATUS_RETRY

    @SerializedName("8")
    Expired, // INVOICE_STATUS_EXPIRED

    @SerializedName("9")
    Reserved, // INVOICE_STATUS_HOLD

}
