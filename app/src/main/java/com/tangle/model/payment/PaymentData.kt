package com.tangle.model.payment;

import com.google.gson.annotations.SerializedName;
import java.io.Serializable

data class PaymentData(
        @SerializedName("invoiceId")
        val invoiceId: String?,
        @SerializedName("eventId")
        val eventId: String?,
        @SerializedName("inviteUserId")
        val inviteUserId: String?,
        @SerializedName("ticketsToChargeFromHold")
        val ticketsToChargeFromHold: Boolean?,
        @SerializedName("ticketsToBuyAmount")
        val ticketsToBuyAmount: Int?,
        @SerializedName("ticketsToHold")
        val ticketsToHold: Int?,
        @SerializedName("ticketCost")
        val ticketCost: Int?,
        @SerializedName("currencySymbol")
        val currencySymbol: String?,
        @SerializedName("currencyCode")
        val currencyCode: String?
) : Serializable
