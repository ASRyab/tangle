package com.tangle.model.event;

import java.io.Serializable;

public class Location implements Serializable {
    public String latitude;
    public String longitude;
    public String location;
    public String address;
    public String city;
    public String postalCode;
}