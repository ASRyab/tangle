package com.tangle.model.event;

import com.google.gson.annotations.SerializedName;

public enum TypeEvent {
    @SerializedName("0")
    DEFAULT(0),
    @SerializedName("1")
    FREE(1);

    private final int code;

    TypeEvent(int code) {
        this.code = code;
    }

    public int getCode() {
        return code;
    }
}
