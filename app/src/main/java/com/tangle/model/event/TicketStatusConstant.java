package com.tangle.model.event;

import com.google.gson.annotations.SerializedName;

public enum TicketStatusConstant {
    @SerializedName("0")
    NON_REQUESTED(0),
    @SerializedName("1")
    WAIT_APPROVE(1),
    @SerializedName("2")
    APPROVED(2),
    @SerializedName("3")
    DECLINED(3);

    public int status;

    TicketStatusConstant(int status) {
        this.status = status;
    }
}