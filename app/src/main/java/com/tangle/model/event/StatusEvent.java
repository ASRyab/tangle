package com.tangle.model.event;

import com.google.gson.annotations.SerializedName;

public enum StatusEvent {
    @SerializedName("1")
    ACTIVE,
    @SerializedName("0")
    DRAFT,
    @SerializedName("2")
    EXPIRED
}
