package com.tangle.model.event;

import android.content.Context;
import android.text.TextUtils;

import com.google.android.gms.maps.model.LatLng;
import com.tangle.R;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventInfo implements Serializable {
    public enum EventTicketStatus {
        EventTicketStatusNone,
        EventTicketStatusBought,
        EventTicketStatusReserved
    }

    public String eventId;
    // Example : 2018-03-24 09:37:43
    public Date eventStartDate;
    public Date eventEndDate;
    public String title;
    public String description;
    public int price;
    public String currency;
    public int maleTickets;
    public int femaleTickets;
    public int noGenderTickets;
    public boolean isSecretLocation;
    public StatusEvent status;
    public String feature;
    public Date createdAt;
    public String timeZone;
    public int maxInviteTickets;
    public int maxBookTickets;
    public int maxFreeTickets;
    public int ticketStatus;
    public boolean hasActivatedTicket;
    private Location location;
    public List<Media> galleryMedia = new ArrayList<>();
    public List<Media> promoMedia = new ArrayList<>();
    public ArrayList<String> usersForInvite = new ArrayList<>();
    public ArrayList<String> participantsUsers = new ArrayList<>();
    public int maleAvailableTickets;
    public int femaleAvailableTickets;
    public int noGenderAvailableTickets;
    public TypeEvent type;
    public TicketStatus freeTicketStatus;
    public List<String> linkedEvents;
    public List<String> pendingInvites;  //inviteId without answer

    // In App variable
    public boolean isInited;

    public TicketStatusConstant getFreeTicketStatus() {
        return freeTicketStatus.status;
    }

    public boolean isEventFree() {
        return type == TypeEvent.FREE;
    }

    public boolean hasTicket() {
        return getEventTicketStatus().equals(EventTicketStatus.EventTicketStatusBought);
    }

    public boolean isReserved() {
        return getEventTicketStatus().equals(EventTicketStatus.EventTicketStatusReserved);
    }

    public boolean isBookAvailable() {
        return maxBookTickets > 0;
    }

    public boolean isInviteAvailable() {
        return maxInviteTickets > 0;
    }

    public String getBookButtonText(Context context) {
        String result = context.getResources().getString(R.string.book);
        if (!isBookAvailable() && hasTicket()) {
            result = context.getResources().getString(R.string.booked);
        } else if (!isBookAvailable() && !hasTicket()) {
            result = context.getResources().getString(R.string.overbooked);
        }
        return result;
    }

    public int getAllTickets() {
        return femaleTickets + maleTickets + noGenderTickets;
    }

    public int getAllAvailableTickets() {
        return femaleAvailableTickets + maleAvailableTickets + noGenderAvailableTickets;
    }

    public EventTicketStatus getEventTicketStatus() {
        switch (ticketStatus) {
            case 0:
                return EventTicketStatus.EventTicketStatusNone;
            case 1:
                return EventTicketStatus.EventTicketStatusBought;
            case 2:
                return EventTicketStatus.EventTicketStatusReserved;
            default:
                return EventTicketStatus.EventTicketStatusNone;
        }
    }

    public boolean hasPassed() {
        return new Date().after(eventEndDate);
    }

    public boolean hasStarted() {
        return new Date().after(eventStartDate);
    }

    public LatLng getEventPosition() {
        return !hasValidLocation() ? null : new LatLng(
                Float.parseFloat(location.latitude),
                Float.parseFloat(location.longitude));
    }

    public boolean hasValidLocation() {
        return location != null && !TextUtils.isEmpty(location.address);
    }

    public String getEventAddress(Context context) {
        if (!isSecretLocation) {
            return location == null ? null : location.address;
        } else {
            return context.getString(R.string.event_secret_place);
        }
    }

    public String getEventLocation() {
        return location.location;
    }

    public List<Media> getActualMediaList() {
        return promoMedia;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("EventInfo{");
        sb.append("eventId='").append(eventId).append('\'');
        sb.append(", title=").append(title);
        sb.append(", ticketStatus=").append(ticketStatus);
        sb.append('}');
        return sb.toString();
    }

    public String getPreviewImageUrl() {
        List<Media> actualMediaList = getActualMediaList();
        if (actualMediaList == null || actualMediaList.isEmpty()) {
            return null;
        } else {
            Media firstItem = actualMediaList.get(0);
            return Media.Type.VIDEO.equals(firstItem.type) ? firstItem.videoImagePreviewUrl : firstItem.photoThumbnail2xUrl;
        }
    }

    public String getPostCode() {
        return location.postalCode;
    }
}