package com.tangle.model.event;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Media implements Serializable {

    public enum Type {
        @SerializedName("photo")
        PHOTO,
        @SerializedName("video")
        VIDEO
    }

    public String id;
    @SerializedName("mediaType")
    public Type type;
    public int isPrimary;
    public int order;

    @SerializedName("big3xUrl")
    public String photoBig3xUrl;
    @SerializedName("big2xUrl")
    public String photoBig2xUrl;
    @SerializedName("thumbnail3xUrl")
    public String photoThumbnail3xUrl;
    @SerializedName("thumbnail2xUrl")
    public String photoThumbnail2xUrl;

    @SerializedName("previewUrl")
    public String videoImagePreviewUrl;
    @SerializedName("originUrl")
    public String videoOriginUrl;
    @SerializedName("externalResourceId")
    public String videoExternalResourceId;


    public Media() {
    }

    public Media(String id, String photoUrl) {
        this.id = id;
        this.photoBig2xUrl = photoUrl;
        this.type = Type.PHOTO;
    }

    public Media(String id, String videoOriginUrl, String videoImagePreviewUrl) {
        this.id = id;
        this.videoOriginUrl = videoOriginUrl;
        this.type = Type.VIDEO;
        this.videoImagePreviewUrl = videoImagePreviewUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Media media = (Media) o;

        return id != null ? id.equals(media.id) : media.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
