package com.tangle.model;

public class ServerMessage {
    private ServerMessageType type;
    private boolean notify;

    public ServerMessageType getType() {
        return type;
    }

    public boolean isNotify() {
        return notify;
    }
}