package com.tangle.model.activities.liked_you;

import com.tangle.model.BaseModelResponse;
import com.tangle.model.profile_data.ProfileData;

import java.util.List;

public class LikedYouResponse extends BaseModelResponse {
    private List<ProfileData> users;

    public LikedYouResponse(List<ProfileData> users) {
        this.users = users;
    }

    public List<ProfileData> getLikedYouUsers() {
        return users;
    }

    @Override
    public String toString() {
        return "LikedMeResponse{" +
                "users=" + users +
                '}';
    }
}