package com.tangle.model.blog;

import android.support.annotation.NonNull;

import com.tangle.model.event.EventInfo;

import java.util.Date;

public class BlogEntity
        implements Comparable<BlogEntity>
{
    private String postId;
    private String eventId;
    private String categoryId;
    private String title;
    private String description;
    private Date postDate;
    private Integer status;
    private Boolean isDisplayMedia;
    private Date updatedAt;
    private Date createdAt;
    private EventInfo event;
    private String coverImage;
    private String timeZone;
    private Date postTzDate;

    public String getPostId() {
        return postId;
    }

    public String getEventId() {
        return eventId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Date getPostDate() {
        return postDate;
    }

    public Integer getStatus() {
        return status;
    }

    public Boolean getDisplayMedia() {
        return isDisplayMedia;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public EventInfo getEvent() {
        return event;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public Date getPostTzDate() {
        return postTzDate;
    }

    @Override
    public int compareTo(@NonNull BlogEntity entity) {
        return entity.postDate.compareTo(postDate);
    }
}
