package com.tangle.model.profile_data;

import com.tangle.model.BaseModelResponse;

public class PhotoDelete extends BaseModelResponse {
    public String id;
    public String link;
    public boolean result;
    public boolean changed;
    public int count;
}