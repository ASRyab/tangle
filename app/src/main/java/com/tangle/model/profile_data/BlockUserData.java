package com.tangle.model.profile_data;

import com.tangle.model.BaseModelResponse;

public class BlockUserData extends BaseModelResponse {
    public boolean unblocked;
    public boolean blocked;
}
