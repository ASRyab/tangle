package com.tangle.model.profile_data;

public class ProfileGeo {
    public String country;
    public String city;
    public String cityArea;
    public String region;
    public String postcode;
    public String regionCode;
    public String country_code;
}