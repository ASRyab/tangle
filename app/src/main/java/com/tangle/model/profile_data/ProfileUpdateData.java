package com.tangle.model.profile_data;

import com.tangle.model.BaseModelResponse;

public class ProfileUpdateData extends BaseModelResponse {
    public Integer age;

    public static ProfileUpdateData convertFromProfile(ProfileData profile) {
        ProfileUpdateData data = new ProfileUpdateData();
        data.age = profile.age;
        return data;
    }
}
