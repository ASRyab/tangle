package com.tangle.model.profile_data;

import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;
import com.tangle.model.BaseModelResponse;

public class EmailUpdateData extends BaseModelResponse {
    @Nullable
    @SerializedName("password")
    private String password;
    @Nullable
    @SerializedName("email")
    private String email;
    @Nullable
    @SerializedName("emailService")
    private String emailService;
    @SerializedName("is_valid")
    private boolean isValid;
    @Nullable
    @SerializedName("environment")
    private String environment;
    @Nullable
    @SerializedName("csrfToken")
    private CsrfToken csrfToken;


    class CsrfToken {
        @Nullable
        @SerializedName("name")
        private String name;
        @Nullable
        @SerializedName("value")
        private String value;
    }
}

