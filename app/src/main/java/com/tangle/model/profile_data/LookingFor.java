package com.tangle.model.profile_data;

import com.tangle.model.pojos.Gender;

public class LookingFor {
    public Gender gender;
    public String location;
    public String distance;
    public String country;
    public String age_from;
    public String age_to;
}