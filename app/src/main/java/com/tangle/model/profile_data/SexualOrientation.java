package com.tangle.model.profile_data;

import com.google.gson.annotations.SerializedName;

public enum SexualOrientation {
    @SerializedName(value = "homo", alternate = {"Gay", "Lesbian"})
    HOMO("homo"),
    @SerializedName(value = "hetero", alternate = "Straight")
    HETERO("hetero");

    private final String name;

    SexualOrientation(String orientation) {
        this.name = orientation;
    }

    public String getName() {
        return name;
    }
}
