package com.tangle.model.profile_data;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.pojos.Gender;
import com.tangle.utils.ConvertingUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

public class ProfileData extends BaseModelResponse {

    public static ProfileData EMPTY_STATE = new ProfileData();

    public String id;
    //NOT USE
    private String name;
    public String password;
    public Gender gender;
    public Integer age;
    public List<Photo> photos;

    public String login;
    public String email;
    public String birthday;
    public String gender_key;
    @SerializedName(value = "createdAt")
    public String lastUserActivity;
    public ArrayList<String> futureEventsList;
    public ArrayList<String> pastEventsList;
    public ArrayList<String> eventsForInvite;
    public Map<String, Boolean> interests;
    public LookingFor looking;
    public String lookingForGender;
    public SexualOrientation sexual_orientation;

    public ProfileGeo geo;
    public String location;

    public String about;
    public String description;
    public int photo_count;
    private int photoCount;
    public int isTargetCountry;

    private String industry;
    private String careerLevel;
    public String matchedEvent;
    // In App Variables
    public Photo primaryPhoto;
    public boolean isNew;
    public boolean isYouLikedUser;
    @SerializedName(value = "matched", alternate = {"isMatchedUser"})
    public boolean isMatchedUser;
    public boolean isLikedMeUser;
    public boolean isYouSkippedUser;
    public boolean isUserCanBeLiked;
    public boolean blockedUser;
    public boolean blockedByUser;
    public boolean isTester;

    public boolean isInited;

    public static ProfileData mock() {
        ProfileData profileData = new ProfileData();
        Photo photo = new Photo();
        photo.avatar = "https://i.ebayimg.com/images/g/ubMAAMXQZdFRHCjn/s-l300.jpg";
        profileData.primaryPhoto = photo;
        return profileData;
    }

    public boolean isHomo() {
        return SexualOrientation.HOMO.equals(sexual_orientation);
    }

    public String getIndustry() {
        if (TextUtils.isEmpty(industry)) {
            return "0";
        }
        return industry;
    }

    public String getCareerLevel() {
        if (TextUtils.isEmpty(careerLevel)) {
            return "0";
        }
        return careerLevel;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public void setCareerLevel(String careerLevel) {
        this.careerLevel = careerLevel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ProfileData)) {
            return false;
        }
        ProfileData profile = (ProfileData) o;
        return id != null && profile.id != null && id.equals(profile.id);
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

    public String getNOTUSEDName() {
        return name;
    }

    public int getNOTUSEDPhotoCount() {
        return photoCount;
    }

    public boolean isBlocked() {
        return blockedByUser || blockedUser;
    }

    public Date getLastUserActivityDate() {
        return ConvertingUtils.getDateFromString(lastUserActivity, ConvertingUtils.DATE_FORMAT_YMD_HMS_T);
    }
}