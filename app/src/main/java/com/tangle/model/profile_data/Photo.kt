package com.tangle.model.profile_data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Embedded
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import android.text.TextUtils

import com.google.gson.annotations.SerializedName

import java.io.Serializable

@Entity(tableName = "Photo")
data class Photo(
        @JvmField
        @NonNull
        @PrimaryKey
        @ColumnInfo(name = "id")
        var id: String,

        @SerializedName("hash")
        @ColumnInfo(name = "hash")
        var hash: String? = null,

        @JvmField
        @SerializedName("avatar")
        @ColumnInfo(name = "avatar")
        var avatar: String? = null,

        @SerializedName("downsize")
        @ColumnInfo(name = "downsize")
        var downsize: String? = null,

        @SerializedName("resized")
        @ColumnInfo(name = "resized")
        var resized: String? = null,// this use ios

        @SerializedName("normal")
        @JvmField
        @ColumnInfo(name = "normal")
        var normal: String? = null,

        @SerializedName("is_primary")
        @JvmField
        @ColumnInfo(name = "isPrimary")
        var isPrimary: Int = 0,

        @JvmField
        @SerializedName("pendingDelete")
        @ColumnInfo(name = "pendingDelete")
        var pendingDelete: Boolean = false,

        @JvmField
        @ColumnInfo(name = "isDeleteByUser")
        var isDeleteByUser: Boolean = false,

        @SerializedName("attributes")
        @Embedded
        var attributes: Attributes) : Serializable, Comparable<Photo> {

    constructor() : this("", null, "", "", "", "", 0, false, false, Attributes())

    override fun equals(o: Any?): Boolean {
        if (this === o) return true
        if (o !is Photo) return false
        val photo = o as Photo?
        return !TextUtils.isEmpty(id) && id == photo!!.id
    }

    override fun hashCode(): Int {
        return if (id != null) id!!.hashCode() else 0
    }

    override fun compareTo(o: Photo): Int {
        return Integer.compare(o.isPrimary, isPrimary)
    }

    override fun toString(): String {
        val sb = StringBuffer("\n\nPhoto{")
        sb.append("id='").append(id).append('\'')
        sb.append("\nhash='").append(hash).append('\'')
        sb.append("\navatar='").append(avatar).append('\'')
        sb.append("\nisPrimary=").append(isPrimary)
        sb.append("\npendingDelete=").append(pendingDelete)
        sb.append('}')
        return sb.toString()
    }

}

fun createEmpty(): Photo {
    return Photo()
}

data class Attributes(

        @SerializedName("privatePhoto")
        @ColumnInfo(name = "privatePhoto")
        var privatePhoto: Int? = null,

        @SerializedName("width")
        @ColumnInfo(name = "width")
        var width: Int? = null,

        @SerializedName("height")
        @ColumnInfo(name = "height")
        var height: Int? = null,

        @SerializedName("level")
        @ColumnInfo(name = "level")
        var level: Int? = null,

        @SerializedName("rate")
        @ColumnInfo(name = "rate")
        var rate: Int? = null
)