package com.tangle.model;

import android.text.Html;

import com.google.gson.annotations.Expose;

import java.util.HashMap;

public class Meta {
    private final String[] possibleErrorKeys = {
            "message",
            "password",
            "errors",
            "email",
            "general"
    };

    public Meta(int code, String redirect, HashMap<String, String[]> description) {
        this.code = code;
        this.redirect = redirect;
        this.description = description;
    }

    public Meta() {
    }

    @Expose
    private int code;

    @Expose
    private String redirect;

    @Expose
    private HashMap<String, String[]> description;

    public int getCode() {
        return code;
    }

    public HashMap<String, String[]> getDescription() {
        return description;
    }

    public String getFirstMessage() {
        String firstKey = null;
        if (description != null && !description.isEmpty()) {
            for (String key : possibleErrorKeys) {
                if (description.containsKey(key)) {
                    firstKey = key;
                    break;
                }
            }
            if (firstKey == null) {
                firstKey = description.keySet().iterator().next();
            }
            return Html.fromHtml(description.get(firstKey)[0]).toString();
        }
        return null;
    }

    public String getFirstMessageByKey(String key) {
        if (description != null && !description.isEmpty()) {
            if (description.containsKey(key) && description.get(key).length > 0) {
                return Html.fromHtml(description.get(key)[0]).toString();
            }
        }
        return null;
    }

    public String getRedirect() {
        return redirect;
    }

    @Override
    public String toString() {
        return "Meta{" +
                "code=" + code +
                ", redirect='" + redirect + '\'' +
                ", description=" + description +
                '}';
    }
}