package com.tangle.model.like_or_not;

import com.tangle.model.pojos.Gender;
import com.tangle.model.profile_data.Photo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.model.profile_data.ProfileGeo;

import java.io.Serializable;
import java.util.List;

public class LikeOrNotUser implements Serializable {
    public String id;
    public String name;
    public String country;
    public String city;
    public int age;
    // Not used, better primary photo !
    public String photo;
    public Gender gender;
    public List<Photo> photos;
    public int photoCount;

    public ProfileData toData() {
        ProfileData profileData = new ProfileData();
        profileData.id = id;
        profileData.login = name;
        profileData.geo = new ProfileGeo();
        profileData.geo.country = country;
        profileData.geo.city = city;
        profileData.age = age;
        profileData.photos = photos;
        profileData.photo_count = photos.size();
        profileData.gender = gender;
        return profileData;
    }
}