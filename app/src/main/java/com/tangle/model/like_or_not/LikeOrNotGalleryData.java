package com.tangle.model.like_or_not;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LikeOrNotGalleryData {

    @SerializedName("users")
    public List<LikeOrNotUser> userGallery;
    public long availabilityTime;

}
