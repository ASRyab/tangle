package com.tangle.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class BaseModelResponse<T> implements Serializable {

    private Status status;
    private Meta meta;
    private T data;

    public BaseModelResponse() {
    }

    public Status getStatus () {
        return status;
    }

    public T getData() {
        return data;
    }

    public Meta getMeta () {
        return meta;
    }

    public boolean isError() {
        return Status.ERROR.equals(status);
    }

    public enum Status {
        @SerializedName("success")
        SUCCESS,

        @SerializedName("error")
        ERROR
    }

    @Override
    public String toString() {
        return "BaseModelResponse{" +
                "status=" + status +
                ", meta=" + meta +
                ", data=" + data +
                '}';
    }
}