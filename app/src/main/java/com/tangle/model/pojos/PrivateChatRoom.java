package com.tangle.model.pojos;

import android.support.annotation.NonNull;

import com.tangle.model.chat.MailMessagePhoenix;
import com.tangle.utils.ConvertingUtils;
import com.tangle.utils.NoDuplicatesList;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class PrivateChatRoom implements Serializable, Comparable<PrivateChatRoom> {

    public PrivateChatRoom() {
    }

    public PrivateChatRoom(MailMessagePhoenix lastChatRoomMsg, List<MailMessagePhoenix> roomMsg, String chatId) {
        this.lastChatRoomMsg = lastChatRoomMsg;
        this.roomMsg.addAll(roomMsg);
        this.chatId = chatId;
    }

    public MailMessagePhoenix lastChatRoomMsg;
    public NoDuplicatesList<MailMessagePhoenix> roomMsg = new NoDuplicatesList<>();
    public String chatId;

    @Override
    public int compareTo(@NonNull PrivateChatRoom o) {
        return getDateTime().compareTo(o.getDateTime());
    }

    public Date getDateTime() {
        return ConvertingUtils.getDateFromString(lastChatRoomMsg.time, ConvertingUtils.DATE_FORMAT_YMD_HMS_T);
    }

    @Override
    public String toString() {
        return "Chat Id : " + chatId;
    }

    public int hashCode() {
        return chatId.hashCode();
    }

    public boolean equals(Object obj) {
        if (obj instanceof PrivateChatRoom) {
            PrivateChatRoom pp = (PrivateChatRoom) obj;
            return (pp.chatId.equals(this.chatId));
        } else {
            return false;
        }
    }
}