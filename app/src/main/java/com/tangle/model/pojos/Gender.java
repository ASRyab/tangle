package com.tangle.model.pojos;

import com.google.gson.annotations.SerializedName;

public enum Gender {
    @SerializedName(value = "male", alternate = {"a man", "1", "m"})
    MALE("male"),
    @SerializedName(value = "female", alternate = {"a woman", "2", "f"})
    FEMALE("female");

    String name;

    Gender(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getIndex() {
        switch (this) {
            case MALE:
                return 1;
            case FEMALE:
                return 2;
            default:
                return 1;
        }
    }
}