package com.tangle.model.pojos;


import com.tangle.R;
import com.tangle.model.profile_data.ProfileData;

import java.util.List;

public class InnAppNotification {
    public List<ProfileData> profiles;
    public String title;
    public String message;
    public Kind kind;

    public ProfileData firstProfile() {
        return profiles.get(0);
    }

    public enum Kind {
        WHO_LIKED(R.string.notification_who_liked)
        , MUTUAL_LIKE(R.string.notification_new_match)
        , MESSAGE(R.string.notification_new_message)
        , MESSAGES(R.string.notification_new_message)
        , INVITE(R.string.notification_send_invitation);

        private final int message;

        Kind(int message) {
            this.message = message;
        }

        public int getMessage() {
            return message;
        }
    }

}
