package com.tangle.model.pojos;

import java.io.Serializable;
import java.util.List;

import io.reactivex.ObservableTransformer;
import io.reactivex.functions.BiFunction;

public class DictionaryItem implements Serializable {
    public int id;
    public String name;

    public DictionaryItem(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DictionaryItem industry = (DictionaryItem) o;

        if (id != industry.id) return false;
        return name != null ? name.equals(industry.name) : industry.name == null;
    }

    @Override
    public String toString() {
        return name + ", id : " + id;
    }

    public static ObservableTransformer<List<DictionaryItem>, List<DictionaryItem>> composeNotGiven() {
        return upstream -> upstream
                .flatMapIterable(list -> list)
                .filter(item -> item.id != 0)
                .toList()
                .toObservable();
    }

    public static BiFunction<DictionaryItem, DictionaryItem, String> industryWithCareer() {
        return (industry, career) -> {
            StringBuilder builder = new StringBuilder().append(industry.id != 0 ? industry.name : "");
            if ((industry.id != 0 && career.id != 0)) {
                builder.append(", ");
            }
            builder.append(career.id != 0 ? career.name : "");
            return builder.toString();
        };
    }
}