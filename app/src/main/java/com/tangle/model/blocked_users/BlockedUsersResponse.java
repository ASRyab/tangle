package com.tangle.model.blocked_users;

import com.tangle.model.profile_data.ProfileData;

import java.util.List;

public class BlockedUsersResponse {
    public List<ProfileData> users;
}
