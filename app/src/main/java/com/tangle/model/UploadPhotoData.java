package com.tangle.model;

public class UploadPhotoData {

    public String id;
    public String imgSize;
    public String normal;
    public String avatar;

    @Override
    public String toString() {
        return "UploadPhotoData{" +
                "id='" + id + '\'' +
                ", imgSize='" + imgSize + '\'' +
                ", normal='" + normal + '\'' +
                ", avatar='" + avatar + '\'' +
                '}';
    }
}
