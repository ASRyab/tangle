package com.tangle.model;

public enum ServerMessageType {
    MAIL("mail"),
    MESSAGES_READ("readmessages"),
    WINK("wink"),
    VIEW("view"),
    COUNTERS("counters"),
    ASK_FOR_PHOTO("askForPhoto"),
    ASK_FOR_PHOTO_UPLOADED("askForPhotoUploaded"),
    VIDEO_PROCESSED("videoProcessed"),
    VIDEO_APPROVED("userVideoApproved"),
    VIDEO_DECLINED("userVideoDeclined"),
    VIDEO_SEND_APPROVED("videoSendApproved"),
    VIDEO_SEND_DECLINED("videoSendDeclined"),
    PHOTO_SEND_DECLINED("photoSendDeclined");

    private String value;

    ServerMessageType(String value) {
        this.value = value;
    }

    public static ServerMessageType getValueFor(String type) {
        ServerMessageType result = null;
        for (ServerMessageType messageType : ServerMessageType.values()) {
            if (messageType.value.equals(type)) {
                result = messageType;
                break;
            }
        }
        return result;
    }
}