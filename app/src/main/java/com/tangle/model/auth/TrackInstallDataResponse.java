package com.tangle.model.auth;

public class TrackInstallDataResponse {
    private String redirectUrl;

    public String getRedirectUrl() {
        return redirectUrl;
    }

    @Override
    public String toString() {
        return "EmptyData{" +
                "redirectUrl='" + redirectUrl + '\'' +
                '}';
    }
}
