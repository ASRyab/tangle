package com.tangle.model.auth;

public class RemindPasswordDataResponse {

    private boolean valid;
    private String message;

    public boolean isValid() {
        return valid;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "RemindPasswordModel{" +
                "valid=" + valid +
                ", message='" + message + '\'' +
                '}';
    }
}