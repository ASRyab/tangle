package com.tangle.model.auth;

public class AuthDataResponse {
    private String autologin_key;
    private String token_type;
    private String refresh_token;
    private String access_token;

    private boolean isFunnelCompleted;

    public String getAutologinKey()
    {
        return autologin_key;
    }

    public String getTokenType()
    {
        return token_type;
    }

    public String getRefreshToken()
    {
        return refresh_token;
    }

    public String getAccessToken()
    {
        return access_token;
    }
    public void setAccessToken(String access_token) {
        this.access_token = access_token;
    }

    public boolean isFunnelCompleted() {
        return isFunnelCompleted;
    }

    @Override
    public String toString() {
        return "AuthData{" +
                "autologin_key='" + autologin_key + '\'' +
                ", token_type='" + token_type + '\'' +
                ", refresh_token='" + refresh_token + '\'' +
                ", access_token='" + access_token + '\'' +
                ", isFunnelCompleted=" + isFunnelCompleted +
                '}';
    }
}