package com.tangle.model.media

data class MediaResponse(var media: MediaList)

data class MediaList(var tangle_gallery_promo: List<PlacementName>)

data class PlacementName(var mediaType: String, var originalUrl: String, var previewUrl: String, var originSize: OriginalSize)

data class OriginalSize(var width: Int, var height: String)