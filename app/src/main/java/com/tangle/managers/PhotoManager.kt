package com.tangle.managers

import com.tangle.ApplicationLoader
import com.tangle.db.photo.PhotoDao
import com.tangle.model.profile_data.Photo
import com.tangle.model.profile_data.ProfileData
import com.tangle.utils.RxUtils
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.SingleEmitter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit

class PhotoManager(val dao: PhotoDao) {
    private var observable: SingleEmitter<PhotoState>? = null
    private var photoStatusPublisher: PublishSubject<PhotoStatus> = PublishSubject.create()
    private var currentPhotos: ArrayList<Photo> = ArrayList()

    enum class PhotoState {
        HAS_PHOTO, HAS_APPROVE_PHOTO, NO_PHOTO
    }

    enum class PhotoStatus {
        PHOTO_APPROVE_STATUS_NONE,
        PHOTO_APPROVE_STATUS_WAITING_FOR_APPROVE,
        PHOTO_APPROVE_STATUS_APPROVED,
        PHOTO_APPROVE_STATUS_DECLINED
    }

    interface PhotoView {
        fun openPhotoChooseScreen()
        fun showNoApprovePhoto()
    }

    init {
        dao.allFlowable
                .doOnNext { currentPhotos.clear() }
                .subscribe(Consumer { it -> currentPhotos.addAll(it) }, RxUtils.getEmptyErrorConsumer("init"))
    }

    fun checkPhoto(view: PhotoView): Single<PhotoState> {
        return getCurrentUser()
                .map { profileData ->
                    if (!profileData.photos.isEmpty()) {
                        if (profileData.primaryPhoto != null && profileData.primaryPhoto.attributes.level != null)
                            PhotoState.HAS_APPROVE_PHOTO
                        else
                            PhotoState.HAS_PHOTO
                    } else {
                        view.openPhotoChooseScreen()
                        PhotoState.NO_PHOTO
                    }
                }
    }

    fun checkPhotoObservable(view: PhotoView): Single<PhotoState> {
        return getCurrentUser()
                .flatMap<PhotoState> { profileData ->
                    if (!profileData.photos.isEmpty()) {
                        Single.just<PhotoState>(PhotoState.HAS_PHOTO)
                    } else {
                        view.openPhotoChooseScreen()
                        Single.create<PhotoState> { emitter -> this.observable = emitter }
                    }
                }
                .delay(20, TimeUnit.MILLISECONDS)//todo blinking need fix
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun getCurrentUser(): Single<ProfileData> {
        return ApplicationLoader.getApplicationInstance()
                .userManager
                .currentUserWithUpdates()
                .firstOrError()
    }

    fun updatePhotoState(state: PhotoState) {
        observable?.onSuccess(state)
    }

    fun onReceiveNewPhotos(photos: List<Photo>?) {
        photos?.let { photoStatusPublisher.onNext(checkActualPhotos(it)) }
    }

    fun getPhotoStatusObservable(): Observable<PhotoStatus> {
        return photoStatusPublisher.subscribeOn(AndroidSchedulers.mainThread())
    }


    fun saveNewPhoto(newPhotos: List<Photo>?) {
        newPhotos?.let {
            dao.deleteAll()
            dao.insertAll(it)
        }
    }

    private fun checkActualPhotos(newPhotos: List<Photo>): PhotoStatus {
        var isDeclined = false
        var hasApproved = false
        val isWait = newPhotos.size > currentPhotos.size
        for (value in currentPhotos) {
            isDeclined = if (!isDeclined) (!newPhotos.contains(value) && !value.isDeleteByUser) else true
            hasApproved = if (!hasApproved) (value.attributes.level != null) else true
            if (newPhotos.contains(value)
                    && newPhotos[newPhotos.indexOf(value)].attributes.level != null
                    && value.attributes.level == null)
                return PhotoStatus.PHOTO_APPROVE_STATUS_APPROVED
        }
        return when {
            isDeclined -> PhotoStatus.PHOTO_APPROVE_STATUS_DECLINED
            isWait && !hasApproved -> PhotoStatus.PHOTO_APPROVE_STATUS_WAITING_FOR_APPROVE
            else -> PhotoStatus.PHOTO_APPROVE_STATUS_NONE
        }
    }

    fun removeByUser(photoId: String) {
        currentPhotos.find { it.id == photoId }?.isDeleteByUser = true
    }
}