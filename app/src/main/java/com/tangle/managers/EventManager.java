package com.tangle.managers;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.ShareCompat;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.rx_tasks.events.GetEventById;
import com.tangle.api.rx_tasks.events.GetEventList;
import com.tangle.api.rx_tasks.events.GetEventsByIds;
import com.tangle.api.rx_tasks.payment.ChargeTicket;
import com.tangle.base.repository.BaseQueueManager;
import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.StatusEvent;
import com.tangle.model.event.TicketStatusConstant;
import com.tangle.model.event.TypeEvent;
import com.tangle.model.payment.PaymentActionResult;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.RxUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class EventManager extends BaseQueueManager<EventInfo> {
    private static final String TAG = EventManager.class.getSimpleName();
    private final PublishSubject<EventInfo> updatesPublisher = PublishSubject.create();
    private final PublishSubject<List<EventInfo>> updatesPublisherList = PublishSubject.create();
    private final PublishSubject<EventInfo> freeEventDialogPublisher = PublishSubject.create();
    private AtomicBoolean isAllLoaded = new AtomicBoolean();
    private HashMap<String, EventInfo> freeEvents;

    private Observable<List<EventInfo>> loadNewObservable;

    public EventManager() {
        freeEvents = ApplicationLoader.getApplicationInstance().getPreferenceManager().loadFreeEvents();
        loadNewObservable = new GetEventList().getDataTask()
                .doOnNext(__ -> deleteAll())
                .compose(composeSort())
                .flatMap(this::addAllObserve)
                .doOnNext(__ -> isAllLoaded.set(true))
                .flatMap(__ -> getAll())
                .share();
    }

    public Observable<EventInfo> getEventById(String eventId) {
        return getFromRepositoryById(eventId)
                .flatMap(eventInfo -> {
                    if (eventInfo.eventId == null) {
                        return getFromServerById(eventId);
                    } else {
                        return Observable.just(eventInfo);
                    }
                });
    }

    public Observable<EventInfo> getEventByIdAndUpdate(String eventId) {
        return getEventById(eventId)
                .doOnNext(info -> getFromServerById(info.eventId)
                        .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(this.getClass().getSimpleName(), "update after")));
    }

    private Observable<EventInfo> getFromServerById(String eventId) {
        return new GetEventById(eventId).getDataTask()
                .doOnNext(eventInfo -> eventInfo.isInited = true)
                .compose(composeToDb());
    }

    private Observable<EventInfo> getFromRepositoryById(String id) {
        EventInfo eventInfo = allItems.get(id);
        return Observable.just(eventInfo == null ? new EventInfo() : eventInfo);
    }

    public Observable<List<EventInfo>> getEventsByIds(List<String> eventIds) {
        if (eventIds.isEmpty()) {
            return Observable.empty();
        } else {
            return getFromRepositoryByIds(eventIds)
                    .flatMap(data -> {
                        if (data.size() != eventIds.size()) {
                            return getFromServerByIds(eventIds);
                        } else {
                            return Observable.just(data);
                        }
                    })
                    .compose(composeSort());
        }
    }

    public Observable<List<EventInfo>> getEventsByIdsActual(List<String> eventIds) {
        return getEventsByIds(eventIds).compose(checkStatusEvents())
                .flatMapIterable(list -> list)
                .filter(info -> TypeEvent.DEFAULT == info.type)
                .toList().toObservable();
    }

    private Observable<List<EventInfo>> getFromServerByIds(List<String> eventIds) {
        List<String> ids = getAskIds(eventIds);
        if (ids.isEmpty()) {
            return Observable.empty();
        }
        return new GetEventsByIds(ids).getDataTask().observeOn(Schedulers.io())
                .doOnNext(this::removeReceivedId)
                .compose(composeSort())
                .flatMap(this::addSingleObserve)
                .flatMap(__ -> getFromRepositoryByIds(eventIds));
    }

    protected void removeReceivedId(List<EventInfo> data) {
        for (EventInfo eventInfo : data) {
            cashList.remove(eventInfo.eventId);
        }
    }

    private Observable<List<EventInfo>> getFromRepositoryByIds(List<String> eventIds) {
        return Observable.fromCallable(() -> {
            List<EventInfo> dataList = new ArrayList<>();
            for (String eventId : eventIds) {
                if (allItems.containsKey(eventId)) {
                    dataList.add(allItems.get(eventId));
                }
            }
            return dataList;
        });
    }

    private ObservableTransformer<List<EventInfo>, List<EventInfo>> checkStatusEvents() {
        return upstream -> upstream
                .flatMap(infos -> Observable.fromIterable(infos)
                        .compose(checkStatusEvent())
                        .toList().toObservable());
    }

    ObservableTransformer<EventInfo, EventInfo> composeToDb() {
        return upstream -> upstream
                .doOnNext(itemResult -> allItems.put(itemResult.eventId, itemResult))
                .doOnNext(this::checkFreeEvent)
                .doOnNext(updatesPublisher::onNext);

    }

    private void checkFreeEvent(EventInfo info) {
        if (info.type != TypeEvent.FREE) {
            return;
        }

        if (!freeEvents.containsKey(info.eventId)) {
            saveFreeEvents(info);
        } else if (freeEvents.get(info.eventId).getFreeTicketStatus() == TicketStatusConstant.WAIT_APPROVE
                && info.getFreeTicketStatus() != TicketStatusConstant.WAIT_APPROVE) {
            saveFreeEvents(info);
            showFreeTicketResponseDialog(info);
        } else if (freeEvents.get(info.eventId).getFreeTicketStatus() != info.getFreeTicketStatus()) {
            saveFreeEvents(info);
        }
    }

    private void saveFreeEvents(EventInfo info) {
        freeEvents.put(info.eventId, info);
        ApplicationLoader.getApplicationInstance().getPreferenceManager().saveFreeEvents(freeEvents);
    }

    private void showFreeTicketResponseDialog(EventInfo info) {
        freeEventDialogPublisher.onNext(info);
    }

    public Observable<EventInfo> updateFreeEvent() {
        return freeEventDialogPublisher;
    }

    Observable<List<EventInfo>> addAllObserve(List<EventInfo> items) {
        return Observable.fromIterable(items)
                .compose(composeToDb())
                .toList()
                .toObservable()
                .doOnNext(updatesPublisherList::onNext);
    }

    Observable<List<EventInfo>> addSingleObserve(List<EventInfo> items) {
        return Observable.fromIterable(items)
                .compose(composeToDb())
                .toList()
                .toObservable();
    }

    public Observable<List<EventInfo>> getAll() {
        return Observable.fromCallable(() -> isAllLoaded.get())
                .flatMap(isAllLoaded -> isAllLoaded ?
                        Observable.fromCallable(() -> allItems.values()).compose(composeSort())
                        : loadNewObservable)
                .compose(removeDeclined());
    }

    public Observable<EventInfo> getUpdates() {
        return updatesPublisher
                .filter(info -> info.getFreeTicketStatus() != TicketStatusConstant.DECLINED)
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<EventInfo>> getUpdateListEvents() {
        return updatesPublisherList.compose(removeDeclined());
    }

    public void forceLoadEvent(String id) {
        getFromServerById(id).subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "forceLoadEvent"));
    }

    public Observable<List<EventInfo>> forceLoadNewEvent() {
        return loadNewObservable;
    }

    public static ObservableTransformer<List<EventInfo>, List<EventInfo>> composeProfileUpcoming(ProfileData profile) {
        return upstream -> upstream
                .flatMapIterable(list -> list)
                .filter(eventInfo -> !profile.futureEventsList.contains(eventInfo.eventId))
                .filter(EventInfo::isBookAvailable)
                .compose(checkStatusEvent())
                .toList().toObservable();
    }

    public static ObservableTransformer<EventInfo, EventInfo> checkStatusEvent() {
        return upstream -> upstream
                .filter(eventInfo -> StatusEvent.ACTIVE.equals(eventInfo.status))
                .filter(e -> !e.hasStarted());
    }


    public static ObservableTransformer<Collection<EventInfo>, List<EventInfo>> composeSort() {
        return upstream -> upstream
                .flatMapIterable(list -> list)
                .toSortedList((o1, o2) -> o1.eventStartDate.compareTo(o2.eventStartDate)).toObservable()
                .flatMapIterable(list -> list)
                .toSortedList((o1, o2) -> o2.type.compareTo(o1.type))
                .toObservable();
    }

    public static ObservableTransformer<Collection<EventInfo>, List<EventInfo>> composeSortDateDescendant() {
        return upstream -> upstream
                .flatMapIterable(list -> list)
                .toSortedList((o1, o2) -> o2.eventStartDate.compareTo(o1.eventStartDate)).toObservable()
                .flatMapIterable(list -> list)
                .toSortedList((o1, o2) -> o2.type.compareTo(o1.type))
                .toObservable();
    }

    public Observable<EventInfo> getEventById(String id, TypeEvent typeEvent) {
        return getEventById(id).filter(info -> typeEvent == info.type);
    }

    public Observable<List<EventInfo>> getAll(TypeEvent typeEvent) {
        return getAll()
                .flatMapIterable(list -> list)
                .filter(info -> typeEvent == info.type)
                .toList().toObservable();
    }

    ObservableTransformer<Collection<EventInfo>, List<EventInfo>> removeDeclined() {
        return upstream -> upstream
                .flatMap(infos -> Observable.fromIterable(infos)
                        .filter(info -> info.getFreeTicketStatus() != TicketStatusConstant.DECLINED)
                        .toList().toObservable());
    }


    public static void shareEvent(Activity activity, String userId) {
        Intent shareIntent = ShareCompat.IntentBuilder.from(activity)
                .setType("text/plain")
                .setText(activity.getString(R.string.share_text, userId))
                .createChooserIntent();
        if (shareIntent.resolveActivity(activity.getPackageManager()) != null) {
            activity.startActivity(shareIntent);
        }
    }

    public static Observable<PaymentActionResult> chargeTicket(EventInfo event, LoadingView view) {
        return new ChargeTicket(event.eventId)
                .getDataTask()
                .doOnNext(result -> ApplicationLoader.getApplicationInstance().getEventManager().forceLoadEvent(event.eventId))
                .doOnNext(result -> ApplicationLoader.getApplicationInstance().getUserManager().loadCurrentUser())
                .compose(RxUtils.withLoading(view))
                ;
    }
}