package com.tangle.managers;

import com.tangle.ApplicationLoader;
import com.tangle.utils.RxUtils;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DataPollingManager {

    private static final long POLLING_PERIOD = 30;
    private static final long POLLING_DELAY = POLLING_PERIOD;
    private static final String TAG = DataPollingManager.class.getSimpleName();

    private final Scheduler scheduler = Schedulers.newThread();
    private Disposable pollingDisposable;

    public void startPolling() {
        pollingDisposable = Observable.interval(POLLING_DELAY, POLLING_PERIOD, TimeUnit.SECONDS, scheduler)
                .subscribe(interval -> pollData(), RxUtils.getEmptyErrorConsumer(DataPollingManager.class.getSimpleName()));
    }

    public void stopPolling() {
        RxUtils.safeDispose(pollingDisposable);
        pollingDisposable = null;
    }

    private void pollData() {
        ApplicationLoader.getApplicationInstance().getUserManager().loadCurrentUser();
        ApplicationLoader.getApplicationInstance().getLikeManager().forceLoadNew();
        ApplicationLoader.getApplicationInstance().getInviteManager().forceLoadNew(0);
        ApplicationLoader.getApplicationInstance().getEventManager().forceLoadNewEvent().subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "pollDataNewEven"));
        ApplicationLoader.getApplicationInstance().getBlogInfoManager().forceLoad().subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "pollDataNewBlogPosts"));
    }
}
