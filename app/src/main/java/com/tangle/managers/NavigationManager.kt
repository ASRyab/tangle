package com.tangle.managers

import android.content.Intent
import android.support.v4.app.FragmentManager
import com.tangle.base.ui.BaseActivity
import com.tangle.screen.activities.likes.LikesListFragment
import com.tangle.screen.main.ActivityHolder
import com.tangle.screen.main.MainActivity
import com.tangle.screen.other_profile.OtherProfileFragment
import com.tangle.screen.other_profile.ProfileEventPresenter
import com.tangle.screen.photoupload.PhotoRulesFragment
import com.tangle.screen.photoupload.PhotoUploadFragment

class NavigationManager {

    private lateinit var activity: BaseActivity

    fun init(activity: BaseActivity) {
        this.activity = activity
    }

    fun showMainScreen() {
        activity.startActivity(Intent(activity, MainActivity::class.java))
        activity.finish()
    }

    fun showUserProfile(userId: String) {
        showUserProfile(userId, false)
    }

    fun showUserProfile(userId: String, shouldSwitch: Boolean) {
        showUserProfile(userId, ProfileEventPresenter.GoingFrom.OTHER, null, shouldSwitch)
    }

    fun showUserProfile(userId: String, participants: ProfileEventPresenter.GoingFrom, eventId: String?) {
        showUserProfile(userId, participants, eventId,false)
    }

    fun showUserProfile(userId: String, participants: ProfileEventPresenter.GoingFrom, eventId: String?, shouldSwitch: Boolean) {
        val fragment = OtherProfileFragment.newInstance(userId, participants, eventId)
        if (shouldSwitch) {
            activity.switchFragment(fragment, true)
        } else {
            activity.addFragment(fragment, true)
        }
    }

    fun showPhotoUpload(holder: ActivityHolder) {
        holder.moveToScreen(PhotoUploadFragment.newInstance())
    }

    fun showPhotoUploadDecline(holder: ActivityHolder) {
        holder.moveToScreen(PhotoUploadFragment.newInstance(PhotoUploadFragment.State.DECLINE))
    }

    fun showPhotoUploadFrame(manager: FragmentManager, container: Int) {
        val transaction = manager.beginTransaction()
        transaction.replace(container, PhotoUploadFragment.newInstance(PhotoUploadFragment.State.FRAME))
        transaction.addToBackStack(null)
        transaction.commit()
    }

    fun showPhotoRulesFragment() {
        activity.addFragment(PhotoRulesFragment.getInstance(), true)
    }

    fun showLikes(isIncomingLikes: Boolean) {
        activity.addFragment(LikesListFragment.newInstance(isIncomingLikes), true)
    }
}
