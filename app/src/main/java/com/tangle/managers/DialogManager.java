package com.tangle.managers;

import android.support.v4.app.Fragment;

import com.tangle.R;
import com.tangle.base.ui.BaseActivity;
import com.tangle.base.ui.dialogs.FreeEventApprovedDialog;
import com.tangle.base.ui.dialogs.FreeEventWaitDialog;
import com.tangle.base.ui.dialogs.FreeTicketCountDialog;
import com.tangle.base.ui.dialogs.PhotoUploadDialog;
import com.tangle.base.ui.dialogs.ShareFreeDialog;
import com.tangle.base.ui.dialogs.StyledDialog;
import com.tangle.base.ui.dialogs.TicketsCountDialog;
import com.tangle.model.event.EventInfo;
import com.tangle.utils.own_user_photo.OwnPhotoPresenter;
import com.tangle.utils.own_user_photo.OwnPhotoView;

public class DialogManager {

    private final Fragment fragment;

    private DialogManager(Fragment fragment) {
        this.fragment = fragment;
    }

    public static DialogManager getInstance(Fragment fragment) {
        return new DialogManager(fragment);
    }

    public static void showWaitPhotoApprove(BaseActivity activity, StyledDialog.DialogButtonClickListener listener) {
        new StyledDialog.Builder(activity.getString(R.string.photo_checking_dialog))
                .setLeftActionBtnText(activity.getString(R.string.got_it))
                .setLeftActionBtnListener((v, dialog) -> {
                    if (listener != null) {
                        listener.onDialogButtonClick(v, dialog);
                    }
                })
                .setCancelable(listener == null)
                .setIconRes(R.drawable.ic_wait_photo_approve)
                .build().show(activity.getSupportFragmentManager(), "showWaitPhotoApprove");
    }

    public static void showPhotoApprove(BaseActivity activity) {
        new StyledDialog.Builder(activity.getString(R.string.photo_approve_dialog))
                .setLeftActionBtnText(activity.getString(R.string.got_it))
                .setIconRes(R.drawable.ic_approve_photo)
                .build().show(activity.getSupportFragmentManager(), "showPhotoApprove");
    }

    public static void showNoApprovePhotoToInvite(BaseActivity activity) {
        new StyledDialog.Builder(activity.getString(R.string.photo_no_approve_invite_dialog))
                .setLeftActionBtnText(activity.getString(R.string.got_it))
                .setIconRes(R.drawable.ic_user_circle)
                .build().show(activity.getSupportFragmentManager(), "showNoApprovePhotoToInvite");
    }

    public void showInviteOwnPurchase(StyledDialog.DialogButtonClickListener reserveListener, StyledDialog.DialogButtonClickListener purchaseListener) {
        new StyledDialog.Builder(fragment.getString(R.string.invite_own_purchase_dialog_title))
                .setBodyText(fragment.getString(R.string.invite_own_purchase_dialog_body))
                .setRightActionBtnListener(purchaseListener::onDialogButtonClick)
                .setRightActionBtnText(fragment.getString(R.string.invite_own_purchase_dialog_purchase))
                .setLeftActionBtnText(fragment.getString(R.string.invite_own_purchase_dialog_reserve))
                .setLeftActionBtnListener(reserveListener::onDialogButtonClick)
                .build().show(fragment.getActivity().getSupportFragmentManager(), "showInviteOwnPurchase");
    }

    public void showInviteOwnReserve(StyledDialog.DialogButtonClickListener confirmListener) {
        new StyledDialog.Builder(fragment.getString(R.string.invite_own_reserve_dialog_title))
                .setBodyText(fragment.getString(R.string.invite_own_reserve_dialog_body))
                .setRightActionBtnListener(confirmListener)
                .setRightActionBtnText(fragment.getString(R.string.invite_own_reserve_dialog_ok))
                .setLeftActionBtnText(fragment.getString(R.string.no_thanks))
                .build().show(fragment.getActivity().getSupportFragmentManager(), "showInviteOwnReserve");
    }

    public void showBookingOwnReserve(StyledDialog.DialogButtonClickListener confirmListener) {
        new StyledDialog.Builder(fragment.getString(R.string.invite_own_reserve_dialog_title))
                .setBodyText(fragment.getString(R.string.booking_own_reserve_dialog_body))
                .setRightActionBtnListener(confirmListener)
                .setRightActionBtnText(fragment.getString(R.string.invite_own_reserve_dialog_ok))
                .setLeftActionBtnText(fragment.getString(R.string.no_thanks))
                .build().show(fragment.getActivity().getSupportFragmentManager(), "showInviteOwnReserve");
    }

    public void showNonAnsweredInvite(StyledDialog.DialogButtonClickListener refuseListener
            , StyledDialog.DialogButtonClickListener invitesListener) {
        new StyledDialog.Builder(fragment.getString(R.string.non_answered_invite_dialog_title))
                .setBodyText(fragment.getString(R.string.non_answered_invite_dialog_body))
                .setRightActionBtnListener(refuseListener)
                .setRightActionBtnText(fragment.getString(R.string.non_answered_invite_dialog_refuse_all))
                .setLeftActionBtnText(fragment.getString(R.string.non_answered_invite_dialog_show_invites))
                .setLeftActionBtnListener(invitesListener)
                .build().show(fragment.getActivity().getSupportFragmentManager(), "showInviteOwnReserve");
    }

    public void showConfirmTicket(StyledDialog.DialogButtonClickListener confirmListener, StyledDialog.DialogButtonClickListener cancelListener) {
        new StyledDialog.Builder(fragment.getString(R.string.confirm_ticket_dialog_title))
                .setRightActionBtnListener(confirmListener)
                .setRightActionBtnText(fragment.getString(R.string.confirm))
                .setLeftActionBtnText(fragment.getString(R.string.no_thanks))
                .setLeftActionBtnListener((v, dialog) -> {
                    if (cancelListener != null) {
                        cancelListener.onDialogButtonClick(v, dialog);
                    }
                })
                .build().show(fragment.getActivity().getSupportFragmentManager(), "showConfirmPurchase");
    }

    public void showTicketsCountDialog(EventInfo event, TicketsCountDialog.CountTicketListener ticketListener) {
        TicketsCountDialog dialog = new TicketsCountDialog(ticketListener, event.maxBookTickets);
        dialog.show(fragment.getActivity().getSupportFragmentManager(), TicketsCountDialog.class.getSimpleName());
    }

    public void showFreeTicketsCountDialog(EventInfo event, TicketsCountDialog.CountTicketListener ticketListener) {
        FreeTicketCountDialog dialog = new FreeTicketCountDialog(ticketListener, event.maxFreeTickets);
        dialog.show(fragment.getActivity().getSupportFragmentManager(), TicketsCountDialog.class.getSimpleName());
    }

    public void showExpiredDialog() {
        new StyledDialog.Builder(fragment.getString(R.string.session_expired))
                .setBodyText(fragment.getString(R.string.session_expired_text))
                .setLeftActionBtnText(fragment.getString(R.string.ok))
                .build().show(fragment.getActivity().getSupportFragmentManager(), "showExpiredDialog");
    }

    public void showChangeEmailDialog(StyledDialog.DialogButtonClickListener listener) {
        new StyledDialog.Builder(fragment.getString(R.string.change_email_address))
                .setBodyText(fragment.getString(R.string.change_email_address_text))
                .setLeftActionBtnText(fragment.getString(R.string.ok))
                .setLeftActionBtnListener(listener)
                .build().show(fragment.getActivity().getSupportFragmentManager(), "showChangeEmailDialog");
    }

    public void showBlockUserDialog(StyledDialog.DialogButtonClickListener listener) {
        new StyledDialog.Builder(fragment.getString(R.string.block_user_dialog))
                .setLeftActionBtnText(fragment.getString(R.string.cancel))
                .setRightActionBtnText(fragment.getString(R.string.block))
                .setRightActionBtnListener(listener)
                .build().show(fragment.getActivity().getSupportFragmentManager(), "blockUserDialog");
    }

    public void showAcceptPopup(StyledDialog.DialogButtonClickListener listener) {
        new StyledDialog.Builder(fragment.getString(R.string.you_accepted_invite))
                .setBodyText(fragment.getString(R.string.accept_popup_sub_title))
                .setLeftActionBtnListener((listener))
                .setLeftActionBtnText(fragment.getString(R.string.ok))
                .build()
                .show(fragment.getActivity().getSupportFragmentManager(), "showAcceptPopup");
    }

    public void showRefusePopup(StyledDialog.DialogButtonClickListener listener) {
        new StyledDialog.Builder(fragment.getString(R.string.refuse_popup_title))
                .setBodyText(fragment.getString(R.string.refuse_popup_sub_title))
                .setRightActionBtnListener((listener))
                .setRightActionBtnText(fragment.getString(R.string.refuse))
                .setLeftActionBtnText(fragment.getString(R.string.cancel))

                .build()
                .show(fragment.getActivity().getSupportFragmentManager(), "showRefusePopup");
    }

    public void showAddToCalendarDialog(StyledDialog.DialogButtonClickListener listener) {
        new StyledDialog.Builder(fragment.getString(R.string.ask_for_add_to_calendar_text))
                .setLeftActionBtnText(fragment.getString(R.string.add))
                .setLeftActionBtnListener(listener)
                .build()
                .show(fragment.getActivity().getSupportFragmentManager(), "showAddToCalendarDialog");
    }

    public void showUnblockUserDialog(StyledDialog.DialogButtonClickListener listener) {
        new StyledDialog.Builder(fragment.getString(R.string.unblock_user_dialog))
                .setLeftActionBtnText(fragment.getString(R.string.cancel))
                .setRightActionBtnText(fragment.getString(R.string.unblock))
                .setRightActionBtnListener(listener)

                .build().show(fragment.getActivity().getSupportFragmentManager(), "unblockUserDialog");
    }

    public void showPhotoUploadDialog(OwnPhotoPresenter presenter) {
        if (fragment instanceof OwnPhotoView) {
            PhotoUploadDialog dialog = new PhotoUploadDialog(fragment, presenter);
            dialog.show(fragment.getActivity().getSupportFragmentManager(), PhotoUploadDialog.class.getSimpleName());
        }
    }

    public static void showApproveFreeTicket(BaseActivity activity, EventInfo eventInfo) {
        FreeEventApprovedDialog.newInstance(eventInfo).show(activity.getFragmentManager(), "ApproveFreeTicket");
    }


    public static void showDeclineFreeTicket(BaseActivity activity, EventInfo eventInfo) {
        new StyledDialog.Builder(activity.getString(R.string.free_event_decline_title, eventInfo.title))
                .setBodyText(activity.getString(R.string.free_event_decline_subtitle))
                .setLeftActionBtnText(activity.getString(R.string.ok))

                .build().show(activity.getSupportFragmentManager(), "DeclineFreeTicket");
    }

    public void showRemindLocation(StyledDialog.DialogButtonClickListener listener) {
        new StyledDialog.Builder(fragment.getString(R.string.notice))
                .setBodyText(fragment.getString(R.string.dialog_remind_book_tickets_in_uk))
                .setLeftActionBtnText(fragment.getString(R.string.got_it))
                .setLeftActionBtnListener(listener)
                .build().show(fragment.getActivity().getSupportFragmentManager(), "InviteRemindLocation");
    }

    public static void showWaitApproveFreeTicket(BaseActivity activity, EventInfo info) {
        FreeEventWaitDialog dialog = new FreeEventWaitDialog(info);
        dialog.show(activity.getSupportFragmentManager(), FreeEventWaitDialog.class.getSimpleName());
    }

    public void showShareFreeDialog() {
        ShareFreeDialog dialog = new ShareFreeDialog();
        dialog.show(fragment.getActivity().getSupportFragmentManager(), ShareFreeDialog.class.getSimpleName());
    }
}
