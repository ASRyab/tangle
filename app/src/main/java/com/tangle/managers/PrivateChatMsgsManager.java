package com.tangle.managers;

import android.text.TextUtils;

import com.tangle.ApplicationLoader;
import com.tangle.api.Api;
import com.tangle.api.rpc.rpc_actions.chat.MarkMailAsReadAction;
import com.tangle.api.rx_tasks.msg.HistoryType;
import com.tangle.db.messages.MessagesDataSource;
import com.tangle.model.chat.MailMessagePhoenix;
import com.tangle.model.pojos.PrivateChatRoom;
import com.tangle.screen.chats.private_chat.PrivateChatRoomsHistoryModel;
import com.tangle.utils.NoDuplicatesList;
import com.tangle.utils.RxUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.PublishSubject;

public class PrivateChatMsgsManager {
    private static final String TAG = PrivateChatMsgsManager.class.getSimpleName();

    private final PublishSubject<PrivateChatRoom> updatesPublisher = PublishSubject.create();
    private final MessagesDataSource dao;
    private Map<String, PrivateChatRoom> allItems = new HashMap<>();
    private Disposable messagesDisposable;
    private boolean isLoaded;

    public PrivateChatMsgsManager(MessagesDataSource dao) {
        this.dao = dao;
    }

    public Observable<List<MailMessagePhoenix>> getMsgsListByChatMateId(Integer offset, HistoryType historyType, String chatMateId) {
        return getFromRepositoryById(chatMateId)
                .flatMap(msgs -> {
                    if (msgs == null || msgs.isEmpty() || offset == msgs.size()) {
                        return getFromServerById(offset, historyType, chatMateId);
                    } else {
                        return Observable.just(msgs);
                    }
                })
                .flatMapIterable(phoenixes -> phoenixes)
                .filter(phoenix -> !phoenix.isSystemMsg())
                .toList().toObservable();
    }

    public void updateMsgsListByChatMateId(String chatMateId) {
        getFromServerById(0, HistoryType.UNDEFINED, chatMateId).subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer("updateMsgsListByChatMateId"));
    }

    private Observable<List<MailMessagePhoenix>> getFromServerById(Integer offset, HistoryType historyType, String chatMateId) {
        return new PrivateChatRoomsHistoryModel(offset, historyType, chatMateId).getTask()
                .map(response -> response.messages)
                .flatMap(messages -> addAllMessageObserve(messages, chatMateId))
                .flatMap(__ -> getFromRepositoryById(chatMateId));
    }

    public void setAllAsReadByChatMateId(String chatMateId) {
        getFromRepositoryById(chatMateId)
                .subscribeOn(Schedulers.io())
                .filter(msgs -> msgs != null && !msgs.isEmpty())
                .map(msgs -> {
                    for (MailMessagePhoenix msg : msgs) {
                        msg.read = true;
                    }
                    return msgs;
                })
                .doOnNext(dao::insertAll)
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(msgs -> updateAndPublishMsgRoomData(msgs, chatMateId))
                .flatMap(msgs -> Api.getInst().getSocketManager().executeRPCAction(new MarkMailAsReadAction(chatMateId)))
                .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "setAllAsReadByChatMateId"));
    }

    private void updateAndPublishMsgRoomData(List<MailMessagePhoenix> msgs, String chatMateId) {
        MailMessagePhoenix lastMsg = msgs.get(0);
        PrivateChatRoom privateChatRoom = allItems.get(chatMateId);
        if (privateChatRoom == null) {
            privateChatRoom = new PrivateChatRoom(lastMsg, msgs, chatMateId);
        } else {
            privateChatRoom.lastChatRoomMsg = lastMsg;
        }
        allItems.put(chatMateId, privateChatRoom);
        updatesPublisher.onNext(privateChatRoom);
    }

    private Observable<List<MailMessagePhoenix>> getFromRepositoryById(String chatMateId) {
        return Observable.just(allItems.get(chatMateId) == null ? new ArrayList<>() : allItems.get(chatMateId).roomMsg);
    }

    public void addMessage(MailMessagePhoenix item) {
        addMessageObserve(item).subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "addMessage"));
    }

    public Observable<MailMessagePhoenix> addMessageObserve(MailMessagePhoenix item) {
        return Observable.zip(Observable.just(item), ApplicationLoader.getApplicationInstance().getUserManager().currentUser(),
                (mailMessagePhoenix, profile) -> {
                    if (TextUtils.isEmpty(mailMessagePhoenix.to)) {
                        mailMessagePhoenix.to = profile.id;
                    }
                    return mailMessagePhoenix;
                })
                .observeOn(Schedulers.io())
                .doOnNext(dao::insert)
                .map(mailMessagePhoenix -> updatePrivateChats(new ArrayList<>(Collections.singletonList(mailMessagePhoenix)),
                        getChatMateId(mailMessagePhoenix),
                        mailMessagePhoenix))
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(updatesPublisher::onNext)
                .map(room -> room.roomMsg.get(0));
    }

    private String getChatMateId(MailMessagePhoenix message) {
        return ApplicationLoader.getApplicationInstance().getPreferenceManager().getCurrentUserId().equals(message.to)
                ? message.getSenderId() : message.to;
    }

    public Observable<List<MailMessagePhoenix>> addAllMessageObserve(List<MailMessagePhoenix> items, String chatMateId) {
        return Observable.just(items)
                .observeOn(Schedulers.io())
                .filter(phoenixes -> !phoenixes.isEmpty())
                .doOnNext(dao::insertAll)
                .map(mailMessagesPhoenix -> {
                    MailMessagePhoenix lastMsg = mailMessagesPhoenix.get(0);
                    return updatePrivateChats(mailMessagesPhoenix, chatMateId, lastMsg);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(updatesPublisher::onNext)
                .map(room -> room.roomMsg);
    }

    private PrivateChatRoom updatePrivateChats(List<MailMessagePhoenix> items, String chatMateId, MailMessagePhoenix lastMsg) {
        PrivateChatRoom privateChatRoom = allItems.get(chatMateId);
        if (privateChatRoom == null) {
            privateChatRoom = new PrivateChatRoom(lastMsg, items, chatMateId);
            ApplicationLoader.getApplicationInstance().getUserManager().updateUserInfo(chatMateId);
        } else {
            NoDuplicatesList<MailMessagePhoenix> messagePhoenixes = privateChatRoom.roomMsg;
            messagePhoenixes.addAll(items);
            Collections.sort(messagePhoenixes);
            privateChatRoom.lastChatRoomMsg = messagePhoenixes.get(0);
        }
        allItems.put(chatMateId, privateChatRoom);
        return privateChatRoom;
    }

    public Observable<PrivateChatRoom> getUpdateRooms() {
        return updatesPublisher
                .flatMap(chatRoom -> Observable.fromIterable(chatRoom.roomMsg)
                        .filter(phoenix -> !phoenix.isSystemMsg())
                        .toList()
                        .toObservable()
                        .map(list -> {
                            NoDuplicatesList<MailMessagePhoenix> phoenixes = new NoDuplicatesList<>();
                            phoenixes.addAll(list);
                            chatRoom.roomMsg = phoenixes;
                            return chatRoom;
                        }));
    }

    public void init() {
        if (allItems.isEmpty() && !isLoaded) {
            isLoaded = true;
            refreshDb();
        }
    }

    private void refreshDb() {
        Observable.fromCallable(dao::getAll).subscribeOn(Schedulers.io())
                .map(this::sortToRooms)
                .filter(lists -> !lists.isEmpty())
                .firstOrError()
                .flattenAsObservable(lists -> lists)
                .doOnNext(Collections::sort)
                .map(roomMsgs -> {
                    MailMessagePhoenix lastMsg = roomMsgs.get(0);
                    String chatMateId = getChatMateId(lastMsg);
                    return new PrivateChatRoom(lastMsg, roomMsgs, chatMateId);
                })
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(room -> allItems.put(room.chatId, room))
                .doOnNext(updatesPublisher::onNext)
                .toList()
                .subscribe(rooms -> {
                    if (isLoaded) {
                        updateAllRooms();
                    }
                    isLoaded = false;
                }, RxUtils.getEmptyErrorConsumer(TAG, "refreshDb"));
    }

    private void updateAllRooms() {
        Observable.fromIterable(allItems.keySet())
                .doOnNext(this::updateMsgsListByChatMateId)
                .subscribe();
    }

    private List<List<MailMessagePhoenix>> sortToRooms(Collection<MailMessagePhoenix> msgs) {
        HashMap<String, List<MailMessagePhoenix>> chatRooms = new HashMap<>();
        for (MailMessagePhoenix mailMessagePhoenix : msgs) {
            List<MailMessagePhoenix> mailMessagePhoenixes;
            String tag1 = mailMessagePhoenix.getSenderId() + mailMessagePhoenix.to;
            String tag2 = mailMessagePhoenix.to + mailMessagePhoenix.getSenderId();
            if (chatRooms.get(tag1) != null) {
                mailMessagePhoenixes = chatRooms.get(tag1);
                mailMessagePhoenixes.add(mailMessagePhoenix);
                chatRooms.put(tag1, mailMessagePhoenixes);
            } else if (chatRooms.get(tag2) != null) {
                mailMessagePhoenixes = chatRooms.get(tag2);
                mailMessagePhoenixes.add(mailMessagePhoenix);
                chatRooms.put(tag2, mailMessagePhoenixes);
            } else {
                mailMessagePhoenixes = new ArrayList<>();
                mailMessagePhoenixes.add(mailMessagePhoenix);
                chatRooms.put(tag1, mailMessagePhoenixes);
            }
        }
        return new ArrayList<>(chatRooms.values());
    }

    public void deleteAll() {
        RxUtils.safeDispose(messagesDisposable);
        messagesDisposable = null;
        dao.deleteAll();
        allItems.clear();
    }

    public Flowable<List<MailMessagePhoenix>> getUpdateDb() {
        return dao.getAllFlowable()
                .subscribeOn(Schedulers.io());
    }
}