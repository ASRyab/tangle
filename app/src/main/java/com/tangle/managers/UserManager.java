package com.tangle.managers;

import android.text.TextUtils;

import com.tangle.ApplicationLoader;
import com.tangle.api.Api;
import com.tangle.api.rpc.rpc_actions.GetUsersAction;
import com.tangle.api.rx_tasks.profile.OwnProfileModel;
import com.tangle.api.rx_tasks.profile.ProfileTask;
import com.tangle.api.utils.Config;
import com.tangle.base.repository.BaseQueueManager;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.RxUtils;
import com.tangle.utils.UserCheckUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

public class UserManager extends BaseQueueManager<ProfileData> {
    private static final String TAG = UserManager.class.getSimpleName();

    private final PublishSubject<ProfileData> updatesPublisher = PublishSubject.create();
    private final BehaviorSubject<ProfileData> ownProfileSubject = BehaviorSubject.createDefault(ProfileData.EMPTY_STATE);
    private final Observable<ProfileData> getOwnProfile;
    private String currentUserId;
    private boolean isUserTracked;

    public UserManager() {
        getOwnProfile = new OwnProfileModel().getTask().share();
    }

    public Observable<ProfileData> getUserById(String id) {
        if (Config.IS_ALL_GRAPH) {
            return getUserGraphById(id);
        } else {
            return getFromRepositoryById(id).switchIfEmpty(getFromServerById(id));
        }
    }

    public Observable<ProfileData> getUserGraphById(String id) {
        return getFromRepositoryFullById(id)
                .switchIfEmpty(getFromGraphServerById(id))
                .compose(checkBlockedUser());
    }

    private Observable<ProfileData> getFromServerById(String id) {
        return Api.getInst().getSocketManager().executeRPCAction(new GetUsersAction(id))
                .map(response -> response.getResult().getData())
                .flatMap(map -> addAllObserve(new ArrayList<>(map.values())))
                .flatMap(__ -> getFromRepositoryById(id));
    }

    private Observable<ProfileData> getFromGraphServerById(String id) {
        return new ProfileTask(id).getDataTask()
                .doOnNext(profileData -> profileData.isInited = true)
                .compose(composeToDb());
    }

    private Observable<ProfileData> getFromRepositoryById(String id) {
        ProfileData item = allItems.get(id);
        if (item == null) {
            return Observable.empty();
        } else {
            return Observable.just(item);
        }
    }

    private Observable<ProfileData> getFromRepositoryFullById(String id) {
        ProfileData item = allItems.get(id);
        if (item == null || !item.isInited) {
            return Observable.empty();
        } else {
            return Observable.just(item).doOnNext(profileData -> getFromGraphServerById(profileData.id)
                    .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(this.getClass().getSimpleName(), "update after")));
        }
    }

    public Observable<List<ProfileData>> getUserByIds(List<String> ids) {
        if (Config.IS_ALL_GRAPH) {
            return getUserGraphByIds(ids);
        }
        if (ids.isEmpty()) {
            return Observable.just(new ArrayList<>());
        }
        return getFromRepositoryByIds(ids)
                .flatMap(data -> {
                    if (data.size() != ids.size()) {
                        return getFromServerByIds(ids);
                    } else {
                        return Observable.just(data);
                    }
                });
    }

    public Observable<List<ProfileData>> getUserGraphByIds(List<String> ids) {
        if (ids.isEmpty()) {
            return Observable.just(new ArrayList<>());
        }
        return getFromRepositoryFullByIds(ids)
                .flatMap(data -> {
                    if (data.size() != ids.size()) {
                        return getFromGraphServerByIds(ids);
                    } else {
                        return Observable.just(data);
                    }
                }).compose(checkBlockedUsers());
    }

    private Observable<List<ProfileData>> getFromServerByIds(List<String> ids) {
        return Api.getInst().getSocketManager().executeRPCAction(new GetUsersAction(getAskIds(ids)))
                .map(response -> response.getResult().getData())
                .map(map -> new ArrayList<>(map.values()))
                .doOnNext(this::removeReceived)
                .flatMap(this::addAllObserve)
                .flatMap(__ -> getFromRepositoryByIds(ids));
    }

    private Observable<List<ProfileData>> getFromGraphServerByIds(List<String> ids) {
        List<String> askIds = getAskIds(ids);
        if (askIds.isEmpty()) {
            return Observable.empty();
        }
        return Observable.just(askIds)
                .flatMapIterable(strings -> strings)
                .flatMap(id -> getFromGraphServerById(id).doOnEach(notification -> removeReceivedById(id)))
                .toList().toObservable()
                .flatMap(s -> getFromRepositoryFullByIds(ids));
    }

    protected void removeReceived(List<ProfileData> data) {
        for (ProfileData profileData : data) {
            cashList.remove(profileData.id);
        }
    }

    protected void removeReceivedById(String data) {
        cashList.remove(data);
    }

    private Observable<List<ProfileData>> getFromRepositoryByIds(List<String> ids) {
        return Observable.fromCallable(() -> {
            List<ProfileData> dataList = new ArrayList<>();
            for (String id : ids) {
                if (allItems.containsKey(id)) {
                    dataList.add(allItems.get(id));
                }
            }
            return dataList;
        });
    }


    private Observable<List<ProfileData>> getFromRepositoryFullByIds(List<String> ids) {
        return Observable.fromIterable(allItems.values())
                .filter(profileData -> ids.contains(profileData.id) && profileData.isInited)
                .toList()
                .toObservable();
    }

    private ObservableTransformer<ProfileData, ProfileData> composeToDb() {
        return upstream -> upstream
                .filter(item -> !allItems.containsKey(item.id)
                        || !allItems.get(item.id).isInited
                        || item.isInited
                )
                .doOnNext(itemResult -> allItems.put(itemResult.id, itemResult))
                .doOnNext(updatesPublisher::onNext);

    }

    ObservableTransformer<List<ProfileData>, List<ProfileData>> updateLikesInfo(boolean isLikedMe) {
        return upstream -> upstream
                .switchMap(profileData -> Observable.fromIterable(profileData)
                        .filter(profile -> allItems.containsKey(profile.id))
                        .map(profile -> allItems.get(profile.id))
                        .doOnNext(profile -> {
                            if (isLikedMe) {
                                profile.isLikedMeUser = true;
                            } else {
                                profile.isYouLikedUser = true;
                            }
                        })
                        .compose(composeToDb())
                        .toList()
                        .toObservable()
                        .map(profileData1 -> profileData));

    }

    public Observable<ProfileData> addObserve(ProfileData item1) {
        return Observable.just(item1)
                .compose(composeToDb());
    }

    public Observable<List<ProfileData>> addAllObserve(List<ProfileData> items) {
        return Observable.fromIterable(items)
                .compose(composeToDb())
                .toList().toObservable();
    }

    public Observable<List<ProfileData>> getAll() {
        return allItems.isEmpty() ? Observable.empty() : Observable.just(allItems).flatMapIterable(Map::values).toList().toObservable();
    }

    public Observable<ProfileData> getUpdates() {
        return updatesPublisher;
    }

    public void addAll(List<ProfileData> items) {
        addAllObserve(items).subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "addAll"));
    }

    public void setCurrentUser(ProfileData currentUser) {
        if (!isUserTracked) {
            isUserTracked = true;
            ApplicationLoader.getApplicationInstance().getAnalyticManager().initUser(currentUser);
        }
        ownProfileSubject.onNext(currentUser);
    }

    public Observable<ProfileData> currentUser() {
        return currentUserWithUpdates().firstOrError().toObservable();
    }

    public Observable<Boolean> checkUserInWhiteList() {
        return currentUser()
                .map(res -> res.isTargetCountry)
                .map(UserCheckUtils::isCountryInWhiteList);
    }

    public Observable<ProfileData> currentUserWithUpdates() {
        return ownProfileSubject.filter(profileData -> {
            if (ProfileData.EMPTY_STATE.equals(profileData)) {
                loadCurrentUser();
                return false;
            }
            return true;
        });
    }

    public void loadCurrentUser() {
        getOwnProfile.subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "loadCurrentUser"));
    }

    public void updateUserInfo(String id) {
        getFromGraphServerById(id).subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "updateUserInfo:" + id));
    }

    public String getCurrentUserId() {
        return currentUserId != null ? currentUserId
                : (currentUserId = ApplicationLoader.getApplicationInstance().getPreferenceManager().getCurrentUserId());
    }

    public boolean isCurrentUser(String id) {
        return !TextUtils.isEmpty(id) && id.equals(getCurrentUserId());
    }

    @Override
    protected boolean containsInCache(String id) {
        return super.containsInCache(id) && (!Config.IS_ALL_GRAPH || allItems.get(id).isInited);
    }

    private static ObservableTransformer<List<ProfileData>, List<ProfileData>> checkBlockedUsers() {
        return upstream -> upstream
                .flatMap(list -> Observable.fromIterable(list)
                        .compose(checkBlockedUser())
                        .toList().toObservable());
    }

    private static ObservableTransformer<ProfileData, ProfileData> checkBlockedUser() {
        return upstream -> upstream
                .filter(data -> !data.isBlocked());
    }

    @Override
    public void deleteAll() {
        super.deleteAll();
        ownProfileSubject.onNext(ProfileData.EMPTY_STATE);
    }
}