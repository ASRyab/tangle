package com.tangle.managers

import android.annotation.SuppressLint
import com.tangle.api.rx_tasks.gallery.GetBrandBlogList
import com.tangle.db.ListItemsCache
import com.tangle.db.blog.BlogDao
import com.tangle.db.blog.BlogReadInfo
import com.tangle.model.blog.BlogEntity
import com.tangle.utils.RxUtils
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.observables.ConnectableObservable
import io.reactivex.schedulers.Schedulers

class BlogInfoManager(var dao: BlogDao) {

    private val blogListCache: ListItemsCache<BlogEntity> = ListItemsCache { post -> post.postId }

    private var dbAutoConnect: ConnectableObservable<List<BlogReadInfo>> = dao.allFlowable
            .toObservable()
            .subscribeOn(Schedulers.io())
            .replay(1)

    private var memCacheDisposable: Disposable? = null

    private var mainObservable: Observable<List<BlogEntity>> = GetBrandBlogList().dataTask
            .doOnNext { blogListCache.insertItems(it) }
            .compose(composeRemoveUnnecessaryDbValues(blogListCache, dao))
            .compose(composeSyncWithDatabase(dao))

    fun init() {
        if (memCacheDisposable == null) {
            memCacheDisposable = dbAutoConnect.connect()
        }

    }

    @SuppressLint("CheckResult")
    fun forceLoad(): Observable<List<BlogEntity>> {
        return mainObservable
    }

    private fun composeSyncWithDatabase(dao: BlogDao): ObservableTransformer<List<BlogEntity>, List<BlogEntity>> {
        return ObservableTransformer { items ->
            items.observeOn(Schedulers.io())
                    .flatMapIterable { list -> list }
                    .doOnNext {
                        dao.insert(BlogReadInfo(it.postId, false))
                    }
                    .toList()
                    .toObservable()
        }
    }

    private fun composeRemoveUnnecessaryDbValues(cache: ListItemsCache<BlogEntity>, dao: BlogDao): ObservableTransformer<List<BlogEntity>, List<BlogEntity>> {
        return ObservableTransformer { items ->
            items.observeOn(Schedulers.io())
                    .map { list ->
                        val listInfo = dao.all
                        for (dbVal in listInfo) {
                            if (!cache.contains(dbVal.postId)) {
                                dao.deleteById(dbVal.postId)
                            }
                        }
                        list
                    }
        }
    }

    fun getItemsWithUpdates(): Observable<List<BlogEntity>> {
        return blogListCache.items.toObservable().flatMap { list ->
            Observable.fromIterable(list)
                    .toSortedList()
                    .toObservable()
        }
    }

    fun getUnreadCount(): Observable<Int> {
        return dbAutoConnect
                .flatMap {
                    Observable
                            .fromIterable(it)
                            .filter { res -> !res.isRead }
                            .toList()
                            .toObservable()
                }
                .map { list ->
                    list.size
                }
    }

    fun setAllAsRead(): Disposable {
        return dbAutoConnect.observeOn(Schedulers.io())
                .firstElement()
                .toObservable()
                .flatMapIterable { data -> data }
                .doOnNext { data -> data.isRead = true }
                .toList()
                .subscribe(Consumer<List<BlogReadInfo>> { dao.insertAll(it) }, RxUtils.getEmptyErrorConsumer())
    }

    fun deleteAll() {
        RxUtils.safeDispose(memCacheDisposable)
        memCacheDisposable = null
        blogListCache.clear()
        dao.deleteAll()
    }
}