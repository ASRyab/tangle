package com.tangle.managers

import android.annotation.SuppressLint
import com.google.android.gms.common.util.IOUtils
import com.tangle.ApplicationLoader
import com.tangle.api.Api
import com.tangle.utils.Debug
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.IOException
import java.io.InputStream
import java.net.URL


class MediaManager {

    companion object {
        val TAG: String = MediaManager::class.java.simpleName
    }

    val cacheProxy = ApplicationLoader.getApplicationInstance().cacheProxy
    var galleryPromoUrl: String? = ""

    @SuppressLint("CheckResult")
    fun loadMediaItems() {
        Api.getInst().media().getMediaUrls()
                .subscribeOn(Schedulers.newThread())
                .map {
                    galleryPromoUrl = it.data.media.tangle_gallery_promo[0].originalUrl //"https://cdn.tngcdn.com/api/v1/media/showVideo/mediaId/3e108e8b02aec109529c8f573c38ac1e/hash/eyJlbnRpdHlJZCI6ImZiNzliYTI3NDljZDRmNzE5ZDE5MDg5ZGMzNTg4ZDdjIiwiZW50aXR5VHlwZSI6OSwic2l6ZVR5cGUiOiJvcmlnaW4ifQ"
                    cacheProxy.registerCacheListener({ _, url, percentsAvailable -> Debug.logD(TAG, percentsAvailable.toString() + " available for url " + url) }, galleryPromoUrl)
                    var input: InputStream? = null
                    try {
                        val proxyUrl = cacheProxy.getProxyUrl(galleryPromoUrl)
                        Debug.logD(TAG, "fileUrl: $proxyUrl")
                        val url = URL(proxyUrl)
                        input = url.openStream()
                        val byteChunk = IOUtils.toByteArray(input!!)
                    } catch (e: Exception) {
                        Debug.logD(TAG, "Failed while reading bytes from: " + galleryPromoUrl + "  :" + e.message)
                        false
                    } finally {
                        if (input != null) {
                            try {
                                input.close()
                            } catch (e: IOException) {
                                return@map false
                            }
                        }
                        return@map true
                    }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        {
                            Debug.logD(TAG, "downloaded: " + it.toString())
                        },
                        {
                            Debug.logD(TAG, "error:" + it.message)
                        })

    }

    fun updateNetworkState(connected: Boolean) {
        if (connected) loadMediaItems()
    }

    fun hasCachedVideo(): Boolean {
        return cacheProxy.isCached(galleryPromoUrl)
    }

}

