package com.tangle.managers;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.PixelFormat;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.rpc.rpc_response.invite.InviteData;
import com.tangle.model.chat.MailMessagePhoenix;
import com.tangle.model.pojos.InnAppNotification;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.activities.main.ActivitiesFragment;
import com.tangle.screen.chats.private_chat.PrivateChatFragment;
import com.tangle.screen.main.ActivityHolder;
import com.tangle.screen.main.OnSwipeNotificationListener;
import com.tangle.screen.main.ScreenJump;
import com.tangle.utils.ConvertingUtils;
import com.tangle.utils.RxUtils;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;
import static android.content.Context.WINDOW_SERVICE;

public class NotificationViewManager {
    private final static String TAG = NotificationViewManager.class.getSimpleName();
    private static final long HIDE_DURATION = 500;
    private Context context;
    private ArrayList<View> viewList = new ArrayList<View>();
    private ActivityHolder activityHolder;
    private String chatId;

    @SuppressLint("CheckResult")
    public NotificationViewManager() {
        ApplicationLoader.getApplicationInstance().getLikeManager().getItemsWithUpdates()
                .filter(profileData -> activityHolder != null)
                .flatMapIterable(list -> list)
                .filter(profileData -> profileData.isNew)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(profileData -> showNotification(transformLike(profileData))
                        , RxUtils.getEmptyErrorConsumer(TAG, "getLikeManager"));

        ApplicationLoader.getApplicationInstance().getInviteManager().getAllWithUpdate()
                .filter(profileData -> activityHolder != null)
                .flatMapSingle(list -> Observable.fromIterable(list)
                        .filter(data -> !data.isRead)
                        .filter(data -> !(ApplicationLoader.getApplicationInstance().getUserManager().isCurrentUser(data.from) && data.status == 1))
                        .toList())
                .flatMap(invites -> Observable.fromIterable(invites)
                        .map(data -> ApplicationLoader.getApplicationInstance().getUserManager().isCurrentUser(data.from) ? data.to : data.from)
                        .toList().toObservable()
                        .flatMap(strings -> ApplicationLoader.getApplicationInstance().getUserManager().getUserByIds(strings)
                                .filter(profiles -> !profiles.isEmpty()))
                        .map(data -> Pair.create(invites, data)))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pair -> showNotification(transformInvite(pair, InnAppNotification.Kind.INVITE))
                        , RxUtils.getEmptyErrorConsumer(TAG, "getInviteManager"));

        ApplicationLoader.getApplicationInstance().getPrivateChatMsgsManager().getUpdateDb()
                .filter(profileData -> activityHolder != null)
                .flatMapSingle(phoenixes -> Observable.fromIterable(phoenixes)
                        .filter(phoenix -> !phoenix.isMyMsg())
                        .filter(phoenix -> !phoenix.isSystemMsg())
                        .filter(phoenix -> !phoenix.from.id.equals(chatId))
                        .filter(phoenix -> !phoenix.read)
                        .toSortedList())
                .flatMap(phoenixes -> Observable.fromIterable(phoenixes).map(phoenix -> phoenix.from.id).toList()
                        .flatMapObservable(ids -> ApplicationLoader.getApplicationInstance().getUserManager().getUserByIds(ids))
                        .map(profiles -> Pair.create(phoenixes, profiles))
                        .toFlowable(BackpressureStrategy.BUFFER)
                )
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pair -> showNotification(transformMessage(pair)), RxUtils.getEmptyErrorConsumer(TAG, "getPrivateChatMsgsManager"));
    }

    private InnAppNotification transformMessage(Pair<List<MailMessagePhoenix>, List<ProfileData>> pair) {
        InnAppNotification innAppNotification = new InnAppNotification();
        List<ProfileData> dataList = pair.second;
        boolean isOne = dataList.size() == 1;
        InnAppNotification.Kind kind = innAppNotification.kind = isOne ? InnAppNotification.Kind.MESSAGE : InnAppNotification.Kind.MESSAGES;
        String login = dataList.get(0).login;
        if (isOne) {
            innAppNotification.title = login;
        } else {
            innAppNotification.title = context.getString(R.string.notification_with_other, login, dataList.size() - 1);
        }
        innAppNotification.profiles = dataList;
        innAppNotification.message = isOne ? ConvertingUtils.getLastMessageText(context.getResources(), dataList.get(0), pair.first.get(0))
                : context.getString(kind.getMessage());
        return innAppNotification;
    }

    private InnAppNotification transformLike(ProfileData profileData) {
        InnAppNotification innAppNotification = new InnAppNotification();
        InnAppNotification.Kind kind = profileData.isMatchedUser ? InnAppNotification.Kind.MUTUAL_LIKE : InnAppNotification.Kind.WHO_LIKED;
        innAppNotification.kind = kind;
        innAppNotification.title = profileData.login;
        innAppNotification.profiles = Collections.singletonList(profileData);
        innAppNotification.message = context.getString(kind.getMessage());
        return innAppNotification;
    }

    private InnAppNotification transformInvite(Pair<List<InviteData>, List<ProfileData>> pair, InnAppNotification.Kind kind) {
        InnAppNotification innAppNotification = new InnAppNotification();
        innAppNotification.kind = kind;
        List<InviteData> inviteData = pair.first;
        List<ProfileData> newInvites = pair.second;
        String firstSenderName = newInvites.get(0).login;
        if (newInvites.size() == 1) {
            innAppNotification.title = firstSenderName;
        } else {
            innAppNotification.title = context.getString(R.string.notification_with_other, firstSenderName, newInvites.size() - 1);
        }
        innAppNotification.profiles = newInvites;
        innAppNotification.message = getInviteString(inviteData);
        return innAppNotification;
    }

    private String getInviteString(List<InviteData> data) {
        String result;
        if (data.size() == 1) {
            InviteData inviteData = data.get(0);
            switch (inviteData.status) {
                case InviteData.REJECT_INVITE_INDEX:
                    result = context.getString(R.string.invite_notification_reject);
                    break;
                case InviteData.ACCEPT_INVITE_INDEX:
                    result = context.getString(R.string.invite_notification_accept);
                    break;
                default:
                    result = context.getString(R.string.notification_send_invitation);
                    break;
            }
        } else {
            result = context.getString(R.string.invits_notification);
        }
        return result;
    }

    private void showNotification(InnAppNotification innAppNotification) {
        if (activityHolder == null) {
            return;
        }
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_APPLICATION_SUB_PANEL,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE
                        | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
                        | WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH
                        | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
                PixelFormat.TRANSLUCENT
        );

        params.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;
        WindowManager wm = (WindowManager) context.getSystemService(WINDOW_SERVICE);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        View notificationView = inflater.inflate(R.layout.notification_view, null);

        ImageView userAvatar = notificationView.findViewById(R.id.iv_user_ava);
        ImageView secondUserAvatar = notificationView.findViewById(R.id.iv_second_user_ava);
        View avatarsSeparator = notificationView.findViewById(R.id.avatars_separator);
        TextView message = notificationView.findViewById(R.id.tv_message);
        TextView title = notificationView.findViewById(R.id.tv_title);
        title.setText(innAppNotification.title);
        message.setText(innAppNotification.message);
        List<ProfileData> profiles = innAppNotification.profiles;
        if (profiles != null && !profiles.isEmpty()) {
            ProfileData profile = profiles.get(0);
            loadAvatar(profile, userAvatar);
        }

        if (profiles != null && profiles.size() > 1) {
            ProfileData secondProfile = profiles.get(1);
            loadAvatar(secondProfile, secondUserAvatar);
            secondUserAvatar.setVisibility(View.VISIBLE);
            avatarsSeparator.setVisibility(View.VISIBLE);
        } else {
            secondUserAvatar.setVisibility(View.GONE);
            avatarsSeparator.setVisibility(View.GONE);
        }

        notificationView.setOnTouchListener(new OnSwipeNotificationListener(context) {
            @Override
            public void onSwipeTop() {
                super.onSwipeTop();
                hideNotification(notificationView, wm);
            }
        });
        notificationView.findViewById(R.id.view).setOnClickListener(v -> {
            onClick(innAppNotification, innAppNotification.kind);
            removeView(wm, notificationView);
        });
        if (wm != null) {
            wm.addView(notificationView, params);
            viewList.add(notificationView);
        }
        Observable.just(notificationView).delay(3, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                .filter(myView -> viewList.contains(myView))
                .subscribe(view -> hideNotification(view, wm));
    }

    private void loadAvatar(ProfileData profile, ImageView userAvatar) {
        GlideApp.with(context)
                .load(profile.primaryPhoto.avatar)
                .placeholder(RequestOptionsFactory.getNonAvatar(profile))
                .apply(RequestOptions.circleCropTransform())
                .into(userAvatar);
    }

    private void onClick(InnAppNotification innAppNotification, InnAppNotification.Kind kind) {
        switch (kind) {
            case MESSAGES:
            case MUTUAL_LIKE:
                activityHolder.moveToScreen(new ActivitiesFragment(), ScreenJump.MUTUAL_LIKE);
                break;
            case INVITE:
                activityHolder.moveToScreen(new ActivitiesFragment(), ScreenJump.INVITE);
                break;
            case MESSAGE:
                activityHolder.moveToScreen(PrivateChatFragment.newInstance(innAppNotification.firstProfile().id));
                break;
            case WHO_LIKED:
                activityHolder.moveToScreen(new ActivitiesFragment(), ScreenJump.WHO_LIKED);
                break;
        }
    }

    private void hideNotification(View notificationView, WindowManager wm) {
        notificationView.animate().translationY(-notificationView.getHeight())
                .setDuration(HIDE_DURATION)
                .setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        if (wm != null && viewList.contains(notificationView) && notificationView.isAttachedToWindow()) {
                            removeView(wm, notificationView);
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }
                })
                .start();
    }

    private void removeView(WindowManager wm, View notificationView) {
        wm.removeViewImmediate(notificationView);
        viewList.remove(notificationView);
    }


    public void initOnActivity(Context context) {
        this.context = context;
    }

    public void setActivityHolder(ActivityHolder activityHolder) {
        this.activityHolder = activityHolder;
    }

    public void setChatId(String chatId) {
        this.chatId = chatId;
    }

    public String getChatId() {
        return chatId;
    }
}
