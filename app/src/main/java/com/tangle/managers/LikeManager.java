package com.tangle.managers;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.util.Pair;

import com.tangle.ApplicationLoader;
import com.tangle.api.Api;
import com.tangle.api.rx_tasks.activities.liked_other.LikedOther;
import com.tangle.api.rx_tasks.activities.liked_you.LikedYou;
import com.tangle.db.ListItemsCache;
import com.tangle.db.likes.LikeDao;
import com.tangle.db.likes.LikeUser;
import com.tangle.model.activities.liked_you.LikedYouResponse;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.ConvertingUtils;
import com.tangle.utils.RxUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Single;
import io.reactivex.disposables.Disposable;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.schedulers.Schedulers;

import static com.tangle.utils.ConvertingUtils.DATE_FORMAT_YMD_HMS_T;

@SuppressLint("CheckResult")
public class LikeManager {
    private static final String TAG = LikeManager.class.getSimpleName();
    private static final int LIMIT = 1000;//TODO fix it

    private final LikeDao dbLikedMe;
    private final ListItemsCache<ProfileData> profileCache;

    private ConnectableObservable<Map<String, LikeUser>> likeUsersData;
    private Disposable likeUsersDataDisposable;

    public LikeManager(LikeDao dao) {
        this.dbLikedMe = dao;
        this.profileCache = new ListItemsCache<>(profileData -> profileData.id);
        this.likeUsersData = dbLikedMe.getAllFlowable().toObservable()
                .flatMapSingle(likeUsers -> Observable.fromIterable(likeUsers).toMap(LikeUser::getSenderId))
                .replay(1);

        ApplicationLoader.getApplicationInstance().getUserManager().getUpdates()
                .filter(this::shouldFilterProfile)
                .flatMap(data -> likeUsersData.firstElement()
                        .toObservable()
                        .map(map -> map.get(data.id))
                        .map(user -> updateProfile(data, user)))
                .subscribe(data -> profileCache.insertItems(Collections.singletonList(data)), RxUtils.getEmptyErrorConsumer(TAG, "LikeManager.getUserManager().getUpdates()"));
    }

    private boolean shouldFilterProfile(ProfileData newProfile) {
        return profileCache.contains(newProfile.id) && !profileCache.getByKey(newProfile.id).lastUserActivity.equals(newProfile.lastUserActivity) && profileCache.getByKey(newProfile.id).isBlocked() != newProfile.isBlocked();
    }

    public void init() {
        if (likeUsersDataDisposable == null) {
            likeUsersDataDisposable = likeUsersData.connect();
            likeUsersData
                    .concatMap(this::getProfilesFor)
                    .filter(items -> !items.isEmpty())
                    .doOnNext(profileCache::insertItems)
                    .subscribeOn(Schedulers.io())
                    .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "iniFullWhoLikedMe"));
            Api.getInst().getSocketManager().subscribeOn(this::forceLoadNew);
        }
    }

    public static boolean updateCache(LikeUser likeUser, ListItemsCache<ProfileData> pd) {
        ProfileData data = pd.getByKey(likeUser.getSenderId());
        return data == null
                || data.isMatchedUser != likeUser.isMatch()
                || !data.lastUserActivity.equals(likeUser.getLastUserActivity());
    }

    private Observable<List<ProfileData>> getProfilesFor(Map<String, LikeUser> likeUsers) {
        return Observable.fromIterable(likeUsers.values())
                .filter(value -> updateCache(value, profileCache))
                .map(LikeUser::getSenderId)
                .toList()
                .filter(items -> !items.isEmpty())
                .flatMapObservable(ids -> ApplicationLoader.getApplicationInstance().getUserManager().getUserByIds(ids)
                        .flatMapSingle(profiles -> updateProfiles(profiles, likeUsers))
                );
    }

    public void setAllAsRead() {
        likeUsersData.firstElement()
                .toObservable()
                .flatMapIterable(Map::values)
                .compose(updateRead())
                .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "setAllAsRead"));
    }

    public void setMatchesAsRead() {
        likeUsersData.firstElement()
                .toObservable()
                .flatMapIterable(Map::values)
                .filter(LikeUser::isMatch)
                .compose(updateRead())
                .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "setMatchesAsRead"));
    }

    private ObservableTransformer<LikeUser, List<ProfileData>> updateRead() {
        return upstream -> upstream
                .filter(user -> !user.isRead())
                .doOnNext(user -> user.setRead(true))
                .toList().toObservable()
                .doOnNext(dbLikedMe::insertAll)
                .flatMapIterable(list -> list)
                .flatMap(user -> ApplicationLoader.getApplicationInstance().getUserManager().getUserById(user.getSenderId())
                        .map(profileData -> updateProfile(profileData, user)))
                .doOnNext(profileData -> profileCache.insertItems(Collections.singletonList(profileData)))
                .toList().toObservable().subscribeOn(Schedulers.io());
    }

    public void setAsRead(String id) {
        likeUsersData.firstOrError().map(users -> users.get(id))
                .filter(user -> !user.isRead())
                .doOnSuccess(user -> user.setRead(true))
                .doOnSuccess(dbLikedMe::insert)
                .flatMapObservable(user -> ApplicationLoader.getApplicationInstance().getUserManager().getUserById(user.getSenderId())
                        .map(profileData -> updateProfile(profileData, user))
                )
                .doOnNext(profileData -> profileCache.insertItems(Collections.singletonList(profileData)))
                .subscribeOn(Schedulers.io())
                .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "setAsRead"));
    }


    public void forceLoadNew() {
        forceLoadNewOther(0);
        forceLoadNewMy(0);
    }

    public void forceLoadNewOther(int offset) {
        getNewFromServer(offset, false).subscribe(items -> {
            if (!items.isEmpty()) {
                forceLoadNewOther(offset + items.size());
            }
        }, RxUtils.getEmptyErrorConsumer(TAG, "forceLoadNewOther"));
    }

    public void forceLoadNewMy(int offset) {
        getNewFromServer(offset, true).subscribe(items -> {
            if (!items.isEmpty()) {
                forceLoadNewMy(offset + items.size());
            }
        }, RxUtils.getEmptyErrorConsumer(TAG, "forceLoadNewMy"));
    }

    private Observable<List<LikeUser>> getNewFromServer(int actualOffset, boolean isLikedMe) {
        Observable<LikedYouResponse> likedObservable = isLikedMe ? new LikedYou(LIMIT, actualOffset).getDataTask() : new LikedOther(LIMIT, actualOffset).getDataTask();
        return likedObservable
                .map(LikedYouResponse::getLikedYouUsers)
                .compose(ApplicationLoader.getApplicationInstance().getUserManager().updateLikesInfo(isLikedMe))
                .observeOn(Schedulers.io())
                .flatMap(it -> saveNewItems(it, isLikedMe))
                .zipWith(likeUsersData.firstElement().map(Map::values).toObservable(), Pair::create)
                .filter(pair -> !pair.second.containsAll(pair.first))
                .map(pair -> pair.first)
                .firstElement().toObservable();
    }

    private Single<List<ProfileData>> updateProfiles(List<ProfileData> profiles, Map<String, LikeUser> likeUsers) {
        return Observable.fromIterable(profiles).map(profileData -> updateProfile(profileData, likeUsers.get(profileData.id)))
                .toList();
    }

    private ProfileData updateProfile(ProfileData profileData, LikeUser user) {
        profileData.isMatchedUser = user.isMatch();
        profileData.isNew = !user.isRead();
        profileData.lastUserActivity = user.getLastUserActivity();
        return profileData;
    }

    private ProfileData updateProfileByLike(ProfileData profileData) {
        profileData.isYouLikedUser = true;
        profileData.lastUserActivity = ConvertingUtils.getStringDateByPattern(new Date(), DATE_FORMAT_YMD_HMS_T);
        return profileData;
    }

    private Single<List<LikeUser>> zipLikeUsers(List<ProfileData> profiles, Map<String, LikeUser> likedUsers, boolean isLikedMe) {
        return Observable.fromIterable(profiles)
                .map(this::transformToDb)
                .map(user -> initIsReadNewLike(likedUsers, user, isLikedMe))
                .map(user -> initActivityTime(likedUsers, user))
                .map(likeUser -> {
                    if (likedUsers.containsKey(likeUser.getSenderId()) && likedUsers.get(likeUser.getSenderId()).isMatch()) {
                        likeUser.setMatch(true);
                    }
                    return likeUser;
                })
                .toList();
    }

    @NonNull
    private LikeUser initIsReadNewLike(Map<String, LikeUser> likedUsers, LikeUser user, boolean isLikedMe) {
        if (!isLikedMe) user.setRead(true);
        else if (likedUsers.containsKey(user.getSenderId()) && !likedUsers.get(user.getSenderId()).isMatch() && user.isMatch()) {
            user.setRead(false); //it works only when user use more devices
        } else if (likedUsers.containsKey(user.getSenderId())) {
            user.setRead(likedUsers.get(user.getSenderId()).isRead());
        }
        return user;
    }

    private LikeUser initActivityTime(Map<String, LikeUser> likedUsers, LikeUser user) {
        if (likedUsers.containsKey(user.getSenderId())) {
            Date dbDate = ConvertingUtils.getDateFromString(likedUsers.get(user.getSenderId()).getLastUserActivity(), ConvertingUtils.DATE_FORMAT_YMD_HMS_T);
            Date newDate = ConvertingUtils.getDateFromString(user.getLastUserActivity(), ConvertingUtils.DATE_FORMAT_YMD_HMS_T);
            if (dbDate != null && newDate != null) {
                Date resultDate = newDate.after(dbDate) ? newDate : dbDate;
                user.setLastUserActivity(ConvertingUtils.getStringDateByPattern(resultDate, ConvertingUtils.DATE_FORMAT_YMD_HMS_T));
            }
        }
        return user;
    }

    private Observable<List<LikeUser>> saveNewItems(List<ProfileData> data, boolean isLikedMe) {
        return Single.zip(Single.just(data), likeUsersData.firstOrError(), Pair::create)
                .flatMap(pair -> zipLikeUsers(pair.first, pair.second, isLikedMe))
                .doOnSuccess(dbLikedMe::insertAll)
                .toObservable();
    }

    private LikeUser transformToDb(ProfileData profileData) {
        return new LikeUser(profileData.id, profileData.lastUserActivity, profileData.isMatchedUser);
    }

    public void setMatched(String userId) {
        likeUsersData.firstElement().filter(map -> map.containsKey(userId)).map(map -> map.get(userId))
                .doOnSuccess(user -> user.setMatch(true))
                .doOnSuccess(user -> user.setRead(true))
                .doOnSuccess(dbLikedMe::insert)
                .flatMapObservable(user -> ApplicationLoader.getApplicationInstance().getUserManager().getUserById(user.getSenderId())
                        .map(profileData -> updateProfile(profileData, user))
                )
                .doOnNext(profileData -> profileCache.insertItems(Collections.singletonList(profileData)))
                .subscribeOn(Schedulers.io())
                .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "setMatched"));
    }

    public void setLiked(String userId) {
        likeUsersData.firstElement()
                .filter(map -> map.containsKey(userId))
                .switchIfEmpty(ApplicationLoader.getApplicationInstance()
                        .getUserManager()
                        .getUserById(userId)
                        .map(this::updateProfileByLike)
                        .flatMap(it -> {
                            List<ProfileData> list = new ArrayList<>();
                            list.add(it);
                            return saveNewItems(list, false);
                        })
                        .singleOrError()
                        .flatMap(res -> likeUsersData.firstOrError()))
                .map(map -> map.get(userId))
                .flatMapObservable(user -> ApplicationLoader.getApplicationInstance().getUserManager().getUserById(user.getSenderId())
                        .map(this::updateProfileByLike))
                .doOnNext(profileData -> profileCache.insertItems(Collections.singletonList(profileData)))
                .subscribeOn(Schedulers.io())
                .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "setLiked"));
    }

    public Observable<List<ProfileData>> getItemsWithUpdates() {
        return profileCache.getItems().toObservable();
    }

    public Single<List<ProfileData>> getAll() {
        return profileCache.getItems().firstOrError();
    }

    public void deleteAll() {
        RxUtils.safeDispose(likeUsersDataDisposable);
        likeUsersDataDisposable = null;
        profileCache.clear();
        dbLikedMe.deleteAll();
    }
}