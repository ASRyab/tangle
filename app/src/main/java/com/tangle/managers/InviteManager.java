package com.tangle.managers;

import android.util.Pair;

import com.tangle.ApplicationLoader;
import com.tangle.api.Api;
import com.tangle.api.rpc.rpc_response.invite.InviteData;
import com.tangle.api.rx_tasks.activities.InvitingList;
import com.tangle.api.rx_tasks.payment.Respond;
import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.db.invite.InviteDao;
import com.tangle.model.event.EventInfo;
import com.tangle.model.payment.PaymentActionResult;
import com.tangle.utils.RxUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observables.ConnectableObservable;
import io.reactivex.schedulers.Schedulers;

public class InviteManager {
    private static final String TAG = InviteManager.class.getSimpleName();
    private final InviteDao dbInvite;
    private final ConnectableObservable<List<InviteData>> autoConnect;
    private Disposable memCacheDisposable;

    public InviteManager(InviteDao dao) {
        this.dbInvite = dao;
        autoConnect = dbInvite.getAllFlowable().toObservable()
                .subscribeOn(Schedulers.io())
                .replay(1);
    }

    public Observable<List<InviteData>> getAllWithUpdate() {
        return autoConnect
                .flatMap(data -> Observable.fromIterable(data)
                        .filter(inviteData -> inviteData.status!=InviteData.USER_REJECT_SENDING_INVITE_INDEX)
                        .flatMap(inviteData -> {
                            String id = ApplicationLoader.getApplicationInstance().getUserManager().isCurrentUser(inviteData.to) ? inviteData.from : inviteData.to;
                            return Observable.zip(
                                    Observable.just(inviteData)
                                    , ApplicationLoader.getApplicationInstance().getUserManager().getUserById(id).isEmpty().toObservable()
                                    , Pair::create);
                        })
                        .filter(pair -> !pair.second)
                        .map(pair -> pair.first).toList().toObservable());
    }

    public Observable<List<InviteData>> getAll() {
        return autoConnect.firstOrError().toObservable();
    }

    public void init() {
        if (memCacheDisposable == null) {
            memCacheDisposable = autoConnect.connect();
            Api.getInst().getSocketManager().subscribeOn(() -> forceLoadNew(0));
        }
    }

    public void forceLoadNew(int offset) {
        new InvitingList(offset).getDataTask()
                .observeOn(Schedulers.io())
                .zipWith(getAll(), Pair::create)
                .flatMap(pair -> Observable.fromIterable(pair.first)
                        .filter(data -> !pair.second.contains(data))
                        .toList().toObservable())
                .doOnNext(dbInvite::insertAll)
                .subscribe(items -> {
                    if (!items.isEmpty()) {
                        forceLoadNew(offset + items.size());
                    }
                }, RxUtils.getEmptyErrorConsumer(TAG, "forceLoadNew"));
    }

    public Observable<InviteData> getInviteById(String inviteId) {
        return dbInvite.loadInviteById(inviteId)
                .toObservable();
    }

    public void setInvite(InviteData invite) {
        dbInvite.insert(invite);
    }

    public void setAllAsRead() {
        autoConnect.observeOn(Schedulers.io()).firstElement()
                .toObservable()
                .flatMapIterable(data -> data)
                .doOnNext(data -> data.isRead = true)
                .toList()
                .subscribe(dbInvite::insertAll, RxUtils.getEmptyErrorConsumer(TAG, "setAllAsRead"));
    }

    public void deleteAll() {
        RxUtils.safeDispose(memCacheDisposable);
        memCacheDisposable = null;
        dbInvite.deleteAll();
    }

    public  Observable<PaymentActionResult> refuseAllForEvent(EventInfo event, LoadingView view) {
        return  getAll().flatMapIterable(inviteData -> inviteData)
                .compose(RxUtils.withLoading(view))
                .filter(inviteData -> inviteData.eventId.equals(event.eventId)
                        && InviteData.NOT_RESPOND_TO_INVITE_INDEX == inviteData.status
                ).flatMap(inviteData -> new Respond(inviteData,Respond.TICKET_STATUS_DECLINE)
                        .getDataTask()
                );
    }
}