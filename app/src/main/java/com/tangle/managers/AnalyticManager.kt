package com.tangle.managers

import com.google.firebase.analytics.FirebaseAnalytics
import com.tangle.ApplicationLoader
import com.tangle.analytics.PaymentActions
import com.tangle.analytics.PaymentPoints
import com.tangle.analytics.SimpleActions
import com.tangle.analytics.Via
import com.tangle.api.utils.Config
import com.tangle.model.profile_data.ProfileData
import com.tangle.utils.Debug

class AnalyticManager {

    val TAG = this.javaClass.simpleName

    var via: Via = Via.NONE
    private var isTracking: Boolean = false
    private val firebaseAnalytics: FirebaseAnalytics

    init {
        isTracking = Config.isTracking()
        firebaseAnalytics = FirebaseAnalytics.getInstance(ApplicationLoader.getContext())
    }


    fun trackEvent(action: SimpleActions) {
        if (isTracking) {
            firebaseAnalytics.logEvent(action.firebaseAction, null)
            Debug.logI(TAG, "trackSimpleEvent " + action.firebaseAction)
        }
    }

    private fun trackEvent(action: String) {
        if (isTracking) {
            firebaseAnalytics.logEvent(action, null)
            Debug.logI(TAG, "trackEvent $action")
        }
    }

    fun initUser(currentUser: ProfileData) {
        isTracking = Config.isTracking() && !isTestMail(currentUser.email)
        if (isTracking) {
            firebaseAnalytics.setUserId(currentUser.id)
            firebaseAnalytics.setUserProperty("gender", currentUser.gender.getName())
            Debug.logI(TAG, "initUser")
        }
    }

    private fun isTestMail(email: String): Boolean {
        return email.toLowerCase().contains("@maildrop.ropot.net")
    }


    fun trackVia(paymentPoints: PaymentPoints) {
        val actionFromVia = getActionFromVia(via)
        if (isTracking && actionFromVia != PaymentActions.NONE)
            when (paymentPoints) {
                PaymentPoints.OPEN_PP -> trackEvent(actionFromVia.openPP)
                PaymentPoints.ON_TRY_PP -> trackEvent(actionFromVia.onTryPP)
                PaymentPoints.SUCCESS -> trackEvent(actionFromVia.success)
            }
    }

    private fun getActionFromVia(via: Via): PaymentActions {
        return when (via) {
            Via.LIKEBOOK_INVITE -> PaymentActions.LIKEBOOK
            Via.EVENT_INVITE -> PaymentActions.EVENT_INVITE
            Via.EVENT_BOOK -> PaymentActions.EVENT_BOOK
            Via.PROFILE_BOOK -> PaymentActions.PROFILE_BOOK
            Via.PROFILE_INVITE -> PaymentActions.PROFILE_INVITE
            Via.MATCH_INVITE -> PaymentActions.MATCH_INVITE
            Via.MATCH_INVITE_SHORT -> PaymentActions.MATCH_INVITE_SHORT
            else -> PaymentActions.NONE
        }
    }


}

