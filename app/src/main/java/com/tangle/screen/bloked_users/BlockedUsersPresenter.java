package com.tangle.screen.bloked_users;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.RxUtils;

public class BlockedUsersPresenter extends LifecyclePresenter<BlockedUsersContracts.BlockedUsersView> {
    private BlockedUsersContracts.BlockedUsersModel blockedUsersModel;

    public BlockedUsersPresenter(BlockedUsersModel model) {
        super();
        this.blockedUsersModel = model;
    }

    @Override
    public void attachToView(BlockedUsersContracts.BlockedUsersView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        loadUsers();
    }

    private void loadUsers() {
        monitor(blockedUsersModel
                        .loadUsers()
                        .compose(RxUtils.withLoading(getView()))
                        .subscribe(list -> getView().showUsers(list), RxUtils.getEmptyErrorConsumer(TAG, "loadUsers"))
                , State.DESTROY_VIEW);
    }

    public void approveUnBlock(ProfileData user) {
        monitor(blockedUsersModel.approveUnBlock(user)
                        .compose(RxUtils.withLoading(getView()))
                        .doOnNext(data -> getView().removeUser(user))
                        .compose(BlockedUsersModel.unblockTransformer(user))
                        .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "approveUnBlock"))
                , State.DESTROY_VIEW);
    }

    public void onUnblockClicked(ProfileData user) {
        getView().showUnblockDialog(user);
    }
}
