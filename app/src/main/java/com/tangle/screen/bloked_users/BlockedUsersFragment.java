package com.tangle.screen.bloked_users;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.managers.DialogManager;
import com.tangle.model.profile_data.ProfileData;

import java.util.List;

public class BlockedUsersFragment extends SecondaryScreenFragment implements BlockedUsersContracts.BlockedUsersView, BlockUsersAdapter.UnblockListener {
    private BlockedUsersPresenter presenter = new BlockedUsersPresenter(new BlockedUsersModel());
    private RecyclerView recyclerView;
    private View notificationInformationLayout;
    private BlockUsersAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_blocked_users, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new BlockUsersAdapter(this);
        recyclerView.setAdapter(adapter);
        notificationInformationLayout = view.findViewById(R.id.notify_view);
        initToolbar(getResources().getString(R.string.blocked_users),true);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showUsers(List<ProfileData> list) {
        adapter.setUsers(list);
        if (!list.isEmpty()) {
            notificationInformationLayout.setVisibility(View.GONE);
        } else {
            notificationInformationLayout.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void showUnblockDialog(ProfileData user) {
        DialogManager.getInstance(this).showUnblockUserDialog((v, dialog) -> presenter.approveUnBlock(user));
    }

    @Override
    public void removeUser(ProfileData user) {
        if (adapter != null) {
            adapter.removeUser(user);
            if (adapter.getUsers().size()==0) {
                notificationInformationLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onUnblockClicked(ProfileData user) {
        presenter.onUnblockClicked(user);
    }
}
