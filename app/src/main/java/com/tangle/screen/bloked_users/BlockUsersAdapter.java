package com.tangle.screen.bloked_users;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.tangle.R;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.List;

public class BlockUsersAdapter extends RecyclerView.Adapter<BlockUsersAdapter.ViewHolder> {
    private final UnblockListener listener;
    private List<ProfileData> users;

    public BlockUsersAdapter(UnblockListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_blocked_user, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(users.get(position));
    }

    @Override
    public int getItemCount() {
        return (users == null) ? 0 : users.size();
    }

    public void removeUser(ProfileData user) {
        int index = users.indexOf(user);

        notifyItemRemoved(index);
        users.remove(user);
    }

    public List<ProfileData> getUsers() {
        return users;
    }

    public void setUsers(List<ProfileData> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView name, age, unblock;
        private final ImageView avatar;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.txt_name);
            age = itemView.findViewById(R.id.txt_age);
            unblock = itemView.findViewById(R.id.btn_unblock);
            avatar = itemView.findViewById(R.id.img_user_photo);
        }

        public void bind(ProfileData data) {
            name.setText(data.login);
            age.setText(age.getContext().getString(R.string.format_age, data.age));
            GlideApp.with(avatar)
                    .load(data.primaryPhoto.avatar)
                    .apply(RequestOptions.priorityOf(Priority.HIGH))
                    .apply(RequestOptions.circleCropTransform())
                    .placeholder(RequestOptionsFactory.getNonAvatar(data))
                    .into(avatar);
            unblock.setOnClickListener(view -> listener.onUnblockClicked(data));
        }
    }

    interface UnblockListener {
        void onUnblockClicked(ProfileData user);
    }
}
