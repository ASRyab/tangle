package com.tangle.screen.bloked_users;

import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.profile.GetBlockedUsers;
import com.tangle.api.rx_tasks.profile.UnblockUser;
import com.tangle.model.profile_data.BlockUserData;
import com.tangle.model.profile_data.ProfileData;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;

public class BlockedUsersModel implements BlockedUsersContracts.BlockedUsersModel {
    @Override
    public Observable<List<ProfileData>> loadUsers() {
        return new GetBlockedUsers().getDataTask()
                .map(response -> response.users);
    }

    @Override
    public Observable<BlockUserData> approveUnBlock(ProfileData user) {
        return new UnblockUser(user.id).getDataTask();
    }

    public static ObservableTransformer<BlockUserData, ProfileData> unblockTransformer(ProfileData user) {
        return upstream -> upstream
                .flatMap(data -> ApplicationLoader.getApplicationInstance().getUserManager().getUserById(user.id))
                .flatMap(data -> {
                    data.blockedUser = false;
                    return ApplicationLoader.getApplicationInstance().getUserManager().addObserve(data);
                });
    }

}
