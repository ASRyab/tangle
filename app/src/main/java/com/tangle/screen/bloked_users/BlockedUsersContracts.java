package com.tangle.screen.bloked_users;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.profile_data.BlockUserData;
import com.tangle.model.profile_data.ProfileData;

import java.util.List;

import io.reactivex.Observable;

public interface BlockedUsersContracts {
    interface BlockedUsersView extends LoadingView {
        void showUsers(List<ProfileData> list);

        void showUnblockDialog(ProfileData user);

        void removeUser(ProfileData user);
    }

    interface BlockedUsersModel {
        Observable<List<ProfileData>> loadUsers();

        Observable<BlockUserData> approveUnBlock(ProfileData user);
    }
}
