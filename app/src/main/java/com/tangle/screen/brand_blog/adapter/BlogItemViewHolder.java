package com.tangle.screen.brand_blog.adapter;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.screen.brand_blog.OnBlogItemClickListener;
import com.tangle.screen.brand_blog.model.BlogRegularItem;
import com.tangle.screen.events.gallery.GalleryAdapter;
import com.tangle.screen.linked_events.LinkedEventVH;
import com.tangle.utils.ConvertingUtils;
import com.tangle.utils.GalleryUtils;
import com.tangle.utils.Patterns;
import com.tangle.utils.RxUtils;
import com.tangle.utils.glide.GlideApp;

import java.util.List;

import static com.tangle.ApplicationLoader.getContext;

public class BlogItemViewHolder extends RecyclerView.ViewHolder {

    private static final String TAG = BlogItemViewHolder.class.getSimpleName();
    private BlogRegularItem item;
    private boolean isEvent;
    private EventInfo event;
    private TextView tvTitle;
    private TextView tvDescription;
    private TextView tvEventName;
    private TextView tvDateLocation;
    private ImageView ivPreview;
    private View loaderView;
    private View imageContainer;
    private RecyclerView rvGallery;
    private GalleryAdapter galleryAdapter;
    private View.OnClickListener eventClickListener;
    private OnBlogItemClickListener listener;
    private View linkedEventsView;

    public BlogItemViewHolder(View itemView, OnBlogItemClickListener listener) {
        super(itemView);
        this.listener = listener;
        this.eventClickListener = v -> {
            if (listener != null) listener.onEventClick(event, false);
        };
        tvTitle = itemView.findViewById(R.id.tv_header);
        tvDescription = itemView.findViewById(R.id.tv_content);
        ivPreview = itemView.findViewById(R.id.img_preview);
        loaderView = itemView.findViewById(R.id.view_loader);
        imageContainer = itemView.findViewById(R.id.image_data_container);
        rvGallery = itemView.findViewById(R.id.rv_gallery);
        tvEventName = itemView.findViewById(R.id.tv_event_name);
        tvDateLocation = itemView.findViewById(R.id.tv_date_location);
        linkedEventsView = itemView.findViewById(R.id.linked_events_container);
        initGallery();
    }

    void bind(BlogRegularItem item) {
        this.item = item;
        event = item.getEvent();
        isEvent = event != null;
        initListeners();
        setGalleryVisibility(false);
        tvTitle.setText(item.getTitle());
        if (!TextUtils.isEmpty(item.getDescription())) {
            tvDescription.setVisibility(View.VISIBLE);
            Spanned spanned = Html.fromHtml(item.getDescription());
            tvDescription.setText(ConvertingUtils.trimTrailingWhitespace(spanned));
        } else {
            tvDescription.setVisibility(View.GONE);
        }

        if (isEvent) {
            ApplicationLoader.getApplicationInstance().getEventManager().getEventById(event.eventId)
                    .subscribe(info -> bindEvent(info), RxUtils.getEmptyErrorConsumer(TAG, "bind"));
        }
        loadPhoto();
    }

    private void bindEvent(EventInfo info) {
        setGalleryItems(info.galleryMedia);
        tvEventName.setText(info.title);
        String date = Patterns.DATE_TIME_FORMAT.format(info.eventStartDate);
        String address = info.getEventAddress(getContext());
        if (!TextUtils.isEmpty(address)) {
            date = date + "/" + address;
        }
        tvDateLocation.setText(date);
        if (item.getLinkedEvents() != null && !item.getLinkedEvents().isEmpty()) {
            tvEventName.setVisibility(View.GONE);
            tvDateLocation.setVisibility(View.GONE);
            linkedEventsView.setVisibility(View.VISIBLE);
            new LinkedEventVH(linkedEventsView, event -> listener.onEventClick(event, true))
                    .bind(item.getLinkedEvents().get(0));
        } else {
            linkedEventsView.setVisibility(View.GONE);
            tvEventName.setVisibility(View.VISIBLE);
            tvDateLocation.setVisibility(View.VISIBLE);
        }
    }

    private void initListeners() {
        tvDateLocation.setOnClickListener(isEvent ? eventClickListener : null);
        tvEventName.setOnClickListener(isEvent ? eventClickListener : null);
        ivPreview.setOnClickListener(isEvent ? eventClickListener : null);
    }

    private void loadPhoto() {
        if (item.getDisplayMedia() && item.getCoverImage() != null && !item.getCoverImage().isEmpty()) {
            imageContainer.setVisibility(View.VISIBLE);
            ivPreview.setVisibility(View.INVISIBLE);
            loaderView.setVisibility(View.VISIBLE);
            GlideApp.with(ivPreview)
                    .load(Uri.parse(item.getCoverImage()))
                    .into(new DrawableImageViewTarget(ivPreview) {
                        @Override
                        public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                            super.onResourceReady(resource, transition);
                            loaderView.setVisibility(View.INVISIBLE);
                            ivPreview.setVisibility(View.VISIBLE);
                        }
                    });
        } else imageContainer.setVisibility(View.GONE);

    }

    private void initGallery() {
        galleryAdapter = GalleryUtils.setupListAndGetAdapter(rvGallery, getContext());
    }

    private void setGalleryItems(List<Media> galleryItems) {
        setGalleryVisibility(!galleryItems.isEmpty());
        galleryAdapter.switchGalleryItems(galleryItems);
        galleryAdapter.setMediaClickListener(media -> {
            if (listener != null) {
                String linkedEventId = null;
                if (item.getLinkedEvents() != null && !item.getLinkedEvents().isEmpty()) {
                    linkedEventId = item.getLinkedEvents().get(0).eventId;
                }
                listener.onGalleryItemClick(galleryItems, media, linkedEventId);
            }
        });
    }

    private void setGalleryVisibility(boolean hasImages) {
        rvGallery.setVisibility(hasImages ? View.VISIBLE : View.GONE);
        tvEventName.setVisibility(hasImages ? View.VISIBLE : View.GONE);
        tvDateLocation.setVisibility(hasImages ? View.VISIBLE : View.GONE);
    }

}