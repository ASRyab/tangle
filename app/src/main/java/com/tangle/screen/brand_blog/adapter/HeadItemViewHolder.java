package com.tangle.screen.brand_blog.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.screen.brand_blog.model.BlogHeadItem;

public class HeadItemViewHolder extends RecyclerView.ViewHolder {

    private TextView tvHeadText;

    public HeadItemViewHolder(View itemView) {
        super(itemView);
        tvHeadText = itemView.findViewById(R.id.tv_head_text);
    }

    void bind(BlogHeadItem item) {
        tvHeadText.setText(item.getTitle());
    }
}