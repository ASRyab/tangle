package com.tangle.screen.brand_blog.model;

import com.tangle.model.blog.BlogEntity;
import com.tangle.model.event.EventInfo;

import java.util.Date;
import java.util.List;

public class BlogRegularItem extends BlogItem {

    private String postId;
    private String eventId;
    private String categoryId;
    private String title;
    private String description;
    private Date postDate;
    private int status;
    private boolean isDisplayMedia;
    private Date updatedAt;
    private Date createdAt;
    private EventInfo event;
    private String coverImage;
    private String timeZone;
    private Date postTzDate;
    private List<EventInfo> linkedEvents;

    private BlogRegularItem() {
    }

    public static BlogItem convertFromSource(BlogEntity item) {
        BlogRegularItem result = new BlogRegularItem();
        result.postId = item.getPostId();
        result.eventId = item.getEventId();
        result.categoryId = item.getCategoryId();
        result.title = item.getTitle();
        result.description = item.getDescription();
        result.postDate = item.getPostDate();
        result.status = item.getStatus();
        result.isDisplayMedia = item.getDisplayMedia();
        result.updatedAt = item.getUpdatedAt();
        result.createdAt = item.getCreatedAt();
        result.event = item.getEvent();
        result.coverImage = item.getCoverImage();
        result.timeZone = item.getTimeZone();
        result.postTzDate = item.getPostTzDate();
        return result;
    }

    public List<EventInfo> getLinkedEvents() {
        return linkedEvents;
    }

    public void setLinkedEvents(List<EventInfo> linkedEvents) {
        this.linkedEvents = linkedEvents;
    }

    public String getPostId() {
        return postId;
    }

    public String getEventId() {
        return eventId;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Date getPostDate() {
        return postDate;
    }

    public int getStatus() {
        return status;
    }

    public Boolean getDisplayMedia() {
        return isDisplayMedia;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public EventInfo getEvent() {
        return event;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public Date getPostTzDate() {
        return postTzDate;
    }
}
