package com.tangle.screen.brand_blog;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.blog.BlogEntity;
import com.tangle.model.event.EventInfo;
import com.tangle.screen.brand_blog.model.BlogHeadItem;
import com.tangle.screen.brand_blog.model.BlogItem;
import com.tangle.screen.brand_blog.model.BlogRegularItem;
import com.tangle.utils.RxUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;

public class BrandBlogPresenter extends LifecyclePresenter<BrandBlogContract.View> {

    private BrandBlogContract.Model model;

    BrandBlogPresenter(BrandBlogContract.Model model) {
        this.model = model;
    }

    @Override
    public void attachToView(BrandBlogContract.View view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        loadBrandBlog();
    }

    private void loadBrandBlog() {
        monitor(model.setAllAsRead(), State.DESTROY_VIEW);

        monitor(model
                .getItemsWithUpdates()
                .compose(composeTypeCast())
                .map(list -> {
                    list.add(0, new BlogHeadItem(getContext().getString(R.string.blog_info_message)));
                    return list;
                })
                .subscribe(res -> getView().setBlogItems(res), RxUtils.getEmptyErrorConsumer(TAG,"loadBrandBlog")), State.DESTROY_VIEW);
    }


    private Observable<List<EventInfo>> getLinkedEventsObservable(List<String> linkedEvents) {
        return ApplicationLoader
                .getApplicationInstance()
                .getEventManager()
                .getEventsByIds(linkedEvents);
    }

    private ObservableTransformer<List<BlogEntity>, List<BlogItem>> composeTypeCast() {
        return items -> items.flatMap(eventInfos -> Observable.fromIterable(eventInfos)
                .flatMap(entity -> {
                    if (entity.getEvent() != null && !entity.getEvent().linkedEvents.isEmpty()) {
                        return getLinkedEventsObservable(entity.getEvent().linkedEvents)
                                .flatMap(value -> {
                                    BlogItem item = BlogRegularItem.convertFromSource(entity);
                                    ((BlogRegularItem) item).setLinkedEvents(value);
                                    return Observable.just(item);
                                });
                    } else {
                        return Observable.just(BlogRegularItem.convertFromSource(entity));
                    }
                })
                .toList()
                .toObservable());
    }


}
