package com.tangle.screen.brand_blog;

import com.tangle.managers.BlogInfoManager;
import com.tangle.model.blog.BlogEntity;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class BrandBlogModel implements BrandBlogContract.Model {

    private BlogInfoManager manager;

    public BrandBlogModel(BlogInfoManager manager) {
        this.manager = manager;
    }

    @Override
    public Observable<List<BlogEntity>> getItemsWithUpdates() {
        return manager.getItemsWithUpdates();
    }

    @Override
    public Observable<Integer> getUnreadCount() {
        return manager.getUnreadCount();
    }

    @Override
    public Disposable setAllAsRead() {
        return manager.setAllAsRead();
    }

    @Override
    public Observable<List<BlogEntity>> forceLoad() {
        return manager.forceLoad();
    }
}