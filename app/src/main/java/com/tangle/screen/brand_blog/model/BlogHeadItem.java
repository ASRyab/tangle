package com.tangle.screen.brand_blog.model;

public class BlogHeadItem extends BlogItem {
    private String title;

    public BlogHeadItem(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
