package com.tangle.screen.brand_blog;

import com.tangle.model.blog.BlogEntity;
import com.tangle.screen.brand_blog.model.BlogItem;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public interface BrandBlogContract {

    interface View {
        void setBlogItems(List<BlogItem> items);
    }

    interface Model {
        Observable<List<BlogEntity>> getItemsWithUpdates();

        Observable<Integer> getUnreadCount();

        Disposable setAllAsRead();

        Observable<List<BlogEntity>> forceLoad();
    }
}
