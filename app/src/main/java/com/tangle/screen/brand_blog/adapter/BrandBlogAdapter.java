package com.tangle.screen.brand_blog.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.tangle.R;
import com.tangle.screen.brand_blog.OnBlogItemClickListener;
import com.tangle.screen.brand_blog.model.BlogHeadItem;
import com.tangle.screen.brand_blog.model.BlogItem;
import com.tangle.screen.brand_blog.model.BlogRegularItem;

import java.util.ArrayList;
import java.util.List;

public class BrandBlogAdapter extends RecyclerView.Adapter {

    private final static int HEAD_ITEM_TYPE = 0;
    private final static int BLOG_ITEM_TYPE = 1;

    private List<BlogItem> items = new ArrayList<>();

    private OnBlogItemClickListener listener;

    public BrandBlogAdapter(OnBlogItemClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        switch (viewType) {
            case HEAD_ITEM_TYPE:
                return new HeadItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_brand_blog_head, parent, false));
            default:
                return new BlogItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_brand_blog, parent, false), listener);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {
            case HEAD_ITEM_TYPE:
                ((HeadItemViewHolder) holder).bind((BlogHeadItem) items.get(position));
                break;
            case BLOG_ITEM_TYPE:
                ((BlogItemViewHolder) holder).bind((BlogRegularItem) items.get(position));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (items.get(position) instanceof BlogHeadItem) return HEAD_ITEM_TYPE;
        else return BLOG_ITEM_TYPE;
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void update(List<BlogItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }
}
