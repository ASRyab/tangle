package com.tangle.screen.brand_blog;

import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;

import java.util.List;

public interface OnBlogItemClickListener {
    void onEventClick(EventInfo eventInfo, boolean isLinked);
    void onGalleryItemClick(List<Media> galleryItems, Media media,String linkedEventId);
}