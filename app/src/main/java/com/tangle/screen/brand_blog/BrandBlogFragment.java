package com.tangle.screen.brand_blog;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.screen.brand_blog.adapter.BrandBlogAdapter;
import com.tangle.screen.brand_blog.model.BlogItem;
import com.tangle.screen.events.details.EventDetailsFragment;
import com.tangle.screen.events.map.BaseEventPresenter;
import com.tangle.screen.media.pager.MediaContainerFragment;
import com.tangle.utils.ConvertingUtils;
import com.tangle.utils.ui.HideViewOnScrollListener;

import java.util.List;

public class BrandBlogFragment extends SecondaryScreenFragment implements BrandBlogContract.View {
    private static final float BG_MAX_SCALE = 3f;
    private static final float BG_MIN_SCALE = 0.9f;
    private static final int BG_ANIM_DURATION = 400;

    private BrandBlogPresenter presenter = new BrandBlogPresenter(new BrandBlogModel(ApplicationLoader.getApplicationInstance().getBlogInfoManager()));
    private BrandBlogAdapter adapter;

    private RecyclerView brandBlogRecyclerView;
    private ImageView background;
    private int shift;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_brand_blog, container, false);
        brandBlogRecyclerView = v.findViewById(R.id.rv_brand_blog);
        background = v.findViewById(R.id.ivBackground);
        initBrandBlogList();
        shift = Math.round(ConvertingUtils.convertDpToPixel(180, getContext()));
        return v;
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        scaleView(background, BG_MIN_SCALE, BG_MAX_SCALE);
    }

    @Override
    public boolean onBackPressed() {
        scaleView(background, BG_MAX_SCALE, BG_MIN_SCALE);
        return super.onBackPressed();
    }

    public void scaleView(View v, float startScale, float endScale) {
        final Rect startBounds = new Rect();
        v.getGlobalVisibleRect(startBounds);
        final Rect finalBounds = new Rect();
        finalBounds.top = startBounds.top - shift;
        Animator shiftAnim = startScale > endScale ?
                ObjectAnimator.ofFloat(v, View.Y, startBounds.top, finalBounds.top) :
                ObjectAnimator.ofFloat(v, View.Y, finalBounds.top, startBounds.top);

        AnimatorSet set = new AnimatorSet();
        set.play(ObjectAnimator.ofFloat(background, View.SCALE_Y, startScale, endScale))
                .with(shiftAnim)
                .with(ObjectAnimator.ofFloat(background, View.SCALE_X, startScale, endScale));
        set.setDuration(BG_ANIM_DURATION);
        set.start();
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    private void initBrandBlogList() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        brandBlogRecyclerView.setLayoutManager(linearLayoutManager);
        adapter = new BrandBlogAdapter(new OnBlogItemClickListener() {
            @Override
            public void onEventClick(EventInfo event, boolean isLinkedEvent) {
                addFragment(isLinkedEvent ? EventDetailsFragment.newInstance(event, BaseEventPresenter.GoingFrom.LINKED_EVENTS) : EventDetailsFragment.newInstance(event), true);
            }

            @Override
            public void onGalleryItemClick(List<Media> galleryItems, Media media, String linkedEventId) {
                MediaContainerFragment mediaContainerFragment = MediaContainerFragment.newInstance(galleryItems, media, MediaContainerFragment.ShowType.PAST_EVENT, linkedEventId);
                addFragment(mediaContainerFragment, true);
            }
        });

        brandBlogRecyclerView.setAdapter(adapter);
        brandBlogRecyclerView.addOnScrollListener(new HideViewOnScrollListener(background));
    }

    @Override
    public void setBlogItems(List<BlogItem> items) {
        adapter.update(items);
    }
}