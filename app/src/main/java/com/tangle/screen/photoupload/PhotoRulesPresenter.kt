package com.tangle.screen.photoupload

import com.tangle.api.rx_tasks.dictionary.GetContactUsInfo
import com.tangle.base.mvp.LifecyclePresenter
import com.tangle.base.mvp.State
import com.tangle.model.contact_us.ContactUsData
import com.tangle.utils.RxUtils
import io.reactivex.functions.Consumer

class PhotoRulesPresenter : LifecyclePresenter<PhotoRulesView>() {
    fun onClickContactUs() {
        monitor(GetContactUsInfo().dataTask
                .compose<ContactUsData>(RxUtils.withLoading(view))
                .subscribe(Consumer { view.openContactUs(it) }, RxUtils.getEmptyErrorConsumer(PhotoRulesPresenter::class.java.simpleName)),
                State.DESTROY_VIEW)
    }
}