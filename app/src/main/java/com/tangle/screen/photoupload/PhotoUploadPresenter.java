package com.tangle.screen.photoupload;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.photo.UploadPhotoModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.managers.PhotoManager;
import com.tangle.utils.RxUtils;

import java.io.File;

import io.reactivex.disposables.Disposable;

public class PhotoUploadPresenter extends LifecyclePresenter<PhotoUploadView> {

    public void uploadNewPhoto(File photoFile) {
        Disposable disposable = new UploadPhotoModel(photoFile, false).getTask()
                .compose(RxUtils.withLoading(getView()))
                .subscribe(uploadPhoto -> {
                            refreshProfile();
                            getView().goNextScreen();
                            ApplicationLoader.getApplicationInstance().getPhotoManager().updatePhotoState(PhotoManager.PhotoState.HAS_PHOTO);
                        },
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    private void refreshProfile() {
        ApplicationLoader.getApplicationInstance().getUserManager().loadCurrentUser();
    }

    @Override
    public void attachToView(PhotoUploadView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().currentUser()
                .subscribe(data -> getView().updateUserName(data.login), RxUtils.getEmptyErrorConsumer()), State.DESTROY_VIEW);
    }

    public void onRulesClick() {
        getView().showRules();
    }
}