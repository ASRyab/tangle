package com.tangle.screen.photoupload;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.managers.DialogManager;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.own_user_photo.OwnPhotoPresenter;
import com.tangle.utils.own_user_photo.OwnPhotoView;

import java.io.File;

public class PhotoUploadFragment extends SecondaryScreenFragment implements PhotoUploadView, OwnPhotoView {

    private static final String STATE_KEY = "state_key";

    private PhotoUploadPresenter presenter = new PhotoUploadPresenter();
    private OwnPhotoPresenter ownPhotoPresenter = new OwnPhotoPresenter(this);

    private ImageView imgAvatar;
    private TextView vTakePhoto;
    private TextView vChoosePhoto;
    private TextView vTitle;
    private TextView vMaybeLater;
    private Button btnFinish;
    private State state = State.NONE;
    private View backgroundContainer;
    private View vRules;
    private TextView vMessage;

    public enum State {
        FRAME, DECLINE, NONE
    }

    public static PhotoUploadFragment newInstance() {
        return new PhotoUploadFragment();
    }

    public static PhotoUploadFragment newInstance(State state) {
        PhotoUploadFragment fragment = new PhotoUploadFragment();
        Bundle args = new Bundle();
        args.putSerializable(STATE_KEY, state);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_photo_upload, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        if (getArguments() != null) {
            state = (State) getArguments().getSerializable(STATE_KEY);
        }
        initUI(view);
    }

    private void initUI(View view) {
        btnFinish = view.findViewById(R.id.btn_next);
        backgroundContainer = view.findViewById(R.id.background_container);
        imgAvatar = view.findViewById(R.id.img_avatar);
        vMaybeLater = view.findViewById(R.id.txt_maybe_later);
        vTakePhoto = view.findViewById(R.id.txt_make_photo);
        vTitle = view.findViewById(R.id.tvTitle);
        vChoosePhoto = view.findViewById(R.id.txt_choose_photo);
        vRules = view.findViewById(R.id.txt_posting_rules);
        vRules.setOnClickListener(v -> presenter.onRulesClick());
        vMessage = view.findViewById(R.id.txt_message);
        imgAvatar.setOnClickListener(v -> ownPhotoPresenter.onAddPhotoClicked());
        vChoosePhoto.setOnClickListener(v -> ownPhotoPresenter.onGalleryPickClicked(this));
        vTakePhoto.setOnClickListener(v -> ownPhotoPresenter.onTakePictureClicked(this));
        vMaybeLater.setOnClickListener(v -> goNextScreen());
        btnFinish.setOnClickListener(v -> ownPhotoPresenter.onNextClickedHandler());

        switch (state) {
            case FRAME:
                backgroundContainer.setBackgroundResource(R.drawable.gradient_dark);
                vMaybeLater.setVisibility(View.GONE);
                break;
            case DECLINE:
                vRules.setVisibility(View.GONE);
                vMessage.setText(R.string.photo_declined_message);
                break;
            case NONE:
                backgroundContainer.setBackgroundResource(R.color.colorBackgroundFunnel);
                break;
        }
    }

    public void setError(String error) {
        showAPIError(error);
    }

    @Override
    public void setCurrentImage(File photoFile) {

        if (state == State.FRAME) {
            ownPhotoPresenter.onNextClickedHandler();
        } else if (photoFile != null) {
            vChoosePhoto.setVisibility(View.INVISIBLE);
            vTakePhoto.setText(R.string.funnel_replace_photo);
            vTakePhoto.setOnClickListener(v -> ownPhotoPresenter.onAddPhotoClicked());
            imgAvatar.setVisibility(View.VISIBLE);
            vMaybeLater.setVisibility(View.GONE);
            GlideApp.with(this)
                    .load(photoFile)
                    .apply(RequestOptions.circleCropTransform())
                    .into(imgAvatar);
            btnFinish.setVisibility(View.VISIBLE);
        } else {
            vChoosePhoto.setVisibility(View.VISIBLE);
            vTakePhoto.setText(R.string.funnel_take_photo);
            vTakePhoto.setOnClickListener(v -> ownPhotoPresenter.onTakePictureClicked(this));
            imgAvatar.setImageResource(R.drawable.screen_avatar_ic_add_photo);
        }
    }

    @Override
    public void updateUserName(String currentUserName) {
        switch (state) {
            case FRAME:
                backgroundContainer.setBackgroundResource(R.drawable.gradient_dark);
                vMaybeLater.setVisibility(View.GONE);
                break;
            case DECLINE:
                vRules.setVisibility(View.GONE);
                break;
            case NONE:
                backgroundContainer.setBackgroundResource(R.color.colorBackgroundFunnel);
                break;
        }
        if (state == State.DECLINE) {
            generateSpan(vTitle);
        } else {
            vTitle.setText(getString(R.string.should_upload_photo, currentUserName));
        }
    }

    private void generateSpan(TextView title) {
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                presenter.onRulesClick();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(getResources().getColor(R.color.colorPrimary));
            }
        };
        String text = getString(R.string.photo_declined_title);
        String spanned = getString(R.string.posting_rules);
        SpannableStringBuilder builder = new SpannableStringBuilder(text);
        builder.setSpan(clickableSpan, text.indexOf(spanned), text.indexOf(spanned) + spanned.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        title.setText(builder);
        title.setMovementMethod(LinkMovementMethod.getInstance());
        title.setHighlightColor(Color.TRANSPARENT);
    }

    public void showUploadDialog(OwnPhotoPresenter presenter) {
        DialogManager manager = DialogManager.getInstance(this);
        manager.showPhotoUploadDialog(presenter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ownPhotoPresenter.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    public void uploadAvailablePhoto(File photoFile) {
        presenter.uploadNewPhoto(photoFile);
    }

    @Override
    public void goNextScreen() {
        if (state != State.FRAME)
            if (getActivity() != null) {
                getActivity().onBackPressed();
            }
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showRules() {
        ApplicationLoader.getApplicationInstance().getNavigationManager().showPhotoRulesFragment();
    }
}