package com.tangle.screen.photoupload

interface PhotoRulesOpenView {
    fun showRules()
}