package com.tangle.screen.photoupload

import android.graphics.Color
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.SpannableString
import android.text.Spanned
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tangle.R
import com.tangle.base.mvp.LifecyclePresenter
import com.tangle.base.ui.SecondaryScreenFragment
import com.tangle.model.contact_us.ContactUsData
import com.tangle.screen.contact_us.ContactUsFragment
import kotlinx.android.synthetic.main.fragment_photo_rules.*

class PhotoRulesFragment : SecondaryScreenFragment(), PhotoRulesView {
    val presenterPhoto: PhotoRulesPresenter = PhotoRulesPresenter()

    override fun openContactUs(contactUsData: ContactUsData) {
        switchFragment(ContactUsFragment.newInstance(contactUsData), true)
    }

    override fun getPresenter(): LifecyclePresenter<*> {
        return presenterPhoto
    }

    companion object {
        @JvmStatic
        fun getInstance(): PhotoRulesFragment {
            return PhotoRulesFragment()
        }
    }

    override fun onPostViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onPostViewCreated(view, savedInstanceState)
        initUI()
    }

    private fun initUI() {
        txt_footer.movementMethod = LinkMovementMethod.getInstance()
        val spanText = getString(R.string.settings_item_contact_us)
        val body = String.format(getString(R.string.posting_rules_contact), spanText)
        val start = body.indexOf(spanText)
        val finish = start + spanText.length
        val spannableBody = SpannableString(body)
        spannableBody.setSpan(object : ClickableSpan() {

            override fun onClick(view: View) {
                onClickContactUs()
            }

            override fun updateDrawState(ds: TextPaint) {
                context?.let {
                    ds.color = ContextCompat.getColor(it, R.color.colorPrimary)
                }
                ds.isUnderlineText = false
            }
        }, start, finish, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE)
        txt_footer.highlightColor = Color.TRANSPARENT
        txt_footer.text = spannableBody
    }

    private fun onClickContactUs() {
        presenterPhoto.onClickContactUs()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_photo_rules, container, false)
    }
}