package com.tangle.screen.photoupload

import com.tangle.base.ui.interfaces.LoadingView
import com.tangle.model.contact_us.ContactUsData

interface PhotoRulesView: LoadingView {
    fun openContactUs(contactUsData: ContactUsData)
}
