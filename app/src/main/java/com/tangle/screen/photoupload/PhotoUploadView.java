package com.tangle.screen.photoupload;

import com.tangle.base.ui.interfaces.LoadingView;

public interface PhotoUploadView extends LoadingView, PhotoRulesOpenView {
    void setError(String error);

    void goNextScreen();

    void updateUserName(String currentUserName);
}