package com.tangle.screen.profile_photos;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.model.profile_data.Photo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ProfilePhotosAdapter extends RecyclerView.Adapter<ProfilePhotosAdapter.PhotoViewHolder> {
    private List<Photo> photos = new ArrayList<>();
    private PhotoClickListener photoClickListener;
    private RequestOptions userPhotoRequestOptions;
    private boolean isOwnProfile;
    private ProfileData profile;

    public ProfilePhotosAdapter(List<Photo> photos, PhotoClickListener photoClickListener, boolean isOwnProfile, ProfileData profile) {
        Collections.sort(photos);
        this.photos.addAll(photos);
        this.photoClickListener = photoClickListener;
        this.isOwnProfile = isOwnProfile;
        this.profile = profile;
        this.userPhotoRequestOptions = RequestOptionsFactory.getSmallRoundedCornersOptions(ApplicationLoader.getContext());
    }

    public class PhotoViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgUserPhoto, imgBtnPhotoDelete;
        public TextView txtPendingDelete;

        public PhotoViewHolder(View view) {
            super(view);
            imgUserPhoto = view.findViewById(R.id.img_user_photo);
            imgBtnPhotoDelete = view.findViewById(R.id.img_delete_user_photo);
            txtPendingDelete = view.findViewById(R.id.txt_pending_delete);
        }
    }

    @Override
    public ProfilePhotosAdapter.PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_profile_photos_item, parent, false);
        return new ProfilePhotosAdapter.PhotoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ProfilePhotosAdapter.PhotoViewHolder holder, int position) {
        Photo photo = photos.get(position);
        ImageView imgUserPhoto = holder.imgUserPhoto;
        imgUserPhoto.setOnClickListener(v -> {
            if (photoClickListener != null && !photo.pendingDelete) {
                photoClickListener.onPhotoClicked(photo);
            }
        });
        GlideApp.with(imgUserPhoto)
                .load(photo.normal)
                .placeholder(RequestOptionsFactory.getNonAvatar(profile))
                .apply(userPhotoRequestOptions)
                .into(imgUserPhoto);
        holder.txtPendingDelete.setVisibility(!photo.pendingDelete ? View.GONE : View.VISIBLE);
        if(!photo.pendingDelete) {
            holder.imgBtnPhotoDelete.setVisibility(isOwnProfile ? View.VISIBLE : View.GONE);
            if (isOwnProfile) {
                holder.imgBtnPhotoDelete.setOnClickListener(v -> {
                    if (photoClickListener != null) {
                        photoClickListener.onDeleteClicked(photo);
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }
}