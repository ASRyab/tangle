package com.tangle.screen.profile_photos;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.rx_tasks.photo.DeletePhoto;
import com.tangle.api.rx_tasks.photo.SetPrimaryPhoto;
import com.tangle.api.rx_tasks.photo.UploadPhotoModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.profile_data.Photo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.RxUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;

public class ProfilePhotosPresenter extends LifecyclePresenter<ProfilePhotosView> {
    private String userId;
    private ProfileData profileData;

    void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public void attachToView(ProfilePhotosView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        view.initToolbar(getResources().getString(R.string.photos), true,
                isOwnProfile() ? R.drawable.ic_edit_my_profile_add_new_photo_plus : null);
        loadProfile();
    }

    private void loadProfile() {
        if (isOwnProfile()) {
            monitor(ApplicationLoader.getApplicationInstance().getUserManager().currentUserWithUpdates()
                    .subscribe(this::setProfileData, error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
        } else {
            monitor(ApplicationLoader.getApplicationInstance().getUserManager().getUserGraphById(userId)
                    .subscribe(this::setProfileData, error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (!isOwnProfile()) {
            monitor(ApplicationLoader.getApplicationInstance().getUserManager().getUpdates()
                    .filter(profile -> profile.id.equals(userId))
                    .subscribe(this::setProfileData), State.STOP);
        }
    }

    private void setProfileData(ProfileData profile) {
        this.profileData = profile;
        getView().setPhotos(profile.photos, isOwnProfile(), profile);
    }

    private void refreshProfile() {
        if (isOwnProfile()) {
            ApplicationLoader.getApplicationInstance().getUserManager().loadCurrentUser();
        } else {
            loadProfile();
        }
    }

    public void onPhotoClicked(Photo photo) {
        getView().showFullPhotoViewer(profileData.photos, photo);
    }

    private boolean isOwnProfile() {
        return ApplicationLoader.getApplicationInstance().getUserManager().isCurrentUser(userId);
    }

    public void setPhotoAsAvatar(String photoId) {
        Disposable disposable = new SetPrimaryPhoto(photoId).getDataTask()
                .subscribe(changePrimaryPhoto -> {
                            if (changePrimaryPhoto.isApproved) {
                                refreshProfile();
                            } else {
                                List<Photo> newUpdatesPhoto = new ArrayList<>();
                                for (Photo photo : this.profileData.photos) {
                                    if (photo.id.equals(photoId)) {
                                        photo.isPrimary = 1;
                                        this.profileData.primaryPhoto = photo;
                                    } else {
                                        photo.isPrimary = 0;
                                    }
                                    newUpdatesPhoto.add(photo);
                                }
                                this.profileData.photos = newUpdatesPhoto;
                                ApplicationLoader.getApplicationInstance().getUserManager().setCurrentUser(this.profileData);
                            }
                        },
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    public void deletePhoto(String photoId) {
        Disposable disposable = new DeletePhoto(photoId).getDataTask()
                .compose(RxUtils.withLoading(getView()))
                .subscribe(deleteResponse -> refreshProfile(),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    public void uploadNewPhoto(File photoFile) {
        Disposable disposable = new UploadPhotoModel(photoFile, false).getTask()
                .compose(RxUtils.withLoading(getView()))
                .subscribe(uploadPhoto -> refreshProfile(),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }
}