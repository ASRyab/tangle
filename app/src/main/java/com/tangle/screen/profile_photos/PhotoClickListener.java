package com.tangle.screen.profile_photos;

import com.tangle.model.profile_data.Photo;

public interface PhotoClickListener {
    void onPhotoClicked(Photo photo);

    void onDeleteClicked(Photo photo);
}