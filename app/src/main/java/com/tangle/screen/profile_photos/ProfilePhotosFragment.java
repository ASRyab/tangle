package com.tangle.screen.profile_photos;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.managers.DialogManager;
import com.tangle.model.profile_data.Photo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.media.pager.MediaContainerFragment;
import com.tangle.utils.own_user_photo.OwnPhotoPresenter;
import com.tangle.utils.own_user_photo.OwnPhotoView;

import java.io.File;
import java.util.List;

public class ProfilePhotosFragment extends SecondaryScreenFragment implements ProfilePhotosView,
        PhotoClickListener, OwnPhotoView, View.OnClickListener {
    private final static String USER_ID_KEY = "user_id";
    private ProfilePhotosPresenter presenter = new ProfilePhotosPresenter();
    private OwnPhotoPresenter ownPhotoPresenter = new OwnPhotoPresenter(this);
    private RecyclerView rviewPhotos;

    public static ProfilePhotosFragment newInstance() {
        return newInstance(ApplicationLoader.getApplicationInstance().getUserManager().getCurrentUserId());
    }

    public static ProfilePhotosFragment newInstance(String userId) {
        ProfilePhotosFragment profilePhotosFragment = new ProfilePhotosFragment();
        Bundle args = new Bundle();
        args.putString(USER_ID_KEY, userId);
        profilePhotosFragment.setArguments(args);
        return profilePhotosFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getBundleData(getArguments());
    }

    private void getBundleData(Bundle bundle) {
        if (bundle != null) {
            presenter.setUserId(bundle.getString(USER_ID_KEY));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile_photos, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    @Override
    public void initToolbar(String title, boolean showBack, Integer rightIcoResId) {
        super.initToolbar(title, showBack, rightIcoResId);
    }

    private void initUI(View view) {
        view.findViewById(R.id.btn_right_ico).setOnClickListener(v -> ownPhotoPresenter.onAddPhotoClicked());
        rviewPhotos = view.findViewById(R.id.rview_photos);
    }

    @Override
    public void setPhotos(List<Photo> photos, boolean isOwnProfile, ProfileData profile) {
        rviewPhotos.setLayoutManager(new GridLayoutManager(rviewPhotos.getContext(), 3));
        rviewPhotos.setAdapter(new ProfilePhotosAdapter(photos, this, isOwnProfile, profile));
    }

    @Override
    public void showFullPhotoViewer(List<Photo> userPhotos, Photo startPhoto) {
        MediaContainerFragment profileMediaContainerFragment = MediaContainerFragment.newInstance(userPhotos,
                startPhoto, MediaContainerFragment.ShowType.CHANGE_OWN_PHOTO);
        profileMediaContainerFragment.setTargetFragment(this, MediaContainerFragment.SET_MEDIA_INDEX_CODE);
        addFragment(profileMediaContainerFragment, true);
    }

    @Override
    public void handlerAction(int action, Object resultObj) {
        String photoId = (String) resultObj;
        switch (action) {
            case ResultActions.ACTION_DELETE_OWN_PHOTO:
                presenter.deletePhoto(photoId);
                break;
            case ResultActions.ACTION_SET_PHOTO_AS_AVATAR:
                presenter.setPhotoAsAvatar(photoId);
                break;
            case ResultActions.ACTION_OPEN_ADD_PHOTOS_VIEW:
                ownPhotoPresenter.onAddPhotoClicked();
                break;
        }
    }

    @Override
    public void setError(String error) {
        showAPIError(error);
    }

    @Override
    public void setCurrentImage(File photoFile) {
        presenter.uploadNewPhoto(photoFile);
    }

    @Override
    public void uploadAvailablePhoto(File photoFile) {
    }

    @Override
    public void showUploadDialog(OwnPhotoPresenter presenter) {
        DialogManager manager = DialogManager.getInstance(this);
        manager.showPhotoUploadDialog(presenter);
    }

    @Override
    public void onPhotoClicked(Photo photo) {
        presenter.onPhotoClicked(photo);
    }

    @Override
    public void onDeleteClicked(Photo photo) {
        presenter.deletePhoto(photo.id);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_btn_add_photo:
                ownPhotoPresenter.onAddPhotoClicked();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ownPhotoPresenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}