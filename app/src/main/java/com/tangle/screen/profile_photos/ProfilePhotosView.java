package com.tangle.screen.profile_photos;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.profile_data.Photo;
import com.tangle.model.profile_data.ProfileData;

import java.util.List;

public interface ProfilePhotosView extends LoadingView {
    void showFullPhotoViewer(List<Photo> userPhotos, Photo startPhoto);

    void setPhotos(List<Photo> photos, boolean isOwnProfile, ProfileData profile);

    void initToolbar(String title, boolean showBack, Integer rightIcoResId);
}