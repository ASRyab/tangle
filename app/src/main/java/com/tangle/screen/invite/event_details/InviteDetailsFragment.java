package com.tangle.screen.invite.event_details;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.bumptech.glide.request.RequestOptions;
import com.google.android.gms.maps.GoogleMap;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.views.MultipleIndicatorView;
import com.tangle.base.ui.views.ShapeButton;
import com.tangle.managers.DialogManager;
import com.tangle.model.event.EventInfo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.details.ProfilesAdapter;
import com.tangle.screen.events.details.map.EventMapFragment;
import com.tangle.screen.events.list.MediaPagerAdapter;
import com.tangle.screen.events.map.BaseEventDetailFragment;
import com.tangle.utils.ConvertingUtils;
import com.tangle.utils.Patterns;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.List;

public class InviteDetailsFragment extends BaseEventDetailFragment implements InviteDetailsView {
    private InviteDetailsPresenter inviteDetailsPresenter = new InviteDetailsPresenter();

    private static final String BUNDLE_EVENT_ID_KEY = "event_id_key";
    private static final String BUNDLE_USER_ID_KEY = "user_id_key";
    private static final String BUNDLE_BASE_INVITE_RESPONSE_KEY = "base_invite_response_key";

    private ImageView imgViewLeftProfilePhoto, imgViewRightProfilePhoto;
    private TextView txtInviteStatus, txtTitle, txtEventName, txtEventDate, txtEventAddress,
            ticketCountTV, txtPeopleAreGoingAmount, txtDescription, txtPrice, txtStatus, txtClickedStatus;
    private MultipleIndicatorView eventMediaView;
    private LinearLayout layoutPeopleAreGoingHeader;
    private RecyclerView rViewPeopleAreGoing;
    private ViewFlipper vfBtnContainer;
    private View vWithDraw, vWithDrawBackground;

    private MediaPagerAdapter mediaPagerAdapter;
    private ProfilesAdapter participantsAdapter;

    public static InviteDetailsFragment newInstance(String eventId,
                                                    String userId,
                                                    String inviteId) {
        Bundle args = new Bundle();
        args.putString(BUNDLE_EVENT_ID_KEY, eventId);
        args.putString(BUNDLE_USER_ID_KEY, userId);
        args.putString(BUNDLE_BASE_INVITE_RESPONSE_KEY, inviteId);
        InviteDetailsFragment fragment = new InviteDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        inviteDetailsPresenter.setDialogManager(DialogManager.getInstance(this));
        inviteDetailsPresenter.setEventId(bundle.getString(BUNDLE_EVENT_ID_KEY));
        inviteDetailsPresenter.setUserId(bundle.getString(BUNDLE_USER_ID_KEY));
        inviteDetailsPresenter.setInviteId(bundle.getString(BUNDLE_BASE_INVITE_RESPONSE_KEY));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_invite_details, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        imgViewLeftProfilePhoto = view.findViewById(R.id.img_left_photo);
        imgViewRightProfilePhoto = view.findViewById(R.id.img_right_photo);
        txtInviteStatus = view.findViewById(R.id.txt_invite_status);
        txtTitle = view.findViewById(R.id.tvTitle);
        txtEventName = view.findViewById(R.id.txt_event_name);
        txtEventDate = view.findViewById(R.id.txt_date);
        txtEventAddress = view.findViewById(R.id.txt_address);
        ticketCountTV = view.findViewById(R.id.txt_ticket_count);
        txtDescription = view.findViewById(R.id.txt_descriptions);
        txtPrice = view.findViewById(R.id.tvPrice);
        txtStatus = view.findViewById(R.id.txt_status);

        eventMediaView = view.findViewById(R.id.invite_media_view);

        vfBtnContainer = view.findViewById(R.id.btn_container);
        txtClickedStatus = view.findViewById(R.id.txt_clicked_status);
        vWithDraw = view.findViewById(R.id.container_withdraw);
        vWithDrawBackground = view.findViewById(R.id.withdraw_background);
        view.findViewById(R.id.accept_btn).setOnClickListener(v -> inviteDetailsPresenter.onAcceptClicked());
        view.findViewById(R.id.refuse_btn).setOnClickListener(v -> inviteDetailsPresenter.onRefuseClicked());
        view.findViewById(R.id.btn_withdraw).setOnClickListener(v -> inviteDetailsPresenter.onClickReject());
        ((ShapeButton) view.findViewById(R.id.btn_withdraw)).setShapeEnabled(false);
        initPeopleAreGoingList(view);

    }

    private void initPeopleAreGoingList(View view) {
        layoutPeopleAreGoingHeader = view.findViewById(R.id.container_people_are_going);
        layoutPeopleAreGoingHeader.setOnClickListener(v -> inviteDetailsPresenter.onParticipantsClicked());
        txtPeopleAreGoingAmount = view.findViewById(R.id.txt_people_are_going_amount);
        rViewPeopleAreGoing = view.findViewById(R.id.rview_people_are_going);
        LinearLayoutManager layoutManager = new LinearLayoutManager(rViewPeopleAreGoing.getContext(),
                LinearLayoutManager.HORIZONTAL,
                false);
        rViewPeopleAreGoing.setLayoutManager(layoutManager);
        rViewPeopleAreGoing.setHasFixedSize(true);
        rViewPeopleAreGoing.setAdapter(new ProfilesAdapter(id -> {
            ApplicationLoader.getApplicationInstance().getNavigationManager().showUserProfile(id);
        }));
        participantsAdapter = (ProfilesAdapter) rViewPeopleAreGoing.getAdapter();
    }

    @Override
    public void setEventInfo(EventInfo event) {
        txtEventName.setText(event.title);
        txtEventDate.setText(String.format("%s, %s",
                Patterns.DATE_FORMAT_LONG.format(event.eventStartDate),
                Patterns.TIME_FORMAT.format(event.eventStartDate)));
        txtEventAddress.setText(event.getEventAddress(getContext()));
        if (event.hasTicket()) {
            ticketCountTV.setText(getString(R.string.event_tickets_left, event.getAllAvailableTickets()));
        } else {
            ticketCountTV.setText(getString(R.string.event_capacity, event.getAllTickets()));
        }
        if (!TextUtils.isEmpty(event.description)) {
            txtDescription.setVisibility(View.VISIBLE);
            Spanned spanned = Html.fromHtml(event.description);
            txtDescription.setText(ConvertingUtils.trimTrailingWhitespace(spanned));
        } else {
            txtDescription.setVisibility(View.GONE);
        }
        txtPrice.setText(getString(R.string.event_price, event.price));
        mediaPagerAdapter = new MediaPagerAdapter();
        mediaPagerAdapter.setEvent(event);
        eventMediaView.setPagerAdapter(mediaPagerAdapter);
        eventMediaView.setOffscreenPageLimit(event.getActualMediaList().size());
        eventMediaView.updateIndicator();
    }

    @Override
    public void setHeaderPhoto(ProfileData leftProfile, ProfileData rightProfile) {
        GlideApp.with(this)
                .load(leftProfile.primaryPhoto.avatar)
                .placeholder(RequestOptionsFactory.getNonAvatar(leftProfile))
                .apply(RequestOptions.circleCropTransform())
                .into(imgViewLeftProfilePhoto);
        GlideApp.with(this)
                .load(rightProfile.primaryPhoto.avatar)
                .placeholder(RequestOptionsFactory.getNonAvatar(rightProfile))
                .apply(RequestOptions.circleCropTransform())
                .into(imgViewRightProfilePhoto);
    }

    @Override
    public void setTitle(String title) {
        txtTitle.setVisibility(View.VISIBLE);
        txtTitle.setText(title);
    }

    @Override
    public void setInviteStatus(String status, int statusDrawable) {
        txtInviteStatus.setText(status);
        txtInviteStatus.setCompoundDrawablesWithIntrinsicBounds(statusDrawable, 0, 0, 0);
    }

    @Override
    public void setParticipants(List<ProfileData> profiles) {
        participantsAdapter.switchProfiles(profiles);
        txtPeopleAreGoingAmount.setText(Integer.toString(profiles.size()));
    }

    @Override
    public void showBtnContainer() {
        vfBtnContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void showBtnClickedStatus(String status) {
        Context context = vfBtnContainer.getContext();
        txtClickedStatus.setText(status);
        vfBtnContainer.setInAnimation(context, R.anim.slide_in_from_left);
        vfBtnContainer.setOutAnimation(context, R.anim.slide_out_to_right);
        vfBtnContainer.showNext();
    }

    @Override
    public void showAcceptPopup() {
        DialogManager.getInstance(this).showAcceptPopup((v, dialog) -> inviteDetailsPresenter.onPopupAcceptClicked());
    }

    @Override
    public void showRefusePopup() {
        DialogManager.getInstance(this).showRefusePopup((v, dialog) -> inviteDetailsPresenter.onPopupRefuseClicked());
    }

    @Override
    public void showRejectButton() {
        vWithDraw.setVisibility(View.VISIBLE);
        vWithDrawBackground.setVisibility(View.VISIBLE);
    }

    @Override
    public void goToMap(EventInfo event) {
        switchFragment(EventMapFragment.newInstance(event), true);
    }

    @Override
    protected void onMapClicked() {
        inviteDetailsPresenter.onMapClicked();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        inviteDetailsPresenter.onMapInitialized();
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return inviteDetailsPresenter;
    }
}