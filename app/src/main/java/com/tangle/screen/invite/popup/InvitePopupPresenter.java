package com.tangle.screen.invite.popup;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.api.Api;
import com.tangle.api.rpc.rpc_actions.invite.AddInviteAction;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.event.EventInfo;
import com.tangle.model.profile_data.ProfileData;

import io.reactivex.disposables.Disposable;

public class InvitePopupPresenter extends LifecyclePresenter<InvitePopupView>  {

    private InvitingPopupType invitingPopupType;
    private EventInfo invitingEvent;
    private ProfileData invitingUser;

    public void setInvitingPopupType(InvitingPopupType invitingPopupType) {
        this.invitingPopupType = invitingPopupType;
    }

    public InvitingPopupType getInvitingPopupType() {
        return invitingPopupType;
    }

    public void setInvitingEvent(EventInfo invitingEvent) {
        this.invitingEvent = invitingEvent;
    }

    public void setInvitingUser(ProfileData invitingUser) {
        this.invitingUser = invitingUser;
    }

    public void onJustInviteClick() {
        Disposable disposable = Api.getInst().getSocketManager().executeRPCAction(new AddInviteAction(invitingUser.id, invitingEvent.eventId))
                .subscribe(baseInviteRPCResponse -> getView().showFinishInviteFragment(invitingEvent, invitingUser), throwable -> throwable.printStackTrace());
        monitor(disposable, State.DESTROY_VIEW);
    }

    public void onBackPressed() {
        getView().goBack();
    }

    @Override
    public void attachToView(InvitePopupView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        view.loadUserImage(invitingUser);
        switch (invitingPopupType) {
            case JUST_INVITE:
                view.setJustInviteResultUI(invitingUser, invitingEvent);
                break;
            case HAVE_TICKET_INVITE:
                view.setJustTicketInviteUI(invitingUser, invitingEvent);
                break;
            case CHOOSE_INVITE_WAY:
                view.setChooseInviteWayUI(invitingUser, invitingEvent);
                break;
        }
    }
}