package com.tangle.screen.invite.result;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.event.EventInfo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.invite.can_invite.InvitingResultStatus;
import com.tangle.utils.RxUtils;

public class InviteResultPresenter extends LifecyclePresenter<InviteResultView> {
    private InvitingResultStatus invitingResultStatus;
    private EventInfo invitingEvent;
    private ProfileData invitingUser;

    public void setInvitingResultStatus(InvitingResultStatus invitingResultStatus) {
        this.invitingResultStatus = invitingResultStatus;
    }

    public void setInvitingEvent(EventInfo invitingEvent) {
        this.invitingEvent = invitingEvent;
    }

    public void setInvitingUser(ProfileData invitingUser) {
        this.invitingUser = invitingUser;
    }

    @Override
    public void attachToView(InviteResultView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().currentUser()
                .subscribe(this::setProfileData, RxUtils.getEmptyErrorConsumer()), State.DESTROY_VIEW);
        switch (invitingResultStatus) {
            case APPROVED:
                view.setInvitingResultApprovedUI(invitingUser, invitingEvent);
                break;
            case REFUSED:
                view.setInvitingResultRefusedUI(invitingUser, invitingEvent);
                break;
        }
    }

    private void setProfileData(ProfileData currentUser) {
        getView().loadAvatars(invitingUser, currentUser);
    }
}