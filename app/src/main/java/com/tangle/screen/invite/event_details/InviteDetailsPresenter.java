package com.tangle.screen.invite.event_details;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.analytics.SimpleActions;
import com.tangle.api.rpc.rpc_response.invite.InviteData;
import com.tangle.api.rx_tasks.payment.Respond;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.State;
import com.tangle.managers.DialogManager;
import com.tangle.managers.EventManager;
import com.tangle.model.event.EventInfo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.map.BaseEventPresenter;
import com.tangle.utils.RxUtils;
import com.tangle.utils.Triple;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class InviteDetailsPresenter extends BaseEventPresenter<InviteDetailsView> {
    private String userId;
    private String inviteId;
    private InviteData inviteData;
    private boolean isMyInvite;
    private DialogManager dialogManager;

    void setUserId(String userId) {
        this.userId = userId;
    }

    void setInviteId(String inviteId) {
        this.inviteId = inviteId;
    }

    @Override
    public void attachToView(InviteDetailsView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        loadInviteWithProfile();
        loadEventAndSetEventData();
    }

    private void loadInviteWithProfile() {
        ApplicationLoader instance = ApplicationLoader.getApplicationInstance();
        monitor(Observable.zip(instance.getUserManager().getUserGraphById(userId)
                , instance.getUserManager().currentUser()
                , instance.getInviteManager().getInviteById(inviteId)
                , Triple::create)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(triple -> {
                    setInviteData(triple.c);
                    setProfileData(triple.a, triple.b);
                }, error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
    }

    private void setInviteData(InviteData inviteData) {
        this.inviteData = inviteData;
        isMyInvite = ApplicationLoader.getApplicationInstance().getUserManager().isCurrentUser(inviteData.from);
    }

    private void setProfileData(ProfileData profile, ProfileData ownProfile) {
        if (isMyInvite) {
            getView().setHeaderPhoto(ownProfile, profile);
            switch (inviteData.status) {
                case InviteData.ACCEPT_INVITE_INDEX:
                    getView().setInviteStatus(getResources().getString(R.string.accepted), R.drawable.accepted_circle_indicator_drawable);
                    getView().setTitle(getResources().getString(R.string.formatted_accept_you_invite, profile.login));
                    break;
                case InviteData.REJECT_INVITE_INDEX:
                case InviteData.USER_REJECT_SENDING_INVITE_INDEX:
                    getView().setInviteStatus(getResources().getString(R.string.refused), R.drawable.refused_circle_indicator_drawable);
                    getView().setTitle(getResources().getString(R.string.formatted_refused_you_invite, profile.login));
                    break;
                default:
                    getView().setTitle(getResources().getString(R.string.invite_result_title_format_text, profile.login));
                    getView().showRejectButton();
                    //todo status of ticket?
//                    getView().showYourTicketStatus();
                    break;
            }
        } else {
            getView().setHeaderPhoto(profile, ownProfile);
            switch (inviteData.status) {
                case InviteData.ACCEPT_INVITE_INDEX:
                    getView().setInviteStatus(getResources().getString(R.string.accepted), R.drawable.accepted_circle_indicator_drawable);
                    getView().setTitle(getResources().getString(R.string.you_accepted_invite));
                    break;
                case InviteData.REJECT_INVITE_INDEX:
                case InviteData.USER_REJECT_SENDING_INVITE_INDEX:
                    getView().setInviteStatus(getResources().getString(R.string.refused), R.drawable.refused_circle_indicator_drawable);
                    getView().setTitle(getResources().getString(R.string.you_refused_invite));
                    break;
                default:
                    getView().showBtnContainer();
                    getView().setTitle(getResources().getString(R.string.formatted_invited_you_to_the_event, profile.login));
                    break;

            }
        }
    }

    public void chargeTicket(EventInfo event) {
        monitor(EventManager.chargeTicket(event, getView())
                        .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "chargeTicket"))
                , State.DESTROY_VIEW);
    }

    public void onClickReject() {
        if (event.isReserved()) {
            dialogManager.showConfirmTicket((v, dialog) -> {
                        chargeTicket(event);
                        rejectProcess();
                    }
                    , (v, dialog) -> rejectProcess());
        } else {
            rejectProcess();
        }
    }

    private void rejectProcess() {
        monitor(new Respond(inviteData, Respond.TICKET_STATUS_REJECT).getDataTask()
                        .compose(RxUtils.withLoading(getView()))
                        .subscribe(result -> getView().close()
                                , RxUtils.getEmptyErrorConsumer(TAG, "rejectProcess"))
                , State.DESTROY_VIEW);
    }

    private void loadEventAndSetEventData() {
        Disposable disposable = ApplicationLoader.getApplicationInstance().getEventManager().getEventByIdAndUpdate(eventId)
                .doOnNext(this::setEvent)
                .flatMap(eventInfo -> ApplicationLoader.getApplicationInstance().getUserManager().getUserByIds(event.participantsUsers))
                .subscribe(profiles -> getView().setParticipants(profiles), error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    public void onAcceptClicked() {
        ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.INVITES_CLICK_CONFIRM);
        getView().showAcceptPopup();
    }

    public void onPopupAcceptClicked() {
        monitor(new Respond(inviteData, Respond.TICKET_STATUS_ACCEPT).getDataTask()
                .compose(RxUtils.withLoading(getView()))
                .subscribe(result -> getView().showBtnClickedStatus(getResources().getString(R.string.accepted)),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
    }

    public void onRefuseClicked() {
        ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.INVITES_CLICK_REFUSE);
        getView().showRefusePopup();
    }

    public void onPopupRefuseClicked() {
        monitor(new Respond(inviteData, Respond.TICKET_STATUS_DECLINE).getDataTask()
                .compose(RxUtils.withLoading(getView()))
                .subscribe(result -> getView().showBtnClickedStatus(getResources().getString(R.string.refused)),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
    }

    public void onParticipantsClicked() {
    }

    public void onMapClicked() {
        getView().goToMap(event);
    }

    public void setDialogManager(DialogManager manager) {
        this.dialogManager = manager;
    }
}