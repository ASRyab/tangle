package com.tangle.screen.invite.popup;

public enum InvitingPopupType {
    JUST_INVITE,
    HAVE_TICKET_INVITE,
    CHOOSE_INVITE_WAY
}