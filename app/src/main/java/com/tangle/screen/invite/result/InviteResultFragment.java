package com.tangle.screen.invite.result;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.MVPFragment;
import com.tangle.base.ui.views.ShapeButton;
import com.tangle.base.ui.views.video_view.ScalableVideoView;
import com.tangle.model.event.EventInfo;
import com.tangle.model.profile_data.Photo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.invite.can_invite.InvitingResultStatus;
import com.tangle.utils.Patterns;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.io.IOException;

public class InviteResultFragment extends MVPFragment implements InviteResultView {
    private final static String EVENT_BUNDLE_KEY = "event_key";
    private final static String USER_BUNDLE_KEY = "user_key";
    private final static String INVITE_RESULT_TYPE_BUNDLE_KEY = "invite_result_type_key";

    private InviteResultPresenter presenter = new InviteResultPresenter();

    private ImageView imgViewYouPhoto,
            imgViewInvitedUserPhoto, imgViewPreview;
    private TextView txtViewTitle, txtViewSubTitle,
            txtViewEventName, txtViewEventDate, txtViewEventPlace;
    private FrameLayout videoContainerLayout, videoViewLayout;
    private ScalableVideoView videoView;
    private LinearLayout footerLayout;
    private ShapeButton btnInviteOtherUser;

    public static InviteResultFragment newInstance(EventInfo invitingEvent,
                                                   ProfileData invitingUser,
                                                   InvitingResultStatus invitingResultStatus) {
        InviteResultFragment invitePopupFragment = new InviteResultFragment();
        Bundle args = new Bundle();
        args.putSerializable(EVENT_BUNDLE_KEY, invitingEvent);
        args.putSerializable(USER_BUNDLE_KEY, invitingUser);
        args.putSerializable(INVITE_RESULT_TYPE_BUNDLE_KEY, invitingResultStatus);
        invitePopupFragment.setArguments(args);
        return invitePopupFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundleData(getArguments());
    }

    private void getBundleData(Bundle bundle) {
        if (bundle != null) {
            presenter.setInvitingEvent((EventInfo) bundle.getSerializable(EVENT_BUNDLE_KEY));
            presenter.setInvitingUser((ProfileData) bundle.getSerializable(USER_BUNDLE_KEY));
            presenter.setInvitingResultStatus((InvitingResultStatus) bundle.getSerializable(INVITE_RESULT_TYPE_BUNDLE_KEY));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(getLayoutId(), container, false);
    }

    protected int getLayoutId() {
        return R.layout.fragment_invite_result;
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);

        initUI(view);
    }

    private void initUI(View view) {
        imgViewYouPhoto = view.findViewById(R.id.img_you_photo);
        imgViewInvitedUserPhoto = view.findViewById(R.id.img_invite_user_photo);
        txtViewTitle = view.findViewById(R.id.title);
        txtViewSubTitle = view.findViewById(R.id.sub_title);
        txtViewEventName = view.findViewById(R.id.txt_view_event_name);
        txtViewEventDate = view.findViewById(R.id.txt_view_event_date);
        txtViewEventPlace = view.findViewById(R.id.txt_view_event_place);
        videoContainerLayout = view.findViewById(R.id.event_video_container);
        videoView = view.findViewById(R.id.view_video);
        imgViewPreview = view.findViewById(R.id.img_preview);
        videoViewLayout = view.findViewById(R.id.container_video);
        footerLayout = view.findViewById(R.id.footer);
        btnInviteOtherUser = view.findViewById(R.id.btn_invite_other_user);
    }

    @Override
    public void setInvitingResultApprovedUI(ProfileData invitingUser, EventInfo invitingEvent) {
        fillBasicData(invitingUser, invitingEvent);
        txtViewSubTitle.setText(String.format(getString(R.string.invite_result_sub_title_format_approved_text),
                invitingUser.login));
        try {
            startVideo(invitingEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void setInvitingResultRefusedUI(ProfileData invitingUser, EventInfo invitingEvent) {
       fillBasicData(invitingUser, invitingEvent);
        footerLayout.setVisibility(View.VISIBLE);
        txtViewSubTitle.setMovementMethod(LinkMovementMethod.getInstance());
        txtViewSubTitle.setText(initSpannableBody(getText(R.string.invite_result_sub_title_format_refused_text).toString(),
                invitingUser.login,
                getText(R.string.check_balance).toString()));
        try {
            startVideo(invitingEvent);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private SpannableString initSpannableBody(String body, String userName, String spanText){
        body = String.format(body, userName, spanText);
        int start = body.indexOf(spanText);
        int finish = start + spanText.length();
        SpannableString spannableBody = new SpannableString(body);
        spannableBody.setSpan(new ClickableSpan() {
            @Override
            public void onClick(final View view) {
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setColor(ContextCompat.getColor(ApplicationLoader.getContext(), R.color.colorPrimary));
                ds.setUnderlineText(false);
            }
        }, start, finish, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return spannableBody;
    }

    private void fillBasicData(ProfileData invitingUser, EventInfo invitingEvent) {
        txtViewTitle.setText(String.format(getString(R.string.invite_result_title_format_text),
                invitingUser.login));
        txtViewEventName.setText(invitingEvent.title);
        txtViewEventDate.setText(String.format("%s, %s",
                Patterns.DATE_FORMAT_LONG.format(invitingEvent.eventStartDate),
                Patterns.TIME_FORMAT.format(invitingEvent.eventStartDate)));
        txtViewEventPlace.setText(invitingEvent.getEventAddress(getContext()));
    }

    @Override
    public void loadAvatars(ProfileData invitingUser, ProfileData currentUser) {
        GlideApp.with(this)
                .load(currentUser)
                .placeholder(RequestOptionsFactory.getNonAvatar(currentUser))
                .apply(RequestOptions.circleCropTransform())
                .into(imgViewYouPhoto);
        Photo mainPhoto = invitingUser.primaryPhoto;
        GlideApp.with(this)
                .load(mainPhoto.avatar)
                .placeholder(RequestOptionsFactory.getNonAvatar(invitingUser))
                .apply(RequestOptions.circleCropTransform())
                .into(imgViewInvitedUserPhoto);
    }

    private void startVideo(EventInfo invitingEvent) throws IOException {
        imgViewPreview.setVisibility(View.VISIBLE);
        GlideApp.with(imgViewPreview)
                .load(invitingEvent.getActualMediaList().get(0).videoImagePreviewUrl)
                .into(imgViewPreview);
        videoView.setDataSource(invitingEvent.getActualMediaList().get(0).videoOriginUrl);
        videoView.setLooping(true);
        videoView.prepareAsync(mp -> {
            imgViewPreview.setVisibility(View.INVISIBLE);
            mp.setVolume(0, 0);
            mp.start();
        });
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}