package com.tangle.screen.invite.can_invite;

public enum InvitingResultStatus {
    APPROVED,
    REFUSED
}