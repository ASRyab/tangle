package com.tangle.screen.invite.popup;

import com.tangle.model.event.EventInfo;
import com.tangle.model.profile_data.ProfileData;

public interface InvitePopupView {
    void setJustInviteResultUI(ProfileData invitingUser, EventInfo invitingEvent);
    void setJustTicketInviteUI(ProfileData invitingUser, EventInfo invitingEvent);
    void setChooseInviteWayUI(ProfileData invitingUser, EventInfo invitingEvent);
    void loadUserImage(ProfileData invitingUser);
    void showFinishInviteFragment(EventInfo event, ProfileData inviteProfile);
    void goBack();
}