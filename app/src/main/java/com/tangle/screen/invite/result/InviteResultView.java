package com.tangle.screen.invite.result;

import com.tangle.model.event.EventInfo;
import com.tangle.model.profile_data.ProfileData;

public interface InviteResultView {
    void setInvitingResultApprovedUI(ProfileData invitingUser, EventInfo invitingEvent);
    void setInvitingResultRefusedUI(ProfileData invitingUser, EventInfo invitingEvent);
    void loadAvatars(ProfileData invitingUser, ProfileData currentUser);
}