package com.tangle.screen.invite.popup;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.views.ShapeButton;
import com.tangle.model.event.EventInfo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.Patterns;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import static com.tangle.screen.invite.popup.InvitingPopupType.JUST_INVITE;

// not used
public class InvitePopupFragment extends SecondaryScreenFragment implements InvitePopupView {
    private final static String EVENT_BUNDLE_KEY = "event_key";
    private final static String USER_BUNDLE_KEY = "user_key";
    private final static String TYPE_INVITE_BUNDLE_KEY = "type_invite_key";

    private InvitePopupPresenter presenter = new InvitePopupPresenter();

    private ImageView imgUserBackground, imgUserPhoto;
    private ShapeButton actionBtn;
    private TextView txtViewFooterInfo, txtViewEventPrice,
            txtViewEventPlace, txtViewEventDate, txtViewSubTitle, txtViewTitle;

    public static InvitePopupFragment newInstance(EventInfo invitingEvent,
                                                  ProfileData invitingUser, InvitingPopupType invitingPopupType) {
        InvitePopupFragment invitePopupFragment = new InvitePopupFragment();
        Bundle args = new Bundle();
        args.putSerializable(EVENT_BUNDLE_KEY, invitingEvent);
        args.putSerializable(USER_BUNDLE_KEY, invitingUser);
        args.putSerializable(TYPE_INVITE_BUNDLE_KEY, invitingPopupType);
        invitePopupFragment.setArguments(args);
        return invitePopupFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setData(getArguments());
    }

    private void setData(Bundle bundle) {
        if (bundle != null) {
            presenter.setInvitingEvent((EventInfo) bundle.getSerializable(EVENT_BUNDLE_KEY));
            presenter.setInvitingUser((ProfileData) bundle.getSerializable(USER_BUNDLE_KEY));
            presenter.setInvitingPopupType((InvitingPopupType) bundle.getSerializable(TYPE_INVITE_BUNDLE_KEY));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutId(), container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        actionBtn = view.findViewById(R.id.btn_invite_popup_fragment);
        txtViewFooterInfo = view.findViewById(R.id.txt_view_footer_info);
        txtViewEventPrice = view.findViewById(R.id.txt_view_event_price);
    }

    @Override
    public void setJustInviteResultUI(ProfileData invitingUser, EventInfo invitingEvent) {
        View view = getView();
        view.findViewById(R.id.btn_back).setVisibility(View.GONE);
        actionBtn.setText(getText(R.string.ok));
        actionBtn.setOnClickListener(v -> presenter.onBackPressed());
        txtViewFooterInfo.setText(getText(R.string.add_to_calendar));
        fillTimeAndPlaceView(view, invitingEvent);
        fillTitleAndSubTitle(view, String.format(getString(R.string.just_invite_popup_fragment_format_title),
                invitingUser.login),
                invitingEvent.title);
    }

    @Override
    public void setJustTicketInviteUI(ProfileData invitingUser, EventInfo invitingEvent) {
        View view = getView();
        actionBtn.setText(getText(R.string.gift));
        txtViewFooterInfo.setText(getText(R.string.just_invite));
        txtViewFooterInfo.setOnClickListener(v -> presenter.onJustInviteClick());
        txtViewEventPrice.setVisibility(View.VISIBLE);
        txtViewEventPrice.setText(getString(R.string.event_price, invitingEvent.price));
        fillTimeAndPlaceView(view, invitingEvent);
        fillTitleAndSubTitle(view, String.format(getString(R.string.have_ticket_invite_popup_fragment_format_title),
                invitingUser.login),
                invitingEvent.title);
    }

    @Override
    public void setChooseInviteWayUI(ProfileData invitingUser, EventInfo invitingEvent) {
        View view = getView();
        actionBtn.setText(getText(R.string.gift));
        txtViewFooterInfo.setText(getText(R.string.just_invite));
        txtViewFooterInfo.setOnClickListener(v -> presenter.onJustInviteClick());
        txtViewEventPrice.setVisibility(View.VISIBLE);
        txtViewEventPrice.setText(getString(R.string.event_price, invitingEvent.price));
        fillTitleAndSubTitle(view, String.format(getText(R.string.choose_invite_way_popup_fragment_invite_title).toString(),
                invitingUser.login),
                String.format(getText(R.string.choose_invite_way_popup_fragment_invite_subtitle).toString(),
                        invitingEvent.title,
                        String.format("%s, %s",
                                Patterns.DATE_FORMAT_LONG.format(invitingEvent.eventStartDate),
                                Patterns.TIME_FORMAT.format(invitingEvent.eventStartDate))));
    }

    private void fillTimeAndPlaceView(@NonNull View view, EventInfo invitingEvent) {
        txtViewEventPlace = view.findViewById(R.id.txt_view_event_place);
        txtViewEventPlace.setText(invitingEvent.getEventAddress(view.getContext()));
        txtViewEventDate = view.findViewById(R.id.txt_view_event_date);
        txtViewEventDate.setText(String.format("%s, %s",
                Patterns.DATE_FORMAT_LONG.format(invitingEvent.eventStartDate),
                Patterns.TIME_FORMAT.format(invitingEvent.eventStartDate)));
    }

    private void fillTitleAndSubTitle(@NonNull View view, String titleText, String subTitleText) {
        txtViewTitle = view.findViewById(R.id.txt_view_invite_title);
        txtViewTitle.setText(titleText);
        txtViewSubTitle = view.findViewById(R.id.txt_view_event_sub_title);
        txtViewSubTitle.setText(subTitleText);
    }

    @Override
    public void loadUserImage(ProfileData invitingUser) {
        View view = getView();
        imgUserBackground = view.findViewById(R.id.img_background);
        imgUserPhoto = view.findViewById(R.id.img_view_user_photo);
        String userPhotoUrl = invitingUser.primaryPhoto.avatar;
        GlideApp.with(this)
                .load(userPhotoUrl)
                .placeholder(RequestOptionsFactory.getNonAvatar(invitingUser))
                .apply(RequestOptions.circleCropTransform())
                .into(imgUserPhoto);
        GlideApp.with(this)
                .load(userPhotoUrl)
                .apply(RequestOptionsFactory.getDefaultBlurOptions(getActivity()))
                .into(imgUserBackground);
    }

    @Override
    public void showFinishInviteFragment(EventInfo event, ProfileData inviteProfile) {
        switchFragment(InvitePopupFragment.newInstance(event, inviteProfile, JUST_INVITE), true);
    }

    @Override
    public void goBack() {
        getActivity().onBackPressed();
    }

    private int getLayoutId() {
        int layoutId = R.layout.fragment_invite_popup_big_photo;
        switch (presenter.getInvitingPopupType()) {
            case CHOOSE_INVITE_WAY:
                layoutId = R.layout.fragment_invite_popup_small_photo;
                break;
        }
        return layoutId;
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}