package com.tangle.screen.invite.event_details;

import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.map.BaseEventDetailView;

import java.util.List;

public interface InviteDetailsView extends BaseEventDetailView, AutoCloseable {
    void setTitle(String title);

    void setInviteStatus(String status, int statusDrawable);

    void setParticipants(List<ProfileData> profiles);

    void setHeaderPhoto(ProfileData leftProfile, ProfileData rightProfile);

    void showBtnContainer();

    void showBtnClickedStatus(String status);

    void showAcceptPopup();

    void showRefusePopup();

    void showRejectButton();
}