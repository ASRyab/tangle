package com.tangle.screen.main;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.screen.events.main.MainEventsFragment;
import com.tangle.screen.likebook.list.LikebookFragment;

import java.lang.reflect.InvocationTargetException;

public class MenuFragmentAdapter extends FragmentPagerAdapter {
    private LikebookFragment likebookFragment;
    private MainEventsFragment eventsFragment;

    public MenuFragmentAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public SecondaryScreenFragment getItem(int position) {
        SecondaryScreenFragment fragment = null;
        switch (position) {
            case 1:
                fragment = eventsFragment = (MainEventsFragment) getFragment(eventsFragment, MainEventsFragment.class.getClassLoader(), MainEventsFragment.class.getName());
                break;
            case 0:
                fragment = likebookFragment = (LikebookFragment) getFragment(likebookFragment, LikebookFragment.class.getClassLoader(), LikebookFragment.class.getName());
                break;
        }

        return fragment;
    }

    @NonNull
    private SecondaryScreenFragment getFragment(SecondaryScreenFragment secondaryScreenFragment, ClassLoader classLoader, String fragmentClass) {
        SecondaryScreenFragment fragment = null;
        try {
            if (secondaryScreenFragment != null) {
                fragment = secondaryScreenFragment;
            } else {
                fragment = (SecondaryScreenFragment) Class.forName(fragmentClass).getConstructor().newInstance();
            }
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }
}