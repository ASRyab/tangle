package com.tangle.screen.main.navigation_menu;

import android.support.annotation.IntDef;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

@Retention(SOURCE)
@IntDef({MenuState.TAB_LIKE_OR_NOT_INDEX, MenuState.TAB_EVENT_INDEX})
public @interface NavigationMenuState {
}