package com.tangle.screen.main.navigation_menu;

public interface OnTabClickListener {
    void onTabClick(int tabIndex);
}