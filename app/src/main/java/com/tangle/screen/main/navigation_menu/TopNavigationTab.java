package com.tangle.screen.main.navigation_menu;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tangle.R;


public class TopNavigationTab extends LinearLayout {
    // View attr
    private int tabIndex;
    private String tabText;

    // View
    private RelativeLayout tabContainer;
    private TextView btnTabText;

    private OnTabClickListener onTabClickListener;
    private View dotNotification;

    public TopNavigationTab(Context context,
                            TopNavigationTabParam topNavigationTabParam,
                            OnTabClickListener onTabClickListener) {
        super(context);
        this.onTabClickListener = onTabClickListener;
        readAttrs(topNavigationTabParam);
        initLayout(context);
        initUI();
    }

    private void readAttrs(TopNavigationTabParam topNavigationTabParam) {
        tabIndex = topNavigationTabParam.getTabIndex();
        tabText = topNavigationTabParam.getTabText();
    }

    private void initLayout(Context context) {
        inflate(context, getLayoutId(), this);
    }

    private int getLayoutId() {
        return R.layout.layout_bottom_navigation_tab;
    }

    private void initUI() {
        tabContainer = findViewById(R.id.lnr_layout_tab_container);
        btnTabText = findViewById(R.id.btn_tab_text);
        dotNotification = findViewById(R.id.dot_notification);
        btnTabText.setText(tabText);
        btnTabText.requestLayout();
        tabContainer.setTag(true);
        tabContainer.setOnClickListener(v -> {
            setChecked(v);
            if (onTabClickListener != null) {
                onTabClickListener.onTabClick(getTabIndex());
            }
        });
        setChecked(tabContainer);
    }

    public void setVisibilityDot(int visiblity) {
        dotNotification.setVisibility(visiblity);
    }

    private void setChecked(View view) {
        if (!(boolean) view.getTag()) {
            checked(view);
        } else {
            unchecked(view);
        }
    }

    protected void checked(View view) {
        handelCheck(view, getContext().getResources().getColor(R.color.colorTextPrimary), true);
    }

    protected void unchecked(View view) {
        handelCheck(view, getContext().getResources().getColor(R.color.colorSecondary), false);
    }

    private void handelCheck(View view, int resId, boolean tag) {
        btnTabText.setTextColor(resId);
        view.setTag(tag);
    }

    protected int getTabIndex() {
        return tabIndex;
    }
}