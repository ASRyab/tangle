package com.tangle.screen.main.navigation_menu;

import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.tangle.ApplicationLoader;

import java.util.ArrayList;

public class TopNavigationMenuHandler implements OnTabClickListener {

    private ArrayList<TopNavigationTab> navigationTabs;
    private ViewGroup rootLayout;

    private OnTabClickListener onTabClickListener;

    public TopNavigationMenuHandler(ViewGroup rootLayout) {
        subscribeRepositoty();
        this.navigationTabs = new ArrayList<>();
        this.rootLayout = rootLayout;
    }

    private void subscribeRepositoty() {
        ApplicationLoader applicationInstance = ApplicationLoader.getApplicationInstance();
    }

    public void setTabClickListener(OnTabClickListener onTabClickListener) {
        this.onTabClickListener = onTabClickListener;
    }

    public void initTabs(TopNavigationTabParam... topNavigationTabParams) {
        for (TopNavigationTabParam topNavigationTabParam : topNavigationTabParams) {
            TopNavigationTab topNavigationTab
                    = new TopNavigationTab(rootLayout.getContext(),
                    topNavigationTabParam,
                    this);
            navigationTabs.add(topNavigationTab);
            rootLayout.addView(topNavigationTab,
                    new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.MATCH_PARENT, 1f));
        }
    }

    public void setDotNotification(int tab, int visibility) {
        navigationTabs.get(tab).setVisibilityDot(visibility);
    }

    private void handelClick(int tabIndex) {
        for (TopNavigationTab topNavigationTab : navigationTabs) {
            if (topNavigationTab.getTabIndex() != tabIndex) {
                topNavigationTab.unchecked(topNavigationTab);
            } else {
                topNavigationTab.checked(topNavigationTab);
            }
        }
    }

    public void setSelectedTab(int tabIndex) {
        handelClick(tabIndex);
    }

    @Override
    public void onTabClick(int tabIndex) {
        handelClick(tabIndex);
        if (onTabClickListener != null) {
            onTabClickListener.onTabClick(tabIndex);
        }
    }
}