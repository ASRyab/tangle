package com.tangle.screen.main;

import com.tangle.base.ScreenMover;
import com.tangle.model.payment.PaymentData;

public interface ActivityHolder extends ScreenMover {
    void moveToMenu(int index, ScreenJump screenJump);

    void openPP(PaymentData data);
}
