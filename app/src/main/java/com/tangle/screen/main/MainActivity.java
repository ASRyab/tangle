package com.tangle.screen.main;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.Api;
import com.tangle.base.PrePaymentPresenter;
import com.tangle.base.PrePaymentView;
import com.tangle.base.SecondaryScreenFragmentOperations;
import com.tangle.base.ui.BaseActivity;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.managers.DialogManager;
import com.tangle.model.event.TicketStatusConstant;
import com.tangle.model.payment.PaymentData;
import com.tangle.screen.activities.main.ActivitiesFragment;
import com.tangle.screen.main.navigation_menu.MenuState;
import com.tangle.screen.main.navigation_menu.OnTabClickListener;
import com.tangle.screen.main.navigation_menu.TopNavigationMenuHandler;
import com.tangle.screen.main.navigation_menu.TopNavigationTabParam;
import com.tangle.screen.own_profile.OwnProfileFragment;
import com.tangle.screen.payment.PaymentActivity;
import com.tangle.screen.welcome.WelcomeFragment;
import com.tangle.utils.AnimationUtils;
import com.tangle.utils.RxUtils;

import io.reactivex.disposables.Disposable;

public class MainActivity extends BaseActivity implements SecondaryScreenFragmentOperations, OnTabClickListener, ActivityHolder {
    public static final String AFTER_REG = "after registration";
    private static final int REQUEST_PAYMENT_SCREEN = 103;

    public static final String EVENT_PAYMENT_SCREEN = "event_id";
    public static final String USER_PAYMENT_SCREEN = "user_id";

    private ViewPager viewPager;
    private MenuFragmentAdapter pageAdapter;
    private TopNavigationMenuHandler bottomNavigationMenuHandler;
    private ViewGroup topNavigationView;
    private boolean animating;
    private Handler fragmentsOpsHandler;
    private Disposable updateFreeEvent, photoStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ApplicationLoader.getApplicationInstance().getLikeManager().init();
        ApplicationLoader.getApplicationInstance().getInviteManager().init();
        ApplicationLoader.getApplicationInstance().getBlogInfoManager().init();
        ApplicationLoader.getApplicationInstance().getPrivateChatMsgsManager().init();

        fragmentsOpsHandler = new Handler();
        viewPager = findViewById(R.id.viewpager_container);
        pageAdapter = new MenuFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(pageAdapter);
        viewPager.setOffscreenPageLimit(pageAdapter.getCount() - 1);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                moveToMenu(position);
            }
        });
        initBtnTabBar();
        initTopButtons();
        checkWelcomeScreen();
    }

    @Override
    protected void onStart() {
        super.onStart();
        ApplicationLoader.getApplicationInstance().getUserManager().loadCurrentUser();
        ApplicationLoader.getApplicationInstance().getDataPollingManager().startPolling();
        if (!Api.getInst().getSocketManager().isConnected()) {
            Api.getInst().connectToWebSocket();
        }
        updateFreeEvent = ApplicationLoader.getApplicationInstance().getEventManager().updateFreeEvent()
                .subscribe(info -> {
                    if (info.getFreeTicketStatus() == TicketStatusConstant.APPROVED) {
                        DialogManager.showApproveFreeTicket(this, info);
                    } else if (info.getFreeTicketStatus() == TicketStatusConstant.DECLINED) {
                        DialogManager.showDeclineFreeTicket(this, info);
                    }
                }, RxUtils.getEmptyErrorConsumer(TAG, "updateFreeEvent"));

        photoStatus = ApplicationLoader.getApplicationInstance().getPhotoManager().getPhotoStatusObservable()
                .subscribe(status -> {
                    switch (status) {
                        case PHOTO_APPROVE_STATUS_APPROVED:
                            DialogManager.showPhotoApprove(this);
                            break;
                        case PHOTO_APPROVE_STATUS_WAITING_FOR_APPROVE:
                            DialogManager.showWaitPhotoApprove(this, null);
                            break;
                        case PHOTO_APPROVE_STATUS_DECLINED:
                            ApplicationLoader.getApplicationInstance().getNavigationManager().showPhotoUploadDecline(this);
                            break;
                    }
                }, RxUtils.getEmptyErrorConsumer(TAG, "getPhotoStatusObservable"));
    }

    @Override
    protected void onStop() {
        super.onStop();
        ApplicationLoader.getApplicationInstance().getDataPollingManager().stopPolling();
        if (!updateFreeEvent.isDisposed()) {
            updateFreeEvent.dispose();
            updateFreeEvent = null;
        }
        if (!photoStatus.isDisposed()) {
            photoStatus.dispose();
            photoStatus = null;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        fragmentsOpsHandler.removeCallbacksAndMessages(null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        ApplicationLoader.getApplicationInstance().getNotificationViewManager().initOnActivity(this);
        ApplicationLoader.getApplicationInstance().getNotificationViewManager().setActivityHolder(this);
    }

    private void checkWelcomeScreen() {
        boolean isAfterReg = getIntent().getBooleanExtra(AFTER_REG, false);
        if (isAfterReg && !ApplicationLoader.getApplicationInstance().getPreferenceManager().isWelcomeAccepted()) {
            addFragment(new WelcomeFragment(), true);
            ApplicationLoader.getApplicationInstance().getPreferenceManager().saveWelcomeAccepted();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        ApplicationLoader.getApplicationInstance().getNotificationViewManager().setActivityHolder(null);
    }

    private void onBackPressedEmptyBackStack() {
        finish();
    }

    private void popChild(SecondaryScreenFragment currentFragment, FragmentManager childFragmentManager) {
        if (childFragmentManager.getBackStackEntryCount() > 0
                && currentFragment instanceof AnimationUtils.Dismissible) {
            if (!animating) {
                animating = true;
                ((AnimationUtils.Dismissible) currentFragment).dismiss(() -> fragmentsOpsHandler.post(() -> {
                    animating = false;
                    childFragmentManager.popBackStackImmediate();
                }));
            }
        } else {
            fragmentsOpsHandler.post(childFragmentManager::popBackStackImmediate);
        }
    }

    @Override
    public void onBackPressed() {
        SecondaryScreenFragment currentFragment = (SecondaryScreenFragment) getSecondaryFragment();
        if (fragmentManager.getBackStackEntryCount() == 0) {
            onBackPressedEmptyBackStack();
        } else {
            if (!currentFragment.onBackPressed()) {
                popChild(currentFragment, fragmentManager);
            }
        }
    }

    @Override
    public void clearStack() {
        fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    private void initBtnTabBar() {
        topNavigationView = findViewById(R.id.btn_navigation_bar);
        bottomNavigationMenuHandler = new TopNavigationMenuHandler(topNavigationView);
        bottomNavigationMenuHandler.setTabClickListener(this);
        bottomNavigationMenuHandler.initTabs(
                TopNavigationTabParam.newBuilder()
                        .setTabIndex(MenuState.TAB_LIKE_OR_NOT_INDEX)
                        .setTabText(getString(R.string.people))
                        .build(),
                TopNavigationTabParam.newBuilder()
                        .setTabIndex(MenuState.TAB_EVENT_INDEX)
                        .setTabText(getString(R.string.events))
                        .build()
        );
        moveToMenu(MenuState.TAB_EVENT_INDEX);
    }

    private void initTopButtons() {
        findViewById(R.id.btn_communication).setOnClickListener(v -> switchFragment(new ActivitiesFragment(), true));
        findViewById(R.id.btn_profile).setOnClickListener(v -> switchFragment(new OwnProfileFragment(), true));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        SecondaryScreenFragment fragment = (SecondaryScreenFragment) getSecondaryFragment();
        if (fragment == null) return;
        if (fragment instanceof PrePaymentView) {
            PrePaymentPresenter presenter = ((PrePaymentView) fragment).getPrePaymentPresenter();
            if (requestCode == REQUEST_PAYMENT_SCREEN && data != null) {
                String eventId = data.getStringExtra(EVENT_PAYMENT_SCREEN);
                String userId = data.getStringExtra(USER_PAYMENT_SCREEN);
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        presenter.onPPSuccess(eventId, userId);
                        break;
                    case Activity.RESULT_CANCELED:
                        presenter.onPPCanceled();
                        break;
                    default:
                        break;
                }
            }
        }
    }

    @Override
    public void openPP(PaymentData data) {
        Intent intent = new Intent(this, PaymentActivity.class);
        PaymentActivity.setExtras(intent, data);
        startActivityForResult(intent, REQUEST_PAYMENT_SCREEN);
    }

    @Override
    public void moveToMenu(int index) {
        bottomNavigationMenuHandler.setSelectedTab(index);
        onTabClick(index);
    }

    @Override
    public void moveToMenu(int index, ScreenJump screenJump) {
        moveToMenu(index);
        MenuFragmentAdapter adapter = (MenuFragmentAdapter) viewPager.getAdapter();
        adapter.getItem(index).getPresenter().onNotificationAction(screenJump);
    }

    @Override
    public void moveToScreen(SecondaryScreenFragment fragment, ScreenJump screenJump) {
        addFragment(fragment, true);
        fragment.getPresenter().onNotificationAction(screenJump);
    }

    @Override
    public void moveToScreen(SecondaryScreenFragment fragment) {
        moveToScreen(fragment, null);
    }


    @Override
    public void onTabClick(int tabIndex) {
        viewPager.setCurrentItem(tabIndex);
    }
}
