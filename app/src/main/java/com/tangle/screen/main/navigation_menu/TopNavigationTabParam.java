package com.tangle.screen.main.navigation_menu;


public class TopNavigationTabParam {
    // Param with default values
    private int tabIndex = 0;
    private String tabText;

    private TopNavigationTabParam() {
    }

    int getTabIndex() {
        return tabIndex;
    }

    public String getTabText() {
        return tabText;
    }

    public static Builder newBuilder() {
        return new TopNavigationTabParam().new Builder();
    }

    public class Builder {

        private Builder() {
        }

        public Builder setTabIndex(int tabIndex) {
            TopNavigationTabParam.this.tabIndex = tabIndex;
            return this;
        }

        public Builder setTabText(String tabText) {
            TopNavigationTabParam.this.tabText = tabText;
            return this;
        }

        public TopNavigationTabParam build() {
            return TopNavigationTabParam.this;
        }
    }
}