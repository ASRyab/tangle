package com.tangle.screen.other_profile;

import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.ui.views.ShapeButton;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.screen.events.EventActionButtonClickListener;
import com.tangle.screen.events.EventClickListener;
import com.tangle.utils.EventUtils;
import com.tangle.utils.Patterns;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GoingToEventsAdapter extends RecyclerView.Adapter<GoingToEventsAdapter.EventViewHolder> {
    private final boolean isOwn;
    private EventClickListener eventClickListener;
    private EventActionButtonClickListener eventActionButtonClickListener;
    private List<EventInfo> events = new ArrayList<>();

    public GoingToEventsAdapter(EventClickListener eventClickListener,
                                EventActionButtonClickListener eventActionButtonClickListener,
                                List<EventInfo> events, boolean isOwn) {
        this.eventClickListener = eventClickListener;
        this.eventActionButtonClickListener = eventActionButtonClickListener;
        this.events = events;
        this.isOwn = isOwn;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EventViewHolder(LayoutInflater.from(parent.getContext()).inflate(getLayoutId(), parent, false));
    }

    public int getLayoutId() {
        return R.layout.item_other_profile_going_to_event;
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        holder.bind(events.get(position));
    }

    @Override
    public int getItemCount() {
        return events == null ? 0 : events.size();
    }

    class EventViewHolder extends RecyclerView.ViewHolder {
        private EventInfo eventInfo;

        private TextView txtEventName, tvTicketStatus,
                txtEventDate, txtPeopleWillGoCounter;
        private ImageView imgEvent;
        private ShapeButton btnBook;

        private EventViewHolder(View itemView) {
            super(itemView);
            imgEvent = itemView.findViewById(R.id.ivEvent);
            txtEventName = itemView.findViewById(R.id.txt_event_title);
            txtEventDate = itemView.findViewById(R.id.txt_event_date);
            txtPeopleWillGoCounter = itemView.findViewById(R.id.txt_will_go_people_counter);
            tvTicketStatus = itemView.findViewById(R.id.txt_status);
            btnBook = itemView.findViewById(R.id.btn_book);
        }

        private void bind(EventInfo event) {
            this.eventInfo = event;
            if (isOwn) {
                btnBook.setText((event.isReserved())
                        ? btnBook.getContext().getString(R.string.confirm)
                        : btnBook.getContext().getString(R.string.show_ticket));
                btnBook.setEnabled(true);
            } else {
                btnBook.setText(eventInfo.getBookButtonText(btnBook.getContext()));
                btnBook.setEnabled(eventInfo.isBookAvailable());
            }
            btnBook.setOnClickListener(v -> {
                if (eventActionButtonClickListener != null) {
                    eventActionButtonClickListener.onBookClicked(eventInfo);
                }
            });
            itemView.setOnClickListener(v -> {
                if (eventClickListener != null) {
                    eventClickListener.onEventClicked(eventInfo);
                }
            });
            txtEventName.setText(event.title);
            txtPeopleWillGoCounter.setText(String.format(txtPeopleWillGoCounter.getResources().getString(R.string.events_participant),
                    event.participantsUsers.size()));

            EventUtils.updateEventStatus(tvTicketStatus, event);
            setDate();
            setPicture();
        }

        private void setPicture() {
            List<Media> actualMediaList = eventInfo.getActualMediaList();
            if (actualMediaList != null && !actualMediaList.isEmpty()) {
                Media media = eventInfo.getActualMediaList().get(0);
                GlideApp.with(imgEvent)
                        .load(media.type == Media.Type.PHOTO ? media.photoThumbnail2xUrl : media.videoImagePreviewUrl)
                        .apply(RequestOptionsFactory.getSmallRoundedCornersOptions(itemView.getContext()))
                        .into(imgEvent);
            }
        }

        private void setDate() {
            Date launchDate = eventInfo.eventStartDate;
            String date;
            if (DateUtils.isToday(launchDate.getTime())) {
                date = txtEventDate.getResources().getString(R.string.own_profile_today);
            } else {
                date = String.format("%s", Patterns.DATE_FORMAT_SHORT.format(launchDate));
            }
            String time = Patterns.TIME_FORMAT.format(launchDate);
            txtEventDate.setText(txtEventDate.getResources().getString(R.string.two_string_template, date, time));
        }
    }
}