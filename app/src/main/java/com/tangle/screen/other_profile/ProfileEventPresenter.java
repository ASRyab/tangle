package com.tangle.screen.other_profile;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.analytics.Via;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.managers.PhotoManager;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.TypeEvent;
import com.tangle.model.pojos.Gender;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.choose.ShowedEventsType;
import com.tangle.utils.RxUtils;

import java.util.Collections;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class ProfileEventPresenter extends LifecyclePresenter<ProfileEventView> {
    public final static String GOING_FROM_EVENT_ID_KEY = "going_from_event_id";
    public final static String GOING_FROM_KEY = "going_from";

    public enum GoingFrom {
        YOU_CAN_INVITE,
        PARTICIPANTS,
        OTHER
    }

    private String goingFromEventId;
    private GoingFrom goingFrom;
    private ProfileData profile;

    void setGoingFromEventId(String goingFromEventId) {
        this.goingFromEventId = goingFromEventId;
    }

    void setGoingFrom(GoingFrom goingFrom) {
        this.goingFrom = goingFrom;
    }

    void setProfileData(ProfileData profile) {
        this.profile = profile;
        loadEvents();
        getView().showGoingToGenderTitle(getFormattedGoingToGenderTitle(profile.gender));
    }

    private void loadEvents() {
        loadUserGoingToEvents(profile.futureEventsList);
        loadUserUpcomingEvents();
    }

    private void loadUserGoingToEvents(List<String> eventIds) {
        switch (goingFrom) {
            case OTHER:
            case YOU_CAN_INVITE:
                if (eventIds != null) {
                    Disposable disposable = ApplicationLoader.getApplicationInstance().getEventManager().getEventsByIdsActual(eventIds)
                            .observeOn(AndroidSchedulers.mainThread())
                            .filter(events -> !events.isEmpty())
                            .subscribe(events -> {
                                        if (events.size() == 1) {
                                            getView().showSingleGoingToEvent(events.get(0), this.goingFrom.equals(GoingFrom.YOU_CAN_INVITE));
                                        } else {
                                            getView().showListGoingToEvent(events, this.goingFrom.equals(GoingFrom.YOU_CAN_INVITE));
                                        }
                                    },
                                    error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
                    monitor(disposable, State.DESTROY_VIEW);
                }
                break;
            case PARTICIPANTS:
                monitor(ApplicationLoader.getApplicationInstance().getEventManager().getEventById(goingFromEventId, TypeEvent.DEFAULT)
                        .subscribe(eventInfo -> getView().showSingleGoingToEvent(eventInfo, false), RxUtils.getEmptyErrorConsumer(TAG, "loadUserGoingToEvents : " + goingFrom)), State.DESTROY_VIEW);
                break;


        }

    }

    private void loadUserUpcomingEvents() {
        switch (goingFrom) {
            case YOU_CAN_INVITE:
                monitor(ApplicationLoader.getApplicationInstance().getEventManager().getEventById(goingFromEventId, TypeEvent.DEFAULT)
                                .subscribe(eventInfo -> getView().showUpcomingEvents(Collections.singletonList(eventInfo)), RxUtils.getEmptyErrorConsumer(TAG, "loadUserUpcomingEvents : " + goingFrom))
                        , State.DESTROY_VIEW);
                break;
            case PARTICIPANTS:
                break;
            case OTHER:
                monitor(ApplicationLoader.getApplicationInstance().getEventManager().getEventsByIdsActual(profile.eventsForInvite)
                                .subscribe(eventInfo -> getView().showUpcomingEvents(eventInfo)
                                        , RxUtils.getEmptyErrorConsumer(TAG, "loadUserUpcomingEvents : " + goingFrom))
                        , State.DESTROY_VIEW);
        }

    }

    private String getFormattedGoingToGenderTitle(Gender gender) {
        switch (gender) {
            case MALE:
                return getResources().getString(R.string.going_to_male);
            case FEMALE:
                return getResources().getString(R.string.going_to_female);
        }
        return "";
    }

    public void onUserGoingToHeaderClick() {
        getView().showChooseEventFragment(profile.id, ShowedEventsType.USER_WILL_GO);
    }

    public void onUpcomingEventsHeaderClick() {
        getView().showChooseEventFragment(profile.id, ShowedEventsType.USER_UPCOMING_EVENT);
    }

    public void onBookClicked(EventInfo event) {
        ApplicationLoader.getApplicationInstance().getAnalyticManager().setVia(Via.PROFILE_BOOK);
        getView().getPrePaymentPresenter().onBookClicked(event);
    }

    public void onInviteClicked(EventInfo event) {
        PhotoManager photoManager = ApplicationLoader.getApplicationInstance().getPhotoManager();
        monitor(photoManager
                        .checkPhoto(getView())
                        .subscribe(data -> {
                            switch (data) {
                                case HAS_APPROVE_PHOTO:
                                    ApplicationLoader.getApplicationInstance().getAnalyticManager().setVia(Via.PROFILE_INVITE);
                                    getView().getPrePaymentPresenter().onInviteClicked(event, profile);
                                    break;
                                case HAS_PHOTO:
                                    getView().showNoApprovePhoto();
                                    break;
                            }
                        }, RxUtils.getEmptyErrorConsumer()),
                State.DESTROY);
    }
}