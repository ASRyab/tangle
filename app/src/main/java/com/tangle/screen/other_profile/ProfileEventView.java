package com.tangle.screen.other_profile;

import com.tangle.base.PrePaymentView;
import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.managers.PhotoManager;
import com.tangle.model.event.EventInfo;
import com.tangle.screen.events.choose.ShowedEventsType;

import java.util.List;

public interface ProfileEventView extends PrePaymentView, LoadingView, PhotoManager.PhotoView {
    void showSingleGoingToEvent(EventInfo event, boolean isReadOnly);

    void showGoingToGenderTitle(String title);

    void showListGoingToEvent(List<EventInfo> events, boolean isReadOnly);

    void showUpcomingEvents(List<EventInfo> events);

    void addUpcomingEvent(EventInfo event);

    void showChooseEventFragment(String userId, ShowedEventsType showedEventsType);
}