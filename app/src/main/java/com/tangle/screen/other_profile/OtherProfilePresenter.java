package com.tangle.screen.other_profile;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.rx_tasks.dictionary.GetCareersModel;
import com.tangle.api.rx_tasks.dictionary.GetIndustriesModel;
import com.tangle.api.rx_tasks.dictionary.GetInterestsModel;
import com.tangle.api.rx_tasks.like_or_not.AddUserLike;
import com.tangle.api.rx_tasks.like_or_not.SkipUser;
import com.tangle.api.rx_tasks.profile.BlockUser;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.managers.PhotoManager;
import com.tangle.model.pojos.DictionaryItem;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.RxUtils;

import java.util.Map;
import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class OtherProfilePresenter extends LifecyclePresenter<OtherProfileView> {
    private String userId;
    private int mediaPage;
    private ProfileData profileData;

    void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public void attachToView(OtherProfileView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        loadProfile();
    }

    private void loadProfile() {
        Disposable disposable = ApplicationLoader.getApplicationInstance().getUserManager().getUserGraphById(userId)
                .subscribe(profileData -> {
                    this.profileData = profileData;
                    getView().showActionBtnContainer(isNeedShowActionBtnContainer());
                    setProfileData(profileData);
                }, error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    private void setProfileData(ProfileData profile) {
        OtherProfileView view = getView();
        view.setUserGoingToData(profile);
        view.setLiked(profile.isLikedMeUser, profile.isMatchedUser);
        view.setMediaPhotos(profileData);
        view.setName(profile.login);
        setCareer(profile);
        Resources resources = getResources();
        view.setLocation(resources.getString(R.string.profile_basic_info_template, profile.age, profile.geo.city, profile.geo.country_code));
        view.setBackPhoto(profile.primaryPhoto.normal);
        view.setBlockVisibility(!profile.isBlocked());
        if (profile.interests != null) {
            setInterests(profile.interests);
        }
    }

    private void setInterests(Map<String, Boolean> interests) {
        Set<String> ids = interests.keySet();
        Disposable disposable = new GetInterestsModel().getTask()
                .flatMapIterable(list -> list)
                .filter(item -> ids.contains(Integer.toString(item.id)))
                .toList()
                .subscribe(getView()::setInterests, error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    private boolean isNeedShowActionBtnContainer() {
        return !profileData.isMatchedUser && !profileData.isYouLikedUser
                && !profileData.isYouSkippedUser
                && profileData.isUserCanBeLiked;
    }

    public void sendUserLike() {
        PhotoManager photoManager = ApplicationLoader.getApplicationInstance().getPhotoManager();
        monitor(photoManager
                        .checkPhoto(getView())
                        .subscribe(data -> {
                            switch (data) {
                                case HAS_PHOTO:
                                case HAS_APPROVE_PHOTO:
                                    doLike();
                                    break;
                            }
                        }, RxUtils.getEmptyErrorConsumer()),
                State.DESTROY);
    }

    private void doLike() {
        Disposable disposable = new AddUserLike(profileData.id).getDataTask()
                .doOnNext(response -> {
                    if (response.isNewMatch) {
                        ApplicationLoader.getApplicationInstance().getLikeManager().setMatched(profileData.id);
                    } else {
                        ApplicationLoader.getApplicationInstance().getLikeManager().setLiked(profileData.id);
                    }
                })
                .flatMap(response -> {
                    profileData.isYouLikedUser = true;
                    profileData.isMatchedUser = response.isNewMatch;
                    return ApplicationLoader.getApplicationInstance().getUserManager().addObserve(profileData);
                })
                .subscribe(profile -> {
                            getView().showActionBtnContainer(false);
                            if (profile.isMatchedUser) {
                                getView().showMutualLikePopup(profileData.id);
                            }
                        },
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    public void sendUserSkip() {
        Disposable disposable = new SkipUser(profileData.id).getDataTask()
                .flatMap(response -> {
                    profileData.isYouSkippedUser = true;
                    return ApplicationLoader.getApplicationInstance().getUserManager().addObserve(profileData);
                })
                .subscribe(profile -> getView().exitFromProfile(),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    private void setCareer(ProfileData profile) {
        Observable<DictionaryItem> industriesObservable = new GetIndustriesModel().getTask()
                .flatMapIterable(list -> list)
                .filter(industry -> profile.getIndustry().equals(Integer.toString(industry.id)));
        Observable<DictionaryItem> careersObservable = new GetCareersModel().getTask()
                .flatMapIterable(list -> list)
                .filter(career -> profile.getCareerLevel().equals(Integer.toString(career.id)));
        Observable<String> careerObservable = Observable.zip(industriesObservable, careersObservable,
                DictionaryItem.industryWithCareer());
        Disposable disposable = careerObservable.subscribe(
                careerInfo -> getView().setCareer(careerInfo),
                error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    public void onMediaPageSelected(int page) {
        this.mediaPage = page;
        safeScrollToPosition();
    }

    private void safeScrollToPosition() {
        getView().scrollMediaToPosition(mediaPage);
    }

    public void onBlockClicked() {
        getView().showBlockDialog();
    }

    public void approveBlock() {
        Disposable disposable = new BlockUser(profileData.id).getDataTask()
                .flatMap(response -> {
                    profileData.blockedUser = true;
                    return ApplicationLoader.getApplicationInstance().getUserManager().addObserve(profileData);
                })
                .doOnNext(data -> getView().setBlockVisibility(!profileData.isBlocked()))
                .doOnSubscribe(disposable1 -> getView().showLoading())
                .doOnEach(notification -> getView().hideLoading())
                .subscribe(profile -> getView().exitFromProfile(),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }
}