package com.tangle.screen.other_profile;

import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.ui.views.ShapeButton;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.screen.events.EventActionButtonClickListener;
import com.tangle.screen.events.EventClickListener;
import com.tangle.utils.EventUtils;
import com.tangle.utils.Patterns;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UpcomingEventsAdapter extends RecyclerView.Adapter<UpcomingEventsAdapter.EventViewHolder> {
    private EventClickListener eventClickListener;
    private EventActionButtonClickListener eventActionButtonClickListener;
    private List<EventInfo> events = new ArrayList<>();

    public UpcomingEventsAdapter(EventClickListener eventClickListener,
                                 EventActionButtonClickListener eventActionButtonClickListener,
                                 List<EventInfo> events) {
        this.eventClickListener = eventClickListener;
        this.eventActionButtonClickListener = eventActionButtonClickListener;
        this.events = events;
    }

    @Override
    public UpcomingEventsAdapter.EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new UpcomingEventsAdapter.EventViewHolder(LayoutInflater.from(parent.getContext()).inflate(getLayoutId(), parent, false));
    }

    public int getLayoutId() {
        return R.layout.item_other_profile_upcoming_event;
    }

    @Override
    public void onBindViewHolder(UpcomingEventsAdapter.EventViewHolder holder, int position) {
        holder.bind(events.get(position));
    }

    public void addEvent(EventInfo event) {
        events.add(event);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return events == null ? 0 : events.size();
    }

    class EventViewHolder extends RecyclerView.ViewHolder {
        private EventInfo eventInfo;

        private TextView txtEventName, txtEventDate, tvStatus;
        private ImageView imgEvent;
        private ShapeButton btnInvite;

        private EventViewHolder(View itemView) {
            super(itemView);
            imgEvent = itemView.findViewById(R.id.ivEvent);
            tvStatus = itemView.findViewById(R.id.txt_status);
            txtEventName = itemView.findViewById(R.id.txt_event_title);
            txtEventDate = itemView.findViewById(R.id.txt_event_date);
            btnInvite = itemView.findViewById(R.id.btn_invite);
            btnInvite.setOnClickListener(v -> {
                if (eventActionButtonClickListener != null) {
                    eventActionButtonClickListener.onInviteClicked(eventInfo);
                }
            });
            itemView.setOnClickListener(v -> {
                if (eventClickListener != null) {
                    eventClickListener.onEventClicked(eventInfo);
                }
            });
        }

        private void bind(EventInfo event) {
            this.eventInfo = event;
            txtEventName.setText(event.title);
            EventUtils.updateEventStatus(tvStatus, event);

            setDate();
            setPicture();
        }

        private void setPicture() {
            List<Media> actualMediaList = eventInfo.getActualMediaList();
            if (actualMediaList != null && !actualMediaList.isEmpty()) {
                Media media = eventInfo.getActualMediaList().get(0);
                GlideApp.with(imgEvent)
                        .load(media.type == Media.Type.PHOTO ? media.photoThumbnail2xUrl : media.videoImagePreviewUrl)
                        .apply(RequestOptionsFactory.getSmallRoundedCornersOptions(itemView.getContext()))
                        .into(imgEvent);
            }
        }

        private void setDate() {
            Date launchDate = eventInfo.eventStartDate;
            String date;
            if (DateUtils.isToday(launchDate.getTime())) {
                date = txtEventDate.getResources().getString(R.string.own_profile_today);
            } else {
                date = String.format("%s", Patterns.DATE_FORMAT_SHORT.format(launchDate));
            }
            String time = Patterns.TIME_FORMAT.format(launchDate);
            txtEventDate.setText(txtEventDate.getResources().getString(R.string.two_string_template, date, time));
        }
    }
}