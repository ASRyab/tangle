package com.tangle.screen.other_profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.tangle.R;
import com.tangle.base.PrePaymentPresenter;
import com.tangle.base.implementation.adapters.InterestsAdapter;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.recyclerview.layout_managers.flex.FlexDirection;
import com.tangle.base.ui.recyclerview.layout_managers.flex.FlexWrap;
import com.tangle.base.ui.recyclerview.layout_managers.flex.FlexboxLayoutManager;
import com.tangle.base.ui.recyclerview.layout_managers.flex.JustifyContent;
import com.tangle.base.ui.views.MultipleIndicatorView;
import com.tangle.base.ui.views.ShapeButton;
import com.tangle.managers.DialogManager;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.model.payment.PaymentData;
import com.tangle.model.pojos.DictionaryItem;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.EventActionButtonClickListener;
import com.tangle.screen.events.choose.ChooseEventFragment;
import com.tangle.screen.events.choose.ShowedEventsType;
import com.tangle.screen.events.details.EventDetailsFragment;
import com.tangle.screen.events.details.EventDetailsPresenter;
import com.tangle.screen.events.popup.CongratsFragment;
import com.tangle.screen.events.popup.CongratsViewType;
import com.tangle.screen.likebook.mutual_like.MutualLikePopupFragment;
import com.tangle.screen.main.ActivityHolder;
import com.tangle.screen.media.pager.MediaContainerFragment;
import com.tangle.utils.EventUtils;
import com.tangle.utils.Patterns;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class OtherProfileFragment extends SecondaryScreenFragment
        implements OtherProfileView, MediaContainerFragment.MediaContainerCallback, ProfileEventView {
    private final static String USER_ID_KEY = "user_id";
    private OtherProfilePresenter presenter = new OtherProfilePresenter();
    private ProfileEventPresenter profileEventPresenter = new ProfileEventPresenter();
    private PrePaymentPresenter prePaymentPresenter = new PrePaymentPresenter(DialogManager.getInstance(this));

    private ImageView imgBackground, imgMatch;
    private TextView txtUserName, txtUserInfoBasic, txtUserSpeciality, tvTicketStatus;
    private MultipleIndicatorView multipleIndicatorView;
    private RelativeLayout layoutActionBtn;
    private View additionalPadding;
    private LinearLayout containerUserIsGoingSingle,
            containerUserIsGoingList, containerUpcomingEvents;
    private ImageView btnBlock;
    private InterestsAdapter interestsAdapter;
    private View containerInterests;
    private RecyclerView interestsRecyclerView;

    public static OtherProfileFragment newInstance(String userId) {
        return newInstance(userId, ProfileEventPresenter.GoingFrom.OTHER, null);
    }

    public static OtherProfileFragment newInstance(String userId, ProfileEventPresenter.GoingFrom goingFrom, String goingFromEventId) {
        OtherProfileFragment otherProfileFragment = new OtherProfileFragment();
        Bundle args = new Bundle();
        args.putString(USER_ID_KEY, userId);
        args.putString(ProfileEventPresenter.GOING_FROM_EVENT_ID_KEY, goingFromEventId);
        args.putSerializable(ProfileEventPresenter.GOING_FROM_KEY, goingFrom);
        otherProfileFragment.setArguments(args);
        return otherProfileFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getBundleData(getArguments());
    }

    private void getBundleData(Bundle bundle) {
        if (bundle != null) {
            presenter.setUserId(bundle.getString(USER_ID_KEY));
            profileEventPresenter.setGoingFromEventId(bundle.getString(ProfileEventPresenter.GOING_FROM_EVENT_ID_KEY));
            profileEventPresenter.setGoingFrom((ProfileEventPresenter.GoingFrom) bundle.getSerializable(ProfileEventPresenter.GOING_FROM_KEY));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_other_profile, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        imgBackground = view.findViewById(R.id.img_background);
        txtUserName = view.findViewById(R.id.txt_name);
        txtUserInfoBasic = view.findViewById(R.id.txt_info_basic);
        txtUserSpeciality = view.findViewById(R.id.txt_info_speciality);
        multipleIndicatorView = view.findViewById(R.id.view_multiple);
        imgMatch = view.findViewById(R.id.img_match);
        layoutActionBtn = view.findViewById(R.id.layout_btn_container);
        additionalPadding = view.findViewById(R.id.additional_padding);
        btnBlock = view.findViewById(R.id.iv_block);
        tvTicketStatus = view.findViewById(R.id.txt_status);
        initEventContainer(view);
        initInterests(view);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public List<LifecyclePresenter> getPresenters() {
        return Arrays.asList(prePaymentPresenter, profileEventPresenter);
    }

    @Override
    public void setName(String name) {
        txtUserName.setText(name);
    }

    @Override
    public void setLocation(String text) {
        txtUserInfoBasic.setVisibility(View.VISIBLE);
        txtUserInfoBasic.setText(text);
    }

    @Override
    public void setCareer(String text) {
        txtUserSpeciality.setVisibility(View.VISIBLE);
        txtUserSpeciality.setText(text);
    }

    @Override
    public void setBlockVisibility(boolean isShow) {
        btnBlock.setVisibility(isShow ? View.VISIBLE : View.INVISIBLE);
        if (isShow) {
            btnBlock.setOnClickListener(view -> presenter.onBlockClicked());
        }
    }

    @Override
    public void showBlockDialog() {
        DialogManager.getInstance(this).showBlockUserDialog((v, dialog) -> presenter.approveBlock());
    }

    @Override
    public void setBackPhoto(String url) {
        GlideApp.with(this)
                .load(url)
                .apply(RequestOptions.priorityOf(Priority.HIGH))
                .apply(RequestOptionsFactory.getDefaultBlurOptions(getActivity()))
                .into(imgBackground);
    }

    @Override
    public void setMediaPhotos(ProfileData profile) {
        UserMediaContentPagerAdapter pagerAdapter = new UserMediaContentPagerAdapter(true);
        pagerAdapter.setProfileData(profile);
        pagerAdapter.setProfileMediaClickListener((userId, photo) -> {
            if (profile.photos.isEmpty()) {
                return;
            }
            MediaContainerFragment profileMediaContainerFragment = MediaContainerFragment.newInstance(profile.photos,
                    photo, MediaContainerFragment.ShowType.USER_PHOTO);
            profileMediaContainerFragment.setTargetFragment(this, MediaContainerFragment.SET_MEDIA_INDEX_CODE);
            addFragment(profileMediaContainerFragment, true);
        });
        multipleIndicatorView.setPagerAdapter(pagerAdapter);
    }

    @Override
    public void setLiked(boolean liked, boolean matched) {
        imgMatch.setVisibility(liked || matched ? View.VISIBLE : View.INVISIBLE);
        imgMatch.setImageResource(matched ? R.drawable.ic_circle_double_match_icon : R.drawable.ic_circle_match_icon);
    }

    @Override
    public void onFullScreenPageSelected(int position) {
        presenter.onMediaPageSelected(position);
    }

    @Override
    public void scrollMediaToPosition(int position) {
        multipleIndicatorView.setCurrentItem(position);
    }

    @Override
    public void showMutualLikePopup(String userId) {
        addFragment(MutualLikePopupFragment.newInstance(userId), true);
    }

    @Override
    public void showChooseEventFragment(String userId, ShowedEventsType showedEventsType) {
        addFragment(ChooseEventFragment.newInstance(userId, showedEventsType, ChooseEventFragment.GoingFrom.OTHER), true);
    }

    private void initEventContainer(View view) {
        containerUserIsGoingSingle = view.findViewById(R.id.container_user_going_to_single);
        containerUserIsGoingList = view.findViewById(R.id.container_user_going_to_list);
        containerUpcomingEvents = view.findViewById(R.id.container_upcoming_events);
    }

    @Override
    public void setUserGoingToData(ProfileData profile) {
        profileEventPresenter.setProfileData(profile);
    }

    @Override
    public void showActionBtnContainer(boolean isNeedShowActionBtnContainer) {
        layoutActionBtn.setVisibility(isNeedShowActionBtnContainer ? View.VISIBLE : View.GONE);
        additionalPadding.setVisibility(isNeedShowActionBtnContainer ? View.VISIBLE : View.GONE);
        if (isNeedShowActionBtnContainer) {
            layoutActionBtn.findViewById(R.id.btn_skip).setOnClickListener(v -> presenter.sendUserSkip());
            layoutActionBtn.findViewById(R.id.btn_like).setOnClickListener(v -> presenter.sendUserLike());
        }
    }

    @Override
    public void exitFromProfile() {
        getActivity().onBackPressed();
    }

    @Override
    public void showGoingToGenderTitle(String title) {
        ((TextView) containerUserIsGoingSingle.findViewById(R.id.txt_view_going_to)).setText(title);
        ((TextView) containerUserIsGoingList.findViewById(R.id.txt_view_going_to)).setText(title);
    }

    @Override
    public void showSingleGoingToEvent(EventInfo event, boolean isReadOnly) {
        containerUserIsGoingSingle.setVisibility(View.VISIBLE);
        if (!isReadOnly) {
            containerUserIsGoingSingle.setOnClickListener(v -> addFragment(EventDetailsFragment.newInstance(event, EventDetailsPresenter.GoingFrom.GOING_TO), true));
        }
        ImageView eventImg = containerUserIsGoingSingle.findViewById(R.id.ivEvent);
        List<Media> actualMediaList = event.getActualMediaList();
        if (actualMediaList != null && !actualMediaList.isEmpty()) {
            Media media = event.getActualMediaList().get(0);
            GlideApp.with(eventImg)
                    .load(media.type == Media.Type.PHOTO ? media.photoThumbnail2xUrl : media.videoImagePreviewUrl)
                    .apply(RequestOptionsFactory.getSmallRoundedCornersOptions(eventImg.getContext()))
                    .into(eventImg);
        }
        ((TextView) containerUserIsGoingSingle.findViewById(R.id.txt_will_go_people_counter))
                .setText(String.format(getResources().getString(R.string.events_participant), event.participantsUsers.size()));
        TextView eventDate = containerUserIsGoingSingle.findViewById(R.id.txt_event_date);
        Date launchDate = event.eventStartDate;
        String date;
        if (DateUtils.isToday(launchDate.getTime())) {
            date = eventDate.getResources().getString(R.string.own_profile_today);
        } else {
            date = String.format("%s", Patterns.DATE_FORMAT_SHORT.format(launchDate));
        }
        String time = Patterns.TIME_FORMAT.format(launchDate);
        eventDate.setText(getResources().getString(R.string.two_string_template, date, time));
        ((TextView) containerUserIsGoingSingle.findViewById(R.id.txt_event_title)).setText(event.title);
        ShapeButton btnBook = containerUserIsGoingSingle.findViewById(R.id.btn_book);
        btnBook.setText(event.getBookButtonText(getContext()));
        btnBook.setEnabled(event.isBookAvailable());
        btnBook.setOnClickListener(v -> profileEventPresenter.onBookClicked(event));
        EventUtils.updateEventStatus(tvTicketStatus, event);
    }

    @Override
    public void showListGoingToEvent(List<EventInfo> events, boolean isReadOnly) {
        containerUserIsGoingList.setVisibility(View.VISIBLE);
        if (isReadOnly) {
            containerUserIsGoingList.findViewById(R.id.txt_will_going_events_count).setVisibility(View.INVISIBLE);
        } else {
            ((TextView) containerUserIsGoingList.findViewById(R.id.txt_will_going_events_count)).setText(String.valueOf(events.size()));
            containerUserIsGoingList.findViewById(R.id.layout_header_user_is_going)
                    .setOnClickListener(v -> profileEventPresenter.onUserGoingToHeaderClick());
        }
        RecyclerView goingToEventRView = containerUserIsGoingList.findViewById(R.id.list_events_user_going_go);
        LinearLayoutManager layoutManager = new LinearLayoutManager(goingToEventRView.getContext(),
                LinearLayoutManager.HORIZONTAL, false);
        goingToEventRView.setLayoutManager(layoutManager);
        goingToEventRView.setHasFixedSize(true);
        goingToEventRView.setAdapter(new GoingToEventsAdapter(isReadOnly ? null : event ->
                addFragment(EventDetailsFragment.newInstance(event, EventDetailsPresenter.GoingFrom.GOING_TO), true), new EventActionButtonClickListener() {
            @Override
            public void onBookClicked(EventInfo event) {
                profileEventPresenter.onBookClicked(event);
            }

            @Override
            public void onInviteClicked(EventInfo event) {
            }
        }, events, false));
    }

    @Override
    public void setInterests(List<DictionaryItem> interests) {
        if (!interests.isEmpty()) {
            containerInterests.setVisibility(View.VISIBLE);
        }
        interestsAdapter.switchInterests(interests);
        interestsAdapter.setSelectedInterests(interests);
    }


    private void initInterests(View view) {
        interestsRecyclerView = view.findViewById(R.id.list_interests);
        containerInterests = view.findViewById(R.id.container_interests);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getActivity(), FlexDirection.ROW, FlexWrap.WRAP);
        layoutManager.setJustifyContent(JustifyContent.CENTER);
        interestsRecyclerView.setLayoutManager(layoutManager);
        interestsAdapter = new InterestsAdapter();
        interestsAdapter.setSelectAvailable(false);
        interestsRecyclerView.setAdapter(interestsAdapter);
    }

    @Override
    public void showUpcomingEvents(List<EventInfo> events) {
        if (events.size() == 0) {
            return;
        }
        containerUpcomingEvents.setVisibility(View.VISIBLE);
        boolean hasMore = events.size() > 3;
        if (hasMore) {
            containerUpcomingEvents.findViewById(R.id.layout_header_upcoming_events)
                    .setOnClickListener(v -> profileEventPresenter.onUpcomingEventsHeaderClick());
            ((TextView) containerUpcomingEvents.findViewById(R.id.txt_upcoming_events_count)).setText(String.valueOf(events.size()));
        } else {
            containerUpcomingEvents.findViewById(R.id.txt_upcoming_events_count).setVisibility(View.INVISIBLE);
            containerUpcomingEvents.findViewById(R.id.img_will_going_event_arrow).setVisibility(View.INVISIBLE);
        }

        List<EventInfo> cutEvents = hasMore ? events.subList(0, 3) : events;

        RecyclerView upcomingEventsRView = containerUpcomingEvents.findViewById(R.id.list_upcoming_events);
        LinearLayoutManager layoutManager = new LinearLayoutManager(upcomingEventsRView.getContext(),
                LinearLayoutManager.VERTICAL, false);
        upcomingEventsRView.setLayoutManager(layoutManager);
        upcomingEventsRView.setHasFixedSize(true);
        upcomingEventsRView.setAdapter(new UpcomingEventsAdapter(event ->
                addFragment(EventDetailsFragment.newInstance(event, EventDetailsPresenter.GoingFrom.UPCOMING_EVENTS), true), new EventActionButtonClickListener() {
            @Override
            public void onBookClicked(EventInfo event) {
            }

            @Override
            public void onInviteClicked(EventInfo event) {
                profileEventPresenter.onInviteClicked(event);
            }
        }, cutEvents));
    }

    @Override
    public void addUpcomingEvent(EventInfo event) {
        UpcomingEventsAdapter upcomingEventsAdapter = (UpcomingEventsAdapter)
                ((RecyclerView) containerUpcomingEvents.findViewById(R.id.list_upcoming_events)).getAdapter();
        TextView upcomingEventsCount = containerUpcomingEvents.findViewById(R.id.txt_upcoming_events_count);
        upcomingEventsCount.setText(String.valueOf(Integer.valueOf(upcomingEventsCount.getText().toString()) + 1));
        if (upcomingEventsAdapter.getItemCount() < 3) {
            upcomingEventsAdapter.addEvent(event);
        }
    }

    @Override
    public void goToPP(PaymentData data) {
        ActivityHolder activity = (ActivityHolder) getActivity();
        activity.openPP(data);
    }

    @Override
    public void goToSuccess(String eventId, String userId, CongratsViewType type) {
        CongratsFragment fragment = CongratsFragment.newInstance(eventId, userId, type);
        addFragment(fragment, true);
    }

    @Override
    public PrePaymentPresenter getPrePaymentPresenter() {
        return prePaymentPresenter;
    }
}