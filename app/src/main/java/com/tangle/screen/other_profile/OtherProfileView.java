package com.tangle.screen.other_profile;

import com.tangle.base.PrePaymentView;
import com.tangle.managers.PhotoManager;
import com.tangle.model.pojos.DictionaryItem;
import com.tangle.model.profile_data.ProfileData;

import java.util.List;

public interface OtherProfileView extends PrePaymentView, PhotoManager.PhotoView {
    void setName(String name);

    void setLocation(String text);

    void setCareer(String text);

    void setBackPhoto(String url);

    void setBlockVisibility(boolean isShow);

    void setMediaPhotos(ProfileData profileData);

    void setLiked(boolean liked, boolean matched);

    void scrollMediaToPosition(int position);

    void showMutualLikePopup(String userId);

    void setUserGoingToData(ProfileData profile);

    void showActionBtnContainer(boolean isNeedShowActionBtnContainer);

    void exitFromProfile();

    void showBlockDialog();

    void setInterests(List<DictionaryItem> dictionaryItems);
}