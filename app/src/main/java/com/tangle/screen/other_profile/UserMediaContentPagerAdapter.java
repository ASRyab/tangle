package com.tangle.screen.other_profile;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.tangle.R;
import com.tangle.base.ui.views.LoaderView;
import com.tangle.model.profile_data.Photo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import io.reactivex.annotations.NonNull;

import static com.tangle.model.profile_data.PhotoKt.createEmpty;

public class UserMediaContentPagerAdapter extends PagerAdapter {
    private boolean isTransparent;
    private ProfileData profileData;
    private ProfileMediaClickListener profileMediaClickListener;

    public UserMediaContentPagerAdapter(boolean isTransparent) {
        this.isTransparent = isTransparent;
    }

    public UserMediaContentPagerAdapter() {
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View child = LayoutInflater.from(container.getContext()).inflate(R.layout.item_user_media_pager, container, false);
        container.addView(child);
        ImageView imageView = child.findViewById(R.id.img_preview);
        imageView.setBackgroundResource((isTransparent) ? R.color.colorBackgroundDarkTransparent : R.color.colorBackgroundDark);
        LoaderView loaderView = child.findViewById(R.id.view_loader);
        Photo photo = profileData.photo_count == 0 ? createEmpty() : profileData.photos.get(position);
        imageView.setOnClickListener(v -> {
            if (profileMediaClickListener != null) {
                profileMediaClickListener.onMediaClicked(profileData.id, photo);
            }
        });
        loaderView.setVisibility(View.VISIBLE);
        GlideApp.with(container)
                .load(photo.normal)
                .placeholder(RequestOptionsFactory.getNonAvatar(profileData))
                .apply(RequestOptionsFactory.getMediumRoundedCornersOptions(child.getContext()))
                .apply(RequestOptions.priorityOf(Priority.HIGH))
                .into(new DrawableImageViewTarget(imageView) {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        super.onResourceReady(resource, transition);
                        loaderView.setVisibility(View.INVISIBLE);
                    }

                    @Override
                    public void onLoadFailed(@Nullable Drawable errorDrawable) {
                        super.onLoadFailed(errorDrawable);
                        loaderView.setVisibility(View.INVISIBLE);
                    }
                });
        return child;
    }

    @Override
    public int getCount() {
        return (profileData == null || profileData.photo_count == 0) ? 1 : profileData.photo_count;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return object == view;
    }

    public ProfileData getProfile() {
        return profileData;
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

    public void setProfileMediaClickListener(ProfileMediaClickListener profileMediaClickListener) {
        this.profileMediaClickListener = profileMediaClickListener;
    }

    public void setProfileData(ProfileData profileData) {
        this.profileData = profileData;
        notifyDataSetChanged();
    }
}