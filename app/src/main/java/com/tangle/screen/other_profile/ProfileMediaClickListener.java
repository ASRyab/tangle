package com.tangle.screen.other_profile;

import com.tangle.model.profile_data.Photo;

public interface ProfileMediaClickListener {
    void onMediaClicked(String userId, Photo photo);
}