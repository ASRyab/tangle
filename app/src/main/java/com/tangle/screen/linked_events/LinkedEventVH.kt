package com.tangle.screen.linked_events

import android.text.TextUtils
import android.view.View
import android.widget.TextView
import com.tangle.R
import com.tangle.base.ui.views.MultipleIndicatorView
import com.tangle.model.event.EventInfo
import com.tangle.screen.events.EventClickListener
import com.tangle.screen.events.list.MediaPagerAdapter
import com.tangle.utils.Patterns


class LinkedEventVH(var itemView: View, var eventClickListener: EventClickListener?) {

    fun bind(bindEvent: EventInfo): View {

        val multipleView: MultipleIndicatorView = itemView.findViewById(R.id.view_multiple)
        val titleTextView: TextView = itemView.findViewById(R.id.tvTitle)
        val dateTextView: TextView = itemView.findViewById(R.id.txt_date)
        val addressTextView: TextView = itemView.findViewById(R.id.txt_address)
        val pagerAdapter = MediaPagerAdapter(true)

        pagerAdapter.event = bindEvent
        multipleView.setPagerAdapter(pagerAdapter)
        pagerAdapter.setMediaClickListener { _, _ -> eventClickListener?.onEventClicked(bindEvent) }
        itemView.setOnClickListener { eventClickListener?.onEventClicked(bindEvent) }
        multipleView.setCurrentItem(0)
        titleTextView.text = bindEvent.title
        dateTextView.text = String.format("%s, %s",
                Patterns.DATE_FORMAT_LONG.format(bindEvent.eventStartDate),
                Patterns.TIME_FORMAT.format(bindEvent.eventStartDate))
        val address = bindEvent.getEventAddress(itemView.context)
        if (!TextUtils.isEmpty(address)) {
            addressTextView.text = address
        }
        return itemView
    }

}