package com.tangle.screen.linked_events

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tangle.R
import com.tangle.model.event.EventInfo
import com.tangle.screen.events.EventClickListener


class LinkedEventsPagerAdapter(val context: Context) : PagerAdapter() {
    private var eventList: List<EventInfo> = ArrayList()
    private val MAX_LINKED_EVENTS_COUNT = 3

    var eventClickListener: EventClickListener? = null

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return if (eventList.size > MAX_LINKED_EVENTS_COUNT) MAX_LINKED_EVENTS_COUNT else eventList.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val event = eventList[position]
        val inflater = LayoutInflater.from(context)
        val layout = inflater.inflate(R.layout.item_linked_event_horizontal_view, container, false)
        LinkedEventVH(layout, eventClickListener).bind(event)
        container.addView(layout)

        return layout
    }


    override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
        container.removeView(view as View)
    }

    fun update(events: List<EventInfo>) {
        eventList = events
        notifyDataSetChanged()
    }
}
