package com.tangle.screen.linked_events

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.View
import com.tangle.utils.ConvertingUtils


class WrapContentHeightViewPager : ViewPager, ViewPager.PageTransformer {
    private var maxScale = 0.0f
    private var mPageMarginLeftRight: Int = 0
    private var mPageMarginTopBottom: Int = 0
    private var animationEnabled = true
    private val fadeFactor = 0.6f

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        // clipping should be off on the pager for its children so that they can scale out of bounds.
        clipChildren = false
        clipToPadding = false
        // to avoid fade effect at the end of the page
        overScrollMode = 2
        offscreenPageLimit = 3
        mPageMarginLeftRight = ConvertingUtils.convertDpToPixel(40f, context).toInt()
        mPageMarginTopBottom = ConvertingUtils.convertDpToPixel(24f, context).toInt()
        setPadding(mPageMarginLeftRight, mPageMarginTopBottom, mPageMarginLeftRight, mPageMarginTopBottom)
        setPageTransformer(false, this)
    }

    override fun setPageMargin(marginPixels: Int) {
        mPageMarginLeftRight = marginPixels
        setPadding(mPageMarginLeftRight, mPageMarginTopBottom, mPageMarginLeftRight, mPageMarginTopBottom);
    }

    // We override onMeasure to make ViewPage wrap its children content
    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var heightMeasureSpecif = heightMeasureSpec
        var height = 0
        for (i in 0 until childCount) {
            val child = getChildAt(i)
            child.measure(widthMeasureSpec, View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED))
            val h = child.measuredHeight
            if (h > height) height = h
        }

        heightMeasureSpecif = View.MeasureSpec.makeMeasureSpec(height, View.MeasureSpec.EXACTLY)

        super.onMeasure(widthMeasureSpec, heightMeasureSpecif)
    }

    override fun transformPage(page: View, position: Float) {
        if (mPageMarginLeftRight <= 0 || !animationEnabled)
            return
        page.setPadding(mPageMarginLeftRight / 2, mPageMarginLeftRight / 2, mPageMarginLeftRight / 2, mPageMarginLeftRight / 2);
        var mPos = position
        if (maxScale == 0.0f && mPos > 0.0f && mPos < 1.0f) {
            maxScale = mPos
        }
        mPos -= maxScale
        val absolutePosition = Math.abs(mPos)
        if (mPos <= -1.0f || mPos >= 1.0f) {
            page.alpha = fadeFactor
        } else if (mPos == 0.0f) {
            // Page is selected -- reset any views if necessary
            page.scaleX = (1 + maxScale)
            page.scaleY = (1 + maxScale)
            page.alpha = 1f
        } else {
            page.scaleX = 1 + maxScale * (1 - absolutePosition)
            page.scaleY = 1 + maxScale * (1 - absolutePosition)
            page.alpha = Math.max(fadeFactor, 1 - absolutePosition)
        }
    }

}