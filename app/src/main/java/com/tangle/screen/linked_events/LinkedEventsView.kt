package com.tangle.screen.linked_events

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import com.tangle.R
import com.tangle.base.ui.views.pagers.LinePageIndicator
import com.tangle.model.event.EventInfo
import com.tangle.screen.events.EventClickListener


class LinkedEventsView(context: Context, attr: AttributeSet) : FrameLayout(context, attr) {

    private var vpEventList: WrapContentHeightViewPager
    private var mainContainer: View
    private var adapter: LinkedEventsPagerAdapter? = null
    private var pageIndicator: LinePageIndicator?
    private var tvEventType: TextView?
    private var tvEventText: TextView?
    private var tvEventSecondaryText: TextView?

    init {
        val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v = inflater.inflate(R.layout.layout_linked_event, this, true)
        pageIndicator = v.findViewById(R.id.indicator)
        vpEventList = v.findViewById(R.id.vp_linked_events_list)
        mainContainer = v.findViewById(R.id.linked_events_layout_container)

        tvEventType = v.findViewById(R.id.txt_linked_events_type)
        tvEventText = v.findViewById(R.id.txt_linked_text)
        tvEventSecondaryText = v.findViewById(R.id.txt_linked_secondary_text)
        adapter = LinkedEventsPagerAdapter(context)

        vpEventList.adapter = adapter
        pageIndicator?.setViewPager(vpEventList)

    }

    fun updateClickListener(eventClickListener: EventClickListener) {
        adapter?.eventClickListener = eventClickListener
    }

    fun setLinkedEventList(events: List<EventInfo>, isFutureEvents: Boolean) {
        pageIndicator?.visibility = View.VISIBLE
        adapter?.update(events)
        updateText(isFutureEvents)
        updateBackground(isFutureEvents)
    }

    private fun updateBackground(futureEvents: Boolean) {
        mainContainer.setBackgroundResource(if (futureEvents) R.color.colorBackgroundDark else R.color.colorBackgroundLinkedPast)
    }

    private fun updateText(isFutureEvents: Boolean) {
        tvEventType?.setText(if (isFutureEvents) R.string.linked_past_event_title else R.string.linked_future_event_title)
        tvEventText?.setText(if (isFutureEvents) R.string.linked_past_event_text else R.string.linked_future_event_text)
        tvEventSecondaryText?.setText(if (isFutureEvents) R.string.linked_past_event_secondary_text else R.string.linked_future_event_secondary_text)
    }


    fun setCurrentItem(index: Int) {
        vpEventList.setCurrentItem(index, false)
        pageIndicator?.setCurrentItem(index)
    }
}