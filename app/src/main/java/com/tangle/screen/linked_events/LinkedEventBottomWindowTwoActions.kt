package com.tangle.screen.linked_events

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.animation.TranslateAnimation
import android.widget.LinearLayout
import com.tangle.R
import com.tangle.model.event.EventInfo
import com.tangle.model.event.Media
import com.tangle.model.event.StatusEvent
import com.tangle.utils.Patterns
import com.tangle.utils.glide.GlideApp
import com.tangle.utils.glide.RequestOptionsFactory
import kotlinx.android.synthetic.main.layout_book_future_dialog.view.*


class LinkedEventBottomWindowTwoActions @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : LinearLayout(context, attrs, defStyleAttr) {

    var eventInfo: EventInfo? = null

    init {
        val inflater = context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.layout_book_future_dialog, this, true)
        orientation = VERTICAL

    }


    fun setEventInfo(eventInfo: EventInfo, book: Runnable, invite: Runnable) {

        this.eventInfo = eventInfo
        btnInvite?.setOnClickListener { invite.run() }
        btnBook?.setOnClickListener { book.run() }
        val date = Patterns.DATE_TIME_FORMAT_LONG.format(eventInfo.eventStartDate)
        tvEventDate?.text = date

        val isBookShowing = eventInfo.status == StatusEvent.ACTIVE && eventInfo.isBookAvailable
        btnBook?.visibility = if (isBookShowing) View.VISIBLE else View.GONE

        tvEventName?.text = eventInfo.title
        if (!eventInfo.actualMediaList.isEmpty()) {
            val media = eventInfo.actualMediaList[0]
            ivEvent?.let {
                ivEvent.visibility = View.VISIBLE
                GlideApp.with(ivEvent)
                        .load(if (media.type == Media.Type.PHOTO) media.photoThumbnail2xUrl else media.videoImagePreviewUrl)
                        .apply(RequestOptionsFactory.getSmallRoundedCornersOptions(context))
                        .into(ivEvent)
            }
        }
    }

    fun slideUp() {
        post {
            eventInfo?.let {
                visibility = View.VISIBLE
                val animate = TranslateAnimation(
                        0f,
                        0f,
                        this.height.toFloat(),
                        0f)
                animate.duration = 350
                animate.fillAfter = true
                startAnimation(animate)
            }
        }

    }

    fun slideDown() {
        post {
            eventInfo?.let {
                val animate = TranslateAnimation(
                        0f,
                        0f,
                        0f,
                        this.height.toFloat())
                animate.duration = 350
                animate.fillAfter = true
                this.startAnimation(animate)
            }
        }
    }
}