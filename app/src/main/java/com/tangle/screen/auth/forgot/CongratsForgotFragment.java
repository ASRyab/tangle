package com.tangle.screen.auth.forgot;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.MVPFragment;
import com.tangle.screen.auth.login.LoginFragment;


public class CongratsForgotFragment extends MVPFragment {

    private static final String KEY_EMAIL = "key email";
    private String email;

    public CongratsForgotFragment() {
    }

    public static CongratsForgotFragment newInstance(String email) {
        CongratsForgotFragment fragment = new CongratsForgotFragment();
        Bundle args = new Bundle();
        args.putString(KEY_EMAIL, email);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            email = getArguments().getString(KEY_EMAIL);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_congrads_forgot, container, false);
        ((TextView) view.findViewById(R.id.tvTitle)).setText(getString(R.string.congrats_forgot_text, email));
        view.findViewById(R.id.btn_log_in).setOnClickListener(v -> goToLogin());
        return view;
    }

    private void goToLogin() {
        switchFragment(LoginFragment.newInstance(email), false);
    }

}
