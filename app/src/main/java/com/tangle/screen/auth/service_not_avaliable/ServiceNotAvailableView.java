package com.tangle.screen.auth.service_not_avaliable;

import com.tangle.base.ui.interfaces.LoadingView;

public interface ServiceNotAvailableView extends LoadingView {
    void goToLoginScreen();
}
