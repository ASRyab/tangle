package com.tangle.screen.auth.forgot;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.screen.auth.ErrorResponseParsers;
import com.tangle.screen.auth.UserFieldsValidator;
import com.tangle.utils.RxUtils;

public class ForgotPasswordPresenter extends LifecyclePresenter<ForgotPasswordContract.ForgotPasswordView> {
    private final ForgotPasswordContract.ForgotPasswordModel model;
    private final UserFieldsValidator validator;
    private String email;

    public ForgotPasswordPresenter(ForgotPasswordContract.ForgotPasswordModel model) {
        this.model = model;
        this.validator = UserFieldsValidator.getInstance();
    }

    @Override
    public void attachToView(ForgotPasswordContract.ForgotPasswordView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (!TextUtils.isEmpty(email)) {
            getView().setEmail(email);
        }
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void onResetClicked(String email) {
        if (validateEmail(email)) {
            monitor(model.sendEmail(email)
                    .compose(RxUtils.withLoading(getView()))
                    .subscribe(
                            response -> getView().goToSuccess(email),
                            throwable -> ErrorResponseParsers.errorForgotResponse(throwable, getView())), State.DESTROY_VIEW);
        }
    }

    private boolean validateEmail(String email) {
        getView().setEmailError(null);
        return validator.validateEmail(getContext(), email, s -> getView().setEmailError(s));
    }
}
