package com.tangle.screen.auth;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.profile_data.ProfileData;

import io.reactivex.Observable;

public class AuthorizationContract {
    interface AuthorizationView extends LoadingView {
        void setAnimationShowed();

        void goToMainScreen();

        void goToFunnelAge();

        void goToFunnelGender();

        void goToLogin();

        void goToRegistration();

        void goToPromoVideo();

        void goToAppNotAvailableScreen();
        }

    interface AuthorizationModel {
        Observable<ProfileData> autoLogin();
    }
}