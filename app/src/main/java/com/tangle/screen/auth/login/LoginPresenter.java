package com.tangle.screen.auth.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.tangle.ApplicationLoader;
import com.tangle.analytics.SimpleActions;
import com.tangle.base.RedirectManager;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.auth.ErrorResponseParsers;
import com.tangle.screen.auth.UserFieldsValidator;
import com.tangle.utils.RxUtils;
import com.tangle.utils.UserCheckUtils;

import io.reactivex.disposables.Disposable;


public class LoginPresenter extends LifecyclePresenter<LoginContract.LoginView> {

    private UserFieldsValidator validator;
    private LoginContract.LoginModel loginModel;
    private String preEmail;

    public LoginPresenter(LoginContract.LoginModel model) {
        validator = UserFieldsValidator.getInstance();
        this.loginModel = model;
    }

    @Override
    public void attachToView(LoginContract.LoginView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (!TextUtils.isEmpty(preEmail)) {
            getView().setEmail(preEmail);
        }
    }

    private boolean validateEmail(String email) {
        getView().setLoginError(null);
        return validator.validateEmail(getContext(), email, s -> getView().setLoginError(s));
    }

    private boolean validatePassword(String password) {
        getView().setPasswordError(null);
        return validator.validatePassword(getContext(), password, s -> getView().setPasswordError(s));
    }

    @SuppressWarnings("unchecked")
    public void onLoginClicked(String login, String password) {
        if (validateEmail(login) & validatePassword(password)) {
            loginModel.initLoginRequestData(login, password);
            Disposable disposable = loginModel.makeLogin()
                    .compose(RxUtils.withLoading(getView()))
                    .compose(RedirectManager.composeFunnelCheck())
                    .subscribe(
                            this::checkUserInSupportedCountry,
                            error -> RedirectManager.checkFunnel(
                                    error, checkResult -> {
                                        switch (checkResult) {
                                            case GO_TO_FUNNEL_AGE:
                                                getView().goToFunnelAge();
                                                break;
                                            case GO_TO_FUNNEL_GENDER:
                                                getView().goToFunnelGender();
                                                break;
                                            case NO_REDIRECT:
                                                ErrorResponseParsers.errorLoginResponse(error, getView());
                                                break;
                                        }
                                    }));
            monitor(disposable, State.DESTROY_VIEW);
        }
    }

    private void checkUserInSupportedCountry(ProfileData data) {
        if (UserCheckUtils.isNotSupportedCountry(data.isTargetCountry)) {
            getView().goToAppNotAvailableScreen();
        } else {
            ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.REGISTRATION_CLICK_SIGNIN_OK);
            getView().goToMainScreen();
        }
    }

    public void setEmail(String email) {
        this.preEmail = email;
    }
}
