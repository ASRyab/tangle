package com.tangle.screen.auth.splash;

import com.tangle.base.mvp.LifecyclePresenter;

public class SplashPresenter extends LifecyclePresenter<SplashView> {

    public void onSignInClicked() {
        getView().goToSignInScreen();
    }

    public void onSignUpClicked() {
        getView().goToSignUpScreen();
    }

}
