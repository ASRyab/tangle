package com.tangle.screen.auth.service_not_avaliable;

import com.tangle.api.rx_tasks.auth.logout.LogoutModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;

import io.reactivex.disposables.Disposable;

public class ServiceNotAvailablePresenter extends LifecyclePresenter<ServiceNotAvailableView> {

    public void onLogoutClicked() {
        Disposable disposable = new LogoutModel().getTask()
                .subscribe(data -> getView().goToLoginScreen(),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

}