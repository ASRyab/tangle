package com.tangle.screen.auth;

import android.content.Context;
import android.text.TextUtils;

import com.tangle.R;
import com.tangle.utils.Patterns;

import java.util.Date;

import io.reactivex.functions.Consumer;

public class UserFieldsValidator {

    private static UserFieldsValidator instance;


    public static UserFieldsValidator getInstance() {
        if (instance == null) {
            instance = new UserFieldsValidator();
        }
        return instance;
    }

    private UserFieldsValidator() {
    }

    public boolean validateNotEmpty(String line) {
        return !TextUtils.isEmpty(line);
    }

    private boolean validateEmailPattern(String email) {
        return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    public boolean validateSame(String line1, String line2) {
        if (line1 == null && line2 != null) return false;
        if (line1 != null && line2 == null) return false;
        if (line1 == null && line2 == null) return true;
        return line1.equals(line2);
    }

    private boolean validatePasswordMinLength(String password) {
        return password.length() >= 6;
    }

    private boolean validatePasswordMaxLength(String password) {
        return password.length() <= 20;
    }

    private boolean validateNameMinLength(String password) {
        return password.length() >= 2;
    }

    private boolean validateNameMaxLength(String password) {
        return password.length() <= 20;
    }

    private boolean validateNoSpaces(String line) {
        return !line.contains(" ");
    }

    public boolean validateName(Context context, String str, Consumer<String> setError) {
        try {
            if (TextUtils.isEmpty(str)) {
                setError.accept(context.getResources().getString(R.string.name_error_empty));
                return false;
            }
            if (!validateNameMaxLength(str)) {
                setError.accept(context.getResources().getString(R.string.validator_error_name_length));
                return false;
            }
            if (!validateNameMinLength(str)) {
                setError.accept(context.getResources().getString(R.string.validator_error_name_length));
                return false;
            }
            if (validateOnlyNumbers(str)) {
                setError.accept(context.getResources().getString(R.string.name_error_numbers));
                return false;
            }
            if (!validateInvalidCharacters(str) || !validateNoSpaces(str)) {
                setError.accept(context.getResources().getString(R.string.name_error));
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    private boolean validateOnlyNumbers(String str) {
        return str.matches("[0-9]+");
    }


    private boolean validateInvalidCharacters(String userName) {
        return !Patterns.CHARACTERS_PATTERN.matcher(userName).find();
    }


    public boolean validatePasswords(Context context, String oldPass, String newPass, Consumer<String> setError) {
        try {
            if (oldPass.equals(newPass)) {
                setError.accept(context.getResources().getString(R.string.password_error_the_same_as_old));
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;

    }

    public boolean validatePassword(Context context, String str, Consumer<String> setError) {
        try {
            if (TextUtils.isEmpty(str)) {
                setError.accept(context.getResources().getString(R.string.password_error_empty));
                return false;
            }
            if (!validatePasswordMaxLength(str)) {
                setError.accept(context.getResources().getString(R.string.validator_error_password_length));
                return false;
            }
            if (!validatePasswordMinLength(str)) {
                setError.accept(context.getResources().getString(R.string.validator_error_password_length));
                return false;
            }
            if (!validateInvalidCharacters(str) || !validateNoSpaces(str)) {
                setError.accept(context.getResources().getString(R.string.password_error));
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean validateEmail(Context context, String str, Consumer<String> setError) {
        if (!validateEmailPattern(str) || !validateNotEmpty(str)) {
            try {
                setError.accept(context.getResources().getString(R.string.email_error));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
        return true;
    }

    public boolean validateDate(Date date) {
        return date != null;
    }

}
