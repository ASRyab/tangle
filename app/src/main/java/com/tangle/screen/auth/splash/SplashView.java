package com.tangle.screen.auth.splash;

public interface SplashView {

    void goToSignInScreen();

    void goToSignUpScreen();

}
