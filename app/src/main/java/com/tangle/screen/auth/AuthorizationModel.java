package com.tangle.screen.auth;

import com.tangle.api.rx_tasks.auth.refresh_token.RefreshTokenModel;
import com.tangle.api.rx_tasks.profile.OwnProfileModel;

import io.reactivex.Observable;

public class AuthorizationModel implements AuthorizationContract.AuthorizationModel {

    @Override
    public Observable autoLogin() {
        return Observable
                .concat(new RefreshTokenModel().getTask(), new OwnProfileModel(true).getTask())
                .takeLast(1);
    }
}