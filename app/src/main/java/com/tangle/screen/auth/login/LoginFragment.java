package com.tangle.screen.auth.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.MVPFragment;
import com.tangle.base.ui.listeners.ImeActionListener;
import com.tangle.screen.auth.forgot.ForgotPasswordFragment;
import com.tangle.screen.auth.register.RegisterFragment;
import com.tangle.screen.auth.service_not_avaliable.ServiceNotAvailableFragment;
import com.tangle.screen.funnel.FunnelConstants;
import com.tangle.screen.funnel.age.AgeFragment;
import com.tangle.screen.funnel.you_are.YouAreFragment;
import com.tangle.screen.main.MainActivity;
import com.tangle.utils.PlatformUtils;

public class LoginFragment extends MVPFragment implements LoginContract.LoginView {
    private static final String KEY_EMAIL = "key email";
    private LoginPresenter presenter = new LoginPresenter(new LoginModel());

    private EditText emailInput;
    private EditText passwordInput;
    private TextInputLayout emailInputLayout;
    private TextInputLayout passwordInputLayout;
    private Button loginButton;

    public static LoginFragment newInstance(String email) {
        LoginFragment fragment = new LoginFragment();
        Bundle bundle = new Bundle();
        if (!TextUtils.isEmpty(email)) {
            bundle.putString(KEY_EMAIL, email);
        }
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        emailInput = view.findViewById(R.id.input_email);
        emailInputLayout = view.findViewById(R.id.input_layout_login);
        passwordInput = view.findViewById(R.id.input_password);
        passwordInputLayout = view.findViewById(R.id.input_layout_password);
        loginButton = view.findViewById(R.id.btn_sign_in);
        loginButton.setOnClickListener(v -> doLogin());
        view.findViewById(R.id.btn_create_account).setOnClickListener(v -> goToRegistration());
        view.findViewById(R.id.btn_forgot_password).setOnClickListener(v -> goToForgotPassword());
        initLoginInput();
        initPasswordInput();
        initArguments();
    }

    private void goToForgotPassword() {
        switchFragment(ForgotPasswordFragment.newInstance(emailInput.getText().toString()), true);
    }

    private void goToRegistration() {
        switchFragment(RegisterFragment.newInstance(), false);
    }

    private void initArguments() {
        String email = getArguments().getString(KEY_EMAIL);
        if (!TextUtils.isEmpty(email)) {
            presenter.setEmail(email);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        PlatformUtils.Keyboard.hideKeyboard(getActivity());
    }

    private void initLoginInput() {
        emailInput.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                emailInputLayout.setError(null);
            }
        });
    }

    private void initPasswordInput() {
        passwordInput.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                passwordInputLayout.setError(null);
            }
        });
        passwordInput.setOnEditorActionListener(new ImeActionListener(this::doLogin, EditorInfo.IME_ACTION_DONE));
    }

    private void doLogin() {
        passwordInput.clearFocus();
        PlatformUtils.Keyboard.hideKeyboard(getActivity());
        String login = emailInput.getText().toString();
        String password = passwordInput.getText().toString();
        presenter.onLoginClicked(login, password);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setLoginError(String error) {
        emailInputLayout.setError(error);
    }

    @Override
    public void setPasswordError(String error) {
        passwordInputLayout.setError(error);
    }

    @Override
    public void goToMainScreen() {
        Activity activity = getActivity();
        startActivity(new Intent(activity, MainActivity.class));
        activity.finish();
    }

    @Override
    public void goToFunnelGender() {
        switchFragment(YouAreFragment.newInstance(), false);
    }

    @Override
    public void goToFunnelAge() {
        Bundle args = new Bundle();
        args.putSerializable(FunnelConstants.IS_FROM_WEB, true);
        switchFragment(AgeFragment.newInstance(args, true), false);
    }

    @Override
    public void setEmail(String email) {
        emailInput.setText(email);
    }

    @Override
    public void goToAppNotAvailableScreen() {
        switchFragment(new ServiceNotAvailableFragment(), false);
    }
}