package com.tangle.screen.auth;

import android.text.TextUtils;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.model.Meta;
import com.tangle.screen.auth.forgot.ForgotPasswordContract;
import com.tangle.screen.auth.login.LoginContract;
import com.tangle.screen.auth.register.RegistrationContract;

public class ErrorResponseParsers {
    public static void errorForgotResponse(Throwable throwable, ForgotPasswordContract.ForgotPasswordView view) {
        if (throwable instanceof ErrorResponse) {
            Meta meta = ((ErrorResponse) throwable).getMeta();
            String message = meta.getFirstMessageByKey("email");
            if (message != null) {
                view.setEmailError(
                        "User with this email address is not registered".equals(message)
                                ? ApplicationLoader.getContext().getString(R.string.cant_find_email)
                                : message);
            }
        } else if (throwable != null) {
            view.showAPIError(throwable.getMessage());
        }
    }

    public static void errorLoginResponse(Throwable throwable, LoginContract.LoginView view) {
        if (throwable instanceof ErrorResponse) {
            Meta meta = ((ErrorResponse) throwable).getMeta();
            String type = meta.getFirstMessageByKey("type");
            String message = meta.getFirstMessageByKey("message");
            if (!TextUtils.isEmpty(message) && !TextUtils.isEmpty(type) && "email".equals(type)) {
                view.setLoginError(
                        "This email is not registered".equals(message)
                                ? ApplicationLoader.getContext().getString(R.string.cant_find_email)
                                : message);
            }
            if (!TextUtils.isEmpty(message) && !TextUtils.isEmpty(type) && "password".equals(type)) {
                view.setPasswordError("The password you have entered is incorrect. Please try again.".equals(message)
                        ? ApplicationLoader.getContext().getString(R.string.incorrect_password)
                        : message);
            }
        } else if (throwable != null) {
            view.showAPIError(throwable.getMessage());
        }
    }

    public static void errorRegistrationResponse(Throwable throwable, RegistrationContract.RegistrationView view) {
        if (throwable instanceof ErrorResponse) {
            Meta meta = ((ErrorResponse) throwable).getMeta();
            String message = meta.getFirstMessageByKey("email");
            if (message != null) {
                view.setEmailError(message, "This email is already registered.".equals(message));
            }
            message = meta.getFirstMessageByKey("password");
            if (message != null) {
                view.setPasswordError(message);
            }
        } else if (throwable != null) {
            view.showAPIError(throwable.getMessage());
        }
    }
}
