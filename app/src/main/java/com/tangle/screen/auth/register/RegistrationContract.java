package com.tangle.screen.auth.register;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.auth.AuthDataResponse;

import java.util.Date;

import io.reactivex.Observable;

public class RegistrationContract {
    public interface RegistrationView extends LoadingView {

        void setNameError(String error);

        void setPasswordError(String error);

        void setEmailError(String error, boolean isRedirect);

        void goToFunnel();

        void processUrl(String url);
    }

    interface RegistrationModel {
        Observable<AuthDataResponse> makeRegistration(String name,
                                                      String email,
                                                      String password,
                                                      Date birthDay);
    }
}
