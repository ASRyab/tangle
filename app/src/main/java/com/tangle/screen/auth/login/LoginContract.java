package com.tangle.screen.auth.login;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.profile_data.ProfileData;

import io.reactivex.Observable;

public class LoginContract {
    public interface LoginView extends LoadingView {

        void setLoginError(String error);

        void setPasswordError(String password);

        void goToMainScreen();

        void goToFunnelGender();

        void goToFunnelAge();

        void setEmail(String email);

        void goToAppNotAvailableScreen();
    }

    public interface LoginModel {
        Observable<ProfileData> makeLogin();

        void initLoginRequestData(String login, String password);
    }
}
