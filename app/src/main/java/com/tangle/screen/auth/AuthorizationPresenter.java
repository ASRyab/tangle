package com.tangle.screen.auth;

import android.text.TextUtils;

import com.tangle.api.Api;
import com.tangle.api.utils.Config;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.RedirectManager;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.UserCheckUtils;

import io.reactivex.disposables.Disposable;

public class AuthorizationPresenter extends LifecyclePresenter<AuthorizationContract.AuthorizationView> {

    private final AuthorizationContract.AuthorizationModel authModel;
    private boolean isFirstStart;
    private boolean isAfterLogout;

    public AuthorizationPresenter(AuthorizationContract.AuthorizationModel model) {
        this.authModel = model;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void onCreate() {
        super.onCreate();
        if (Config.isLogging()
                && !TextUtils.isEmpty(Config.TEST_USER_AUTOLOGIN)) {
            Api.getInst().getSession().updateRefreshToken(Config.TEST_USER_AUTOLOGIN);
        }
        String refreshToken = Api.getInst().getSession().getRefreshToken();
        processAfterLogout();
        if (TextUtils.isEmpty(refreshToken)) {
            isFirstStart = true;
        } else {
            Disposable subscribe = authModel.autoLogin()
                    .compose(RedirectManager.composeFunnelCheck())
                    .subscribe(this::checkUserInSupportedCountry,
                            throwable -> RedirectManager.checkFunnel(
                                    throwable, checkResult -> {
                                        switch (checkResult) {
                                            case GO_TO_FUNNEL_AGE:
                                                getView().goToFunnelAge();
                                                break;
                                            case GO_TO_FUNNEL_GENDER:
                                                getView().goToFunnelGender();
                                                break;
                                            case NO_REDIRECT:
                                                getView().showAPIError(ErrorResponse.getServerMessage(throwable));
                                                break;
                                        }
                                    }));
            monitor(subscribe, State.DESTROY);
        }
    }

    private void processAfterLogout() {
        if (!isAfterLogout) {
            getView().goToPromoVideo();
        } else {
            getView().goToLogin();
        }
    }

    public void endPromoVideo() {
        if (isFirstStart) {
            getView().goToRegistration();
        }
    }

    public void setAfterLogout(boolean logout) {
        this.isAfterLogout = logout;
    }

    private void checkUserInSupportedCountry(ProfileData profileData) {
        if (UserCheckUtils.isNotSupportedCountry(profileData.isTargetCountry)) {
            getView().goToAppNotAvailableScreen();
        } else {
            getView().goToMainScreen();
        }
    }
}
