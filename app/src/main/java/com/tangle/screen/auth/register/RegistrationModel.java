package com.tangle.screen.auth.register;

import com.tangle.ApplicationLoader;
import com.tangle.api.Api;
import com.tangle.api.rx_tasks.auth.refresh_token.RefreshTokenModel;
import com.tangle.api.rx_tasks.auth.registration.Registration;
import com.tangle.model.auth.AuthDataResponse;
import com.tangle.utils.PreferenceManager;

import java.util.Date;

import io.reactivex.Observable;

public class RegistrationModel implements RegistrationContract.RegistrationModel {

    private void storeDataToPref(AuthDataResponse authData) {
        PreferenceManager preferenceManager = ApplicationLoader.getApplicationInstance().getPreferenceManager();
        preferenceManager.saveRefreshToken(authData.getRefreshToken());
        Api.getInst().createSessionWithAuthData(authData);
    }

    @Override
    public Observable<AuthDataResponse> makeRegistration(String login, String email, String password, Date birthDay) {
        Api.getInst().clearSession();
        return new Registration(login, email, password, birthDay)
                .getDataTask()
                .flatMap(RefreshTokenModel::authRefresh)
                .doOnNext(this::storeDataToPref);
    }
}