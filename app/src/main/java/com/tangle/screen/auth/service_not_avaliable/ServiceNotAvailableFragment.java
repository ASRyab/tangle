package com.tangle.screen.auth.service_not_avaliable;

import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.MVPFragment;
import com.tangle.screen.auth.AuthorizationActivity;

public class ServiceNotAvailableFragment extends MVPFragment implements ServiceNotAvailableView {

    private ServiceNotAvailablePresenter presenter = new ServiceNotAvailablePresenter();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.layout_service_not_avaliable, container, false);
        ImageView imageView = v.findViewById(R.id.img_background);
        makeBgBlackAndWhite(imageView);
        View vLogout = v.findViewById(R.id.txt_logout);
        vLogout.setOnClickListener(view -> presenter.onLogoutClicked());
        return v;
    }

    private void makeBgBlackAndWhite(ImageView imageView) {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
        imageView.setColorFilter(filter);
    }

    @Override
    public void goToLoginScreen() {
        AuthorizationActivity.startAuth(getActivity());
        getActivity().finish();
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}
