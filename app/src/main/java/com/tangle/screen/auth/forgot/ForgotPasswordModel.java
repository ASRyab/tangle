package com.tangle.screen.auth.forgot;

import com.tangle.api.rx_tasks.auth.remind_password.RemindPassword;
import com.tangle.model.auth.RemindPasswordDataResponse;

import io.reactivex.Observable;

public class ForgotPasswordModel implements ForgotPasswordContract.ForgotPasswordModel {
    @Override
    public Observable<RemindPasswordDataResponse> sendEmail(String email) {
        return new RemindPassword(email).getDataTask();
    }
}
