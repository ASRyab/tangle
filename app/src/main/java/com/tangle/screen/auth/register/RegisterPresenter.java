package com.tangle.screen.auth.register;

import com.tangle.ApplicationLoader;
import com.tangle.analytics.SimpleActions;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.auth.ErrorResponseParsers;
import com.tangle.screen.auth.UserFieldsValidator;
import com.tangle.utils.ConvertingUtils;
import com.tangle.utils.RxUtils;

import java.util.Date;

import io.reactivex.disposables.Disposable;

import static com.tangle.api.utils.Config.PRIVACY_POLICY_URL;
import static com.tangle.api.utils.Config.TERMS_OF_USE_URL;

public class RegisterPresenter extends LifecyclePresenter<RegistrationContract.RegistrationView> {
    private RegistrationContract.RegistrationModel model;
    private UserFieldsValidator validator;
    public static boolean isAfterTerms;
    private ProfileData profile;

    public RegisterPresenter(RegistrationContract.RegistrationModel model) {
        this.validator = UserFieldsValidator.getInstance();
        this.model = model;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (isAfterTerms) {
            isAfterTerms = false;
            makeRegister();
        }
    }

    private boolean validateName(String name) {
        getView().setNameError(null);
        return validator.validateName(getContext(), name, s -> getView().setNameError(s));
    }

    private boolean validateEmail(String email) {
        getView().setEmailError(null, false);
        return validator.validateEmail(getContext(), email, s -> getView().setEmailError(s, false));
    }

    private boolean validatePassword(String password) {
        getView().setPasswordError(null);
        return validator.validatePassword(getContext(), password, (error) -> getView().setPasswordError(error));
    }

    public void onRegisterClicked(String name, String email, String password) {
        if (validateName(name) & validateEmail(email) & validatePassword(password)) {
            profile = new ProfileData();
            profile.login = name;
            profile.email = email;
            profile.password = password;
            makeRegister();
        }
    }

    private void makeRegister() {
        Date testDate = ConvertingUtils.getDateFromString("14-11-1994", "dd-MM-yyyy");
        Disposable disposable = model.makeRegistration(profile.login, profile.email, profile.password, testDate)
                .compose(RxUtils.withLoading(getView()))
                .subscribe(
                        response -> {
                            ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.REGISTRATION_CLICK_SIGNUP_OK);
                            getView().goToFunnel();
                        },
                        throwable -> ErrorResponseParsers.errorRegistrationResponse(throwable, getView()));
        monitor(disposable, State.DESTROY_VIEW);
    }

    void onTermsOfUseClick() {
        getView().processUrl(TERMS_OF_USE_URL);
    }

    void onPrivacyPolicyClick() {
        getView().processUrl(PRIVACY_POLICY_URL);
    }
}