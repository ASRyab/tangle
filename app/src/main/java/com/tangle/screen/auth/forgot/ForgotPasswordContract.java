package com.tangle.screen.auth.forgot;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.auth.RemindPasswordDataResponse;

import io.reactivex.Observable;

public class ForgotPasswordContract {
    public interface ForgotPasswordView extends LoadingView {
        void setEmailError(String error);

        void setEmail(String email);

        void goToSuccess(String email);
    }

    interface ForgotPasswordModel {
        Observable<RemindPasswordDataResponse> sendEmail(String email);
    }
}
