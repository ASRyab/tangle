package com.tangle.screen.auth.splash;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.MVPFragment;
import com.tangle.screen.auth.login.LoginFragment;
import com.tangle.screen.auth.register.RegisterFragment;

public class SplashFragment extends MVPFragment implements SplashView {
    private SplashPresenter presenter = new SplashPresenter();

    protected TextView signUpTextView;
    protected TextView signInTextView;

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_splash, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        signUpTextView = view.findViewById(R.id.txt_sign_up);
        signInTextView = view.findViewById(R.id.txt_sign_in);
        signUpTextView.setOnClickListener(v -> presenter.onSignUpClicked());
        signInTextView.setOnClickListener(v -> presenter.onSignInClicked());
    }

    @Override
    public void goToSignInScreen() {
        switchFragment(new LoginFragment(), true);
    }

    @Override
    public void goToSignUpScreen() {
        switchFragment(new RegisterFragment(), true);
    }
}