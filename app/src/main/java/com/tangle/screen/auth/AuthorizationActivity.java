package com.tangle.screen.auth;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.Toast;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.BaseActivity;
import com.tangle.screen.auth.login.LoginFragment;
import com.tangle.screen.auth.register.RegisterFragment;
import com.tangle.screen.auth.service_not_avaliable.ServiceNotAvailableFragment;
import com.tangle.screen.funnel.FunnelConstants;
import com.tangle.screen.funnel.age.AgeFragment;
import com.tangle.screen.funnel.you_are.YouAreFragment;
import com.tangle.screen.media.video.promo.AppLaunchPromoVideoFragment;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.net.MalformedURLException;
import java.net.URL;

import io.branch.referral.Branch;

public class AuthorizationActivity extends BaseActivity implements AuthorizationContract.AuthorizationView {
    private static final String AFTER_LOGOUT = "after logout";
    private AuthorizationPresenter presenter = new AuthorizationPresenter(new AuthorizationModel());
    private boolean animationShowed;

    public static void startAuth(FragmentActivity activity) {
        Intent intent = new Intent(activity, AuthorizationActivity.class);
        intent.putExtra(AFTER_LOGOUT, true);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authorization);
        presenter.setAfterLogout(getIntent().getBooleanExtra(AFTER_LOGOUT, false));
        GlideApp.with(this)
                .load(R.drawable.auth_background)
                .apply(RequestOptionsFactory.getDefaultBlurOptions(this))
                .into((ImageView) findViewById(R.id.auth_background));
        if (getPresenter() != null) {
            getPresenter().onAttach(this.getApplicationContext());
            getPresenter().attachToView(this, savedInstanceState);
            getPresenter().onCreate();
        }
    }

    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onStart() {
        super.onStart();
        initBranch();
        if (getPresenter() != null) {
            getPresenter().onStart();
        }
    }

    private void initBranch() {
        Branch.getInstance().initSession((referringParams, error) -> {
            boolean isBranch = referringParams.optBoolean("+clicked_branch_link");
            boolean isFirst = referringParams.optBoolean("+is_first_session");
            String referrerLink = referringParams.optString("~referring_link");
            if (isBranch && isFirst && !TextUtils.isEmpty(referrerLink)) {
                try {
                    URL url = new URL(referrerLink);
                    String query = url.getQuery();
                    ApplicationLoader.getApplicationInstance().getTrackingManager().trackBranchInstall(query);
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    ApplicationLoader.getApplicationInstance().getTrackingManager().trackInstall();
                }
            } else {
                ApplicationLoader.getApplicationInstance().getTrackingManager().trackInstall();
            }

        }, this.getIntent().getData(), this);
    }

    @Override
    public void onNewIntent(Intent intent) {
        this.setIntent(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getPresenter() != null) {
            getPresenter().onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getPresenter() != null) {
            getPresenter().onPause();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (getPresenter() != null) {
            getPresenter().onStop();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getPresenter() != null) {
            getPresenter().onDestroyView();
            getPresenter().onDestroy();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (getPresenter() != null) {
            getPresenter().onSaveInstanceState(outState);
        }
    }

    @Override
    public void goToMainScreen() {
        ApplicationLoader.getApplicationInstance().getNavigationManager().showMainScreen();
    }

    @Override
    public void goToAppNotAvailableScreen() {
        switchFragment(new ServiceNotAvailableFragment(), false);
    }

    @Override
    public void goToFunnelAge() {
        Bundle arguments = new Bundle();
        arguments.putSerializable(FunnelConstants.IS_FROM_WEB, true);
        switchFragment(AgeFragment.newInstance(arguments, true), false);
    }

    @Override
    public void goToFunnelGender() {
        switchFragment(YouAreFragment.newInstance(), false);
    }

    @Override
    public void goToLogin() {
        setAnimationShowed();
        switchFragment(LoginFragment.newInstance(null), false);
    }

    @Override
    public void goToRegistration() {
        switchFragment(RegisterFragment.newInstance(), false);
    }

    @Override
    public void goToPromoVideo() {
        switchFragment(new AppLaunchPromoVideoFragment(view -> {
            onBackPressed();
            presenter.endPromoVideo();
        }), true);
    }

    @Override
    public void showLoading() {
    }

    @Override
    public void hideLoading() {
    }

    @Override
    public void showAPIError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    public boolean isAnimationShowed() {
        return animationShowed;
    }

    public void setAnimationShowed() {
        this.animationShowed = true;
    }
}
