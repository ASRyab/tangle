package com.tangle.screen.auth.register;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.MVPFragment;
import com.tangle.base.ui.listeners.ImeActionListener;
import com.tangle.screen.auth.AuthorizationActivity;
import com.tangle.screen.auth.login.LoginFragment;
import com.tangle.screen.funnel.you_are.YouAreFragment;
import com.tangle.screen.web_browser.WebBrowserFragment;
import com.tangle.utils.PlatformUtils;

public class RegisterFragment extends MVPFragment implements RegistrationContract.RegistrationView {
    private RegisterPresenter presenter = new RegisterPresenter(new RegistrationModel());

    private EditText nameInput;
    private EditText passwordInput;
    private EditText emailInput;
    private TextInputLayout nameInputLayout;
    private TextInputLayout passwordInputLayout;
    private TextInputLayout emailInputLayout;
    private Button registerButton;
    private View containerInput;
    private TextView policy;
    private TextView customEmailError;
    private TextView loginButton;

    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_register, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        nameInput = view.findViewById(R.id.input_name);
        nameInputLayout = view.findViewById(R.id.input_layout_name);
        emailInput = view.findViewById(R.id.input_email);
        emailInputLayout = view.findViewById(R.id.input_layout_email);
        passwordInput = view.findViewById(R.id.input_password);
        passwordInputLayout = view.findViewById(R.id.input_layout_password);
        registerButton = view.findViewById(R.id.btn_register);
        containerInput = view.findViewById(R.id.main_container);
        policy = view.findViewById(R.id.txt_policy);
        customEmailError = view.findViewById(R.id.txt_custom_error);
        loginButton = view.findViewById(R.id.btn_log_in);
        registerButton.setOnClickListener(v -> doRegister());
        loginButton.setOnClickListener(v -> goToLogin(null));
        initNameInput();
        initEmailInput();
        initPasswordInput();
        startAnimation();
        initPolicy();
    }

    private void initPolicy() {
        String terms = getString(R.string.terms_chb_terms_span_text);
        String privacy = getString(R.string.terms_chb_privacy_span_text);
        String body = getString(R.string.terms_chb_privacy_text_registration);
        SpannableString spannableBody = new SpannableString(body);
        policy.setMovementMethod(LinkMovementMethod.getInstance());
        policy.setHighlightColor(Color.TRANSPARENT);
        spannableBody = getSpanText(spannableBody, body, terms, __ -> presenter.onTermsOfUseClick());
        spannableBody = getSpanText(spannableBody, body, privacy, __ -> presenter.onPrivacyPolicyClick());
        policy.setText(spannableBody);
    }

    private SpannableString getSpanText(SpannableString spannableBody, String body, String spanText, View.OnClickListener clickListener) {
        body = String.format(body, spanText);
        int start = body.indexOf(spanText);
        int finish = start + spanText.length();
        spannableBody.setSpan(new ClickableSpan() {
            @Override
            public void onClick(final View view) {
                clickListener.onClick(view);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                ds.setUnderlineText(false);
            }
        }, start, finish, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableBody;
    }

    @Override
    public void processUrl(String url) {
        switchFragment(WebBrowserFragment.newInstance(url), true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        PlatformUtils.Keyboard.hideKeyboard(getActivity());
    }

    private void initNameInput() {
        nameInput.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                nameInputLayout.setError(null);
            }
        });
    }

    private void initEmailInput() {
        emailInput.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                emailInputLayout.setError(null);
                customEmailError.setText("");
            }
        });
    }

    private void initPasswordInput() {
        passwordInput.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                passwordInputLayout.setError(null);
            }
        });
        passwordInput.setOnEditorActionListener(new ImeActionListener(this::doRegister, EditorInfo.IME_ACTION_DONE));
    }

    private void doRegister() {
        PlatformUtils.Keyboard.hideKeyboard(getActivity());
        String name = nameInput.getText().toString();
        String password = passwordInput.getText().toString();
        String email = emailInput.getText().toString();
        presenter.onRegisterClicked(name, email, password);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setNameError(String error) {
        nameInputLayout.setError(error);
    }

    @Override
    public void setPasswordError(String error) {
        passwordInputLayout.setError(error);
    }

    @Override
    public void setEmailError(String error, boolean isRedirect) {
        emailInputLayout.setError(error);
        if (!isRedirect) {
            return;
        }
        emailInputLayout.setError("");
        String string = getString(R.string.auth_sign_in);
        String body = getString(R.string.error_taken_email);
        SpannableString spannableBody = new SpannableString(body);

        spannableBody = getSpanText(spannableBody, body, string, __ -> goToLogin(emailInput.getText().toString()));
        customEmailError.setText(spannableBody);
    }

    private void goToLogin(String email) {
        switchFragment(LoginFragment.newInstance(email), false);
    }

    @Override
    public void goToFunnel() {
        switchFragment(YouAreFragment.newInstance(), false);
    }

    private void startAnimation() {
        boolean showed = ((AuthorizationActivity) getActivity()).isAnimationShowed();
        if (!showed) {
            ((AuthorizationActivity) getActivity()).setAnimationShowed();
            ObjectAnimator slideTitleUpX = ObjectAnimator.ofFloat(containerInput, View.SCALE_X, 0f, 1.2f, 0.9f, 1.05f, 0.95f, 1.0f);
            ObjectAnimator slideTitleUpY = ObjectAnimator.ofFloat(containerInput, View.SCALE_Y, 0f, 1.2f, 0.9f, 1.05f, 0.95f, 1.0f);
            AnimatorSet set = new AnimatorSet();
            set.setDuration(1000);
            set.playTogether(slideTitleUpX, slideTitleUpY);
            set.playSequentially();
            set.start();
        }
    }
}