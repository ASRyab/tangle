package com.tangle.screen.auth.login;

import com.tangle.ApplicationLoader;
import com.tangle.api.Api;
import com.tangle.api.rx_tasks.auth.login.CheckLogin;
import com.tangle.api.rx_tasks.auth.login.Login;
import com.tangle.api.rx_tasks.auth.refresh_token.RefreshTokenModel;
import com.tangle.api.rx_tasks.profile.OwnProfileModel;
import com.tangle.model.auth.AuthDataResponse;
import com.tangle.utils.PreferenceManager;

import io.reactivex.Observable;

public class LoginModel implements LoginContract.LoginModel {
    private Login loginObservable;
    private CheckLogin checkLogin;

    public void initLoginRequestData(String email, String password) {
        this.loginObservable = new Login(email, password);
        this.checkLogin = new CheckLogin(email, password);
    }

    private void storeDataToPref(AuthDataResponse authData) {
        PreferenceManager preferenceManager = ApplicationLoader.getApplicationInstance().getPreferenceManager();
        preferenceManager.saveRefreshToken(authData.getRefreshToken());
        Api.getInst().createSessionWithAuthData(authData);
    }

    @Override
    public Observable makeLogin() {
        Api.getInst().clearSession();
        return Observable.concat(
                checkLogin.getDataTask()
                , loginObservable.getDataTask()
                        .flatMap(RefreshTokenModel::authRefresh)
                        .doOnNext(this::storeDataToPref)
                , new OwnProfileModel(true).getTask())
                .takeLast(1);
    }
}