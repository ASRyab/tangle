package com.tangle.screen.auth.forgot;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.MVPFragment;
import com.tangle.base.ui.listeners.ImeActionListener;
import com.tangle.utils.PlatformUtils;

public class ForgotPasswordFragment extends MVPFragment implements ForgotPasswordContract.ForgotPasswordView {

    private static final String KEY_EMAIL = "key email";
    private EditText emailInput;
    private TextInputLayout emailInputLayout;

    private ForgotPasswordPresenter presenter = new ForgotPasswordPresenter(new ForgotPasswordModel());

    public ForgotPasswordFragment() {
        // Required empty public constructor
    }

    public static ForgotPasswordFragment newInstance(String email) {
        ForgotPasswordFragment fragment = new ForgotPasswordFragment();
        Bundle bundle = new Bundle();
        bundle.putString(KEY_EMAIL, email);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String email = getArguments().getString(KEY_EMAIL);
            presenter.setEmail(email);
        }

    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initToolbar("", true);
        view.findViewById(R.id.btn_back).setOnClickListener(v -> getActivity().onBackPressed());

        emailInput = view.findViewById(R.id.input_email);
        emailInputLayout = view.findViewById(R.id.input_layout_login);
        emailInput.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                emailInputLayout.setError(null);
            }
        });
        emailInput.setOnEditorActionListener(new ImeActionListener(this::doReset, EditorInfo.IME_ACTION_DONE));

        View resetPassword = view.findViewById(R.id.btn_reset_password);
        resetPassword.setOnClickListener(v -> doReset());
    }

    private void doReset() {
        PlatformUtils.Keyboard.hideKeyboard(getActivity());
        String email = emailInput.getText().toString();
        presenter.onResetClicked(email);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_forgot_password, container, false);
    }

    @Override
    public void setEmailError(String error) {
        emailInputLayout.setError(error);
    }

    @Override
    public void setEmail(String email) {
        emailInput.setText(email);
    }

    @Override
    public void goToSuccess(String email) {
        switchFragment(CongratsForgotFragment.newInstance(email), false);
    }
}
