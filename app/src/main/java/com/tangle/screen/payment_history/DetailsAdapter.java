package com.tangle.screen.payment_history;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.model.payment.TicketResponse;
import com.tangle.utils.ConvertingUtils;

import java.util.Currency;
import java.util.List;

public class DetailsAdapter extends RecyclerView.Adapter<DetailsAdapter.ViewHolder> {

    private final List<TicketResponse> tickets;
    private final String currency;

    public DetailsAdapter(List<TicketResponse> tickets, String currency) {
        this.tickets = tickets;
        this.currency = currency;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(View.inflate(parent.getContext(), R.layout.item_payment_detail_history, null));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(tickets.get(position));
    }

    @Override
    public int getItemCount() {
        return tickets.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private final TextView txtTicketType, txtTicketTime, txtChargetAmount;

        public ViewHolder(View itemView) {
            super(itemView);
            txtTicketType = itemView.findViewById(R.id.txt_ticket_type);
            txtTicketTime = itemView.findViewById(R.id.txt_ticket_time);
            txtChargetAmount = itemView.findViewById(R.id.txt_charget_amount);
        }

        public void bind(TicketResponse ticket) {
            txtChargetAmount.setText(Currency.getInstance(currency).getSymbol() + String.valueOf(ticket.getAmount()));
            txtTicketTime.setText(ConvertingUtils.getStringDateByPattern(
                    ConvertingUtils.getDateFromString(ticket.getCreatedAt(), ConvertingUtils.DATE_FORMAT_YMD_HMS),
                    ConvertingUtils.DATE_FORMAT_TICKET_CREATED_TIME));
            Context context = txtTicketType.getContext();
            txtTicketType.setText(ticket.isDonated() ? context.getString(R.string.donated_ticket)
                    : context.getString(R.string.ticket_for_you));
        }
    }
}
