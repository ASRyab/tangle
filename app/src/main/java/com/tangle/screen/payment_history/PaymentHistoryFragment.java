package com.tangle.screen.payment_history;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;

import java.util.List;

public class PaymentHistoryFragment extends SecondaryScreenFragment implements PaymentHistoryView {
    private PaymentHistoryPresenter presenter = new PaymentHistoryPresenter();

    private RecyclerView rvPaymentHistory;
    private PaymentHistoryAdapter paymentHistoryAdapter;
    private View notificationInformationLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_payment_history, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        initToolbar(getString(R.string.payment_history), true);
        rvPaymentHistory = view.findViewById(R.id.payment_history_rview);
        notificationInformationLayout = view.findViewById(R.id.notify_view);
        paymentHistoryAdapter = new PaymentHistoryAdapter();
        rvPaymentHistory.setLayoutManager(new LinearLayoutManager(rvPaymentHistory.getContext()));
        rvPaymentHistory.setItemAnimator(new DefaultItemAnimator());
        rvPaymentHistory.setAdapter(paymentHistoryAdapter);
        paymentHistoryAdapter.setOnLoadMoreListener(rvPaymentHistory, () -> presenter.loadPaymentHistory());
    }

    @Override
    public void setPaymentHistory(List<PaymentHistoryViewData> paymentHistoryList) {
        paymentHistoryAdapter.addPaymentHistory(paymentHistoryList);
        if (paymentHistoryList.isEmpty() && paymentHistoryAdapter.getItemCount() == 0) {
            notificationInformationLayout.setVisibility(View.VISIBLE);
        } else {
            notificationInformationLayout.setVisibility(View.GONE);
        }
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}