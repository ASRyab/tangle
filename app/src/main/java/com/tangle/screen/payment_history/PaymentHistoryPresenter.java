package com.tangle.screen.payment_history;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.payment.InvoiceInfo;
import com.tangle.utils.RxUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.disposables.Disposable;

public class PaymentHistoryPresenter extends LifecyclePresenter<PaymentHistoryView> {
    private static final String TAG = PaymentHistoryPresenter.class.getSimpleName();

    private PaymentHistoryModel paymentHistoryModel = new PaymentHistoryModel();

    @Override
    public void attachToView(PaymentHistoryView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        Disposable disposable = paymentHistoryModel.getTask()
                .compose(RxUtils.withLoading(getView()))
                .compose(zipWithEvent())
                .subscribe(paymentHistoryList -> getView().setPaymentHistory(paymentHistoryList),
                        RxUtils.getEmptyErrorConsumer(TAG, "attachToView"));
        monitor(disposable, State.DESTROY_VIEW);
    }

    public void loadPaymentHistory() {
        Disposable disposable = paymentHistoryModel.getTask()
                .compose(zipWithEvent())
                .subscribe(paymentHistoryList -> getView().setPaymentHistory(paymentHistoryList),
                        RxUtils.getEmptyErrorConsumer(TAG, "loadPaymentHistory"));
        monitor(disposable, State.DESTROY_VIEW);
    }


    private ObservableTransformer<List<InvoiceInfo>, List<PaymentHistoryViewData>> zipWithEvent() {
        return upstream -> upstream
                .switchMap(invoiceInfos -> Observable.fromIterable(invoiceInfos)
                        .switchMap(info -> ApplicationLoader.getApplicationInstance().getEventManager().getEventById(info.getEventId())
                                .map(event -> new PaymentHistoryViewData(info, event)))
                        .toList().toObservable());
    }
}