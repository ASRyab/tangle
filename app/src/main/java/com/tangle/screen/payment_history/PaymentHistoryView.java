package com.tangle.screen.payment_history;

import com.tangle.base.ui.interfaces.LoadingView;

import java.util.List;

public interface PaymentHistoryView extends LoadingView{
    void setPaymentHistory(List<PaymentHistoryViewData> paymentHistoryList);
}