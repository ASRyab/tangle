package com.tangle.screen.payment_history

import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import com.tangle.ApplicationLoader
import com.tangle.R
import com.tangle.base.ui.recyclerview.decorations.SpaceItemDecoration
import com.tangle.model.event.EventInfo
import com.tangle.model.event.Media
import com.tangle.model.payment.EventInvoiceTransactionType
import com.tangle.model.payment.InvoiceInfo
import com.tangle.utils.ConvertingUtils
import com.tangle.utils.RxUtils
import com.tangle.utils.glide.GlideApp
import com.tangle.utils.glide.RequestOptionsFactory
import io.reactivex.functions.Consumer
import kotlinx.android.synthetic.main.item_payment_history.view.*
import java.util.*

class HistoryItemViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
    companion object {
        private val TAG = HistoryItemViewHolder::class.java.simpleName
    }

    private lateinit var invoiceInfo: InvoiceInfo
    private lateinit var eventInfo: EventInfo


    init {
        itemView.rvDescription.layoutManager = LinearLayoutManager(view.context)
        itemView.rvDescription.addItemDecoration(SpaceItemDecoration(15))
        itemView.cbOpenCloseIndicator.setOnCheckedChangeListener { buttonView
                                                                   , isChecked -> itemView.rvDescription.visibility = if (isChecked) View.VISIBLE else View.GONE }
    }

    fun bindData(paymentHistoryData: PaymentHistoryViewData) {
        this.invoiceInfo = paymentHistoryData.info
        this.eventInfo = paymentHistoryData.event
        setData()
        itemView.rvDescription.adapter = DetailsAdapter(paymentHistoryData.info.tickets, paymentHistoryData.info.currency)
    }

    private fun setFormattedTitle(eventInfo: EventInfo) {
        val context = ApplicationLoader.getContext()
        if (ApplicationLoader.getApplicationInstance().userManager.isCurrentUser(invoiceInfo.tickets[0].userId)) {
            itemView.tvTitle.text = SpannableString.valueOf(eventInfo.title + " " + context.getString(R.string.ticket_for_myself))
        } else {
            ApplicationLoader.getApplicationInstance().userManager.getUserById(invoiceInfo.tickets[0].userId)
                    .subscribe(
                            Consumer { profileData -> itemView.tvTitle.text = initSpannableBody(eventInfo!!.title + " " + context.getString(R.string.formatted_ticket_for), profileData.login) }
                            , RxUtils.getEmptyErrorConsumer(TAG, "setFormattedTitle"))
        }
    }

    private fun initSpannableBody(body: String, userName: String): SpannableString {
        val formatBody = String.format(body, userName)
        val start = formatBody.indexOf(userName)
        val finish = start + userName.length
        val spannableBody = SpannableString(formatBody)
        spannableBody.setSpan(ForegroundColorSpan(ContextCompat.getColor(ApplicationLoader.getContext(), R.color.colorPrimary)),
                start, finish, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return spannableBody
    }

    private fun setData() {
        itemView.tvTime.text = ConvertingUtils.getStringDateByPattern(
                ConvertingUtils.getDateFromString(invoiceInfo.createdAt, ConvertingUtils.DATE_FORMAT_YMD_HMS),
                ConvertingUtils.DATE_FORMAT_DMY)
        setFormattedTitle(eventInfo)
        setEventData(eventInfo)
        itemView.tvPrice.text = Currency.getInstance(invoiceInfo.currency).symbol + invoiceInfo.amount
        itemView.tvPrice.setTextColor(ContextCompat.getColor(itemView.context, android.R.color.white))
        setTicketStatus(invoiceInfo.transactionType)
    }


    private fun setTicketStatus(transactionType: EventInvoiceTransactionType?) {
        val redStatus = itemView.context.resources.getDrawable(R.drawable.payment_status_green)
        itemView.tvStatus.setCompoundDrawablesWithIntrinsicBounds(null, null, redStatus, null)
        itemView.tvStatus.compoundDrawablePadding = 8
        itemView.tvStatus.text = when (transactionType) {
            EventInvoiceTransactionType.Hold -> itemView.resources.getString(R.string.reserved)
            else -> itemView.resources.getString(R.string.billed)
        }
    }

    private fun setEventData(event: EventInfo) {
        val actualMediaList = event.actualMediaList
        if (actualMediaList != null && !event.actualMediaList.isEmpty()) {
            val media = event.actualMediaList[0]
            GlideApp.with(itemView.ivEvent)
                    .load(if (media.type == Media.Type.PHOTO) media.photoThumbnail2xUrl else media.videoImagePreviewUrl)
                    .apply(RequestOptionsFactory.getSmallRoundedCornersOptions(itemView.context))
                    .into(itemView.ivEvent)
            itemView.vLoaderEventImage.visibility = View.INVISIBLE
        }
    }
}
