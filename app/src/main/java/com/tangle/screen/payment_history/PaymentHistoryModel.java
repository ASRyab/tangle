package com.tangle.screen.payment_history;

import com.tangle.api.rx_tasks.ObservableTask;
import com.tangle.api.rx_tasks.payment.PaymentHistory;
import com.tangle.model.payment.InvoiceInfo;

import java.util.List;

import io.reactivex.Observable;

public class PaymentHistoryModel extends ObservableTask<List<InvoiceInfo>> {
    public static final int LIMIT = 25;
    private int offset = 0;

    public PaymentHistoryModel() {
        super(false);
    }

    @Override
    protected Observable<List<InvoiceInfo>> getObservableTask() {
        return new PaymentHistory(LIMIT, offset).getDataTask().doOnNext(list -> offset = offset + list.size());
    }
}