package com.tangle.screen.payment_history

import com.tangle.model.event.EventInfo
import com.tangle.model.payment.InvoiceInfo

data class PaymentHistoryViewData(val info: InvoiceInfo, val event: EventInfo)