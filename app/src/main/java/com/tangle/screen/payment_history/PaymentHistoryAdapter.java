package com.tangle.screen.payment_history;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.tangle.R;

import java.util.ArrayList;
import java.util.List;

public class PaymentHistoryAdapter extends RecyclerView.Adapter<HistoryItemViewHolder> {
    private List<PaymentHistoryViewData> paymentHistoryList = new ArrayList<>();

    private boolean isLoading;
    private int lastVisibleItem, totalItemCount;

    public void addPaymentHistory(List<PaymentHistoryViewData> paymentHistoryList) {
        this.paymentHistoryList.addAll(paymentHistoryList);
        notifyDataSetChanged();
        setLoaded();
    }

    @Override
    public HistoryItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new HistoryItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment_history, parent,false));
    }

    @Override
    public void onBindViewHolder(HistoryItemViewHolder holder, int position) {
        holder.bindData(paymentHistoryList.get(position));
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return paymentHistoryList.size();
    }


    public void setOnLoadMoreListener(RecyclerView recyclerView, OnLoadMoreListener onLoadMoreListener) {
        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (getItemCount() != 0) {
                    totalItemCount = linearLayoutManager.getItemCount();
                    lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                    if (!isLoading && totalItemCount <= (lastVisibleItem + PaymentHistoryModel.LIMIT)) {
                        if (onLoadMoreListener != null) {
                            onLoadMoreListener.onLoadMore();
                        }
                        isLoading = true;
                    }
                }
            }
        });
    }

    public void setLoaded() {
        isLoading = false;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }
}