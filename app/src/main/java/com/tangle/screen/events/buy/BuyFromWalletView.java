package com.tangle.screen.events.buy;

import com.tangle.model.event.EventInfo;

public interface BuyFromWalletView {
    void setData(EventInfo event);

    void goToSuccess(EventInfo event);
}