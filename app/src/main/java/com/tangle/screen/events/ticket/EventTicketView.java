package com.tangle.screen.events.ticket;

import com.tangle.screen.events.map.BaseEventDetailView;

import java.io.File;
import java.util.List;

public interface EventTicketView extends BaseEventDetailView {
    void setQRCodeData(List<String> qrCodesUrls);

    void ticketSaved(File file);

    void showAPIError(String error);
}