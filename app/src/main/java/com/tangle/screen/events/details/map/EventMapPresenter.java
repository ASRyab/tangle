package com.tangle.screen.events.details.map;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.model.event.EventInfo;

public class EventMapPresenter extends LifecyclePresenter<EventMapView> {

    private EventInfo event;

    public void setEvent(EventInfo event) {
        this.event = event;
        if (!isViewDestroyed()) {
            getView().setEvent(event);
        }
    }

    @Override
    public void attachToView(EventMapView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (event != null) {
            view.setEvent(event);
        }
    }
}
