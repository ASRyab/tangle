package com.tangle.screen.events;

import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;

public interface MediaClickListener {
    void onMediaClicked(EventInfo event, Media media);
}
