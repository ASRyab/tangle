package com.tangle.screen.events.popup;

public enum CongratsViewType {
    BOUGHT_EVENT_TICKET,
    SEND_EVENT_TICKET
}