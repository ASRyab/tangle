package com.tangle.screen.events.details.map;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.views.StyledMapFragment;
import com.tangle.model.event.EventInfo;
import com.tangle.utils.Patterns;

public class EventMapFragment extends SecondaryScreenFragment implements EventMapView {

    private EventMapPresenter presenter = new EventMapPresenter();
    private StyledMapFragment mapFragment;
    private TextView dateTV, addressTV;
    private TextView toolbarTitleTextView;

    public static EventMapFragment newInstance(EventInfo event) {
        Bundle args = new Bundle();
        args.putSerializable("event", event);
        EventMapFragment fragment = new EventMapFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventInfo event = (EventInfo) getArguments().getSerializable("event");
        presenter.setEvent(event);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_event_map, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        dateTV = view.findViewById(R.id.txt_date);
        addressTV = view.findViewById(R.id.txt_address);
        toolbarTitleTextView = view.findViewById(R.id.txt_toolbar_title);
        toolbarTitleTextView.setVisibility(View.VISIBLE);
        initMapFragment();
    }

    private void initMapFragment() {
        mapFragment = (StyledMapFragment) getChildFragmentManager().findFragmentByTag(StyledMapFragment.class.getSimpleName());
        if (mapFragment == null) {
            mapFragment = new StyledMapFragment();
            getChildFragmentManager().beginTransaction()
                    .add(R.id.map_container, mapFragment, StyledMapFragment.class.getSimpleName())
                    .commit();
            mapFragment.setGesturesEnabled(true);
        }
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setEvent(EventInfo event) {
        mapFragment.setEvent(event);
        dateTV.setText(String.format("%s, %s",
                Patterns.DATE_FORMAT_LONG.format(event.eventStartDate),
                Patterns.TIME_FORMAT.format(event.eventStartDate)));
        addressTV.setText(event.getEventAddress(getContext()));
        toolbarTitleTextView.setText(event.title);
    }
}
