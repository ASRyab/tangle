package com.tangle.screen.events.gallery;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.R;
import com.tangle.model.event.Media;
import com.tangle.screen.events.list.MediaItem;
import com.tangle.screen.events.list.PhotoItem;
import com.tangle.screen.events.list.VideoItem;

import java.util.List;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.GalleryHolder> {

    private final RequestOptions requestOptions;
    private final int itemWidth;
    private List<Media> galleryItems;
    private MediaClickListener mediaClickListener;

    public GalleryAdapter() {
        this(RequestOptions.noTransformation(), ViewGroup.LayoutParams.MATCH_PARENT);
    }

    public GalleryAdapter(RequestOptions requestOptions, int itemWidth) {
        this.requestOptions = requestOptions;
        this.itemWidth = itemWidth;
    }

    @Override
    public GalleryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        MediaItem mediaItem;
        if (viewType == Media.Type.VIDEO.ordinal()) {
            mediaItem = new VideoItem(parent, requestOptions, R.layout.item_gallery);
            mediaItem.getView().findViewById(R.id.img_video_icon).setVisibility(View.VISIBLE);
        } else {
            mediaItem = new PhotoItem(parent, requestOptions, R.layout.item_gallery);
        }
        return new GalleryHolder(mediaItem);
    }

    @Override
    public void onBindViewHolder(GalleryHolder holder, int position) {
        Media media = getItem(position);
        holder.bindItem(media, itemWidth);
        holder.itemView.setOnClickListener(view -> {
            if (mediaClickListener != null) {
                mediaClickListener.onMediaClick(media);
            }
        });
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).type.ordinal();
    }

    @Override
    public int getItemCount() {
        return galleryItems == null ? 0 : galleryItems.size();
    }

    private Media getItem(int position) {
        return galleryItems.get(position);
    }

    public void switchGalleryItems(List<Media> galleryItems) {
        this.galleryItems = galleryItems;
        notifyDataSetChanged();
    }

    public void setMediaClickListener(MediaClickListener mediaClickListener) {
        this.mediaClickListener = mediaClickListener;
    }

    static class GalleryHolder extends RecyclerView.ViewHolder {

        MediaItem mediaItem;

        GalleryHolder(MediaItem mediaItem) {
            super(mediaItem.getView());
            this.mediaItem = mediaItem;
        }

        void bindItem(Media media, int itemWidth) {
            ViewGroup.LayoutParams layoutParams = itemView.getLayoutParams();
            layoutParams.width = itemWidth;
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
            itemView.setLayoutParams(layoutParams);
            mediaItem.setMedia(media);
        }
    }

    public interface MediaClickListener {
        void onMediaClick(Media media);
    }

}
