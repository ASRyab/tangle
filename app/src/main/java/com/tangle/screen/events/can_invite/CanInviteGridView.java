package com.tangle.screen.events.can_invite;

import com.tangle.base.PrePaymentView;
import com.tangle.base.ui.interfaces.EmptyList;
import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.profile_data.ProfileData;

import java.util.List;

public interface CanInviteGridView extends LoadingView, PrePaymentView, EmptyList {
    void setProfileList(List<ProfileData> userList);

    void goToLikebook();
}
