package com.tangle.screen.events.buy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.views.ShapeButton;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.screen.events.popup.CongratsFragment;
import com.tangle.screen.events.popup.CongratsViewType;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

public class BuyFromWalletFragment extends SecondaryScreenFragment implements BuyFromWalletView {
    private final static String EVENT_BUNDLE_KEY = "event_key";

    private BuyFromWalletPresenter presenter = new BuyFromWalletPresenter();

    private ImageView imgBackground;
    private TextView toolbarTitle, txtPrice;
    private ShapeButton btnConfirm;

    public static BuyFromWalletFragment newInstance(EventInfo invitingEvent) {
        BuyFromWalletFragment buyFromWalletFragment = new BuyFromWalletFragment();
        Bundle args = new Bundle();
        args.putSerializable(EVENT_BUNDLE_KEY, invitingEvent);
        buyFromWalletFragment.setArguments(args);
        return buyFromWalletFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundleData(getArguments());
    }

    private void getBundleData(Bundle bundle) {
        if (bundle != null) {
            presenter.setEvent((EventInfo) bundle.getSerializable(EVENT_BUNDLE_KEY));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_buy_from_wallet, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);

        initUI(view);
    }

    private void initUI(View view) {
        toolbarTitle = view.findViewById(R.id.txt_toolbar_title);
        toolbarTitle.setVisibility(View.VISIBLE);

        txtPrice = view.findViewById(R.id.tvPrice);
        imgBackground = view.findViewById(R.id.img_background);
        btnConfirm = view.findViewById(R.id.btn_confirm);
        btnConfirm.setOnClickListener(v -> presenter.onConfirmClick());
    }

    @Override
    public void setData(EventInfo event) {
        toolbarTitle.setText(getString(R.string.booking));
        txtPrice.setText(getString(R.string.event_price, event.price));
        if (event.getActualMediaList() != null && !event.getActualMediaList().isEmpty()) {
            Media media = event.getActualMediaList().get(0);
            GlideApp.with(this)
                    .load(media.type == Media.Type.PHOTO ? media.photoBig2xUrl : media.videoImagePreviewUrl)
                    .apply(RequestOptionsFactory.getDefaultBlurOptions(getActivity()))
                    .into(imgBackground);
        }
    }

    @Override
    public void goToSuccess(EventInfo event) {
        CongratsFragment fragment = CongratsFragment.newInstance(event.eventId, null, CongratsViewType.BOUGHT_EVENT_TICKET);
        switchFragment(fragment, true);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}