package com.tangle.screen.events.popup;

import com.tangle.model.event.EventInfo;
import com.tangle.model.profile_data.ProfileData;

public interface CongratsView {
    void setSendTicketUI(ProfileData user, EventInfo event);

    void setBoughtTicketUI(EventInfo event);

    void showAddToCalendarDialog(EventInfo event);

    void goBack();

    void successAndClose();
}