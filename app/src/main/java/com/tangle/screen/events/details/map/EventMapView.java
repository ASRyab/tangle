package com.tangle.screen.events.details.map;

import com.tangle.model.event.EventInfo;

public interface EventMapView {

    void setEvent(EventInfo event);

}
