package com.tangle.screen.events.participant;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class ParticipantPresenter extends LifecyclePresenter<ParticipantGridView> {
    private String eventId;
    private boolean isCommunications;
    private List<ProfileData> users = new ArrayList<>();

    public void onUserClicked(ProfileData item) {
        getView().goToUser(eventId, item);
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    public void setIsCommunications(boolean isCommunications) {
        this.isCommunications = isCommunications;
    }

    @Override
    public void attachToView(ParticipantGridView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        Observable<List<ProfileData>> userList = ApplicationLoader.getApplicationInstance().getEventManager().getEventById(eventId)
                .flatMap(info -> ApplicationLoader.getApplicationInstance().getUserManager().getUserByIds(info.participantsUsers));

        if (isCommunications) {
            userList.flatMapIterable(data -> data)
                    .filter(profileData -> profileData.isLikedMeUser || profileData.isYouLikedUser || profileData.isMatchedUser)
                    .toList()
                    .doOnSuccess(list -> users = list)
                    .subscribe(data -> getView().setProfileList(data), RxUtils.getEmptyErrorConsumer(TAG, "isCommunications=true"));
        } else {
            userList.doOnNext(list -> users = list)
                    .subscribe(data -> getView().setProfileList(data), RxUtils.getEmptyErrorConsumer(TAG, "isCommunications=false"));
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().getUpdates()
                .filter(users::contains)
                .filter(data -> data.isBlocked())
                .doOnNext(users::remove)
                .subscribe(__ -> getView().setProfileList(users), RxUtils.getEmptyErrorConsumer(TAG, "getUpdates")), State.STOP);
    }
}
