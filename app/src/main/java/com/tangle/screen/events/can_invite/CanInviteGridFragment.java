package com.tangle.screen.events.can_invite;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.PrePaymentPresenter;
import com.tangle.base.implementation.adapters.LikePeopleGridAdapter;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.views.NotificationInformationLayout;
import com.tangle.managers.DialogManager;
import com.tangle.model.payment.PaymentData;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.popup.CongratsFragment;
import com.tangle.screen.events.popup.CongratsViewType;
import com.tangle.screen.main.ActivityHolder;
import com.tangle.screen.main.navigation_menu.MenuState;

import java.util.Collections;
import java.util.List;

public class CanInviteGridFragment extends SecondaryScreenFragment implements CanInviteGridView {
    private static final int GRID_COLUMN_NUMBER = 3;
    private static final String EVENT_KEY = "eventId";
    private NotificationInformationLayout notificationInformationLayout;
    private RecyclerView canInviteRVList;

    public static CanInviteGridFragment newInstance(String eventId) {
        CanInviteGridFragment fragment = new CanInviteGridFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EVENT_KEY, eventId);
        fragment.setArguments(bundle);
        return fragment;
    }

    private CanInvitePresenter presenter = new CanInvitePresenter();
    private PrePaymentPresenter prePaymentPresenter = new PrePaymentPresenter(DialogManager.getInstance(this));

    private LikePeopleGridAdapter canInviteGridAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setEventId(getArguments().getString(EVENT_KEY));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_can_invite, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        TextView toolbarTitle = view.findViewById(R.id.txt_toolbar_title);
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarTitle.setText(getText(R.string.can_invite_fragment_title));
        canInviteRVList = view.findViewById(R.id.mutual_like_clickable_rview);
        canInviteGridAdapter = new LikePeopleGridAdapter(this::onItemClick, false);
        canInviteRVList.setLayoutManager(new GridLayoutManager(view.getContext().getApplicationContext(), GRID_COLUMN_NUMBER));
        canInviteRVList.setItemAnimator(new DefaultItemAnimator());
        canInviteRVList.setAdapter(canInviteGridAdapter);
        canInviteGridAdapter.setLoadMoreListener(canInviteRVList, null);

        notificationInformationLayout = view.findViewById(R.id.notify_rview);
        notificationInformationLayout.setVisibility(View.GONE);
        notificationInformationLayout.setClickListener(v -> presenter.onEmptyListClick());
    }

    void onItemClick(ProfileData profileData) {
        presenter.onUserChosen(profileData);
    }

    @Override
    public void setProfileList(List<ProfileData> userList) {
        canInviteGridAdapter.setProfileItems(userList);
    }

    @Override
    public void goToPP(PaymentData data) {
        ActivityHolder activity = (ActivityHolder) getActivity();
        activity.openPP(data);
    }

    @Override
    public void goToSuccess(String eventId, String userId, CongratsViewType type) {
        CongratsFragment fragment = CongratsFragment.newInstance(eventId, userId, type);
        addFragment(fragment, true);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public List<LifecyclePresenter> getPresenters() {
        return Collections.singletonList(prePaymentPresenter);
    }

    @Override
    public PrePaymentPresenter getPrePaymentPresenter() {
        return prePaymentPresenter;
    }

    @Override
    public void showEmptyListError() {
        canInviteRVList.setVisibility(View.GONE);
        notificationInformationLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyListError() {
        canInviteRVList.setVisibility(View.VISIBLE);
        notificationInformationLayout.setVisibility(View.GONE);
    }

    @Override
    public void goToLikebook() {
        moveToMenu(MenuState.TAB_LIKE_OR_NOT_INDEX);
    }
}
