package com.tangle.screen.events.details;

import android.content.Context;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.ui.views.MultipleIndicatorView;
import com.tangle.model.event.EventInfo;
import com.tangle.screen.events.MediaClickListener;
import com.tangle.screen.events.list.MediaPagerAdapter;
import com.tangle.utils.ConvertingUtils;
import com.tangle.utils.Patterns;

public class BaseEventDetailsView {

    private Context context;
    private MediaPagerAdapter pagerAdapter;
    private TextView titleTV;
    private TextView dateTV, addressTV, descriptionTV;
    MultipleIndicatorView multipleView;
    private MediaClickListener mediaClickListener;

    BaseEventDetailsView(View view, MediaClickListener mediaClickListener) {
        inflateViews(view, mediaClickListener);
    }

    private void inflateViews(View view, MediaClickListener mediaClickListener) {
        context = view.getContext();
        titleTV = view.findViewById(R.id.txt_title_medium);
        descriptionTV = view.findViewById(R.id.txt_description);
        dateTV = view.findViewById(R.id.txt_date);
        addressTV = view.findViewById(R.id.txt_address);
        multipleView = view.findViewById(R.id.view_multiple);
        this.mediaClickListener = mediaClickListener;
    }

    private void initPager(MediaClickListener mediaClickListener, EventInfo event) {
        pagerAdapter = new MediaPagerAdapter();
        pagerAdapter.setEvent(event);
        pagerAdapter.setMediaClickListener(mediaClickListener);
        multipleView.setPagerAdapter(pagerAdapter);
    }

    void destroyView() {
        if (pagerAdapter != null) {
            pagerAdapter.release();
        }
    }

    public void setEventInfo(EventInfo event) {
        setTitleMedium(event.title);
        dateTV.setText(String.format("%s, %s",
                Patterns.DATE_FORMAT_LONG.format(event.eventStartDate),
                Patterns.TIME_FORMAT.format(event.eventStartDate)));
        String address = event.getEventAddress(context);
        if (!TextUtils.isEmpty(address)) {
            addressTV.setVisibility(View.VISIBLE);
            addressTV.setText(address);
        } else {
            addressTV.setVisibility(View.INVISIBLE);
        }
        if (!TextUtils.isEmpty(event.description)) {
            descriptionTV.setVisibility(View.VISIBLE);
            Spanned spanned = Html.fromHtml(event.description);
            descriptionTV.setText(ConvertingUtils.trimTrailingWhitespace(spanned));
        } else {
            descriptionTV.setVisibility(View.GONE);
        }
        initPager(mediaClickListener, event);
        if (event.getActualMediaList() != null) {
            multipleView.setOffscreenPageLimit(event.getActualMediaList().size());
            multipleView.updateIndicator();
        }
        multipleView.setCurrentItem(0);
    }

    public void setTitleMedium(String title) {
        titleTV.setText(title);
    }
}
