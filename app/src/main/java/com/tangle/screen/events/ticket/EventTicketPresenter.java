package com.tangle.screen.events.ticket;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.events.GetEventQRCode;
import com.tangle.api.rx_tasks.payment.TicketDownload;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.State;
import com.tangle.screen.events.map.BaseEventPresenter;
import com.tangle.utils.RxUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class EventTicketPresenter extends BaseEventPresenter<EventTicketView> {

    @Override
    public void attachToView(EventTicketView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (event != null) {
            setEventQRCodeUrl(event.eventId);
        }
        monitor(ApplicationLoader.getApplicationInstance().getEventManager().getEventByIdAndUpdate(eventId)
                .subscribe(eventInfo -> {
                    setEvent(eventInfo);
                    setEventQRCodeUrl(eventInfo.eventId);
                }, RxUtils.getEmptyErrorConsumer(TAG, "setEventId")), State.DESTROY_VIEW);
    }

    private void setEventQRCodeUrl(String eventId) {
        monitor(new GetEventQRCode(eventId).getDataTask()
                .subscribe(urls -> getView().setQRCodeData(urls),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
    }

    public void onSaveTicketClicked() {
        monitor(new TicketDownload(event.eventId, ApplicationLoader.getApplicationInstance().getUserManager().getCurrentUserId()).download()
                .observeOn(AndroidSchedulers.mainThread())
                .compose(RxUtils.withLoading(getView()))
                .subscribe(file -> getView().ticketSaved(file)
                        , error -> getView().showAPIError(ErrorResponse.getServerMessage(error)))
                , State.DESTROY_VIEW);
    }
}