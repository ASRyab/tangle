package com.tangle.screen.events.list;

import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tangle.R;
import com.tangle.base.ui.views.LoaderView;
import com.tangle.base.ui.views.video_view.ScalableVideoView;
import com.tangle.model.event.Media;

public abstract class MediaItem {

    protected ViewGroup parent;
    protected ImageView previewIV;
    protected ScalableVideoView videoView;
    protected LoaderView loaderView;
    private View view;
    @LayoutRes
    private int layoutId;
    private boolean active;

    private View.OnClickListener itemClickListener;

    public MediaItem(ViewGroup parent, @LayoutRes int layoutId) {
        this.parent = parent;
        this.layoutId = layoutId;
    }

    public View getView() {
        if (view == null) {
            view = inflateView();
        }
        return view;
    }

    protected View inflateView() {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
        previewIV = view.findViewById(R.id.img_preview);
        videoView = view.findViewById(R.id.view_video);
        loaderView = view.findViewById(R.id.view_loader);
        view.setOnClickListener(v -> {
            if (itemClickListener != null) {
                itemClickListener.onClick(v);
            }
        });
        return view;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isActive() {
        return active;
    }

    public void setOnClickListener(View.OnClickListener listener) {
        itemClickListener = listener;
    }

    public void release() {
    }

    public abstract void setMedia(Media media);
}
