package com.tangle.screen.events.main;

import android.annotation.SuppressLint;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.ui.views.MultipleIndicatorView;
import com.tangle.model.event.EventInfo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.EventClickListener;
import com.tangle.screen.events.list.MediaPagerAdapter;
import com.tangle.utils.EventUtils;
import com.tangle.utils.Patterns;
import com.tangle.utils.RxUtils;

import java.util.ArrayList;

public class EventViewHolder extends RecyclerView.ViewHolder {
    private static String TAG = EventViewHolder.class.getSimpleName();
    private EventClickListener eventClickListener;
    private EventClickListener participantClickListener;
    private MultipleIndicatorView multipleView;
    private TextView titleTextView, dateTextView, addressTextView;
    private MediaPagerAdapter pagerAdapter;
    protected EventInfo event;
    private RecyclerView rvParticipants;
    private MainEventsUserAdapter mainEventsUserAdapter;
    private ArrayList<String> participantsUsers;

    TextView tvTicketStatus;

    public EventViewHolder(View itemView, EventClickListener eventClickListener, EventClickListener participantClickListener) {
        super(itemView);
        this.eventClickListener = eventClickListener;
        this.participantClickListener = participantClickListener;
        multipleView = itemView.findViewById(R.id.view_multiple);
        titleTextView = itemView.findViewById(R.id.tvTitle);
        dateTextView = itemView.findViewById(R.id.txt_date);
        addressTextView = itemView.findViewById(R.id.txt_address);
        tvTicketStatus = itemView.findViewById(R.id.txt_status);

        initParticipantView(itemView);
        itemView.setOnClickListener(v -> {
            if (eventClickListener != null) {
                eventClickListener.onEventClicked(event);
            }
        });

    }

    private void initParticipantView(View itemView) {
        rvParticipants = itemView.findViewById(R.id.rv_participants);
        LinearLayoutManager layoutManager = new LinearLayoutManager(rvParticipants.getContext(), LinearLayoutManager.HORIZONTAL, false) {
            @Override
            public boolean canScrollHorizontally() {
                return true;
            }
        };
        rvParticipants.setLayoutManager(layoutManager);
        mainEventsUserAdapter = new MainEventsUserAdapter();
        rvParticipants.setAdapter(mainEventsUserAdapter);
    }

    @SuppressLint("CheckResult")
    public void bind(EventInfo bindEvent) {
        this.event = bindEvent;
        pagerAdapter = new MediaPagerAdapter(true);
        pagerAdapter.setEvent(event);
        multipleView.setPagerAdapter(pagerAdapter);
        pagerAdapter.setMediaClickListener((event, media) -> {
            if (eventClickListener != null) {
                eventClickListener.onEventClicked(event);
            }
        });
        multipleView.setCurrentItem(0);
        titleTextView.setText(event.title);
        dateTextView.setText(String.format("%s, %s",
                Patterns.DATE_FORMAT_LONG.format(event.eventStartDate),
                Patterns.TIME_FORMAT.format(event.eventStartDate)));
        String address = event.getEventAddress(itemView.getContext());
        if (!TextUtils.isEmpty(address)) {
            addressTextView.setText(address);
        }
        participantsUsers = event.participantsUsers;
        if (participantsUsers.isEmpty()) {
            rvParticipants.setVisibility(View.GONE);
        } else {
            rvParticipants.setVisibility(View.VISIBLE);
            mainEventsUserAdapter.setParticipantClickListener(v -> participantClickListener.onEventClicked(event));
            ApplicationLoader.getApplicationInstance().getUserManager().getUserByIds(participantsUsers)
                    .subscribe(data -> mainEventsUserAdapter.setUsers(data), RxUtils.getEmptyErrorConsumer(TAG, "bind"));
            //check for blocking
            ApplicationLoader.getApplicationInstance().getUserManager().getUpdates()
                    .filter(data -> participantsUsers.contains(data.id))
                    .filter(ProfileData::isBlocked)
                    .doOnNext(data -> participantsUsers.remove(data.id))
                    .flatMap(data -> ApplicationLoader.getApplicationInstance().getUserManager().getUserByIds(participantsUsers))
                    .subscribe(data -> mainEventsUserAdapter.setUsers(data), RxUtils.getEmptyErrorConsumer(TAG, "getUpdates"));
        }

        EventUtils.updateEventStatus(tvTicketStatus, event);
    }
}

