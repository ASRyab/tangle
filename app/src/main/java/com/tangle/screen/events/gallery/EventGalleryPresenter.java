package com.tangle.screen.events.gallery;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.utils.RxUtils;

public class EventGalleryPresenter extends LifecyclePresenter<EventGalleryView> {

    private String eventId;

    @Override
    public void attachToView(EventGalleryView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        monitor(ApplicationLoader.getApplicationInstance().getEventManager().getEventById(eventId)
                .subscribe(event -> view.setGalleryItems(event.galleryMedia), RxUtils.getEmptyErrorConsumer(TAG, "attachToView")), State.DESTROY_VIEW);
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }
}
