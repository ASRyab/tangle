package com.tangle.screen.events.buy;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.model.event.EventInfo;

//deprecated
public class BuyFromWalletPresenter extends LifecyclePresenter<BuyFromWalletView> {
    private static final String TAG = BuyFromWalletPresenter.class.getSimpleName();

    private EventInfo event;

    @Override
    public void attachToView(BuyFromWalletView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (event != null) {
            view.setData(event);
        }
    }

    public void setEvent(EventInfo event) {
        this.event = event;
        if (!isViewDestroyed()) {
            getView().setData(event);
        }
    }

    void onConfirmClick() {
//        monitor(new PaymentModel(event.eventId, null, true, false)
//                        .getTask()
//                        .subscribe(ticketResponse -> {
//                                    if (ticketResponse.result) {
//                                        updateData();
//                                        getView().goToSuccess(event);
//                                    }
//                                }, RxUtils.getEmptyErrorConsumer(TAG, "onConfirmClick"))
//                , State.DESTROY_VIEW);
    }

    private void updateData() {
        ApplicationLoader.getApplicationInstance().getUserManager().loadCurrentUser();
        ApplicationLoader.getApplicationInstance().getEventManager().forceLoadEvent(event.eventId);
    }
}