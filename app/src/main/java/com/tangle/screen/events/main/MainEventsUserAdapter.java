package com.tangle.screen.events.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.ArrayList;
import java.util.List;

public class MainEventsUserAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final String TAG = MainEventsUserAdapter.class.getSimpleName();
    private List<ProfileData> users = new ArrayList<>();
    private static final int ITEM_USERS = 0;
    private static final int ITEM_MORE = 1;
    private View.OnClickListener participantClickListener;

    public void setUsers(List<ProfileData> users) {
        this.users = users;
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RecyclerView.ViewHolder holder = null;
        switch (viewType) {
            case ITEM_USERS:
                holder = new UserHolder(inflater.inflate(R.layout.item_main_user_participant, parent, false));
                break;
            case ITEM_MORE:
                holder = new MoreUsers(inflater.inflate(R.layout.item_main_more_participant, parent, false));
                break;
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case ITEM_USERS:
                ((UserHolder) holder).bind(users.get(position));
                break;
            case ITEM_MORE:
                ((MoreUsers) holder).bind(users.size());
                break;
        }
        holder.itemView.setOnClickListener(participantClickListener);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 3) {
            return ITEM_MORE;
        } else {
            return ITEM_USERS;
        }
    }

    @Override
    public int getItemCount() {
        return (users.size() > 3) ? 4 : users.size();
    }

    public void setParticipantClickListener(View.OnClickListener participantClickListener) {
        this.participantClickListener = participantClickListener;
    }

    private class UserHolder extends RecyclerView.ViewHolder {

        private ImageView userPhoto, bage;

        public UserHolder(View itemView) {
            super(itemView);
            userPhoto = itemView.findViewById(R.id.img_view_user_photo);
            bage = itemView.findViewById(R.id.img_view_is_like);
        }

        public void bind(ProfileData id) {
            bindUser(id);
        }

        private void bindUser(ProfileData data) {
            if (data.primaryPhoto != null) {
                GlideApp.with(userPhoto)
                        .load(data.primaryPhoto.avatar)
                        .placeholder(RequestOptionsFactory.getNonAvatar(data))
                        .apply(RequestOptionsFactory.getSmallRoundedCornersOptions(userPhoto.getContext()))
                        .into(userPhoto);
            }
            if (data.isMatchedUser) {
                bage.setVisibility(View.VISIBLE);
                bage.setImageResource(R.drawable.ic_mutual_like_small);
            } else if(data.isYouLikedUser) {
                bage.setVisibility(View.VISIBLE);
                bage.setImageResource(R.drawable.ic_like_small);
            } else {
                bage.setVisibility(View.INVISIBLE);
            }
        }
    }

    private class MoreUsers extends RecyclerView.ViewHolder {

        public MoreUsers(View itemView) {
            super(itemView);
        }

        public void bind(int size) {
            ((TextView) itemView.findViewById(R.id.tv_more_users))
                    .setText(itemView.getResources()
                            .getString(R.string.main_events_participant, size - 3));
        }
    }
}
