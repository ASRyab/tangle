package com.tangle.screen.events.details;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.base.mvp.State;
import com.tangle.managers.UserManager;
import com.tangle.model.event.EventInfo;
import com.tangle.screen.events.map.BaseEventPresenter;
import com.tangle.screen.events.participant.ParticipantFragment;
import com.tangle.utils.RxUtils;

import java.util.Collections;

import io.reactivex.functions.Consumer;

public class PastEventDetailsPresenter extends BaseEventPresenter<PastEventDetailsView> {

    private int mediaPage;
    private String futureEventId;

    @Override
    protected void onEvent(@NonNull EventInfo event) {
        super.onEvent(event);
        getView().setGalleryItems(event.galleryMedia);
        UserManager userManager = ApplicationLoader.getApplicationInstance().getUserManager();
        Consumer<Throwable> errorConsumer = RxUtils.getEmptyErrorConsumer(TAG, "onEvent");
        monitor(userManager.getUserByIds(event.participantsUsers).defaultIfEmpty(Collections.emptyList())
                .subscribe(participants -> getView().setParticipants(participants, isFromLinkedEvents()), errorConsumer), State.DESTROY_VIEW);
    }

    @Override
    public void attachToView(PastEventDetailsView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        monitor(ApplicationLoader.getApplicationInstance().getEventManager().getEventByIdAndUpdate(eventId)
                .subscribe(this::setEvent, RxUtils.getEmptyErrorConsumer(TAG, "attachToView")), State.DESTROY_VIEW);
        if (futureEventId != null && !futureEventId.isEmpty()) {
            monitor(ApplicationLoader.getApplicationInstance().getEventManager().getEventById(futureEventId)
                    .subscribe(info -> getView().setFutureEvent(info), RxUtils.getEmptyErrorConsumer(TAG, "attachToView")), State.DESTROY_VIEW);
        }

    }


    @Override
    public void onStart() {
        super.onStart();
        monitor(ApplicationLoader.getApplicationInstance().getEventManager().getUpdates()
//                .observeOn(AndroidSchedulers.mainThread()) // todo it's really strange
                .filter(info -> info.eventId.equals(eventId))
                .subscribe(info -> {
                    event = info;
                    setEvent(event);
                }, RxUtils.getEmptyErrorConsumer(TAG, "onStart")), State.STOP);
    }

    public void onMediaPageSelected(int page) {
        this.mediaPage = page;
        safeScrollToPosition();
    }

    public void onGalleryItemsAmountClicked() {
        getView().goToEventGallery(event.eventId);
    }

    private void safeScrollToPosition() {
        if (!isViewDestroyed()) {
            getView().scrollMediaToPosition(mediaPage);
        }
    }

    public void onParticipantsAmountClicked(ParticipantFragment.GoingFrom goingFrom) {
        getView().goToParticipantsScreen(event.eventId, goingFrom);
    }

    boolean isFromLinkedEvents() {
        return GoingFrom.LINKED_EVENTS.equals(goingFrom);
    }

    public void setGoingFromEventId(String futureEventId) {
        this.futureEventId = futureEventId;
    }


    @Override
    public void invite() {
        getView().goToInviteScreen(futureEventId);
    }

}
