package com.tangle.screen.events.map;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.base.ui.interfaces.LocationCheckableView;
import com.tangle.managers.PhotoManager;
import com.tangle.model.event.EventInfo;

import java.util.List;

public interface BaseEventDetailView extends LoadingView, PhotoManager.PhotoView, LocationCheckableView {

    void goToMap(EventInfo event);

    void setEventInfo(EventInfo event);

    void setEventMap(EventInfo event);

    void updateMapState(boolean shouldDisplayStub);

    void setLinkedEvents(List<EventInfo> linkedEvents, boolean isFutureEvents);

    void goToInviteScreen(String eventId);

}