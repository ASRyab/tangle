package com.tangle.screen.events.details;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.load.MultiTransformation;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.PlatformUtils;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.ArrayList;
import java.util.List;

public class ProfilesAdapter extends RecyclerView.Adapter<ProfilesAdapter.ProfileViewHolder> {

    private final ProfileClickListener clickListener;
    private List<ProfileData> profiles = new ArrayList<>();

    public ProfilesAdapter(ProfileClickListener listener) {
        this.clickListener = listener;
    }

    @Override
    public ProfileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProfileViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_profile, parent, false));
    }

    @Override
    public void onBindViewHolder(ProfileViewHolder holder, int position) {
        holder.bind(profiles.get(position));
    }

    @Override
    public int getItemCount() {
        return profiles.size();
    }

    public class ProfileViewHolder extends RecyclerView.ViewHolder {

        private ImageView imgUserPhoto, imgMutualLikeIndicator, imgLikeIndicator;

        public ProfileViewHolder(View itemView) {
            super(itemView);
            imgUserPhoto = itemView.findViewById(R.id.img_view_user_photo);
            imgMutualLikeIndicator = itemView.findViewById(R.id.img_view_is_mutual_like);
            imgLikeIndicator = itemView.findViewById(R.id.img_view_is_like);
        }

        public void bind(ProfileData profile) {
            if (profile.isMatchedUser) {
                imgMutualLikeIndicator.setVisibility(View.VISIBLE);
                imgLikeIndicator.setVisibility(View.INVISIBLE);
            } else if (profile.isYouLikedUser) {
                imgLikeIndicator.setVisibility(View.VISIBLE);
                imgMutualLikeIndicator.setVisibility(View.INVISIBLE);
            } else {
                imgLikeIndicator.setVisibility(View.INVISIBLE);
                imgMutualLikeIndicator.setVisibility(View.INVISIBLE);
            }
            itemView.setOnClickListener(view -> clickListener.onProfileClick(profile.id));
            boolean hasPhoto = profile.primaryPhoto != null && profile.primaryPhoto.avatar != null;
            imgUserPhoto.setVisibility(hasPhoto ? View.VISIBLE : View.INVISIBLE);
            if (hasPhoto) {
                GlideApp.with(imgUserPhoto)
                        .load(profile.primaryPhoto.normal)
                        .placeholder(RequestOptionsFactory.getNonAvatar(profile))
                        .apply(getUserPhotoRequestOptions())
                        .into(imgUserPhoto);
            }
        }

        private RequestOptions getUserPhotoRequestOptions() {
            return RequestOptions.bitmapTransform(new MultiTransformation<>(new CenterCrop(),
                    new RoundedCorners((int) PlatformUtils.Dimensions.dipToPixels(ApplicationLoader.getContext(), 3))));
        }
    }

    public void switchProfiles(List<ProfileData> profiles) {
        this.profiles = profiles;
        notifyDataSetChanged();
    }

    public interface ProfileClickListener {
        void onProfileClick(String id);
    }
}
