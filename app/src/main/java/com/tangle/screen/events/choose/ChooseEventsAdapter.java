package com.tangle.screen.events.choose;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.ui.views.ShapeButton;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.utils.EventUtils;
import com.tangle.utils.Patterns;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ChooseEventsAdapter extends RecyclerView.Adapter<ChooseEventsAdapter.EventViewHolder> {
    private ChooseEventClickListener chooseEventClickListener;
    private List<EventInfo> events = new ArrayList<>();
    private ShowedEventsType showedEventsType;

    public ChooseEventsAdapter(ShowedEventsType showedEventsType) {
        this.showedEventsType = showedEventsType;
    }

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EventViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_choose_event, parent, false));
    }

    @Override
    public void onBindViewHolder(ChooseEventsAdapter.EventViewHolder holder, int position) {
        holder.bind(events.get(position));
    }

    @Override
    public int getItemCount() {
        return events == null ? 0 : events.size();
    }

    void switchEvents(List<EventInfo> events) {
        this.events = events;
        notifyDataSetChanged();
    }

    class EventViewHolder extends RecyclerView.ViewHolder {
        EventInfo event;

        TextView titleTxtView;
        TextView dateTxtView;
        TextView timeTxtView;
        TextView tvTicketStatus;
        ImageView previewImgView;
        ShapeButton actionBtn;

        int primaryTextColor, secondaryTextColor;

        EventViewHolder(View itemView) {
            super(itemView);
            titleTxtView = itemView.findViewById(R.id.tvTitle);
            dateTxtView = itemView.findViewById(R.id.txt_date);
            timeTxtView = itemView.findViewById(R.id.tvTime);
            tvTicketStatus = itemView.findViewById(R.id.tvTicketStatus);
            previewImgView = itemView.findViewById(R.id.img_preview);
            Context context = itemView.getContext().getApplicationContext();
            primaryTextColor = ContextCompat.getColor(context, R.color.colorTextPrimary);
            secondaryTextColor = ContextCompat.getColor(context, R.color.colorTextSecondary);
            itemView.setOnClickListener(v -> {
                if (chooseEventClickListener != null) {
                    chooseEventClickListener.onEventClicked(event);
                }
            });
        }

        private void initButton(View itemView) {
            actionBtn = itemView.findViewById(R.id.action_btn);
            if (showedEventsType.equals(ShowedEventsType.PAST_EVENTS)) {
                actionBtn.setVisibility(View.GONE);
                return;
            }
            Context context = itemView.getContext();
            String buttonText = context.getString(R.string.invite);
            switch (showedEventsType) {
                case USER_WILL_GO:
                    buttonText = event.getBookButtonText(context);
                    actionBtn.setEnabled(event.isBookAvailable());
                    break;
                case OWN_EVENTS:
                    buttonText = context.getString(R.string.show_ticket);
                    break;
            }
            actionBtn.setText(buttonText);
            actionBtn.setOnClickListener(v -> {
                if (chooseEventClickListener != null) {
                    chooseEventClickListener.onButtonClicked(event);
                }
            });
        }

        void bind(EventInfo event) {
            this.event = event;
            titleTxtView.setText(event.title);
            setDate();
            setPicture();
            initButton(itemView);
            EventUtils.updateEventStatus(tvTicketStatus, event);
        }

        private void setPicture() {
            List<Media> actualMediaList = event.getActualMediaList();
            if (actualMediaList != null && !event.getActualMediaList().isEmpty()) {
                Media media = event.getActualMediaList().get(0);
                Context context = itemView.getContext().getApplicationContext();
                GlideApp.with(context)
                        .load(media.type == Media.Type.PHOTO ? media.photoThumbnail2xUrl : media.videoImagePreviewUrl)
                        .apply(RequestOptionsFactory.getSmallRoundedCornersOptions(context))
                        .into(previewImgView);
            }
        }

        private void setDate() {
            Date launchDate = event.eventStartDate;
            if (DateUtils.isToday(launchDate.getTime())) {
                dateTxtView.setBackgroundResource(R.drawable.own_profile_date_background);
                dateTxtView.setTextColor(primaryTextColor);
                dateTxtView.setText(itemView.getResources().getString(R.string.own_profile_today));
            } else {
                dateTxtView.setBackgroundResource(0);
                dateTxtView.setBackground(null);
                dateTxtView.setTextColor(secondaryTextColor);
                dateTxtView.setText(String.format("%s,", Patterns.DATE_FORMAT_SHORT.format(launchDate)));
            }
            timeTxtView.setText(Patterns.TIME_FORMAT.format(launchDate));
        }

        void setBoughtState() {
            tvTicketStatus.setVisibility(View.VISIBLE);
            tvTicketStatus.setBackground(ContextCompat.getDrawable(tvTicketStatus.getContext(), R.drawable.own_profile_date_background));
            tvTicketStatus.setText(tvTicketStatus.getResources().getString(R.string.bought));
        }

    }

    void setClickListener(ChooseEventClickListener chooseEventClickListener) {
        this.chooseEventClickListener = chooseEventClickListener;
    }
}