package com.tangle.screen.events.list;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.R;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.screen.events.MediaClickListener;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.ArrayList;
import java.util.List;

public class MediaPagerAdapter extends PagerAdapter {

    private boolean isMainScreen;
    private int lastPosition = -1;
    private List<MediaItem> mediaItems = new ArrayList<>();
    private MediaItem primaryItem;
    private EventInfo event;
    private MediaClickListener mediaClickListener;

    public MediaPagerAdapter(boolean isMainScreen) {
        this.isMainScreen = isMainScreen;
    }

    public MediaPagerAdapter() {
    }

    public void setEvent(EventInfo event) {
        this.event = event;
        notifyDataSetChanged();

    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        RequestOptions requestOptions = RequestOptionsFactory.getMediumRoundedCornersOptions(container.getContext());
        Media media = event.getActualMediaList().get(position);
        MediaItem item = null;
        switch (media.type) {
            case PHOTO:
                item = new PhotoItem(container, requestOptions, R.layout.item_event_pager);
                break;
            case VIDEO:
                item = new VideoItem(container, requestOptions, R.layout.item_event_pager);
                break;
        }
        setListeners(item, media);
        item.setMedia(media);
        mediaItems.add(item);
        View view = item.getView();
        view.setTag(media);
        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
        mediaItems.get(position).release();
    }

    @Override
    public void setPrimaryItem(ViewGroup container, int position, Object object) {
        super.setPrimaryItem(container, position, object);
        if (lastPosition != position) {
            lastPosition = position;
            updatePrimaryItem(position);
        }
    }

    private void updatePrimaryItem(int position) {
        if (position >= mediaItems.size()) return;
        primaryItem = mediaItems.get(position);
        if (primaryItem != null) {
            primaryItem.setActive(true);
        }
        for (MediaItem mediaItem : mediaItems) {
            if (mediaItem != null && !mediaItem.equals(primaryItem)) {
                mediaItem.setActive(false);
            }
        }
    }

    private void setListeners(MediaItem item, Media media) {
        item.setOnClickListener(v -> {
            if (mediaClickListener != null) {
                mediaClickListener.onMediaClicked(event, media);
            }
        });
    }

    public void setActive(boolean active) {
        for (MediaItem mediaItem : mediaItems) {
            mediaItem.setActive(active);
        }
    }

    public void release() {
        for (MediaItem mediaItem : mediaItems) {
            mediaItem.release();
        }
    }

    @Override
    public int getCount() {
        return (event == null || event.getActualMediaList() == null || event.getActualMediaList().isEmpty()) ? 0 : getEventCount();
    }

    private int getEventCount() {
        return isMainScreen ? 1 : event.getActualMediaList().size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        Object tag = ((View) object).getTag();
        return tag != null && tag.equals(view.getTag());
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public EventInfo getEvent() {
        return event;
    }

    public void setMediaClickListener(MediaClickListener mediaClickListener) {
        this.mediaClickListener = mediaClickListener;
    }
}
