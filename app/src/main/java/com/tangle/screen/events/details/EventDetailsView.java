package com.tangle.screen.events.details;

import com.tangle.base.FreePaymentView;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.TicketStatusConstant;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.map.BaseEventDetailView;
import com.tangle.screen.events.participant.ParticipantFragment;

import java.util.List;

public interface EventDetailsView extends BaseEventDetailView, FreePaymentView {

    void setCanInvite(List<ProfileData> profiles);

    void setParticipants(List<ProfileData> profiles, boolean isReadOnly, boolean isFromLinkedEvents);

    void goToParticipantsScreen(String eventId, ParticipantFragment.GoingFrom goingFrom);

    void scrollMediaToPosition(int position);

    void goToLikeBook();

    void setInviteButtonVisible(boolean visible);

    void setBookButtonVisible(boolean visible, boolean isBooked);

    void showQRCodeFragment(String eventId);

    void setSoldOutVisible(boolean isVisible);

    void initFreeEventActionButton(TicketStatusConstant freeTicketStatus);

    void setLinkedEvents(List<EventInfo> linkedEvents, boolean isFutureEvents);

}