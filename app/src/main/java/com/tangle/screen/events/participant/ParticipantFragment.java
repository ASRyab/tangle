package com.tangle.screen.events.participant;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.analytics.SimpleActions;
import com.tangle.base.implementation.adapters.TabViewPagerAdapter;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;

public class ParticipantFragment extends SecondaryScreenFragment {
    private static final int TAB_CONNECTIONS = 1;
    private static final int TAB_ALL = 0;
    private static final String BUNDLE_EVENT_KEY = "key_ids";
    private static final String BUNDLE_TITLE = "key_title";
    private static final String GOING_FROM_KEY = "going_from";
    private TabLayout participantTabs;
    private ViewPager participantVP;
    private String eventId;
    private int titleId;
    private GoingFrom goingFrom;

    public ParticipantFragment() {
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return null;
    }

    public static ParticipantFragment newInstance(String eventId) {
        return newInstance(eventId, GoingFrom.OTHER);
    }

    public static ParticipantFragment newInstance(String eventId, GoingFrom goingFrom) {
        ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.EVENT_CLICK_PARTICIPANTS);
        ParticipantFragment participantFragment = new ParticipantFragment();
        boolean isPast = goingFrom.equals(GoingFrom.PAST) || goingFrom.equals(GoingFrom.LINKED_EVENT_PAST);
        Bundle args = new Bundle();
        args.putString(BUNDLE_EVENT_KEY, eventId);
        args.putInt(BUNDLE_TITLE, isPast ? R.string.participants : R.string.event_details_participants);
        args.putSerializable(GOING_FROM_KEY, goingFrom);

        participantFragment.setArguments(args);
        return participantFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_participant, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        eventId = getArguments().getString(BUNDLE_EVENT_KEY);
        titleId = getArguments().getInt(BUNDLE_TITLE);
        goingFrom = (GoingFrom) getArguments().getSerializable(GOING_FROM_KEY);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        ((TextView) view.findViewById(R.id.txt_toolbar_title)).setVisibility(View.VISIBLE);
        ((TextView) view.findViewById(R.id.txt_toolbar_title)).setText(titleId);

        participantVP = view.findViewById(R.id.vp_participant);
        participantVP.setOffscreenPageLimit(2);
        setupViewPager(participantVP);


        participantTabs = view.findViewById(R.id.tab_participant);
        participantTabs.setupWithViewPager(participantVP);

        setupTab(TAB_ALL, false);
        setupTab(TAB_CONNECTIONS, false);

    }

    private void setupViewPager(ViewPager viewPager) {
        TabViewPagerAdapter adapter = new TabViewPagerAdapter(getChildFragmentManager());

        boolean isClicksEnabled = goingFrom != GoingFrom.LINKED_EVENT && goingFrom != GoingFrom.LINKED_EVENT_PAST;

        adapter.addFragment(ParticipantGridFragment.newInstance(eventId, false, isClicksEnabled), "");
        adapter.addFragment(ParticipantGridFragment.newInstance(eventId, true, isClicksEnabled), "");
        viewPager.setAdapter(adapter);
    }

    public void setupTab(int tabIndex, boolean showIndicator) {
        View customTabView = participantTabs.getTabAt(tabIndex).getCustomView();
        if (customTabView != null) {
            customTabView.findViewById(R.id.tab_indicator).setVisibility(showIndicator ? View.VISIBLE : View.GONE);
        } else {
            Context context = ApplicationLoader.getContext();
            View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_tab_item, null);
            TextView title = view.findViewById(R.id.title);
            switch (tabIndex) {
                case TAB_CONNECTIONS:
                    title.setText(context.getString(R.string.tab_participant_connections));
                    break;
                case TAB_ALL:
                    title.setText(context.getString(R.string.tab_participant_all));
                    break;
            }
            view.findViewById(R.id.tab_indicator).setVisibility(showIndicator ? View.VISIBLE : View.GONE);
            participantTabs.getTabAt(tabIndex).setCustomView(view);
        }
    }

    public enum GoingFrom {
        LINKED_EVENT_PAST,
        LINKED_EVENT,
        PAST,
        OTHER
    }
}
