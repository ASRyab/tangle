package com.tangle.screen.events.ticket;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.views.circle_indicator.CirclePageIndicator;
import com.tangle.model.event.EventInfo;
import com.tangle.screen.events.map.BaseEventDetailFragment;
import com.tangle.utils.Patterns;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.util.List;

public class EventTicketFragment extends BaseEventDetailFragment implements EventTicketView, OnMapReadyCallback {
    private static final String BUNDLE_EVENT_ID_KEY = "event_id_key";

    private EventTicketPresenter presenter = new EventTicketPresenter();

    private ViewPager vpEvents;
    private CirclePageIndicator vpIndicator;
    private TextView txtDate, txtLocation, txtTitle;

    public static EventTicketFragment newInstance(String eventId) {
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_EVENT_ID_KEY, eventId);
        EventTicketFragment fragment = new EventTicketFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String eventId = getArguments().getString(BUNDLE_EVENT_ID_KEY);
        presenter.setEventId(eventId);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_event_ticket, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        initToolbar("", true);
        txtTitle = view.findViewById(R.id.tvTitle);
        txtDate = view.findViewById(R.id.txt_date);
        txtLocation = view.findViewById(R.id.txt_address);
        vpEvents = view.findViewById(R.id.events_vpager);
        vpIndicator = view.findViewById(R.id.events_vpager_indicator);
        view.findViewById(R.id.btn_save_ticket).setOnClickListener(v ->
                new RxPermissions(getActivity())
                .request(Manifest.permission.WRITE_EXTERNAL_STORAGE).filter(granted -> granted)
                .subscribe(granted -> presenter.onSaveTicketClicked()));
    }

    @Override
    public void setEventInfo(EventInfo event) {
        txtTitle.setText(event.title);
        txtDate.setText(String.format("%s, %s",
                Patterns.DATE_FORMAT_LONG.format(event.eventStartDate),
                Patterns.TIME_FORMAT.format(event.eventStartDate)));
        String address = event.getEventAddress(getContext());
        if (!TextUtils.isEmpty(address)) {
            txtLocation.setVisibility(View.VISIBLE);
            txtLocation.setText(address);
        } else {
            txtLocation.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void setQRCodeData(List<String> qrCodesUrls) {
        vpEvents.setAdapter(new QRCodePagerAdapter(vpEvents.getContext(), qrCodesUrls));
        vpIndicator.setViewPager(vpEvents);
    }

    @Override
    public void ticketSaved(File file) {
        Toast.makeText(getContext(),R.string.ticket_saved,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        presenter.onMapInitialized();
    }

    @Override
    protected void onMapClicked() {
        presenter.onMapClicked();
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}