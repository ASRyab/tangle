package com.tangle.screen.events.can_invite;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.analytics.Via;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.event.EventInfo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.RxUtils;

import io.reactivex.disposables.Disposable;

public class CanInvitePresenter extends LifecyclePresenter<CanInviteGridView> {

    private EventInfo event;
    private String eventId;

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    @Override
    public void attachToView(CanInviteGridView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        Disposable profilesDisposable = ApplicationLoader.getApplicationInstance().getEventManager().getEventById(eventId)
                .doOnNext(eventInfo -> event = eventInfo)
                .flatMap(info -> ApplicationLoader.getApplicationInstance().getUserManager().getUserByIds(info.usersForInvite))
                .subscribe(data -> {
                    if (data.isEmpty()) {
                        getView().showEmptyListError();
                    } else {
                        getView().setProfileList(data);
                    }
                }, RxUtils.getEmptyErrorConsumer());
        monitor(profilesDisposable, State.DESTROY_VIEW);
    }

    public void onUserChosen(ProfileData data) {
        ApplicationLoader.getApplicationInstance().getAnalyticManager().setVia(Via.EVENT_INVITE);
        getView().getPrePaymentPresenter().onInviteClicked(event, data);
    }

    public void onEmptyListClick() {
        getView().goToLikebook();
    }
}
