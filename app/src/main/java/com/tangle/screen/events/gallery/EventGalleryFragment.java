package com.tangle.screen.events.gallery;

import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.layoutmanagers.SpannableGridLayoutManager;
import com.tangle.model.event.Media;
import com.tangle.screen.media.pager.MediaContainerFragment;
import com.tangle.utils.GalleryUtils;
import com.tangle.utils.PlatformUtils;

import java.util.List;

public class EventGalleryFragment extends SecondaryScreenFragment implements EventGalleryView {
    private static final String BUNDLE_EVENT_KEY = "events_key";

    public static EventGalleryFragment newInstance(String eventId) {
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_EVENT_KEY, eventId);
        EventGalleryFragment fragment = new EventGalleryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    EventGalleryPresenter presenter = new EventGalleryPresenter();

    private RecyclerView galleryRV;
    private GalleryAdapter galleryAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String eventId = (String) getArguments().getSerializable(BUNDLE_EVENT_KEY);
        presenter.setEventId(eventId);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_event_gallery, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        galleryRV = view.findViewById(R.id.rv_gallery);
        initUI(view);
        initGallery();
    }

    private void initUI(View view) {
        TextView toolbarTitle = view.findViewById(R.id.txt_toolbar_title);
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarTitle.setText(getText(R.string.event_details_gallery));
    }

    private void initGallery() {
        SpannableGridLayoutManager.GridSpanLookup gridSpanLookup = position -> GalleryUtils.isHugeItem(position) ? new SpannableGridLayoutManager.SpanInfo(2, 2)
                : new SpannableGridLayoutManager.SpanInfo(1, 1);
        SpannableGridLayoutManager layoutManager = new SpannableGridLayoutManager(gridSpanLookup, 3, 1f);
        galleryRV.setLayoutManager(layoutManager);
        galleryAdapter = new GalleryAdapter();
        galleryRV.setAdapter(galleryAdapter);
        final int itemOffset = (int) PlatformUtils.Dimensions.dipToPixels(getContext(), 6);
        galleryRV.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                final int itemPosition = parent.getChildAdapterPosition(view);
                outRect.left = GalleryUtils.isStartItem(itemPosition) ? 0 : itemOffset;
                outRect.right = GalleryUtils.isEndItem(itemPosition) ? 0 : itemOffset;
                outRect.top = GalleryUtils.isTopItem(itemPosition) ? 0 : itemOffset;
                outRect.bottom = itemOffset;
            }
        });
    }

    @Override
    public void setGalleryItems(List<Media> galleryItems) {
        galleryAdapter.switchGalleryItems(galleryItems);
        galleryAdapter.setMediaClickListener(media -> {
            MediaContainerFragment mediaContainerFragment = MediaContainerFragment.newInstance(galleryItems, media, MediaContainerFragment.ShowType.PAST_EVENT);
            addFragment(mediaContainerFragment, true);
        });
    }


    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}
