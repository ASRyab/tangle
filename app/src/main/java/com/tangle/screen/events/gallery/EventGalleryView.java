package com.tangle.screen.events.gallery;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.event.Media;

import java.util.List;

public interface EventGalleryView extends LoadingView {
    void setGalleryItems(List<Media> galleryItems);
}
