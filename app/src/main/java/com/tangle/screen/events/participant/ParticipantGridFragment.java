package com.tangle.screen.events.participant;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.implementation.adapters.LikePeopleGridAdapter;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.other_profile.ProfileEventPresenter;

import java.util.List;

public class ParticipantGridFragment extends SecondaryScreenFragment implements ParticipantGridView, LikePeopleGridAdapter.GridItemClickListener {
    private static final int GRID_COLUMN_NUMBER = 3;
    private static final String EVENT_KEY = "eventId";
    private static final String IS_COMMUNICATIONS_KEY = "is_communications";
    private static final String GRID_CLICK_ENABLED = "grid_click_enabled";
    private boolean isGridClicksEnabled;
    private RecyclerView participantRVList;
    private LikePeopleGridAdapter participantGridAdapter;

    private ParticipantPresenter presenter = new ParticipantPresenter();
    private View notificationInformationLayout;

    public ParticipantGridFragment() {
    }

    public static ParticipantGridFragment newInstance(String eventId, boolean isCommunications, boolean gridClicksEnabled) {
        ParticipantGridFragment fragment = new ParticipantGridFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EVENT_KEY, eventId);
        bundle.putBoolean(IS_COMMUNICATIONS_KEY, isCommunications);
        bundle.putBoolean(GRID_CLICK_ENABLED, gridClicksEnabled);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setEventId(getArguments().getString(EVENT_KEY));
        presenter.setIsCommunications(getArguments().getBoolean(IS_COMMUNICATIONS_KEY));
        this.isGridClicksEnabled = getArguments().getBoolean(GRID_CLICK_ENABLED, true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_participant_grid, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        notificationInformationLayout = view.findViewById(R.id.notify_view);
        participantRVList = view.findViewById(R.id.participant_rv);
        participantGridAdapter = new LikePeopleGridAdapter(isGridClicksEnabled ? this : null, false);
        participantRVList.setLayoutManager(new GridLayoutManager(view.getContext().getApplicationContext(), GRID_COLUMN_NUMBER));
        participantRVList.setItemAnimator(new DefaultItemAnimator());
        participantRVList.setAdapter(participantGridAdapter);
        participantGridAdapter.setLoadMoreListener(participantRVList, null);
    }

    @Override
    public void setProfileList(List<ProfileData> likedYouList) {
        participantGridAdapter.setProfileItems(likedYouList);
        if (!likedYouList.isEmpty()) {
            notificationInformationLayout.setVisibility(View.GONE);
        } else {
            notificationInformationLayout.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onItemClick(ProfileData item) {
        presenter.onUserClicked(item);
    }

    @Override
    public void goToUser(String eventId, ProfileData profileData) {
        ApplicationLoader.getApplicationInstance().getNavigationManager().showUserProfile(profileData.id, ProfileEventPresenter.GoingFrom.PARTICIPANTS, eventId);
    }
}