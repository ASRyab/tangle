package com.tangle.screen.events.choose;

import com.tangle.base.PrePaymentView;
import com.tangle.managers.PhotoManager;
import com.tangle.model.event.EventInfo;

import java.util.List;

public interface ChooseEventView extends PrePaymentView, PhotoManager.PhotoView {
    void setEvents(List<EventInfo> events);

    void showEmptyLayout();

    void initAdapter(ShowedEventsType showedEventsType);

    void showAPIError(String error);

    void goToEventInfo(EventInfo event, ChooseEventFragment.GoingFrom goingFrom);

    void goToTickets(EventInfo event);

    void goToEvents();
}