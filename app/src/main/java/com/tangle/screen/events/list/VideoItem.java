package com.tangle.screen.events.list;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.danikula.videocache.HttpProxyCacheServer;
import com.tangle.ApplicationLoader;
import com.tangle.model.event.Media;
import com.tangle.utils.glide.GlideApp;

public class VideoItem extends MediaItem {

    private HttpProxyCacheServer cacheProxy = ApplicationLoader.getApplicationInstance().getCacheProxy();
    private final RequestOptions requestOptions;
    private boolean playerInitialized;
    private Media media;
    private String lastVideoUrl;

    public VideoItem(ViewGroup parent, RequestOptions requestOptions, int layoutId) {
        super(parent, layoutId);
        this.requestOptions = requestOptions;
    }

    @Override
    public void setMedia(Media media) {
        getView();
        this.media = media;
        startVideo();
    }

    private void startVideo() {
        setPlayerVisibility(false);
        loaderView.setVisibility(View.VISIBLE);
        GlideApp.with(previewIV)
                .load(media.videoImagePreviewUrl)
                .apply(requestOptions)
                .into(new DrawableImageViewTarget(previewIV) {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        super.onResourceReady(resource, transition);
                        loaderView.setVisibility(View.INVISIBLE);
                    }
                });
    }

    private void playVideo() {
        String videoUrl = media.videoOriginUrl;
        if (videoUrl.equals(lastVideoUrl)) {
            // can start initialize before, but hasn't initialized yet. do nothing, wait
            if (playerInitialized) {
                startVideoInternal();
            }
        } else {
            lastVideoUrl = videoUrl;
            videoView.setDataSourceAsync(cacheProxy.getProxyUrl(videoUrl), () -> {
                videoView.setLooping(true);
                videoView.prepareAsync(mp -> {
                    playerInitialized = true;
                    mp.setVolume(0, 0);
                    startVideoInternal();
                });
            });
        }
    }

    private void startVideoInternal() {
        setPlayerVisibility(true);
        videoView.start();
    }

    private void setPlayerVisibility(boolean visible) {
        previewIV.setVisibility(visible ? View.INVISIBLE : View.VISIBLE);
        videoView.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    private void pauseVideo() {
        if (playerInitialized && videoView != null && videoView.isPlaying()) {
            videoView.pause();
        }
    }

    @Override
    public void setActive(boolean active) {
        super.setActive(active);
        if (active) {
            playVideo();
        } else {
            pauseVideo();
        }
    }

    @Override
    public void release() {
        super.release();
        playerInitialized = false;
        lastVideoUrl = null;
        videoView.release();
    }
}
