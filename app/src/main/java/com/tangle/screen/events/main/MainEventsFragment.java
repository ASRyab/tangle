package com.tangle.screen.events.main;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.transition.Fade;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.model.event.EventInfo;
import com.tangle.screen.brand_blog.BrandBlogFragment;
import com.tangle.screen.brand_blog.BrandBlogModel;
import com.tangle.screen.events.details.EventDetailsFragment;
import com.tangle.screen.events.participant.ParticipantFragment;
import com.tangle.screen.media.video.promo.BlogPromoVideoFragment;
import com.tangle.screen.media.video.promo.FreeEventPromoVideoFragment;

import java.util.List;

public class MainEventsFragment extends SecondaryScreenFragment implements MainEventsView {
    private MainEventsPresenter presenter = new MainEventsPresenter(new BrandBlogModel(ApplicationLoader.getApplicationInstance().getBlogInfoManager()));
    private MainEventsAdapter adapter;
    private ViewGroup loaderContainer;
    private RecyclerView eventsRecyclerView;
    private SwipeRefreshLayout swipeContainer;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_events_main, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        loaderContainer = view.findViewById(R.id.container_loader);
        initSwipeRefresh(view);
        eventsRecyclerView = view.findViewById(R.id.list_events);
        eventsRecyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        adapter = new MainEventsAdapter(event -> presenter.onEventClicked(event)
                , event ->
                switchFragment(ParticipantFragment.newInstance(event.eventId), true),
                v -> presenter.onBrandBlogClicked());
        eventsRecyclerView.setAdapter(adapter);
    }

    public void openBrandBlogPromo() {
        addFragment(new BlogPromoVideoFragment(), true);
    }

    public void openBrandBlog() {
        BrandBlogFragment fragment = new BrandBlogFragment();
        fragment.setEnterTransition(new Fade());
        setExitTransition(new Fade());
        switchFragment(fragment, true);
    }

    private void initSwipeRefresh(View view) {
        swipeContainer = view.findViewById(R.id.swipeContainer);
        swipeContainer.setOnRefreshListener(() -> presenter.refreshData());
        swipeContainer.setColorSchemeResources(R.color.colorPrimaryDark);
        swipeContainer.setProgressBackgroundColorSchemeResource(R.color.colorPrimary);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    private void setLoaderVisible(boolean visible) {
        loaderContainer.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
        eventsRecyclerView.setVisibility(visible ? View.INVISIBLE : View.VISIBLE);
    }

    @Override
    public void setUpcomingEvents(List<EventInfo> events) {
        adapter.switchMainEvents(events);
        hideSwipeLoadingContainer();
    }

    @Override
    public void updateNewPostBadge(int count) {
        adapter.updateUnread(count);
    }

    @Override
    public void showPromo() {
        addFragment(new FreeEventPromoVideoFragment(), true);
    }

    @Override
    public void showEvent(EventInfo event) {
        switchFragment(EventDetailsFragment.newInstance(event), true);
    }

    @Override
    public void showLoading() {
        setLoaderVisible(true);
    }

    @Override
    public void hideLoading() {
        hideSwipeLoadingContainer();
        setLoaderVisible(false);
    }

    private void hideSwipeLoadingContainer() {
        if (swipeContainer.isRefreshing()) {
            swipeContainer.setRefreshing(false);
        }
    }
}