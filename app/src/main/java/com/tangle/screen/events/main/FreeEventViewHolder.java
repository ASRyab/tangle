package com.tangle.screen.events.main;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.implementation.adapters.InterestsAdapter;
import com.tangle.base.ui.recyclerview.layout_managers.flex.FlexDirection;
import com.tangle.base.ui.recyclerview.layout_managers.flex.FlexWrap;
import com.tangle.base.ui.recyclerview.layout_managers.flex.FlexboxLayoutManager;
import com.tangle.base.ui.recyclerview.layout_managers.flex.JustifyContent;
import com.tangle.model.event.EventInfo;
import com.tangle.screen.events.EventClickListener;
import com.tangle.utils.EventUtils;

public class FreeEventViewHolder extends EventViewHolder {
    private TextView tvLocation;
    private RecyclerView rvBadges;
    private InterestsAdapter badgeAdapter;

    public FreeEventViewHolder(View itemView, EventClickListener eventClickListener, EventClickListener participantClickListener) {
        super(itemView, eventClickListener, participantClickListener);
        initBages(itemView);
        tvLocation = itemView.findViewById(R.id.txt_location);
    }

    @Override
    public void bind(EventInfo bindEvent) {
        super.bind(bindEvent);
        tvLocation.setText(bindEvent.getEventLocation());
        badgeAdapter.switchInterests(EventUtils.initEventBadges(tvLocation.getContext()));
        initTicketStatus();
    }

    private void initTicketStatus() {
        switch (event.getFreeTicketStatus()) {
            case APPROVED:
                tvTicketStatus.setVisibility(View.VISIBLE);
                tvTicketStatus.setText(R.string.confirmed);
                tvTicketStatus.setBackgroundResource(R.drawable.own_profile_date_background);
                break;
            case WAIT_APPROVE:
                tvTicketStatus.setVisibility(View.VISIBLE);
                tvTicketStatus.setText(R.string.requested);
                tvTicketStatus.setBackgroundResource(R.drawable.reserved_label_background);
                break;
        }
    }

    private void initBages(View view) {
        rvBadges = view.findViewById(R.id.list_badges);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(view.getContext(), FlexDirection.ROW, FlexWrap.WRAP);
        layoutManager.setJustifyContent(JustifyContent.CENTER);
        rvBadges.setLayoutManager(layoutManager);
        badgeAdapter = new InterestsAdapter();
        badgeAdapter.setSelectAvailable(false);
        rvBadges.setAdapter(badgeAdapter);
    }
}
