package com.tangle.screen.events.choose;

public enum ShowedEventsType {
    USER_WILL_GO,
    OWN_EVENTS,
    PAST_EVENTS,
    USER_UPCOMING_EVENT
}