package com.tangle.screen.events;

import com.tangle.model.event.EventInfo;

public interface EventClickListener {
    void onEventClicked(EventInfo event);
}