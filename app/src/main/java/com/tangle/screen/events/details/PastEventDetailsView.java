package com.tangle.screen.events.details;

import com.tangle.base.PrePaymentView;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.map.BaseEventDetailView;
import com.tangle.screen.events.participant.ParticipantFragment;

import java.util.List;

public interface PastEventDetailsView extends BaseEventDetailView,PrePaymentView  {

    void setGalleryItems(List<Media> galleryItems);

    void scrollMediaToPosition(int position);

    void goToEventGallery(String eventId);

    void setFutureEvent(EventInfo futureEvent);

    void setParticipants(List<ProfileData> profiles, boolean isFromLinkedEvents);

    void goToParticipantsScreen(String eventId, ParticipantFragment.GoingFrom goingFrom);
}