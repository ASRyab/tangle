package com.tangle.screen.events.choose;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.analytics.SimpleActions;
import com.tangle.analytics.Via;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.managers.EventManager;
import com.tangle.managers.PhotoManager;
import com.tangle.model.event.EventInfo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.RxUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ChooseEventPresenter extends LifecyclePresenter<ChooseEventView> {
    private ShowedEventsType showedEventsType;
    private ProfileData profile;
    private String userId;
    private ChooseEventFragment.GoingFrom goingFrom;

    public void setShowedEventsType(ShowedEventsType showedEventsType) {
        this.showedEventsType = showedEventsType;
    }

    public void setGoingFrom(ChooseEventFragment.GoingFrom goingFrom) {
        this.goingFrom = goingFrom;
    }

    public void setInviteUserId(String id) {
        this.userId = id;
    }

    @Override
    public void attachToView(ChooseEventView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        getView().initAdapter(showedEventsType);
        loadProfile();
    }

    private void loadProfile() {
        Observable<ProfileData> profileObservable = ShowedEventsType.OWN_EVENTS.equals(showedEventsType) ?
                ApplicationLoader.getApplicationInstance().getUserManager().currentUser()
                : ApplicationLoader.getApplicationInstance().getUserManager().getUserGraphById(userId);
        Disposable disposable = profileObservable.subscribe(this::setProfile, error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    private void setProfile(ProfileData profile) {
        this.profile = profile;
        getEvents();
    }

    @Override
    public void onStart() {
        super.onStart();
        monitor(ApplicationLoader.getApplicationInstance().getEventManager().getUpdates()
                .filter(info -> profile != null)
                .subscribe(event -> getEvents(),
                        RxUtils.getEmptyErrorConsumer(TAG, "onStart : getInviteRepository")), State.STOP);
    }

    private void getEvents() {
        switch (showedEventsType) {
            case USER_WILL_GO:
                setEvents(ApplicationLoader.getApplicationInstance().getEventManager().getEventsByIdsActual(profile.futureEventsList));
                break;
            case OWN_EVENTS:
                setEvents(ApplicationLoader.getApplicationInstance().getEventManager().getAll()
                        .flatMap(infos -> Observable.fromIterable(infos)
                                .compose(EventManager.checkStatusEvent())
                                .filter(info -> info.hasTicket() || info.isReserved())
                                .toList().toObservable()));
                break;
            case USER_UPCOMING_EVENT:
                setEvents(ApplicationLoader.getApplicationInstance().getEventManager().getEventsByIdsActual(profile.eventsForInvite)
                );
                break;
            case PAST_EVENTS:
                setEvents(ApplicationLoader.getApplicationInstance().getEventManager().getEventsByIds(profile.pastEventsList));
                break;
        }
    }

    public void onInviteClick(EventInfo event) {
        PhotoManager photoManager = ApplicationLoader.getApplicationInstance().getPhotoManager();
        monitor(photoManager
                        .checkPhoto(getView())
                        .subscribe(data -> {
                            switch (data) {
                                case HAS_APPROVE_PHOTO:
                                    setVia();
                                    getView().getPrePaymentPresenter().onInviteClicked(event, profile);
                                    break;
                                case HAS_PHOTO:
                                    getView().showNoApprovePhoto();
                                    break;
                            }
                        }, RxUtils.getEmptyErrorConsumer()),
                State.DESTROY);
    }


    private void setVia() {
        switch (goingFrom) {
            case LIKEBOOK:
                ApplicationLoader.getApplicationInstance().getAnalyticManager().setVia(Via.LIKEBOOK_INVITE);
                break;
            case MATCH:
                ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.MATCH_CLICK_INVITE);
                ApplicationLoader.getApplicationInstance().getAnalyticManager().setVia(Via.MATCH_INVITE);
                break;
            default:
                ApplicationLoader.getApplicationInstance().getAnalyticManager().setVia(Via.NONE);
                break;
        }
    }

    private void setEvents(Observable<List<EventInfo>> eventsObservable) {
        Disposable disposable = eventsObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(events -> {
                    if (events == null || events.isEmpty()) {
                        getView().showEmptyLayout();
                    } else {
                        getView().setEvents(events);
                    }
                }, error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    public void onEventClicked(EventInfo event) {
        getView().goToEventInfo(event, goingFrom);

    }

    public void onBookClicked(EventInfo event) {
        getView().getPrePaymentPresenter().onBookClicked(event);
    }

    public void showTicket(EventInfo event) {
        getView().goToTickets(event);
    }

    public void onCheckEventsClicked() {
        getView().goToEvents();
    }
}