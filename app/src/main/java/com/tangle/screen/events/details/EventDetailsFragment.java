package com.tangle.screen.events.details;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.analytics.SimpleActions;
import com.tangle.base.FreePaymentPresenter;
import com.tangle.base.PrePaymentPresenter;
import com.tangle.base.implementation.adapters.InterestsAdapter;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.BaseActivity;
import com.tangle.base.ui.recyclerview.decorations.SpaceItemDecoration;
import com.tangle.base.ui.recyclerview.layout_managers.flex.FlexDirection;
import com.tangle.base.ui.recyclerview.layout_managers.flex.FlexWrap;
import com.tangle.base.ui.recyclerview.layout_managers.flex.FlexboxLayoutManager;
import com.tangle.base.ui.recyclerview.layout_managers.flex.JustifyContent;
import com.tangle.base.ui.views.ImageShapeButton;
import com.tangle.base.ui.views.ShapeButton;
import com.tangle.managers.DialogManager;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.model.event.TicketStatus;
import com.tangle.model.event.TicketStatusConstant;
import com.tangle.model.payment.PaymentData;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.map.BaseEventDetailFragment;
import com.tangle.screen.events.participant.ParticipantFragment;
import com.tangle.screen.events.popup.CongratsFragment;
import com.tangle.screen.events.popup.CongratsViewType;
import com.tangle.screen.events.ticket.EventTicketFragment;
import com.tangle.screen.main.ActivityHolder;
import com.tangle.screen.main.navigation_menu.MenuState;
import com.tangle.screen.media.pager.MediaContainerFragment;
import com.tangle.screen.other_profile.ProfileEventPresenter;
import com.tangle.utils.EventUtils;
import com.tangle.utils.PlatformUtils;

import java.util.Collections;
import java.util.List;

public class EventDetailsFragment extends BaseEventDetailFragment implements EventDetailsView,
        MediaContainerFragment.MediaContainerCallback {

    private EventDetailsPresenter presenter = new EventDetailsPresenter();
    private FreePaymentPresenter freePaymentPresenter = new FreePaymentPresenter(DialogManager.getInstance(this));

    private BaseEventDetailsView baseDetailsView;
    private TextView ticketCountTV, priceTV;
    private RecyclerView canInviteRV;
    private ProfilesAdapter canInviteAdapter;
    private TextView canInviteAmountTV;
    private RecyclerView participantsRV;
    private ProfilesAdapter participantsAdapter;
    private TextView participantsAmountTV;
    private ViewGroup containerCanInvite, containerParticipants;
    private View dividerButtons;
    private ShapeButton ticketBtn, inviteBtn;
    private ImageShapeButton showTicketBtn;
    private TextView disableField;
    private LinearLayout actionButtons;
    private TextView titleLargeTV;
    private TextView tvReservedLabel;
    private View headerContainer;
    private RecyclerView badgesRV;
    private InterestsAdapter badgeAdapter;
    private TextView tvTicketStatus;


    public static EventDetailsFragment newInstance(EventInfo event) {
        return newInstance(event, EventDetailsPresenter.GoingFrom.OTHER);
    }

    public static EventDetailsFragment newInstance(EventInfo event, EventDetailsPresenter.GoingFrom goingFrom) {
        ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.EVENT_CLICK_DETAILS);
        Bundle args = new Bundle();
        args.putString(BUNDLE_EVENT_KEY, event.eventId);
        args.putSerializable(GOING_FROM_KEY, goingFrom);
        EventDetailsFragment fragment = new EventDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String eventId = getArguments().getString(BUNDLE_EVENT_KEY);
        presenter.setEventId(eventId);
        presenter.setGoingFrom((EventDetailsPresenter.GoingFrom) getArguments().getSerializable(GOING_FROM_KEY));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_event_details, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        baseDetailsView = new BaseEventDetailsView(view, this::openMediaItem);
        titleLargeTV = view.findViewById(R.id.tvTitle);
        badgesRV = view.findViewById(R.id.list_badges);
        headerContainer = view.findViewById(R.id.header_container);
        participantsRV = view.findViewById(R.id.list_profiles_participants);
        participantsAmountTV = view.findViewById(R.id.txt_participants_amount);
        canInviteRV = view.findViewById(R.id.list_can_invite);
        canInviteAmountTV = view.findViewById(R.id.txt_can_invite_amount);
        ticketCountTV = view.findViewById(R.id.txt_ticket_count);
        priceTV = view.findViewById(R.id.tvPrice);
        containerCanInvite = view.findViewById(R.id.container_profiles_can_invite);
        containerParticipants = view.findViewById(R.id.container_profiles_participants);
        dividerButtons = view.findViewById(R.id.view_btn_divider);
        ticketBtn = view.findViewById(R.id.btn_ticket);
        inviteBtn = view.findViewById(R.id.btn_invite);
        showTicketBtn = view.findViewById(R.id.btn_show_ticket);
        disableField = view.findViewById(R.id.soldout);
        actionButtons = view.findViewById(R.id.action_buttons);
        tvTicketStatus = view.findViewById(R.id.txt_status);
        tvReservedLabel = view.findViewById(R.id.tvReservedLabel);

        initProfileLists();
        initProfileListsActions();
    }

    private void initProfileListsActions() {
        canInviteAmountTV.setOnClickListener(view -> presenter.onInviteClicked());
    }

    private void initActionButtons() {
        ticketBtn.setOnClickListener(v -> presenter.onTicketClicked());
        inviteBtn.setOnClickListener(v -> presenter.onInviteClicked());
        showTicketBtn.setOnClickListener(v -> presenter.onQRCodeClicked());
    }

    @Override
    public void goToLikeBook() {
        moveToMenu(MenuState.TAB_LIKE_OR_NOT_INDEX);
    }

    @Override
    public void goToPP(PaymentData response) {
        ActivityHolder activity = (ActivityHolder) getActivity();
        activity.openPP(response);
    }

    @Override
    public void goToSuccess(String eventId, String userId, CongratsViewType type) {
        CongratsFragment fragment = CongratsFragment.newInstance(eventId, userId, type);
        addFragment(fragment, true);
    }

    @Override
    public void setInviteButtonVisible(boolean visible) {
        inviteBtn.setVisibility(visible ? View.VISIBLE : View.GONE);
        checkDividerVisibility();
    }

    @Override
    public void showQRCodeFragment(String eventId) {
        addFragment(EventTicketFragment.newInstance(eventId), true);
    }

    @Override
    public void setBookButtonVisible(boolean visible, boolean isBooked) {
        ticketBtn.setVisibility(visible ? View.VISIBLE : View.GONE);
        ticketBtn.setText(!isBooked ? getString(R.string.ticket) : getString(R.string.book).toLowerCase());
        checkDividerVisibility();
    }

    @Override
    public void initFreeEventActionButton(TicketStatusConstant freeTicketStatus) {
        switch (freeTicketStatus) {
            case NON_REQUESTED:
                ticketBtn.setVisibility(View.VISIBLE);
                ticketBtn.setText(getString(R.string.request_ticket));
                ticketBtn.setOnClickListener((v -> presenter.onFreeClicked()));
                checkDividerVisibility();
                break;
            case WAIT_APPROVE:
                ticketBtn.setVisibility(View.GONE);
                setSoldOutVisible(true);
                disableField.setText(R.string.requested);
                tvTicketStatus.setBackgroundResource(R.drawable.reserved_label_background);
                tvTicketStatus.setText(R.string.requested);
                tvTicketStatus.setVisibility(View.VISIBLE);
                break;
            case APPROVED:
                tvTicketStatus.setText(R.string.confirmed);
                tvTicketStatus.setBackgroundResource(R.drawable.own_profile_date_background);
                tvTicketStatus.setVisibility(View.VISIBLE);
                setBookButtonVisible(true, false);
                ticketBtn.setText(R.string.invite_friends);
                ticketBtn.setOnClickListener((v -> DialogManager.getInstance(this).showShareFreeDialog()));
                showTicketBtn.setVisibility(View.VISIBLE);
                break;
        }
    }


    @Override
    public void setSoldOutVisible(boolean isVisible) {
        disableField.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        actionButtons.setVisibility(!isVisible ? View.VISIBLE : View.GONE);

    }

    private void checkDividerVisibility() {
        boolean dividerVisible = inviteBtn.getVisibility() == View.VISIBLE && ticketBtn.getVisibility() == View.VISIBLE;
        dividerButtons.setVisibility(dividerVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        baseDetailsView.destroyView();
    }

    private void openMediaItem(EventInfo event, Media media) {
        ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.EVENT_CLICK_PHOTOBIG);
        MediaContainerFragment mediaContainerFragment = MediaContainerFragment.newInstance(event.getActualMediaList(), media, MediaContainerFragment.ShowType.OTHER);
        mediaContainerFragment.setTargetFragment(this, MediaContainerFragment.SET_MEDIA_INDEX_CODE);
        addFragment(mediaContainerFragment, true);
    }

    private void initProfileLists() {
        Context context = getView().getContext();
        RecyclerView.ItemDecoration spaceDecoration = new SpaceItemDecoration(
                (int) PlatformUtils.Dimensions.dipToPixels(context, 0),
                (int) PlatformUtils.Dimensions.dipToPixels(context, 18), LinearLayoutManager.HORIZONTAL);
        initSingleList(canInviteRV, spaceDecoration, ProfileEventPresenter.GoingFrom.YOU_CAN_INVITE);
        canInviteAdapter = (ProfilesAdapter) canInviteRV.getAdapter();
        initSingleList(participantsRV, spaceDecoration, ProfileEventPresenter.GoingFrom.PARTICIPANTS);
        participantsAdapter = (ProfilesAdapter) participantsRV.getAdapter();
    }

    private void initSingleList(RecyclerView list, RecyclerView.ItemDecoration decoration, ProfileEventPresenter.GoingFrom goingFrom) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(list.getContext(), LinearLayoutManager.HORIZONTAL, false);
        list.setLayoutManager(layoutManager);
        list.setHasFixedSize(true);
        list.addItemDecoration(decoration);
        list.setAdapter(new ProfilesAdapter(id -> {
            if (!presenter.isReadOnly()) {
                ApplicationLoader.getApplicationInstance().getNavigationManager().showUserProfile(id, goingFrom, presenter.getEventId());
            } else if (presenter.isLinkedEvent()) {
                presenter.onParticipantsAmountClicked(ParticipantFragment.GoingFrom.LINKED_EVENT);
            }
        }));
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setEventInfo(EventInfo event) {
        baseDetailsView.setEventInfo(event);
        if (event.isEventFree()) {
            baseDetailsView.setTitleMedium(event.getEventLocation());
            titleLargeTV.setText(event.title);
            headerContainer.setVisibility(View.VISIBLE);
            priceTV.setText(getString(R.string.free_entrance));
            badgesRV.setVisibility(View.VISIBLE);
            FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(badgesRV.getContext(), FlexDirection.ROW, FlexWrap.WRAP);
            layoutManager.setJustifyContent(JustifyContent.CENTER);
            badgesRV.setLayoutManager(layoutManager);
            badgeAdapter = new InterestsAdapter();
            badgeAdapter.setSelectAvailable(false);
            badgesRV.setAdapter(badgeAdapter);
            badgeAdapter.switchInterests(EventUtils.initEventBadges(getContext()));
        } else {
            priceTV.setText(getString(R.string.event_price, event.price));
            if (event.isBookAvailable()) {
                ticketCountTV.setVisibility(View.VISIBLE);
                if (event.hasTicket()) {
                    ticketCountTV.setText(getString(R.string.event_tickets_left, event.getAllAvailableTickets()));
                } else {
                    ticketCountTV.setText(getString(R.string.event_capacity, event.getAllTickets()));
                }
            }
            tvReservedLabel.setVisibility(event.isReserved() && !event.hasStarted() ? View.VISIBLE : View.GONE);
            EventUtils.updateEventStatus(tvTicketStatus, event);
        }
        initActionButtons();
    }

    @Override
    public void setCanInvite(List<ProfileData> profiles) {
        if (profiles != null && !profiles.isEmpty()) {
            containerCanInvite.setVisibility(View.VISIBLE);
            canInviteAdapter.switchProfiles(profiles);
            canInviteAmountTV.setText(Integer.toString(profiles.size()));
        } else {
            containerCanInvite.setVisibility(View.GONE);
        }
    }

    @Override
    public void setParticipants(List<ProfileData> profiles, boolean isReadOnly, boolean isFromLinkedEvents) {
        if (profiles != null && !profiles.isEmpty()) {
            containerParticipants.setVisibility(View.VISIBLE);
            participantsAdapter.switchProfiles(profiles);
            participantsAmountTV.setText(Integer.toString(profiles.size()));
            if (!isReadOnly || isFromLinkedEvents) {
                participantsAmountTV.setOnClickListener(view -> presenter.onParticipantsAmountClicked(isFromLinkedEvents ? ParticipantFragment.GoingFrom.LINKED_EVENT : ParticipantFragment.GoingFrom.OTHER));
                participantsAmountTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_right, 0);
            }
        } else {
            containerParticipants.setVisibility(View.GONE);
        }
    }


    @Override
    public void goToParticipantsScreen(String eventId, ParticipantFragment.GoingFrom goingFrom) {
        addFragment(ParticipantFragment.newInstance(eventId, goingFrom), true);
    }

    @Override
    public void scrollMediaToPosition(int position) {
        baseDetailsView.multipleView.setCurrentItem(position);
    }

    @Override
    public void onFullScreenPageSelected(int position) {
        presenter.onMediaPageSelected(position);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        presenter.onMapInitialized();
    }

    @Override
    protected void onMapClicked() {
        presenter.onMapClicked();
    }

    @Override
    public PrePaymentPresenter getPrePaymentPresenter() {
        return freePaymentPresenter;
    }

    @Override
    public List<LifecyclePresenter> getPresenters() {
        return Collections.singletonList(freePaymentPresenter);
    }

    @Override
    public void freeTicketSuccess(TicketStatus status, EventInfo eventInfo) {
        presenter.refreshEvent();
        if (status.status == TicketStatusConstant.APPROVED) {
            DialogManager.showApproveFreeTicket((BaseActivity) getActivity(), eventInfo);
        } else if (status.status == TicketStatusConstant.WAIT_APPROVE) {
            DialogManager.showWaitApproveFreeTicket((BaseActivity) getActivity(), eventInfo);
        } else if (status.status == TicketStatusConstant.DECLINED) {
            DialogManager.showDeclineFreeTicket((BaseActivity) getActivity(), eventInfo);
        }
    }
}