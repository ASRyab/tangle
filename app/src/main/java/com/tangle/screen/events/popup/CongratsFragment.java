package com.tangle.screen.events.popup;

import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.v4.widget.Space;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.managers.DialogManager;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.model.pojos.Gender;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.main.navigation_menu.MenuState;
import com.tangle.utils.ConvertingUtils;
import com.tangle.utils.Patterns;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

public class CongratsFragment extends SecondaryScreenFragment implements CongratsView {
    private final static String EVENT_BUNDLE_KEY = "event_key";
    private final static String PROFILE_BUNDLE_KEY = "profile_key";
    private final static String TYPE_TICKET_ACTION_BUNDLE_KEY = "ticket_action_type_key";

    private CongratsPresenter presenter = new CongratsPresenter();

    private ImageView imgBackground;
    private TextView txtTitle;
    private TextView txtEventName;
    private TextView txtDate;
    private TextView txtPlace;
    private Space spaceSend, spaceBought;
    private RelativeLayout invitedHeaderImg;
    private ImageView imgInvited;
    private ImageView imgEvent;
    private ImageView imgCheck;

    public static CongratsFragment newInstance(String eventId,
                                               String profileId,
                                               CongratsViewType congratsViewType) {
        CongratsFragment invitePopupFragment = new CongratsFragment();
        Bundle args = new Bundle();
        args.putString(EVENT_BUNDLE_KEY, eventId);
        args.putString(PROFILE_BUNDLE_KEY, profileId);
        args.putSerializable(TYPE_TICKET_ACTION_BUNDLE_KEY, congratsViewType);
        invitePopupFragment.setArguments(args);
        return invitePopupFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setData(getArguments());
    }

    private void setData(Bundle bundle) {
        if (bundle != null) {
            presenter.setEvent(bundle.getString(EVENT_BUNDLE_KEY));
            presenter.setProfile(bundle.getString(PROFILE_BUNDLE_KEY));
            presenter.setCongratsViewType((CongratsViewType) bundle.getSerializable(TYPE_TICKET_ACTION_BUNDLE_KEY));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_ticket_action_popup, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        imgBackground = view.findViewById(R.id.img_background);
        txtTitle = view.findViewById(R.id.tvTitle);
        txtEventName = view.findViewById(R.id.txt_event_name);
        txtDate = view.findViewById(R.id.txt_date);
        txtPlace = view.findViewById(R.id.txt_place);
        spaceSend = view.findViewById(R.id.send_space);
        spaceBought = view.findViewById(R.id.bought_space);
        invitedHeaderImg = view.findViewById(R.id.invited_header_image);
        imgInvited = view.findViewById(R.id.img_invited);
        imgEvent = view.findViewById(R.id.ivEvent);
        imgCheck = view.findViewById(R.id.img_check);
        view.findViewById(R.id.btn_ok).setOnClickListener(v -> presenter.onActionOkClicked());
        view.findViewById(R.id.txt_add_to_calendar).setOnClickListener(v -> presenter.onAddToCalendarClicked());
    }

    @Override
    public void setSendTicketUI(ProfileData user, EventInfo event) {
        setBasicDataToView(event);
        invitedHeaderImg.setVisibility(View.VISIBLE);
        spaceSend.setVisibility(View.VISIBLE);
        imgCheck.setVisibility(View.GONE);
        spaceBought.setVisibility(View.GONE);
        String userName = user.login;
        Gender gender = user.gender;
        switch (gender) {
            case FEMALE:
                txtTitle.setText(getString(R.string.ticket_action_popup_send_title_female, userName));
                break;
            default:
                txtTitle.setText(getString(R.string.ticket_action_popup_send_title_male, userName));
                break;
        }
        GlideApp.with(imgInvited)
                .load(user.primaryPhoto.avatar)
                .placeholder(RequestOptionsFactory.getNonAvatar(user))
                .apply(RequestOptions.circleCropTransform())
                .into(imgInvited);
        GlideApp.with(imgEvent)
                .load(event.getPreviewImageUrl())
                .apply(RequestOptionsFactory.getSmallRoundedCornersOptions(ApplicationLoader.getContext()))
                .into(imgEvent);
    }

    @Override
    public void setBoughtTicketUI(EventInfo event) {
        setBasicDataToView(event);
        invitedHeaderImg.setVisibility(View.GONE);
        spaceSend.setVisibility(View.GONE);
        imgCheck.setVisibility(View.VISIBLE);
        spaceBought.setVisibility(View.VISIBLE);
        txtTitle.setText(getString(R.string.ticket_action_popup_bought_title));
    }

    @Override
    public void showAddToCalendarDialog(EventInfo event) {
        DialogManager.getInstance(this).showAddToCalendarDialog((v, dialog) -> {
            openCalendarScreen(event);
        });
    }

    @Override
    public void goBack() {
        getActivity().onBackPressed();
    }

    private void setBasicDataToView(EventInfo event) {
        if (event.getActualMediaList() != null && !event.getActualMediaList().isEmpty()) {
            Media media = event.getActualMediaList().get(0);
            GlideApp.with(this)
                    .load(media.type == Media.Type.PHOTO ? media.photoBig2xUrl : media.videoImagePreviewUrl)
                    .apply(RequestOptionsFactory.getDefaultBlurOptions(getActivity()))
                    .into(imgBackground);
        }
        txtEventName.setText(event.title);
        txtDate.setText(String.format("%s, %s",
                Patterns.DATE_FORMAT_LONG.format(event.eventStartDate),
                Patterns.TIME_FORMAT.format(event.eventStartDate)));
        txtPlace.setText(event.getEventAddress(getContext()));
    }

    private void openCalendarScreen(EventInfo event) {
        Intent intent = new Intent(Intent.ACTION_INSERT);
        intent.setType("vnd.android.cursor.item/event");
        intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, event.eventStartDate.getTime());
        intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, event.eventEndDate.getTime());
        intent.putExtra(CalendarContract.Events.TITLE, event.title);
        if (!TextUtils.isEmpty(event.description)) {
            Spanned spanned = Html.fromHtml(event.description);
            intent.putExtra(CalendarContract.Events.DESCRIPTION, ConvertingUtils.trimTrailingWhitespace(spanned).toString());
        }
        intent.putExtra(CalendarContract.Events.EVENT_LOCATION, event.getEventAddress(getContext()));
        getActivity().startActivity(intent);
    }

    @Override
    public void successAndClose() {
        moveToMenu(MenuState.TAB_EVENT_INDEX);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}