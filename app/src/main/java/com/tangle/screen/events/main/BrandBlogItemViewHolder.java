package com.tangle.screen.events.main;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.tangle.R;

public class BrandBlogItemViewHolder extends RecyclerView.ViewHolder {

    private TextView tvNewPost;

    BrandBlogItemViewHolder(View itemView, View.OnClickListener clickListener) {
        super(itemView);
        itemView.setOnClickListener(clickListener);
        tvNewPost = itemView.findViewById(R.id.txt_new_post);
    }

    public void bind(int newBlogItemsCount) {
        if (newBlogItemsCount > 0) {
            tvNewPost.setVisibility(View.VISIBLE);
        } else {tvNewPost.setVisibility(View.INVISIBLE);}
    }
}