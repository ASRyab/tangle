package com.tangle.screen.events.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tangle.R;
import com.tangle.model.event.EventInfo;
import com.tangle.screen.events.EventClickListener;

import java.util.ArrayList;
import java.util.List;

public class MainEventsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = MainEventsAdapter.class.getSimpleName();
    private static final int FREE_EVENT_TYPE = 1;
    private static final int DEFAULT_EVENT_TYPE = 0;
    private static final int BRAND_BLOG_ITEM_TYPE = 2;

    private List<EventInfo> mainEvents = new ArrayList<>();
    private int newBlogItemsCount = 0;
    private EventClickListener eventClickListener;
    private EventClickListener participantClickListener;
    private View.OnClickListener brandBlogClickListener;
    private static final int NOT_EVENT_ITEM_COUNT = 1;


    public MainEventsAdapter(EventClickListener eventClickListener, EventClickListener participantClickListener, View.OnClickListener brandBlogClickListener) {
        this.eventClickListener = eventClickListener;
        this.participantClickListener = participantClickListener;
        this.brandBlogClickListener = brandBlogClickListener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case FREE_EVENT_TYPE:
                return new FreeEventViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_free_main_event, parent, false)
                        , eventClickListener, participantClickListener);
            case BRAND_BLOG_ITEM_TYPE:
                return new BrandBlogItemViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_brand_blog_logo, parent, false)
                        , brandBlogClickListener);
            default:
                return new EventViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_main_event, parent, false)
                        , eventClickListener, participantClickListener);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case BRAND_BLOG_ITEM_TYPE:
                ((BrandBlogItemViewHolder) holder).bind(newBlogItemsCount);
                break;
            default:
                ((EventViewHolder) holder).bind(mainEvents.get(getEventItemPosition(position)));
        }

    }


    @Override
    public int getItemViewType(int position) {
        if (position == 0) return BRAND_BLOG_ITEM_TYPE;
        if (mainEvents.get(getEventItemPosition(position)).isEventFree()) {
            return FREE_EVENT_TYPE;
        } else {
            return DEFAULT_EVENT_TYPE;
        }
    }

    @Override
    public int getItemCount() {
        return mainEvents == null ? NOT_EVENT_ITEM_COUNT : mainEvents.size() + NOT_EVENT_ITEM_COUNT;
    }

    public void switchMainEvents(List<EventInfo> mainEvents) {
        this.mainEvents = mainEvents;
        notifyDataSetChanged();
    }

    public void updateUnread(int newBlogItemsCount) {
        this.newBlogItemsCount = newBlogItemsCount;
        notifyItemChanged(0);
    }

    private int getEventItemPosition(int position) {
        return position - NOT_EVENT_ITEM_COUNT;
    }
}
