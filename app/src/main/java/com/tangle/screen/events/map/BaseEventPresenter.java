package com.tangle.screen.events.map;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.analytics.SimpleActions;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.managers.EventManager;
import com.tangle.managers.PhotoManager;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.StatusEvent;
import com.tangle.screen.events.details.EventDetailsPresenter;
import com.tangle.utils.RxUtils;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class BaseEventPresenter<View extends BaseEventDetailView> extends LifecyclePresenter<View> {

    private boolean mapInitialized;
    protected EventInfo event;
    protected String eventId;
    protected GoingFrom goingFrom = null;

    @Override
    public void attachToView(View view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (event != null) {
            onEvent(event);
        }
    }

    public void setEventId(String eventId) {
        this.eventId = eventId;
    }

    protected void onEvent(@NonNull EventInfo event) {
        getView().setEventInfo(event);
        updateMapState(event);
        if (goingFrom != null && goingFrom != GoingFrom.LINKED_EVENTS) {
            fetchLinkedEvents(event);
        }
    }

    public void setEvent(EventInfo event) {
        this.event = event;
        if (!isViewDestroyed()) {
            onEvent(event);
        }
    }

    public void onMapInitialized() {
        mapInitialized = true;
        if (event != null) {
            updateMapState(event);
        }
    }

    private void fetchLinkedEvents(EventInfo eventInfo) {
        if (eventInfo.status != StatusEvent.DRAFT) {
            monitor(ApplicationLoader.getApplicationInstance().getEventManager()
                    .getEventsByIds(eventInfo.linkedEvents)
                    .flatMap(res -> Observable.just(res)
                            .compose(eventInfo.hasPassed() ? EventManager.composeSort() : EventManager.composeSortDateDescendant()))
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(res -> {
                        boolean isFutureEvents = eventInfo.status != StatusEvent.EXPIRED;
                        getView().setLinkedEvents(res, isFutureEvents);

                    }, RxUtils.getEmptyErrorConsumer()), State.DESTROY_VIEW);
        }
    }

    public void onMapClicked() {
        ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.EVENT_CLICK_MAP);
        getView().goToMap(event);
    }

    private void updateMapState(EventInfo event) {
        if (event.isSecretLocation) {
            mapInitialized = false;
            getView().updateMapState(true);
        } else if (mapInitialized) {
            getView().setEventMap(event);
        } else {
            getView().updateMapState(false);
            getView().setEventMap(event);
        }
    }

    public String getEventId() {
        return eventId;
    }

    public void setGoingFrom(EventDetailsPresenter.GoingFrom goingFrom) {
        this.goingFrom = goingFrom;
    }

    public enum GoingFrom {
        GOING_TO,
        UPCOMING_EVENTS,
        UPCOMING_EVENTS_MATCHES,
        OTHER,
        LINKED_EVENTS
    }

    public void onInviteClicked() {
        PhotoManager photoManager = ApplicationLoader.getApplicationInstance().getPhotoManager();
        monitor(photoManager
                        .checkPhoto(getView())
                        .subscribe(data -> {
                            switch (data) {
                                case HAS_APPROVE_PHOTO:
                                    checkUserLocation();
                                    break;
                                case HAS_PHOTO:
                                    getView().showNoApprovePhoto();
                                    break;
                            }
                        }, RxUtils.getEmptyErrorConsumer()),
                State.DESTROY_VIEW);

    }

    public void invite() {
        getView().goToInviteScreen(event.eventId);
    }

    private void checkUserLocation() {
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().checkUserInWhiteList()
                        .subscribe(this::doCheck, RxUtils.getEmptyErrorConsumer(TAG)),
                State.DESTROY_VIEW);
    }

    private void doCheck(Boolean isInWhiteList) {
        if (isInWhiteList) {
            getView().showRemindLocationDialog(this::invite);
        } else {
            invite();
        }
    }

}
