package com.tangle.screen.events.choose;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.PrePaymentPresenter;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.views.NotificationInformationLayout;
import com.tangle.managers.DialogManager;
import com.tangle.model.event.EventInfo;
import com.tangle.model.payment.PaymentData;
import com.tangle.screen.events.details.EventDetailsFragment;
import com.tangle.screen.events.details.EventDetailsPresenter;
import com.tangle.screen.events.details.PastEventDetailsFragment;
import com.tangle.screen.events.popup.CongratsFragment;
import com.tangle.screen.events.popup.CongratsViewType;
import com.tangle.screen.events.ticket.EventTicketFragment;
import com.tangle.screen.main.ActivityHolder;
import com.tangle.screen.main.navigation_menu.MenuState;

import java.util.Collections;
import java.util.List;

public class ChooseEventFragment extends SecondaryScreenFragment implements ChooseEventView {
    private final static String USER_ID_KEY = "user_id";
    private final static String SHOWED_EVENT_TYPE_KEY = "showed_event_type";
    private final static String GO_FROM = "go_from";

    private ChooseEventPresenter presenter = new ChooseEventPresenter();

    private TextView toolbarTitle;
    private RecyclerView eventsRVGrid;
    private ChooseEventsAdapter eventsAdapter;
    private NotificationInformationLayout emptyView;

    public static ChooseEventFragment newInstance(String userId, ShowedEventsType showedEventsType, GoingFrom goingFrom) {
        Bundle args = new Bundle();
        args.putString(USER_ID_KEY, userId);
        args.putSerializable(SHOWED_EVENT_TYPE_KEY, showedEventsType);
        args.putInt(GO_FROM, goingFrom.ordinal());
        ChooseEventFragment fragment = new ChooseEventFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getBundleData(getArguments());
    }

    private void getBundleData(Bundle bundle) {
        if (bundle != null) {
            presenter.setInviteUserId(bundle.getString(USER_ID_KEY));
            presenter.setShowedEventsType((ShowedEventsType) bundle.getSerializable(SHOWED_EVENT_TYPE_KEY));
            int ordinal = bundle.getInt(GO_FROM);
            presenter.setGoingFrom(GoingFrom.values()[ordinal]);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_choose_events, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        toolbarTitle = view.findViewById(R.id.txt_toolbar_title);
        emptyView = view.findViewById(R.id.notify_rview);
        emptyView.setClickListener(v -> presenter.onCheckEventsClicked());
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarTitle.setText(getText(R.string.choose_event));
        eventsRVGrid = view.findViewById(R.id.events_rview);
    }

    @Override
    public void initAdapter(ShowedEventsType showedEventsType) {
        eventsRVGrid.setLayoutManager(new GridLayoutManager(eventsRVGrid.getContext(), 2));
        eventsRVGrid.setHasFixedSize(true);
        eventsRVGrid.setAdapter(new ChooseEventsAdapter(showedEventsType));
        eventsAdapter = (ChooseEventsAdapter) eventsRVGrid.getAdapter();
        eventsAdapter.setClickListener(new ChooseEventClickListener() {
            @Override
            public void onEventClicked(EventInfo event) {
                presenter.onEventClicked(event);
            }

            @Override
            public void onButtonClicked(EventInfo event) {
                switch (showedEventsType) {
                    case USER_WILL_GO:
                        presenter.onBookClicked(event);
                        break;
                    case OWN_EVENTS:
                        presenter.showTicket(event);
                        break;
                    case USER_UPCOMING_EVENT:
                        presenter.onInviteClick(event);
                        break;
                }
            }
        });
    }

    @Override
    public void goToEventInfo(EventInfo event, ChooseEventFragment.GoingFrom goingFrom) {
        addFragment(getEventFragment(event, goingFrom), true);
    }

    @Override
    public void goToTickets(EventInfo event) {
        addFragment(EventTicketFragment.newInstance(event.eventId), true);
    }

    @Override
    public void goToEvents() {
        moveToMenu(MenuState.TAB_EVENT_INDEX);
    }

    @Override
    public void goToPP(PaymentData data) {
        ActivityHolder activity = (ActivityHolder) getActivity();
        activity.openPP(data);
    }

    @Override
    public void goToSuccess(String eventId, String userId, CongratsViewType type) {
        CongratsFragment fragment = CongratsFragment.newInstance(eventId, userId, type);
        addFragment(fragment, true);
    }

    private PrePaymentPresenter prePaymentPresenter = new PrePaymentPresenter(DialogManager.getInstance(this));

    @Override
    public List<LifecyclePresenter> getPresenters() {
        return Collections.singletonList(prePaymentPresenter);
    }

    @Override
    public PrePaymentPresenter getPrePaymentPresenter() {
        return prePaymentPresenter;
    }

    @Override
    public void setEvents(List<EventInfo> events) {
        eventsAdapter.switchEvents(events);
    }

    @Override
    public void showEmptyLayout() {
        emptyView.setVisibility(View.VISIBLE);
    }

    private Fragment getEventFragment(EventInfo event, ChooseEventFragment.GoingFrom goingFrom) {
        boolean isFromMatch = goingFrom.equals(GoingFrom.MATCH);
        return event.hasPassed() ? PastEventDetailsFragment.newInstance(event) :
                EventDetailsFragment.newInstance(event, isFromMatch ?
                        EventDetailsPresenter.GoingFrom.UPCOMING_EVENTS_MATCHES :
                        EventDetailsPresenter.GoingFrom.UPCOMING_EVENTS);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    public enum GoingFrom {
        LIKEBOOK,
        MATCH,
        OTHER
    }
}