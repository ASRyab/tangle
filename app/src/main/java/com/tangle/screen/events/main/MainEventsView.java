package com.tangle.screen.events.main;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.event.EventInfo;

import java.util.List;

public interface MainEventsView extends LoadingView {

    void setUpcomingEvents(List<EventInfo> events);

    void updateNewPostBadge(int count);

    void showPromo();

    void showEvent(EventInfo event);

    void openBrandBlogPromo();

    void openBrandBlog();
}
