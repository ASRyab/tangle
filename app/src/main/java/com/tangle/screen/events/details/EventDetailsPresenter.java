package com.tangle.screen.events.details;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.analytics.SimpleActions;
import com.tangle.analytics.Via;
import com.tangle.api.rx_tasks.photo.UploadPhotoModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.FreePaymentPresenter;
import com.tangle.base.mvp.State;
import com.tangle.managers.PhotoManager;
import com.tangle.managers.UserManager;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.StatusEvent;
import com.tangle.model.event.TicketStatusConstant;
import com.tangle.screen.events.map.BaseEventPresenter;
import com.tangle.screen.events.participant.ParticipantFragment;
import com.tangle.utils.RxUtils;

import java.io.File;
import java.util.Collections;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;

public class EventDetailsPresenter extends BaseEventPresenter<EventDetailsView> {
    private static final String TAG = EventDetailsPresenter.class.getSimpleName();

    public void onFreeClicked() {

        PhotoManager photoManager = ApplicationLoader.getApplicationInstance().getPhotoManager();
        monitor(photoManager
                        .checkPhoto(getView())
                        .subscribe(data -> {
                            switch (data) {
                                case HAS_APPROVE_PHOTO:
                                    processRequestTicket();
                                    break;
                                case HAS_PHOTO:
                                    getView().showNoApprovePhoto();
                                    break;
                            }
                        }, RxUtils.getEmptyErrorConsumer()),
                State.DESTROY_VIEW);
    }

    private void processRequestTicket() {
        ((FreePaymentPresenter) getView().getPrePaymentPresenter()).onFreeClicked(event);
    }

    public void uploadNewPhoto(File photoFile) {
        Disposable disposable = new UploadPhotoModel(photoFile, false).getTask()
                .doOnNext(data -> ApplicationLoader.getApplicationInstance().getUserManager().loadCurrentUser())
                .compose(RxUtils.withLoading(getView()))
                .subscribe(uploadPhoto -> processRequestTicket(),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    public void refreshEvent() {
        monitor(ApplicationLoader.getApplicationInstance().getEventManager()
                .getEventByIdAndUpdate(eventId)
                .subscribe(this::setEvent, RxUtils.getEmptyErrorConsumer(TAG, "attachToView")), State.DESTROY_VIEW)
        ;
    }


    private int mediaPage;

    @Override
    public void attachToView(EventDetailsView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        refreshEvent();
    }

    @Override
    protected void onEvent(@NonNull EventInfo event) {
        super.onEvent(event);
        safeScrollToPosition();
        setupActionButton();
        UserManager userManager = ApplicationLoader.getApplicationInstance().getUserManager();
        Consumer<Throwable> errorConsumer = RxUtils.getEmptyErrorConsumer(TAG, "onEvent");
        if (!isReadOnly() && event.isInviteAvailable() && !event.isEventFree()) {
            userManager.getUserByIds(event.usersForInvite)
                    .subscribe(getView()::setCanInvite, errorConsumer);
        }
        userManager.getUserByIds(event.participantsUsers).defaultIfEmpty(Collections.emptyList())
                .subscribe(participants -> getView().setParticipants(participants, isReadOnly(), isLinkedEvent()), errorConsumer);
    }

    private void setupActionButton() {
        getView().setSoldOutVisible(!event.isBookAvailable() && !event.hasTicket());
        if (event.isEventFree()) {
            getView().initFreeEventActionButton(event.getFreeTicketStatus());
            getView().setInviteButtonVisible(false);
        } else if (event.status == StatusEvent.ACTIVE && (event.isBookAvailable() || event.hasTicket())) {
            getView().setBookButtonVisible(GoingFrom.OTHER.equals(goingFrom) || GoingFrom.LINKED_EVENTS.equals(goingFrom) || event.isBookAvailable(), event.isBookAvailable());
            getView().setInviteButtonVisible(!GoingFrom.GOING_TO.equals(goingFrom) && event.isInviteAvailable());
        } else {
            getView().setBookButtonVisible(false, false);
            getView().setInviteButtonVisible(false);
        }
    }


    public boolean isReadOnly() {
        return !GoingFrom.OTHER.equals(goingFrom);
    }

    public boolean isLinkedEvent() {
        return GoingFrom.LINKED_EVENTS.equals(goingFrom);
    }

    public void onMediaPageSelected(int page) {
        this.mediaPage = page;
        safeScrollToPosition();
    }

    private void safeScrollToPosition() {
        if (!isViewDestroyed() && event != null) {
            getView().scrollMediaToPosition(mediaPage);
        }
    }

    public void onTicketClicked() {
        if ((event.hasTicket() && event.maxBookTickets == 0)
                || (event.isEventFree() && event.getFreeTicketStatus() == TicketStatusConstant.APPROVED)) {
            getView().showQRCodeFragment(event.eventId);
        } else {
            ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.EVENT_CLICK_BOOK);
            ApplicationLoader.getApplicationInstance().getAnalyticManager().setVia(Via.EVENT_BOOK);
            getView().getPrePaymentPresenter().onBookClicked(event);
        }
    }

    public void onQRCodeClicked() {
        getView().showQRCodeFragment(event.eventId);
    }

    public void invite() {
        getView().goToInviteScreen(event.eventId);
    }

    private void checkUserLocation() {
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().checkUserInWhiteList()
                        .subscribe(this::doCheck, RxUtils.getEmptyErrorConsumer(TAG)),
                State.DESTROY_VIEW);
    }

    private void doCheck(Boolean isInWhiteList) {
        if (isInWhiteList) {
            getView().showRemindLocationDialog(this::invite);
        } else {
            invite();
        }
    }

    public void onParticipantsAmountClicked(ParticipantFragment.GoingFrom goingFrom) {
        getView().goToParticipantsScreen(event.eventId, goingFrom);
    }

    @Override
    public void onStart() {
        super.onStart();
        monitor(ApplicationLoader.getApplicationInstance().getEventManager().getUpdates().subscribe(info -> {
            if (info.eventId.equals(eventId)) {
                event = info;
                setEvent(event);
            }
        }, RxUtils.getEmptyErrorConsumer(TAG, "onStart")), State.STOP);
    }
}