package com.tangle.screen.events;

public enum  EventType {
    FUTURE,
    PAST
}