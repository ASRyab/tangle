package com.tangle.screen.events;

import com.tangle.model.event.EventInfo;

public interface EventActionButtonClickListener {
    void onBookClicked(EventInfo event);

    void onInviteClicked(EventInfo event);
}