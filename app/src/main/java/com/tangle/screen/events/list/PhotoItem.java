package com.tangle.screen.events.list;

import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.tangle.model.event.Media;
import com.tangle.utils.glide.GlideApp;

public class PhotoItem extends MediaItem {

    private final RequestOptions requestOptions;
    private Media media;

    public PhotoItem(ViewGroup parent, RequestOptions requestOptions, int layoutId) {
        super(parent, layoutId);
        this.requestOptions = requestOptions;
    }

    @Override
    public void setMedia(Media media) {
        this.media = media;
        getView();
        loadPhoto();
    }

    private void loadPhoto() {
        videoView.setVisibility(View.INVISIBLE);
        previewIV.setVisibility(View.INVISIBLE);
        loaderView.setVisibility(View.VISIBLE);
        GlideApp.with(previewIV)
                .load(Uri.parse(media.photoThumbnail2xUrl))
                .apply(requestOptions)
                .apply(RequestOptions.priorityOf(Priority.HIGH))
                .into(new DrawableImageViewTarget(previewIV) {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        super.onResourceReady(resource, transition);
                        videoView.setVisibility(View.INVISIBLE);
                        loaderView.setVisibility(View.INVISIBLE);
                        previewIV.setVisibility(View.VISIBLE);
                    }
                });
    }

}
