package com.tangle.screen.events.ticket;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tangle.R;
import com.tangle.base.ui.views.QRCodeImageView;

import java.util.List;

public class QRCodePagerAdapter extends PagerAdapter {
    private Context context;
    private List<String> urls;
    private LayoutInflater inflater;

    public QRCodePagerAdapter(Context context, List<String> urls) {
        this.context = context;
        this.urls = urls;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        QRCodeImageView imgQRCode;
        inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.item_qrcode_page_adapter, container,
                false);
        imgQRCode = itemView.findViewById(R.id.qr_code);
        imgQRCode.generateQRCode(urls.get(position));
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object view) {
        container.removeView((View) view);
    }

    @Override
    public int getCount() {
        return urls.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}