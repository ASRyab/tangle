package com.tangle.screen.events.popup;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Pair;

import com.tangle.ApplicationLoader;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.event.EventInfo;
import com.tangle.utils.RxUtils;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class CongratsPresenter extends LifecyclePresenter<CongratsView> {
    private CongratsViewType congratsViewType;
    private String eventId;
    private String profileId;
    private EventInfo currentEvent;

    void setCongratsViewType(CongratsViewType congratsViewType) {
        this.congratsViewType = congratsViewType;
    }

    public void setEvent(String eventId) {
        this.eventId = eventId;
    }

    public void setProfile(String profileId) {
        this.profileId = profileId;
    }

    void onActionOkClicked() {
        getView().successAndClose();
    }

    void onAddToCalendarClicked() {
        getView().showAddToCalendarDialog(currentEvent);
    }

    @Override
    public void attachToView(CongratsView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        monitor(loadData(view), State.DESTROY_VIEW);
    }

    private Disposable loadData(CongratsView view) {
        Disposable disposable;
        ApplicationLoader instance = ApplicationLoader.getApplicationInstance();
        if (CongratsViewType.BOUGHT_EVENT_TICKET.equals(congratsViewType)) {
            disposable = instance.getEventManager().getEventByIdAndUpdate(eventId)
                    .subscribe(event -> {
                        this.currentEvent = event;
                        view.setBoughtTicketUI(event);
                    }, RxUtils.getEmptyErrorConsumer(TAG));
        } else {
            disposable = Observable.zip(instance.getUserManager().getUserById(profileId)
                    , instance.getEventManager().getEventByIdAndUpdate(eventId)
                    , Pair::create)
                    .subscribe(pair -> {
                        this.currentEvent = pair.second;
                        view.setSendTicketUI(pair.first, pair.second);
                    }, RxUtils.getEmptyErrorConsumer(TAG));
        }
        return disposable;
    }
}