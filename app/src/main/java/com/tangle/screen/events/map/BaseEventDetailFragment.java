package com.tangle.screen.events.map;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.tangle.R;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.views.StyledMapFragment;
import com.tangle.model.event.EventInfo;
import com.tangle.screen.events.can_invite.CanInviteGridFragment;
import com.tangle.screen.events.details.EventDetailsFragment;
import com.tangle.screen.events.details.EventDetailsPresenter;
import com.tangle.screen.events.details.PastEventDetailsFragment;
import com.tangle.screen.events.details.map.EventMapFragment;
import com.tangle.screen.linked_events.LinkedEventsView;

import java.util.List;

public abstract class BaseEventDetailFragment extends SecondaryScreenFragment implements BaseEventDetailView, OnMapReadyCallback {

    private FrameLayout mapContainer;
    protected LinkedEventsView linkedEventsView;
    private TextView tvEventPlace;

    private StyledMapFragment mapFragment;
    private boolean shouldDisplayStub;

    protected final static String GOING_FROM_KEY = "going_from";
    protected static final String BUNDLE_EVENT_KEY = "events_key";
    protected final static String GOING_FROM_EVENT_KEY = "going_from_event";

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        Bundle args = getArguments();
        String currentEventId = "";
        if (args != null) {
            currentEventId = getArguments().getString(BUNDLE_EVENT_KEY, "");
        }
        String finalCurrentEventId = currentEventId;
        mapContainer = view.findViewById(R.id.map_container);
        tvEventPlace = view.findViewById(R.id.txt_event_place);
        linkedEventsView = view.findViewById(R.id.linked_events_container);
        if (linkedEventsView != null) {
            linkedEventsView.updateClickListener(event -> addFragment(
                    event.hasPassed() ?
                            PastEventDetailsFragment.newInstance(event, EventDetailsPresenter.GoingFrom.LINKED_EVENTS, finalCurrentEventId) :
                            EventDetailsFragment.newInstance(event, EventDetailsPresenter.GoingFrom.LINKED_EVENTS)));
        }
    }

    protected void setMapVisibility(boolean isVisible) {
        tvEventPlace.setVisibility(isVisible ? View.VISIBLE : View.GONE);
        mapContainer.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void updateMapState(boolean shouldDisplayStub) {
        if (isInitialized() && this.shouldDisplayStub == shouldDisplayStub) {
            return;
        }
        this.shouldDisplayStub = shouldDisplayStub;
        mapFragment = (StyledMapFragment) getChildFragmentManager().findFragmentByTag(StyledMapFragment.class.getSimpleName());
        if (!shouldDisplayStub) {
            getView().getHandler().post(this::initMapFragment);
        } else {
            if (mapFragment != null) {
                getChildFragmentManager().beginTransaction().remove(mapFragment).commitNowAllowingStateLoss();
            }
            mapContainer.removeAllViews();
            mapContainer.addView(createStubImageView());
        }
    }

    @Override
    public void setEventMap(EventInfo event) {
        if (mapFragment != null) {
            mapFragment.setEvent(event);
        }
    }

    private void initMapFragment() {
        mapFragment = (StyledMapFragment) getChildFragmentManager().findFragmentByTag(StyledMapFragment.class.getSimpleName());
        if (mapFragment == null) {
            mapContainer.removeAllViews();
            mapFragment = new StyledMapFragment();
            getChildFragmentManager().beginTransaction()
                    .add(R.id.map_container, mapFragment, StyledMapFragment.class.getSimpleName())
                    .commitNowAllowingStateLoss();
        }
        setMapCallbacks();
    }

    private void setMapCallbacks() {
        mapFragment.setMapReadyCallback(this);
        mapFragment.setGesturesEnabled(false);
        mapFragment.setOnMapClickListener(v -> onMapClicked());
    }

    private boolean isInitialized() {
        return mapFragment != null;
    }

    private ImageView createStubImageView() {
        ImageView imageView = new ImageView(getContext());
        imageView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageResource(R.drawable.map_empty_image);
        return imageView;
    }

    protected abstract void onMapClicked();

    @Override
    public void goToMap(EventInfo event) {
        switchFragment(EventMapFragment.newInstance(event), true);
    }

    @Override
    public void setLinkedEvents(List<EventInfo> linkedEvents, boolean isFutureEvents) {
        if (!linkedEvents.isEmpty()) {
            linkedEventsView.setVisibility(View.VISIBLE);
            linkedEventsView.setLinkedEventList(linkedEvents, isFutureEvents);
        } else {
            linkedEventsView.setVisibility(View.GONE);
        }
    }



    @Override
    public void goToInviteScreen(String eventId) {
        switchFragment(CanInviteGridFragment.newInstance(eventId), true);
    }
}
