package com.tangle.screen.events.participant;

import com.tangle.model.profile_data.ProfileData;

import java.util.List;

public interface ParticipantGridView {
    void setProfileList(List<ProfileData> userList);

    void goToUser(String eventId, ProfileData profileData);
}
