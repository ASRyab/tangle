package com.tangle.screen.events.choose;

import com.tangle.model.event.EventInfo;

public interface ChooseEventClickListener {
    void onEventClicked(EventInfo event);

    void onButtonClicked(EventInfo event);
}