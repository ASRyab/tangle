package com.tangle.screen.events.main;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.image_preload.PreloadEventsPhotosModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.managers.EventManager;
import com.tangle.model.blog.BlogEntity;
import com.tangle.model.event.EventInfo;
import com.tangle.screen.brand_blog.BrandBlogContract;
import com.tangle.utils.RxUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Consumer;

public class MainEventsPresenter extends LifecyclePresenter<MainEventsView> {

    private Consumer<Throwable> errorConsumer = ErrorResponse::getServerMessage;
    private Observable<List<EventInfo>> refreshObservable = ApplicationLoader.getApplicationInstance().getEventManager().forceLoadNewEvent()
            .compose(transformEvents());
    private Observable<List<BlogEntity>> refreshBlogObservable;

    private BrandBlogContract.Model brandBlogModel;

    public MainEventsPresenter(BrandBlogContract.Model brandBlogModel) {
        this.brandBlogModel = brandBlogModel;
        refreshBlogObservable = brandBlogModel.forceLoad();
    }


    @Override
    public void attachToView(MainEventsView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        refreshData();
    }

    @SuppressLint("CheckResult")
    private void preloadImages(List<EventInfo> events) {
        new PreloadEventsPhotosModel(events).getTask()
                .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "preloadImages"));
    }

    public void refreshData() {
        monitor(refreshObservable
                .compose(RxUtils.withLoading(getView()))
                .subscribe(getView()::setUpcomingEvents, errorConsumer), State.DESTROY_VIEW);

        monitor(refreshBlogObservable
                .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "refreshBlog")), State.DESTROY_VIEW);

        monitor(brandBlogModel
                .getUnreadCount()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(res -> getView().updateNewPostBadge(res)
                        , RxUtils.getEmptyErrorConsumer()), State.DESTROY_VIEW);
    }


    @Override
    public void onStart() {
        super.onStart();
        monitor(ApplicationLoader.getApplicationInstance().getEventManager().getUpdateListEvents()
                .compose(transformEvents())
                .subscribe(getView()::setUpcomingEvents, errorConsumer), State.STOP);
    }

    private ObservableTransformer<List<EventInfo>, List<EventInfo>> transformEvents() {
        return upstream -> upstream
                .doOnNext(this::preloadImages)
                .flatMap(infos -> Observable.fromIterable(infos)
                        .compose(EventManager.checkStatusEvent())
                        .toList().toObservable());
    }

    public void onEventClicked(EventInfo event) {
        getView().showEvent(event);
    }

    void onBrandBlogClicked() {
        if (!ApplicationLoader.getApplicationInstance().getPreferenceManager().isBrandBlogPromoShown()
                && ApplicationLoader.getApplicationInstance().getMediaManager().hasCachedVideo()) {
            getView().openBrandBlogPromo();
        } else {
            getView().openBrandBlog();
        }
    }
}

