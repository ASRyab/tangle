package com.tangle.screen.events.details;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.analytics.SimpleActions;
import com.tangle.base.PrePaymentPresenter;
import com.tangle.base.PrePaymentView;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.recyclerview.decorations.SpaceItemDecoration;
import com.tangle.managers.DialogManager;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.model.payment.PaymentData;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.gallery.EventGalleryFragment;
import com.tangle.screen.events.gallery.GalleryAdapter;
import com.tangle.screen.events.map.BaseEventDetailFragment;
import com.tangle.screen.events.participant.ParticipantFragment;
import com.tangle.screen.events.popup.CongratsFragment;
import com.tangle.screen.events.popup.CongratsViewType;
import com.tangle.screen.linked_events.LinkedEventBottomWindowTwoActions;
import com.tangle.screen.main.ActivityHolder;
import com.tangle.screen.media.pager.MediaContainerFragment;
import com.tangle.screen.other_profile.ProfileEventPresenter;
import com.tangle.utils.GalleryUtils;
import com.tangle.utils.PlatformUtils;

import java.util.Collections;
import java.util.List;

public class PastEventDetailsFragment extends BaseEventDetailFragment implements PastEventDetailsView, PrePaymentView,
        MediaContainerFragment.MediaContainerCallback, OnMapReadyCallback {
    private PastEventDetailsPresenter presenter = new PastEventDetailsPresenter();
    private PrePaymentPresenter prePaymentPresenter = new PrePaymentPresenter(DialogManager.getInstance(this));

    private BaseEventDetailsView baseDetailsView;
    private RecyclerView galleryRV;
    private TextView galleryItemsAmountTV;
    private ViewGroup containerGallery;
    private GalleryAdapter galleryAdapter;
    private ViewGroup containerParticipants;
    private RecyclerView participantsRV;
    private ProfilesAdapter participantsAdapter;
    private TextView participantsAmountTV;
    private LinkedEventBottomWindowTwoActions bottomWindow;

    public static PastEventDetailsFragment newInstance(EventInfo event) {
        Bundle args = new Bundle();
        args.putString(BUNDLE_EVENT_KEY, event.eventId);
        PastEventDetailsFragment fragment = new PastEventDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static PastEventDetailsFragment newInstance(EventInfo event, PastEventDetailsPresenter.GoingFrom goingFrom, String goingFromEventId) {
        Bundle args = new Bundle();
        args.putString(BUNDLE_EVENT_KEY, event.eventId);
        args.putSerializable(GOING_FROM_KEY, goingFrom);
        args.putString(GOING_FROM_EVENT_KEY, goingFromEventId);
        PastEventDetailsFragment fragment = new PastEventDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String eventId = getArguments().getString(BUNDLE_EVENT_KEY);
        presenter.setEventId(eventId);
        presenter.setGoingFrom((EventDetailsPresenter.GoingFrom) getArguments().getSerializable(GOING_FROM_KEY));
        presenter.setGoingFromEventId(getArguments().getString(GOING_FROM_EVENT_KEY));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_past_event_details, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        baseDetailsView = new BaseEventDetailsView(view, this::openMediaItem);
        containerParticipants = view.findViewById(R.id.container_profiles_participants);
        participantsAmountTV = view.findViewById(R.id.txt_participants_amount);
        ((TextView) view.findViewById(R.id.txt_participants_title)).setText(getString(R.string.participants));
        participantsRV = view.findViewById(R.id.list_profiles_participants);
        galleryRV = view.findViewById(R.id.list_gallery);
        galleryItemsAmountTV = view.findViewById(R.id.txt_gallery_amount);
        containerGallery = view.findViewById(R.id.container_gallery);
        bottomWindow = view.findViewById(R.id.linked_event_bottom_window);

        galleryItemsAmountTV.setOnClickListener(v -> presenter.onGalleryItemsAmountClicked());
        initGallery();
        initParticipantList();
        setMapVisibility(false);

    }

    private void initParticipantList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(participantsRV.getContext(), LinearLayoutManager.HORIZONTAL, false);
        RecyclerView.ItemDecoration spaceDecoration = new SpaceItemDecoration(
                (int) PlatformUtils.Dimensions.dipToPixels(getContext(), 0),
                (int) PlatformUtils.Dimensions.dipToPixels(getContext(), 18), LinearLayoutManager.HORIZONTAL);
        participantsRV.setLayoutManager(layoutManager);
        participantsRV.setHasFixedSize(true);
        participantsRV.addItemDecoration(spaceDecoration);
        participantsAdapter = new ProfilesAdapter(id -> {
            if (!presenter.isFromLinkedEvents()) {
                ApplicationLoader.getApplicationInstance().getNavigationManager().showUserProfile(id, ProfileEventPresenter.GoingFrom.PARTICIPANTS, presenter.getEventId());
            } else {
                presenter.onParticipantsAmountClicked(ParticipantFragment.GoingFrom.LINKED_EVENT_PAST);
            }
        });
        participantsRV.setAdapter(participantsAdapter);
    }

    @Override
    public void setParticipants(List<ProfileData> profiles, boolean isFromLinkedEvents) {
        if (profiles != null && !profiles.isEmpty()) {
            containerParticipants.setVisibility(View.VISIBLE);
            participantsAdapter.switchProfiles(profiles);
            participantsAmountTV.setText(Integer.toString(profiles.size()));
            ParticipantFragment.GoingFrom goingFrom;
            if (isFromLinkedEvents) {
                goingFrom = ParticipantFragment.GoingFrom.LINKED_EVENT_PAST;
            } else {
                goingFrom = ParticipantFragment.GoingFrom.PAST;
            }
            participantsAmountTV.setOnClickListener(view -> presenter.onParticipantsAmountClicked(goingFrom));
            participantsAmountTV.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_arrow_right, 0);
        } else {
            containerParticipants.setVisibility(View.GONE);
        }
    }

    @Override
    public void goToParticipantsScreen(String eventId, ParticipantFragment.GoingFrom goingFrom) {
        switchFragment(ParticipantFragment.newInstance(eventId, goingFrom), true);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        baseDetailsView.destroyView();
    }

    private void initGallery() {
        galleryAdapter = GalleryUtils.setupListAndGetAdapter(galleryRV, getContext());
    }

    private void openMediaItem(EventInfo event, Media media) {
        MediaContainerFragment mediaContainerFragment = MediaContainerFragment.newInstance(event.getActualMediaList(), media, MediaContainerFragment.ShowType.OTHER);
        mediaContainerFragment.setTargetFragment(this, MediaContainerFragment.SET_MEDIA_INDEX_CODE);
        addFragment(mediaContainerFragment, true);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setGalleryItems(List<Media> galleryItems) {
        if (galleryItems != null && !galleryItems.isEmpty()) {
            containerGallery.setVisibility(View.VISIBLE);
            galleryItemsAmountTV.setText(String.valueOf(galleryItems.size()));
            galleryAdapter.switchGalleryItems(galleryItems);
            galleryAdapter.setMediaClickListener(media -> {
                ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.EVENT_CLICK_PHOTOBIG);
                MediaContainerFragment mediaContainerFragment = MediaContainerFragment.newInstance(galleryItems, media, MediaContainerFragment.ShowType.PAST_EVENT);
                addFragment(mediaContainerFragment, true);
            });
        } else {
            containerGallery.setVisibility(View.GONE);
        }
    }

    @Override
    public void setEventInfo(EventInfo event) {
        baseDetailsView.setEventInfo(event);
    }

    @Override
    public void goToEventGallery(String eventId) {
        addFragment(EventGalleryFragment.newInstance(eventId), true);
    }

    @Override
    public void setFutureEvent(EventInfo futureEvent) {
        Runnable book = () -> getPrePaymentPresenter().onBookClicked(futureEvent);
        Runnable invite = () -> presenter.onInviteClicked();
        bottomWindow.setEventInfo(futureEvent, book, invite);
        bottomWindow.slideUp();
    }

    @Override
    public void scrollMediaToPosition(int position) {
        baseDetailsView.multipleView.setCurrentItem(position);
    }

    @Override
    public void onFullScreenPageSelected(int position) {
        presenter.onMediaPageSelected(position);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        presenter.onMapInitialized();
    }

    @Override
    protected void onMapClicked() {
        presenter.onMapClicked();
    }

    @Override
    public void goToPP(PaymentData response) {
        ActivityHolder activity = (ActivityHolder) getActivity();
        activity.openPP(response);
    }

    @Override
    public void goToSuccess(String eventId, String userId, CongratsViewType type) {
        CongratsFragment fragment = CongratsFragment.newInstance(eventId, userId, type);
        addFragment(fragment, true);
    }

    @Override
    public PrePaymentPresenter getPrePaymentPresenter() {
        return prePaymentPresenter;
    }

    @Override
    public List<LifecyclePresenter> getPresenters() {
        return Collections.singletonList(prePaymentPresenter);
    }

}