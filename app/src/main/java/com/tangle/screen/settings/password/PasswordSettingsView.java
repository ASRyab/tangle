package com.tangle.screen.settings.password;

import com.tangle.base.ui.interfaces.LoadingView;

public interface PasswordSettingsView extends LoadingView {
    void closeView();

    void setOldPasswordError(String error);

    void setNewPasswordError(String error);
}