package com.tangle.screen.settings.email;

import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.profile.ChangeEmail;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.screen.auth.UserFieldsValidator;
import com.tangle.utils.RxUtils;

import io.reactivex.disposables.Disposable;

class EmailSettingsPresenter extends LifecyclePresenter<EmailSettingsView> {

    private UserFieldsValidator validator;

    EmailSettingsPresenter() {
        validator = UserFieldsValidator.getInstance();
    }

    private boolean validateEmail(String email) {
        getView().setMailError(null);
        return validator.validateEmail(getContext(), email, s -> getView().setMailError(s));
    }

    private boolean validatePassword(String password) {
        getView().setPasswordError(null);
        return validator.validatePassword(getContext(), password, s -> getView().setPasswordError(s));
    }


    void changeEmail(String newMail, String password) {
        if (validateEmail(newMail) & validatePassword(password)) {
            Disposable disposable = new ChangeEmail(newMail, password).getDataTask()
                    .compose(RxUtils.withLoading(getView()))
                    .subscribe(response -> successUpdate(),
                            error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
            monitor(disposable, State.DESTROY_VIEW);
        }
    }

    private void successUpdate() {
        ApplicationLoader.getApplicationInstance().getUserManager().loadCurrentUser();
        getView().showSuccessDialog();
    }

}