package com.tangle.screen.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.auth.logout.LogoutModel;
import com.tangle.api.rx_tasks.dictionary.GetContactUsInfo;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.contact_us.ContactUsPresenter;
import com.tangle.utils.RxUtils;

import io.reactivex.disposables.Disposable;

import static com.tangle.api.utils.Config.PRIVACY_POLICY_URL;
import static com.tangle.api.utils.Config.TERMS_OF_USE_URL;

public class SettingsPresenter extends LifecyclePresenter<SettingsView> {

    @Override
    public void attachToView(SettingsView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().currentUserWithUpdates()
                .subscribe(this::setProfileData, error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
    }

    private void setProfileData(ProfileData profile) {
        getView().setEmail(profile.email);
    }

    void onRateUsClick() {
        getView().processUrl("https://www.gettangle.com/");
    }

    void onContactUsClick() {
        monitor(new GetContactUsInfo().getDataTask().compose(RxUtils.withLoading(getView()))
                        .subscribe(getView()::showContactUsFragment, RxUtils.getEmptyErrorConsumer(ContactUsPresenter.class.getSimpleName())),
                State.DESTROY_VIEW);
    }

    void onTermsOfUseClick() {
        getView().processUrl(TERMS_OF_USE_URL);
    }

    void onPrivacyPolicyClick() {
        getView().processUrl(PRIVACY_POLICY_URL);
    }

    void onLogOutClick() {
        Disposable disposable = new LogoutModel().getTask()
                .subscribe(data -> getView().logOut(),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    void onPaymentHistoryClick() {
        getView().showPaymentHistoryFragment();
    }

    public void onBlockedUsersClick() {
        getView().showBlockedUsersFragment();
    }

    public void onChangeMailClick() {
        getView().showChangeMailFragment();
    }

    public void onChangePassClick() {
        getView().showChangePassFragment();
    }
}