package com.tangle.screen.settings;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.model.contact_us.ContactUsData;
import com.tangle.screen.auth.AuthorizationActivity;
import com.tangle.screen.bloked_users.BlockedUsersFragment;
import com.tangle.screen.contact_us.ContactUsFragment;
import com.tangle.screen.payment_history.PaymentHistoryFragment;
import com.tangle.screen.settings.email.EmailSettingsFragment;
import com.tangle.screen.settings.password.PasswordSettingsFragment;
import com.tangle.screen.web_browser.WebBrowserFragment;

public class SettingsFragment extends SecondaryScreenFragment implements SettingsView, View.OnClickListener {
    private SettingsPresenter presenter = new SettingsPresenter();
    private TextView vEmail;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_settings, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        TextView toolbarTitle = view.findViewById(R.id.txt_toolbar_title);
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarTitle.setText(getText(R.string.settings));
        vEmail = view.findViewById(R.id.txt_email);
        view.findViewById(R.id.btn_rate_this_app).setOnClickListener(this);
        view.findViewById(R.id.btn_contact_us).setOnClickListener(this);
        view.findViewById(R.id.btn_terms_of_use).setOnClickListener(this);
        view.findViewById(R.id.btn_privacy_policy).setOnClickListener(this);
        view.findViewById(R.id.btn_log_out).setOnClickListener(this);
        view.findViewById(R.id.btn_payment_history).setOnClickListener(this);
        view.findViewById(R.id.btn_blocked_users).setOnClickListener(this);
        view.findViewById(R.id.btn_change_pass).setOnClickListener(this);
        view.findViewById(R.id.btn_change_email).setOnClickListener(this);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_rate_this_app:
                presenter.onRateUsClick();
                break;
            case R.id.btn_contact_us:
                presenter.onContactUsClick();
                break;
            case R.id.btn_terms_of_use:
                presenter.onTermsOfUseClick();
                break;
            case R.id.btn_privacy_policy:
                presenter.onPrivacyPolicyClick();
                break;
            case R.id.btn_log_out:
                presenter.onLogOutClick();
                break;
            case R.id.btn_payment_history:
                presenter.onPaymentHistoryClick();
                break;
            case R.id.btn_blocked_users:
                presenter.onBlockedUsersClick();
                break;
            case R.id.btn_change_email:
                presenter.onChangeMailClick();
                break;
            case R.id.btn_change_pass:
                presenter.onChangePassClick();
                break;
            default:
                break;
        }
    }

    @Override
    public void processUrl(String url) {
        switchFragment(WebBrowserFragment.newInstance(url), true);
    }

    @Override
    public void logOut() {
        AuthorizationActivity.startAuth(getActivity());
        getActivity().finish();
    }

    @Override
    public void showPaymentHistoryFragment() {
        switchFragment(new PaymentHistoryFragment(), true);
    }

    @Override
    public void showBlockedUsersFragment() {
        switchFragment(new BlockedUsersFragment(), true);
    }

    @Override
    public void showChangeMailFragment() {
        switchFragment(new EmailSettingsFragment(), true);
    }

    @Override
    public void showChangePassFragment() {
        switchFragment(new PasswordSettingsFragment(), true);
    }

    @Override
    public void setEmail(String emailText) {
        vEmail.setText(emailText);
    }

    @Override
    public void showContactUsFragment(ContactUsData contactUsData) {
        switchFragment(ContactUsFragment.newInstance(contactUsData), true);
    }
}