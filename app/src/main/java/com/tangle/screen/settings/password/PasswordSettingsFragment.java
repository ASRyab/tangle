package com.tangle.screen.settings.password;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;

public class PasswordSettingsFragment extends SecondaryScreenFragment implements PasswordSettingsView {

    private PasswordSettingsPresenter presenter = new PasswordSettingsPresenter();

    private Button btnSave;

    private TextInputLayout inputLayoutPasswordOld;
    private TextInputLayout inputLayoutPasswordNew;
    private EditText inputPassword;
    private EditText inputPasswordNew;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_change_password, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        TextView toolbarTitle = view.findViewById(R.id.txt_toolbar_title);
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarTitle.setText(getText(R.string.settings_change_password_title));
        inputPassword = view.findViewById(R.id.input_password);
        inputPasswordNew = view.findViewById(R.id.input_password_new);
        inputLayoutPasswordOld = view.findViewById(R.id.input_layout_password);
        inputLayoutPasswordNew = view.findViewById(R.id.input_layout_password_new);
        btnSave = view.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(view1 -> presenter.updatePassword(inputPassword.getText().toString(), inputPasswordNew.getText().toString()));

        initOldPasswordInput();
        initNewPasswordInput();
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void closeView() {
        Toast.makeText(getContext(), R.string.update_successful, Toast.LENGTH_SHORT).show();
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    private void initOldPasswordInput() {
        inputPassword.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                inputLayoutPasswordOld.setError(null);
            }
        });
    }

    private void initNewPasswordInput() {
        inputPasswordNew.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                inputLayoutPasswordNew.setError(null);
            }
        });
    }

    @Override
    public void setOldPasswordError(String error) {
        inputLayoutPasswordOld.setError(error);
    }

    @Override
    public void setNewPasswordError(String error) {
        inputLayoutPasswordNew.setError(error);
    }
}