package com.tangle.screen.settings.email;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.managers.DialogManager;
import com.tangle.utils.PlatformUtils;

public class EmailSettingsFragment extends SecondaryScreenFragment implements EmailSettingsView {

    private EmailSettingsPresenter presenter = new EmailSettingsPresenter();

    private TextInputLayout emailInputLayout;
    private TextInputLayout passwordInputLayout;
    private EditText inputEmail;
    private EditText inputPassword;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_change_mail, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        TextView toolbarTitle = view.findViewById(R.id.txt_toolbar_title);
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarTitle.setText(getText(R.string.settings_change_email_title));
        emailInputLayout = view.findViewById(R.id.input_layout_mail);
        passwordInputLayout = view.findViewById(R.id.input_layout_password);
        inputEmail = view.findViewById(R.id.input_email);
        inputPassword = view.findViewById(R.id.input_password);

        Button btnSave = view.findViewById(R.id.btn_save);
        btnSave.setOnClickListener(view1 -> presenter.changeEmail(inputEmail.getText().toString(), inputPassword.getText().toString()));

        initLoginInput();
        initPasswordInput();
    }

    private void initLoginInput() {
        inputEmail.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                emailInputLayout.setError(null);
            }
        });
    }

    private void initPasswordInput() {
        inputPassword.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) {
                passwordInputLayout.setError(null);
            }
        });
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showSuccessDialog() {
        if (getActivity() != null) {
            PlatformUtils.Keyboard.hideKeyboard(getActivity());
            DialogManager.getInstance(this).showChangeEmailDialog((view, dialog) -> getActivity().onBackPressed());
        }
    }

    @Override
    public void setMailError(String error) {
        emailInputLayout.setError(error);
    }

    @Override
    public void setPasswordError(String error) {
        passwordInputLayout.setError(error);
    }

}
