package com.tangle.screen.settings.email;

import com.tangle.base.ui.interfaces.LoadingView;

public interface EmailSettingsView extends LoadingView {
    void showSuccessDialog();

    void setMailError(String error);

    void setPasswordError(String error);
}