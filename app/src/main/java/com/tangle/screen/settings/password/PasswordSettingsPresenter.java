package com.tangle.screen.settings.password;

import com.tangle.api.rx_tasks.profile.ChangePassword;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.screen.auth.UserFieldsValidator;
import com.tangle.utils.RxUtils;

import io.reactivex.disposables.Disposable;

class PasswordSettingsPresenter extends LifecyclePresenter<PasswordSettingsView> {

    private UserFieldsValidator validator;

    PasswordSettingsPresenter() {
        validator = UserFieldsValidator.getInstance();
    }

    private boolean validateOldPassword(String password) {
        getView().setOldPasswordError(null);
        return validator.validatePassword(getContext(), password, s -> getView().setOldPasswordError(s));
    }

    private boolean validateNewPassword(String password) {
        getView().setNewPasswordError(null);
        return validator.validatePassword(getContext(), password, s -> getView().setNewPasswordError(s));
    }

    private boolean validatePasswords(String oldPassword, String newPassword) {
        getView().setNewPasswordError(null);
        return validator.validatePasswords(getContext(), oldPassword, newPassword, s -> getView().setNewPasswordError(s));
    }

    void updatePassword(String oldPassword, String newPassword) {
        if (validateOldPassword(oldPassword) & validateNewPassword(newPassword) && validatePasswords(oldPassword, newPassword)) {
            Disposable disposable = new ChangePassword(oldPassword, newPassword).getDataTask()
                    .compose(RxUtils.withLoading(getView()))
                    .subscribe(response -> successUpdate(),
                            this::errorResponse);
                            monitor(disposable, State.DESTROY_VIEW);
        }
    }

    private void successUpdate() {
        getView().closeView();
    }


    private void errorResponse(Object throwable) {
        if (throwable instanceof ErrorResponse) {
            String message = ((ErrorResponse) throwable).getMeta().getFirstMessageByKey("oldPassword");
            if (message != null) {
                getView().setOldPasswordError(message);
            }
        } else if (throwable instanceof Throwable) {
            getView().showAPIError(((Throwable) throwable).getMessage());
        }
    }
}