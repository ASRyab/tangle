package com.tangle.screen.settings;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.contact_us.ContactUsData;

public interface SettingsView extends LoadingView {

    void processUrl(String url);

    void logOut();

    void showPaymentHistoryFragment();

    void showContactUsFragment(ContactUsData contactUsData);

    void showBlockedUsersFragment();

    void showChangeMailFragment();

    void showChangePassFragment();

    void setEmail(String email);
}