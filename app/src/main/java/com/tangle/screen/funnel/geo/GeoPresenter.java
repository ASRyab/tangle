package com.tangle.screen.funnel.geo;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Pair;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.rx_tasks.funnel.geo_cities.GeoCitiesModel;
import com.tangle.api.rx_tasks.funnel.geo_regions.GeoRegionsModel;
import com.tangle.api.rx_tasks.profile.update.ProfileUpdateModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.funnel.ProfileDataMapper;
import com.tangle.utils.RxUtils;

import java.util.ArrayList;
import java.util.Objects;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class GeoPresenter extends LifecyclePresenter<GeoView> {
    private ProfileData profileData;
    private String city;
    private String region;
    private boolean isFromFunnel;

    public void setIsFromFunnel(boolean isFromFunnel) {
        this.isFromFunnel = isFromFunnel;
    }

    @Override
    public void attachToView(GeoView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (!isFromFunnel) {
            monitor(ApplicationLoader.getApplicationInstance().getUserManager().currentUser()
                    .subscribe(this::setProfileData, error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
        } else {
            checkGeo();
        }
    }

    private void setProfileData(ProfileData profileData) {
        this.profileData = profileData;
        this.city = profileData.geo.city;
        this.region = profileData.geo.region;
        checkGeo();
        loadGeoData();
    }

    private void checkGeo() {
        GeoView view = getView();
        view.setChooseCityState();
        loadGeoData();
    }

    private void loadGeoData() {
        Disposable disposable = Observable.zip(new GeoCitiesModel(ProfileDataMapper.GREAT_BRITAIN, "").getTask(),
                new GeoRegionsModel(ProfileDataMapper.GREAT_BRITAIN, ProfileDataMapper.LONDON).getTask(), Pair::create)
                .compose(RxUtils.withLoading(getView()))
                .subscribe(pair -> getView().setGeo(new ArrayList<>(pair.first), new ArrayList<>(pair.second)),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    public void onCitySelected(String city) {
        this.city = city;
        getView().setCity(city);
        if (ProfileDataMapper.LONDON.equalsIgnoreCase(city)) {
            getView().setChooseRegionState();
        } else {
            getView().setDataState();
        }
    }

    public void onRegionSelected(String region) {
        if (TextUtils.isEmpty(region)) {
            if (TextUtils.isEmpty(this.region)
                    && profileData != null
                    && profileData.geo != null
                    && profileData.geo.region != null) {
                region = profileData.geo.region;
            } else {
                region = this.region;
            }
        }
        this.region = region;
        getView().setRegion(region);
        getView().setDataState();
    }

    public void onPickCityClicked() {
        this.city = null;
        this.region = null;
        getView().setChooseCityState();
    }

    public void onPickRegionClicked() {
        this.region = null;
        getView().setChooseRegionState();
    }

    public void onNextClicked() {
        if (ProfileDataMapper.LONDON.equalsIgnoreCase(city) && (region != null)) {
            getView().goNextScreen(city, region);
        } else if (city != null && !ProfileDataMapper.LONDON.equalsIgnoreCase(city)) {
            getView().goNextScreen(city, region);
        } else {
            getView().setError(getResources().getString(R.string.funnel_error_fill_geo_fields));
        }
    }

    public void onSaveClicked() {
        if (!ProfileDataMapper.LONDON.equalsIgnoreCase(city) && !profileData.geo.city.equals(city)
                || ProfileDataMapper.LONDON.equalsIgnoreCase(city) && !Objects.equals(profileData.geo.region, region)) {
            if (ProfileDataMapper.LONDON.equalsIgnoreCase(city) && TextUtils.isEmpty(region.trim())) {
                getView().showAPIError(null);
                return;
            }
            ProfileData profile = new ProfileDataMapper().mapValue(profileData);
            profile.geo.city = city;
            if (ProfileDataMapper.LONDON.equalsIgnoreCase(city)) {
                profile.geo.region = region;
            }
            Disposable disposable = new ProfileUpdateModel(profile).saveGeo()
                    .compose(RxUtils.withLoading(getView()))
                    .subscribe(profileData -> {
                        getView().showAPIError(getResources().getString(R.string.update_successful));
                        getView().close();
                    }, error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
            monitor(disposable, State.DESTROY_VIEW);
        } else {
            getView().showAPIError(null);
        }
    }
}