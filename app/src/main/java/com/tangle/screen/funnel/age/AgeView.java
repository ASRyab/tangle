package com.tangle.screen.funnel.age;

import com.tangle.base.ui.interfaces.AutoCloseable;
import com.tangle.base.ui.interfaces.ErrorView;
import com.tangle.base.ui.interfaces.LoadingView;

public interface AgeView extends ErrorView, LoadingView, AutoCloseable {
    void openNextScreen(int age);

    void setAge(int age);
}