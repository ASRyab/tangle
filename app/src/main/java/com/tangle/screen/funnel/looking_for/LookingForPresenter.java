package com.tangle.screen.funnel.looking_for;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.rx_tasks.profile.update.ProfileUpdateModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.pojos.Gender;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.funnel.ProfileDataMapper;

import io.reactivex.disposables.Disposable;

public class LookingForPresenter extends LifecyclePresenter<LookingForView> {
    private ProfileData profileData;
    private Gender targetGender;
    private boolean isFromFunnel;

    public void setIsFromFunnel(boolean isFromFunnel) {
        this.isFromFunnel = isFromFunnel;
    }

    @Override
    public void attachToView(LookingForView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (!isFromFunnel) {
            monitor(ApplicationLoader.getApplicationInstance().getUserManager().currentUserWithUpdates()
                    .subscribe(this::setProfileData, error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
        }
    }

    private void setProfileData(ProfileData profileData) {
        this.profileData = profileData;
        targetGender = profileData.looking.gender;
        getView().setError(null);
        getView().setLookingFor(targetGender);
    }

    public void onSaveClicked() {
        if (!profileData.looking.gender.equals(targetGender)) {
            ProfileData profile = new ProfileDataMapper().mapValue(profileData);
            profile.looking.gender = targetGender;
            Disposable disposable = new ProfileUpdateModel(profile).saveLookingFor().subscribe(profileData -> getView().showAPIError(getResources().getString(R.string.update_successful)), error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
            monitor(disposable, State.DESTROY_VIEW);
        } else {
            getView().showAPIError(null);
        }
    }

    public void onNextClicked() {
        if (targetGender != null) {
            getView().setError(null);
            getView().goNextScreen(targetGender);
        } else {
            getView().setError(getResources().getString(R.string.funnel_error_choose_looking_for));
        }
    }

    public void onSwitchClicked(Gender lookingFor) {
        this.targetGender = lookingFor;
        getView().setLookingFor(lookingFor);
    }
}