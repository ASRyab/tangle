package com.tangle.screen.funnel.terms;

import com.tangle.ApplicationLoader;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.screen.auth.register.RegisterPresenter;
import com.tangle.utils.PreferenceManager;

import static com.tangle.api.utils.Config.PRIVACY_POLICY_URL;
import static com.tangle.api.utils.Config.TERMS_OF_USE_URL;

public class TermsPresenter extends LifecyclePresenter<TermsView> {
    private boolean isTermsAccept;
    private boolean isPrivacyAccept;

    public void termsChecked(boolean isChecked) {
        isTermsAccept = isChecked;
        getView().showTermsWarning(false);
    }

    public void privacyChecked(boolean isChecked) {
        isPrivacyAccept = isChecked;
        getView().showPrivacyWarning(false);
    }

    void onTermsOfUseClick() {
        getView().processUrl(TERMS_OF_USE_URL);
    }

    void onPrivacyPolicyClick() {
        getView().processUrl(PRIVACY_POLICY_URL);
    }

    public void onContinuedClicked() {
        if (!isTermsAccept && !isPrivacyAccept) {
            getView().showTermsWarning(true);
            getView().showPrivacyWarning(true);
            return;
        } else if (isTermsAccept && !isPrivacyAccept) {
            getView().showPrivacyWarning(true);
            return;
        } else if (!isTermsAccept && isPrivacyAccept) {
            getView().showTermsWarning(true);
            return;
        }
        makeRegRequest();
    }

    private void makeRegRequest() {
        PreferenceManager manager = ApplicationLoader.getApplicationInstance().getPreferenceManager();
        manager.saveTermsAccepted(true);
        RegisterPresenter.isAfterTerms = true;
        getView().goBack();
    }
}