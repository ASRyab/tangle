package com.tangle.screen.funnel.avatar

import com.tangle.base.ui.dialogs.StyledDialog
import com.tangle.base.ui.interfaces.ErrorView
import com.tangle.base.ui.interfaces.LoadingView
import com.tangle.screen.photoupload.PhotoRulesOpenView

interface AvatarView : ErrorView, LoadingView, PhotoRulesOpenView {

    fun goToMainScreen()

    fun setProfilePhoto(primaryPhoto: String)

    fun goToAppNotAvailableScreen()

    fun showWaitApprovePhotoDialog(listener: StyledDialog.DialogButtonClickListener)
}