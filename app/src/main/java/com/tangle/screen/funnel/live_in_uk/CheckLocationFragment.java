package com.tangle.screen.funnel.live_in_uk;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.screen.funnel.FunnelFragment;
import com.tangle.screen.funnel.geo.GeoFragment;
import com.tangle.screen.funnel.industry.IndustryFragment;

import static com.tangle.screen.funnel.FunnelConstants.UPDATE_FROM_WHITE_LIST;

public class CheckLocationFragment extends FunnelFragment implements CheckLocationContract.View {

    private View txtTitle;
    private View containerBtns;

    private CheckLocationPresenter presenter = new CheckLocationPresenter(new CheckLocationModel());

    public static Fragment newInstance(Bundle args) {
        Fragment fragment = new CheckLocationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_funnel_check_location, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        Button btnYes = view.findViewById(R.id.btn_yes);
        Button btnNo = view.findViewById(R.id.btn_no);
        txtTitle = view.findViewById(R.id.tvTitle);
        containerBtns = view.findViewById(R.id.container_btns);

        txtTitle.setVisibility(View.INVISIBLE);
        containerBtns.setVisibility(View.INVISIBLE);
        Bundle arguments = getArguments();

        btnYes.setOnClickListener((v) -> {
            if (arguments != null) {
                arguments.putBoolean(UPDATE_FROM_WHITE_LIST, true);
            }
            switchFragment(GeoFragment.newInstance(arguments, true), true);
        });
        btnNo.setOnClickListener((v) ->
        {
            if (arguments != null) {
                arguments.putBoolean(UPDATE_FROM_WHITE_LIST, false);
            }
            switchFragment(IndustryFragment.newInstance(getArguments(), true), true);
        });
    }

    @Override
    protected int getStep() {
        return 4;
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void openGeoFragment() {
        switchFragmentWithPopLast(GeoFragment.newInstance(getArguments(), true));
    }

    @Override
    public void showContent() {
        txtTitle.setVisibility(View.VISIBLE);
        containerBtns.setVisibility(View.VISIBLE);
    }
}