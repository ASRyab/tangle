package com.tangle.screen.funnel.age;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.rx_tasks.profile.update.ProfileUpdateModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.funnel.ProfileDataMapper;
import com.tangle.utils.RxUtils;

import io.reactivex.disposables.Disposable;

public class AgePresenter extends LifecyclePresenter<AgeView> {
    private AgeValidator validator = new AgeValidator();
    private ProfileData profileData;
    private boolean isFromFunnel;
    private int age;

    public void setIsFromFunnel(boolean isFromFunnel) {
        this.isFromFunnel = isFromFunnel;
    }

    @Override
    public void attachToView(AgeView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (!isFromFunnel) {
            monitor(ApplicationLoader.getApplicationInstance().getUserManager().currentUser()
                    .subscribe(this::setProfileData, error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
        } else {
            view.setAge(age);
        }
    }

    private void setProfileData(ProfileData profileData) {
        this.profileData = profileData;
        this.age = profileData.age;
        getView().setAge(age);
    }

    public void onNextClicked(int age) {
        if (!validator.validateAge(age)) {
            getView().setError(getResources().getString(R.string.funnel_error_choose_age, age));
        } else {
            this.age = age;
            getView().setError(null);
            openNextScreen(age);
        }
    }

    private void openNextScreen(int age) {
            getView().openNextScreen(age);
    }

    public void onSaveClicked(int age) {
        if (!profileData.age.equals(age)) {
            ProfileData profile = new ProfileDataMapper().mapValue(profileData, age);
            Disposable disposable = new ProfileUpdateModel(profile).saveAge()
                    .compose(RxUtils.withLoading(getView()))
                    .subscribe(profileData -> {
                        getView().showAPIError(getResources().getString(R.string.update_successful));
                        getView().close();
                    }, error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
            monitor(disposable, State.DESTROY_VIEW);
        } else {
            getView().showAPIError(null);
        }
    }
}