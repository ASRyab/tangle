package com.tangle.screen.funnel.you_are;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.model.pojos.Gender;
import com.tangle.screen.funnel.FunnelConstants;
import com.tangle.screen.funnel.FunnelFragment;
import com.tangle.screen.funnel.looking_for.LookingForFragment;

public class YouAreFragment extends FunnelFragment implements YouAreView {

    private YouArePresenter presenter = new YouArePresenter();

    public static YouAreFragment newInstance() {
        Bundle args = new Bundle();
        args.putBoolean(IS_FROM_FUNNEL_KEY, true);
        YouAreFragment fragment = new YouAreFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private Button menButton;
    private Button womenButton;
    private Button nextButton;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_funnel_you_are, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        menButton = view.findViewById(R.id.btn_men);
        womenButton = view.findViewById(R.id.btn_women);
        nextButton = view.findViewById(R.id.btn_next);
        nextButton.setOnClickListener(v -> presenter.onNextClicked());
        initSwitches();
        view.findViewById(R.id.btn_back).setVisibility(View.INVISIBLE);
    }

    @Override
    protected int getStep() {
        return 1;
    }

    private void initSwitches() {
        View.OnClickListener lookingForButtonClickListener = v -> {
            switch (v.getId()) {
                case R.id.btn_men:
                    presenter.onSwitchClicked(Gender.MALE);
                    break;
                case R.id.btn_women:
                    presenter.onSwitchClicked(Gender.FEMALE);
                    break;
            }
        };
        menButton.setOnClickListener(lookingForButtonClickListener);
        womenButton.setOnClickListener(lookingForButtonClickListener);
        menButton.setSelected(false);
        womenButton.setSelected(false);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void goNextScreen(Gender gender) {
        getArguments().putSerializable(FunnelConstants.GENDER, gender);
        switchFragment(LookingForFragment.newInstance(getArguments(), true), true);
    }

    @Override
    public void setOwnGender(Gender gender) {
        switch (gender) {
            case MALE:
                menButton.setSelected(true);
                womenButton.setSelected(false);
                break;
            case FEMALE:
                menButton.setSelected(false);
                womenButton.setSelected(true);
                break;
            default:
                menButton.setSelected(false);
                womenButton.setSelected(false);
                break;
        }
    }
}