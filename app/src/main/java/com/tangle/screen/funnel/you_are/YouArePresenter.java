package com.tangle.screen.funnel.you_are;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.R;
import com.tangle.api.rx_tasks.dictionary.GetCareersModel;
import com.tangle.api.rx_tasks.dictionary.GetIndustriesModel;
import com.tangle.api.rx_tasks.dictionary.GetInterestsModel;
import com.tangle.api.rx_tasks.funnel.geo_cities.GeoCitiesModel;
import com.tangle.api.rx_tasks.funnel.geo_regions.GeoRegionsModel;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.model.pojos.Gender;
import com.tangle.screen.funnel.ProfileDataMapper;
import com.tangle.screen.funnel.live_in_uk.CheckLocationModel;
import com.tangle.utils.RxUtils;

import io.reactivex.functions.Consumer;

public class YouArePresenter extends LifecyclePresenter<YouAreView> {
    private static final String TAG = YouArePresenter.class.getSimpleName();

    private Gender myGender;

    @Override
    public void attachToView(YouAreView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        preloadData();
    }

    public void onNextClicked() {
        if (myGender != null) {
            getView().goNextScreen(myGender);
        } else {
            getView().setError(getResources().getString(R.string.funnel_error_choose_gender));
        }
    }

    public void onSwitchClicked(Gender gender) {
        this.myGender = gender;
        getView().setError(null);
        getView().setOwnGender(gender);
    }

    @SuppressLint("CheckResult")
    private void preloadData() {
        Consumer dataConsumer = RxUtils.getEmptyDataConsumer();
        Consumer<Throwable> errorConsumer = RxUtils.getEmptyErrorConsumer(TAG, "preloadData");
        new GetIndustriesModel().getTask().subscribe(dataConsumer, errorConsumer);
        new GetCareersModel().getTask().subscribe(dataConsumer, errorConsumer);
        new GetInterestsModel().getTask().subscribe(dataConsumer, errorConsumer);
        new GeoCitiesModel(ProfileDataMapper.GREAT_BRITAIN, "").getTask().subscribe(dataConsumer, errorConsumer);
        new GeoRegionsModel(ProfileDataMapper.GREAT_BRITAIN, ProfileDataMapper.LONDON).getTask().subscribe(dataConsumer, errorConsumer);
        new CheckLocationModel().getIsTargetLocation().subscribe(dataConsumer, errorConsumer);
    }

}
