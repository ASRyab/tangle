package com.tangle.screen.funnel.you_are;

import com.tangle.base.ui.interfaces.ErrorView;
import com.tangle.model.pojos.Gender;

public interface YouAreView extends ErrorView {

    void goNextScreen(Gender gender);

    void setOwnGender(Gender gender);

}
