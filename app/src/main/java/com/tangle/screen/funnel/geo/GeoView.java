package com.tangle.screen.funnel.geo;

import com.tangle.base.ui.interfaces.AutoCloseable;
import com.tangle.base.ui.interfaces.ErrorView;
import com.tangle.base.ui.interfaces.LoadingView;

import java.util.List;

public interface GeoView extends ErrorView, LoadingView, AutoCloseable {

    void setGeo(List<String> cities, List<String> regions);

    void setCity(String city);

    void setRegion(String region);

    void setChooseCityState();

    void setChooseRegionState();

    void setDataState();

    void goNextScreen(String city, String region);
}