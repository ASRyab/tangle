package com.tangle.screen.funnel;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.ui.SecondaryScreenFragment;

public abstract class FunnelFragment extends SecondaryScreenFragment {
    protected static final String IS_FROM_FUNNEL_KEY = "is_from_funnel_key";
    private static final int TOTAL_STEPS = 8;

    protected boolean isFromFunnel;

    private TextView stepCountTextView;
    private TextView errorTextView;
    private View toolbar;

    public FunnelFragment() {
        if (getArguments() == null) {
            setArguments(new Bundle());
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isFromFunnel = getArguments().getBoolean(IS_FROM_FUNNEL_KEY, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        if(isFromFunnel) {
            stepCountTextView = view.findViewById(R.id.txt_step_count);
            errorTextView = view.findViewById(R.id.txt_error);
            stepCountTextView.setText(String.format("%d/%d", getStep(), TOTAL_STEPS));
            toolbar = view.findViewById(R.id.toolbar_container);
            toolbar.findViewById(R.id.btn_back).setVisibility(getStep() == 1
                    ? View.INVISIBLE
                    : View.VISIBLE);
        }
    }

    public View getToolbar() {
        return toolbar;
    }

    protected abstract int getStep();

    public void setError(String error) {
        if (errorTextView == null) return;
        if (TextUtils.isEmpty(error)) {
            errorTextView.setVisibility(View.INVISIBLE);
        } else {
            errorTextView.setVisibility(View.VISIBLE);
            errorTextView.setText(error);
        }
    }
}