package com.tangle.screen.funnel.career;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.recyclerview.decorations.SpaceItemDecoration;
import com.tangle.model.pojos.DictionaryItem;
import com.tangle.screen.edit_own_profile.included_fragments.ChangeItemSpacesDecoration;
import com.tangle.screen.funnel.FunnelConstants;
import com.tangle.screen.funnel.FunnelFragment;
import com.tangle.screen.funnel.interests.InterestsFragment;
import com.tangle.utils.PlatformUtils;

import java.util.List;

public class CareerFragment extends FunnelFragment implements CareerView {
    private CareerPresenter presenter = new CareerPresenter();

    private CareerAdapter adapter;
    private RecyclerView recyclerView;
    private Button actionButton;

    public static CareerFragment newInstance(Bundle args, boolean isFromFunnel) {
        if(args == null) {
            args = new Bundle();
        }
        args.putBoolean(IS_FROM_FUNNEL_KEY, isFromFunnel);
        CareerFragment fragment = new CareerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setIsFromFunnel(isFromFunnel);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(isFromFunnel ? R.layout.fragment_funnel_career : R.layout.fragment_change_my_carier, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);

        initUI(view);
    }

    private void initUI(View view) {
        if(!isFromFunnel) {
            initToolbar(getString(R.string.carier_level_is), true);
        }
        recyclerView = view.findViewById(android.R.id.list);
        actionButton = isFromFunnel ? view.findViewById(R.id.btn_next) : view.findViewById(R.id.btn_save);
        actionButton.setOnClickListener(v -> {
            if(isFromFunnel) {
                presenter.onNextClicked(adapter.getSelectedCareer());
            } else {
                presenter.onSaveClicked(adapter.getSelectedCareer());
            }
        });
        initList();
    }

    private void initList() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        if(isFromFunnel) {
            recyclerView.addItemDecoration(new SpaceItemDecoration(0,
                    (int) PlatformUtils.Dimensions.dipToPixels(getActivity(), 80),
                    LinearLayoutManager.VERTICAL));
        } else {
            recyclerView.addItemDecoration(new ChangeItemSpacesDecoration(0,
                    (int) PlatformUtils.Dimensions.dipToPixels(getActivity(), 120)));
        }
        adapter = new CareerAdapter(isFromFunnel);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected int getStep() {
        return 6;
    }

    @Override
    public void setError(String error) {
        adapter.setError(error);
        recyclerView.smoothScrollToPosition(0);
    }

    @Override
    public void setAvailableCareers(List<DictionaryItem> careers) {
        adapter.switchCareers(careers);
    }

    @Override
    public void setSelectedCareer(DictionaryItem career) {
        adapter.setSelectedCareer(career);
    }

    @Override
    public void goNextScreen(DictionaryItem career) {
        getArguments().putSerializable(FunnelConstants.CAREER, career);
        switchFragment(InterestsFragment.newInstance(getArguments(), true), true);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}