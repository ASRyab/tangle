package com.tangle.screen.funnel.live_in_uk;

import com.tangle.base.ui.interfaces.AutoCloseable;
import com.tangle.base.ui.interfaces.ErrorView;
import com.tangle.base.ui.interfaces.LoadingView;

import io.reactivex.Observable;

public interface CheckLocationContract {
    interface View extends ErrorView, LoadingView, AutoCloseable {
        void openGeoFragment();

        void showContent();
    }

    interface Model {
        Observable<Integer> getIsTargetLocation();
    }
}