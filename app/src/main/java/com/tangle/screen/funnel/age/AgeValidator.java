package com.tangle.screen.funnel.age;

public class AgeValidator {

    public static final int DEFAULT_AGE = 32;
    private boolean validateCalled;

    public boolean validateAge(int age) {
        boolean result = !(age == DEFAULT_AGE && !validateCalled);
        validateCalled = true;
        return result;
    }

}
