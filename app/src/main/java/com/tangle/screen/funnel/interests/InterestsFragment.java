package com.tangle.screen.funnel.interests;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tangle.R;
import com.tangle.base.implementation.adapters.InterestsAdapter;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.recyclerview.layout_managers.flex.FlexDirection;
import com.tangle.base.ui.recyclerview.layout_managers.flex.FlexWrap;
import com.tangle.base.ui.recyclerview.layout_managers.flex.FlexboxLayoutManager;
import com.tangle.base.ui.recyclerview.layout_managers.flex.JustifyContent;
import com.tangle.model.pojos.DictionaryItem;
import com.tangle.screen.funnel.FunnelConstants;
import com.tangle.screen.funnel.FunnelFragment;
import com.tangle.screen.funnel.avatar.AvatarFragment;

import java.util.ArrayList;
import java.util.List;

public class InterestsFragment extends FunnelFragment implements InterestsView {
    private InterestsPresenter presenter = new InterestsPresenter();

    private InterestsAdapter adapter;
    private RecyclerView recyclerView;
    private Button actionButton;

    public static InterestsFragment newInstance(Bundle args, boolean isFromFunnel) {
        if(args == null) {
            args = new Bundle();
        }
        args.putBoolean(IS_FROM_FUNNEL_KEY, isFromFunnel);
        InterestsFragment fragment = new InterestsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setIsFromFunnel(isFromFunnel);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(isFromFunnel ? R.layout.fragment_funnel_interests : R.layout.fragment_change_my_interest, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);

        initUI(view);
    }

    private void initUI(View view) {
        if(!isFromFunnel) {
            initToolbar(getString(R.string.my_interest), true);
        }
        recyclerView = view.findViewById(android.R.id.list);
        actionButton = isFromFunnel ? view.findViewById(R.id.btn_next) : view.findViewById(R.id.btn_save);
        actionButton.setOnClickListener(v -> {
            if(isFromFunnel) {
                presenter.onNextClicked(adapter.getSelectedInterests());
            } else {
                presenter.onSaveClicked(adapter.getSelectedInterests());
            }
        });
        initList();
    }

    private void initList() {
        adapter = new InterestsAdapter();
        adapter.setSelectAvailable(true);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getActivity(), FlexDirection.ROW, FlexWrap.WRAP);
        layoutManager.setJustifyContent(JustifyContent.CENTER);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void setAllInterests(List<DictionaryItem> interests) {
        adapter.switchInterests(interests);
    }

    @Override
    public void setSelectedInterests(List<DictionaryItem> interests) {
        adapter.setSelectedInterests(interests);
    }

    @Override
    public void goNextScreen(List<DictionaryItem> interests) {
        AvatarFragment fragment = new AvatarFragment();
        getArguments().putSerializable(FunnelConstants.INTERESTS, new ArrayList<>(interests));
        fragment.setArguments(getArguments());
        switchFragment(fragment, true);
    }

    @Override
    protected int getStep() {
        return 7;
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}