package com.tangle.screen.funnel.avatar

import android.os.Bundle
import com.appsflyer.AFInAppEventParameterName
import com.appsflyer.AFInAppEventType
import com.appsflyer.AppsFlyerLib
import com.tangle.ApplicationLoader
import com.tangle.analytics.SimpleActions
import com.tangle.api.rx_tasks.funnel.init.GetFields
import com.tangle.api.rx_tasks.funnel.init.PostFields
import com.tangle.api.rx_tasks.photo.UploadPhotoModel
import com.tangle.api.rx_tasks.profile.OwnProfileModel
import com.tangle.api.rx_tasks.profile.update.ProfileUpdateLocation
import com.tangle.api.rx_tasks.profile.update.ProfileUpdateModel
import com.tangle.api.utils.Config
import com.tangle.api.utils.ErrorResponse
import com.tangle.base.mvp.LifecyclePresenter
import com.tangle.base.mvp.State
import com.tangle.base.ui.dialogs.StyledDialog
import com.tangle.managers.PhotoManager
import com.tangle.model.funnel.FunnelData
import com.tangle.model.funnel.UserAttributes
import com.tangle.model.profile_data.ProfileData
import com.tangle.model.profile_data.ProfileUpdateData
import com.tangle.screen.funnel.FunnelConstants
import com.tangle.screen.funnel.ProfileDataMapper
import com.tangle.utils.PreferenceManager
import com.tangle.utils.RxUtils
import com.tangle.utils.UserCheckUtils
import io.reactivex.Observable
import io.reactivex.functions.Consumer
import java.io.File
import java.util.*

class AvatarPresenter : LifecyclePresenter<AvatarView>() {
    private val dataMapper = ProfileDataMapper()
    private var isFromWeb: Boolean = false
    private var updateFromWhiteList: Boolean = false

    override fun attachToView(view: AvatarView, savedInstanceState: Bundle?) {
        super.attachToView(view, savedInstanceState)
        monitor(ApplicationLoader.getApplicationInstance().userManager.currentUserWithUpdates()
                .subscribe({ profile -> this.setProfileData(profile) }, { error -> getView().showAPIError(ErrorResponse.getServerMessage(error)) })
                , State.DESTROY_VIEW)
    }

    private fun setProfileData(profile: ProfileData) {
        profile.primaryPhoto?.normal?.let { view.setProfilePhoto(it) }
    }

    private fun getFunnelObservable(profileData: ProfileData, isFromWeb: Boolean): Observable<FunnelData> {
        return if (isFromWeb)
            Observable.empty()
        else
            GetFields().dataTask
                    .flatMap { data ->
                        data.userAttributes = UserAttributes()
                        data.userAttributes.gender = profileData.gender.getName()
                        PostFields(data).dataTask
                    }
    }

    fun uploadAvailablePhoto(args: Bundle, photoFile: File?) {
        view.setError(null)
        val uploadPhotoObservable = UploadPhotoModel(photoFile, false).task
        val profileData = dataMapper.mapValues(args)
        val profileObservable = ProfileUpdateModel(profileData).task
        val updateLocationObservable = getWhiteListObservable(updateFromWhiteList)

        val concatDisposable = Observable.concatArray(getFunnelObservable(profileData, isFromWeb)
                , uploadPhotoObservable
                , updateLocationObservable
                , profileObservable
                , OwnProfileModel().task)
                .takeLast(1)
                .compose(RxUtils.withLoading(view))
                .subscribe(RxUtils.getEmptyDataConsumer()
                        , Consumer { error -> view.showAPIError(ErrorResponse.getServerMessage(error)) })
        monitor(concatDisposable, State.DESTROY_VIEW)
    }

    override fun onStart() {
        super.onStart()
        monitor(ApplicationLoader.getApplicationInstance().photoManager.getPhotoStatusObservable().subscribe { status ->
            if (status == PhotoManager.PhotoStatus.PHOTO_APPROVE_STATUS_WAITING_FOR_APPROVE) {
                view.showWaitApprovePhotoDialog(StyledDialog.DialogButtonClickListener { _, _ -> checkUserInSupportedCountry(isFromWeb) })
            } else {
                checkUserInSupportedCountry(isFromWeb)
            }
        }, State.STOP)
    }

    private fun getWhiteListObservable(updateFromWhiteList: Boolean): Observable<ProfileUpdateData> {
        return if (updateFromWhiteList)
            ProfileUpdateLocation("London", "GBR").dataTask
                    .doOnNext { PreferenceManager.getInstance(context).saveTargetCountry(1) }
        else
            Observable.empty()
    }

    private fun checkUserInSupportedCountry(isFromWeb: Boolean) {
        if (UserCheckUtils.isNotSupportedCountry(PreferenceManager.getInstance(context).targetCountry!!)) {
            view.goToAppNotAvailableScreen()
        } else {
            if (!isFromWeb && Config.isTracking()) {
                trackRegistrationToAF()
                ApplicationLoader.getApplicationInstance().analyticManager.trackEvent(SimpleActions.REGISTRATION_COMPLETE)
            }
            view.goToMainScreen()
        }
    }

    private fun trackRegistrationToAF() {
        val params = HashMap<String, Any>()
        params[AFInAppEventParameterName.REGSITRATION_METHOD] = "default"
        AppsFlyerLib.getInstance().trackEvent(context, AFInAppEventType.COMPLETE_REGISTRATION, params)
    }

    fun onRulesClick() {
        view.showRules()
    }

    fun setArguments(args: Bundle) {
        isFromWeb = args.getBoolean(FunnelConstants.IS_FROM_WEB, false)
        updateFromWhiteList = args.getBoolean(FunnelConstants.UPDATE_FROM_WHITE_LIST, false)
    }
}