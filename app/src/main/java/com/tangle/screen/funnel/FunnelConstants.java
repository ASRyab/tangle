package com.tangle.screen.funnel;

public class FunnelConstants {

    public static final String IS_FROM_WEB = "is_web";
    public static final String GENDER = "gender";
    public static final String LOOKING_FOR = "looking_for";
    public static final String CAREER = "career";
    public static final String INDUSTRY = "industries";
    public static final String INTERESTS = "interests";
    public static final String AGE = "age";
    public static final String GEO_CITY = "geo_city";
    public static final String GEO_REGION = "geo_region";
    public static final String UPDATE_FROM_WHITE_LIST = "is_from_white_list";

}
