package com.tangle.screen.funnel.interests;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.rx_tasks.dictionary.GetInterestsModel;
import com.tangle.api.rx_tasks.profile.update.ProfileUpdateModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.pojos.DictionaryItem;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.funnel.ProfileDataMapper;
import com.tangle.utils.RxUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class InterestsPresenter extends LifecyclePresenter<InterestsView> {
    private ProfileData profileData;
    private Observable<List<DictionaryItem>> observable = new GetInterestsModel().getTask().cache();
    private List<DictionaryItem> selectedInterests = new ArrayList<>();
    private boolean isFromFunnel;

    public void setIsFromFunnel(boolean isFromFunnel) {
        this.isFromFunnel = isFromFunnel;
    }

    @Override
    public void attachToView(InterestsView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (!isFromFunnel) {
            monitor(ApplicationLoader.getApplicationInstance().getUserManager().currentUser()
                    .subscribe(this::setProfileData, error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
        } else {
            loadInterests(Collections.emptySet());
        }
    }

    private void setProfileData(ProfileData profileData) {
        this.profileData = profileData;
        loadInterests(profileData.interests.keySet());
    }

    private void loadInterests(Set<String> myInterestIds) {
        Disposable disposable = observable.subscribe(
                interests -> {
                    getView().setAllInterests(interests);
                    if (!isFromFunnel) {
                        for (DictionaryItem interest : interests) {
                            if (myInterestIds.contains(Integer.toString(interest.id))) {
                                setInterest(interest);
                            }
                        }
                    }
                    refreshSelectedInterests();
                },
                error -> {
                    if (isFromFunnel) {
                        getView().setError(ErrorResponse.getServerMessage(error));
                    } else {
                        getView().showAPIError(ErrorResponse.getServerMessage(error));
                    }
                });
        monitor(disposable, State.DESTROY);
    }

    public void setInterest(DictionaryItem interest) {
        selectedInterests.add(interest);
    }

    private void refreshSelectedInterests() {
        if (selectedInterests != null & !selectedInterests.isEmpty()) {
            getView().setSelectedInterests(selectedInterests);
        }
    }

    public void onNextClicked(List<DictionaryItem> selectedInterests) {
        if (selectedInterests == null || selectedInterests.isEmpty()) {
            getView().setError(getResources().getString(R.string.funnel_error_choose_interest));
        } else {
            getView().setError(null);
            this.selectedInterests = selectedInterests;
            getView().goNextScreen(selectedInterests);
        }
    }

    public void onSaveClicked(List<DictionaryItem> selectedInterests) {
        if (selectedInterests == null || selectedInterests.isEmpty()) {
            getView().showAPIError(getResources().getString(R.string.funnel_error_choose_interest));
        } else {
            Disposable disposable = new ProfileUpdateModel(new ProfileDataMapper().mapValue(profileData, selectedInterests)).saveInterests()
                    .compose(RxUtils.withLoading(getView()))
                    .subscribe(profileData -> {
                        getView().showAPIError(getResources().getString(R.string.update_successful));
                        getView().close();
                    }, error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
            monitor(disposable, State.DESTROY_VIEW);
        }
    }
}