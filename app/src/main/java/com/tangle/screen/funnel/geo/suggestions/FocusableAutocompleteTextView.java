package com.tangle.screen.funnel.geo.suggestions;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.KeyEvent;

import com.tangle.utils.PlatformUtils;

public class FocusableAutocompleteTextView extends android.support.v7.widget.AppCompatAutoCompleteTextView {

    private Runnable keyboardHideAction;

    public FocusableAutocompleteTextView(Context context) {
        super(context);
    }

    public FocusableAutocompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FocusableAutocompleteTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean enoughToFilter() {
        return true;
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        if (isFocused()) {
            showDropDown();
        } else {
            PlatformUtils.Keyboard.hideKeyboard(this);
        }
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (keyboardHideAction != null) {
                keyboardHideAction.run();
            }
            return true;
        }
        return super.dispatchKeyEvent(event);
    }

    public void setOnBackPressedAction(Runnable keyboardHideAction) {
        this.keyboardHideAction = keyboardHideAction;
    }

}