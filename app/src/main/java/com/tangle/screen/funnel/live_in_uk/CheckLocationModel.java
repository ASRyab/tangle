package com.tangle.screen.funnel.live_in_uk;

import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.profile.GetTargetCountry;
import com.tangle.model.TargetCountry;
import com.tangle.utils.PreferenceManager;

import io.reactivex.Observable;

public class CheckLocationModel implements CheckLocationContract.Model {

    @Override
    public Observable<Integer> getIsTargetLocation() {
        return getFromRepository()
                .flatMap(result -> {
                    if (result < 0) {
                        return getFromServer();
                    } else {
                        return Observable.just(result);
                    }
                });

    }

    private Observable<Integer> getFromRepository() {
        PreferenceManager preferenceManager = ApplicationLoader.getApplicationInstance().getPreferenceManager();
        int targetCountry = preferenceManager.getTargetCountry();
        return Observable.just(targetCountry);
    }

    private Observable<Integer> getFromServer() {
        return new GetTargetCountry().getDataTask()
                .map(TargetCountry::getTargetCountry)
                .doOnNext(this::saveTargetCountry);

    }

    private void saveTargetCountry(Integer targetCountry) {
        PreferenceManager preferenceManager = ApplicationLoader.getApplicationInstance().getPreferenceManager();
        preferenceManager.saveTargetCountry(targetCountry);
    }

}
