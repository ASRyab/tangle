package com.tangle.screen.funnel.terms;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.MVPFragment;
import com.tangle.base.ui.views.ShapeButton;
import com.tangle.screen.web_browser.WebBrowserFragment;

public class TermsFragment extends MVPFragment implements TermsView {
    private static final String PROFILE_KEY = "profile_key";
    private TermsPresenter presenter = new TermsPresenter();

    private CheckBox chbPrivacy, chbTerms;
    private ShapeButton btnContinueReg;
    private TextView txtTerms, txtPrivacy;

    public static TermsFragment newInstance() {
        return new TermsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_terms, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        chbTerms = view.findViewById(R.id.chb_terms);
        txtTerms = view.findViewById(R.id.txt_terms);
        txtTerms.setText(getSpanText(txtTerms, getString(R.string.terms_chb_terms_text), getString(R.string.terms_chb_terms_span_text), __ -> presenter.onTermsOfUseClick()));
        chbTerms.setOnCheckedChangeListener((buttonView, isChecked) -> presenter.termsChecked(isChecked));
        chbPrivacy = view.findViewById(R.id.chb_privacy);
        txtPrivacy = view.findViewById(R.id.txt_privacy);
        txtPrivacy.setText(getSpanText(txtPrivacy, getString(R.string.terms_chb_privacy_text), getString(R.string.terms_chb_privacy_span_text), __ -> presenter.onPrivacyPolicyClick()));
        chbPrivacy.setOnCheckedChangeListener((buttonView, isChecked) -> presenter.privacyChecked(isChecked));
        btnContinueReg = view.findViewById(R.id.btn_continued_reg);
        btnContinueReg.setOnClickListener(v -> presenter.onContinuedClicked());
    }

    private SpannableString getSpanText(TextView txtField, String body, String spanText, View.OnClickListener clickListener) {
        txtField.setMovementMethod(LinkMovementMethod.getInstance());
        body = String.format(body, spanText);
        int start = body.indexOf(spanText);
        int finish = start + spanText.length();
        SpannableString spannableBody = new SpannableString(body);
        spannableBody.setSpan(new ClickableSpan() {
            @Override
            public void onClick(final View view) {
                clickListener.onClick(view);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setColor(ContextCompat.getColor(chbTerms.getContext(), R.color.colorPrimary));
                ds.setUnderlineText(true);
            }
        }, start, finish, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannableBody;
    }

    @Override
    public void showTermsWarning(boolean isWarning) {
        chbTerms.setButtonDrawable(isWarning ? R.drawable.ic_circle_red : R.drawable.terms_checkbox_selector);
        chbTerms.setTextColor(isWarning ? ContextCompat.getColor(chbTerms.getContext(), R.color.colorAccent) : Color.WHITE);
    }

    @Override
    public void showPrivacyWarning(boolean isWarning) {
        chbPrivacy.setButtonDrawable(isWarning ? R.drawable.ic_circle_red : R.drawable.terms_checkbox_selector);
        chbPrivacy.setTextColor(isWarning ? ContextCompat.getColor(chbTerms.getContext(), R.color.colorAccent) : Color.WHITE);
    }

    @Override
    public void processUrl(String url) {
        switchFragment(WebBrowserFragment.newInstance(url), true);
    }

    @Override
    public void goBack() {
        getActivity().onBackPressed();
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}