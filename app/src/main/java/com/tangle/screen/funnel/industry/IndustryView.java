package com.tangle.screen.funnel.industry;

import com.tangle.base.ui.interfaces.AutoCloseable;
import com.tangle.base.ui.interfaces.ErrorView;
import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.pojos.DictionaryItem;

import java.util.List;

public interface IndustryView extends ErrorView, LoadingView, AutoCloseable {
    void setAvailableIndustries(List<DictionaryItem> industries);

    void setChosenIndustry(DictionaryItem industry);

    void goNextScreen(DictionaryItem industry);
}