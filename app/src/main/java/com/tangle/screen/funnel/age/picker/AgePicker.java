package com.tangle.screen.funnel.age.picker;

import android.animation.ArgbEvaluator;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Space;
import android.widget.TextView;

import com.tangle.R;

public class AgePicker extends RecyclerView {

    public static final int MAX_AGE = 99;
    public static final int MIN_AGE = 18;

    private AgeAdapter ageAdapter;
    private int minAge, maxAge, age;
    private int[] ages;

    public AgePicker(Context context) {
        super(context);
        init();
        initAdapter();
    }

    public AgePicker(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
        applyAttrs(attrs);
        initAdapter();
    }

    public AgePicker(Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        applyAttrs(attrs);
        initAdapter();
    }

    public int getAge() {
        return age;
    }

    private int[] createAgesArray() {
        int[] ages = new int[maxAge - minAge + 1];
        for (int i = minAge; i <= maxAge; i++) {
            ages[i - minAge] = i;
        }
        return ages;
    }

    private void setLayoutManager() {
        LayoutManager layoutManager = new CenterZoomLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false) {

            ArgbEvaluator argbEvaluator = new ArgbEvaluator();
            int normalColor = ContextCompat.getColor(getContext(), R.color.colorTextSecondarySemiTransparent);
            int highlightColor = ContextCompat.getColor(getContext(), R.color.colorTextPrimary);

            @Override
            protected void transformChild(View child, float factor) {
                super.transformChild(child, factor);
                ViewHolder viewHolder = getChildViewHolder(child);
                if (viewHolder.getClass().getSimpleName().equals(AgeViewHolder.class.getSimpleName())) {
                    AgeViewHolder ageViewHolder = (AgeViewHolder) viewHolder;
                    if (factor == 1) {
                        age = ageViewHolder.age;
                    }
                    int color = (int) argbEvaluator.evaluate(factor, normalColor, highlightColor);
                    ageViewHolder.ageTextView.setTextColor(color);
                }
            }
        };
        setLayoutManager(layoutManager);
    }

    private void initAdapter() {
        setHasFixedSize(true);
        SnapHelper snapHelper = new LinearSnapHelper();
        snapHelper.attachToRecyclerView(this);
        setLayoutManager();
        ages = createAgesArray();
        ageAdapter = new AgeAdapter(ages);
        post(() -> {
            setAdapter(ageAdapter);
            smoothScrollToAge(age);
        });
    }

    public void smoothScrollToAge(int age) {
        if (age >= minAge && age <= maxAge) {
            this.age = age;
            scrollToAgeInternally(true);
        }
    }

    private void scrollToAgeInternally(boolean smooth) {
        int position = (age - minAge) + 3;
        post(() -> {
            if (smooth) {
                smoothScrollToPosition(position);
            } else {
                scrollToPosition(position);
            }
        });
    }

    private void init() {
        minAge = MIN_AGE;
        maxAge = MAX_AGE;
        age = 32;
    }

    private void applyAttrs(AttributeSet attrs) {
        if (attrs != null) {
            readAttrs(attrs);
        }
    }

    protected void readAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.AgePicker);
        minAge = typedArray.getInt(R.styleable.AgePicker_ag_minAge, minAge);
        maxAge = typedArray.getInt(R.styleable.AgePicker_ag_maxAge, maxAge);
        age = typedArray.getInt(R.styleable.AgePicker_ag_age, age);
        typedArray.recycle();
    }

    private class AgeAdapter extends RecyclerView.Adapter {

        public static final int ITEM = 0;
        public static final int SPACE_START = 1;
        public static final int SPACE_END = 2;


        private int[] ages;

        public AgeAdapter(int[] ages) {
            this.ages = ages;
        }

        @Override
        public int getItemViewType(int position) {
            if (position == 0) {
                return SPACE_START;
            }
            if (position == ages.length + 1) {
                return SPACE_END;
            }
            return ITEM;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            switch (viewType) {
                case ITEM:
                    return new AgeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_funnel_age, parent, false));
                case SPACE_START:
                    View startSpace = new Space(getContext());
                    startSpace.setLayoutParams(new ViewGroup.LayoutParams(getWidth() / 2, 0));
                    return new ViewHolder(startSpace) {
                    };
                case SPACE_END:
                    View lastSpace = new Space(getContext());
                    lastSpace.setLayoutParams(new ViewGroup.LayoutParams(getWidth() / 2, 0));
                    return new ViewHolder(lastSpace) {
                    };
            }
            return null;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            if (getItemViewType(position) == ITEM) {
                ((AgeViewHolder) holder).bind(ages[position - 1]);
            }
        }

        @Override
        public int getItemCount() {
            return ages.length + 2;
        }
    }

    private static class AgeViewHolder extends ViewHolder {

        public int age;
        TextView ageTextView;

        public AgeViewHolder(View itemView) {
            super(itemView);
            ageTextView = itemView.findViewById(R.id.txt_age);
        }

        public void bind(int age) {
            this.age = age;
            ageTextView.setText(Integer.toString(age));
        }
    }

}
