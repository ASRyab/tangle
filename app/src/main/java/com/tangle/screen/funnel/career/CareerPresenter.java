package com.tangle.screen.funnel.career;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.rx_tasks.dictionary.GetCareersModel;
import com.tangle.api.rx_tasks.profile.update.ProfileUpdateModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.pojos.DictionaryItem;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.funnel.ProfileDataMapper;
import com.tangle.utils.RxUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class CareerPresenter extends LifecyclePresenter<CareerView> {
    private ProfileData profileData;
    private DictionaryItem career;
    private Observable<List<DictionaryItem>> observable = new GetCareersModel().getTask()
            .compose(DictionaryItem.composeNotGiven()).cache();
    private boolean isFromFunnel;

    public void setIsFromFunnel(boolean isFromFunnel) {
        this.isFromFunnel = isFromFunnel;
    }

    @Override
    public void attachToView(CareerView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (!isFromFunnel) {
            monitor(ApplicationLoader.getApplicationInstance().getUserManager().currentUser()
                    .subscribe(this::setProfileData, error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
        } else {
            loadCarier();
        }
    }

    private void setProfileData(ProfileData profileData) {
        this.profileData = profileData;
        loadCarier();
    }

    private void loadCarier() {
        Disposable disposable = observable.subscribe(
                careers -> {
                    getView().setAvailableCareers(careers);
                    if (!isFromFunnel) {
                        for (DictionaryItem carier : careers) {
                            if (Integer.toString(carier.id).equals(profileData.getCareerLevel())) {
                                setCarier(carier);
                            }
                        }
                    }
                    refreshSelected();
                }, error -> {
                    if (isFromFunnel) {
                        getView().setError(ErrorResponse.getServerMessage(error));
                    } else {
                        getView().showAPIError(ErrorResponse.getServerMessage(error));
                    }
                });
        monitor(disposable, State.DESTROY);
    }

    public void setCarier(DictionaryItem career) {
        this.career = career;
    }

    private void refreshSelected() {
        if (career != null) {
            getView().setSelectedCareer(career);
        }
    }

    public void onNextClicked(DictionaryItem career) {
        if (career == null) {
            getView().setError(getResources().getString(R.string.funnel_error_choose_career));
        } else {
            this.career = career;
            getView().setError(null);
            getView().goNextScreen(career);
        }
    }

    public void onSaveClicked(DictionaryItem career) {
        if (career == null) {
            getView().showAPIError(getResources().getString(R.string.funnel_error_choose_career));
            return;
        }
        if (this.career != null && career.id == this.career.id) {
            getView().showAPIError(null);
        } else {
            ProfileData profile = new ProfileDataMapper().mapValue(profileData);
            profile.setCareerLevel(String.valueOf(career.id));
            Disposable disposable = new ProfileUpdateModel(profile).saveCareer()
                    .compose(RxUtils.withLoading(getView()))
                    .subscribe(profileData -> {
                        setCarier(career);
                        getView().showAPIError(getResources().getString(R.string.update_successful));
                        getView().close();
                    }, error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
            monitor(disposable, State.DESTROY_VIEW);
        }
    }
}