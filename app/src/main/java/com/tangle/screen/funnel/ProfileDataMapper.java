package com.tangle.screen.funnel;

import android.os.Bundle;

import com.tangle.model.pojos.DictionaryItem;
import com.tangle.model.pojos.Gender;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.model.profile_data.ProfileGeo;
import com.tangle.model.profile_data.SexualOrientation;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class ProfileDataMapper {
    static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    public static final String LONDON = "London";
    public static final String GREAT_BRITAIN = "GBR";

    @SuppressWarnings("unchecked")
    public ProfileData mapValues(Bundle args) {
        Gender gender = (Gender) args.getSerializable(FunnelConstants.GENDER);
        Gender lookingFor = (Gender) args.getSerializable(FunnelConstants.LOOKING_FOR);
        DictionaryItem industry = (DictionaryItem) args.getSerializable(FunnelConstants.INDUSTRY);
        DictionaryItem career = (DictionaryItem) args.getSerializable(FunnelConstants.CAREER);
        String city = args.getString(FunnelConstants.GEO_CITY);
        List<DictionaryItem> interests = (ArrayList<DictionaryItem>) args.getSerializable(FunnelConstants.INTERESTS);
        Date birthday = getDate(args.getInt(FunnelConstants.AGE));
        ProfileData profileData = new ProfileData();
        profileData.gender = gender;
        profileData.lookingForGender = Integer.toString(lookingFor.getIndex());
        profileData.sexual_orientation = (gender == lookingFor)? SexualOrientation.HOMO
                : SexualOrientation.HETERO;
        profileData.birthday = DATE_FORMAT.format(birthday);
        profileData.setCareerLevel(Integer.toString(career.id));
        profileData.setIndustry(Integer.toString(industry.id));
        profileData.geo = new ProfileGeo();
        profileData.geo.city = city;
        profileData.geo.country_code = GREAT_BRITAIN;
        if (LONDON.equalsIgnoreCase(city)) {
            String region = args.getString(FunnelConstants.GEO_REGION);
            profileData.geo.region = region;
        }
        profileData.interests = new HashMap<>();
        for (DictionaryItem it : interests) {
            profileData.interests.put(Integer.toString(it.id), true);
        }
        return profileData;
    }

    public ProfileData mapValue(ProfileData profile) {
        return mapValues(profile, null, null);
    }

    public ProfileData mapValue(ProfileData profile, Integer age) {
        return mapValues(profile, age, null);
    }

    public ProfileData mapValue(ProfileData profile, List<DictionaryItem> selectedInterests) {
        return mapValues(profile, null, selectedInterests);
    }

    public ProfileData mapValues(ProfileData profile, Integer age, List<DictionaryItem> selectedInterests) {
        Date birthday = getDate(age == null ? profile.age : age);
        profile.birthday = DATE_FORMAT.format(birthday);
        profile.age = (age == null ? profile.age : age);
        if (LONDON.equalsIgnoreCase(profile.geo.city)) {
            profile.geo.region = profile.geo.region == null || profile.geo.region.trim().isEmpty() ? "Other" : profile.geo.region;
        }
        if (selectedInterests != null) {
            profile.interests = new HashMap<>();
            for (DictionaryItem interest : selectedInterests) {
                profile.interests.put(Integer.toString(interest.id), true);
            }
        }
        return profile;
    }

    private Date getDate(int age) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -age);
        return calendar.getTime();
    }
}