package com.tangle.screen.funnel.career;

import com.tangle.base.ui.interfaces.AutoCloseable;
import com.tangle.base.ui.interfaces.ErrorView;
import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.pojos.DictionaryItem;

import java.util.List;

public interface CareerView extends ErrorView, LoadingView, AutoCloseable {
    void setAvailableCareers(List<DictionaryItem> careers);

    void setSelectedCareer(DictionaryItem career);

    void goNextScreen(DictionaryItem career);
}