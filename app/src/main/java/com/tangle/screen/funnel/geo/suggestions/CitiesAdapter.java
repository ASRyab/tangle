package com.tangle.screen.funnel.geo.suggestions;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.ListView;
import android.widget.TextView;

import com.tangle.R;

import java.util.ArrayList;
import java.util.List;

public class CitiesAdapter extends ArrayAdapter<String> {

    private List<String> tempItems, suggestions;

    public CitiesAdapter(Context context, List<String> items) {
        super(context, R.layout.item_funnel_geo, R.id.tvTitle, items);
        this.tempItems = new ArrayList<>(items);
        this.suggestions = new ArrayList<>();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ListView list = (ListView) parent;
        list.setVerticalScrollBarEnabled(false);
        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_funnel_geo, parent, false);
            convertView.setTag(convertView.findViewById(R.id.tvTitle));
        }
        TextView titleTextView = (TextView) convertView.getTag();
        titleTextView.setText(getItem(position));
        return convertView;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Filter getFilter() {
        return citiesFilter;
    }

    private Filter citiesFilter = new Filter() {
        @Override
        public CharSequence convertResultToString(Object resultValue) {
            return ((String) resultValue);
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            suggestions.clear();
            if (constraint != null) {
                for (String city : tempItems) {
                    if (city.toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(city);
                    }
                }
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
            }
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            List<String> filterList = (ArrayList<String>) results.values;
            if (results.count > 0) {
                clear();
                addAll(filterList);
                notifyDataSetChanged();
            }
        }
    };
}