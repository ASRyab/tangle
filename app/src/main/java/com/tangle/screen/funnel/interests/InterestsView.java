package com.tangle.screen.funnel.interests;

import com.tangle.base.ui.interfaces.AutoCloseable;
import com.tangle.base.ui.interfaces.ErrorView;
import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.pojos.DictionaryItem;

import java.util.List;

public interface InterestsView extends ErrorView, LoadingView, AutoCloseable {
    void setAllInterests(List<DictionaryItem> interests);

    void setSelectedInterests(List<DictionaryItem> interests);

    void goNextScreen(List<DictionaryItem> interests);
}