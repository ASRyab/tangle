package com.tangle.screen.funnel.terms;

import com.tangle.base.ui.interfaces.LoadingView;

public interface TermsView extends LoadingView {

    void showTermsWarning(boolean isWarning);

    void showPrivacyWarning(boolean isWarning);

    void processUrl(String url);

    void goBack();
}