package com.tangle.screen.funnel.looking_for;


import com.tangle.base.ui.interfaces.ErrorView;
import com.tangle.model.pojos.Gender;

public interface LookingForView extends ErrorView {
    void goNextScreen(Gender lookingFor);

    void setLookingFor(Gender lookingFor);

    void showAPIError(String error);
}
