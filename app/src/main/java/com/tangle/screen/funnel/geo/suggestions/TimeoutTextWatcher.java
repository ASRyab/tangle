package com.tangle.screen.funnel.geo.suggestions;

import android.os.Handler;
import android.os.Looper;
import android.support.annotation.IntRange;
import android.text.Editable;
import android.text.TextWatcher;

public abstract class TimeoutTextWatcher implements TextWatcher {

    public static final int DEFAULT_TIMEOUT_MILLIS = 300;
    private Handler handler;
    private int timeout = DEFAULT_TIMEOUT_MILLIS;

    public TimeoutTextWatcher() {
        this.handler = new Handler(Looper.getMainLooper());
    }

    public TimeoutTextWatcher(@IntRange(from = 1) int timeout) {
        this.handler = new Handler(Looper.getMainLooper());
        this.timeout = timeout;
    }

    @Override
    public final void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public final void onTextChanged(CharSequence s, int start, int before, int count) {
        handler.removeCallbacksAndMessages(null);
        handler.postDelayed(() -> onTextChanged(s), timeout);
    }

    @Override
    public final void afterTextChanged(Editable s) {
    }

    public abstract void onTextChanged(CharSequence s);

}
