package com.tangle.screen.funnel.geo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.screen.funnel.FunnelConstants;
import com.tangle.screen.funnel.FunnelFragment;
import com.tangle.screen.funnel.geo.suggestions.CitiesAdapter;
import com.tangle.screen.funnel.geo.suggestions.FocusableAutocompleteTextView;
import com.tangle.screen.funnel.industry.IndustryFragment;
import com.tangle.utils.PlatformUtils;

import java.util.List;

public class GeoFragment extends FunnelFragment implements GeoView {

    private GeoPresenter presenter = new GeoPresenter();
    private CitiesAdapter citiesAdapter;
    private CitiesAdapter regionsAdapter;

    private ViewGroup locationContainer;
    private TextView cityTextView;
    private TextView regionTextView;
    private FocusableAutocompleteTextView geoInput;
    private Button actionButton;

    public static GeoFragment newInstance(Bundle args, boolean isFromFunnel) {
        if (args == null) {
            args = new Bundle();
        }
        args.putBoolean(IS_FROM_FUNNEL_KEY, isFromFunnel);
        GeoFragment fragment = new GeoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setIsFromFunnel(isFromFunnel);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(isFromFunnel ? R.layout.fragment_funnel_geo : R.layout.fragment_change_location, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        if (!isFromFunnel) {
            initToolbar(getString(R.string.i_am_from), true);
        }
        geoInput = view.findViewById(R.id.input_geo);
        cityTextView = view.findViewById(R.id.txt_city);
        regionTextView = view.findViewById(R.id.txt_region);
        locationContainer = view.findViewById(R.id.container_location);
        actionButton = isFromFunnel ? view.findViewById(R.id.btn_next) : view.findViewById(R.id.btn_save);
        cityTextView.setOnClickListener(v -> presenter.onPickCityClicked());
        regionTextView.setOnClickListener(v -> presenter.onPickRegionClicked());
        actionButton.setOnClickListener(v -> {
            if (isFromFunnel) {
                presenter.onNextClicked();
            } else {
                presenter.onSaveClicked();
            }
        });
        geoInput.setOnFocusChangeListener((v, isFocused) -> {
            setError(null);
            if (!isFocused && geoInput.getVisibility() == View.VISIBLE) {
                if (geoInput.getAdapter() == citiesAdapter && citiesAdapter.getCount() == 1) {
                    presenter.onCitySelected(citiesAdapter.getItem(0));
                } else if (geoInput.getAdapter() == regionsAdapter && regionsAdapter.getCount() == 1) {
                    presenter.onRegionSelected(regionsAdapter.getItem(0));
                }
            }
        });
        initGeoInput();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (geoInput.getVisibility() == View.VISIBLE && citiesAdapter != null) {
            requestInputFocus();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        clearInputFocus();
    }

    private void initGeoInput() {
        geoInput.setDropDownBackgroundResource(android.R.color.transparent);
        geoInput.setOnBackPressedAction(this::clearInputFocus);
    }

    @Override
    public void setGeo(List<String> cities, List<String> regions) {
        citiesAdapter = new CitiesAdapter(getContext(), cities);
        regionsAdapter = new CitiesAdapter(getContext(), regions);
        geoInput.setAdapter(citiesAdapter);
    }

    @Override
    public void setCity(String city) {
        locationContainer.setVisibility(View.VISIBLE);
        cityTextView.setText(city);
    }

    @Override
    public void setRegion(String region) {
        if (region == null) {
            regionTextView.setVisibility(View.GONE);
        } else {
            regionTextView.setVisibility(View.VISIBLE);
            regionTextView.setText(region);
        }
    }

    @Override
    public void setChooseCityState() {
        regionTextView.setVisibility(View.GONE);
        locationContainer.setVisibility(View.GONE);
        geoInput.setVisibility(View.VISIBLE);
        geoInput.setHint(R.string.funnel_from_hint_city);
        geoInput.setText("");
        geoInput.clearFocus();
        getView().requestFocus();
        geoInput.setAdapter(citiesAdapter);
        geoInput.setOnItemClickListener(cityClickListener);
//        actionButton.setVisibility(View.INVISIBLE);
        requestInputFocus();
    }

    @Override
    public void setChooseRegionState() {
        regionTextView.setVisibility(View.GONE);
        geoInput.setVisibility(View.VISIBLE);
        geoInput.setHint(R.string.funnel_from_hint_district);
        geoInput.setText("");
        geoInput.clearFocus();
        getView().requestFocus();
        geoInput.setAdapter(regionsAdapter);
        geoInput.setOnItemClickListener(regionClickListener);
//        actionButton.setVisibility(View.INVISIBLE);
        requestInputFocus();
    }

    @Override
    public void setDataState() {
        setError(null);
        geoInput.setVisibility(View.GONE);
        PlatformUtils.Keyboard.hideKeyboard(geoInput);
        actionButton.setVisibility(View.VISIBLE);
    }

    private void clearInputFocus() {
        geoInput.clearFocus();
        getView().requestFocus();
        PlatformUtils.Keyboard.hideKeyboard(geoInput);
    }

    private void requestInputFocus() {
        geoInput.postDelayed(() -> {
            geoInput.requestFocus();
            geoInput.showDropDown();
            PlatformUtils.Keyboard.showKeyboard(geoInput);
        }, 50);
    }

    @Override
    public void goNextScreen(String city, String region) {
        getArguments().putString(FunnelConstants.GEO_CITY, city);
        getArguments().putSerializable(FunnelConstants.GEO_REGION, region);
        switchFragment(IndustryFragment.newInstance(getArguments(), true), true);
    }

    AdapterView.OnItemClickListener cityClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            presenter.onCitySelected(citiesAdapter.getItem(position));
        }
    };

    AdapterView.OnItemClickListener regionClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            presenter.onRegionSelected(regionsAdapter.getItem(position));
        }
    };

    @Override
    protected int getStep() {
        return 4;
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}