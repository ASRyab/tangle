package com.tangle.screen.funnel.looking_for;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.model.pojos.Gender;
import com.tangle.screen.funnel.FunnelConstants;
import com.tangle.screen.funnel.FunnelFragment;
import com.tangle.screen.funnel.age.AgeFragment;

public class LookingForFragment extends FunnelFragment implements LookingForView {
    private LookingForPresenter presenter = new LookingForPresenter();

    private Button menButton;
    private Button womenButton;
    private Button actionButton;

    public static LookingForFragment newInstance(Bundle args, boolean isFromFunnel) {
        if(args == null) {
            args = new Bundle();
        }
        args.putBoolean(IS_FROM_FUNNEL_KEY, isFromFunnel);
        LookingForFragment fragment = new LookingForFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setIsFromFunnel(isFromFunnel);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(isFromFunnel ? R.layout.fragment_funnel_looking_for : R.layout.fragment_change_looking_for, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        if(!isFromFunnel) {
            initToolbar(getString(R.string.looking_for_title), true);
        }
        menButton = view.findViewById(R.id.btn_men);
        womenButton = view.findViewById(R.id.btn_women);
        actionButton = isFromFunnel ? view.findViewById(R.id.btn_next) : view.findViewById(R.id.btn_save);
        actionButton.setOnClickListener(v -> {
            if(isFromFunnel) {
                presenter.onNextClicked();
            } else {
                presenter.onSaveClicked();
            }
        });
        initSwitches();
    }

    private void initSwitches() {
        View.OnClickListener lookingForButtonClickListener = v -> {
            switch (v.getId()) {
                case R.id.btn_men:
                    presenter.onSwitchClicked(Gender.MALE);
                    break;
                case R.id.btn_women:
                    presenter.onSwitchClicked(Gender.FEMALE);
                    break;
            }
        };
        menButton.setOnClickListener(lookingForButtonClickListener);
        womenButton.setOnClickListener(lookingForButtonClickListener);
    }

    @Override
    public void setLookingFor(Gender lookingFor) {
        switch (lookingFor) {
            case MALE:
                menButton.setSelected(true);
                womenButton.setSelected(false);
                break;
            case FEMALE:
                menButton.setSelected(false);
                womenButton.setSelected(true);
                break;
            default:
                menButton.setSelected(false);
                womenButton.setSelected(false);
                break;
        }
    }

    @Override
    public void goNextScreen(Gender lookingFor) {
        getArguments().putSerializable(FunnelConstants.LOOKING_FOR, lookingFor);
        switchFragment(AgeFragment.newInstance(getArguments(), true), true);
    }

    @Override
    protected int getStep() {
        return 2;
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}