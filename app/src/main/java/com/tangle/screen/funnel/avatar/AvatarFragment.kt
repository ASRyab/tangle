package com.tangle.screen.funnel.avatar

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.bumptech.glide.request.RequestOptions
import com.tangle.ApplicationLoader
import com.tangle.R
import com.tangle.base.mvp.LifecyclePresenter
import com.tangle.base.ui.BaseActivity
import com.tangle.base.ui.dialogs.StyledDialog
import com.tangle.managers.DialogManager
import com.tangle.screen.auth.service_not_avaliable.ServiceNotAvailableFragment
import com.tangle.screen.funnel.FunnelFragment
import com.tangle.screen.main.MainActivity
import com.tangle.utils.glide.GlideApp
import com.tangle.utils.own_user_photo.OwnPhotoPresenter
import com.tangle.utils.own_user_photo.OwnPhotoView
import kotlinx.android.synthetic.main.fragment_funnel_avatar.*

import java.io.File

class AvatarFragment : FunnelFragment(), AvatarView, OwnPhotoView {
    private val presenter = AvatarPresenter()
    private val ownPhotoPresenter = OwnPhotoPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { presenter.setArguments(it) }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_funnel_avatar, container, false)
    }

    override fun onPostViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onPostViewCreated(view, savedInstanceState)
        tvMaybeLater.setOnClickListener { ownPhotoPresenter.onNextClickedHandler() }
        tvChoosePhoto.setOnClickListener { ownPhotoPresenter.onGalleryPickClicked(this) }
        tvMakePhoto.setOnClickListener { ownPhotoPresenter.onTakePictureClicked(this) }
        ivAvatar.setOnClickListener { ownPhotoPresenter.onAddPhotoClicked() }
        btnNext.setOnClickListener { ownPhotoPresenter.onNextClickedHandler() }
        view.findViewById<View>(R.id.txt_posting_rules).setOnClickListener { presenter.onRulesClick() }
    }

    override fun uploadAvailablePhoto(photoFile: File?) {
        arguments?.let {
            presenter.uploadAvailablePhoto(it, photoFile)
        }
    }

    override fun showUploadDialog(presenter: OwnPhotoPresenter) {
        val manager = DialogManager.getInstance(this)
        manager.showPhotoUploadDialog(presenter)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        ownPhotoPresenter.onActivityResult(requestCode, resultCode, data)
    }

    override fun setCurrentImage(photoFile: File?) {
        if (photoFile != null) {
            tvMaybeLater.visibility = View.GONE
            updatePhotoViewWithPhoto()
            GlideApp.with(this)
                    .load(photoFile)
                    .apply(RequestOptions.circleCropTransform())
                    .into(ivAvatar)
            btnNext.visibility = View.VISIBLE
        } else {
            updatePhotoViewWithoutPhoto()
        }
    }

    override fun setProfilePhoto(url: String) {
        updatePhotoViewWithPhoto()
        GlideApp.with(this)
                .load(url)
                .apply(RequestOptions.circleCropTransform())
                .into(ivAvatar)
    }

    override fun goToAppNotAvailableScreen() {
        switchFragment(ServiceNotAvailableFragment(), false)
    }

    override fun showWaitApprovePhotoDialog(listener: StyledDialog.DialogButtonClickListener) {
        DialogManager.showWaitPhotoApprove(activity?.let { activity as BaseActivity }, listener)
    }

    private fun updatePhotoViewWithoutPhoto() {
        tvChoosePhoto.visibility = View.VISIBLE
        tvMakePhoto.setText(R.string.funnel_take_photo)
        tvMakePhoto.setOnClickListener { ownPhotoPresenter.onTakePictureClicked(this) }
        ivAvatar.setImageResource(R.drawable.screen_avatar_ic_upload_photo)
    }

    private fun updatePhotoViewWithPhoto() {
        tvChoosePhoto.visibility = View.INVISIBLE
        tvMakePhoto.setText(R.string.funnel_replace_photo)
        tvMakePhoto.setOnClickListener { ownPhotoPresenter.onAddPhotoClicked() }
    }

    override fun goToMainScreen() {
        val intent = Intent(activity, MainActivity::class.java)
        intent.putExtra(MainActivity.AFTER_REG, true)
        startActivity(intent)
        activity?.finish()
    }


    override fun getStep(): Int {
        return 8
    }

    override fun getPresenter(): LifecyclePresenter<*> {
        return presenter
    }

    override fun showRules() {
        ApplicationLoader.getApplicationInstance().navigationManager.showPhotoRulesFragment()
    }
}