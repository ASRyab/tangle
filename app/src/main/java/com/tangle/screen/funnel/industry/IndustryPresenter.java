package com.tangle.screen.funnel.industry;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.rx_tasks.dictionary.GetIndustriesModel;
import com.tangle.api.rx_tasks.profile.update.ProfileUpdateModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.pojos.DictionaryItem;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.funnel.ProfileDataMapper;
import com.tangle.utils.RxUtils;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class IndustryPresenter extends LifecyclePresenter<IndustryView> {
    private ProfileData profileData;
    private DictionaryItem industry;
    private Observable<List<DictionaryItem>> observable = new GetIndustriesModel().getTask()
            .compose(DictionaryItem.composeNotGiven()).cache();
    private boolean isFromFunnel;

    public void setIsFromFunnel(boolean isFromFunnel) {
        this.isFromFunnel = isFromFunnel;
    }

    @Override
    public void attachToView(IndustryView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (!isFromFunnel) {
            monitor(ApplicationLoader.getApplicationInstance().getUserManager().currentUser()
                    .subscribe(this::setProfileData, error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
        } else {
            loadIndustries();
        }
    }

    private void setProfileData(ProfileData profileData) {
        this.profileData = profileData;
        loadIndustries();
    }

    private void loadIndustries() {
        Disposable disposable = observable.subscribe(
                industries -> {
                    getView().setAvailableIndustries(industries);
                    if (!isFromFunnel) {
                        for (DictionaryItem industry : industries) {
                            if (Integer.toString(industry.id).equals(profileData.getIndustry())) {
                                setIndustry(industry);
                            }
                        }
                    }
                    refreshChosenIndustry();
                }, error -> {
                    if (isFromFunnel) {
                        getView().setError(ErrorResponse.getServerMessage(error));
                    } else {
                        getView().showAPIError(ErrorResponse.getServerMessage(error));
                    }
                });
        monitor(disposable, State.DESTROY);
    }

    public void setIndustry(DictionaryItem industry) {
        this.industry = industry;
    }

    private void refreshChosenIndustry() {
        if (industry != null) {
            getView().setChosenIndustry(industry);
        }
    }

    public void onNextClicked(DictionaryItem industry) {
        if (industry == null) {
            getView().setError(getResources().getString(R.string.funnel_error_choose_industry));
        } else {
            this.industry = industry;
            getView().setError(null);
            getView().goNextScreen(industry);
        }
    }

    public void onSaveClicked(DictionaryItem industry) {
        if (industry == null) {
            getView().showAPIError(getResources().getString(R.string.funnel_error_choose_industry));
            return;
        }
        if (this.industry != null && industry.id == this.industry.id) {
            getView().showAPIError(null);
        } else {
            ProfileData profile = new ProfileDataMapper().mapValue(profileData);
            profile.setIndustry(String.valueOf(industry.id));
            Disposable disposable = new ProfileUpdateModel(profile).saveIndustry()
                    .compose(RxUtils.withLoading(getView()))
                    .subscribe(profileData -> {
                        setIndustry(industry);
                        getView().showAPIError(getResources().getString(R.string.update_successful));
                        getView().close();
                    }, error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
            monitor(disposable, State.DESTROY_VIEW);
        }
    }
}