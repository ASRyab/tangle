package com.tangle.screen.funnel.industry;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.model.pojos.DictionaryItem;

import java.util.ArrayList;
import java.util.List;

public class IndustryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int ITEM = 0;
    public static final int HEADER = 1;

    private String errorText;
    private List<DictionaryItem> industries = new ArrayList<>();
    private int selectedIndex;
    private boolean isFromFunnel;

    public IndustryAdapter(boolean isFromFunnel) {
        this.isFromFunnel = isFromFunnel;
        if(isFromFunnel) {
            this.selectedIndex = -1;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM:
                return new IndustryViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selected_industry, parent, false));
            case HEADER:
                return new HeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_industry_header, parent, false));
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if(!isFromFunnel) {
            return ITEM;
        }
        return position == 0 ? HEADER : ITEM;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);
        switch (type) {
            case ITEM:
                int index = isFromFunnel ? position - 1 : position;
                ((IndustryViewHolder) holder).bind(index);
                break;
            case HEADER:
                ((HeaderViewHolder) holder).bind();
                break;
        }
    }

    @Override
    public int getItemCount() {
        if(!isFromFunnel) {
            return industries.size();
        }
        return industries == null ? 1 : industries.size() + 1;
    }

    public void switchIndustries(List<DictionaryItem> industries) {
        this.industries = industries;
        notifyDataSetChanged();
    }

    public DictionaryItem getSelectedIndustry() {
        if(!isFromFunnel) {
            return industries.get(selectedIndex);
        }
        return selectedIndex > -1 ? industries.get(selectedIndex) : null;
    }

    public void setSelectedIndustry(DictionaryItem industry) {
        selectedIndex = industries.indexOf(industry);
        notifyDataSetChanged();
    }

    public void setError(String errorText) {
        this.errorText = errorText;
        notifyDataSetChanged();
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView errorTextView;

        public void bind() {
            if (TextUtils.isEmpty(errorText)) {
                errorTextView.setVisibility(View.INVISIBLE);
            } else {
                errorTextView.setVisibility(View.VISIBLE);
                errorTextView.setText(errorText);
            }
        }

        public HeaderViewHolder(View itemView) {
            super(itemView);
            errorTextView = itemView.findViewById(R.id.txt_error);
        }
    }

    public class IndustryViewHolder extends RecyclerView.ViewHolder {
        TextView industryTextView;
        ImageView checkImageView;
        DictionaryItem industry;
        int index;

        public void bind(int index) {
            this.index = index;
            industry = industries.get(index);
            industryTextView.setText(industry.name);
            boolean selected = (index == selectedIndex);
            industryTextView.setSelected(selected);
            checkImageView.setVisibility(selected ? View.VISIBLE : View.INVISIBLE);
        }

        public IndustryViewHolder(View itemView) {
            super(itemView);
            industryTextView = itemView.findViewById(R.id.txt_industry);
            checkImageView = itemView.findViewById(R.id.img_check);
            itemView.setOnClickListener(v -> {
                selectedIndex = index;
                notifyDataSetChanged();
            });
        }
    }
}