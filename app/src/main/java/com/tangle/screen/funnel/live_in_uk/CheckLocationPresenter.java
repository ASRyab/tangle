package com.tangle.screen.funnel.live_in_uk;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.utils.UserCheckUtils;

public class CheckLocationPresenter extends LifecyclePresenter<CheckLocationContract.View> {

    private CheckLocationContract.Model model;

    CheckLocationPresenter(CheckLocationContract.Model checkLocationModel) {
        this.model = checkLocationModel;
    }

    @Override
    public void attachToView(CheckLocationContract.View view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        monitor(model
                .getIsTargetLocation()
                .subscribe(result -> {
                    if (UserCheckUtils.isCountryInWhiteList(result)) {
                        getView().showContent();
                    } else {
                        getView().openGeoFragment();
                    }
                }, err -> {
                    getView().showContent();
                    getView().showAPIError(ErrorResponse.getServerMessage(err));
                }), State.DESTROY_VIEW);
    }
}