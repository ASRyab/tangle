package com.tangle.screen.funnel.career;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.model.pojos.DictionaryItem;

import java.util.ArrayList;
import java.util.List;

public class CareerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    public static final int ITEM = 0;
    public static final int HEADER = 1;

    private String errorText;
    private List<DictionaryItem> careers = new ArrayList<>();
    private int selectedIndex;
    private boolean isFromFunnel;

    public CareerAdapter(boolean isFromFunnel) {
        this.isFromFunnel = isFromFunnel;
        if(isFromFunnel) {
            this.selectedIndex = -1;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case ITEM:
                return new CareerViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_selected_career, parent, false));
            case HEADER:
                return new HeaderViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_career_header, parent, false));
        }
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if(!isFromFunnel) {
            return ITEM;
        }
        return position == 0 ? HEADER : ITEM;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);
        switch (type) {
            case ITEM:
                int index = isFromFunnel ? position - 1 : position;
                ((CareerViewHolder) holder).bind(index);
                break;
            case HEADER:
                ((HeaderViewHolder) holder).bind();
                break;
        }
    }

    @Override
    public int getItemCount() {
        if(!isFromFunnel) {
            return careers.size();
        }
        return careers == null ? 1 : careers.size() + 1;
    }

    public void switchCareers(List<DictionaryItem> careers) {
        this.careers = careers;
        notifyDataSetChanged();
    }

    public DictionaryItem getSelectedCareer() {
        if(!isFromFunnel) {
            return careers.get(selectedIndex);
        }
        return selectedIndex > -1 ? careers.get(selectedIndex) : null;
    }

    public void setSelectedCareer(DictionaryItem career) {
        selectedIndex = careers.indexOf(career);
        notifyDataSetChanged();
    }

    public void setError(String errorText) {
        this.errorText = errorText;
        notifyDataSetChanged();
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView errorTextView;

        public void bind() {
            if (TextUtils.isEmpty(errorText)) {
                errorTextView.setVisibility(View.INVISIBLE);
            } else {
                errorTextView.setVisibility(View.VISIBLE);
                errorTextView.setText(errorText);
            }
        }

        public HeaderViewHolder(View itemView) {
            super(itemView);
            errorTextView = itemView.findViewById(R.id.txt_error);
        }
    }

    public class CareerViewHolder extends RecyclerView.ViewHolder {
        TextView careerTextView;
        ImageView checkImageView;
        int index;

        public void bind(int index) {
            this.index = index;
            DictionaryItem career = careers.get(index);
            careerTextView.setText(career.name);
            boolean selected = (index == selectedIndex);
            careerTextView.setSelected(selected);
            checkImageView.setVisibility(selected ? View.VISIBLE : View.INVISIBLE);
        }

        public CareerViewHolder(View itemView) {
            super(itemView);
            careerTextView = itemView.findViewById(R.id.txt_career);
            checkImageView = itemView.findViewById(R.id.img_check);
            itemView.setOnClickListener(v -> {
                selectedIndex = index;
                notifyDataSetChanged();
            });
        }
    }
}