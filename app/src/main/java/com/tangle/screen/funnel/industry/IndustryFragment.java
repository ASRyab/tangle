package com.tangle.screen.funnel.industry;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.recyclerview.decorations.SpaceItemDecoration;
import com.tangle.model.pojos.DictionaryItem;
import com.tangle.screen.edit_own_profile.included_fragments.ChangeItemSpacesDecoration;
import com.tangle.screen.funnel.FunnelConstants;
import com.tangle.screen.funnel.FunnelFragment;
import com.tangle.screen.funnel.career.CareerFragment;
import com.tangle.utils.PlatformUtils;

import java.util.List;

public class IndustryFragment extends FunnelFragment implements IndustryView {
    private IndustryPresenter presenter = new IndustryPresenter();

    private IndustryAdapter adapter;
    private RecyclerView recyclerView;
    private Button actionButton;

    public static IndustryFragment newInstance(Bundle args, boolean isFromFunnel) {
        if(args == null) {
            args = new Bundle();
        }
        args.putBoolean(IS_FROM_FUNNEL_KEY, isFromFunnel);
        IndustryFragment fragment = new IndustryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setIsFromFunnel(isFromFunnel);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(isFromFunnel ? R.layout.fragment_funnel_industry : R.layout.fragment_change_my_industry, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);

        initUI(view);
    }

    private void initUI(View view) {
        if(!isFromFunnel) {
            initToolbar(getString(R.string.my_industry_is), true);
        }
        recyclerView = view.findViewById(android.R.id.list);
        actionButton = isFromFunnel ? view.findViewById(R.id.btn_next) : view.findViewById(R.id.btn_save);
        actionButton.setOnClickListener(v -> {
            if(isFromFunnel) {
                presenter.onNextClicked(adapter.getSelectedIndustry());
            } else {
                presenter.onSaveClicked(adapter.getSelectedIndustry());
            }
        });
        initList();
    }

    private void initList() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        if(isFromFunnel) {
            recyclerView.addItemDecoration(new SpaceItemDecoration(0,
                    (int) PlatformUtils.Dimensions.dipToPixels(getActivity(), 80),
                    LinearLayoutManager.VERTICAL));
        } else {
            recyclerView.addItemDecoration(new ChangeItemSpacesDecoration(0,
                    (int) PlatformUtils.Dimensions.dipToPixels(getActivity(), 120)));
        }
        adapter = new IndustryAdapter(isFromFunnel);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected int getStep() {
        return 5;
    }

    @Override
    public void setError(String error) {
        adapter.setError(error);
        recyclerView.smoothScrollToPosition(0);
    }

    @Override
    public void setAvailableIndustries(List<DictionaryItem> industries) {
        adapter.switchIndustries(industries);
    }

    @Override
    public void setChosenIndustry(DictionaryItem industry) {
        adapter.setSelectedIndustry(industry);
    }

    @Override
    public void goNextScreen(DictionaryItem industry) {
        getArguments().putSerializable(FunnelConstants.INDUSTRY, industry);
        switchFragment(CareerFragment.newInstance(getArguments(), true), true);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}