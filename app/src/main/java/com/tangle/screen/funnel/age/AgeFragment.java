package com.tangle.screen.funnel.age;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.screen.funnel.FunnelConstants;
import com.tangle.screen.funnel.FunnelFragment;
import com.tangle.screen.funnel.age.picker.AgePicker;
import com.tangle.screen.funnel.live_in_uk.CheckLocationFragment;

public class AgeFragment extends FunnelFragment implements AgeView {
    private AgePresenter presenter = new AgePresenter();

    private AgePicker agePicker;
    private Button actionButton;

    public static AgeFragment newInstance(Bundle args, boolean isFromFunnel) {
        if (args == null) {
            args = new Bundle();
        }
        args.putBoolean(IS_FROM_FUNNEL_KEY, isFromFunnel);
        AgeFragment fragment = new AgeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setIsFromFunnel(isFromFunnel);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(isFromFunnel ? R.layout.fragment_funnel_age : R.layout.fragment_change_age, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);

        initUI(view);
    }

    private void initUI(View view) {
        if (!isFromFunnel) {
            initToolbar(getString(R.string.funnel_age), true);
        }
        agePicker = view.findViewById(android.R.id.list);
        actionButton = isFromFunnel ? view.findViewById(R.id.btn_next) : view.findViewById(R.id.btn_save);
        initAgePicker();
        actionButton.setOnClickListener(v -> {
            if (isFromFunnel) {
                presenter.onNextClicked(agePicker.getAge());
            } else {
                presenter.onSaveClicked(agePicker.getAge());
            }
        });
    }

    private void initAgePicker() {
        RecyclerView.OnScrollListener controlDisableScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING ||
                        newState == RecyclerView.SCROLL_STATE_SETTLING) {
                    actionButton.setClickable(false);
                } else {
                    actionButton.setClickable(true);
                }
                super.onScrollStateChanged(recyclerView, newState);
            }
        };
        agePicker.addOnScrollListener(controlDisableScrollListener);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void openNextScreen(int age) {
        getArguments().putInt(FunnelConstants.AGE, age);
        switchFragment(CheckLocationFragment.newInstance(getArguments()), true);
    }

    @Override
    public void setAge(int age) {
        agePicker.post(() -> agePicker.smoothScrollToAge(age));
    }

    @Override
    protected int getStep() {
        return 3;
    }
}