package com.tangle.screen.welcome;

import com.tangle.base.ui.interfaces.AutoCloseable;
import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.profile_data.ProfileData;

public class WelcomeContract {
    public interface View extends LoadingView, AutoCloseable {
        void setUser(ProfileData user);
    }
}
