package com.tangle.screen.welcome;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.utils.RxUtils;

public class WelcomePresenter extends LifecyclePresenter<WelcomeContract.View> {
    public void onNextClicked() {
        getView().close();
    }

    @Override
    public void attachToView(WelcomeContract.View view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().currentUser()
                .subscribe(data -> getView().setUser(data), RxUtils.getEmptyErrorConsumer("attachToView")), State.DESTROY_VIEW);
    }
}
