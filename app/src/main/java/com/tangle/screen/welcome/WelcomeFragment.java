package com.tangle.screen.welcome;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.model.profile_data.ProfileData;

public class WelcomeFragment extends SecondaryScreenFragment implements WelcomeContract.View {

    private WelcomePresenter presenter = new WelcomePresenter();
    private TextView tvWelcome;

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initView(view);
    }

    private void initView(View view) {
        tvWelcome = view.findViewById(R.id.txt_welcome);
        view.findViewById(R.id.btn_ok).setOnClickListener(view1 -> presenter.onNextClicked());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_welcome, container, false);
    }

    @Override
    public void setUser(ProfileData user) {
        tvWelcome.setText(getString(R.string.welcome_user, user.login));
    }
}
