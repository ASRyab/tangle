package com.tangle.screen.activities.matches;

public interface MatchesItemClickListener {
    void onItemChatClick(String userId);

    void onItemEventClick(String userId);

    void onItemPhotoClick(String userId);
}