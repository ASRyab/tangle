package com.tangle.screen.activities.matches;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.R;
import com.tangle.model.chat.MailMessagePhoenix;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.ConvertingUtils;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

public class MatchesItemViewHolder extends RecyclerView.ViewHolder implements MatchesItemView {
    private RelativeLayout parentLayout, inviteImgLayout;
    private ImageView imgUserPhoto, imgMutualIndicator;
    private TextView txtUserName, txtItemBody, txtTimeIndicator, txtUnreadMsgCount;

    private MatchesItemClickListener matchesItemClickListener;
    private MatchesItemPresenter matchesItemPresenter;

    public MatchesItemViewHolder(View view) {
        super(view);

        parentLayout = itemView.findViewById(R.id.parent_layout);
        parentLayout.setOnClickListener(v -> matchesItemPresenter.onItemClick());
        inviteImgLayout = itemView.findViewById(R.id.invite_img_layout);
        inviteImgLayout.setOnClickListener(v -> matchesItemPresenter.onItemPhotoClick());
        imgUserPhoto = itemView.findViewById(R.id.img_view_user_photo);
        imgMutualIndicator = itemView.findViewById(R.id.img_view_is_mutual_like);
        txtUserName = itemView.findViewById(R.id.txt_view_user_name);
        txtItemBody = itemView.findViewById(R.id.txt_view_body);
        txtTimeIndicator = itemView.findViewById(R.id.txt_view_time);
        txtUnreadMsgCount = itemView.findViewById(R.id.txt_view_unread_msg_count);
    }

    public void bindData(ProfileData profile) {
        matchesItemPresenter = new MatchesItemPresenter(this, profile);
    }

    public void setPrivateChatRoomsClickListener(MatchesItemClickListener matchesItemClickListener) {
        this.matchesItemClickListener = matchesItemClickListener;
    }

    @Override
    public void setProfileData(ProfileData profile) {
        GlideApp.with(imgUserPhoto)
                .load(profile.primaryPhoto.avatar)
                .placeholder(RequestOptionsFactory.getNonAvatar(profile))
                .apply(RequestOptions.circleCropTransform())
                .into(imgUserPhoto);
        txtUserName.setText(profile.login);
    }

    @Override
    public void goToChat(String userId) {
        if (matchesItemClickListener != null) {
            matchesItemClickListener.onItemChatClick(userId);
        }
    }

    @Override
    public void showInvite(String userId) {
        if (matchesItemClickListener != null) {
            matchesItemClickListener.onItemEventClick(userId);
        }
    }

    @Override
    public void showProfile(String userId) {
        if (matchesItemClickListener != null) {
            matchesItemClickListener.onItemPhotoClick(userId);
        }
    }

    @Override
    public void showEventNotification() {
        txtUnreadMsgCount.setVisibility(View.INVISIBLE);
        txtTimeIndicator.setVisibility(View.INVISIBLE);
        imgMutualIndicator.setVisibility(View.VISIBLE);
        txtItemBody.setText(txtItemBody.getResources().getString(R.string.matches_item_event_dummy_info));
    }

    @Override
    public void showChatInformation(MailMessagePhoenix lastMsg, int unreadMsgCount, ProfileData profile) {
        imgMutualIndicator.setVisibility(View.INVISIBLE);
        if(unreadMsgCount == 0) {
            txtUnreadMsgCount.setVisibility(View.GONE);
        } else {
            txtUnreadMsgCount.setText(String.valueOf(unreadMsgCount));
            txtUnreadMsgCount.setVisibility(View.VISIBLE);
        }
        txtTimeIndicator.setText(ConvertingUtils.getStringDateByPattern(lastMsg.getDateTime(), ConvertingUtils.DATE_FORMAT_HM));
        txtTimeIndicator.setVisibility(View.VISIBLE);
        txtItemBody.setText(ConvertingUtils.getLastMessageText(txtItemBody.getContext().getResources(),profile,lastMsg));
    }

    @Override
    public void showChatNotification() {
        txtUnreadMsgCount.setVisibility(View.INVISIBLE);
        txtTimeIndicator.setVisibility(View.INVISIBLE);
        imgMutualIndicator.setVisibility(View.VISIBLE);
        txtItemBody.setText(txtItemBody.getResources().getString(R.string.matches_item_chat_dummy_info));
    }
}