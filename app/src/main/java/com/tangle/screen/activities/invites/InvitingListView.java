package com.tangle.screen.activities.invites;

import com.tangle.api.rpc.rpc_response.invite.InviteData;
import com.tangle.base.ui.interfaces.EmptyList;

import java.util.List;

public interface InvitingListView extends EmptyList {
    void setInvitingList(List<InviteData> invitingList);

    void goToEvents();

    void showAPIError(String error);
}