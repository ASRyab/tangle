package com.tangle.screen.activities;

public interface ReadListening {
    void markItemsAsRead();
}
