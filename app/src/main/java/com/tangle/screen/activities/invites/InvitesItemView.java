package com.tangle.screen.activities.invites;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.api.rpc.rpc_response.invite.InviteData;
import com.tangle.model.event.EventInfo;
import com.tangle.model.profile_data.ProfileData;

public interface InvitesItemView {
    void setBasicData(InviteData inviteData, ProfileData profile, EventInfo event,
                      boolean isMyInvite, RequestOptions inviteIcoRequestOptions);

    void onInviteClick(InviteData inviteData, ProfileData profile, EventInfo eventInfo);
}