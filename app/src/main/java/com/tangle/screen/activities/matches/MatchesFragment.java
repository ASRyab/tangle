package com.tangle.screen.activities.matches;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.views.ActivitiesControl;
import com.tangle.base.ui.views.NotificationInformationLayout;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.activities.matches_info.MatchesInformationFragment;
import com.tangle.screen.chats.private_chat.PrivateChatFragment;
import com.tangle.screen.main.navigation_menu.MenuState;

import java.util.List;

public class MatchesFragment extends ActivitiesControl implements MatchesView,
        MatchesItemClickListener {
    private RecyclerView matchesRVList;
    private MatchesListAdapter matchesListAdapter;
    private NotificationInformationLayout notificationInformationLayout;

    private MatchesPresenter presenter = new MatchesPresenter();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_matches, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        matchesRVList = view.findViewById(R.id.matches_rview);
        matchesListAdapter = new MatchesListAdapter(this);
        matchesRVList.setLayoutManager(new LinearLayoutManager(matchesRVList.getContext()));
        matchesRVList.setItemAnimator(new DefaultItemAnimator());
        matchesRVList.setAdapter(matchesListAdapter);
        notificationInformationLayout = view.findViewById(R.id.notify_rview);
        notificationInformationLayout.setVisibility(View.GONE);
        notificationInformationLayout.setClickListener(v -> presenter.goToLikeClicked());
    }

    @Override
    public void setProfilesList(List<ProfileData> mutualLikeList) {
        matchesListAdapter.addProfileList(mutualLikeList);
    }

    @Override
    public void goToLikebook() {
        moveToMenu(MenuState.TAB_LIKE_OR_NOT_INDEX);
    }

    @Override
    public void updateEmptyViewState(boolean hasPhoto) {
    }

    @Override
    public void showEmptyListError() {
        matchesRVList.setVisibility(View.GONE);
        notificationInformationLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyListError() {
        matchesRVList.setVisibility(View.VISIBLE);
        notificationInformationLayout.setVisibility(View.GONE);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onItemChatClick(String chatMateId) {
        addFragment(PrivateChatFragment.newInstance(chatMateId), true);
    }

    @Override
    public void onItemEventClick(String userId) {
        addFragment(MatchesInformationFragment.newInstance(userId), true);
    }

    @Override
    public void onItemPhotoClick(String userId) {
        ApplicationLoader.getApplicationInstance().getNavigationManager().showUserProfile(userId);
    }

    @Override
    public void markItemsAsRead() {
        presenter.markItemsAsRead();
    }
}