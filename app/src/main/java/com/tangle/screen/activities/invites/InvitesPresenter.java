package com.tangle.screen.activities.invites;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.screen.activities.ReadListening;
import com.tangle.utils.RxUtils;

import java.util.Collections;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class InvitesPresenter extends LifecyclePresenter<InvitingListView> implements ReadListening {

    @Override
    public void attachToView(InvitingListView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        getInvitingList();
    }

    void onCheckEventsClick() {
        getView().goToEvents();
    }

    private void getInvitingList() {
        Disposable disposable = ApplicationLoader.getApplicationInstance().getInviteManager().getAllWithUpdate()
                .doOnNext(Collections::sort)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(inviteItems -> {
                    if (!inviteItems.isEmpty()) {
                        getView().setInvitingList(inviteItems);
                    } else {
                        getView().showEmptyListError();
                    }
                }, RxUtils.getEmptyErrorConsumer(TAG, "getInvitingList"));
        monitor(disposable, State.DESTROY_VIEW);
    }

    @Override
    public void markItemsAsRead() {
        ApplicationLoader.getApplicationInstance().getInviteManager().setAllAsRead();
    }
}