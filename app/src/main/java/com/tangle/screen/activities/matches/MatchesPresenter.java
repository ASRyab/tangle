package com.tangle.screen.activities.matches;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.activities.ReadListening;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;

public class MatchesPresenter extends LifecyclePresenter<MatchesView> implements ReadListening {
    protected ArrayList<ProfileData> users = new ArrayList<>();

    @Override
    public void attachToView(MatchesView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        monitor(ApplicationLoader.getApplicationInstance().getLikeManager().getItemsWithUpdates()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::updateItems, error -> getView().showEmptyListError()), State.DESTROY_VIEW);
    }

    protected boolean shouldShowProfile(ProfileData profileData) {
        return profileData.isMatchedUser;
    }

    private void updateItems(List<ProfileData> updated) {
        for (ProfileData profileData : updated) {
            if (!shouldShowProfile(profileData) || (profileData.isBlocked())) {
                users.remove(profileData);
            } else if (users.contains(profileData)) {
                int index = users.indexOf(profileData);
                users.remove(index);
                users.add(index, profileData);
            } else {
                users.add(profileData);
            }
        }
        if (users.isEmpty()) {
            getView().showEmptyListError();
        } else {
            getView().hideEmptyListError();
            getView().setProfilesList(users);
        }
    }

    void goToLikeClicked() {
        getView().goToLikebook();
    }

    public void markItemsAsRead() {
        ApplicationLoader.getApplicationInstance().getLikeManager().setMatchesAsRead();
    }
}