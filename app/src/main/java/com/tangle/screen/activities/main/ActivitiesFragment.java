package com.tangle.screen.activities.main;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.implementation.adapters.TabViewPagerAdapter;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.screen.activities.invites.InvitesFragment;
import com.tangle.screen.activities.likes.LikesFragment;
import com.tangle.screen.activities.matches.MatchesFragment;

public class ActivitiesFragment extends SecondaryScreenFragment implements ActivitiesView {
    private static final int TAB_COUNT = 3;

    public static final int TAB_MATCHES_INDEX = 0;
    public static final int TAB_INVITES_INDEX = 1;
    public static final int TAB_LIKES_INDEX = 2;

    private ActivitiesPresenter presenter = new ActivitiesPresenter();

    private ViewPager activitiesVP;
    private TabLayout activitiesTabLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_activities, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initToolbar(getString(R.string.connections),true);
        activitiesVP = view.findViewById(R.id.notices_vp);
        activitiesVP.setOffscreenPageLimit(TAB_COUNT);
        setupViewPager(activitiesVP);

        activitiesTabLayout = view.findViewById(R.id.notices_tab_layout);
        activitiesTabLayout.setupWithViewPager(activitiesVP);

        setupTab(TAB_INVITES_INDEX, false);
        setupTab(TAB_LIKES_INDEX, false);
        setupTab(TAB_MATCHES_INDEX, false);
    }

    private void setupViewPager(ViewPager viewPager) {
        TabViewPagerAdapter adapter = new TabViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new MatchesFragment(), "");
        adapter.addFragment(new InvitesFragment(), "");
        adapter.addFragment(new LikesFragment(), "");
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                presenter.onPageSelected(position);
            }
        });
    }

    @Override
    public void setupTab(int tabIndex, boolean showIndicator) {
        View customTabView = activitiesTabLayout.getTabAt(tabIndex).getCustomView();
        if (customTabView != null) {
            customTabView.findViewById(R.id.tab_indicator).setVisibility(showIndicator ? View.VISIBLE : View.GONE);
        } else {
            Context context = ApplicationLoader.getContext();
            View view = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.layout_tab_item, null);
            TextView title = view.findViewById(R.id.title);
            switch (tabIndex) {
                case TAB_INVITES_INDEX:
                    title.setText(context.getString(R.string.tab_title_notices_invites));
                    break;
                case TAB_LIKES_INDEX:
                    title.setText(context.getString(R.string.tab_title_notices_likes));
                    break;
                case TAB_MATCHES_INDEX:
                    title.setText(context.getString(R.string.tab_title_notices_matches));
                    break;
            }
            view.findViewById(R.id.tab_indicator).setVisibility(showIndicator ? View.VISIBLE : View.GONE);
            activitiesTabLayout.getTabAt(tabIndex).setCustomView(view);
        }
    }

    @Override
    public void moveToTab(int tabIndex) {
        activitiesVP.setCurrentItem(tabIndex);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}