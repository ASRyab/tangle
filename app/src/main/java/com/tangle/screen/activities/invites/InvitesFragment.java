package com.tangle.screen.activities.invites;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tangle.R;
import com.tangle.api.rpc.rpc_response.invite.InviteData;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.views.ActivitiesControl;
import com.tangle.base.ui.views.NotificationInformationLayout;
import com.tangle.screen.invite.event_details.InviteDetailsFragment;
import com.tangle.screen.main.navigation_menu.MenuState;

import java.util.List;

public class InvitesFragment extends ActivitiesControl implements InvitingListView,
        InvitingListAdapter.OnInviteItemClickListener {
    private RecyclerView invitesRVList;
    private InvitingListAdapter invitesListAdapter;
    private NotificationInformationLayout notificationInformationLayout;
    private InvitesPresenter presenter = new InvitesPresenter();

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_invites, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        notificationInformationLayout = view.findViewById(R.id.notify_rview);
        notificationInformationLayout.setVisibility(View.GONE);
        notificationInformationLayout.setClickListener(v -> presenter.onCheckEventsClick());
        invitesRVList = view.findViewById(R.id.invites_rview);
        invitesListAdapter = new InvitingListAdapter(this);
        invitesRVList.setLayoutManager(new LinearLayoutManager(view.getContext().getApplicationContext()));
        invitesRVList.setItemAnimator(new DefaultItemAnimator());
        invitesRVList.setAdapter(invitesListAdapter);
    }

    @Override
    public void setInvitingList(List<InviteData> invitingList) {
        invitesRVList.setVisibility(View.VISIBLE);
        notificationInformationLayout.setVisibility(View.GONE);
        invitesListAdapter.addInvites(invitingList);
    }

    @Override
    public void goToEvents() {
        moveToMenu(MenuState.TAB_EVENT_INDEX);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onInviteClick(String inviteId, String profileId, String eventId) {
        addFragment(InviteDetailsFragment.newInstance(eventId, profileId, inviteId), true);
    }

    @Override
    public void markItemsAsRead() {
        presenter.markItemsAsRead();
    }

    @Override
    public void showEmptyListError() {
        invitesRVList.setVisibility(View.GONE);
        notificationInformationLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyListError() {
        invitesRVList.setVisibility(View.VISIBLE);
        notificationInformationLayout.setVisibility(View.GONE);
    }
}