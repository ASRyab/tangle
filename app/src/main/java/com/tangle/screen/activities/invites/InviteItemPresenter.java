package com.tangle.screen.activities.invites;

import android.util.Pair;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.ApplicationLoader;
import com.tangle.api.rpc.rpc_response.invite.InviteData;
import com.tangle.model.event.EventInfo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.RxUtils;

import io.reactivex.Observable;

class InviteItemPresenter {
    private static final String TAG = InviteItemPresenter.class.getSimpleName();

    private InviteData inviteData;
    private ProfileData profileData;
    private EventInfo eventInfo;

    private InvitesItemView view;

    InviteItemPresenter(InvitesItemView view, RequestOptions inviteIcoRequestOptions,
                        InviteData inviteData) {
        this.view = view;
        this.inviteData = inviteData;

        loadData(inviteIcoRequestOptions);
    }

    private void loadData(RequestOptions inviteIcoRequestOptions) {
        boolean isMyInvite = ApplicationLoader.getApplicationInstance().getUserManager().isCurrentUser(inviteData.from);
        Observable<ProfileData> observable1 = ApplicationLoader.getApplicationInstance().getUserManager().getUserById(isMyInvite ? inviteData.to : inviteData.from);
        Observable<EventInfo> observable2 = ApplicationLoader.getApplicationInstance().getEventManager().getEventById(inviteData.eventId);
        Observable.zip(observable1,
                observable2,
                Pair::create).subscribe(pair -> {
            this.profileData = pair.first;
            this.eventInfo = pair.second;
            view.setBasicData(inviteData, this.profileData, this.eventInfo, isMyInvite, inviteIcoRequestOptions);
        }, RxUtils.getEmptyErrorConsumer(TAG));
    }

    public void onInviteClick() {
        if (profileData != null && inviteData != null && eventInfo != null) {
            view.onInviteClick(inviteData, profileData, eventInfo);
        }
    }
}