package com.tangle.screen.activities.matches_info;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.base.ui.interfaces.LocationCheckableView;
import com.tangle.managers.PhotoManager;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.choose.ShowedEventsType;

public interface MatchesInformationView extends LoadingView, PhotoManager.PhotoView, LocationCheckableView {
    void setProfileData(ProfileData profile);

    void goToInvite(String userId, ShowedEventsType showedEventsType);
}