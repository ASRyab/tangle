package com.tangle.screen.activities.matches_info;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.views.ShapeButton;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.choose.ChooseEventFragment;
import com.tangle.screen.events.choose.ShowedEventsType;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

public class MatchesInformationFragment extends SecondaryScreenFragment implements MatchesInformationView {
    private final static String USER_ID_KEY = "user_id";
    private MatchesInformationPresenter presenter = new MatchesInformationPresenter();
    private ImageView imgUserAvatar;
    private TextView txtUserName, txtUserInfoBasic, txtBodyText;
    private ShapeButton btnInvite;

    public static MatchesInformationFragment newInstance(String userId) {
        MatchesInformationFragment matchesInformationFragment = new MatchesInformationFragment();
        Bundle args = new Bundle();
        args.putString(USER_ID_KEY, userId);
        matchesInformationFragment.setArguments(args);
        return matchesInformationFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundleData(getArguments());
    }

    private void getBundleData(Bundle bundle) {
        if (bundle != null) {
            presenter.setUserId(bundle.getString(USER_ID_KEY));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_matches_information, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    private void initUI(View view) {
        imgUserAvatar = view.findViewById(R.id.img_avatar);
        txtUserName = view.findViewById(R.id.txt_name);
        txtUserInfoBasic = view.findViewById(R.id.txt_info_basic);
        txtBodyText = view.findViewById(R.id.txt_body);
        btnInvite = view.findViewById(R.id.btn_invite);
        btnInvite.setOnClickListener(v -> presenter.onInviteClicked());
    }

    @Override
    public void setProfileData(ProfileData profile) {
        GlideApp.with(this)
                .load(profile.primaryPhoto.normal)
                .placeholder(RequestOptionsFactory.getNonAvatar(profile))
                .apply(RequestOptions.priorityOf(Priority.HIGH))
                .apply(RequestOptions.circleCropTransform())
                .into(imgUserAvatar);
        txtUserName.setText(profile.login);
        txtUserInfoBasic.setText(getResources().getString(R.string.profile_basic_info_template, profile.age, profile.geo.city, profile.geo.country_code));
        txtBodyText.setText(getResources().getString(R.string.formatted_matches_info_body, profile.login));
    }

    @Override
    public void goToInvite(String userId, ShowedEventsType showedEventsType) {
        addFragment(ChooseEventFragment.newInstance(userId, showedEventsType, ChooseEventFragment.GoingFrom.MATCH), true);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}