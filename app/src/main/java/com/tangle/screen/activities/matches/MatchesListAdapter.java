package com.tangle.screen.activities.matches;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.tangle.R;
import com.tangle.model.profile_data.ProfileData;

import java.util.ArrayList;
import java.util.List;

public class MatchesListAdapter extends RecyclerView.Adapter<MatchesItemViewHolder> {
    private List<ProfileData> profileItems = new ArrayList<>();
    private MatchesItemClickListener matchesItemClickListener;

    public MatchesListAdapter(MatchesItemClickListener matchesItemClickListener) {
        this.matchesItemClickListener = matchesItemClickListener;
    }

    public void addProfileList(List<ProfileData> profileItems) {
        this.profileItems.clear();
        this.profileItems.addAll(profileItems);
        notifyDataSetChanged();
    }

    @Override
    public MatchesItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MatchesItemViewHolder(View.inflate(parent.getContext(), R.layout.item_matches_people, null));
    }

    @Override
    public void onBindViewHolder(MatchesItemViewHolder holder, final int position) {
        holder.bindData(profileItems.get(position));
        holder.setPrivateChatRoomsClickListener(matchesItemClickListener);
    }

    public void updateProfile(ProfileData data) {
        int index = profileItems.indexOf(data);
        profileItems.set(index,data);
        notifyItemChanged(index);
    }

    @Override
    public int getItemCount() {
        return profileItems.size();
    }
}