package com.tangle.screen.activities.likes

import android.os.Bundle
import com.tangle.ApplicationLoader
import com.tangle.base.mvp.LifecyclePresenter
import com.tangle.base.mvp.State
import com.tangle.managers.PhotoManager
import com.tangle.model.profile_data.ProfileData
import com.tangle.screen.activities.ReadListening
import com.tangle.utils.RxUtils
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer

class LikesPresenter(var model: LikesContract.Model) : LifecyclePresenter<LikesContract.View>(), ReadListening {
    var users = ArrayList<ProfileData>()


    override fun attachToView(view: LikesContract.View, savedInstanceState: Bundle?) {
        super.attachToView(view, savedInstanceState)

        monitor(getUpdates()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (it.first.isEmpty() && it.second.isEmpty()) {
                        view.showEmptyListError()
                    } else {
                        view.hideEmptyListError()
                        view.setLikedUserList(it.first, it.second)
                    }
                }, { getView().showEmptyListError() }),
                State.DESTROY_VIEW)

        monitor(model.getCheckPhotoObservable(getView())
                .subscribe(Consumer { data ->
                    getView().updateEmptyViewState(data === PhotoManager.PhotoState.HAS_PHOTO || data === PhotoManager.PhotoState.HAS_APPROVE_PHOTO)
                }, RxUtils.getEmptyErrorConsumer()),
                State.DESTROY)


    }

    private fun shouldShowProfile(profileData: ProfileData): Boolean {
        return !profileData.isMatchedUser
    }

    fun updateDataset(updated: List<ProfileData>): List<ProfileData> {
        for (profileData in updated) {
            if (!shouldShowProfile(profileData) || profileData.isBlocked) {
                users.remove(profileData)
            } else if (users.contains(profileData)) {
                val index = users.indexOf(profileData)
                users.removeAt(index)
                users.add(index, profileData)
            } else {
                users.add(profileData)
            }
        }
        return users
    }

    private fun updateItems(users: List<ProfileData>): Pair<ArrayList<ProfileData>, ArrayList<ProfileData>> {
        var likedOther = ArrayList<ProfileData>()
        var likedMe = ArrayList<ProfileData>()
        users.forEach { user ->
            if (user.isYouLikedUser && !user.isLikedMeUser) {
                likedOther.add(user)
            } else if (user.isLikedMeUser && !user.isYouLikedUser)
                likedMe.add(user)
        }
        likedMe = ArrayList(likedMe
                .sortedByDescending { it.lastUserActivityDate }
                .sortedWith(compareBy { !it.isNew }))
        likedOther = ArrayList(likedOther
                .sortedByDescending { it.lastUserActivityDate })
        return Pair(likedMe, likedOther)
    }

    fun onSearchMyLikeClicked() {
        view.goToLikebook()
    }

    fun onUserClicked(item: ProfileData) {
        ApplicationLoader.getApplicationInstance().likeManager.setAsRead(item.id)
    }

    fun getUpdates(): Observable<Pair<ArrayList<ProfileData>, ArrayList<ProfileData>>> {
        return model.getProfilesWithUpdates()
                .map { updateDataset(it) }
                .map { updateItems(it) }
    }

    override fun markItemsAsRead() {
        ApplicationLoader.getApplicationInstance().likeManager.setAllAsRead()
    }
}