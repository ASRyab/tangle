package com.tangle.screen.activities.likes

import android.os.Bundle
import com.tangle.ApplicationLoader
import com.tangle.base.mvp.LifecyclePresenter
import com.tangle.base.mvp.State
import com.tangle.model.profile_data.ProfileData
import com.tangle.utils.RxUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.Consumer

class LikesListPresenter : LifecyclePresenter<LikesListView>() {
    private var users = ArrayList<ProfileData>()
    var isIncomingLike: Boolean? = null
    override fun attachToView(view: LikesListView, savedInstanceState: Bundle?) {
        super.attachToView(view, savedInstanceState)
        monitor(ApplicationLoader.getApplicationInstance().likeManager.itemsWithUpdates
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(Consumer { this.updateItems(it) }, RxUtils.getEmptyErrorConsumer()), State.DESTROY_VIEW)

    }

    private fun shouldShowProfile(profileData: ProfileData): Boolean {
        return !profileData.isMatchedUser
    }

    private fun updateItems(updated: List<ProfileData>) {
        for (profileData in updated) {
            if (!shouldShowProfile(profileData) || profileData.isBlocked) {
                users.remove(profileData)
            } else if (users.contains(profileData)) {
                val index = users.indexOf(profileData)
                users.removeAt(index)
                users.add(index, profileData)
            } else {
                users.add(profileData)
            }
        }
        var likedList: List<ProfileData>? = null
        users = ArrayList(users.sortedByDescending { it.lastUserActivityDate })
        isIncomingLike?.let { it ->
            likedList = if (it) {
                users.filter { it1 -> it1.isLikedMeUser && !it1.isYouLikedUser }.sortedWith(compareBy { !it.isNew })
            } else users.filter { it1 -> it1.isYouLikedUser && !it1.isLikedMeUser }
        }
        view.setLikedUserList(ArrayList(likedList))
    }

    fun onUserClicked(item: ProfileData) {
        ApplicationLoader.getApplicationInstance().likeManager.setAsRead(item.id)
    }

}