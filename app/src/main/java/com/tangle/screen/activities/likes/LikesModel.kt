package com.tangle.screen.activities.likes

import com.tangle.ApplicationLoader
import com.tangle.managers.PhotoManager
import com.tangle.model.profile_data.ProfileData
import io.reactivex.Observable
import io.reactivex.Single

class LikesModel : LikesContract.Model {
    override fun getProfilesWithUpdates(): Observable<List<ProfileData>> {
        return ApplicationLoader.getApplicationInstance().likeManager.itemsWithUpdates
    }

    override fun getCheckPhotoObservable(view: LikesContract.View): Single<PhotoManager.PhotoState> {
        return ApplicationLoader.getApplicationInstance().photoManager.checkPhotoObservable(view)
    }
}