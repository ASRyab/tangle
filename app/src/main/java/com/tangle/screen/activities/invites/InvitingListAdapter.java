package com.tangle.screen.activities.invites;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.rpc.rpc_response.invite.InviteData;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.ArrayList;
import java.util.List;

public class InvitingListAdapter extends RecyclerView.Adapter<InviteItemViewHolder> {
    private List<InviteData> inviteItems;
    private RequestOptions inviteIcoRequestOptions;

    private OnInviteItemClickListener inviteItemClickListener;

    InvitingListAdapter(OnInviteItemClickListener inviteItemClickListener) {
        this.inviteItems = new ArrayList<>();
        this.inviteIcoRequestOptions = RequestOptionsFactory.getSmallRoundedCornersOptions(ApplicationLoader.getContext());
        this.inviteItemClickListener = inviteItemClickListener;
    }

    void addInvites(List<InviteData> inviteItems) {
        this.inviteItems = inviteItems;
        notifyDataSetChanged();
    }

    @Override
    public InviteItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InviteItemViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_invite, parent, false));
    }

    @Override
    public void onBindViewHolder(InviteItemViewHolder holder, final int position) {
        holder.bindData(inviteItems.get(position), inviteIcoRequestOptions, inviteItemClickListener);
    }

    @Override
    public int getItemCount() {
        return inviteItems.size();
    }

    interface OnInviteItemClickListener {
        void onInviteClick(String inviteId, String profileId, String eventId);
    }
}