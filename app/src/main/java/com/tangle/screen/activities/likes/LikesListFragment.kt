package com.tangle.screen.activities.likes

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.tangle.ApplicationLoader
import com.tangle.R
import com.tangle.base.implementation.adapters.LikePeopleGridAdapter
import com.tangle.base.mvp.LifecyclePresenter
import com.tangle.base.ui.SecondaryScreenFragment
import com.tangle.model.profile_data.ProfileData
import kotlinx.android.synthetic.main.fragment_likes_list.*
import kotlinx.android.synthetic.main.toolbar_default.*


class LikesListFragment : SecondaryScreenFragment(), LikesListView, LikePeopleGridAdapter.GridItemClickListener {

    val presenter: LikesListPresenter = LikesListPresenter()
    private var isIncomingLike: Boolean? = null
    private lateinit var likesAdapter: LikePeopleGridAdapter
    private lateinit var gridLayoutManager: GridLayoutManager

    companion object {
        const val GRID_COLUMN_COUNT = 3
        private const val IS_INCOMING_LIKES = "is_incoming_like"

        fun newInstance(isIncomingLike: Boolean): LikesListFragment {
            val fragment = LikesListFragment()
            val bundle = Bundle()
            bundle.putBoolean(IS_INCOMING_LIKES, isIncomingLike)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isIncomingLike = arguments?.getBoolean(IS_INCOMING_LIKES)
        presenter.isIncomingLike = isIncomingLike

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_likes_list, container, false)
    }

    override fun onPostViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onPostViewCreated(view, savedInstanceState)
        initUI(view)
    }

    private fun initUI(view: View) {
        var headerText: String? = ""
        isIncomingLike?.let {
            txt_toolbar_title.visibility = View.VISIBLE
            txt_toolbar_title.text = context?.getString(if (it) R.string.incoming_likes else R.string.outgoing_likes)
            headerText = context?.getString(if (it) R.string.incoming_likes_text else R.string.outgoing_likes_text)
        }

        gridLayoutManager = GridLayoutManager(view.context, GRID_COLUMN_COUNT)
        likesAdapter = LikePeopleGridAdapter(this, false)
        likesAdapter.setShouldShowYouLikeIndicator(false)
        likesAdapter.setHeaderText(headerText)

        rvLikes.layoutManager = gridLayoutManager
        rvLikes.itemAnimator = DefaultItemAnimator()
        rvLikes.adapter = likesAdapter
        likesAdapter.setLoadMoreListener(rvLikes, null)

    }

    override fun setLikedUserList(likedList: ArrayList<ProfileData>) {
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position == 0) GRID_COLUMN_COUNT else 1
            }
        }
        likesAdapter.setProfileItems(likedList)
    }

    override fun onItemClick(profileData: ProfileData) {
        presenter.onUserClicked(profileData)
        ApplicationLoader.getApplicationInstance().navigationManager.showUserProfile(profileData.id)
    }

    override fun getPresenter(): LifecyclePresenter<*>? {
        return presenter
    }

}