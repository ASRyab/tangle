package com.tangle.screen.activities.matches;

import android.text.TextUtils;

import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.msg.HistoryType;
import com.tangle.model.chat.MailMessagePhoenix;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.RxUtils;

import java.util.List;

public class MatchesItemPresenter {
    private static final String TAG = MatchesItemPresenter.class.getSimpleName();

    private MatchesItemView view;
    private ProfileData profile;
    private boolean isChatItem;

    MatchesItemPresenter(MatchesItemView view, ProfileData profile) {
        this.view = view;
        this.profile = profile;

        setProfileData();
        loadLatestMsg();
        subscribeOnUpdates();
        ApplicationLoader.getApplicationInstance().getPrivateChatMsgsManager().updateMsgsListByChatMateId(profile.id);
    }

    private void setProfileData() {
        view.setProfileData(profile);
    }

    public ProfileData getProfileData() {
        return profile;
    }

    public void onItemClick() {
        if (isChatItem) {
            view.goToChat(profile.id);
        } else {
            view.showInvite(profile.id);
        }
    }

    public void onItemPhotoClick() {
        view.showProfile(profile.id);
    }

    private void loadLatestMsg() {
        ApplicationLoader.getApplicationInstance().getPrivateChatMsgsManager().getMsgsListByChatMateId(0, HistoryType.UNDEFINED, profile.id)
                .subscribe(msgs -> {
                    if (msgs.isEmpty()) {
                        if (TextUtils.isEmpty(profile.matchedEvent)) {
                            view.showEventNotification();
                        } else {
                            view.showChatNotification();
                            isChatItem = true;
                        }
                    } else {
                        MailMessagePhoenix lastMsg = msgs.get(0);
                        setLastMsg(lastMsg, getUnreadMsgsCount(msgs));
                    }
                }, RxUtils.getEmptyErrorConsumer(TAG, "loadLatestMsg"));
    }

    private void subscribeOnUpdates() {
        ApplicationLoader.getApplicationInstance().getPrivateChatMsgsManager().getUpdateRooms().subscribe(chatHistoryData -> {
            if (chatHistoryData.chatId.equals(profile.id)) {
                MailMessagePhoenix lastMsg = chatHistoryData.lastChatRoomMsg;
                if (lastMsg.isSystemMsg()) {
                    view.showChatNotification();
                    isChatItem = true;
                } else {
                    setLastMsg(lastMsg, getUnreadMsgsCount(chatHistoryData.roomMsg));
                }
            }
        }, RxUtils.getEmptyErrorConsumer(TAG, "getPrivateChatMsgsManager().getUpdateRooms()"));
        ApplicationLoader.getApplicationInstance().getUserManager().getUpdates().subscribe(data -> {
            if (data.id.equals(profile.id)) {
                profile = data;
                setProfileData();
                loadLatestMsg();
            }
        }, RxUtils.getEmptyErrorConsumer(TAG, "getUserManager().getUpdates()"));
    }

    private int getUnreadMsgsCount(List<MailMessagePhoenix> msgs) {
        int unread = 0;
        for (MailMessagePhoenix msg : msgs) {
            if (!msg.read && !msg.isSystemMsg() && !msg.isMyMsg()) {
                unread++;
            }
        }
        return unread;
    }

    private void setLastMsg(MailMessagePhoenix lastMsg, int unreadMsgCount) {
        if (lastMsg.isSystemMsg()) {
            return;
        }
        view.showChatInformation(lastMsg, unreadMsgCount, profile);
        isChatItem = true;
    }
}