package com.tangle.screen.activities.likes

import android.os.Bundle
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import com.tangle.ApplicationLoader
import com.tangle.R
import com.tangle.base.implementation.adapters.LikeConnectionsAdapter
import com.tangle.base.mvp.LifecyclePresenter
import com.tangle.base.ui.views.ActivitiesControl
import com.tangle.base.ui.views.NotificationInformationLayout
import com.tangle.model.profile_data.ProfileData
import com.tangle.screen.main.navigation_menu.MenuState
import com.tangle.utils.glide.RequestOptionsFactory
import java.util.*

class LikesFragment : ActivitiesControl(), LikesContract.View, LikeConnectionsAdapter.GridItemClickListener {

    private var likedYouRVList: RecyclerView? = null
    private var likeConnectionsAdapter: LikeConnectionsAdapter? = null
    private var notificationInformationLayout: NotificationInformationLayout? = null
    private var noPhotoContainer: FrameLayout? = null
    private var gridLayoutManager: GridLayoutManager? = null
    private val presenter = LikesPresenter(LikesModel())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_liked_you, container, false)
    }

    override fun onPostViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onPostViewCreated(view, savedInstanceState)
        initUI(view)
    }

    private fun initUI(view: View) {

        likedYouRVList = view.findViewById(R.id.liked_you_rview)
        likeConnectionsAdapter = LikeConnectionsAdapter(context, RequestOptionsFactory.getSmallRoundedCornersOptions(context), this)
        gridLayoutManager = GridLayoutManager(view.context.applicationContext, GRID_COLUMN_NUMBER)

        likedYouRVList?.layoutManager = GridLayoutManager(view.context.applicationContext, GRID_COLUMN_NUMBER)
        likedYouRVList?.itemAnimator = DefaultItemAnimator()
        likedYouRVList?.adapter = likeConnectionsAdapter

        notificationInformationLayout = view.findViewById(R.id.notify_rview)
        noPhotoContainer = view.findViewById(R.id.no_photo_container)

        notificationInformationLayout?.setClickListener { presenter.onSearchMyLikeClicked() }
        notificationInformationLayout?.setButtonText(R.string.liked_you_notification_btn_text)
        notificationInformationLayout?.setNotificationBodyText(R.string.liked_you_notification_body)
    }

    override fun goToLikebook() {
        moveToMenu(MenuState.TAB_LIKE_OR_NOT_INDEX)
    }

    override fun updateEmptyViewState(hasPhoto: Boolean) {
        if (hasPhoto) {
            noPhotoContainer?.visibility = View.GONE
        }
    }

    override fun showEmptyListError() {
        likedYouRVList?.visibility = View.GONE
        notificationInformationLayout?.visibility = View.VISIBLE
    }

    override fun hideEmptyListError() {
        likedYouRVList?.visibility = View.VISIBLE
        notificationInformationLayout?.visibility = View.GONE
    }

    override fun getPresenter(): LifecyclePresenter<*> {
        return presenter
    }

    override fun onItemClick(item: ProfileData) {
        presenter.onUserClicked(item)
        ApplicationLoader.getApplicationInstance().navigationManager.showUserProfile(item.id, true)
    }

    override fun openPhotoChooseScreen() {
        ApplicationLoader.getApplicationInstance().navigationManager
                .showPhotoUploadFrame(childFragmentManager, R.id.no_photo_container)
    }

    override fun markItemsAsRead() {
        presenter.markItemsAsRead()
    }

    override fun setLikedUserList(likedYou: ArrayList<ProfileData>, likedOther: ArrayList<ProfileData>) {
        gridLayoutManager?.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                val likeMeSize = if (likedYou.isEmpty()) 0 else if (likedYou.size > MAX_ITEMS_COUNT) MAX_ITEMS_COUNT + 1 else likedYou.size + 1
                return if (position == 0 || position == likeMeSize)
                    GRID_COLUMN_NUMBER
                else
                    1
            }
        }
        likedYouRVList?.layoutManager = gridLayoutManager
        likeConnectionsAdapter?.setLikedItems(likedYou, likedOther)
        likeConnectionsAdapter?.notifyDataSetChanged()

    }

    override fun onIncomingLikesClick() {
        ApplicationLoader.getApplicationInstance().navigationManager.showLikes(true)

    }

    override fun onOutgoingLikesClick() {
        ApplicationLoader.getApplicationInstance().navigationManager.showLikes(false)
    }

    companion object {
        private val TAG = LikesFragment::class.java.simpleName
        private const val GRID_COLUMN_NUMBER = 3
        private const val MAX_ITEMS_COUNT = 6
    }
}