package com.tangle.screen.activities.main;

public interface ActivitiesView {
    void setupTab(int tabIndex, boolean showIndicator);

    void moveToTab(int tabIndex);
}