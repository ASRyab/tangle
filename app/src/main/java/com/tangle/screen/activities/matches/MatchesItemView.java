package com.tangle.screen.activities.matches;

import com.tangle.model.chat.MailMessagePhoenix;
import com.tangle.model.profile_data.ProfileData;

public interface MatchesItemView {
    void setProfileData(ProfileData profile);

    void goToChat(String userId);

    void showInvite(String userId);

    void showProfile(String userId);

    void showEventNotification();

    void showChatInformation(MailMessagePhoenix lastMsg, int unreadMsgCount, ProfileData profile);

    void showChatNotification();
}