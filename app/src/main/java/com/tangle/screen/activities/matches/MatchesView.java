package com.tangle.screen.activities.matches;

import com.tangle.base.ui.interfaces.EmptyList;
import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.managers.PhotoManager;
import com.tangle.model.profile_data.ProfileData;

import java.util.List;

public interface MatchesView extends LoadingView, EmptyList, PhotoManager.PhotoView {
    void setProfilesList(List<ProfileData> userList);

    void goToLikebook();

    void updateEmptyViewState(boolean hasPhoto);
}