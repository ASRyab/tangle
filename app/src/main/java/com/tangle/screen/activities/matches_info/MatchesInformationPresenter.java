package com.tangle.screen.activities.matches_info;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.analytics.SimpleActions;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.managers.PhotoManager;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.choose.ShowedEventsType;
import com.tangle.utils.RxUtils;

import io.reactivex.disposables.Disposable;

public class MatchesInformationPresenter extends LifecyclePresenter<MatchesInformationView> {
    private String userId;
    private ProfileData profileData;

    void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public void attachToView(MatchesInformationView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        loadProfile();
    }

    private void loadProfile() {
        Disposable disposable = ApplicationLoader.getApplicationInstance().getUserManager().getUserGraphById(userId)
                .subscribe(profileData -> {
                    this.profileData = profileData;
                    getView().setProfileData(profileData);
                }, error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    public void onInviteClicked() {
        PhotoManager photoManager = ApplicationLoader.getApplicationInstance().getPhotoManager();
        monitor(photoManager
                        .checkPhoto(getView())
                        .subscribe(data -> {
                            switch (data) {
                                case HAS_APPROVE_PHOTO:
                                    checkUserLocation();
                                    break;
                                case HAS_PHOTO:
                                    getView().showNoApprovePhoto();
                                    break;
                            }
                        }, RxUtils.getEmptyErrorConsumer()),
                State.DESTROY);

    }

    private void goToInvite() {
        ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.MATCH_START_INVITE);
        getView().goToInvite(userId, ShowedEventsType.USER_UPCOMING_EVENT);
    }

    private void checkUserLocation() {
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().checkUserInWhiteList()
                        .subscribe(this::doCheck, RxUtils.getEmptyErrorConsumer(TAG)),
                State.DESTROY_VIEW);
    }

    private void doCheck(Boolean isInWhiteList) {
        if (isInWhiteList) {
            getView().showRemindLocationDialog(this::goToInvite);
        } else {
            goToInvite();
        }
    }

}