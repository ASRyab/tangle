package com.tangle.screen.activities.main;

import com.tangle.ApplicationLoader;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.main.ScreenJump;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;

public class ActivitiesPresenter extends LifecyclePresenter<ActivitiesView> {

    private int actualPage;

    public void showChatIndicator(int tabIndex, boolean showIndicator) {
        getView().setupTab(tabIndex, showIndicator);
    }

    @Override
    public void onStart() {
        super.onStart();
        Observable<ProfileData> dataObservable = ApplicationLoader.getApplicationInstance().getLikeManager().getItemsWithUpdates()
                .flatMapIterable(data -> data)
                .filter(profileData -> profileData.isNew);
        monitor(dataObservable
                        .filter(profileData -> profileData.isMatchedUser)
                        .filter(profileData -> actualPage != ActivitiesFragment.TAB_MATCHES_INDEX)
                        .firstOrError()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(profileData -> showChatIndicator(ActivitiesFragment.TAB_MATCHES_INDEX, true)
                                , throwable -> hideIndicator(ActivitiesFragment.TAB_MATCHES_INDEX))
                , State.STOP);
        monitor(dataObservable
                        .filter(profileData -> actualPage != ActivitiesFragment.TAB_LIKES_INDEX)
                        .filter(profileData -> !profileData.isMatchedUser)
                        .firstOrError()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(profileData -> showChatIndicator(ActivitiesFragment.TAB_LIKES_INDEX, true)
                                , throwable -> hideIndicator(ActivitiesFragment.TAB_LIKES_INDEX))
                , State.STOP);
        monitor(ApplicationLoader.getApplicationInstance().getInviteManager().getAllWithUpdate()
                        .flatMapIterable(responses -> responses)
                        .filter(responses -> !responses.isRead)
                        .filter(profileData -> actualPage != ActivitiesFragment.TAB_INVITES_INDEX)
                        .firstOrError()
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(profileData -> showChatIndicator(ActivitiesFragment.TAB_INVITES_INDEX, true)
                                , throwable -> hideIndicator(ActivitiesFragment.TAB_INVITES_INDEX))
                , State.STOP);
    }


    protected void makeScreenJump(ScreenJump jump) {
        switch (jump) {
            case WHO_LIKED:
                getView().moveToTab(ActivitiesFragment.TAB_LIKES_INDEX);
                break;
            case MUTUAL_LIKE:
                getView().moveToTab(ActivitiesFragment.TAB_MATCHES_INDEX);
                break;
            case INVITE:
                getView().moveToTab(ActivitiesFragment.TAB_INVITES_INDEX);
                break;
        }
    }

    public void onPageSelected(int position) {
        hideIndicator(actualPage = position);
    }

    private void hideIndicator(int position) {
        showChatIndicator(position, false);
    }
}