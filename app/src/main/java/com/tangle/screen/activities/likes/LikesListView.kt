package com.tangle.screen.activities.likes

import com.tangle.model.profile_data.ProfileData

interface LikesListView {
    fun setLikedUserList(likedList: ArrayList<ProfileData>)
}