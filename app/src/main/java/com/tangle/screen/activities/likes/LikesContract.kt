package com.tangle.screen.activities.likes

import com.tangle.base.ui.interfaces.EmptyList
import com.tangle.base.ui.interfaces.LoadingView
import com.tangle.managers.PhotoManager
import com.tangle.model.profile_data.ProfileData
import io.reactivex.Observable
import io.reactivex.Single

class LikesContract {
    interface View : LoadingView, EmptyList, PhotoManager.PhotoView {

        fun setLikedUserList(likedYou: ArrayList<ProfileData>, likedOther: ArrayList<ProfileData>)

        fun goToLikebook()

        fun updateEmptyViewState(hasPhoto: Boolean)
    }

    interface Model {
        fun getProfilesWithUpdates(): Observable<List<ProfileData>>
        fun getCheckPhotoObservable(view: LikesContract.View): Single<PhotoManager.PhotoState>
    }
}