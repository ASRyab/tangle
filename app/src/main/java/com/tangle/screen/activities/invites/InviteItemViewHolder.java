package com.tangle.screen.activities.invites;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.R;
import com.tangle.api.rpc.rpc_response.invite.InviteData;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.Patterns;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.Date;
import java.util.List;

public class InviteItemViewHolder extends RecyclerView.ViewHolder implements InvitesItemView {
    private ImageView imgBig, imgSmall;
    private TextView txtFirstTitle, txtUserName, txtTo, txtSubTitle, txtEventName, txtInviteDate, txtInviteStatus;

    private InviteItemPresenter inviteItemPresenter;
    private InvitingListAdapter.OnInviteItemClickListener inviteItemClickListener;

    InviteItemViewHolder(View view) {
        super(view);
        imgBig = itemView.findViewById(R.id.big_img);
        imgSmall = itemView.findViewById(R.id.small_ico);
        txtFirstTitle = itemView.findViewById(R.id.txt_first_title);
        txtUserName = itemView.findViewById(R.id.txt_name);
        txtTo = itemView.findViewById(R.id.txt_to);
        txtSubTitle = itemView.findViewById(R.id.txt_sub_title);
        txtEventName = itemView.findViewById(R.id.txt_event_name);
        txtInviteDate = itemView.findViewById(R.id.txt_date);
        txtInviteStatus = itemView.findViewById(R.id.txt_invite_status);

        view.setOnClickListener(v -> inviteItemPresenter.onInviteClick());
    }

    void bindData(InviteData item, RequestOptions inviteIcoRequestOptions, InvitingListAdapter.OnInviteItemClickListener inviteItemClickListener) {
        this.inviteItemClickListener = inviteItemClickListener;
        inviteItemPresenter = new InviteItemPresenter(this, inviteIcoRequestOptions, item);
    }

    @SuppressLint("CheckResult")
    @Override
    public void setBasicData(InviteData inviteData, ProfileData profile, EventInfo event, boolean isMyInvite, RequestOptions inviteIcoRequestOptions) {
        Resources res = txtInviteStatus.getResources();
        txtUserName.setText(profile.login.trim());
        txtEventName.setText(event.title);
        String eventImgUrl = "";
        List<Media> actualMediaList = event.getActualMediaList();
        if (actualMediaList != null && !event.getActualMediaList().isEmpty()) {
            Media media = event.getActualMediaList().get(0);
            eventImgUrl = media.type == Media.Type.PHOTO ? media.photoThumbnail2xUrl : media.videoImagePreviewUrl;
        }
        String bigImgUrl = isMyInvite ? profile.primaryPhoto.avatar : eventImgUrl;
        GlideApp.with(imgBig)
                .load(bigImgUrl)
                .placeholder(RequestOptionsFactory.getNonAvatar(profile))
                .apply(RequestOptions.circleCropTransform())
                .into(imgBig);
        String smallImgUrl = isMyInvite ? eventImgUrl : profile.primaryPhoto.avatar;
        GlideApp.with(imgSmall)
                .load(smallImgUrl)
                .apply(inviteIcoRequestOptions)
                .placeholder(RequestOptionsFactory.getNonAvatar(profile))
                .into(imgSmall);
        Date inviteDate = event.eventStartDate;
        String date = String.format("%s", Patterns.DATE_FORMAT_SHORT.format(inviteDate));
        String time = Patterns.TIME_FORMAT.format(inviteDate);
        txtInviteDate.setText(res.getString(R.string.two_string_template, date, time));

        txtFirstTitle.setVisibility(isMyInvite ? View.VISIBLE : View.GONE);
        txtTo.setVisibility(isMyInvite ? View.VISIBLE : View.GONE);
        txtSubTitle.setVisibility(isMyInvite ? View.GONE : View.VISIBLE);
        if (isMyInvite) {
            txtFirstTitle.setText(inviteData.status == 0 ? res.getString(R.string.you_invited) :
                    res.getString(R.string.you_invitation_for));
        } else {
            txtSubTitle.setText(inviteData.status == 0 ? res.getString(R.string.invited_you_to) :
                    res.getString(R.string.invitation_to));
        }
        switch (inviteData.status) {
            case InviteData.ACCEPT_INVITE_INDEX:
                txtInviteStatus.setText(res.getString(R.string.accepted));
                txtInviteStatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.accepted_circle_indicator_drawable, 0);
                break;
            case InviteData.REJECT_INVITE_INDEX:
            case InviteData.USER_REJECT_SENDING_INVITE_INDEX:
                txtInviteStatus.setText(res.getString(R.string.refused));
                txtInviteStatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.refused_circle_indicator_drawable, 0);
                break;
            default:
                txtInviteStatus.setText(res.getString(R.string.expires_in,inviteData.expireDate));
                txtInviteStatus.setTextColor(res.getColor(R.color.colorSecondary));
                txtInviteStatus.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                break;
        }
    }

    @Override
    public void onInviteClick(InviteData inviteData, ProfileData profile, EventInfo eventInfo) {
        if (inviteItemClickListener != null
                && inviteData.status != InviteData.EXPIRED_INVITE) {
            inviteItemClickListener.onInviteClick(inviteData.inviteId, profile.id, eventInfo.eventId);
        }
    }
}