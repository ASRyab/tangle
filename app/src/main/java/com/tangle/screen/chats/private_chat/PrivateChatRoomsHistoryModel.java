package com.tangle.screen.chats.private_chat;

import android.text.TextUtils;

import com.tangle.api.rx_tasks.ObservableTask;
import com.tangle.api.rx_tasks.msg.ChatHistory;
import com.tangle.api.rx_tasks.msg.HistoryType;
import com.tangle.model.chat.ChatHistoryData;

import io.reactivex.Observable;

public class PrivateChatRoomsHistoryModel extends ObservableTask<ChatHistoryData> {
    private final int offset;
    private final HistoryType historyType;
    private String userId;

    public PrivateChatRoomsHistoryModel(int offset, HistoryType historyType, String userId) {
        super(false);
        this.offset = offset;
        this.historyType = historyType;
        this.userId = userId;
    }

    @Override
    protected Observable<ChatHistoryData> getObservableTask() {
        if (TextUtils.isEmpty(userId)) {
            return new ChatHistory(offset, historyType).getDataTask();
        } else {
            return new ChatHistory(offset, historyType, userId).getDataTask();
        }
    }
}