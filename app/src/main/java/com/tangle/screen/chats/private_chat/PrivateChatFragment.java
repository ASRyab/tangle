package com.tangle.screen.chats.private_chat;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.R;
import com.tangle.api.rpc.rpc_actions.chat.MsgType;
import com.tangle.base.implementation.adapters.EndlessRecyclerViewScrollListener;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.listeners.SimpleTextWatcher;
import com.tangle.model.chat.MailMessagePhoenix;
import com.tangle.model.profile_data.Photo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.PlatformUtils;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;
import com.tangle.utils.own_user_photo.OwnPhotoPresenter;
import com.tangle.utils.own_user_photo.OwnPhotoView;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class PrivateChatFragment extends SecondaryScreenFragment implements PrivateChatView, OwnPhotoView {
    private final static String PRIVATE_CHAT_ROOM_ID_KEY = "private_chat_room_id_key";

    private ViewFlipper viewFlipper;
    private RecyclerView privateChatRVList, footerImgsRVList;
    private PrivateChatAdapter privateChatAdapter;
    private EditText msgEnterText;
    private ImageView btnSendMsg, imgUserPhoto, btnOpenGallery;
    private TextView txtUserName, txtUserAgeLocation;
    private LinearLayout emptyChatInformationLayout, privateChatFooterImgContainer;
    private TextView txtEmptyChatBody;

    private PrivateChatPresenter presenter = new PrivateChatPresenter();
    private OwnPhotoPresenter ownPhotopresenter = new OwnPhotoPresenter(this);
    private EndlessRecyclerViewScrollListener scrollListener;

    public static PrivateChatFragment newInstance(String privateChatRoomId) {
        PrivateChatFragment groupChatFragment = new PrivateChatFragment();
        Bundle args = new Bundle();
        args.putString(PRIVATE_CHAT_ROOM_ID_KEY, privateChatRoomId);
        groupChatFragment.setArguments(args);
        return groupChatFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getBundleData(getArguments());
    }

    private void getBundleData(Bundle bundle) {
        if (bundle != null) {
            presenter.setPrivateChatRoomId(bundle.getString(PRIVATE_CHAT_ROOM_ID_KEY));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_private_chat_room, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    @Override
    public boolean onBackPressed() {
        if (isFooterImgsContainerVisible()) {
            hideFooterImgContainer();
            return true;
        } else {
            PlatformUtils.Keyboard.hideKeyboard(getActivity());
            return super.onBackPressed();
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initUI(View view) {
        initSenderView(view);
        initToolbarView(view);
        initChatRV(view);

        emptyChatInformationLayout = view.findViewById(R.id.empty_chat_information_layout);
        txtEmptyChatBody = view.findViewById(R.id.txt_empty_chat_body_text);

        attachKeyboardListeners();
    }

    private void initChatRV(View view) {
        privateChatRVList = view.findViewById(R.id.private_chat_rview);
        Context context = privateChatRVList.getContext();
        privateChatAdapter = new PrivateChatAdapter();
        LinearLayoutManager layout = new LinearLayoutManager(context);
        layout.setReverseLayout(true);
        scrollListener = new EndlessRecyclerViewScrollListener(layout) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                loadNextDataFromApi(page);
            }
        };
        privateChatRVList.setLayoutManager(layout);
        privateChatRVList.setItemAnimator(new DefaultItemAnimator());
        privateChatRVList.setAdapter(privateChatAdapter);
        privateChatRVList.addOnScrollListener(scrollListener);
        privateChatRVList.setOnTouchListener((v, event) -> {
            if (isFooterImgsContainerVisible()) {
                hideFooterImgContainer();
            }
            return false;
        });
    }

    private void initToolbarView(View view) {
        imgUserPhoto = view.findViewById(R.id.img_user_photo);
        txtUserName = view.findViewById(R.id.txt_user_name);
        txtUserAgeLocation = view.findViewById(R.id.txt_user_age_location);
    }

    private void initSenderView(View view) {
        privateChatFooterImgContainer = view.findViewById(R.id.private_chat_footer_img_list_container);
        footerImgsRVList = view.findViewById(R.id.rview_footer_imgs);
        viewFlipper = view.findViewById(R.id.action_btn_container);
        view.findViewById(R.id.btn_camera).setOnClickListener(v -> ownPhotopresenter.onTakePictureClicked(this));
        view.findViewById(R.id.btn_right_arrow).setOnClickListener(v -> showGetMediaBtnContainer());
        btnOpenGallery = view.findViewById(R.id.btn_gallery);
        btnOpenGallery.setOnClickListener(v ->
                new RxPermissions(getActivity()).request(Manifest.permission.READ_EXTERNAL_STORAGE)
                        .subscribe(granted -> {
                            if (granted) {
                                if (isFooterImgsContainerVisible()) {
                                    hideFooterImgContainer();
                                } else {
                                    PlatformUtils.Keyboard.hideKeyboard(getActivity());
                                    presenter.loadImgsFromStorage(getActivity());
                                    showFooterImgContainer();
                                }
                            } else {
                                ownPhotopresenter.onGalleryPickClicked(this);
                            }
                        }));

        msgEnterText = view.findViewById(R.id.edt_msg_text);
        msgEnterText.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenter.checkMsg(s.toString());
            }
        });
        msgEnterText.setOnTouchListener((v, event) -> {
            if (isFooterImgsContainerVisible()) {
                hideFooterImgContainer();
            }
            return false;
        });
        btnSendMsg = view.findViewById(R.id.btn_send_msg);
        btnSendMsg.setOnClickListener(v -> {
            if (isFooterImgsContainerVisible()) {
                ownPhotopresenter.sendPhotoChat();
                hideFooterImgContainer();
            } else {
                presenter.sendMsg(msgEnterText.getText().toString(), MsgType.CHAT, null);
            }
        });
    }

    private void loadNextDataFromApi(int page) {
        presenter.loadMore(page);
    }

    @Override
    public void setImgsFromStorage(ArrayList<String> imgsData) {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(footerImgsRVList.getContext(), LinearLayoutManager.HORIZONTAL, false);
        footerImgsRVList.setLayoutManager(layoutManager);
        footerImgsRVList.setHasFixedSize(true);
        footerImgsRVList.setAdapter(new PrivateChatFooterImgsAdapter(imgsData, photoData -> {
            ownPhotopresenter.onPrivateChatFooterImgsChoose(photoData);
            enableSendMsgBtn(true);
        }));
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void addMsgs(List<MailMessagePhoenix> chatRoomMsgs) {
        privateChatAdapter.addPrivateChatsMsgs(chatRoomMsgs);
        emptyChatInformationLayout.setVisibility(View.GONE);
    }

    @Override
    public void addMsgsFirst(List<MailMessagePhoenix> chatRoomMsg) {
        privateChatAdapter.addPrivateChatsMsgsFirst(chatRoomMsg);
        emptyChatInformationLayout.setVisibility(View.GONE);
    }

    @Override
    public void showEmptyInfoLayout(String bodyText) {
        txtEmptyChatBody.setText(bodyText);
        emptyChatInformationLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void enableSendMsgBtn(boolean enable) {
        btnSendMsg.setEnabled(enable);
        btnSendMsg.setImageResource(enable ? R.drawable.ic_chat_send_active : R.drawable.ic_chat_send_inactive);
    }

    public void showEnterTextIndicator(boolean isTextEnter) {
        int childIndex = viewFlipper.indexOfChild(viewFlipper.getCurrentView());
        if (isTextEnter) {
            if (childIndex == 0) {
                showGetMediaBtnContainer();
            }
        } else {
            if (childIndex == 1) {
                showCircleArrowIndicator();
            }
        }
    }

    private void showGetMediaBtnContainer() {
        Context context = viewFlipper.getContext();
        viewFlipper.setInAnimation(context, R.anim.slide_in_from_left);
        viewFlipper.setOutAnimation(context, R.anim.slide_out_to_right);
        viewFlipper.showNext();
    }

    private void showCircleArrowIndicator() {
        Context context = viewFlipper.getContext();
        viewFlipper.setInAnimation(context, R.anim.slide_in_from_right);
        viewFlipper.setOutAnimation(context, R.anim.slide_out_to_left);
        viewFlipper.showPrevious();
    }

    @Override
    public void cleanEdtMsgView() {
        msgEnterText.setText("");
    }

    @Override
    public void initProfileToolbar(ProfileData profileData) {
        txtUserName.setText(profileData.login != null ? profileData.login : "");
        txtUserAgeLocation.setText(txtUserAgeLocation.getContext().getString(R.string.format_age_country, profileData.age, profileData.geo.city));
        Photo primaryPhoto = profileData.primaryPhoto;
        if (primaryPhoto != null && primaryPhoto.avatar != null) {
            GlideApp.with(this)
                    .load(primaryPhoto.avatar)
                    .placeholder(RequestOptionsFactory.getNonAvatar(profileData))
                    .apply(RequestOptions.circleCropTransform())
                    .into(imgUserPhoto);
        }
    }

    @Override
    public void setError(String error) {
        showAPIError(error);
    }

    @Override
    public void setCurrentImage(File photoFile) {
        presenter.uploadNewPhoto(photoFile);
    }

    @Override
    public void uploadAvailablePhoto(File photoFile) {
    }

    @Override
    public void showUploadDialog(OwnPhotoPresenter presenter) {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ownPhotopresenter.onActivityResult(requestCode, resultCode, data);
    }

    private void showFooterImgContainer() {
        btnOpenGallery.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ic_image_yellow));
        privateChatFooterImgContainer.animate()
                .alpha(1.0f)
                .setDuration(250)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        privateChatFooterImgContainer.setVisibility(View.VISIBLE);
                    }
                });
    }

    private void hideFooterImgContainer() {
        presenter.checkMsg(msgEnterText.getText().toString().trim());
        ownPhotopresenter.onPrivateChatFooterImgsChoose(null);
        btnOpenGallery.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.ic_image_gray));
        privateChatFooterImgContainer.setVisibility(View.GONE);
    }

    private boolean isFooterImgsContainerVisible() {
        return privateChatFooterImgContainer.getVisibility() == View.VISIBLE;
    }

    @Override
    public void actionShowKeyboard() {
        if (isFooterImgsContainerVisible()) {
            hideFooterImgContainer();
        }
    }

    @Override
    public void actionHideKeyboard() {
    }

    @Override
    public boolean isNeedListenKeyboardState() {
        return true;
    }
}