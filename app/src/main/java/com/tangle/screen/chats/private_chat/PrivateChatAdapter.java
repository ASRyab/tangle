package com.tangle.screen.chats.private_chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.model.chat.MailMessagePhoenix;
import com.tangle.utils.ConvertingUtils;
import com.tangle.utils.NoDuplicatesList;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class PrivateChatAdapter extends RecyclerView.Adapter {
    private static final String EMPTY_DATA = "EMPTY DATA";

    private List<MailMessagePhoenix> privateChatRoomMsgs = new ArrayList<>();
    public NoDuplicatesList<MailMessagePhoenix> originPrivateChatRoomMsgs = new NoDuplicatesList<>();

    private static final int TYPE_ITEM_USER_MSG = 2;
    private static final int TYPE_ITEM_MY_MSG = 3;
    private static final int TYPE_ITEM_TIME_SEPARATOR = 4;
    private static final int TYPE_ITEM_USER_IMG_MSG = 5;
    private static final int TYPE_ITEM_MY_IMG_MSG = 6;

    private String myUserId;

    public PrivateChatAdapter() {
        this.privateChatRoomMsgs.clear();
        this.myUserId = ApplicationLoader.getApplicationInstance().getPreferenceManager().getCurrentUserId();
    }

    public void addPrivateChatsMsgsFirst(List<MailMessagePhoenix> chatRoomMsgs) {
        this.originPrivateChatRoomMsgs.addAll(0,chatRoomMsgs);
        sortAndShowItems();
    }

    public void addPrivateChatsMsgs(List<MailMessagePhoenix> chatRoomMsgs) {
        this.originPrivateChatRoomMsgs.addAll(chatRoomMsgs);
        sortAndShowItems();
    }

    private void sortAndShowItems() {
        this.privateChatRoomMsgs.clear();
        this.privateChatRoomMsgs.addAll(filterMsgsByDate(this.originPrivateChatRoomMsgs));
        notifyDataSetChanged();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        RecyclerView.ViewHolder viewHolder = null;
        switch (viewType) {
            case TYPE_ITEM_USER_MSG:
                viewHolder = new UserMsgItemViewHolder(View.inflate(context, R.layout.item_chat_user_msg, null));
                break;
            case TYPE_ITEM_MY_MSG:
                viewHolder = new MyMsgItemViewHolder(View.inflate(context, R.layout.item_chat_my_msg, null));
                break;
            case TYPE_ITEM_TIME_SEPARATOR:
                viewHolder = new TimeSeparatorViewHolder(View.inflate(context, R.layout.item_chat_time_separator, null));
                break;
            case TYPE_ITEM_USER_IMG_MSG:
                viewHolder = new UserMsgItemViewHolder(View.inflate(context, R.layout.item_chat_user_img_msg, null));
                break;
            case TYPE_ITEM_MY_IMG_MSG:
                viewHolder = new MyMsgItemViewHolder(View.inflate(context, R.layout.item_chat_my_img_msg, null));
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        MailMessagePhoenix lastMailMessagePhoenix = privateChatRoomMsgs.get(position);
        switch (viewHolder.getItemViewType()) {
            case TYPE_ITEM_USER_MSG:
                UserMsgItemViewHolder userMsgItemViewHolder = (UserMsgItemViewHolder) viewHolder;
                userMsgItemViewHolder.bindData(lastMailMessagePhoenix);
                break;
            case TYPE_ITEM_MY_MSG:
                MyMsgItemViewHolder myMsgItemViewHolder = (MyMsgItemViewHolder) viewHolder;
                myMsgItemViewHolder.bindData(lastMailMessagePhoenix);
                break;
            case TYPE_ITEM_TIME_SEPARATOR:
                TimeSeparatorViewHolder timeSeparatorViewHolder = (TimeSeparatorViewHolder) viewHolder;
                timeSeparatorViewHolder.bindData(lastMailMessagePhoenix);
                break;
            case TYPE_ITEM_USER_IMG_MSG:
                UserMsgItemViewHolder userMsgItemImgViewHolder = (UserMsgItemViewHolder) viewHolder;
                userMsgItemImgViewHolder.bindImgData(lastMailMessagePhoenix);
                break;
            case TYPE_ITEM_MY_IMG_MSG:
                MyMsgItemViewHolder myMsgItemImgViewHolder = (MyMsgItemViewHolder) viewHolder;
                myMsgItemImgViewHolder.bindImgData(lastMailMessagePhoenix);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        MailMessagePhoenix mailMessagePhoenix = privateChatRoomMsgs.get(position);
        if (mailMessagePhoenix.isSeparator) {
            return TYPE_ITEM_TIME_SEPARATOR;
        } else if (mailMessagePhoenix.getSenderId().equals(myUserId)) {
            if (mailMessagePhoenix.isChatImg()) {
                return TYPE_ITEM_MY_IMG_MSG;
            } else {
                return TYPE_ITEM_MY_MSG;
            }
        } else {
            if (mailMessagePhoenix.isChatImg()) {
                return TYPE_ITEM_USER_IMG_MSG;
            } else {
                return TYPE_ITEM_USER_MSG;
            }
        }
    }

    public class UserMsgItemViewHolder extends RecyclerView.ViewHolder {
        private TextView txtViewUserMsg, txtViewUserMsgTime;
        private ImageView viewMsgImg;

        UserMsgItemViewHolder(View view) {
            super(view);
            txtViewUserMsg = itemView.findViewById(R.id.txt_user_msg);
            txtViewUserMsgTime = itemView.findViewById(R.id.txt_msg_time);
            viewMsgImg = itemView.findViewById(R.id.img_msg);
        }

        void bindData(MailMessagePhoenix mailMessagePhoenix) {
            txtViewUserMsg.setText(mailMessagePhoenix.message);
            setMsgTime(txtViewUserMsgTime, mailMessagePhoenix);
        }

        void bindImgData(MailMessagePhoenix mailMessagePhoenix) {
            setMsgImg(viewMsgImg, mailMessagePhoenix);
            setMsgTime(txtViewUserMsgTime, mailMessagePhoenix);
        }
    }

    public class MyMsgItemViewHolder extends RecyclerView.ViewHolder {
        private TextView txtViewMyMsg, txtViewMyMsgTime;
        private ImageView viewMsgImg;

        MyMsgItemViewHolder(View view) {
            super(view);
            txtViewMyMsg = itemView.findViewById(R.id.txt_my_msg);
            txtViewMyMsgTime = itemView.findViewById(R.id.txt_my_msg_time);
            viewMsgImg = itemView.findViewById(R.id.img_msg);
        }

        void bindData(MailMessagePhoenix mailMessagePhoenix) {
            txtViewMyMsg.setText(mailMessagePhoenix.message);
            setMsgTime(txtViewMyMsgTime, mailMessagePhoenix);
        }

        void bindImgData(MailMessagePhoenix mailMessagePhoenix) {
            setMsgImg(viewMsgImg, mailMessagePhoenix);
            setMsgTime(txtViewMyMsgTime, mailMessagePhoenix);
        }
    }

    private void setMsgImg(ImageView viewMsgImg, MailMessagePhoenix mailMessagePhoenix) {
        GlideApp.with(viewMsgImg)
                .load(mailMessagePhoenix.resources.getImage().fullSize)
                .apply(RequestOptions.priorityOf(Priority.HIGH))
                .apply(RequestOptionsFactory.getMediumRoundedCornersOptions(viewMsgImg.getContext()))
                .into(viewMsgImg);
    }

    private void setMsgTime(TextView txtViewMsgTime, MailMessagePhoenix mailMessagePhoenix) {
        Date msgDate = mailMessagePhoenix.getDateTime();
        if (msgDate != null) {
            String formattedDate = ConvertingUtils.getStringDateByPattern(msgDate, ConvertingUtils.DATE_FORMAT_HM);
            txtViewMsgTime.setText(formattedDate);
        } else {
            txtViewMsgTime.setText(EMPTY_DATA);
        }
    }

    public class TimeSeparatorViewHolder extends RecyclerView.ViewHolder {
        private TextView txtSeparatorTime;

        TimeSeparatorViewHolder(View v) {
            super(v);
            txtSeparatorTime = v.findViewById(R.id.txt_separator_time);
        }

        void bindData(MailMessagePhoenix mailMessagePhoenix) {
            txtSeparatorTime.setText(ConvertingUtils.getStringDateByPattern(mailMessagePhoenix.dateTime, ConvertingUtils.DATE_FORMAT_CHAT_SEPARATOR));
        }
    }

    @Override
    public int getItemCount() {
        return privateChatRoomMsgs.size();
    }

    private List<MailMessagePhoenix> filterMsgsByDate(List<MailMessagePhoenix> msgs) {
        String lastHeader = null;
        LinkedList<MailMessagePhoenix> formattedMsgs = new LinkedList<>();
        for (int i = msgs.size() - 1; i >= 0; i--) {
            MailMessagePhoenix mailMessagePhoenix = msgs.get(i);
            Date msgDate = mailMessagePhoenix.getDateTime();
            String formattedDate = ConvertingUtils.getStringDateByPattern(msgDate,
                    ConvertingUtils.DATE_FORMAT_CHAT_SEPARATOR);
            if (lastHeader == null) {
                formattedMsgs.addFirst(new MailMessagePhoenix(msgDate, true));
                lastHeader = formattedDate;
            }
            if (formattedDate.equals(lastHeader)) {
                formattedMsgs.addFirst(mailMessagePhoenix);
            } else {
                formattedMsgs.addFirst(new MailMessagePhoenix(msgDate, true));
                lastHeader = formattedDate;
                formattedMsgs.addFirst(mailMessagePhoenix);
            }
        }
        return formattedMsgs;
    }
}