package com.tangle.screen.chats.private_chat;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Pair;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.Api;
import com.tangle.api.rpc.RPCResponse;
import com.tangle.api.rpc.rpc_actions.chat.MsgType;
import com.tangle.api.rpc.rpc_actions.chat.PrivateChatSendMsgAction;
import com.tangle.api.rx_tasks.msg.HistoryType;
import com.tangle.api.rx_tasks.photo.UploadPhotoModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.UploadPhotoData;
import com.tangle.model.chat.ImbImage;
import com.tangle.model.chat.ImbImageResources;
import com.tangle.model.chat.MailMessagePhoenix;
import com.tangle.utils.NoDuplicatesList;
import com.tangle.utils.RxUtils;
import com.tangle.utils.photo_compressor.ImageUtil;

import java.io.File;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PrivateChatPresenter extends LifecyclePresenter<PrivateChatView> {
    private String privateChatRoomId;
    private NoDuplicatesList<MailMessagePhoenix> privateChatRoomMsgs = new NoDuplicatesList<>();

    public void setPrivateChatRoomId(String privateChatRoomId) {
        this.privateChatRoomMsgs.clear();
        this.privateChatRoomId = privateChatRoomId;
    }

    @Override
    public void attachToView(PrivateChatView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        setToolbarData();
        loadPrivateChatRoomMsgs();
    }

    private void setToolbarData() {
        Disposable disposable = ApplicationLoader.getApplicationInstance().getUserManager().getUserById(privateChatRoomId)
                .subscribe(profileData -> getView().initProfileToolbar(profileData), RxUtils.getEmptyErrorConsumer(TAG, "setToolbarData"));
        monitor(disposable, State.DESTROY_VIEW);
    }

    private void loadPrivateChatRoomMsgs() {
        Disposable disposable = ApplicationLoader.getApplicationInstance().getPrivateChatMsgsManager()
                .getMsgsListByChatMateId(privateChatRoomMsgs.size(), HistoryType.UNDEFINED, privateChatRoomId).subscribe(msgs -> {
                    if (msgs.isEmpty() && privateChatRoomMsgs.isEmpty()) {
                        emptyMsgHandler();
                    } else {
                        if (privateChatRoomMsgs.addAll(msgs)) {
                            getView().addMsgs(privateChatRoomMsgs);
                        }
                    }
                }, error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    public void loadImgsFromStorage(Activity activity) {
        monitor(ImageUtil.getAllImagesObservable(activity, getView())
                .filter(strings -> !strings.isEmpty())
                .subscribe(imgsData -> getView().setImgsFromStorage(imgsData), RxUtils.getEmptyErrorConsumer(TAG, "loadImgsFromStorage")), State.DESTROY_VIEW);
    }

    private void emptyMsgHandler() {
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().getUserById(privateChatRoomId)
                .flatMap(profileData -> ApplicationLoader.getApplicationInstance().getEventManager()
                        .getEventById(profileData.matchedEvent)
                        .map(eventInfo -> Pair.create(profileData, eventInfo)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(pair -> getView().showEmptyInfoLayout(getResources().getString(R.string.formatted_can_discuss_body,
                        pair.first.login, pair.second.title)),
                        RxUtils.getEmptyErrorConsumer(TAG)), State.DESTROY_VIEW);
    }

    @Override
    public void onStart() {
        super.onStart();
        markChatMsgAsRead();
        subscribeOnUpdate();
        ApplicationLoader.getApplicationInstance().getNotificationViewManager().setChatId(privateChatRoomId);
    }

    @Override
    public void onStop() {
        super.onStop();
        markChatMsgAsRead();
        ApplicationLoader.getApplicationInstance().getNotificationViewManager().setChatId(null);
    }

    private void markChatMsgAsRead() {
        ApplicationLoader.getApplicationInstance().getPrivateChatMsgsManager().setAllAsReadByChatMateId(privateChatRoomId);
    }

    private void subscribeOnUpdate() {
        Disposable disposable = ApplicationLoader.getApplicationInstance().getPrivateChatMsgsManager().getUpdateRooms()
                .flatMap(chatRoom -> Observable.fromIterable(chatRoom.roomMsg)
                        .filter(phoenix -> !phoenix.isSystemMsg())
                        .toList()
                        .toObservable()
                        .map(list -> {
                            NoDuplicatesList<MailMessagePhoenix> phoenixes = new NoDuplicatesList<>();
                            phoenixes.addAll(list);
                            chatRoom.roomMsg = phoenixes;
                            return chatRoom;
                        }))
                .subscribe(chatHistoryData -> {
                    if (chatHistoryData.chatId.equals(privateChatRoomId)) {
                        if (privateChatRoomMsgs.addAll(0, chatHistoryData.roomMsg)) {
                            getView().addMsgsFirst(chatHistoryData.roomMsg);
                        }
                    }
                }, RxUtils.getEmptyErrorConsumer(TAG));
        monitor(disposable, State.STOP);
    }

    public void checkMsg(String msg) {
        boolean enable = !TextUtils.isEmpty(msg.trim());
        getView().enableSendMsgBtn(enable);
        getView().showEnterTextIndicator(enable);
    }

    public void sendMsg(String msg, MsgType msgType, ImbImageResources resources) {
        if (TextUtils.isEmpty(msg)) {
            return;
        }
        monitor(Api.getInst().getSocketManager().executeRPCAction(new PrivateChatSendMsgAction(privateChatRoomId, msg, msgType))
                .doOnSubscribe(__ -> getView().enableSendMsgBtn(false))
                .doOnEach(__ -> getView().enableSendMsgBtn(true))
                .map(RPCResponse::getResult)
                .map(phoenix -> phoenix.unify(privateChatRoomId))
                .map(phoenix -> {
                    phoenix.read = true;
                    return phoenix;
                })
                .doOnNext(phoenix -> {
                    if (resources != null) {
                        phoenix.resources = resources;
                    }
                })
                .doOnNext(phoenix -> ApplicationLoader.getApplicationInstance().getPrivateChatMsgsManager().addMessage(phoenix))
                .subscribe(chatRPCResponse -> {
                    if (msgType.equals(MsgType.CHAT)) {
                        getView().cleanEdtMsgView();
                    }
                }, error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
    }

    public void uploadNewPhoto(File photoFile) {
        Disposable disposable = new UploadPhotoModel(photoFile, true).getTask()
                .compose(RxUtils.withLoading(getView()))
                .subscribe(uploadPhoto -> sendMsg(uploadPhoto.id, MsgType.IMG_MSG, getMessageResources(uploadPhoto)),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    private ImbImageResources getMessageResources(UploadPhotoData photo) {
        ImbImageResources resources = new ImbImageResources();
        ImbImage image = new ImbImage();
        image.avatar = photo.avatar;
        image.fullSize = photo.normal;
        resources.imbImage.add(image);
        return resources;
    }

    public void loadMore(int page) {
        loadPrivateChatRoomMsgs();
    }
}