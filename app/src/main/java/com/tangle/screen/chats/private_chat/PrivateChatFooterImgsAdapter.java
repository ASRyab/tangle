package com.tangle.screen.chats.private_chat;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.tangle.R;
import com.tangle.utils.PlatformUtils;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.blur.BlurTransformation;

import java.util.ArrayList;

public class PrivateChatFooterImgsAdapter extends RecyclerView.Adapter<PrivateChatFooterImgsAdapter.ImgViewHolder> {
    private PhotoClickListener photoClickListener;
    private ArrayList<String> imgsUris;
    private SparseBooleanArray array;

    public PrivateChatFooterImgsAdapter(ArrayList<String> imgsUris, PhotoClickListener photoClickListener) {
        this.imgsUris = imgsUris;
        this.photoClickListener = photoClickListener;
        array = new SparseBooleanArray(imgsUris.size());
    }

    @Override
    public ImgViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ImgViewHolder(LayoutInflater.from(parent.getContext()).inflate(getLayoutId(), parent, false));
    }

    public int getLayoutId() {
        return R.layout.item_private_chat_footer_img;
    }

    @Override
    public void onBindViewHolder(ImgViewHolder holder, int position) {
        holder.bind(imgsUris.get(position), position);
    }

    @Override
    public int getItemCount() {
        return imgsUris == null ? 0 : imgsUris.size();
    }

    class ImgViewHolder extends RecyclerView.ViewHolder {
        private ImageView imgView, imgCheck, imgViewBlur;

        private ImgViewHolder(View itemView) {
            super(itemView);
            imgView = itemView.findViewById(R.id.img_view);
            imgCheck = itemView.findViewById(R.id.img_check);
            imgViewBlur = itemView.findViewById(R.id.img_view_blur);
        }

        private void bind(String data, int position) {
            GlideApp.with(imgView)
                    .load(data)
                    .centerCrop()
                    .into(imgView);
            Context context = imgViewBlur.getContext();
            GlideApp.with(imgViewBlur)
                    .load(data)
                    .transforms(new BlurTransformation(context,
                            (int) PlatformUtils.Dimensions.dipToPixels(context, 8), 2), new CenterCrop())
                    .into(imgViewBlur);
            if (array.get(position)) {
                setCheckVisibility(View.VISIBLE);
            } else {
                setCheckVisibility(View.GONE);
            }
            imgView.setOnClickListener(v -> {
                setCheckVisibility(View.VISIBLE);
                if (photoClickListener != null) {
                    photoClickListener.onPhotoClick(data);
                    setChecked(position);
                }
            });
        }

        private void setCheckVisibility(int visibility) {
            imgCheck.setVisibility(visibility);
            imgViewBlur.setVisibility(visibility);
        }
    }

    private void setChecked(int position) {

        int index = array.indexOfValue(true);
        if (index != -1) {
            array.put(array.keyAt(index), false);
            notifyItemChanged(array.keyAt(index));
        }
        array.put(position, true);
        notifyItemChanged(position);
    }

    public interface PhotoClickListener {
        void onPhotoClick(String photoData);
    }
}