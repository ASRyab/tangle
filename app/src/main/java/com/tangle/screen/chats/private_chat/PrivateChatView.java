package com.tangle.screen.chats.private_chat;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.chat.MailMessagePhoenix;
import com.tangle.model.profile_data.ProfileData;

import java.util.ArrayList;
import java.util.List;

public interface PrivateChatView extends LoadingView {
    void initProfileToolbar(ProfileData profileData);

    void setImgsFromStorage(ArrayList<String> imgsData);

    void addMsgs(List<MailMessagePhoenix> privateChatRoomMsgs);

    void addMsgsFirst(List<MailMessagePhoenix> privateChatRoomMsgs);

    void showEmptyInfoLayout(String bodyText);

    void enableSendMsgBtn(boolean enable);

    void showEnterTextIndicator(boolean isTextEnter);

    void cleanEdtMsgView();
}