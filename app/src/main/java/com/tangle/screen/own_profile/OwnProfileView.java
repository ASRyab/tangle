package com.tangle.screen.own_profile;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.event.EventInfo;
import com.tangle.model.pojos.DictionaryItem;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.choose.ShowedEventsType;

import java.util.List;

public interface OwnProfileView extends LoadingView {

    void setOwnEvents(List<EventInfo> events);

    void setPastEvents(List<EventInfo> events);

    void setName(String name);

    void setLocation(String text);

    void setCareer(String text);

    void setAvatarPhoto(ProfileData profileData);

    void setBackPhoto(String url);

    void setInterests(List<DictionaryItem> interests);

    void showFragmentByIndex(int fragmentIndex);

    void showTicket(String eventId);

    void showChooseEventFragment(String userId, ShowedEventsType showedEventsType);
}