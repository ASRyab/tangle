package com.tangle.screen.own_profile;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.rx_tasks.dictionary.GetCareersModel;
import com.tangle.api.rx_tasks.dictionary.GetIndustriesModel;
import com.tangle.api.rx_tasks.dictionary.GetInterestsModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.managers.DialogManager;
import com.tangle.managers.EventManager;
import com.tangle.model.event.EventInfo;
import com.tangle.model.pojos.DictionaryItem;
import com.tangle.model.profile_data.Photo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.EventType;
import com.tangle.screen.events.choose.ShowedEventsType;
import com.tangle.utils.RxUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class OwnProfilePresenter extends LifecyclePresenter<OwnProfileView> {
    private static final String TAG = OwnProfilePresenter.class.getSimpleName();

    public static final int SETTINGS_FRAGMENT_INDEX = 1;
    public static final int EDIT_PROFILE_FRAGMENT_INDEX = 2;

    private ProfileData myProfile;
    private DialogManager dialogManager;

    @Override
    public void attachToView(OwnProfileView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().currentUserWithUpdates()
                .subscribe(this::setProfileData, error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
    }

    private void setProfileData(ProfileData profile) {
        this.myProfile = profile;
        OwnProfileView view = getView();
        monitor(ApplicationLoader.getApplicationInstance().getEventManager().getAll()
                .flatMap(infos -> Observable.fromIterable(infos)
                        .compose(EventManager.checkStatusEvent())
                        .filter(info -> info.hasTicket() || info.isReserved())
                        .map(info -> info.eventId)
                        .toList().toObservable())
                .subscribe(infos -> setEvents(infos, EventType.FUTURE), RxUtils.getEmptyErrorConsumer("setFutureEvents")), State.DESTROY_VIEW);
        setEvents(profile.pastEventsList, EventType.PAST);
        view.setName(profile.login);
        setCareer(profile);
        Resources resources = getResources();
        view.setLocation(resources.getString(R.string.profile_basic_info_template, profile.age, profile.geo.city, profile.geo.country_code));
        initMainPhoto(profile, view);
        if (profile.interests != null) {
            setInterests(profile.interests);
        }
    }

    private void initMainPhoto(ProfileData profile, OwnProfileView view) {
        Photo mainPhoto = profile.primaryPhoto;
        view.setAvatarPhoto(profile);
        if (mainPhoto != null) {
            view.setBackPhoto(mainPhoto.normal);
        }
    }

    private void setCareer(ProfileData profile) {
        Observable<DictionaryItem> industriesObservable = new GetIndustriesModel().getTask()
                .flatMapIterable(list -> list)
                .filter(industry -> profile.getIndustry().equals(Integer.toString(industry.id)));
        Observable<DictionaryItem> careersObservable = new GetCareersModel().getTask()
                .flatMapIterable(list -> list)
                .filter(career -> profile.getCareerLevel().equals(Integer.toString(career.id)));
        Observable<String> careerObservable = Observable.zip(industriesObservable, careersObservable,
                DictionaryItem.industryWithCareer());
        Disposable disposable = careerObservable.subscribe(
                careerInfo -> getView().setCareer(careerInfo),
                error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    private void setInterests(Map<String, Boolean> interests) {
        Set<String> ids = interests.keySet();
        Disposable disposable = new GetInterestsModel().getTask()
                .flatMapIterable(list -> list)
                .filter(item -> ids.contains(Integer.toString(item.id)))
                .toList()
                .subscribe(getView()::setInterests, error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    private void setEvents(List<String> eventIds, EventType eventType) {
        if (eventIds != null && !eventIds.isEmpty()) {
            monitor(ApplicationLoader.getApplicationInstance().getEventManager().getEventsByIds(eventIds)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(events -> setEventsToView(events, eventType),
                            error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
        } else {
            hideEvents(eventType);
        }
    }

    private void hideEvents(EventType eventType) {
        switch (eventType) {
            case PAST:
                getView().setPastEvents(null);
                break;
            case FUTURE:
                getView().setOwnEvents(null);
                break;
        }
    }

    private void setEventsToView(List<EventInfo> events, EventType eventType) {
        switch (eventType) {
            case PAST:
                getView().setPastEvents(events);
                break;
            case FUTURE:
                getView().setOwnEvents(events);
                break;
        }
    }

    void onPreferenceClick() {
        getView().showFragmentByIndex(SETTINGS_FRAGMENT_INDEX);
    }

    void onEditProfileClick() {
        getView().showFragmentByIndex(EDIT_PROFILE_FRAGMENT_INDEX);
    }

    void onBookClicked(EventInfo eventInfo) {
        if (eventInfo.isReserved()) {
            dialogManager.showConfirmTicket((v, dialog) -> {
                chargeTicket(eventInfo);
            }, null);
        } else {
            getView().showTicket(eventInfo.eventId);
        }
    }

    public void chargeTicket(EventInfo event) {
        monitor(EventManager.chargeTicket(event, getView())
                        .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "chargeTicket"))
                , State.DESTROY_VIEW);
    }

    public void onMyEventsHeaderClick() {
        getView().showChooseEventFragment(myProfile.id, ShowedEventsType.OWN_EVENTS);
    }

    public void onPastEventsHeaderClick() {
        getView().showChooseEventFragment(myProfile.id, ShowedEventsType.PAST_EVENTS);
    }

    public void setDialogManager(DialogManager manager) {
        this.dialogManager = manager;
    }
}