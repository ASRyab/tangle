package com.tangle.screen.own_profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.tangle.R;
import com.tangle.base.implementation.adapters.EventsAdapter;
import com.tangle.base.implementation.adapters.InterestsAdapter;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.recyclerview.decorations.SpaceItemDecoration;
import com.tangle.base.ui.recyclerview.layout_managers.flex.FlexDirection;
import com.tangle.base.ui.recyclerview.layout_managers.flex.FlexWrap;
import com.tangle.base.ui.recyclerview.layout_managers.flex.FlexboxLayoutManager;
import com.tangle.base.ui.recyclerview.layout_managers.flex.JustifyContent;
import com.tangle.base.ui.views.ShapeButton;
import com.tangle.managers.DialogManager;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.model.pojos.DictionaryItem;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.edit_own_profile.EditOwnProfileFragment;
import com.tangle.screen.events.EventActionButtonClickListener;
import com.tangle.screen.events.choose.ChooseEventFragment;
import com.tangle.screen.events.choose.ShowedEventsType;
import com.tangle.screen.events.details.EventDetailsFragment;
import com.tangle.screen.events.details.EventDetailsPresenter;
import com.tangle.screen.events.details.PastEventDetailsFragment;
import com.tangle.screen.events.ticket.EventTicketFragment;
import com.tangle.screen.other_profile.GoingToEventsAdapter;
import com.tangle.screen.settings.SettingsFragment;
import com.tangle.utils.EventUtils;
import com.tangle.utils.Patterns;
import com.tangle.utils.PlatformUtils;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.Date;
import java.util.List;

public class OwnProfileFragment extends SecondaryScreenFragment implements OwnProfileView {
    private OwnProfilePresenter presenter = new OwnProfilePresenter();

    private ImageView backgroundImageView;
    private ImageView avatarImageView;
    private TextView nameTextView;
    private TextView infoBasicTextView;
    private TextView specialityTextView;
    private RecyclerView pastEventsRecyclerView;
    private RecyclerView interestsRecyclerView;
    private TextView pastEventsAmountTextView;
    private EventsAdapter pastEventsAdapter;
    private InterestsAdapter interestsAdapter;
    private LinearLayout containerUserIsGoingSingle,
            containerUserIsGoingList, containerPastEvents;
    private View containerInterests;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setDialogManager(DialogManager.getInstance(this));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_own_profile, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        pastEventsRecyclerView = view.findViewById(R.id.list_events_past);
        interestsRecyclerView = view.findViewById(R.id.list_interests);
        pastEventsAmountTextView = view.findViewById(R.id.txt_events_past_amount);
        backgroundImageView = view.findViewById(R.id.img_background);
        avatarImageView = view.findViewById(R.id.img_avatar);
        nameTextView = view.findViewById(R.id.txt_name);
        infoBasicTextView = view.findViewById(R.id.txt_info_basic);
        specialityTextView = view.findViewById(R.id.txt_info_speciality);
        view.findViewById(R.id.btn_preferences).setOnClickListener(v -> presenter.onPreferenceClick());
        view.findViewById(R.id.btn_edit).setOnClickListener(v -> presenter.onEditProfileClick());
        pastEventsAmountTextView.setOnClickListener(v -> presenter.onPastEventsHeaderClick());
        initEventsContainers(view);
        initEvents();
        initInterests(view);
    }

    private void initEventsContainers(View view) {
        containerUserIsGoingSingle = view.findViewById(R.id.container_user_going_to_single);
        containerUserIsGoingList = view.findViewById(R.id.container_user_going_to_list);
        containerPastEvents = view.findViewById(R.id.container_events_past);
        ((TextView) containerUserIsGoingSingle.findViewById(R.id.txt_view_going_to)).setText(R.string.events_my);
        ((TextView) containerUserIsGoingList.findViewById(R.id.txt_view_going_to)).setText(R.string.events_my);
    }

    private void initInterests(View view) {
        containerInterests = view.findViewById(R.id.container_interests);
        FlexboxLayoutManager layoutManager = new FlexboxLayoutManager(getActivity(), FlexDirection.ROW, FlexWrap.WRAP);
        layoutManager.setJustifyContent(JustifyContent.CENTER);
        interestsRecyclerView.setLayoutManager(layoutManager);
        interestsAdapter = new InterestsAdapter();
        interestsAdapter.setSelectAvailable(false);
        interestsRecyclerView.setAdapter(interestsAdapter);
    }

    private void initEvents() {
        RecyclerView.ItemDecoration spaceDecoration = new SpaceItemDecoration(
                (int) PlatformUtils.Dimensions.dipToPixels(getView().getContext(), 11),
                (int) PlatformUtils.Dimensions.dipToPixels(getView().getContext(), 24), LinearLayoutManager.HORIZONTAL);
        initSingleList(pastEventsRecyclerView, spaceDecoration);
        pastEventsAdapter = (EventsAdapter) pastEventsRecyclerView.getAdapter();
        pastEventsAdapter.setEventClickListener(event -> addFragment(getEventFragment(event), true));
    }

    private void initSingleList(RecyclerView list, RecyclerView.ItemDecoration decoration) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(list.getContext(), LinearLayoutManager.HORIZONTAL, false);
        list.setLayoutManager(layoutManager);
        list.setHasFixedSize(true);
        list.addItemDecoration(decoration);
        list.setAdapter(new EventsAdapter());
    }

    private Fragment getEventFragment(EventInfo event) {
        return event.hasPassed() ? PastEventDetailsFragment.newInstance(event) : EventDetailsFragment.newInstance(event);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setOwnEvents(List<EventInfo> events) {
        if (events == null || events.isEmpty()) {
            containerUserIsGoingList.setVisibility(View.GONE);
            containerUserIsGoingSingle.setVisibility(View.GONE);
        } else if (events.size() == 1) {
            showSingleOwnEvent(events.get(0));
        } else {
            showListOwnEvents(events);
        }
    }

    public void showSingleOwnEvent(EventInfo event) {
        containerUserIsGoingSingle.setVisibility(View.VISIBLE);
        containerUserIsGoingSingle.setOnClickListener(v -> addFragment(EventDetailsFragment.newInstance(event, EventDetailsPresenter.GoingFrom.OTHER), true));
        ImageView eventImg = containerUserIsGoingSingle.findViewById(R.id.ivEvent);
        List<Media> actualMediaList = event.getActualMediaList();
        if (actualMediaList != null && !actualMediaList.isEmpty()) {
            Media media = event.getActualMediaList().get(0);
            GlideApp.with(eventImg)
                    .load(media.type == Media.Type.PHOTO ? media.photoThumbnail2xUrl : media.videoImagePreviewUrl)
                    .apply(RequestOptionsFactory.getSmallRoundedCornersOptions(eventImg.getContext()))
                    .into(eventImg);
        }
        ((TextView) containerUserIsGoingSingle.findViewById(R.id.txt_will_go_people_counter))
                .setText(String.format(getResources().getString(R.string.events_participant), event.participantsUsers.size()));
        TextView eventDate = containerUserIsGoingSingle.findViewById(R.id.txt_event_date);

        TextView tvStatus = containerUserIsGoingSingle.findViewById(R.id.txt_status);
        EventUtils.updateEventStatus(tvStatus, event);

        Date launchDate = event.eventStartDate;
        String date;
        if (DateUtils.isToday(launchDate.getTime())) {
            date = eventDate.getResources().getString(R.string.own_profile_today);
        } else {
            date = String.format("%s", Patterns.DATE_FORMAT_SHORT.format(launchDate));
        }
        String time = Patterns.TIME_FORMAT.format(launchDate);
        eventDate.setText(getResources().getString(R.string.two_string_template, date, time));
        ((TextView) containerUserIsGoingSingle.findViewById(R.id.txt_event_title)).setText(event.title);
        ShapeButton btnBook = containerUserIsGoingSingle.findViewById(R.id.btn_book);
        btnBook.setText((event.isReserved())
                ? getResources().getString(R.string.confirm)
                : getResources().getString(R.string.show_ticket));
        btnBook.setOnClickListener(v -> presenter.onBookClicked(event));
    }

    public void showListOwnEvents(List<EventInfo> events) {
        containerUserIsGoingList.setVisibility(View.VISIBLE);
        ((TextView) containerUserIsGoingList.findViewById(R.id.txt_will_going_events_count)).setText(String.valueOf(events.size()));
        containerUserIsGoingList.findViewById(R.id.layout_header_user_is_going)
                .setOnClickListener(v -> presenter.onMyEventsHeaderClick());
        RecyclerView goingToEventRView = containerUserIsGoingList.findViewById(R.id.list_events_user_going_go);
        LinearLayoutManager layoutManager = new LinearLayoutManager(goingToEventRView.getContext(),
                LinearLayoutManager.HORIZONTAL, false);
        goingToEventRView.setLayoutManager(layoutManager);
        goingToEventRView.setHasFixedSize(true);
        goingToEventRView.setAdapter(new GoingToEventsAdapter(event ->
                addFragment(EventDetailsFragment.newInstance(event, EventDetailsPresenter.GoingFrom.OTHER), true), new EventActionButtonClickListener() {
            @Override
            public void onBookClicked(EventInfo event) {
                presenter.onBookClicked(event);
            }

            @Override
            public void onInviteClicked(EventInfo event) {
            }
        }, events, true));
    }

    @Override
    public void setPastEvents(List<EventInfo> events) {
        if (events != null && !events.isEmpty()) {
            containerPastEvents.setVisibility(View.VISIBLE);
            pastEventsAmountTextView.setText(String.valueOf(events.size()));
            pastEventsAdapter.switchEvents(events);
        } else {
            containerPastEvents.setVisibility(View.GONE);
        }
    }

    @Override
    public void setName(String name) {
        nameTextView.setText(name);
    }

    @Override
    public void setLocation(String text) {
        infoBasicTextView.setText(text);
    }

    @Override
    public void setCareer(String text) {
        if (!TextUtils.isEmpty(text)) {
            specialityTextView.setVisibility(View.VISIBLE);
        }
        specialityTextView.setText(text);
    }

    @Override
    public void setAvatarPhoto(ProfileData profileData) {
        GlideApp.with(this)
                .load(profileData.primaryPhoto.normal)
                .apply(RequestOptions.priorityOf(Priority.HIGH))
                .apply(RequestOptions.circleCropTransform())
                .placeholder(RequestOptionsFactory.getNonAvatar(profileData))
                .into(avatarImageView);
    }

    @Override
    public void setBackPhoto(String url) {
        GlideApp.with(this)
                .load(url)
                .apply(RequestOptions.priorityOf(Priority.HIGH))
                .apply(RequestOptionsFactory.getDefaultBlurOptions(getActivity()))
                .into(backgroundImageView);
    }

    @Override
    public void setInterests(List<DictionaryItem> interests) {
        if (!interests.isEmpty()) {
            containerInterests.setVisibility(View.VISIBLE);
        }
        interestsAdapter.switchInterests(interests);
        interestsAdapter.setSelectedInterests(interests);
    }

    @Override
    public void onStop() {
        super.onStop();
        PlatformUtils.Keyboard.hideKeyboard(getActivity());
    }

    @Override
    public void showFragmentByIndex(int fragmentIndex) {
        switch (fragmentIndex) {
            case OwnProfilePresenter.SETTINGS_FRAGMENT_INDEX:
                switchFragment(new SettingsFragment(), true);
                break;
            case OwnProfilePresenter.EDIT_PROFILE_FRAGMENT_INDEX:
                switchFragment(new EditOwnProfileFragment(), true);
                break;
        }
    }

    @Override
    public void showTicket(String eventId) {
        switchFragment(EventTicketFragment.newInstance(eventId), true);
    }

    @Override
    public void showChooseEventFragment(String userId, ShowedEventsType showedEventsType) {
        addFragment(ChooseEventFragment.newInstance(userId, showedEventsType, ChooseEventFragment.GoingFrom.OTHER), true);
    }
}