package com.tangle.screen.payment;

public interface PaymentActivityHolder {
    void setPaymentResult(int result, String eventId, String userId);
}
