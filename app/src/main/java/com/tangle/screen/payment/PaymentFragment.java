package com.tangle.screen.payment;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.analytics.SimpleActions;
import com.tangle.api.Api;
import com.tangle.api.utils.Config;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.MVPFragment;
import com.tangle.base.ui.interfaces.BackPressable;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.model.payment.EventInvoiceTransactionStatus;
import com.tangle.model.payment.EventInvoiceTransactionType;
import com.tangle.model.payment.InvoiceInfo;
import com.tangle.model.payment.PaymentData;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.Debug;
import com.tangle.utils.Patterns;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.HashMap;

public class PaymentFragment extends MVPFragment implements PaymentContract.View, BackPressable {
    private static final String TAG = PaymentFragment.class.getSimpleName();

    private static final String SUCCESS = "tangle://handlePaymentSuccess";
    private static final String FAILURE = "tangle://handlePaymentFailure";

    protected static final String DONE = "#done";

    private boolean isFirstKeyboardShow = true;
    private WebView webView;

    private ImageView imgBackground;
    private TextView txtTitle, txtAddress, txtDate, txtPrice, txtTicketFor, txtTicketOwn,
            txtPriceGuest,
            txtStatusGuest,
            txtStatusMy;
    private LinearLayout headerContainer, guestContainer, ownContainer;
    private RelativeLayout backgroundContainer;
    private PaymentPresenter presenter = new PaymentPresenter(new PaymentModel());
    private PaymentActivityHolder activityHolder;

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    public static PaymentFragment newInstant(Bundle args) {
        PaymentFragment fragment = new PaymentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getPaymentArgs();
    }

    private void getPaymentArgs() {
        PaymentData data = (PaymentData) getArguments().getSerializable(PaymentActivity.PAYMENT_ID_KEY);
        presenter.setData(data);
    }

    @Override
    protected void onAttachToContext(Context context) {
        super.onAttachToContext(context);
        activityHolder = (PaymentActivityHolder) context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_payment, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initView(view);
    }

    public void loadUrl(String url) {
        webView.loadUrl(url, createHeaderMapWithAuthorization());
    }

    @Override
    public void setResult(int result, String eventId, String userId) {
        activityHolder.setPaymentResult(result, eventId, userId);
    }

    @Override
    public void setBasicData(EventInfo event, PaymentData data) {
        if (event.getActualMediaList() != null && !event.getActualMediaList().isEmpty()) {
            Media media = event.getActualMediaList().get(0);
            GlideApp.with(this)
                    .load(media.type == Media.Type.PHOTO ? media.photoBig2xUrl : media.videoImagePreviewUrl)
                    .apply(RequestOptionsFactory.getDefaultBlurOptions(getActivity()))
                    .into(imgBackground);
        }
        txtTitle.setText(event.title);
        txtDate.setText(String.format("%s, %s",
                Patterns.DATE_FORMAT_LONG.format(event.eventStartDate),
                Patterns.TIME_FORMAT.format(event.eventStartDate)));
        txtAddress.setText(event.getEventAddress(getContext()));
    }

    @Override
    public void close() {
        getActivity().finish();
    }


    private HashMap<String, String> createHeadersMap() {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Content-Type",
                "application/x-www-form-urlencoded");
        map.put("App-Marker", "hand3856be45");
        return map;
    }

    private HashMap<String, String> createHeaderMapWithAuthorization() {
        HashMap<String, String> headersMap = createHeadersMap();
        headersMap.put("Authorization", "Bearer " + Api.getInst().getSession().getAccessToken());
        return headersMap;
    }

    private WebChromeClient chromeClient = new WebChromeClient() {

        @Override
        public boolean onConsoleMessage(
                android.webkit.ConsoleMessage consoleMessage) {

            Debug.logD(TAG, "onConsoleMessage: " + consoleMessage.message()
                    + " at line " + consoleMessage.lineNumber() + " sourceId="
                    + consoleMessage.sourceId());
            return super.onConsoleMessage(consoleMessage);
        }

        @Override
        public boolean onCreateWindow(WebView view, boolean dialog,
                                      boolean userGesture, android.os.Message resultMsg) {
            Debug.logD(TAG, "onCreateWindow: " + dialog + " " + resultMsg);
            return super.onCreateWindow(view, dialog, userGesture, resultMsg);
        }
    };

    private void initView(android.view.View view) {
        ((TextView) view.findViewById(R.id.txt_toolbar_title))
                .setText(getString(R.string.booking));
        view.findViewById(R.id.txt_toolbar_title).setVisibility(android.view.View.VISIBLE);
        view.findViewById(R.id.btn_back).setOnClickListener(v -> onBackPressed());
        imgBackground = view.findViewById(R.id.img_background);
        txtTitle = view.findViewById(R.id.tvTitle);
        txtAddress = view.findViewById(R.id.txt_address);
        txtDate = view.findViewById(R.id.txt_date);
        txtPrice = view.findViewById(R.id.txt_price_my);
        headerContainer = view.findViewById(R.id.header_container);
        backgroundContainer = view.findViewById(R.id.background_data_container);
        guestContainer = view.findViewById(R.id.container_guest);
        ownContainer = view.findViewById(R.id.container_own);
        txtTicketFor = view.findViewById(R.id.ticket_for);
        txtTicketOwn = view.findViewById(R.id.ticket_own);
        txtPriceGuest = view.findViewById(R.id.txt_price_guest);
        txtStatusGuest = view.findViewById(R.id.status_guest);
        txtStatusMy = view.findViewById(R.id.status_my);

        webView = view.findViewById(R.id.payment_web_view);
        webView.setWebChromeClient(chromeClient);
        WebSettings webViewSettings = webView.getSettings();
        webViewSettings.setJavaScriptEnabled(true);
        webViewSettings.setLoadWithOverviewMode(true);
        webViewSettings.setUseWideViewPort(true);

        if (Config.isLogging()) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        webView.requestFocus();
        webViewSettings.setUserAgentString(Api.getInst().getUserAgent());
        webView.setWebViewClient(webViewClient);
    }

    @Override
    public void setOtherUser(ProfileData info, PaymentData data, InvoiceInfo invoiceInfo) {
        guestContainer.setVisibility(android.view.View.VISIBLE);
        txtTicketFor.setText(getString(R.string.ticket_for_, info.login));
        txtPriceGuest.setText(data.getCurrencySymbol() + data.getTicketCost());
        txtStatusGuest.setText((!EventInvoiceTransactionStatus.Canceled.equals(invoiceInfo.getTransactionStatus())) ? getString(R.string.reserved)
                : getString(R.string.error));
    }

    @Override
    public void setOwnData(PaymentData data, InvoiceInfo invoiceInfo) {
        ownContainer.setVisibility(android.view.View.VISIBLE);
        int ticketsToBuyAmount = data.getTicketsToBuyAmount();
        if (ticketsToBuyAmount > 1 && data.getInviteUserId() == null) {
            txtTicketOwn.setText(getString(R.string.tickets_own, ticketsToBuyAmount));
            txtPrice.setText(data.getCurrencySymbol() + data.getTicketCost() + " x" + ticketsToBuyAmount);
        } else {
            txtTicketOwn.setText(getString(R.string.ticket_for_you));
            txtPrice.setText(data.getCurrencySymbol() + data.getTicketCost());
        }

        txtStatusMy.setText((EventInvoiceTransactionType.Hold.equals(invoiceInfo.getTransactionType()))
                ? getString(R.string.reserved)
                : getString(R.string.purchased));
        if (EventInvoiceTransactionStatus.Canceled.equals(invoiceInfo.getTransactionStatus())) {
            txtStatusMy.setText(getString(R.string.error));
        }
    }

    @Override
    public void showErrorText() {
        presenter.updateData();
    }

    @Override
    public boolean onBackPressed() {
        presenter.onBackPressed();
        return true;
    }

    private WebViewClient webViewClient = new WebViewClient() {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Debug.logD(TAG, "onPageStarted: " + url);
            super.onPageStarted(view, url, favicon);
        }

        /**
         * @return false - means load as usual, true - means we'll handle it
         */
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String urlStr) {
            Debug.logD(TAG, "shouldOverrideUrlLoading: " + urlStr);
            if (urlStr.contains(SUCCESS)) {
                onSuccess();
                return true;
            }
            if (urlStr.contains(FAILURE)) {
                onFailure();
            }
            if (urlStr.endsWith(DONE)) {
                Debug.logD(TAG, "Overriding DONE!");
                return true;
            }

            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            webView.setVisibility(View.VISIBLE);
            attachKeyboardListeners();
            super.onPageFinished(view, url);
            Debug.logD(TAG, "onPageFinished: " + url);
        }
    };

    private void onFailure() {
        Debug.logD(TAG, "onFailure: ");
        presenter.paymentResult(Activity.RESULT_CANCELED);
    }

    private void onSuccess() {
        Debug.logD(TAG, "Payment SUCCESS");
        presenter.paymentResult(Activity.RESULT_OK);
    }

    @Override
    public void actionShowKeyboard() {
        if (isFirstKeyboardShow) {
            ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.PAYMENT_TYPE_CARDNUMB);
            isFirstKeyboardShow = false;
        }
        headerContainer.setVisibility(View.GONE);
        backgroundContainer.setVisibility(View.GONE);

    }

    @Override
    public void actionHideKeyboard() {
        headerContainer.setVisibility(View.VISIBLE);
        backgroundContainer.setVisibility(View.VISIBLE);
    }

    @Override
    public void expiredTicket() {
        activityHolder.setPaymentResult(Activity.RESULT_CANCELED, null, null);
    }

    @Override
    public boolean isNeedListenKeyboardState() {
        return true;
    }
}