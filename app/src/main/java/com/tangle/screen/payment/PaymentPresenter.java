package com.tangle.screen.payment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.webkit.CookieManager;

import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.tangle.ApplicationLoader;
import com.tangle.analytics.PaymentPoints;
import com.tangle.analytics.SimpleActions;
import com.tangle.api.Api;
import com.tangle.api.utils.Config;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.event.EventInfo;
import com.tangle.model.payment.InvoiceInfo;
import com.tangle.model.payment.PaymentData;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.RxUtils;
import com.tangle.utils.Triple;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class PaymentPresenter extends LifecyclePresenter<PaymentContract.View> {
    private static final String TAG = PaymentPresenter.class.getSimpleName();
    public static final long PERIOD_EXPIRED_TICKET = 15;
    private PaymentContract.Model paymentModel;

    private PaymentData data;
    private Disposable count;


    public PaymentPresenter(PaymentContract.Model paymentModel) {
        this.paymentModel = paymentModel;
    }

    public void setData(PaymentData data) {
        this.data = data;
    }

    public void paymentResult(int paymentResult) {
        ApplicationLoader.getApplicationInstance().getAnalyticManager().trackVia(PaymentPoints.ON_TRY_PP);
        if (paymentResult == Activity.RESULT_OK) {
            getView().setResult(paymentResult, data.getEventId(), data.getInviteUserId());
            monitor(ApplicationLoader.getApplicationInstance().getUserManager()
                            .currentUser()
                            .filter(profileData -> !profileData.isTester && Config.isTracking())
                            .subscribe(profileData -> trackPaymentSuccess()
                                    , RxUtils.getEmptyErrorConsumer())
                    , State.DESTROY_VIEW);
            close();
        } else {
            getView().showErrorText();
        }
    }

    private void trackPaymentSuccess() {
        if (data == null) {
            return;
        }
        ApplicationLoader.getApplicationInstance().getAnalyticManager().trackVia(PaymentPoints.SUCCESS);
        ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.PAYMENT_SUCCESS);

        Map<String, Object> params = new HashMap();
        params.put(AFInAppEventParameterName.CONTENT_ID, data.getEventId());
        params.put(AFInAppEventParameterName.CURRENCY, data.getCurrencyCode());
        int totalPrice = data.getTicketsToBuyAmount() * data.getTicketCost();
        params.put(AFInAppEventParameterName.REVENUE, totalPrice);
        AppsFlyerLib.getInstance().trackEvent(getContext(), AFInAppEventType.PURCHASE, params);
    }

    @Override
    public void onActivityCreated(Bundle state) {
        super.onActivityCreated(state);
        ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.PAYMENT_CLICK_VISIT);
        ApplicationLoader.getApplicationInstance().getAnalyticManager().trackVia(PaymentPoints.OPEN_PP);
        updateData();
        if (count == null) {
            count = startCount().subscribeOn(AndroidSchedulers.mainThread()).subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer("startCount"));
            paymentModel.setTimeOfOpen(System.currentTimeMillis());
            monitor(count, State.DESTROY_VIEW);
        }
    }

    public void updateData() {
        ApplicationLoader.getApplicationInstance().getUserManager().loadCurrentUser();
        monitor(Observable.zip(
                paymentModel.infoInvoice(data.getInvoiceId())
                , paymentModel.getEvent(data.getEventId())
                , paymentModel.getInvitedUser(data.getInviteUserId())
                , Triple::create)
                .subscribe(triple -> {
                    showEvent(triple.b, triple.a);
                    showInvitedUser(triple.c, triple.a);
                }, RxUtils.getEmptyErrorConsumer(TAG, "updateData")), State.DESTROY_VIEW);
    }

    public void showInvitedUser(ProfileData info, List<InvoiceInfo> invoiceInfos) {
        if (!ProfileData.EMPTY_STATE.equals(info)) {
            getView().setOtherUser(info, data, invoiceInfos.get(invoiceInfos.size() - 1));
        }
    }

    @SuppressLint("CheckResult")
    public void showEvent(EventInfo eventInfo, List<InvoiceInfo> invoiceInfos) {
        getView().setBasicData(eventInfo, data);

        if ((!eventInfo.hasTicket() || data.getInviteUserId() == null) && eventInfo.isBookAvailable()) {
            Observable.fromIterable(invoiceInfos)
                    .filter(info ->
                            info.getTickets().get(0).getUserId().equals(
                                    paymentModel.getCurrentUserId()
                            ))
                    .subscribe(info -> getView().setOwnData(data, info), RxUtils.getEmptyErrorConsumer());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        loadPayUrl();
    }

    private void loadPayUrl() {
        CookieManager.getInstance().setAcceptCookie(true);
        CookieManager.getInstance().removeAllCookies(value -> {
            String cookieString = getCookieString();
            CookieManager.getInstance().setCookie(Config.BASE_PAYMENT_PATH, cookieString);
            CookieManager.getInstance().flush();
            String url = getPayUrl();
            getView().loadUrl(url);
        });
    }

    @NonNull
    private String getPayUrl() {
        return Config.BASE_PAYMENT_PATH +
                "?invoiceId=" + data.getInvoiceId();
    }

    @NonNull
    private String getCookieString() {
        return "PHPSESSID="
                + Api.getInst().getSession().getAccessToken()
                + "; path=/;";
    }

    public void onBackPressed() {
        monitor(paymentModel.undoInvoice(data)
                        .compose(RxUtils.withLoading(getView()))
                        .subscribe(result -> close(), RxUtils.getEmptyErrorConsumer(TAG, "CloseInvoise"))
                , State.DESTROY_VIEW);
    }

    public void close() {
        paymentModel.setTimeOfOpen(0);
        getView().close();
    }


    public Single startCount() {
        return Observable
                .interval(getPeriod(), TimeUnit.MINUTES)
                .firstOrError()
                .doOnSuccess(__ -> getView().expiredTicket())
                .doOnSuccess(__ -> close());
    }


    public long getPeriod() {
        long time = PERIOD_EXPIRED_TICKET - (paymentModel != null ? paymentModel.getTimeOfOpen() : 0);
        long result = time < 0 ? 0 : time;
        return result;
    }


}
