package com.tangle.screen.payment;

import android.content.Intent;
import android.os.Bundle;

import com.tangle.R;
import com.tangle.base.ui.BaseActivity;
import com.tangle.model.payment.PaymentData;
import com.tangle.screen.main.MainActivity;

public class PaymentActivity extends BaseActivity implements PaymentActivityHolder {
    public static final String PAYMENT_ID_KEY = "payment_key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        switchFragment(PaymentFragment.newInstant(getIntent().getExtras()), false);
    }

    @Override
    public void setPaymentResult(int result, String eventId, String userId) {
        Intent intent = new Intent();
        intent.putExtra(MainActivity.EVENT_PAYMENT_SCREEN, eventId);
        intent.putExtra(MainActivity.USER_PAYMENT_SCREEN, userId);
        setResult(result, intent);
    }

    public static void setExtras(Intent intent, PaymentData response) {
        Bundle bundle = new Bundle();
        bundle.putSerializable(PAYMENT_ID_KEY, response);
        intent.putExtras(bundle);
    }
}