package com.tangle.screen.payment

import com.tangle.ApplicationLoader
import com.tangle.api.rx_tasks.payment.CloseInvoice
import com.tangle.api.rx_tasks.payment.UpdateInvoiceInfo
import com.tangle.model.event.EventInfo
import com.tangle.model.payment.InvoiceInfo
import com.tangle.model.payment.PaymentActionResult
import com.tangle.model.payment.PaymentData
import com.tangle.model.profile_data.ProfileData
import io.reactivex.Observable
import java.util.concurrent.TimeUnit

class PaymentModel : PaymentContract.Model {
    override fun getCurrentUserId(): String {
        return ApplicationLoader.getApplicationInstance().userManager.currentUserId
    }

    override fun getInvitedUser(userId: String?): Observable<ProfileData> {
        return (if (userId != null)
            ApplicationLoader.getApplicationInstance().userManager.getUserById(userId)
        else
            Observable.empty<ProfileData>())
                .defaultIfEmpty(ProfileData.EMPTY_STATE)
    }

    override fun getEvent(eventId: String?): Observable<EventInfo> {
        return ApplicationLoader.getApplicationInstance().eventManager.getEventByIdAndUpdate(eventId)
    }

    override fun getTimeOfOpen(): Long {
        var time = ApplicationLoader.getApplicationInstance().preferenceManager.lastPaymentTime
        if (time != 0L) {
            time = (System.currentTimeMillis() - time) / TimeUnit.MINUTES.toMicros(1)
        }
        return time
    }

    override fun setTimeOfOpen(time: Long) {
        ApplicationLoader.getApplicationInstance().preferenceManager.lastPaymentTime = time
    }

    override fun undoInvoice(data: PaymentData): Observable<PaymentActionResult> {
        return CloseInvoice(data.invoiceId).dataTask
    }

    override fun infoInvoice(invoiceId: String): Observable<List<InvoiceInfo>> {
        return UpdateInvoiceInfo(invoiceId).dataTask
    }
}
