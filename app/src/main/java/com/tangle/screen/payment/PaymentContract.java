package com.tangle.screen.payment;

import com.tangle.base.ui.interfaces.AutoCloseable;
import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.event.EventInfo;
import com.tangle.model.payment.InvoiceInfo;
import com.tangle.model.payment.PaymentActionResult;
import com.tangle.model.payment.PaymentData;
import com.tangle.model.profile_data.ProfileData;

import java.util.List;

import io.reactivex.Observable;

public interface PaymentContract {
    interface View extends LoadingView, AutoCloseable {
        void loadUrl(String url);

        void setResult(int result, String eventId, String userId);

        void setBasicData(EventInfo eventInfo, PaymentData data);

        void expiredTicket();

        void setOtherUser(ProfileData info, PaymentData data, InvoiceInfo invoiceInfo);

        void setOwnData(PaymentData data, InvoiceInfo info);

        void showErrorText();
    }

    interface Model {

        long getTimeOfOpen();

        void setTimeOfOpen(long time);

        Observable<List<InvoiceInfo>> infoInvoice(String invoiceId);

        Observable<PaymentActionResult> undoInvoice(PaymentData invoiceId);

        Observable<ProfileData> getInvitedUser(String userId);

        Observable<EventInfo> getEvent(String eventId);

        String getCurrentUserId();
    }
}
