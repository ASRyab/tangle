package com.tangle.screen.web_browser;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.tangle.base.mvp.LifecyclePresenter;

public class WebBrowserPresenter extends LifecyclePresenter<WebBrowserView> {

    private String url;

    public void onBackPressed() {
        getView().processBack();
    }

    public void setUrl(String url) {
        this.url = url;
        if (!isViewDestroyed()) {
            getView().loadPage(url);
        }
    }

    @Override
    public void attachToView(WebBrowserView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (!TextUtils.isEmpty(url)) {
            getView().loadPage(url);
        }
    }
}
