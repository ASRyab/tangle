package com.tangle.screen.web_browser;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.utils.Debug;

public class WebBrowserFragment extends SecondaryScreenFragment implements WebBrowserView {

    private static final String KEY_URL = "url";
    private WebView webView;
    private WebBrowserPresenter presenter = new WebBrowserPresenter();

    public static WebBrowserFragment newInstance(String url) {
        Bundle args = new Bundle();
        args.putString(KEY_URL, url);
        WebBrowserFragment fragment = new WebBrowserFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setUrl(getArguments().getString(KEY_URL));
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_web_browser, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initToolbar("", true);
        view.findViewById(R.id.btn_back).setOnClickListener(v -> presenter.onBackPressed());
        webView = view.findViewById(R.id.webview);
        configureWebView();
    }

    private void configureWebView() {
        webView.setWebViewClient(webViewClient);
        webView.getSettings().setJavaScriptEnabled(true);
    }

    @Override
    public void loadPage(String url) {
        webView.loadUrl(url);
    }

    @Override
    public void processBack() {
        getActivity().onBackPressed();
    }

    private WebViewClient webViewClient = new WebViewClient() {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Debug.logD(TAG, "onPageStarted: " + url);
            super.onPageStarted(view, url, favicon);
        }

        /**
         * @return false - means load as usual, true - means we'll handle it
         */
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String urlStr) {
            Debug.logD(TAG, "shouldOverrideUrlLoading: " + urlStr);


            return false;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            webView.setVisibility(View.VISIBLE);
            super.onPageFinished(view, url);
            Debug.logD(TAG, "onPageFinished: " + url);
        }
    };
}
