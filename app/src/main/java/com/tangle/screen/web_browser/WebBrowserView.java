package com.tangle.screen.web_browser;

public interface WebBrowserView {

    void loadPage(String url);

    void processBack();

}
