package com.tangle.screen.media.video.promo;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.danikula.videocache.HttpProxyCacheServer;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.views.video_view.ScalableVideoView;

import java.io.IOException;

@SuppressLint("ValidFragment")
public abstract class PromoVideoFragment extends SecondaryScreenFragment {
    protected View.OnClickListener clickListener;
    private ScalableVideoView videoView;
    private HttpProxyCacheServer cacheProxy = ApplicationLoader.getApplicationInstance().getCacheProxy();
    private boolean initialized;

    public PromoVideoFragment(View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public PromoVideoFragment() {
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return null;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflateFragment(R.layout.fragment_promo_video, inflater, container);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        videoView = view.findViewById(R.id.view_video);
        if (clickListener != null) {
            videoView.setOnClickListener(clickListener);
        }
        processMedia();
    }

    protected View inflateFragment(int resId, LayoutInflater inflater, ViewGroup container) {
        return inflater.inflate(resId, container, false);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        videoView.stop();
        videoView.release();
    }

    public void processMedia() {
        try {
            String path = isFromResources() ? "android.resource://" + getContext().getPackageName() + "/" + getVideoRawSource()
                    : cacheProxy.getProxyUrl(ApplicationLoader.getApplicationInstance().getMediaManager().getGalleryPromoUrl());
            videoView.setIsBrandBlog(isBrandBlogVideo());
            if (path != null && !path.isEmpty() && !path.equals("0")) {
                Uri uri = Uri.parse(path);
                videoView.setDataSource(getContext(), uri);
                startVideo();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void startVideo() {
        videoView.setLooping(isVideoLooping());
        videoView.setOnCompletionListener(getOnCompletionListener());

        videoView.prepareAsync(mp -> {
            initialized = isAdded();
            if (initialized) {
                videoView.start();
            } else {
                if (videoView != null) {
                    videoView.stop();
                    videoView.release();
                }
            }
        });
    }

    protected boolean isBrandBlogVideo() {
        return false;
    }

    protected boolean isFromResources() {
        return true;
    }

    abstract MediaPlayer.OnCompletionListener getOnCompletionListener();

    abstract int getVideoRawSource();

    abstract boolean isVideoLooping();

}