package com.tangle.screen.media.photo;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.model.event.Media;
import com.tangle.screen.media.FullScreenFragment;
import com.tangle.screen.media.MediaView;
import com.tangle.screen.media.pager.MediaPager;
import com.tangle.utils.glide.GlideApp;

public class PhotoFragment extends FullScreenFragment implements PhotoView, MediaPager {
    private PhotoPresenter presenter = new PhotoPresenter();
    private ImageView photoIV;

    public static PhotoFragment newInstance(Media media) {
        Bundle args = new Bundle();
        args.putSerializable(MediaView.KEY_MEDIA, media);
        PhotoFragment fragment = new PhotoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Media media = (Media) getArguments().getSerializable(MediaView.KEY_MEDIA);
        presenter.setMedia(media);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_photo, container, false);
        v.setOnClickListener(getOnClickListener());
        return v;
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        photoIV = view.findViewById(R.id.img_photo);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void processMedia(Media media) {
        GlideApp.with(this)
                .load(media.photoBig2xUrl)
                .into(photoIV);
    }

    @Override
    public void setActive(boolean active) {

    }
}