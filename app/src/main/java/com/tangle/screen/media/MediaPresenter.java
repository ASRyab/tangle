package com.tangle.screen.media;

import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.model.event.Media;

public abstract class MediaPresenter<T> extends LifecyclePresenter<T> {

    public abstract void setMedia(Media media);

}
