package com.tangle.screen.media.video;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.model.event.Media;
import com.tangle.screen.media.MediaPresenter;

public class VideoPresenter extends MediaPresenter<VideoView> {

    private Media media;

    @Override
    public void attachToView(VideoView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (media != null) {
            view.processMedia(media);
        }
    }


    @Override
    public void setMedia(Media media) {
        this.media = media;
        if (!isViewDestroyed()) {
            getView().processMedia(media);
        }
    }
}
