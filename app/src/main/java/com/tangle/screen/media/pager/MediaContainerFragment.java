package com.tangle.screen.media.pager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.model.profile_data.Photo;
import com.tangle.screen.events.details.EventDetailsFragment;
import com.tangle.screen.events.map.BaseEventPresenter;
import com.tangle.screen.linked_events.LinkedEventBottomWindowOneAction;
import com.tangle.screen.media.FullScreenFragment;

import java.util.ArrayList;
import java.util.List;

public class MediaContainerFragment extends SecondaryScreenFragment implements MediaContainerView {
    public static final int SET_MEDIA_INDEX_CODE = 12;

    private static final String KEY_MEDIA_LIST = "media_list";
    private static final String KEY_POSITION = "start_position";
    private static final String KEY_SHOW_TYPE = "show_type";
    private static final String KEY_LINKED_EVENT_ID = "linked_event_id";
    private LinkedEventBottomWindowOneAction bottomWindow;
    private boolean isControlsVisible = true;
    private boolean isLinkedEventsEnabled = false;

    public enum ShowType {
        CHANGE_OWN_PHOTO,
        USER_PHOTO,
        PAST_EVENT,
        OTHER
    }

    private MediaContainerPresenter presenter = new MediaContainerPresenter();
    private ViewPager viewPager;
    private TextView counterTextView, photoCounterTextView, videoCounterTextView;
    private View btnClose;
    private FullScreenMediaPagerAdapter adapter;

    public static MediaContainerFragment newInstance(List<Media> medias, Media media, ShowType showType, String linkedEventId) {
        Bundle args = new Bundle();
        args.putSerializable(KEY_MEDIA_LIST, new ArrayList<>(medias));
        args.putInt(KEY_POSITION, medias.indexOf(media));
        args.putString(KEY_LINKED_EVENT_ID, linkedEventId);
        return getNewInstance(args, showType);
    }

    public static MediaContainerFragment newInstance(List<Media> medias, Media media, ShowType showType) {
        Bundle args = new Bundle();
        args.putSerializable(KEY_MEDIA_LIST, new ArrayList<>(medias));
        args.putInt(KEY_POSITION, medias.indexOf(media));
        return getNewInstance(args, showType);
    }

    public static MediaContainerFragment newInstance(List<Photo> photos, Photo photo, ShowType showType) {
        Bundle args = new Bundle();
        args.putSerializable(KEY_MEDIA_LIST, new ArrayList<>(MediaContainerPresenter.convertPhotoToMedia(photos)));
        args.putInt(KEY_POSITION, photos.indexOf(photo));
        return getNewInstance(args, showType);
    }

    private static MediaContainerFragment getNewInstance(Bundle args, ShowType showType) {
        args.putSerializable(KEY_SHOW_TYPE, showType);
        MediaContainerFragment fragment = new MediaContainerFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();
        List<Media> mediaList = (List) arguments.getSerializable(KEY_MEDIA_LIST);
        int startPosition = arguments.getInt(KEY_POSITION, 0);
        String linkedEventId = arguments.getString(KEY_LINKED_EVENT_ID, "");
        if (!linkedEventId.isEmpty()) {
            isLinkedEventsEnabled = true;
            isControlsVisible = false;
            presenter.setItemClickListener(view -> {
                isControlsVisible = !isControlsVisible;
                updateControlsVisibility(isControlsVisible);
                updateBottomWindowVisibility(isLinkedEventsEnabled && isControlsVisible, true);
            });
        }
        presenter.setLinkedEventId(linkedEventId);
        presenter.setMediaList(mediaList, startPosition,
                (ShowType) arguments.getSerializable(KEY_SHOW_TYPE));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_media_pager, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initUI(view);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initUI(View view) {
        btnClose = view.findViewById(R.id.btn_close);
        btnClose.setOnClickListener(v -> presenter.onCloseClicked());
        viewPager = view.findViewById(R.id.pager);
        counterTextView = view.findViewById(R.id.txt_counter);
        photoCounterTextView = view.findViewById(R.id.txt_photo_counter);
        videoCounterTextView = view.findViewById(R.id.txt_video_counter);
        bottomWindow = view.findViewById(R.id.linked_event_bottom_window);
        initPager();
        updateControlsVisibility(isControlsVisible);
        updateBottomWindowVisibility(isLinkedEventsEnabled && isControlsVisible, false);
    }

    @Override
    public void handlerShowType(ShowType showType) {
        switch (showType) {
            case CHANGE_OWN_PHOTO:
                initChangeOwnPhotoUI();
                break;
        }
    }

    private void initChangeOwnPhotoUI() {
        View parentView = getView();
        ImageButton btnDelete = parentView.findViewById(R.id.btn_delete);
        btnDelete.setVisibility(View.VISIBLE);
        btnDelete.setOnClickListener(v -> presenter.onDeleteClicked());
        TextView txtBtnAsAvatar = parentView.findViewById(R.id.txt_as_avatar);
        txtBtnAsAvatar.setVisibility(View.VISIBLE);
        txtBtnAsAvatar.setOnClickListener(v -> presenter.onSetAsAvatarClicked());
        TextView txtBtnAddPhotos = parentView.findViewById(R.id.txt_add_photos);
        txtBtnAddPhotos.setVisibility(View.VISIBLE);
        txtBtnAddPhotos.setOnClickListener(v -> presenter.onAddPhotosClicked());
    }

    @Override
    public void showAsAvatarBtn(boolean isShow) {
        getView().findViewById(R.id.txt_as_avatar).setVisibility(isShow ? View.VISIBLE : View.GONE);
    }

    private void initPager() {
        viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                presenter.onPageSelected(position);
                firePositionCallback(position);
            }
        });
        adapter = new FullScreenMediaPagerAdapter(getChildFragmentManager());
        viewPager.setOffscreenPageLimit(adapter.getCount());
        viewPager.setAdapter(adapter);
    }

    private void firePositionCallback(int position) {
        Fragment targetFragment = getTargetFragment();
        if (targetFragment != null && targetFragment instanceof MediaContainerCallback) {
            ((MediaContainerCallback) targetFragment).onFullScreenPageSelected(position);
        }
    }

    @Override
    public void setActive(int index, boolean active) {
        MediaPager mediaPager = (MediaPager) adapter.getItem(index);
        mediaPager.setActive(active);
    }

    @Override
    public void setPosition(int position) {
        viewPager.setCurrentItem(position);
    }

    @Override
    public void setMediaFragments(List<FullScreenFragment> fragments) {
        adapter.switchFragments(fragments);
    }

    @Override
    public void setCounterText(int page, int pagesCount) {

        String pageStr = page + "/";
        String pageCountStr = String.valueOf(pagesCount);

        SpannableString span2 = new SpannableString(pageStr + pageCountStr);
        span2.setSpan(new RelativeSizeSpan(0.6f), pageStr.length(), pageStr.length() + pageCountStr.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        counterTextView.setText(span2);
    }

    @Override
    public void goBack() {
        getActivity().onBackPressed();
    }

    @Override
    public void setPhotoCount(Integer photoCount) {
    }

    @Override
    public void setVideoCount(Integer videoCount) {
    }

    @Override
    public void showLinkedEventDialog(EventInfo eventInfo) {
        Runnable view = () -> switchFragment(EventDetailsFragment.newInstance(eventInfo, BaseEventPresenter.GoingFrom.LINKED_EVENTS), true);
        bottomWindow.setEventInfo(eventInfo, view);
    }

    public interface MediaContainerCallback {
        void onFullScreenPageSelected(int position);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }


    private void updateControlsVisibility(boolean shouldShow) {
        btnClose.setVisibility(shouldShow ? View.VISIBLE : View.INVISIBLE);
        counterTextView.setVisibility(shouldShow ? View.VISIBLE : View.INVISIBLE);
    }

    private void updateBottomWindowVisibility(boolean shouldShow, boolean withAnimation) {
        if (!withAnimation) bottomWindow.setVisibility(shouldShow ? View.VISIBLE : View.GONE);
        else if (shouldShow) {
            bottomWindow.slideUp();
        } else {
            bottomWindow.slideDown();
        }


    }

}