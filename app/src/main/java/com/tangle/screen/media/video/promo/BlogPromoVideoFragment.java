package com.tangle.screen.media.video.promo;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.FrameLayout;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.screen.brand_blog.BrandBlogFragment;

import java.util.concurrent.TimeUnit;

import static com.tangle.utils.AnimationUtils.getAlphaAnimation;

@SuppressLint("ValidFragment")
public class BlogPromoVideoFragment extends PromoVideoFragment {
    private static final long BUTTON_ANIMATION_START_OFFSET = TimeUnit.SECONDS.toMillis(8);
    private static final long TEXT_ANIMATION_START_OFFSET = TimeUnit.SECONDS.toMillis(5);

    private Button vButton;
    private View vMaybeLater;
    private View vIntroducing;
    private View vLogo;
    private View vIntroducingText;
    private View vBackground;

    public BlogPromoVideoFragment() {
        super();
    }

    @Override
    int getVideoRawSource() {
        return 0;
    }

    @Override
    boolean isVideoLooping() {
        return true;
    }

    @Override
    MediaPlayer.OnCompletionListener getOnCompletionListener() {
        return null;
    }

    protected boolean isBrandBlogVideo() {
        return true;
    }

    @Override
    protected View inflateFragment(int resId, LayoutInflater inflater, ViewGroup container) {

        View view = inflater.inflate(resId, container, false);
        FrameLayout layout = view.findViewById(R.id.promo_container);
        View promoView = inflater.inflate(R.layout.fragment_promo_blog, layout, false);
        vButton = promoView.findViewById(R.id.btn_next);
        vMaybeLater = promoView.findViewById(R.id.txt_maybe_later);


        vIntroducing = promoView.findViewById(R.id.txt_introducing);
        vLogo = promoView.findViewById(R.id.img_logo);
        vIntroducingText = promoView.findViewById(R.id.txt_introducing_text);
        vBackground = promoView.findViewById(R.id.background_view);

        layout.addView(promoView);

        initButtons();
        initTextViews();
        setVideoShown();

        return view;
    }

    private void initTextViews() {
        Animation a = getAlphaAnimation(TEXT_ANIMATION_START_OFFSET, () -> {
            vIntroducing.setVisibility(View.VISIBLE);
            vLogo.setVisibility(View.VISIBLE);
            vIntroducingText.setVisibility(View.VISIBLE);
            vBackground.setVisibility(View.VISIBLE);
        }, null);
        vIntroducing.startAnimation(a);
        vBackground.startAnimation(a);
        vLogo.startAnimation(a);
        vIntroducingText.startAnimation(a);

    }

    void setVideoShown() {
        ApplicationLoader.getApplicationInstance().getPreferenceManager().saveBrandBlogPromoShowed();
    }

    private void initButtons() {
        vButton.startAnimation(getAlphaAnimation(BUTTON_ANIMATION_START_OFFSET,
                () -> vButton.setVisibility(View.VISIBLE),
                () -> vButton.setOnClickListener(view -> switchFragmentWithPopLast(new BrandBlogFragment()))));
        vMaybeLater.startAnimation(getAlphaAnimation(BUTTON_ANIMATION_START_OFFSET,
                () -> vMaybeLater.setVisibility(View.VISIBLE),
                () -> vMaybeLater.setOnClickListener(view -> close())));
    }

    @Override
    protected boolean isFromResources() {
        return false;
    }
}
