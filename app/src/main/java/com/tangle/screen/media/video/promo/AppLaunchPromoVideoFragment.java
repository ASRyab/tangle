package com.tangle.screen.media.video.promo;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.view.View;

import com.tangle.R;

@SuppressLint("ValidFragment")
public class AppLaunchPromoVideoFragment extends PromoVideoFragment {

    public AppLaunchPromoVideoFragment(View.OnClickListener clickListener) {
        super(clickListener);
    }

    @Override
    MediaPlayer.OnCompletionListener getOnCompletionListener() {
        return mediaPlayer -> {
            if (clickListener != null && isResumed()) {
                clickListener.onClick(getView());
            }
        };
    }

    @Override
    int getVideoRawSource() {
        return R.raw.app_launch;
    }

    @Override
    boolean isVideoLooping() {
        return false;
    }

}
