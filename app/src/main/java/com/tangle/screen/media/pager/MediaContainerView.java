package com.tangle.screen.media.pager;

import com.tangle.model.event.EventInfo;
import com.tangle.screen.media.FullScreenFragment;

import java.util.List;

public interface MediaContainerView {
    void handlerShowType(MediaContainerFragment.ShowType showType);

    void showAsAvatarBtn(boolean isShow);

    void closeFragmentWithResult(int actionId, Object resultObj, Class parentFragment);

    void setActive(int index, boolean active);

    void setPosition(int position);

    void setMediaFragments(List<FullScreenFragment> fragments);

    void setCounterText(int page,int pagesCount);

    void goBack();

    void setPhotoCount(Integer photoCount);

    void setVideoCount(Integer videoCount);

    void showLinkedEventDialog(EventInfo eventInfo);
}