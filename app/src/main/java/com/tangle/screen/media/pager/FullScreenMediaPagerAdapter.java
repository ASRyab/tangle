package com.tangle.screen.media.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.tangle.screen.media.FullScreenFragment;

import java.util.ArrayList;
import java.util.List;

public class FullScreenMediaPagerAdapter extends FragmentPagerAdapter {

    private List<FullScreenFragment> fragments = new ArrayList<>();

    public FullScreenMediaPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments == null ? 0 : fragments.size();
    }

    public void switchFragments(List<FullScreenFragment> fragments) {
        this.fragments = fragments;
        notifyDataSetChanged();
    }
}
