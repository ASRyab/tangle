package com.tangle.screen.media

import android.view.View
import com.tangle.base.mvp.LifecyclePresenter
import com.tangle.base.mvp.MVPFragment

abstract class FullScreenFragment : MVPFragment() {
    abstract override fun getPresenter(): LifecyclePresenter<*>
    var onClickListener: View.OnClickListener? = null
}