package com.tangle.screen.media.video;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;
import com.danikula.videocache.HttpProxyCacheServer;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.views.LoaderView;
import com.tangle.base.ui.views.video_view.ScalableVideoView;
import com.tangle.model.event.Media;
import com.tangle.screen.media.FullScreenFragment;
import com.tangle.screen.media.MediaView;
import com.tangle.screen.media.pager.MediaPager;
import com.tangle.utils.glide.GlideApp;

import java.io.IOException;

public class VideoFragment extends FullScreenFragment implements VideoView, MediaPager {
    private VideoPresenter presenter = new VideoPresenter();
    private ScalableVideoView videoView;
    private HttpProxyCacheServer cacheProxy = ApplicationLoader.getApplicationInstance().getCacheProxy();
    private boolean initialized;
    private boolean active;
    private ImageView previewIV;
    private LoaderView loaderView;

    public static VideoFragment newInstance(Media media) {
        Bundle args = new Bundle();
        args.putSerializable(MediaView.KEY_MEDIA, media);
        VideoFragment fragment = new VideoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Media media = (Media) getArguments().getSerializable(MediaView.KEY_MEDIA);
        presenter.setMedia(media);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_video, container, false);
        v.setOnClickListener(getOnClickListener());
        return v;
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        videoView = view.findViewById(R.id.view_video);
        previewIV = view.findViewById(R.id.img_preview);
        loaderView = view.findViewById(R.id.view_loader);
    }

    private void setPlayerVisibility(boolean visible) {
        previewIV.setVisibility(visible ? View.INVISIBLE : View.VISIBLE);
        videoView.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }


    private void preStartVideo(Media media) {
        setPlayerVisibility(false);
        loaderView.setVisibility(View.VISIBLE);
        GlideApp.with(previewIV)
                .load(media.videoImagePreviewUrl)
                .into(new DrawableImageViewTarget(previewIV) {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        super.onResourceReady(resource, transition);
                        loaderView.setVisibility(View.INVISIBLE);
                    }
                });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        videoView.stop();
        videoView.release();
    }

    @Override
    public void processMedia(Media media) {
        try {
            preStartVideo(media);
            videoView.setDataSource(cacheProxy.getProxyUrl(media.videoOriginUrl));
            videoView.setLooping(true);
            videoView.prepareAsync(mp -> {
                initialized = isAdded();
                setPlayerVisibility(true);
                if (initialized) {
                    if (active) {
                        videoView.start();
                    }
                } else {
                    if (videoView != null) {
                        videoView.stop();
                        videoView.release();
                    }
                }

            });
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
        if (videoView == null || !initialized) return;
        if (active) {
            if (!videoView.isPlaying())
                videoView.start();
        } else {
            if (videoView.isPlaying())
                videoView.pause();
        }
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }
}