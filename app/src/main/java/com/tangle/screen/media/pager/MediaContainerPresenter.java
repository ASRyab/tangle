package com.tangle.screen.media.pager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.tangle.ApplicationLoader;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.base.ui.BaseFragment;
import com.tangle.model.event.Media;
import com.tangle.model.profile_data.Photo;
import com.tangle.screen.media.FullScreenFragment;
import com.tangle.screen.media.photo.PhotoFragment;
import com.tangle.screen.media.video.VideoFragment;
import com.tangle.screen.profile_photos.ProfilePhotosFragment;
import com.tangle.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;

import static com.tangle.screen.media.pager.MediaContainerFragment.ShowType.OTHER;
import static com.tangle.screen.media.pager.MediaContainerFragment.ShowType.PAST_EVENT;

public class MediaContainerPresenter extends LifecyclePresenter<MediaContainerView> {
    private List<FullScreenFragment> mediaFragments = new ArrayList<>();
    private int position;
    private Media selectedMedia;
    private boolean isNeedShowPhotoCounter;
    private boolean isNeedShowVideoCounter;
    private int photoCount;
    private int videoCount;
    private String linkedEventId;
    private MediaContainerFragment.ShowType showType;
    private List<Media> mediaList;
    private View.OnClickListener parentClickListener;

    @Override
    public void attachToView(MediaContainerView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        getView().handlerShowType(showType);
        if (!mediaFragments.isEmpty()) {
            setMediaFragments();
        }
        if (linkedEventId != null && !linkedEventId.isEmpty()) {
            monitor(ApplicationLoader.getApplicationInstance().getEventManager().getEventById(linkedEventId)
                    .subscribe(info -> getView().showLinkedEventDialog(info),
                            RxUtils.getEmptyErrorConsumer(TAG, "attachToView")), State.DESTROY_VIEW);
        }
    }

    public void setMediaList(List<Media> mediaList, int startPosition, MediaContainerFragment.ShowType showType) {
        this.mediaList = mediaList;
        this.position = startPosition;
        this.showType = showType;
        this.isNeedShowPhotoCounter = !PAST_EVENT.equals(showType);
        this.isNeedShowVideoCounter = OTHER.equals(showType);
        if (isNeedShowPhotoCounter) {
            photoCount = 0;
        }
        if (isNeedShowVideoCounter) {
            videoCount = 0;
        }
        mediaFragments.clear();
        for (Media media : mediaList) {
            mediaFragments.add(createFragment(media));
            if (isNeedShowPhotoCounter) {
                if (media.type.equals(Media.Type.PHOTO)) {
                    photoCount++;
                }
            }
            if (isNeedShowVideoCounter) {
                if (media.type.equals(Media.Type.VIDEO)) {
                    videoCount++;
                }
            }
        }
        if (!isViewDestroyed()) {
            setMediaFragments();
        }
    }

    private void setMediaFragments() {
        MediaContainerView view = getView();
        view.setMediaFragments(mediaFragments);
        view.setPosition(position);
        updateCounterText(position + 1);
        if (isNeedShowPhotoCounter) {
            view.setPhotoCount(photoCount);
        }
        if (isNeedShowVideoCounter) {
            view.setVideoCount(videoCount);
        }
        validateState(position);
    }

    void setItemClickListener(View.OnClickListener listener) {
        this.parentClickListener = listener;
    }

    private FullScreenFragment createFragment(Media media) {
        FullScreenFragment fr = media.type == Media.Type.PHOTO
                ? PhotoFragment.newInstance(media)
                : VideoFragment.newInstance(media);
        if (parentClickListener != null) {
            fr.setOnClickListener(parentClickListener);
        }
        return fr;
    }

    public void onPageSelected(int page) {
        updateCounterText(page + 1);
        validateState(page);
    }

    private void updateCounterText(int page) {
        getView().setCounterText(page, mediaFragments.size());
    }

    private void validateState(int currentPage) {
        for (int i = 0; i < mediaFragments.size(); i++) {
            if (i == currentPage) {
                getView().setActive(i, true);
            } else {
                getView().setActive(i, false);
            }
        }
        this.selectedMedia = mediaList.get(currentPage);
        switch (showType) {
            case CHANGE_OWN_PHOTO:
                getView().showAsAvatarBtn(!isPrimaryPhotoPage(currentPage));
                break;
        }
    }

    private boolean isStartPhotoPage(int currentPage) {
        return position == currentPage;
    }

    private boolean isPrimaryPhotoPage(int currentPage) {
        return mediaList.get(currentPage).isPrimary == 1;
    }

    public void onCloseClicked() {
        getView().goBack();
    }

    public void onDeleteClicked() {
        getView().closeFragmentWithResult(BaseFragment.ResultActions.ACTION_DELETE_OWN_PHOTO, selectedMedia.id, ProfilePhotosFragment.class);
    }

    public void onSetAsAvatarClicked() {
        getView().closeFragmentWithResult(BaseFragment.ResultActions.ACTION_SET_PHOTO_AS_AVATAR, selectedMedia.id, ProfilePhotosFragment.class);
    }

    public void onAddPhotosClicked() {
        getView().closeFragmentWithResult(BaseFragment.ResultActions.ACTION_OPEN_ADD_PHOTOS_VIEW, selectedMedia.id, ProfilePhotosFragment.class);
    }

    public static List<Media> convertPhotoToMedia(List<Photo> photos) {
        List<Media> mediaList = new ArrayList<>(photos.size());
        for (Photo photo : photos) {
            Media media = new Media();
            media.id = photo.id;
            media.isPrimary = photo.isPrimary;
            media.type = Media.Type.PHOTO;
            media.photoBig2xUrl = photo.normal;
            mediaList.add(media);
        }
        return mediaList;
    }

    public void setLinkedEventId(String linkedEventId) {
        this.linkedEventId = linkedEventId;
    }

}