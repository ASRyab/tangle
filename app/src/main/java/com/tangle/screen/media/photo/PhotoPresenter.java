package com.tangle.screen.media.photo;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.model.event.Media;
import com.tangle.screen.media.MediaPresenter;

public class PhotoPresenter extends MediaPresenter<PhotoView> {

    private Media media;

    @Override
    public void attachToView(PhotoView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (media != null) {
            view.processMedia(media);
        }
    }

    @Override
    public void setMedia(Media media) {
        this.media = media;
        if (!isViewDestroyed()) {
            getView().processMedia(media);
        }
    }
}
