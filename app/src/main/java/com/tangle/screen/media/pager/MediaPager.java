package com.tangle.screen.media.pager;

public interface MediaPager {
    void setActive(boolean active);
}