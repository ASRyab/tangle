package com.tangle.screen.media.video.promo;

import android.annotation.SuppressLint;
import android.media.MediaPlayer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.FrameLayout;

import com.tangle.R;

@SuppressLint("ValidFragment")
public class FreeEventPromoVideoFragment extends PromoVideoFragment {

    private View vBackground, vButton;
    private boolean isVideoFinished;

    public FreeEventPromoVideoFragment() {
        super();
    }

    @Override
    MediaPlayer.OnCompletionListener getOnCompletionListener() {
        return mediaPlayer -> showBackground();
    }

    @Override
    int getVideoRawSource() {
        return R.raw.app_launch;
    }

    @Override
    boolean isVideoLooping() {
        return false;
    }

    @Override
    protected View inflateFragment(int resId, LayoutInflater inflater, ViewGroup container) {

        View view = inflater.inflate(resId, container, false);
        FrameLayout layout = view.findViewById(R.id.promo_container);
        View promoView = inflater.inflate(R.layout.fragment_promo_free_event, layout, false);

        vBackground = promoView.findViewById(R.id.background_view);
        vButton = promoView.findViewById(R.id.btn_next);

        return view;
    }

    private void initButton() {
        AlphaAnimation animation1 = new AlphaAnimation(0.0f, 1.0f);
        animation1.setDuration(1000);
        animation1.setStartOffset(18000);
        animation1.setFillAfter(true);
        animation1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                vButton.setVisibility(View.VISIBLE);
                vButton.setOnClickListener(view -> getActivity().onBackPressed());
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                isVideoFinished = true;
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        vButton.startAnimation(animation1);
    }

    @Override
    public boolean onBackPressed() {
        return !isVideoFinished;
    }

    private void showBackground() {
        vBackground.setVisibility(View.VISIBLE);
        if (clickListener != null) {
            clickListener.onClick(getView());
        }
    }
}
