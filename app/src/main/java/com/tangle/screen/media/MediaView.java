package com.tangle.screen.media;

import com.tangle.model.event.Media;

public interface MediaView {

    String KEY_MEDIA = "media";

    void processMedia(Media media);
}
