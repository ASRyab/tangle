package com.tangle.screen.likebook.mutual_like;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.tangle.ApplicationLoader;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;

public class MutualLikePopupPresenter extends LifecyclePresenter<MutualLikePopupView> {

    private String userId;

    public void setUserId(String userId) {
        this.userId = userId;
        if (!isViewDestroyed()) {
            loadUser(userId);
        }
    }

    @Override
    public void attachToView(MutualLikePopupView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        if (!TextUtils.isEmpty(userId)) {
            loadUser(userId);
        }
    }

    private void loadUser(String userId) {
        ApplicationLoader.getApplicationInstance().getUserManager().getUserById(userId)
                .filter(user -> !isViewDestroyed())
                .subscribe(
                        user -> getView().setUser(user),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
    }

    public void onInviteClicked() {
        getView().showChooseEvents(userId);
    }
}
