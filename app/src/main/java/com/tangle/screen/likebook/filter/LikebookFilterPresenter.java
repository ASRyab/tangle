package com.tangle.screen.likebook.filter;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.utils.PreferenceManager;

import java.util.Arrays;

public class LikebookFilterPresenter extends LifecyclePresenter<LikebookFilterView> {

    private AgeRange ageRange;
    private AgeRange oldAgeRange;
    private PreferenceManager preferenceManager;
    private FilterCallback filterCallback;

    @Override
    public void onCreate() {
        super.onCreate();
        preferenceManager = ApplicationLoader.getApplicationInstance().getPreferenceManager();
    }

    @Override
    public void attachToView(LikebookFilterView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        view.setAgeRanges(Arrays.asList(AgeRange.values()));
        readDataFromPrefs();
        validateAgeRange();
    }

    private void validateAgeRange() {
        setAgeRange(ageRange);
    }

    public void onAgeRangeSelected(AgeRange range) {
        setAgeRange(range);
    }

    public void setAgeRange(AgeRange range) {
        if (oldAgeRange == null) {
            oldAgeRange = range;
        }
        this.ageRange = range;
        if (!isViewDestroyed()) {
            getView().setAgeRangeSelected(range);
        }
    }

    public void onApplyClicked() {
        if (ageRange == null) {
            return;
        }
        if (ageRange != oldAgeRange) {
            saveDataToPrefs();
            fireFilterCallback();
        }
        getView().goBack();
    }

    private void saveDataToPrefs() {
        preferenceManager.saveFilterAgeRange(ageRange);
    }

    private void readDataFromPrefs() {
        ageRange = preferenceManager.getFilterAgeRange();
    }

    public void onBackPressed() {
        getView().goBack();
    }

    private void fireFilterCallback() {
        if (filterCallback != null) {
            filterCallback.onFilterUpdated();
        }
    }

    public void setFilterCallback(FilterCallback filterCallback) {
        this.filterCallback = filterCallback;
    }

}
