package com.tangle.screen.likebook.mutual_like;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.views.ShapeButton;
import com.tangle.model.pojos.Gender;
import com.tangle.model.profile_data.Photo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.choose.ChooseEventFragment;
import com.tangle.screen.events.choose.ShowedEventsType;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;


public class MutualLikePopupFragment extends SecondaryScreenFragment implements MutualLikePopupView {
    private static final String KEY_USER_ID = "user_id";
    private MutualLikePopupPresenter presenter = new MutualLikePopupPresenter();

    private ImageView imgUserBackground, imgUserPhoto;
    private ShapeButton inviteBtn;
    private TextView titleTV, messageTV, descriptionTV;

    public static MutualLikePopupFragment newInstance(String userId) {
        Bundle args = new Bundle();
        args.putString(KEY_USER_ID, userId);
        MutualLikePopupFragment fragment = new MutualLikePopupFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String userId = getArguments().getString(KEY_USER_ID);
        presenter.setUserId(userId);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_matches_popup, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        inviteBtn = view.findViewById(R.id.btn_invite);
        imgUserBackground = view.findViewById(R.id.img_background);
        imgUserPhoto = view.findViewById(R.id.img_view_user_photo);
        titleTV = view.findViewById(R.id.txt_like_title);
        messageTV = view.findViewById(R.id.txt_like_message);
        descriptionTV = view.findViewById(R.id.txt_description);
        inviteBtn.setOnClickListener(v -> presenter.onInviteClicked());
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setUser(ProfileData profile) {
        descriptionTV.setText(profile.gender == Gender.MALE
                ? R.string.likebook_mutual_popup_description_male
                : R.string.likebook_mutual_popup_description_female);
        messageTV.setText(getString(R.string.likebook_mutual_popup_title, profile.login));
        Photo photo = profile.primaryPhoto;
        GlideApp.with(this)
                .load(photo.avatar)
                .placeholder(RequestOptionsFactory.getNonAvatar(profile))
                .apply(RequestOptions.circleCropTransform())
                .into(imgUserPhoto);
        GlideApp.with(this)
                .load(photo.normal)
                .apply(RequestOptionsFactory.getDefaultBlurOptions(getActivity()))
                .into(imgUserBackground);
    }

    @Override
    public void showChooseEvents(String userId) {
        addFragment(ChooseEventFragment.newInstance(userId, ShowedEventsType.USER_UPCOMING_EVENT, ChooseEventFragment.GoingFrom.OTHER), true);
    }
}