package com.tangle.screen.likebook.filter;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.views.check.CheckLayoutManager;
import com.tangle.utils.AnimationUtils;

import java.util.List;

public class LikebookFilterFragment extends SecondaryScreenFragment implements LikebookFilterView, AnimationUtils.Dismissible {

    public static final String REVEAL_SETTINGS_KEY = "reveal_settings";
    public static final String AGE_RANGE_KEY = "age_range";

    private RecyclerView ageRV;
    private AgeRangeAdapter ageRangeAdapter;
    private ImageButton closeButton;
    private Button applyButton;

    private LikebookFilterPresenter presenter = new LikebookFilterPresenter();

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    public static LikebookFilterFragment newInstance(AgeRange ageRange, AnimationUtils.RevealAnimationSetting setting) {
        Bundle args = new Bundle();
        args.putSerializable(REVEAL_SETTINGS_KEY, setting);
        args.putSerializable(AGE_RANGE_KEY, ageRange);
        LikebookFilterFragment fragment = new LikebookFilterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static LikebookFilterFragment newInstance(AnimationUtils.RevealAnimationSetting setting) {
        return newInstance(null, setting);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFilterCallback();
        getBundleData(getArguments());
    }

    private void getFilterCallback() {
        Fragment targetFragment = getTargetFragment();
        if (targetFragment instanceof FilterCallback) {
            presenter.setFilterCallback((FilterCallback) targetFragment);
        }
    }

    private void getBundleData(Bundle bundle) {
        AgeRange ageRange = (AgeRange) bundle.getSerializable(AGE_RANGE_KEY);
        presenter.setAgeRange(ageRange);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_likebook_filter, container, false);
        Context context = container.getContext();
        AnimationUtils.registerCreateShareLinkCircularRevealAnimation(context, view,
                (AnimationUtils.RevealAnimationSetting) getArguments().getSerializable(REVEAL_SETTINGS_KEY),
                ContextCompat.getColor(context, R.color.colorBackgroundDark),
                ((ColorDrawable) view.getBackground()).getColor(), null);
        return view;
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        ageRV = view.findViewById(R.id.list_age);
        closeButton = view.findViewById(R.id.btn_close);
        applyButton = view.findViewById(R.id.btn_apply);
        closeButton.setOnClickListener(v -> presenter.onBackPressed());
        applyButton.setOnClickListener(v -> presenter.onApplyClicked());
        initAge();
    }

    private void initAge() {
        ageRangeAdapter = new AgeRangeAdapter();
        ageRangeAdapter.setItemCheckListener(item -> presenter.onAgeRangeSelected(item));
        ageRV.setLayoutManager(new CheckLayoutManager(getActivity()));
        ageRV.setAdapter(ageRangeAdapter);
    }

    @Override
    public void setAgeRangeSelected(AgeRange range) {
        ageRangeAdapter.setItemChecked(range);
    }

    @Override
    public void setAgeRanges(List<AgeRange> ranges) {
        ageRangeAdapter.switchData(ranges);
    }

    @Override
    public void goBack() {
        getActivity().onBackPressed();
    }

    @Override
    public void dismiss(OnDismissedListener listener) {
        Context context = getActivity();
        AnimationUtils.startCreateShareLinkCircularRevealExitAnimation(context, getView(),
                (AnimationUtils.RevealAnimationSetting) getArguments().getSerializable(REVEAL_SETTINGS_KEY),
                ((ColorDrawable) getView().getBackground()).getColor(),
                ContextCompat.getColor(context, R.color.colorBackgroundDark),
                listener::onDismissed);
    }

}
