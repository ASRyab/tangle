package com.tangle.screen.likebook.mutual_like;


import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.profile_data.ProfileData;

public interface MutualLikePopupView extends LoadingView {

    void setUser(ProfileData profileData);

    void showChooseEvents(String userId);
}
