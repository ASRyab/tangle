package com.tangle.screen.likebook.list;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.views.LoaderView;
import com.tangle.base.ui.views.NotificationInformationLayout;
import com.tangle.model.like_or_not.LikeOrNotUser;
import com.tangle.screen.events.choose.ChooseEventFragment;
import com.tangle.screen.events.choose.ShowedEventsType;
import com.tangle.screen.likebook.list.swipecard.OverlayCardLayoutManager;
import com.tangle.screen.likebook.list.swipecard.TransformCardCallback;
import com.tangle.screen.likebook.mutual_like.MutualLikePopupFragment;
import com.tangle.screen.main.navigation_menu.MenuState;

import java.util.List;

public class LikebookFragment extends SecondaryScreenFragment
        implements LikebookView, TransformCardCallback.ItemSwipeObserver {
    private LikebookPresenter presenter = new LikebookPresenter();

    private RecyclerView cardRecyclerView;
    private ImageButton skipButton;
    private ImageButton likeButton;
    private TextView nameTextView;
    private TextView infoBasicTextView;
    private LoaderView loaderView;
    private NotificationInformationLayout notificationInformationLayout;
    private LikebookAdapter adapter;
    private View userInfoContainer;

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_likebook, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        cardRecyclerView = view.findViewById(R.id.list);
        likeButton = view.findViewById(R.id.btn_like);
        skipButton = view.findViewById(R.id.btn_skip);
        nameTextView = view.findViewById(R.id.txt_name);
        userInfoContainer = view.findViewById(R.id.user_info_container);
        infoBasicTextView = view.findViewById(R.id.txt_info_basic);
        loaderView = view.findViewById(R.id.view_loader);
        likeButton.setOnClickListener(v -> presenter.onLikeClicked());
        skipButton.setOnClickListener(v -> presenter.onSkipClicked());
        view.findViewById(R.id.btn_invite).setOnClickListener(v -> {
            presenter.onInviteClicked();
        });
        notificationInformationLayout = view.findViewById(R.id.notify_rview);
        notificationInformationLayout.setVisibility(View.GONE);
        notificationInformationLayout.setClickListener(v -> presenter.onCheckEventsClicked());
        initCardList();
    }

    private void initCardList() {
        cardRecyclerView.setLayoutManager(new OverlayCardLayoutManager());
        adapter = new LikebookAdapter();
        adapter.setUserClickListener(userId -> presenter.onLikebookUserClicked(userId));
        cardRecyclerView.setAdapter(adapter);
    }

    @Override
    public void setUsers(List<LikeOrNotUser> users) {
        adapter.switchUsers(users);
    }

    @Override
    public void swipe(int index, int direction) {
        LikebookAdapter.ProfileViewHolder position = (LikebookAdapter.ProfileViewHolder) cardRecyclerView.findViewHolderForAdapterPosition(index);
        View item = position.itemView;
        View prevItem = null;
        if (index > 0) {
            prevItem = cardRecyclerView.findViewHolderForAdapterPosition(index - 1).itemView;
        }
        animateSwipe(prevItem, item, direction, position.shadow, position.centerIcon);
    }

    private void setButtonsEnabled(boolean enabled) {
        likeButton.setClickable(enabled);
        skipButton.setClickable(enabled);
    }

    private void setSwipeEnabled(boolean enabled) {
        cardRecyclerView.setLayoutFrozen(!enabled);
    }

    @Override
    public void setUIAvailable(boolean available) {
        setButtonsEnabled(available);
        setSwipeEnabled(available);
    }

    @Override
    public void setLoaderVisibility(boolean visible) {
        loaderView.setVisibility(visible ? View.VISIBLE : View.INVISIBLE);
    }

    @Override
    public void setEmptyScreenVisible(boolean visible) {
        notificationInformationLayout.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void goToEvents() {
        moveToMenu(MenuState.TAB_EVENT_INDEX);
    }

    @Override
    public void setUserInfo(LikeOrNotUser userInfo) {
        nameTextView.setText(userInfo.name);
        infoBasicTextView.setText(getString(R.string.profile_basic_info_template, userInfo.age, userInfo.city, userInfo.country));
        animateUserInfoContainer();
    }

    private void animateUserInfoContainer() {
        ObjectAnimator animator = ObjectAnimator.ofFloat(userInfoContainer, View.ALPHA, 0f, 1f);
        animator.setDuration(TransformCardCallback.SWIPE_ANIMATION_DURATION / 2);
        animator.start();
    }

    @Override
    public void goToUserProfile(String userId) {
        ApplicationLoader.getApplicationInstance().getNavigationManager().showUserProfile(userId);
    }

    @Override
    public void showMutualLikePopup(LikeOrNotUser user) {
        addFragment(MutualLikePopupFragment.newInstance(user.id), true);
    }

    @Override
    public void showChooseEventFragment(String userId, ShowedEventsType showedEventsType) {
        addFragment(ChooseEventFragment.newInstance(userId, showedEventsType, ChooseEventFragment.GoingFrom.LIKEBOOK), true);
    }

    @Override
    public void onCardSwiped(int direction) {
        presenter.onCardSwiped(direction);
    }

    @Override
    public void onSwipe(float dX, float dY, int actionState, boolean isCurrentlyActive) {
    }

    private Animator getTranslationAnimator(View item, int direction) {
        int translationX = (direction == ItemTouchHelper.RIGHT) ? cardRecyclerView.getWidth() : -cardRecyclerView.getWidth();
        ObjectAnimator translateAnimator = ObjectAnimator.ofFloat(item, View.TRANSLATION_X, translationX);
        translateAnimator.setDuration(TransformCardCallback.SWIPE_ANIMATION_DURATION);
        return translateAnimator;
    }

    private Animator getRotationAnimator(View item, int direction) {
        int degrees = (direction == ItemTouchHelper.RIGHT) ? TransformCardCallback.MAX_DEGREES : -TransformCardCallback.MAX_DEGREES;
        ObjectAnimator animator = ObjectAnimator.ofFloat(item, View.ROTATION, degrees);
        animator.setDuration(TransformCardCallback.SWIPE_ANIMATION_DURATION);
        return animator;
    }

    private Animator getBackScaleAnimator(View item) {
        AnimatorSet set = new AnimatorSet();
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(item, View.SCALE_X, 1);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(item, View.SCALE_Y, 1);
        set.playTogether(scaleX, scaleY);
        set.setDuration(TransformCardCallback.SWIPE_ANIMATION_DURATION);
        return set;
    }

    private void animateSwipe(View prevItem, View item, int direction, View shadow, View centerIcon) {
        AnimatorSet mainSet = new AnimatorSet();
        mainSet.addListener(new AnimatorSwipeListener(direction));
        mainSet.playSequentially(getAppearingAnimator(shadow, centerIcon, direction), getSwipeAnimator(prevItem, item, direction));
        mainSet.start();
    }

    @NonNull
    private AnimatorSet getSwipeAnimator(View prevItem, View item, int direction) {
        AnimatorSet set = new AnimatorSet();
        set.setStartDelay(TransformCardCallback.DELEY_ANIMATION_DURATION);
        set.playTogether(
                getTextDisappearAnimator(userInfoContainer),
                getTranslationAnimator(item, direction),
                getRotationAnimator(item, direction));
        if (prevItem != null) {
            set.playTogether(getBackScaleAnimator(prevItem));
        }
        return set;
    }

    private Animator getTextDisappearAnimator(View container) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(container, View.ALPHA, 1f, 0f);
        animator.setDuration(TransformCardCallback.SWIPE_ANIMATION_DURATION / 2);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                presenter.shouldShowInfo();
            }
        });
        return animator;
    }

    private Animator getAppearingAnimator(View shadow, View centerIcon, int direction) {
        AnimatorSet set = new AnimatorSet();
        set.playTogether(
                getShadowAnimator(shadow),
                getIconAnimator(centerIcon, direction)
        );
        set.setDuration(TransformCardCallback.APPEARING_ICON_ANIMATION_DURATION / 2);
        return set;
    }

    private Animator getIconAnimator(View icon, int direction) {
        AnimatorSet set = new AnimatorSet();
        ObjectAnimator scaleX = ObjectAnimator.ofFloat(icon, View.SCALE_X, 0, 1);
        ObjectAnimator scaleY = ObjectAnimator.ofFloat(icon, View.SCALE_Y, 0, 1);
        set.playTogether(scaleX, scaleY);
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                icon.setBackgroundResource((direction == ItemTouchHelper.RIGHT) ? R.drawable.likebook_ic_like : R.drawable.likebook_ic_skip);
                icon.setVisibility(View.VISIBLE);
            }
        });
        return set;
    }

    private Animator getShadowAnimator(View item) {
        ObjectAnimator animator = ObjectAnimator.ofFloat(item, View.ALPHA, 0f, 0.6f);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                super.onAnimationStart(animation);
                item.setVisibility(View.VISIBLE);
            }
        });
        return animator;
    }

    private class AnimatorSwipeListener extends AnimatorListenerAdapter {
        private int direction;

        public AnimatorSwipeListener(int direction) {
            this.direction = direction;
        }

        @Override
        public void onAnimationStart(Animator animation) {
            setUIAvailable(false);
        }

        @Override
        public void onAnimationEnd(Animator animation) {
            setUIAvailable(true);
            presenter.onCardSwiped(direction);
        }
    }
}