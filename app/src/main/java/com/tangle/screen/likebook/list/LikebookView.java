package com.tangle.screen.likebook.list;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.base.ui.interfaces.LocationCheckableView;
import com.tangle.managers.PhotoManager;
import com.tangle.model.like_or_not.LikeOrNotUser;
import com.tangle.screen.events.choose.ShowedEventsType;

import java.util.List;

public interface LikebookView extends LoadingView, PhotoManager.PhotoView, LocationCheckableView {

    void setUsers(List<LikeOrNotUser> users);

    void swipe(int index, int direction);

    void setUIAvailable(boolean available);

    void setLoaderVisibility(boolean visible);

    void setEmptyScreenVisible(boolean visible);

    void goToEvents();

    void setUserInfo(LikeOrNotUser userInfo);

    void goToUserProfile(String userId);

    void showMutualLikePopup(LikeOrNotUser user);

    void showChooseEventFragment(String userId, ShowedEventsType showedEventsType);
}