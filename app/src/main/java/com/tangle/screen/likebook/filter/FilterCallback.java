package com.tangle.screen.likebook.filter;

public interface FilterCallback {

    void onFilterUpdated();

}
