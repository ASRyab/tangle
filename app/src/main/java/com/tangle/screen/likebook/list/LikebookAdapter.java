package com.tangle.screen.likebook.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tangle.R;
import com.tangle.base.ui.views.MultipleIndicatorView;
import com.tangle.model.like_or_not.LikeOrNotUser;
import com.tangle.screen.other_profile.UserMediaContentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class LikebookAdapter extends RecyclerView.Adapter<LikebookAdapter.ProfileViewHolder> {

    private List<LikeOrNotUser> likeOrNotUsers = new ArrayList<>();
    private OnLikebookUserClickListener userClickListener;

    @Override
    public ProfileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ProfileViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_likebook_profile, parent, false));
    }

    @Override
    public void onBindViewHolder(ProfileViewHolder holder, int position) {
        holder.bind(likeOrNotUsers.get(position));
    }

    @Override
    public int getItemCount() {
        return likeOrNotUsers == null ? 0 : likeOrNotUsers.size();
    }

    public void switchUsers(List<LikeOrNotUser> users) {
        this.likeOrNotUsers = users;
        notifyDataSetChanged();
    }

    public void setUserClickListener(OnLikebookUserClickListener userClickListener) {
        this.userClickListener = userClickListener;
    }

    public class ProfileViewHolder extends RecyclerView.ViewHolder {

        public View centerIcon;
        public View shadow;
        private UserMediaContentPagerAdapter photoAdapter;
        private MultipleIndicatorView multipleView;

        public ProfileViewHolder(View itemView) {
            super(itemView);
            setIsRecyclable(false);
            multipleView = itemView.findViewById(R.id.view_multiple);
            shadow = itemView.findViewById(R.id.likebook_shadow);
            centerIcon = itemView.findViewById(R.id.center_icon);
            photoAdapter = new UserMediaContentPagerAdapter();
            photoAdapter.setProfileMediaClickListener((userId, photo) -> userClickListener.onUserClicked(userId));
        }

        public void bind(LikeOrNotUser user) {
            photoAdapter.setProfileData(user.toData());
            multipleView.setPagerAdapter(photoAdapter);
        }
    }

    public interface OnLikebookUserClickListener {
        void onUserClicked(String userId);
    }
}
