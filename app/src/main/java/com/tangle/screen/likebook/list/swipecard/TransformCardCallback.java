package com.tangle.screen.likebook.list.swipecard;

import android.graphics.Canvas;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

/**
 * Logic of view transformation while dragging
 */
public class TransformCardCallback extends ItemTouchHelper.SimpleCallback {

    public static float SCALE_GAP = 0.2f;
    public static final float START_ANIMATE_THRESHOLD = 0.3f;
    public static final float SWIPE_END_THRESHOLD = 0.7f;
    public static final float MAX_FRACTION = 1 / START_ANIMATE_THRESHOLD;
    public static final int MAX_DEGREES = 20;
    public static final int SWIPE_ANIMATION_DURATION = 300;
    public static final int APPEARING_ICON_ANIMATION_DURATION = 100;
    public static final int DELEY_ANIMATION_DURATION = 50;
    public static int VISIBLE_ITEMS = 2;

    protected RecyclerView recyclerview;
    protected RecyclerView.Adapter adapter;
    private ItemSwipeObserver observer;
    private boolean swipeEnabled = true;

    public TransformCardCallback(int dragDirs, int swipeDirs, RecyclerView recyclerview, RecyclerView.Adapter adapter) {
        super(dragDirs, swipeDirs);
        this.recyclerview = recyclerview;
        this.adapter = adapter;
    }

    public float getThreshold() {
        return recyclerview.getWidth() * START_ANIMATE_THRESHOLD;
    }

    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return false;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        if (observer != null) {
            observer.onCardSwiped(direction);
        }
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {
        super.clearView(recyclerView, viewHolder);
        viewHolder.itemView.setRotation(0);
    }

    @Override
    public float getSwipeThreshold(RecyclerView.ViewHolder viewHolder) {
        return SWIPE_END_THRESHOLD;
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        double swipeValue = Math.sqrt(dX * dX + dY * dY);
        double fraction = swipeValue / getThreshold();
        int childCount = recyclerView.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View child = recyclerView.getChildAt(i);
            double factor = 1 - (SCALE_GAP - (fraction > 1 ? 1 : fraction) * SCALE_GAP);
            if (i == childCount - 1) {
                float degrees = (float) fraction / MAX_FRACTION * MAX_DEGREES;
                degrees *= dX > 0 ? 1 : -1;
                child.setRotation(degrees);
            }
            int level = childCount - i - 1;
            if (level > 0) {
                child.setScaleX((float) factor);
                child.setScaleY((float) factor);
            }
        }
        if (observer != null) {
            observer.onSwipe(dX, dY, actionState, isCurrentlyActive);
        }
    }

    public void setSwipeCallback(ItemSwipeObserver observer) {
        this.observer = observer;
    }

    @Override
    public long getAnimationDuration(RecyclerView recyclerView, int animationType, float animateDx, float animateDy) {
        return SWIPE_ANIMATION_DURATION;
    }

    public interface ItemSwipeObserver {
        void onCardSwiped(int direction);

        void onSwipe(float dX, float dY, int actionState, boolean isCurrentlyActive);
    }
}
