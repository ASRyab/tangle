package com.tangle.screen.likebook.filter;

public enum AgeRange {

    YOUNG(18, 24),
    AVERAGE(25, 31),
    MATURE(32, 39),
    OLD(40, Integer.MAX_VALUE),
    ALL(18, 79);

    AgeRange(int from, int to) {
        this.from = from;
        this.to = to;
    }

    public static AgeRange getByAge(int age) {
        for (AgeRange ar : AgeRange.values()) {
            if (ar.from <= age && ar.to >= age) {
                return ar;
            }
        }
        return null;
    }

    public int from;
    public int to;
}
