package com.tangle.screen.likebook.list;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.TextUtils;

import com.tangle.ApplicationLoader;
import com.tangle.analytics.SimpleActions;
import com.tangle.analytics.Via;
import com.tangle.api.rx_tasks.image_preload.PreloadLikebookPhotosModel;
import com.tangle.api.rx_tasks.like_or_not.AddUserLike;
import com.tangle.api.rx_tasks.like_or_not.GetLikesGallery;
import com.tangle.api.rx_tasks.like_or_not.SkipUser;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.managers.PhotoManager;
import com.tangle.model.like_or_not.LikeOrNotUser;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.events.choose.ShowedEventsType;
import com.tangle.screen.likebook.filter.AgeRange;
import com.tangle.utils.PreferenceManager;
import com.tangle.utils.RxUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class LikebookPresenter extends LifecyclePresenter<LikebookView> {
    private static final String TAG = LikebookPresenter.class.getSimpleName();

    private List<LikeOrNotUser> dataList = new ArrayList<>();
    private PreferenceManager preferenceManager;
    private int offset, lastSize;

    @Override
    public void onCreate() {
        super.onCreate();
        preferenceManager = ApplicationLoader.getApplicationInstance().getPreferenceManager();
    }

    @Override
    public void attachToView(LikebookView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        loadProfiles();
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().getUpdates()
                        .filter(profileData -> profileData.isYouSkippedUser || profileData.isYouLikedUser || profileData.isBlocked())
                        .observeOn(AndroidSchedulers.mainThread())
                        .map(this::getProfileIndex)
                        .filter(index -> index >= 0)
                        .subscribe(index -> {
                            removeUser(index);
                            if (!dataList.isEmpty()) {
                                getView().setUserInfo(getCurrentUser());
                            }
                        }, RxUtils.getEmptyErrorConsumer(TAG)),
                State.DESTROY_VIEW);
    }

    private int getProfileIndex(ProfileData profileData) {
        for (int i = 0; i < dataList.size(); i++) {
            if (TextUtils.equals(dataList.get(i).id, profileData.id)) {
                return i;
            }
        }
        return -1;
    }

    public void onCardSwiped(int direction) {
        LikeOrNotUser currentUser = getCurrentUser();
        if (currentUser != null) {
            switch (direction) {
                case ItemTouchHelper.RIGHT:
                    checkHomo(currentUser);
                    break;
                case ItemTouchHelper.LEFT:
                    sendUserSkip(currentUser);
                    break;
            }
        }
        removeUser(dataList.size() - 1);
    }

    private void checkHomo(LikeOrNotUser currentUser) {
        Disposable checkHomo = ApplicationLoader.getApplicationInstance().getUserManager().getUserById(currentUser.id)
                .doOnSubscribe(disposable -> getView().setUIAvailable(false))
                .subscribe(user -> {
                            if (user.isUserCanBeLiked) {
                                sendUserLike(currentUser);
                            } else {
                                sendUserSkip(currentUser);
                            }
                        }
                        , RxUtils.getEmptyErrorConsumer("checkHomo"));
        monitor(checkHomo, State.DESTROY_VIEW);
    }

    public void onCheckEventsClicked() {
        getView().goToEvents();
    }

    public void shouldShowInfo() {
        LikeOrNotUser nextUser = getNextUser();
        if (nextUser != null) {
            getView().setUserInfo(nextUser);
        }
    }

    private void loadProfiles() {
        getView().setUIAvailable(false);
        getView().setLoaderVisibility(true);
        AgeRange ageRange = preferenceManager.getFilterAgeRange();
        Disposable disposable = new GetLikesGallery(offset, ageRange).getDataTask()
                .map(response -> response.userGallery)
                .doOnNext(this::preloadImages).doOnEach(notification -> {
                    getView().setUIAvailable(true);
                    getView().setLoaderVisibility(false);
                })
                .subscribe(this::updateDataList,
                        error -> {
                            getView().setEmptyScreenVisible(true);
                            getView().showAPIError(ErrorResponse.getServerMessage(error));
                        });
        monitor(disposable, State.DESTROY_VIEW);
    }

    private void updateDataList(List<LikeOrNotUser> userGallery) {
        if (!userGallery.isEmpty()) {
            getView().setEmptyScreenVisible(false);
            dataList = userGallery;
            lastSize = dataList.size();
            getView().setUsers(dataList);
            getView().setUserInfo(getCurrentUser());
        } else {
            getView().setEmptyScreenVisible(true);
        }
    }

    @SuppressLint("CheckResult")
    private void preloadImages(List<LikeOrNotUser> users) {
        new PreloadLikebookPhotosModel(users).getTask().subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "preloadImages"));
    }

    public void onLikeClicked() {
        ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.LIKEBOOK_CLICK_LIKE);

        PhotoManager photoManager = ApplicationLoader.getApplicationInstance().getPhotoManager();
        monitor(photoManager
                        .checkPhotoObservable(getView())
                        .subscribe(data -> {
                            switch (data) {
                                case HAS_PHOTO:
                                    swipeCurrentItem(ItemTouchHelper.RIGHT);
                                    break;
                            }
                        }, RxUtils.getEmptyErrorConsumer()),
                State.DESTROY);
    }

    public void onSkipClicked() {
        ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.LIKEBOOK_CLICK_DISLIKE);
        swipeCurrentItem(ItemTouchHelper.LEFT);
    }

    public void onLikebookUserClicked(String userId) {
        getView().goToUserProfile(userId);
    }

    private void sendUserLike(LikeOrNotUser user) {
        getView().setUIAvailable(false);
        Disposable disposable = new AddUserLike(user.id).getDataTask()
                .doOnEach(notification -> getView().setUIAvailable(true))
                .subscribe(
                        response -> {
                            if (response.isNewMatch) {
                                getView().showMutualLikePopup(user);
                                ApplicationLoader.getApplicationInstance().getLikeManager().setMatched(user.id);
                            } else {
                                ApplicationLoader.getApplicationInstance().getLikeManager().setLiked(user.id);
                            }
                        },
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    private void sendUserSkip(LikeOrNotUser user) {
        getView().setUIAvailable(false);
        Disposable disposable = new SkipUser(user.id).getDataTask()
                .doOnEach(notification -> getView().setUIAvailable(true))
                .subscribe(RxUtils.getEmptyDataConsumer(),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    private void swipeCurrentItem(int direction) {
        if (!dataList.isEmpty()) {
            getView().swipe(dataList.size() - 1, direction);
        }
    }

    private void removeUser(int index) {
        if (!dataList.isEmpty()) {
            dataList.remove(index);
            getView().setUsers(dataList);
        }
        if (!dataList.isEmpty()) {
            getView().setUIAvailable(true);
        } else {
            offset += lastSize;
            loadProfiles();
        }
    }

    private LikeOrNotUser getNextUser() {
        if (!dataList.isEmpty() && dataList.size() > 1) {
            return dataList.get(dataList.size() - 2);
        }
        return null;
    }

    private LikeOrNotUser getCurrentUser() {
        if (!dataList.isEmpty()) {
            return dataList.get(dataList.size() - 1);
        }
        return null;
    }

    public void onInviteClicked() {
        ApplicationLoader.getApplicationInstance().getAnalyticManager().trackEvent(SimpleActions.LIKEBOOK_CLICK_INVITE);
        if (getCurrentUser() != null) {
            PhotoManager photoManager = ApplicationLoader.getApplicationInstance().getPhotoManager();
            monitor(photoManager
                            .checkPhoto(getView())
                            .subscribe(data -> {
                                switch (data) {
                                    case HAS_APPROVE_PHOTO:
                                        checkUserLocation();
                                        break;
                                    case HAS_PHOTO:
                                        getView().showNoApprovePhoto();
                                        break;
                                }
                            }, RxUtils.getEmptyErrorConsumer()),
                    State.DESTROY);
        }
    }

    private void openChooseEventFragment() {
        getView().showChooseEventFragment(getCurrentUser().id, ShowedEventsType.USER_UPCOMING_EVENT);
        ApplicationLoader.getApplicationInstance().getAnalyticManager().setVia(Via.LIKEBOOK_INVITE);
    }

    private void checkUserLocation() {
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().checkUserInWhiteList()
                        .subscribe(this::inviteProcess, RxUtils.getEmptyErrorConsumer(TAG)),
                State.DESTROY_VIEW);
    }

    private void inviteProcess(boolean isInWhiteList) {
        if (isInWhiteList) {
            getView().showRemindLocationDialog(this::openChooseEventFragment);
        } else {
            openChooseEventFragment();
        }
    }
}