package com.tangle.screen.likebook.list.swipecard;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

/**
 * Logic for view static layouting
 */
public class OverlayCardLayoutManager extends RecyclerView.LayoutManager {

    public static final float FACTOR = 1 - TransformCardCallback.SCALE_GAP;

    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        detachAndScrapAttachedViews(recycler);
        int itemCount = getItemCount();
        if (itemCount > 0) {
            int bottomPosition;
            if (itemCount < TransformCardCallback.VISIBLE_ITEMS) {
                bottomPosition = 0;
            } else {
                bottomPosition = itemCount - TransformCardCallback.VISIBLE_ITEMS;
            }

            for (int position = bottomPosition; position < itemCount; position++) {
                View view = recycler.getViewForPosition(position);
                addView(view);
                int level = itemCount - position - 1;
                handleView(view, level);
            }
        }
    }

    private void handleView(View view, int level) {
        measureChildWithMargins(view, 0, 0);
        int widthSpace = getWidth() - getDecoratedMeasuredWidth(view);
        int heightSpace = getHeight() - getDecoratedMeasuredHeight(view);
        int halfWidth = widthSpace / 2;
        int halfHeight = heightSpace / 2;
        layoutDecoratedWithMargins(view, halfWidth, halfHeight, halfWidth + getDecoratedMeasuredWidth(view), halfHeight + getDecoratedMeasuredHeight(view));
        if (level > 0) {
            view.setScaleX(FACTOR);
            view.setScaleY(FACTOR);
            view.setRotation(0);
        }
    }
}
