package com.tangle.screen.likebook.filter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tangle.R;
import com.tangle.base.ui.views.check.CheckAdapter;

public class AgeRangeAdapter extends CheckAdapter<AgeRange> {

    @Override
    protected CheckViewHolder getViewHolder(ViewGroup parent, int viewType) {
        return new AgeRangeViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_check, parent, false));
    }

    public class AgeRangeViewHolder extends CheckViewHolder {

        public AgeRangeViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void bind(AgeRange item) {
            if (item.to < Integer.MAX_VALUE) {
                labelTextView.setText(String.format("%s-%s", Integer.toString(item.from), item.to));
            } else {
                labelTextView.setText(String.format("%s+", Integer.toString(item.from)));
            }
        }
    }
}
