package com.tangle.screen.likebook.filter;

import java.util.List;

public interface LikebookFilterView {

    void setAgeRangeSelected(AgeRange range);

    void setAgeRanges(List<AgeRange> ranges);

    void goBack();

}
