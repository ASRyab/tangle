package com.tangle.screen.edit_own_profile;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.rx_tasks.dictionary.GetCareersModel;
import com.tangle.api.rx_tasks.dictionary.GetIndustriesModel;
import com.tangle.api.rx_tasks.dictionary.GetInterestsModel;
import com.tangle.api.rx_tasks.photo.DeletePhoto;
import com.tangle.api.rx_tasks.photo.UploadPhotoModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.pojos.DictionaryItem;
import com.tangle.model.profile_data.Photo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.RxUtils;
import com.tangle.utils.UserCheckUtils;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class EditOwnProfilePresenter extends LifecyclePresenter<EditOwnProfileView> {
    public static final int CHANGE_ABOUT_FRAGMENT_INDEX = 2;
    public static final int CHANGE_MY_NAME_FRAGMENT_INDEX = 3;
    public static final int CHANGE_MY_AGE_FRAGMENT_INDEX = 4;
    public static final int CHANGE_LOCATION_FRAGMENT_INDEX = 5;
    public static final int CHANGE_INDUSTRY_FRAGMENT_INDEX = 6;
    public static final int CHANGE_CARIER_FRAGMENT_INDEX = 7;
    public static final int CHANGE_INTEREST_FRAGMENT_INDEX = 8;
    public static final int EDIT_PHOTOS_FRAGMENT_INDEX = 9;

    private ProfileData profileData;

    @Override
    public void attachToView(EditOwnProfileView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        view.initToolbar(getResources().getString(R.string.edit_own_profile), true);
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().currentUserWithUpdates()
                .subscribe(this::setProfileData, error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
    }

    private void refreshProfile() {
        ApplicationLoader.getApplicationInstance().getUserManager().loadCurrentUser();
    }

    private void setProfileData(ProfileData profile) {
        this.profileData = profile;
        Collections.sort(profile.photos);
        EditOwnProfileView view = getView();
        initMainPhoto(profile, view);
        view.setGender(getFormattedGender(profile.gender_key));
        view.setName(profile.login);
        view.setAge(profile.age.toString());
        String city = profile.geo.city;
        String region = profile.geo.region;
        if (region == null) {
            view.setGeo(city);
        } else {
            view.setGeo(getResources().getString(R.string.two_string_template, city == null ? "" : city, region == null ? "" : region));
        }
        boolean isTargetCountry = UserCheckUtils.isTargetCountry(profile.isTargetCountry);
        getView().setGeoVisibility(isTargetCountry);

        setCareer(profile);
        setInterests(profile.interests);
    }

    private String getFormattedGender(String genderKey) {
        switch (genderKey) {
            case "1":
                return getResources().getString(R.string.a_men);
            case "2":
                return getResources().getString(R.string.a_women);
        }
        return "";
    }

    private void initMainPhoto(ProfileData profile, EditOwnProfileView view) {
        view.setAvatarPhoto(profile);
        if (profile.primaryPhoto != null && profile.photos != null) {
            view.setPhotos(profile.photos, profile);
        }
    }

    private void setCareer(ProfileData profile) {
        Observable<DictionaryItem> industriesObservable = new GetIndustriesModel().getTask()
                .flatMapIterable(list -> list)
                .filter(industry -> profile.getIndustry().equals(Integer.toString(industry.id)));
        Observable<DictionaryItem> careersObservable = new GetCareersModel().getTask()
                .flatMapIterable(list -> list)
                .filter(career -> profile.getCareerLevel().equals(Integer.toString(career.id)));
        Disposable disposableIndustry = industriesObservable.subscribe(
                industryInfo -> getView().setIndustry(industryInfo.name),
                error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposableIndustry, State.DESTROY_VIEW);
        Disposable disposableCareer = careersObservable.subscribe(
                careerInfo -> getView().setCareer(careerInfo.name),
                error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposableCareer, State.DESTROY_VIEW);
    }

    private void setInterests(Map<String, Boolean> interests) {
        Set<String> ids = interests.keySet();
        Disposable disposable = new GetInterestsModel().getTask()
                .flatMapIterable(list -> list)
                .filter(item -> ids.contains(Integer.toString(item.id)))
                .toList()
                .subscribe(interestsResponse -> getView().setInterests(getFormattedInterests(interestsResponse)),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    private String getFormattedInterests(List<DictionaryItem> interests) {
        if (interests.isEmpty()) {
            return "";
        }
        StringBuilder formattedInterests = new StringBuilder();
        switch (interests.size()) {
            case 1:
                formattedInterests.append(interests.get(0).name);
                break;
            case 2:
                formattedInterests.append(interests.get(0).name);
                formattedInterests.append(", ");
                formattedInterests.append(interests.get(1).name);
                break;
            default:
                formattedInterests.append(interests.get(0).name);
                formattedInterests.append(", ");
                formattedInterests.append(interests.get(1).name);
                formattedInterests.append(", ");
                formattedInterests.append(interests.get(2).name);
                formattedInterests.append("...");
                break;
        }
        return formattedInterests.toString();
    }

    public void onPhotoEditClick() {
        getView().showIncludedFragmentByIndex(EDIT_PHOTOS_FRAGMENT_INDEX);
    }

    public void onAboutClick() {
        getView().showIncludedFragmentByIndex(CHANGE_ABOUT_FRAGMENT_INDEX);
    }

    public void onMyNameClick() {
        getView().showIncludedFragmentByIndex(CHANGE_MY_NAME_FRAGMENT_INDEX);
    }

    public void onMyAgeClick() {
        getView().showIncludedFragmentByIndex(CHANGE_MY_AGE_FRAGMENT_INDEX);
    }

    public void onMyGeoClick() {
        getView().showIncludedFragmentByIndex(CHANGE_LOCATION_FRAGMENT_INDEX);
    }

    public void onMyIndustryClick() {
        getView().showIncludedFragmentByIndex(CHANGE_INDUSTRY_FRAGMENT_INDEX);
    }

    public void onMyCarierClick() {
        getView().showIncludedFragmentByIndex(CHANGE_CARIER_FRAGMENT_INDEX);
    }

    public void onMyInterestClick() {
        getView().showIncludedFragmentByIndex(CHANGE_INTEREST_FRAGMENT_INDEX);
    }

    public void onViewPhotosClick() {
        getView().showIncludedFragmentByIndex(EDIT_PHOTOS_FRAGMENT_INDEX);
    }

    public void deletePhoto(String photoId) {
        Disposable disposable = new DeletePhoto(photoId).getDataTask()
                .compose(RxUtils.withLoading(getView()))
                .subscribe(deleteResponse -> refreshProfile(),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    public void onPhotoClick(Photo photo) {
        getView().showFullPhotoViewer(profileData.photos, photo);
    }

    public void uploadNewPhoto(File photoFile) {
        Disposable disposable = new UploadPhotoModel(photoFile, false).getTask()
                .compose(RxUtils.withLoading(getView()))
                .subscribe(uploadPhoto -> refreshProfile(),
                        error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }
}