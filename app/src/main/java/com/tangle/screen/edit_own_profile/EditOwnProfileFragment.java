package com.tangle.screen.edit_own_profile;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.managers.DialogManager;
import com.tangle.model.profile_data.Photo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.edit_own_profile.included_fragments.my_name.ChangeNameFragment;
import com.tangle.screen.funnel.age.AgeFragment;
import com.tangle.screen.funnel.career.CareerFragment;
import com.tangle.screen.funnel.geo.GeoFragment;
import com.tangle.screen.funnel.industry.IndustryFragment;
import com.tangle.screen.funnel.interests.InterestsFragment;
import com.tangle.screen.media.pager.MediaContainerFragment;
import com.tangle.screen.profile_photos.ProfilePhotosFragment;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;
import com.tangle.utils.own_user_photo.OwnPhotoPresenter;
import com.tangle.utils.own_user_photo.OwnPhotoView;

import java.io.File;
import java.util.List;

public class EditOwnProfileFragment extends SecondaryScreenFragment implements EditOwnProfileView,
        View.OnClickListener, PhotoClickListener, OwnPhotoView {
    private ImageView imgUserPhoto;
    private ImageView imgBtnEditUserPhoto;
    private TextView txtGenderValue;
    private LinearLayout layoutAbout;
    private LinearLayout layoutMyName;
    private TextView txtMyNameValue;
    private LinearLayout layoutMyAge;
    private TextView txtMyAgeValue;
    private LinearLayout layoutMyGeo;
    private TextView txtMyGeoValue;
    private LinearLayout layoutMyIndustry;
    private TextView txtMyIndustryValue;
    private LinearLayout layoutMyCarier;
    private TextView txtMyCarierValue;
    private LinearLayout layoutMyInterest;
    private TextView txtMyInterestValue;
    private LinearLayout layoutMyPhotos;
    private TextView txtMyPhotoCount;
    private RecyclerView photosRV;
    private ImageView imgAddPhoto;

    private EditOwnProfilePresenter presenter = new EditOwnProfilePresenter();
    private OwnPhotoPresenter ownPhotopresenter = new OwnPhotoPresenter(this);

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_own_profile, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);

        initUI(view);
    }

    private void initUI(View view) {
        imgUserPhoto = view.findViewById(R.id.img_user_photo);
        imgBtnEditUserPhoto = view.findViewById(R.id.img_btn_edit_user_photo);
        imgBtnEditUserPhoto.setOnClickListener(this);
        txtGenderValue = view.findViewById(R.id.txt_gender_value);
        layoutAbout = view.findViewById(R.id.layout_about);
        layoutAbout.setOnClickListener(this);
        layoutMyName = view.findViewById(R.id.layout_my_name);
        layoutMyName.setOnClickListener(this);
        txtMyNameValue = view.findViewById(R.id.txt_my_name_value);
        layoutMyAge = view.findViewById(R.id.layout_my_age);
        layoutMyAge.setOnClickListener(this);
        txtMyAgeValue = view.findViewById(R.id.txt_my_age_value);
        layoutMyGeo = view.findViewById(R.id.layout_from);
        layoutMyGeo.setOnClickListener(this);
        txtMyGeoValue = view.findViewById(R.id.txt_from_value);
        layoutMyIndustry = view.findViewById(R.id.layout_my_industry);
        layoutMyIndustry.setOnClickListener(this);
        txtMyIndustryValue = view.findViewById(R.id.txt_my_industry_value);
        layoutMyCarier = view.findViewById(R.id.layout_carier_level);
        layoutMyCarier.setOnClickListener(this);
        txtMyCarierValue = view.findViewById(R.id.txt_carier_level_value);
        layoutMyInterest = view.findViewById(R.id.layout_interest);
        layoutMyInterest.setOnClickListener(this);
        txtMyInterestValue = view.findViewById(R.id.txt_interest_value);
        layoutMyPhotos = view.findViewById(R.id.layout_photos);
        layoutMyPhotos.setOnClickListener(this);
        txtMyPhotoCount = view.findViewById(R.id.txt_own_photos_amount);
        txtMyPhotoCount.setOnClickListener(this);
        photosRV = view.findViewById(R.id.list_own_photos);
        LinearLayoutManager horizontalLayoutManagaer
                = new LinearLayoutManager(photosRV.getContext(), LinearLayoutManager.HORIZONTAL, false);
        photosRV.setLayoutManager(horizontalLayoutManagaer);
        imgAddPhoto = view.findViewById(R.id.img_btn_add_photo);
        imgAddPhoto.setOnClickListener(this);
    }

    @Override
    public void setAvatarPhoto(ProfileData profileData) {
        GlideApp.with(this)
                .load(profileData.primaryPhoto.normal)
                .placeholder(RequestOptionsFactory.getNonAvatar(profileData))
                .apply(RequestOptions.priorityOf(Priority.HIGH))
                .apply(RequestOptions.circleCropTransform())
                .into(imgUserPhoto);
    }

    @Override
    public void setPhotos(List<Photo> photos, ProfileData ownProfile) {
        if (photos.size() != 0) {
            txtMyPhotoCount.setVisibility(View.VISIBLE);
            txtMyPhotoCount.setText(String.valueOf(photos.size()));
        } else {
            txtMyPhotoCount.setVisibility(View.INVISIBLE);
        }
        photosRV.setAdapter(new EditOwnProfilePhotosAdapter(photos, ownProfile, this));
    }

    @Override
    public void onDeleteClicked(Photo photo) {
        presenter.deletePhoto(photo.id);
    }

    @Override
    public void onPhotoClicked(Photo photo) {
        presenter.onPhotoClick(photo);
    }

    @Override
    public void showFullPhotoViewer(List<Photo> photos, Photo photo) {
        addFragment(MediaContainerFragment.newInstance(photos, photo, MediaContainerFragment.ShowType.USER_PHOTO));
    }

    @Override
    public void setGeoVisibility(boolean isVisible) {
        layoutMyGeo.setVisibility(isVisible ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setGender(String value) {
        txtGenderValue.setText(value);
    }

    @Override
    public void setName(String value) {
        txtMyNameValue.setText(value);
    }

    @Override
    public void setAge(String value) {
        txtMyAgeValue.setText(value);
    }

    @Override
    public void setGeo(String value) {
        txtMyGeoValue.setText(value);
    }

    @Override
    public void setIndustry(String value) {
        txtMyIndustryValue.setText(value);
    }

    @Override
    public void setCareer(String value) {
        txtMyCarierValue.setText(value);
    }

    @Override
    public void setInterests(String value) {
        txtMyInterestValue.setText(value);
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_btn_edit_user_photo:
                presenter.onPhotoEditClick();
                break;
            case R.id.layout_about:
                presenter.onAboutClick();
                break;
            case R.id.layout_my_name:
                presenter.onMyNameClick();
                break;
            case R.id.layout_my_age:
                presenter.onMyAgeClick();
                break;
            case R.id.layout_from:
                presenter.onMyGeoClick();
                break;
            case R.id.layout_my_industry:
                presenter.onMyIndustryClick();
                break;
            case R.id.layout_carier_level:
                presenter.onMyCarierClick();
                break;
            case R.id.layout_interest:
                presenter.onMyInterestClick();
                break;
            case R.id.txt_own_photos_amount:
                presenter.onViewPhotosClick();
                break;
            case R.id.img_btn_add_photo:
                ownPhotopresenter.onAddPhotoClicked();
                break;
        }
    }

    @Override
    public void setCurrentImage(File photoFile) {
        presenter.uploadNewPhoto(photoFile);
    }

    @Override
    public void setError(String error) {
        showAPIError(error);
    }

    @Override
    public void uploadAvailablePhoto(File photoFile) {
    }

    @Override
    public void showUploadDialog(OwnPhotoPresenter presenter) {
        DialogManager manager = DialogManager.getInstance(this);
        manager.showPhotoUploadDialog(presenter);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ownPhotopresenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void showIncludedFragmentByIndex(int fragmentIndex) {
        switch (fragmentIndex) {
            case EditOwnProfilePresenter.CHANGE_MY_NAME_FRAGMENT_INDEX:
                addFragment(new ChangeNameFragment());
                break;
            case EditOwnProfilePresenter.CHANGE_MY_AGE_FRAGMENT_INDEX:
                addFragment(AgeFragment.newInstance(null, false));
                break;
            case EditOwnProfilePresenter.CHANGE_LOCATION_FRAGMENT_INDEX:
                addFragment(GeoFragment.newInstance(null, false));
                break;
            case EditOwnProfilePresenter.CHANGE_INDUSTRY_FRAGMENT_INDEX:
                addFragment(IndustryFragment.newInstance(null, false));
                break;
            case EditOwnProfilePresenter.CHANGE_CARIER_FRAGMENT_INDEX:
                addFragment(CareerFragment.newInstance(null, false));
                break;
            case EditOwnProfilePresenter.CHANGE_INTEREST_FRAGMENT_INDEX:
                addFragment(InterestsFragment.newInstance(null, false));
                break;
            case EditOwnProfilePresenter.EDIT_PHOTOS_FRAGMENT_INDEX:
                addFragment(ProfilePhotosFragment.newInstance());
                break;
        }
    }
}