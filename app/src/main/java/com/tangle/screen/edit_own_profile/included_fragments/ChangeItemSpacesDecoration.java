package com.tangle.screen.edit_own_profile.included_fragments;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class ChangeItemSpacesDecoration extends RecyclerView.ItemDecoration {
    private int topSpace;
    private int bottomSpace;

    public ChangeItemSpacesDecoration(int topSpace, int bottomSpace) {
        this.topSpace = topSpace;
        this.bottomSpace = bottomSpace;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        if(parent.getChildAdapterPosition(view) == state.getItemCount() - 1){
            outRect.bottom = bottomSpace;
            outRect.top = 0;
        }
        if(parent.getChildAdapterPosition(view) == 0){
            outRect.top = topSpace;
            outRect.bottom = 0;
        }
    }
}