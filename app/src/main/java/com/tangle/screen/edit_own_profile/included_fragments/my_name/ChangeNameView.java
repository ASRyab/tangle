package com.tangle.screen.edit_own_profile.included_fragments.my_name;

import com.tangle.base.ui.interfaces.AutoCloseable;
import com.tangle.base.ui.interfaces.LoadingView;

public interface ChangeNameView extends LoadingView, AutoCloseable{
    void setName(String name);

    void setNameError(String error);

    void setEnableSaveBtn(boolean enable);
}