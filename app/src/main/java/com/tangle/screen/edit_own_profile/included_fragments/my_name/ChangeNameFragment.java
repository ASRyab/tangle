package com.tangle.screen.edit_own_profile.included_fragments.my_name;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.listeners.SimpleTextWatcher;
import com.tangle.utils.PlatformUtils;

public class ChangeNameFragment extends SecondaryScreenFragment implements ChangeNameView {
    private ChangeNamePresenter presenter = new ChangeNamePresenter();

    private EditText edtTxtName;
    private Button saveButton;
    private TextInputLayout inputLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_change_name, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);

        initUI(view);
    }

    private void initUI(View view) {
        initToolbar(getString(R.string.my_name_is), true);
        edtTxtName = view.findViewById(R.id.edt_txt_name);
        inputLayout = view.findViewById(R.id.input_layout_name);
        edtTxtName.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                setNameError(null);
                setEnableSaveBtn(true);
            }
        });
        saveButton = view.findViewById(R.id.btn_save);
        saveButton.setOnClickListener(v -> presenter.onSaveClicked(edtTxtName.getText().toString()));
    }

    @Override
    public void setNameError(String error) {
        inputLayout.setError(error);
    }

    @Override
    public void onStop() {
        super.onStop();
        PlatformUtils.Keyboard.hideKeyboard(getActivity());
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setName(String name) {
        edtTxtName.setText(name);
    }

    @Override
    public void setEnableSaveBtn(boolean enable) {
        saveButton.setEnabled(enable);
    }
}