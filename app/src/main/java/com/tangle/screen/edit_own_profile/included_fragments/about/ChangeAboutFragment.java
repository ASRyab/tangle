package com.tangle.screen.edit_own_profile.included_fragments.about;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.utils.PlatformUtils;

public class ChangeAboutFragment extends SecondaryScreenFragment implements ChangeAboutView {
    private ChangeAboutPresenter presenter = new ChangeAboutPresenter();

    private EditText edtTxtAbout;
    private Button saveButton;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_change_about, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);

        initUI(view);
    }

    private void initUI(View view) {
        initToolbar(getString(R.string.about), true);
        edtTxtAbout = view.findViewById(R.id.edt_text_about);
        saveButton = view.findViewById(R.id.btn_save);
        saveButton.setOnClickListener(v -> presenter.onSaveClicked(edtTxtAbout.getText().toString()));
    }

    @Override
    public void onStop() {
        super.onStop();
        PlatformUtils.Keyboard.hideKeyboard(getActivity());
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }

    @Override
    public void setAbout(String about) {
        edtTxtAbout.setText(about);
    }

    @Override
    public void setEnableSaveBtn(boolean enable) {
        saveButton.setEnabled(enable);
    }
}