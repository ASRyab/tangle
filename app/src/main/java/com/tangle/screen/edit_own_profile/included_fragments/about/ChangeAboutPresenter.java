package com.tangle.screen.edit_own_profile.included_fragments.about;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.rx_tasks.profile.update.ProfileUpdateModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.funnel.ProfileDataMapper;

import io.reactivex.disposables.Disposable;

//not used now
public class ChangeAboutPresenter extends LifecyclePresenter<ChangeAboutView> {
    private ProfileData profileData;

    @Override
    public void attachToView(ChangeAboutView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().currentUser()
                .subscribe(this::setProfileData, error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
    }

    private void setProfileData(ProfileData profileData) {
        this.profileData = profileData;
        getView().setAbout(profileData.description);
    }

    public void onSaveClicked(String about) {
        ProfileData profile = new ProfileDataMapper().mapValue(profileData);
        profile.description = about;
        Disposable disposable = new ProfileUpdateModel(profile).getTask().subscribe(profileData -> getView().showAPIError(getResources().getString(R.string.update_successful)), error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }
}