package com.tangle.screen.edit_own_profile;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.profile_data.Photo;
import com.tangle.model.profile_data.ProfileData;

import java.util.List;

public interface EditOwnProfileView extends LoadingView {
    void initToolbar(String title, boolean showBack);

    void setAvatarPhoto(ProfileData url);

    void setPhotos(List<Photo> photos, ProfileData ownProfile);

    void setGender(String value);

    void setName(String value);

    void setAge(String value);

    void setGeo(String value);

    void setIndustry(String value);

    void setCareer(String value);

    void setInterests(String value);

    void showIncludedFragmentByIndex(int fragmentIndex);

    void showFullPhotoViewer(List<Photo> photos, Photo photo);

    void setGeoVisibility(boolean isVisible);
}