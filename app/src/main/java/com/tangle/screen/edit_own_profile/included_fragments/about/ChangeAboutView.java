package com.tangle.screen.edit_own_profile.included_fragments.about;

public interface ChangeAboutView {
    void setAbout(String about);

    void setEnableSaveBtn(boolean enable);

    void showAPIError(String error);
}