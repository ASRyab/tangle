package com.tangle.screen.edit_own_profile;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.model.profile_data.Photo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.ArrayList;
import java.util.List;

public class EditOwnProfilePhotosAdapter extends RecyclerView.Adapter<EditOwnProfilePhotosAdapter.PhotoViewHolder> {
    private List<Photo> photos = new ArrayList<>();
    private PhotoClickListener photoClickListener;
    private RequestOptions userPhotoRequestOptions;
    private ProfileData ownProfile;

    public EditOwnProfilePhotosAdapter(List<Photo> photos, ProfileData ownProfile, PhotoClickListener photoClickListener) {
        this.photos.addAll(photos);
        this.photoClickListener = photoClickListener;
        this.ownProfile = ownProfile;
        this.userPhotoRequestOptions = RequestOptionsFactory.getSmallRoundedCornersOptions(ApplicationLoader.getContext());
    }

    public class PhotoViewHolder extends RecyclerView.ViewHolder {
        public ImageView imgUserPhoto, imgBtnPhotoDelete;
        public TextView txtPendingDelete;

        public PhotoViewHolder(View view) {
            super(view);
            imgUserPhoto = view.findViewById(R.id.img_user_photo);
            imgBtnPhotoDelete = view.findViewById(R.id.img_delete_user_photo);
            txtPendingDelete = view.findViewById(R.id.txt_pending_delete);
        }
    }

    @Override
    public PhotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_edit_own_profile_photos_item, parent, false);
        return new PhotoViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PhotoViewHolder holder, int position) {
        Photo photo = photos.get(position);
        ImageView imgUserPhoto = holder.imgUserPhoto;
        GlideApp.with(imgUserPhoto)
                .load(photo.normal)
                .placeholder(RequestOptionsFactory.getNonAvatar(ownProfile))
                .apply(userPhotoRequestOptions)
                .into(imgUserPhoto);
        holder.txtPendingDelete.setVisibility(!photo.pendingDelete ? View.GONE : View.VISIBLE);
        if (!photo.pendingDelete) {
            holder.imgBtnPhotoDelete.setVisibility(View.VISIBLE);
            holder.imgBtnPhotoDelete.setOnClickListener(v -> {
                if (photoClickListener != null) {
                    photoClickListener.onDeleteClicked(photo);
                }
            });
            holder.imgUserPhoto.setOnClickListener(v -> {
                if (photoClickListener != null) {
                    photoClickListener.onPhotoClicked(photo);
                }
            });
        } else {
            holder.imgBtnPhotoDelete.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return photos.size();
    }
}