package com.tangle.screen.edit_own_profile.included_fragments.my_name;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.api.rx_tasks.profile.update.ProfileUpdateModel;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.funnel.ProfileDataMapper;
import com.tangle.utils.RxUtils;

import io.reactivex.disposables.Disposable;

public class ChangeNamePresenter extends LifecyclePresenter<ChangeNameView> {
    private ProfileData profileData;

    @Override
    public void attachToView(ChangeNameView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().currentUser()
                .subscribe(this::setProfileData, error -> getView().showAPIError(ErrorResponse.getServerMessage(error))), State.DESTROY_VIEW);
    }

    private void setProfileData(ProfileData profileData) {
        this.profileData = profileData;
        setName(profileData.login);
    }

    public void setName(String name) {
        getView().setName(name);
    }

    public boolean validateName(String name) {
        return !TextUtils.isEmpty(name) && name.length() >= 2 && name.length() < 15 && name.matches("[a-zA-Z0-9.? ]*");
    }

    public void onSaveClicked(String name) {
        String trimName = name.trim();
        if (profileData.login.equals(trimName)) {
            return;
        }
        if (validateName(trimName)) {
            ProfileData profile = new ProfileDataMapper().mapValue(profileData);
            profile.login = name;
            Disposable disposable = new ProfileUpdateModel(profile).saveName()
                    .compose(RxUtils.withLoading(getView()))
                    .subscribe(profileData -> {
                        getView().showAPIError(getResources().getString(R.string.update_successful));
                        getView().close();
                    }, error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
            monitor(disposable, State.DESTROY_VIEW);
        } else {
            getView().setEnableSaveBtn(false);
            getView().setNameError(getResources().getString(R.string.name_error));
        }
    }
}