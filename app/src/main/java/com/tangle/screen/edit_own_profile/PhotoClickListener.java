package com.tangle.screen.edit_own_profile;

import com.tangle.model.profile_data.Photo;

public interface PhotoClickListener {
    void onDeleteClicked(Photo photo);
    void onPhotoClicked(Photo photo);
}