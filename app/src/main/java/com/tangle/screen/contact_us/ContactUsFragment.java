package com.tangle.screen.contact_us;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.ui.SecondaryScreenFragment;
import com.tangle.base.ui.listeners.SimpleTextWatcher;
import com.tangle.model.contact_us.ContactData;
import com.tangle.model.contact_us.ContactUsData;
import com.tangle.utils.PlatformUtils;

import java.util.List;

public class ContactUsFragment extends SecondaryScreenFragment implements ContactUsView {
    private static final String BUNDLE_CONTACT_US_KEY = "contact_us_key";

    public static ContactUsFragment newInstance(ContactUsData contactUsData) {
        Bundle args = new Bundle();
        args.putSerializable(BUNDLE_CONTACT_US_KEY, contactUsData);
        ContactUsFragment fragment = new ContactUsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private ContactUsPresenter presenter = new ContactUsPresenter();

    private Spinner categorySpinner;
    private Spinner subjectSpinner;
    private TextView phoneNumberTv;
    private Button sendBtn;
    private EditText messageInput;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter.setContactUsData((ContactUsData) getArguments().getSerializable(BUNDLE_CONTACT_US_KEY));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_us, container, false);
    }

    @Override
    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onPostViewCreated(view, savedInstanceState);
        initToolbar(view);
        phoneNumberTv = view.findViewById(R.id.tv_phone_number);
        sendBtn = view.findViewById(R.id.btn_send);
        categorySpinner = view.findViewById(R.id.spinner_category);
        subjectSpinner = view.findViewById(R.id.spinner_subject);
        messageInput = view.findViewById(R.id.input_message);
        sendBtn.setOnClickListener(v -> {
            String selectedCategory = ((ContactUsItem) categorySpinner.getSelectedItem()).id;
            String selectedSubject = ((ContactUsItem) subjectSpinner.getSelectedItem()).id;
            String message = messageInput.getText().toString();
            presenter.sendContactUs(selectedCategory, selectedSubject, message);
        });
        setMessageTextWatcher();
        setHideKeyboardListeners();
    }

    private void initToolbar(View view) {
        TextView toolbarTitle = view.findViewById(R.id.txt_toolbar_title);
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarTitle.setText(R.string.settings_item_contact_us);
    }

    private void setMessageTextWatcher() {
        messageInput.addTextChangedListener(new SimpleTextWatcher() {
            @Override
            public void afterTextChanged(Editable s) {
                sendBtn.setEnabled(!s.toString().isEmpty());
            }
        });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setHideKeyboardListeners() {
        View.OnTouchListener hideKeyboardListener = (v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                PlatformUtils.Keyboard.hideKeyboard(getActivity());
            }
            return false;
        };
        categorySpinner.setOnTouchListener(hideKeyboardListener);
        subjectSpinner.setOnTouchListener(hideKeyboardListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        PlatformUtils.Keyboard.hideKeyboard(getActivity());
    }

    @Override
    public void setContactData(ContactData contactData) {
        phoneNumberTv.setText(" " + contactData.phoneNumber);
    }

    @Override
    public void setCategories(List<ContactUsItem> categories) {
        ArrayAdapter<ContactUsItem> categoriesAdapter = new ArrayAdapter<>(getContext(), R.layout.item_contact_us_spinner,
                android.R.id.text1, categories);
        categoriesAdapter.setDropDownViewResource(R.layout.item_contact_us_spinner_dropdown);
        categorySpinner.setAdapter(categoriesAdapter);
        categorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ContactUsItem item = (ContactUsItem) parent.getItemAtPosition(position);
                presenter.onCategorySelected(item.id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void setSubjects(List<ContactUsItem> subjects) {
        ArrayAdapter<ContactUsItem> subjectsAdapter = new ArrayAdapter<>(getContext(), R.layout.item_contact_us_spinner,
                android.R.id.text1, subjects);
        subjectsAdapter.setDropDownViewResource(R.layout.item_contact_us_spinner_dropdown);
        subjectSpinner.setAdapter(subjectsAdapter);
    }

    @Override
    public void goBack() {
        getActivity().onBackPressed();
    }

    @Override
    public LifecyclePresenter getPresenter() {
        return presenter;
    }


}
