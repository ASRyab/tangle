package com.tangle.screen.contact_us;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.contact_us.ContactData;

import java.util.List;

public interface ContactUsView extends LoadingView {

    void setContactData(ContactData contactData);

    void setCategories(List<ContactUsItem> categories);

    void setSubjects(List<ContactUsItem> subjects);

    void goBack();
}
