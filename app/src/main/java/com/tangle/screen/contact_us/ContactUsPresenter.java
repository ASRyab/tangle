package com.tangle.screen.contact_us;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.google.common.collect.Sets;
import com.tangle.api.rx_tasks.dictionary.ContactUs;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.model.contact_us.ContactUsCategoryData;
import com.tangle.model.contact_us.ContactUsData;
import com.tangle.model.contact_us.ContactUsSubjectData;
import com.tangle.utils.RxUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

public class ContactUsPresenter extends LifecyclePresenter<ContactUsView> {

    private static final Set<String> categoriesToHide = Collections.singleton("589");
    private static final Set<String> subjectsToHide = Sets.newHashSet("3030", "2987", "3015", "3022", "3021", "3032", "3033", "3034", "3036", "3037");

    private ContactUsData contactUsData;

    @Override
    public void attachToView(ContactUsView view, @Nullable Bundle savedInstanceState) {
        super.attachToView(view, savedInstanceState);
        getView().setContactData(contactUsData.contactData);
        getView().setCategories(fromCategories(contactUsData.categories));
    }

    void setContactUsData(ContactUsData contactUsData) {
        this.contactUsData = contactUsData;
    }

    public void sendContactUs(String category, String subject, String message) {
        Disposable disposable = new ContactUs(category, subject, message).getDataTask()
                .compose(RxUtils.withLoading(getView()))
                .subscribe(data -> getView().goBack(), error -> getView().showAPIError(ErrorResponse.getServerMessage(error)));
        monitor(disposable, State.DESTROY_VIEW);
    }

    public void onCategorySelected(String categoryId) {
        getView().setSubjects(fromSubjects(contactUsData.categories.get(categoryId).subjects));
    }

    private List<ContactUsItem> fromCategories(Map<String, ContactUsCategoryData> categories) {
        return Observable.fromIterable(categories.entrySet())
                .filter(entry -> entry.getValue().subjects != null && !entry.getValue().subjects.isEmpty())
                .filter(entry -> !categoriesToHide.contains(entry.getKey()))
                .map(entry -> new ContactUsItem(entry.getKey(), entry.getValue().title))
                .toList().blockingGet();
    }

    private List<ContactUsItem> fromSubjects(Map<String, ContactUsSubjectData> subjects) {
        return Observable.fromIterable(subjects.entrySet()).filter(entry -> !subjectsToHide.contains(entry.getKey()))
                .map(entry -> new ContactUsItem(entry.getKey(), entry.getValue().title))
                .toList().blockingGet();
    }
}
