package com.tangle.screen.contact_us;

class ContactUsItem {
    String id;
    String title;

    ContactUsItem(String id, String title) {
        this.id = id;
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }
}