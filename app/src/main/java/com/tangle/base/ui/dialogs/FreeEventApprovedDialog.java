package com.tangle.base.ui.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.ui.interfaces.FragmentOperations;
import com.tangle.managers.EventManager;
import com.tangle.model.event.EventInfo;
import com.tangle.screen.own_profile.OwnProfileFragment;
import com.tangle.utils.RxUtils;

@SuppressLint("ValidFragment")
public class FreeEventApprovedDialog extends DialogFragment {
    private EventInfo eventInfo;
    private TextView tvTitle;

    public FreeEventApprovedDialog() {
    }

    private FreeEventApprovedDialog(EventInfo eventInfo) {
        this.eventInfo = eventInfo;
    }

    public static FreeEventApprovedDialog newInstance(EventInfo eventId) {
        FreeEventApprovedDialog dialog = new FreeEventApprovedDialog(eventId);
        return dialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_FRAME, R.style.ApplicationTheme);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_free_ticket_approved, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (eventInfo == null) {
            dismiss();
            return;
        }
        tvTitle = view.findViewById(R.id.tvTitle);
        TextView tvSubTitle = view.findViewById(R.id.txt_sub_title);
        view.findViewById(R.id.btn_share).setOnClickListener(__ -> onShareClick());
        view.findViewById(R.id.txt_maybe_later).setOnClickListener(__ -> dismiss());
        initSpannable(tvSubTitle);
        setEvent(eventInfo);
    }

    private void onShareClick() {
        Activity activity = getActivity();
        if (activity == null) {
            dismiss();
            return;
        }
        ApplicationLoader.getApplicationInstance().getUserManager().currentUser().subscribe(data -> {
            EventManager.shareEvent(activity, data.id);
            dismiss();
        }, RxUtils.getEmptyErrorConsumer(FreeEventApprovedDialog.class.getSimpleName()));
    }

    private void initSpannable(TextView tvSubTitle) {
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View view) {
                ((FragmentOperations) getActivity()).switchFragment(new OwnProfileFragment(), true);
                dismiss();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
                ds.setColor(getResources().getColor(R.color.colorPrimary));
            }
        };
        String text = getString(R.string.free_event_approved_subtitle);
        String myTickets = getString(R.string.free_event_approved_my_profile);
        SpannableStringBuilder builder = new SpannableStringBuilder(text);
        builder.setSpan(clickableSpan, text.indexOf(myTickets), text.indexOf(myTickets) + myTickets.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvSubTitle.setText(builder);
        tvSubTitle.setMovementMethod(LinkMovementMethod.getInstance());
        tvSubTitle.setHighlightColor(getResources().getColor(R.color.colorPrimaryDark));
    }

    public void setEvent(EventInfo eventInfo) {
        tvTitle.setText(getString(R.string.free_event_approved_title, eventInfo.title));
    }
}
