package com.tangle.base.ui;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.base.ui.interfaces.BackPressable;
import com.tangle.base.ui.interfaces.FragmentOperations;
import com.tangle.base.ui.listeners.KeyboardListener;
import com.tangle.utils.network.NetworkService;

public abstract class BaseActivity extends AppCompatActivity
        implements FragmentOperations, KeyboardListener {
    protected String TAG = this.getClass().getSimpleName();

    protected FragmentManager fragmentManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentManager = getSupportFragmentManager();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ApplicationLoader.getApplicationInstance().getNavigationManager().init(this);
    }

    @Override
    public void switchFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.container, fragment, fragment.getClass().getSimpleName());
        if (addToBackStack) {
            transaction.addToBackStack(fragment.getClass().getSimpleName());
        }
        transaction.commit();
    }

    @Override
    public void switchFragmentWithPopLast(Fragment fragment) {
        fragmentManager.popBackStack();
        switchFragment(fragment, true);
    }

    @Override
    public void addFragment(Fragment fragment, boolean addToBackStack) {
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.container, fragment, fragment.getClass().getSimpleName());
        if (addToBackStack) {
            transaction.addToBackStack(fragment.getClass().getSimpleName());
        }
        transaction.commit();
    }

    @Override
    public Fragment getSecondaryFragment() {
        return fragmentManager.findFragmentById(R.id.container);
    }

    @Override
    public void onBackPressed() {
        Fragment currentFragment = getSecondaryFragment();
        if (currentFragment != null && currentFragment instanceof BackPressable) {
            if (!((BackPressable) currentFragment).onBackPressed()) {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public Fragment findFragment(Class fragmentClass) {
        return fragmentManager.findFragmentByTag(fragmentClass.getSimpleName());
    }

    private boolean keyboardListenersAttached = false;
    private ViewGroup rootLayout;
    private String lastActionType = ACTION_HIDE_KEYBOARD;

    private ViewTreeObserver.OnGlobalLayoutListener keyboardLayoutListener = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            int heightDiff = rootLayout.getRootView().getHeight() - rootLayout.getHeight();
            Rect rectangle = new Rect();
            getWindow().getDecorView().getWindowVisibleDisplayFrame(rectangle);
            int statusBarAndSoftBtnBarHeight = rectangle.top + getSoftButtonsBarHeight();
            LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getApplicationContext());
            String actionType;
            if (heightDiff <= statusBarAndSoftBtnBarHeight) {
                actionType = ACTION_HIDE_KEYBOARD;
            } else {
                actionType = ACTION_SHOW_KEYBOARD;
            }
            if (!actionType.equals(lastActionType)) {
                Intent intent = new Intent(actionType);
                broadcastManager.sendBroadcast(intent);
                lastActionType = actionType;
            }
        }
    };

    @Override
    public void attachKeyboardListeners() {
        if (keyboardListenersAttached) {
            return;
        }
        rootLayout = findViewById(R.id.container);
        rootLayout.getViewTreeObserver().addOnGlobalLayoutListener(keyboardLayoutListener);
        keyboardListenersAttached = true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (keyboardListenersAttached) {
            rootLayout.getViewTreeObserver().removeGlobalOnLayoutListener(keyboardLayoutListener);
            keyboardListenersAttached = false;
        }
    }

    private int getSoftButtonsBarHeight() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int usableHeight = metrics.heightPixels;
        getWindowManager().getDefaultDisplay().getRealMetrics(metrics);
        int realHeight = metrics.heightPixels;
        if (realHeight > usableHeight) {
            return realHeight - usableHeight;
        } else {
            return 0;
        }
    }

    @Override
    protected void onStop() {
        stopService(new Intent(this, NetworkService.class));
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        Intent startServiceIntent = new Intent(this, NetworkService.class);
        startService(startServiceIntent);
    }

}