package com.tangle.base.ui.views;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tangle.R;

public class NotificationInformationLayout extends LinearLayout {
    private ImageView notificationIco;
    private TextView notificationTitle;
    private TextView notificationBody;
    private ShapeButton notificationBtn;

    public NotificationInformationLayout(Context context) {
        super(context);
        initUI(context);

    }

    public NotificationInformationLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initUI(context);
        readAndApplyAttrs(attrs);
    }

    public NotificationInformationLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initUI(context);
        readAndApplyAttrs(attrs);
    }

    private void initUI(Context context) {
        LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        ViewGroup view = (ViewGroup) layoutInflater.inflate(
                R.layout.layout_notification_information, this);

        notificationIco = view.findViewById(R.id.notification_ico);
        notificationTitle = view.findViewById(R.id.notification_title);
        notificationBody = view.findViewById(R.id.notification_body);
        notificationBtn = view.findViewById(R.id.notification_btn);
    }

    private void readAndApplyAttrs(AttributeSet attrs) {
        if (attrs == null) {
            return;
        }

        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.NotificationInformationLayout);
        int icoResId = typedArray.getResourceId(R.styleable.NotificationInformationLayout_nl_notification_ico, 0);
        String title = typedArray.getString(R.styleable.NotificationInformationLayout_nl_notification_title);
        String body = typedArray.getString(R.styleable.NotificationInformationLayout_nl_notification_body);
        String btnText = typedArray.getString(R.styleable.NotificationInformationLayout_nl_notification_btn_text);
        typedArray.recycle();

        notificationIco.setImageResource(icoResId);
        notificationTitle.setText(title);
        notificationBody.setText(body);
        if (!TextUtils.isEmpty(btnText)) {
            notificationBtn.setText(btnText);
        } else {
            notificationBtn.setVisibility(GONE);
        }
    }

    public void setButtonText(int resId) {
        notificationBtn.setText(resId);
    }

    public void setNotificationBodyText(int resId) {
        notificationBody.setText(resId);
    }

    public void setClickListener(OnClickListener l) {
        notificationBtn.setOnClickListener(l);
    }
}