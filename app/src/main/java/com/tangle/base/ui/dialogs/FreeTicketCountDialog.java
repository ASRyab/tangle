package com.tangle.base.ui.dialogs;

import android.annotation.SuppressLint;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tangle.R;

@SuppressLint("ValidFragment")
public class FreeTicketCountDialog extends TicketsCountDialog {
    public FreeTicketCountDialog(CountTicketListener listener, int tickets) {
        super(listener, tickets);
    }

    protected View getView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.layout_free_tickets_count, container, false);
    }
}
