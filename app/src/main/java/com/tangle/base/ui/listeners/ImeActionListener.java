package com.tangle.base.ui.listeners;

import android.view.KeyEvent;
import android.widget.EditText;
import android.widget.TextView;

public class ImeActionListener implements EditText.OnEditorActionListener {

    private int[] actionIds;
    private Runnable action;

    public ImeActionListener(Runnable action, int... actionIds) {
        this.actionIds = actionIds;
        this.action = action;
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        for (int id : actionIds) {
            if (id == actionId) {
                action.run();
            }
        }
        return false;
    }
}
