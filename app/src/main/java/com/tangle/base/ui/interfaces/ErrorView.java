package com.tangle.base.ui.interfaces;

public interface ErrorView {

    void setError(String error);

}
