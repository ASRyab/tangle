package com.tangle.base.ui.dialogs;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tangle.R;
import com.tangle.utils.own_user_photo.OwnPhotoPresenter;

@SuppressLint("ValidFragment")
public class PhotoUploadDialog extends BottomSheetDialogFragment implements View.OnClickListener {
    private Fragment fragmentHandler;
    private OwnPhotoPresenter ownPhotopresenter;

    public PhotoUploadDialog(Fragment fragment, OwnPhotoPresenter ownPhotopresenter) {
        this.ownPhotopresenter = ownPhotopresenter;
        this.fragmentHandler = fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_choose_add_photo, container, false);
        view.findViewById(R.id.txt_btn_take_photo).setOnClickListener(this);
        view.findViewById(R.id.txt_btn_from_gallery).setOnClickListener(this);
        return view;

    }

    @Override
    public void onPause() {
        super.onPause();
        dismiss();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_btn_take_photo:
                ownPhotopresenter.onTakePictureClicked(fragmentHandler);
                break;
            case R.id.txt_btn_from_gallery:
                ownPhotopresenter.onGalleryPickClicked(fragmentHandler);
                break;
        }
        dismiss();
    }
}
