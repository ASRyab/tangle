package com.tangle.base.ui.interfaces;

public interface LoadingView {
    void showLoading();

    void hideLoading();

    void showAPIError(String error);
}