package com.tangle.base.ui.interfaces;

public interface BackPressable {
    boolean onBackPressed();
}
