package com.tangle.base.ui.listeners;

public interface KeyboardListener {
    String ACTION_HIDE_KEYBOARD = "KeyboardWillHide";
    String ACTION_SHOW_KEYBOARD = "KeyboardWillShow";
    void attachKeyboardListeners();
}