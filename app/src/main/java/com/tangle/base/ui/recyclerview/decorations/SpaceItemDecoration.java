package com.tangle.base.ui.recyclerview.decorations;

import android.graphics.Rect;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public class SpaceItemDecoration extends RecyclerView.ItemDecoration {

    private int space, startSpace, endSpace;
    private int orientation;

    public SpaceItemDecoration(int space, int startSpace, int endSpace, int orientation) {
        this.space = space;
        this.startSpace = startSpace;
        this.endSpace = endSpace;
        this.orientation = orientation;
    }

    public SpaceItemDecoration(int space, int edgeSpace, @LinearLayoutCompat.OrientationMode int orientation) {
        this(space, edgeSpace, edgeSpace, orientation);
    }

    public SpaceItemDecoration(int space) {
        this(space, 0, 0, LinearLayoutCompat.VERTICAL);
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {

        final int itemPosition = parent.getChildAdapterPosition(view);
        final int itemCount = state.getItemCount();

        outRect.left = space;
        outRect.right = space;
        outRect.bottom = space;
        outRect.top = space;

        if (itemCount > 0) {
            if (itemPosition == itemCount - 1) {
                if (orientation == LinearLayoutManager.VERTICAL) {
                    outRect.bottom = endSpace;
                } else {
                    outRect.right = endSpace;
                }
            }

            if (itemPosition == 0) {
                if (orientation == LinearLayoutManager.VERTICAL) {
                    outRect.top = startSpace;
                } else {
                    outRect.left = startSpace;
                }
            }
        }
    }

}