package com.tangle.base.ui.interfaces;

public interface LocationCheckableView {
    void showRemindLocationDialog(Runnable action);
}
