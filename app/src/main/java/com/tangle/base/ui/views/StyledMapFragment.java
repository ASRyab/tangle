package com.tangle.base.ui.views;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.VisibleRegion;
import com.tangle.R;
import com.tangle.model.event.EventInfo;

public class StyledMapFragment extends SupportMapFragment implements OnMapReadyCallback {

    private static final float DEFAULT_ZOOM = 14.0f;

    private boolean gesturesEnabled;
    private float zoom = DEFAULT_ZOOM;
    private EventInfo event;
    private View.OnClickListener clickListener;
    private GoogleMap googleMap;
    private OnMapReadyCallback mapReadyCallback;
    private LatLng currentPosition;

    @Override
    public void onActivityCreated(Bundle bundle) {
        super.onActivityCreated(bundle);
        getMapAsync(this);
    }

    private void updateStyle() {
        googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.google_maps_default_style));
        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                View view = LayoutInflater.from(getActivity()).inflate(R.layout.layout_map_info, null, false);
                TextView infoTextView = view.findViewById(R.id.txt_info);
                String address = event.getEventAddress(getContext());
                infoTextView.setText(String.format("%s %s", address, event.getPostCode()));
                return view;
            }

            @Override
            public View getInfoContents(Marker marker) {
                return null;
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapClickListener(latLng -> onSafeClick());
        googleMap.setOnInfoWindowClickListener(marker -> onSafeClick());
        googleMap.setOnMarkerClickListener(marker -> {
            onSafeClick();
            return true;
        });
        updateStyle();
        updateScrollGestureEnabled();
        updateMarker();
        onSafeMapInitialized(googleMap);
    }

    private void onSafeClick() {
        if (clickListener != null) {
            clickListener.onClick(getView());
        }
    }

    private void onSafeMapInitialized(GoogleMap googleMap) {
        if (mapReadyCallback != null) {
            mapReadyCallback.onMapReady(googleMap);
        }
    }

    public void setOnMapClickListener(final View.OnClickListener clickListener) {
        this.clickListener = clickListener;
    }

    public void setMapReadyCallback(OnMapReadyCallback mapReadyCallback) {
        this.mapReadyCallback = mapReadyCallback;
    }

    public void setEvent(EventInfo event) {
        this.event = event;
        updateMarker();
    }

    private void updateMarker() {
        if (event != null && event.hasValidLocation()) {
            setMarker(event.getEventPosition());
        }
    }

    public void setZoom(float zoom) {
        this.zoom = zoom;
        updateZoom();
    }

    private void updateZoom() {
        if (googleMap != null && event != null) {
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(event.getEventPosition(), zoom));
        }
    }

    public void setGesturesEnabled(boolean gesturesEnabled) {
        this.gesturesEnabled = gesturesEnabled;
        updateScrollGestureEnabled();
    }


    private void updateScrollGestureEnabled() {
        if (googleMap != null) {
            googleMap.getUiSettings().setAllGesturesEnabled(gesturesEnabled);
        }
    }

    private void setMarker(LatLng position) {
        boolean equals = position.equals(currentPosition);
        if (googleMap != null && !equals) {
            currentPosition = position;
            googleMap.addMarker(new MarkerOptions()
                    .position(position)
                    .icon(BitmapDescriptorFactory.fromBitmap(getBitmapFromVectorDrawable(R.drawable.map_ic_pin))))
                    .showInfoWindow();
            centerCamera(position);
        }
    }

    private void centerCamera(LatLng position) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(position, zoom));
        VisibleRegion visibleRegion = googleMap.getProjection().getVisibleRegion();
        Point x = googleMap.getProjection().toScreenLocation(visibleRegion.farRight);
        Point y = googleMap.getProjection().toScreenLocation(visibleRegion.nearLeft);
        Point upperPoint = new Point(x.x / 2, y.y / 3);
        LatLng upperFromPoint = googleMap.getProjection().fromScreenLocation(upperPoint);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(upperFromPoint, zoom));
    }

    private Bitmap getBitmapFromVectorDrawable(int drawableId) {
        Drawable drawable = ContextCompat.getDrawable(getActivity(), drawableId);
        Bitmap bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

}