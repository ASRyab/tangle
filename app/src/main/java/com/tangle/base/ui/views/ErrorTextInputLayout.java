package com.tangle.base.ui.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.tangle.R;


public class ErrorTextInputLayout extends TextInputLayout {

    private int forceExpandedHintBlockFlags = 0;

    public ErrorTextInputLayout(Context context) {
        super(context);
        init();
    }

    public ErrorTextInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ErrorTextInputLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setErrorTextAppearance(R.style.ApplicationTheme_InputError);
        setHintTextAppearance(R.style.ApplicationTheme_InputHint);
        //fix for password
        setTypeface(ResourcesCompat.getFont(getContext(), R.font.lato_regular));
    }

    @Override
    public void addView(View child, int index, ViewGroup.LayoutParams params) {
        super.addView(child, index, params);

        if (child instanceof EditText) {
            ((EditText) child).addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!TextUtils.isEmpty(getError()) && TextUtils.isEmpty(s)) {
                        forceExpandedHintBlockFlags++;
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {
                    if (!TextUtils.isEmpty(getError()) && TextUtils.isEmpty(s)) {
                        forceExpandedHintBlockFlags--;
                    }
                }
            });
        }
    }

    @Override
    public void setError(@Nullable CharSequence error) {
        forceExpandedHintBlockFlags++;
        super.setError(error);
        forceExpandedHintBlockFlags--;
    }

    @Nullable
    @Override
    public CharSequence getError() {
        // the main hack is right here
        return forceExpandedHintBlockFlags > 0 ? null : super.getError();
    }

    @Override
    public void setHintTextAppearance(int resId) {
        forceExpandedHintBlockFlags++;
        super.setHintTextAppearance(resId);
        forceExpandedHintBlockFlags--;
    }

    @Override
    protected void drawableStateChanged() {
        forceExpandedHintBlockFlags++;
        super.drawableStateChanged();
        forceExpandedHintBlockFlags--;
    }
}
