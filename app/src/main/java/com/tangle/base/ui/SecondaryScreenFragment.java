package com.tangle.base.ui;

import android.content.Context;

import com.tangle.ApplicationLoader;
import com.tangle.base.ScreenMover;
import com.tangle.base.SecondaryScreenFragmentOperations;
import com.tangle.base.mvp.MVPFragment;
import com.tangle.base.ui.interfaces.LocationCheckableView;
import com.tangle.managers.DialogManager;
import com.tangle.managers.PhotoManager;
import com.tangle.screen.main.ActivityHolder;
import com.tangle.screen.main.ScreenJump;
import com.tangle.screen.main.navigation_menu.NavigationMenuState;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

public abstract class SecondaryScreenFragment extends MVPFragment implements PhotoManager.PhotoView, LocationCheckableView, ScreenMover {
    protected SecondaryScreenFragmentOperations fragmentOperations;

    @Override
    protected void onAttachToContext(Context context) {
        super.onAttachToContext(context);
        if (context instanceof SecondaryScreenFragmentOperations) {
            fragmentOperations = (SecondaryScreenFragmentOperations) context;
        }
    }

    public void moveToMenu(@NavigationMenuState int index) {
        fragmentOperations.moveToMenu(index);
        clearStack();
    }

    public void clearStack() {
        fragmentOperations.clearStack();
    }

    @Override
    public void openPhotoChooseScreen() {
        ApplicationLoader.getApplicationInstance().getNavigationManager().showPhotoUpload((ActivityHolder) Objects.requireNonNull(getActivity()));
    }

    @Override
    public void showNoApprovePhoto() {
        DialogManager.showNoApprovePhotoToInvite((BaseActivity) Objects.requireNonNull(getActivity()));
    }

    @Override
    public void showRemindLocationDialog(Runnable action) {
        DialogManager.getInstance(this).showRemindLocation((v, dialog) -> action.run());
    }

    @Override
    public void moveToScreen(@NotNull SecondaryScreenFragment fragment) {
        ((ActivityHolder) Objects.requireNonNull(getActivity())).moveToScreen(fragment);
    }

    @Override
    public void moveToScreen(@NotNull SecondaryScreenFragment fragment, @NotNull ScreenJump screenJump) {
        ((ActivityHolder) Objects.requireNonNull(getActivity())).moveToScreen(fragment, screenJump);
    }
}