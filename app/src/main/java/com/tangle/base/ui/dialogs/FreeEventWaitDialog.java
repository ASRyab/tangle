package com.tangle.base.ui.dialogs;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.model.event.EventInfo;

@SuppressLint("ValidFragment")
public class FreeEventWaitDialog extends BottomSheetDialogFragment {

    private EventInfo eventInfo;

    public FreeEventWaitDialog() {
    }

    public FreeEventWaitDialog(EventInfo info) {
        eventInfo = info;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_free_event_wait, container, false);
        if (eventInfo == null) {
            dismiss();
        } else {
            TextView tvTitle = view.findViewById(R.id.tvTitle);
            tvTitle.setText(getString(R.string.free_event_dialog_title, eventInfo.title));
            view.findViewById(R.id.btn_ok).setOnClickListener(view1 -> dismiss());
        }
        return view;
    }
}
