package com.tangle.base.ui.views.video_view;

import android.graphics.Matrix;

public class ScaleManager {

    private Size viewSize;
    private Size videoSize;

    public ScaleManager(Size viewSize, Size videoSize) {
        this.viewSize = viewSize;
        this.videoSize = videoSize;
    }

    public Matrix getScaleMatrix(ScalableType scalableType) {
        switch (scalableType) {
            case NONE:
                return getNoScale();
            case CENTER_INSIDE:
                return centerInside();
            case CENTER_CROP:
                return getCropScale();
            case FIT_CENTER:
                return fitCenter();
            default:
                return null;
        }
    }

    private Matrix getMatrix(float sx, float sy, float px, float py) {
        Matrix matrix = new Matrix();
        matrix.setScale(sx, sy, px, py);
        return matrix;
    }

    private Matrix getNoScale() {
        float sx = videoSize.getWidth() / (float) viewSize.getWidth();
        float sy = videoSize.getHeight() / (float) viewSize.getHeight();
        return getMatrix(sx, sy, 0, 0);
    }

    private Matrix getFitScale() {
        float sx = (float) viewSize.getWidth() / videoSize.getWidth();
        float sy = (float) viewSize.getHeight() / videoSize.getHeight();
        float minScale = Math.min(sx, sy);
        sx = minScale / sx;
        sy = minScale / sy;
        return getMatrix(sx, sy, viewSize.getWidth() / 2f, viewSize.getHeight() / 2f);
    }

    private Matrix fitCenter() {
        return getFitScale();
    }

    private Matrix getOriginalScale() {
        float sx = videoSize.getWidth() / (float) viewSize.getWidth();
        float sy = videoSize.getHeight() / (float) viewSize.getHeight();
        return getMatrix(sx, sy, viewSize.getWidth() / 2f, viewSize.getHeight() / 2f);
    }

    private Matrix getCropScale() {
        float sx = (float) viewSize.getWidth() / videoSize.getWidth();
        float sy = (float) viewSize.getHeight() / videoSize.getHeight();
        float maxScale = Math.max(sx, sy);
        sx = maxScale / sx;
        sy = maxScale / sy;
        return getMatrix(sx, sy, viewSize.getWidth() / 2f, viewSize.getHeight() / 2f);
    }

    private Matrix centerInside() {
        if (videoSize.getHeight() <= viewSize.getWidth()
                && videoSize.getHeight() <= viewSize.getHeight()) {
            return getOriginalScale();
        } else {
            return fitCenter();
        }
    }
}