package com.tangle.base.ui.views;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.DrawableContainer;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.tangle.R;
import com.tangle.base.ui.views.interpolators.AdvancedBounceInterpolator;
import com.tangle.utils.PlatformUtils;

public class ButtonShaper {
    private Shape shape = Shape.RECTANGLE;
    private int shapeColor, textColor, shapePressedColor, textPressedColor;
    private ObjectAnimator xScaleUp, yScaleUp, xScaleDown, yScaleDown;
    private boolean bounce;
    private View button;

    private final static float SCALE_MIN = 1f;
    private final static float SCALE_MAX = 1.025f;

    public enum Shape {

        ELLIPSE(28, 2), RECTANGLE(8, 2);

        Shape(float radiusDp, float dashWidthDp) {
            this.radiusDp = radiusDp;
            this.dashWidthDp = dashWidthDp;
        }

        float radiusDp;
        float dashWidthDp;
    }

    public void setMainTextColor(int mainColor) {
        textColor = ContextCompat.getColor(button.getContext(), mainColor);
        textPressedColor = ContextCompat.getColor(button.getContext(), mainColor);
    }

    public void setShapeColor(int color) {
        shapeColor = ContextCompat.getColor(button.getContext(), color);
        shapePressedColor = shapeColor;
    }

    public void init(View button) {
        this.button = button;
        setMainTextColor(R.color.colorPrimary);
        setShapeColor(R.color.colorPrimary);
        initBounce();
    }

    public void init(View button, AttributeSet attrs) {
        init(button);
        if (attrs != null) {
            readAttrs(attrs);
        }
        applyParam();

    }


    protected void readAttrs(AttributeSet attrs) {

        TypedArray typedArray = button.getContext().obtainStyledAttributes(attrs, R.styleable.ShapeButton);
        shape = Shape.values()[typedArray.getInt(R.styleable.ShapeButton_btn_shape, Shape.RECTANGLE.ordinal())];
        bounce = typedArray.getBoolean(R.styleable.ShapeButton_btn_bounce, true);
        shapeColor = typedArray.getColor(R.styleable.ShapeButton_btn_shapeColor, shapeColor);
        shapePressedColor = typedArray.getColor(R.styleable.ShapeButton_btn_shapeColorPressed, shapePressedColor);
        textColor = typedArray.getColor(R.styleable.ShapeButton_btn_textColor, textColor);
        textPressedColor = typedArray.getColor(R.styleable.ShapeButton_btn_textColorPressed, textPressedColor);
        shape.radiusDp = PlatformUtils.Dimensions.pixelsToDp(button.getContext(),
                typedArray.getDimensionPixelSize(R.styleable.ShapeButton_btn_radius,
                        (int) PlatformUtils.Dimensions.dipToPixels(button.getContext(), shape.radiusDp)));
        shape.dashWidthDp = PlatformUtils.Dimensions.pixelsToDp(button.getContext(),
                typedArray.getDimensionPixelSize(R.styleable.ShapeButton_btn_strokeWidth,
                        (int) PlatformUtils.Dimensions.dipToPixels(button.getContext(), shape.dashWidthDp)));
        typedArray.recycle();
    }

    private void setShape() {
        DrawableContainer.DrawableContainerState dcs = (DrawableContainer.DrawableContainerState) button.getBackground().getConstantState();
        Drawable[] drawableItems = dcs.getChildren();
        GradientDrawable gradientDrawablePressed = (GradientDrawable) drawableItems[0];
        GradientDrawable gradientDrawableFocused = (GradientDrawable) drawableItems[1];
        GradientDrawable gradientDrawableSelected = (GradientDrawable) drawableItems[2];
        GradientDrawable gradientDrawable = (GradientDrawable) drawableItems[3];
        int width = (int) PlatformUtils.Dimensions.dipToPixels(button.getContext(), shape.dashWidthDp);
        int radius = (int) PlatformUtils.Dimensions.dipToPixels(button.getContext(), shape.radiusDp);
        gradientDrawablePressed.setStroke(width, shapePressedColor);
        gradientDrawablePressed.setCornerRadius(radius);
        gradientDrawableFocused.setStroke(width, shapePressedColor);
        gradientDrawableFocused.setCornerRadius(radius);
        gradientDrawableSelected.setStroke(width, shapePressedColor);
        gradientDrawableSelected.setCornerRadius(radius);
        gradientDrawable.setStroke(width, shapeColor);
        gradientDrawable.setCornerRadius(radius);
    }

    private StateListDrawable makeDrawableSelector() {
        int shapeRes = R.drawable.view_shape_button;
        StateListDrawable res = new StateListDrawable();
        res.addState(new int[]{android.R.attr.state_pressed}, ContextCompat.getDrawable(button.getContext(), shapeRes).mutate());
        res.addState(new int[]{android.R.attr.state_focused}, ContextCompat.getDrawable(button.getContext(), shapeRes).mutate());
        res.addState(new int[]{android.R.attr.state_selected}, ContextCompat.getDrawable(button.getContext(), shapeRes).mutate());
        res.addState(new int[]{}, ContextCompat.getDrawable(button.getContext(), shapeRes).mutate());
        return res;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initBounce() {
        xScaleUp = ObjectAnimator.ofFloat(button, View.SCALE_X, SCALE_MAX);
        xScaleDown = ObjectAnimator.ofFloat(button, View.SCALE_X, SCALE_MIN);
        yScaleUp = ObjectAnimator.ofFloat(button, View.SCALE_Y, SCALE_MAX);
        yScaleDown = ObjectAnimator.ofFloat(button, View.SCALE_Y, SCALE_MIN);

        AdvancedBounceInterpolator interpolator = new AdvancedBounceInterpolator(0.2f, 15);
        AnimatorSet scaleUpSet = new AnimatorSet();
        scaleUpSet.playTogether(xScaleUp, yScaleUp);
        int DEFAULT_BOUNCE_DURATION = 400;
        scaleUpSet.setDuration(DEFAULT_BOUNCE_DURATION);
        scaleUpSet.setInterpolator(interpolator);
        AnimatorSet scaleDownSet = new AnimatorSet();
        scaleDownSet.playTogether(xScaleDown, yScaleDown);
        scaleDownSet.setDuration(DEFAULT_BOUNCE_DURATION);
        scaleDownSet.setInterpolator(interpolator);
        button.setOnTouchListener((v, event) -> {
            if (bounce) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        scaleDownSet.cancel();
                        scaleUpSet.setupStartValues();
                        scaleUpSet.start();
                        break;
                    case MotionEvent.ACTION_UP:
                        scaleUpSet.cancel();
                        scaleDownSet.setupStartValues();
                        scaleDownSet.start();
                        break;
                }
            }
            return false;
        });
    }

    public void setEnabled(boolean enabled) {
        if (!enabled) {
            setMainTextColor(R.color.colorTextSecondary);
            setShapeColor(R.color.colorTextSecondary);
            applyParam();
        } else {
            init(button);
            applyParam();
        }
    }

    public void applyParam() {
        button.setBackgroundDrawable(makeDrawableSelector());
        setShape();
        setTextColor();
    }


    private void setTextColor() {
        if (button instanceof ShapeButton) {
            ColorStateList colorStateList = new ColorStateList(
                    new int[][]{
                            new int[]{android.R.attr.state_pressed},
                            new int[]{android.R.attr.state_focused},
                            new int[]{android.R.attr.state_selected},
                            new int[]{}},
                    new int[]{
                            textPressedColor,
                            textPressedColor,
                            textPressedColor,
                            textColor});
            ((ShapeButton) button).setTextColor(colorStateList);
        }
    }

}
