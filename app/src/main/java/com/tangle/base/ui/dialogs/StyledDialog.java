package com.tangle.base.ui.dialogs;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.base.ui.views.ShapeButton;

import io.reactivex.annotations.NonNull;

public class StyledDialog extends DialogFragment {
    private TextView txtViewTitle, txtViewBody;
    private ShapeButton leftActionButton, rightActionButton;
    private ImageView ivIcon;

    private final String title;
    private final Spannable spannableBody;
    private final Integer bodyTextSize;
    private final String leftActionBtnText;
    private final String rightActionBtnText;
    private final Integer actionBtnMainColor;
    private final DialogButtonClickListener leftActionBtnClickListener;
    private final DialogButtonClickListener rightActionBtnClickListener;

    private BodySpanTextClickListener bodySpanTextClickListener;
    private int iconRes;

    public StyledDialog() {
        this(null, null, null, null, null, null, null, null, null, null, 0);
    }

    @SuppressLint("ValidFragment")
    public StyledDialog(String title, String body, String leftActionBtnText, String rightActionBtnText,
                        DialogButtonClickListener leftActionBtnClickListener, DialogButtonClickListener rightActionBtnClickListener,
                        String bodySpanText,
                        Integer bodyTextSize,
                        BodySpanTextClickListener bodySpanTextClickListener,
                        Integer actionBtnMainColor,
                        int iconRes) {

        this.title = title;
        this.leftActionBtnText = leftActionBtnText;
        this.rightActionBtnText = rightActionBtnText;

        if (!TextUtils.isEmpty(body)) {
            this.spannableBody = initSpannableBody(body, bodySpanText);
        } else {
            this.spannableBody = null;
        }
        this.actionBtnMainColor = actionBtnMainColor;
        this.leftActionBtnClickListener = leftActionBtnClickListener;
        this.rightActionBtnClickListener = rightActionBtnClickListener;
        this.bodySpanTextClickListener = bodySpanTextClickListener;
        this.bodyTextSize = bodyTextSize;
        this.iconRes = iconRes;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.StyledDialog);
    }

    private SpannableString initSpannableBody(String body, String spanText) {
        SpannableString spannableBody;
        if (TextUtils.isEmpty(spanText)) {
            spannableBody = new SpannableString(body);
        } else {
            body = String.format(body, spanText);
            int start = body.indexOf(spanText);
            int finish = start + spanText.length();
            spannableBody = new SpannableString(body);
            spannableBody.setSpan(new ClickableSpan() {
                @Override
                public void onClick(final View view) {
                    if (bodySpanTextClickListener != null) {
                        bodySpanTextClickListener.onBodySpanTextClick(view);
                    }
                }

                @Override
                public void updateDrawState(TextPaint ds) {
                    ds.setColor(ds.linkColor);
                    ds.setUnderlineText(false);
                }
            }, start, finish, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return spannableBody;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_styled, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initUI();
    }

    private void initUI() {
        if (TextUtils.isEmpty(title)) {
            dismiss();
        }
        View parentView = getView();
        if (parentView != null) {
            txtViewTitle = parentView.findViewById(R.id.txt_view_title);
            setTitleText(title);
            txtViewBody = parentView.findViewById(R.id.txt_view_body);
            if (bodyTextSize != null) {
                txtViewBody.setTextSize(bodyTextSize);
            }
            setBodyText(spannableBody);

            leftActionButton = parentView.findViewById(R.id.left_action_btn);
            rightActionButton = parentView.findViewById(R.id.right_action_btn);
            ivIcon = parentView.findViewById(R.id.img_icon);
            setActionBtnMainColor(actionBtnMainColor);
            setButtonsText(leftActionBtnText, rightActionBtnText);

            setLeftActionBtnListener(view -> {
                if (leftActionBtnClickListener != null) {
                    leftActionBtnClickListener.onDialogButtonClick(view, StyledDialog.this);
                }
                dismiss();
            });
            setRightActionBtnListener(view -> {
                if (rightActionBtnClickListener != null) {
                    rightActionBtnClickListener.onDialogButtonClick(view, StyledDialog.this);
                }
                dismiss();
            });

            setBodySpanTextClickListener(bodySpanTextClickListener);

            if (iconRes != 0) {
                ivIcon.setVisibility(View.VISIBLE);
                ivIcon.setImageResource(iconRes);
            }
        }
    }

    public void setTitleText(@NonNull String title) {
        if (!TextUtils.isEmpty(title)) {
            txtViewTitle.setVisibility(View.VISIBLE);
            txtViewTitle.setText(title);
        }
    }

    public void setBodyText(String bodyText) {
        setBodyText(new SpannableString(bodyText));
    }

    public void setBodyText(Spannable spannableBody) {
        if (!TextUtils.isEmpty(spannableBody)) {
            txtViewBody.setVisibility(View.VISIBLE);
            txtViewBody.setMovementMethod(LinkMovementMethod.getInstance());
            txtViewBody.setText(spannableBody);
        }
    }

    public void setActionBtnMainColor(Integer actionBtnMainColor) {
        if (actionBtnMainColor != null) {
            leftActionButton.getButtonShaper().setMainTextColor(actionBtnMainColor);
            leftActionButton.getButtonShaper().setShapeColor(actionBtnMainColor);
            leftActionButton.getButtonShaper().applyParam();

            rightActionButton.getButtonShaper().setMainTextColor(actionBtnMainColor);
            rightActionButton.getButtonShaper().setShapeColor(actionBtnMainColor);
            rightActionButton.getButtonShaper().applyParam();
        }
    }

    private void setButtonsText(String leftActionBtnText, String rightActionBtnText) {
        if (!TextUtils.isEmpty(leftActionBtnText) && !TextUtils.isEmpty(rightActionBtnText)) {
            ((LinearLayout.LayoutParams) leftActionButton.getLayoutParams()).weight = 1;
            ((LinearLayout.LayoutParams) leftActionButton.getLayoutParams()).width = 0;
            ((LinearLayout.LayoutParams) rightActionButton.getLayoutParams()).weight = 1;
            ((LinearLayout.LayoutParams) rightActionButton.getLayoutParams()).width = 0;
        }

        if (!TextUtils.isEmpty(leftActionBtnText)) {
            leftActionButton.setVisibility(View.VISIBLE);
            leftActionButton.setText(leftActionBtnText);
        }
        if (!TextUtils.isEmpty(rightActionBtnText)) {
            rightActionButton.setVisibility(View.VISIBLE);
            rightActionButton.setText(rightActionBtnText);
        }
    }

    public void setLeftActionBtnListener(View.OnClickListener l) {
        leftActionButton.setOnClickListener(l);
    }

    public void setRightActionBtnListener(View.OnClickListener l) {
        rightActionButton.setOnClickListener(l);
    }

    public void setBodySpanTextClickListener(BodySpanTextClickListener bodySpanTextClickListener) {
        this.bodySpanTextClickListener = bodySpanTextClickListener;
    }

    public static class Builder {
        private String title;
        private String body;
        private String bodySpanText;
        private Integer bodyTextSize;
        private String leftActionBtnText;
        private String rightActionBtnText;
        private Integer actionBtnMainColor;
        private DialogButtonClickListener leftActionBtnClickListener;
        private DialogButtonClickListener rightActionBtnClickListener;
        private BodySpanTextClickListener bodySpanTextClickListener;

        private int iconRes;
        private boolean cancelable = true;

        public Builder(String title) {
            this.title = title;
        }

        public Builder setBodyText(String body) {
            this.body = body;

            return this;
        }

        public Builder setBodySpanText(String bodySpanText) {
            this.bodySpanText = bodySpanText;

            return this;
        }

        /**
         * @param bodyTextSize in sp.
         */
        public Builder setBodyTextSize(int bodyTextSize) {
            this.bodyTextSize = bodyTextSize;

            return this;
        }

        public Builder setLeftActionBtnText(String btnText) {
            this.leftActionBtnText = btnText;

            return this;
        }

        public Builder setRightActionBtnText(String btnText) {
            this.rightActionBtnText = btnText;

            return this;
        }

        public Builder setActionBtnMainColor(int actionBtnMainColor) {
            this.actionBtnMainColor = actionBtnMainColor;

            return this;
        }

        public Builder setLeftActionBtnListener(DialogButtonClickListener l) {
            this.leftActionBtnClickListener = l;

            return this;
        }

        public Builder setRightActionBtnListener(DialogButtonClickListener l) {
            this.rightActionBtnClickListener = l;

            return this;
        }

        public Builder setBodySpanTextClickListener(BodySpanTextClickListener bodySpanTextClickListener) {
            this.bodySpanTextClickListener = bodySpanTextClickListener;

            return this;
        }

        public Builder setIconRes(int iconRes) {
            this.iconRes = iconRes;
            return this;
        }

        public StyledDialog build() {
            StyledDialog dialog = new StyledDialog(title, body, leftActionBtnText, rightActionBtnText,
                    leftActionBtnClickListener, rightActionBtnClickListener,
                    bodySpanText,
                    bodyTextSize,
                    bodySpanTextClickListener,
                    actionBtnMainColor,
                    iconRes);
            dialog.setCancelable(cancelable);
            return dialog;

        }

        public Builder setCancelable(boolean cancelable) {
            this.cancelable = cancelable;
            return this;
        }
    }

    public interface BodySpanTextClickListener {
        void onBodySpanTextClick(View v);
    }

    public interface DialogButtonClickListener {
        void onDialogButtonClick(View v, StyledDialog dialog);
    }
}