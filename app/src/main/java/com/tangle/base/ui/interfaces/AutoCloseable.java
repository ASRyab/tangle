package com.tangle.base.ui.interfaces;

public interface AutoCloseable {
    void close();
}
