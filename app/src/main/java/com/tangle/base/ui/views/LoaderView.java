package com.tangle.base.ui.views;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.tangle.R;

public class LoaderView extends View {

    private static final int TRANSITION_ANIM_DURATION_MILLIS = 500;
    private static final int START_COLOR = Color.parseColor("#46494c");
    private static final int END_COLOR = Color.parseColor("#1b1c1d");

    private ValueAnimator startColorAnimator, endColorAnimator;
    private GradientDrawable drawable;
    private int radius;

    private int[] colors = new int[2];

    public LoaderView(Context context) {
        super(context);
        init();
    }

    public LoaderView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        applyAttrs(attrs);
        init();

    }

    public LoaderView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        applyAttrs(attrs);
        init();
    }

    public LoaderView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        applyAttrs(attrs);
        init();
    }

    protected void readAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.LoaderView);
        radius = typedArray.getDimensionPixelSize(R.styleable.LoaderView_lv_radius, 0);
        typedArray.recycle();
    }

    protected void applyAttrs(AttributeSet attrs) {
        if (attrs != null) {
            readAttrs(attrs);
        }
    }

    private void init() {
        drawable = (GradientDrawable) ContextCompat.getDrawable(getContext(), R.drawable.preloader_gradient).mutate();
        drawable.setCornerRadius(radius);
        setBackground(drawable);
        if (getVisibility() == VISIBLE) {
            startAnimation();
        }
    }

    private void startAnimation() {
        if (startColorAnimator != null && endColorAnimator != null) return;
        startColorAnimator = ValueAnimator.ofObject(new ArgbEvaluator(), START_COLOR, END_COLOR);
        endColorAnimator = ValueAnimator.ofObject(new ArgbEvaluator(), END_COLOR, START_COLOR);
        startColorAnimator.addUpdateListener(animation -> {
            colors[0] = (int) animation.getAnimatedValue();
            drawable.setColors(colors);
        });
        endColorAnimator.addUpdateListener(animation -> {
            colors[1] = (int) animation.getAnimatedValue();
            drawable.setColors(colors);
        });
        startColorAnimator.setDuration(TRANSITION_ANIM_DURATION_MILLIS);
        endColorAnimator.setDuration(TRANSITION_ANIM_DURATION_MILLIS);
        startColorAnimator.setRepeatMode(ValueAnimator.REVERSE);
        endColorAnimator.setRepeatMode(ValueAnimator.REVERSE);
        startColorAnimator.setRepeatCount(ValueAnimator.INFINITE);
        endColorAnimator.setRepeatCount(ValueAnimator.INFINITE);
        startColorAnimator.start();
        endColorAnimator.start();
    }

    private void cancelAnimation() {
        if (startColorAnimator != null && startColorAnimator.isStarted()) {
            startColorAnimator.cancel();
        }
        if (endColorAnimator != null && endColorAnimator.isStarted()) {
            endColorAnimator.cancel();
        }
        startColorAnimator = null;
        endColorAnimator = null;
    }

    @Override
    public void setVisibility(int visibility) {
        if (getVisibility() == visibility) return;
        super.setVisibility(visibility);
        if (visibility == VISIBLE) {
            startAnimation();
        } else {
            cancelAnimation();
        }
    }
}
