package com.tangle.base.ui.views.pagers;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class VerticalViewPager extends ViewPager {
    private SwipeListener listener;
    private float startDragY, startDragX;
    private final int SWIPE_THRESHOLD = 20;
    private boolean swipePerformed;
    private boolean expanded;

    public VerticalViewPager(Context context) {
        super(context);
        init();
    }

    public VerticalViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        setPageTransformer(true, new VerticalPageTransformer());
        setOverScrollMode(OVER_SCROLL_NEVER);
    }

    private class VerticalPageTransformer implements ViewPager.PageTransformer {

        @Override
        public void transformPage(View view, float position) {

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 1) { // [-1,1]
                view.setAlpha(1);

                // Counteract the default slide transition
                view.setTranslationX(view.getWidth() * -position);

                //set Y position to swipe in from top
                float yPosition = position * view.getHeight();
                view.setTranslationY(yPosition);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }

    public void setListener(SwipeListener listener) {
        this.listener = listener;
    }

    /**
     * Swaps the X and Y coordinates of your touch event.
     */
    private MotionEvent swapXY(MotionEvent ev) {
        float width = getWidth();
        float height = getHeight();

        float newX = (ev.getY() / height) * width;
        float newY = (ev.getX() / width) * height;

        ev.setLocation(newX, newY);

        return ev;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        boolean intercepted = super.onInterceptTouchEvent(swapXY(ev));
        swapXY(ev); // return touch coordinates to original reference frame for any child views
        float currentY = ev.getY();
        float currentX = ev.getX();
        if (expanded)
            getParent().requestDisallowInterceptTouchEvent(true);
        switch (ev.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                swipePerformed = false;
                startDragY = currentY;
                startDragX = currentX;
                break;
            }
            case MotionEvent.ACTION_MOVE: {
                if (startDragY < currentY - SWIPE_THRESHOLD && getCurrentItem() == 0 && listener != null && isEnabled()) {
                    listener.onSwipeFromTop();
                }
                if (Math.abs(startDragX - currentX) > SWIPE_THRESHOLD || Math.abs(startDragY - currentY) > SWIPE_THRESHOLD) {
                    swipePerformed = true;
                }
                break;
            }
            case MotionEvent.ACTION_UP: {
                if (Math.abs(startDragX - currentX) > SWIPE_THRESHOLD || Math.abs(startDragY - currentY) > SWIPE_THRESHOLD) {
                    swipePerformed = true;
                }
                if (swipePerformed) {
                    intercepted = true;
                }
                swipePerformed = false;
                break;
            }
        }
        return intercepted;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return super.onTouchEvent(swapXY(ev));
    }

    public interface SwipeListener {
        void onSwipeFromTop();
    }

}