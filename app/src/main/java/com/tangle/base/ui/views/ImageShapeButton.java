package com.tangle.base.ui.views;

import android.content.Context;
import android.support.v7.widget.AppCompatImageButton;
import android.util.AttributeSet;

public class ImageShapeButton extends AppCompatImageButton {

    private ButtonShaper shaper = new ButtonShaper();

    public ImageShapeButton(Context context) {
        super(context);
        shaper.init(this);
    }

    public ImageShapeButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        shaper.init(this, attrs);
    }

    public ImageShapeButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        shaper.init(this, attrs);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        shaper.setEnabled(enabled);
    }
}
