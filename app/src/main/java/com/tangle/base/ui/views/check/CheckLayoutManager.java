package com.tangle.base.ui.views.check;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;

public class CheckLayoutManager extends GridLayoutManager {

    private static final int DEFAULT_SPAN_COUNT = 3;

    public CheckLayoutManager(Context context) {
        super(context, DEFAULT_SPAN_COUNT);
    }
}
