package com.tangle.base.ui.views.video_view;

public enum ScalableType {
    NONE,
    CENTER_INSIDE,
    CENTER_CROP,
    FIT_CENTER
}