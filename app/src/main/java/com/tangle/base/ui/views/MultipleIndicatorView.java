package com.tangle.base.ui.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.tangle.R;
import com.tangle.base.ui.views.pagers.LinePageIndicator;

public class MultipleIndicatorView extends FrameLayout {

    private boolean isSquare = true;
    private ViewPager viewPager;
    private LinePageIndicator pageIndicator;

    public MultipleIndicatorView(Context context) {
        super(context);
        init();
    }

    public MultipleIndicatorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        applyAttrs(attrs);
        init();
    }

    private void applyAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.MultipleIndicatorView);
        isSquare = typedArray.getBoolean(R.styleable.MultipleIndicatorView_miv_square, true);
        typedArray.recycle();
    }

    private void init() {
        View child = LayoutInflater.from(getContext()).inflate(R.layout.view_container_with_indicator, this, false);
        viewPager = child.findViewById(R.id.pager);
        pageIndicator = child.findViewById(R.id.indicator);
        addView(child);
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (isSquare) {
            super.onMeasure(widthMeasureSpec, widthMeasureSpec);
        } else {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }
    }

    public void setPagerAdapter(PagerAdapter adapter) {
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(adapter.getCount());
        pageIndicator.setViewPager(viewPager);
        updateIndicator();
    }

    public void setOffscreenPageLimit(int limit) {
        viewPager.setOffscreenPageLimit(limit);
    }

    public void updateIndicator() {
        PagerAdapter adapter = viewPager.getAdapter();
        int count = adapter == null ? 0 : adapter.getCount();
        pageIndicator.post(() -> {
            if (count > 1) {
                pageIndicator.setLineWidth((pageIndicator.getWidth() - (pageIndicator.getGapWidth() * count)) / count);
                pageIndicator.setVisibility(VISIBLE);
            } else {
                pageIndicator.setVisibility(INVISIBLE);
            }
        });
    }

    public void setCurrentItem(int index) {
        viewPager.setCurrentItem(index, false);
        pageIndicator.setCurrentItem(index);
    }

}