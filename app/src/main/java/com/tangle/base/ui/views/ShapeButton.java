package com.tangle.base.ui.views;

import android.content.Context;
import android.util.AttributeSet;

public class ShapeButton extends android.support.v7.widget.AppCompatButton {
    private ButtonShaper shaper = new ButtonShaper();

    public ButtonShaper getButtonShaper() {
        return shaper;
    }

    public ShapeButton(Context context) {
        super(context);
        shaper.init(this);
    }

    public ShapeButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        shaper.init(this, attrs);
    }

    public ShapeButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        shaper.init(this, attrs);
    }


    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        setShapeEnabled(enabled);
    }

    public void setShapeEnabled(boolean enabled) {
        shaper.setEnabled(enabled);
    }


}
