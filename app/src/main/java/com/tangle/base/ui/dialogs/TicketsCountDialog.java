package com.tangle.base.ui.dialogs;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tangle.R;

@SuppressLint("ValidFragment")
public class TicketsCountDialog extends BottomSheetDialogFragment implements View.OnClickListener {

    private CountTicketListener ticketListener;
    private int maxTickets;
    private int actualCount = 1;
    private TextView vActualCount;
    private TextView minus;
    private TextView plus;

    public TicketsCountDialog() {
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (maxTickets == 0) {
            dismiss();
        }
    }

    @SuppressLint("ValidFragment")
    public TicketsCountDialog(CountTicketListener ticketListener, int maxTickets) {
        this.ticketListener = ticketListener;
        this.maxTickets = maxTickets;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = getView(inflater, container);
        view.findViewById(R.id.close_dialog).setOnClickListener(this);
        view.findViewById(R.id.btn_book).setOnClickListener(this);
        minus = view.findViewById(R.id.minus);
        minus.setOnClickListener(this);
        plus = view.findViewById(R.id.plus);
        plus.setOnClickListener(this);
        vActualCount = view.findViewById(R.id.tv_ticket_count);
        vActualCount.setText(String.valueOf(actualCount));
        return view;


    }

    protected View getView(LayoutInflater inflater, @Nullable ViewGroup container) {
        return inflater.inflate(R.layout.layout_tikets_counter, container, false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.close_dialog:
                dismiss();
                break;
            case R.id.btn_book:
                ticketListener.countTicketListener(actualCount);
                dismiss();
                break;
            case R.id.minus:
                if (actualCount > 1) {
                    vActualCount.setText(String.valueOf(--actualCount));
                }
                checkBackground();
                break;
            case R.id.plus:
                if (actualCount < maxTickets) {
                    vActualCount.setText(String.valueOf(++actualCount));
                }
                checkBackground();
                break;
        }
    }

    private void checkBackground() {
        checkButton(1, minus);
        checkButton(maxTickets, plus);
    }

    private void checkButton(int value, TextView button) {
        if (actualCount == value) {
            button.setTextColor(getResources().getColor(R.color.colorSecondary));
        } else {
            button.setTextColor(getResources().getColor(R.color.colorTextPrimary));
        }
    }

    public interface CountTicketListener {
        void countTicketListener(int count);
    }

}
