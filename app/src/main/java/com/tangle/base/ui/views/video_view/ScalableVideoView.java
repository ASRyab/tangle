package com.tangle.base.ui.views.video_view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Matrix;
import android.graphics.SurfaceTexture;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Surface;
import android.view.TextureView;

import com.tangle.R;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class ScalableVideoView extends TextureView implements TextureView.SurfaceTextureListener,
        MediaPlayer.OnVideoSizeChangedListener {

    protected MediaPlayer mediaPlayer;
    protected ScalableType scalableType = ScalableType.NONE;
    private ExecutorService singleExecutor;
    private boolean isBrandBlogVideo;

    public ScalableVideoView(Context context) {
        this(context, null);
    }

    public ScalableVideoView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScalableVideoView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        if (attrs == null) {
            return;
        }

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.scaleStyle, 0, 0);
        if (a == null) {
            return;
        }
        int scaleType = a.getInt(R.styleable.scaleStyle_scalableType, ScalableType.NONE.ordinal());

        a.recycle();
        scalableType = ScalableType.values()[scaleType];
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int width, int height) {
        Surface surface = new Surface(surfaceTexture);
        if (mediaPlayer != null) {
            mediaPlayer.setSurface(surface);
        }
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {
    }

    @Override
    public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
        scaleVideoSize(width, height);
    }

    private void scaleVideoSize(int videoWidth, int videoHeight) {
        if (videoWidth == 0 || videoHeight == 0) {
            return;
        }
        Size viewSize = new Size(getWidth(), getHeight());
        Size videoSize;
        if (isBrandBlogVideo) {
            videoSize = new Size(videoHeight, videoWidth);
        } else {
            videoSize = new Size(videoWidth, videoHeight);
        }
        ScaleManager scaleManager = new ScaleManager(viewSize, videoSize);
        Matrix matrix = scaleManager.getScaleMatrix(scalableType);
        if (matrix != null) {
            setTransform(matrix);
        }
    }

    private void initializeMediaPlayer() {
        if (mediaPlayer == null) {
            mediaPlayer = new MediaPlayer();
            mediaPlayer.setOnVideoSizeChangedListener(this);

            setSurfaceTextureListener(this);
        } else {
            reset();
        }
    }

    public void setDataSource(@NonNull String path) throws IOException {
        initializeMediaPlayer();
        mediaPlayer.setDataSource(path);
    }

    public void setDataSource(Context context, @NonNull Uri path) throws IOException {
        initializeMediaPlayer();
        mediaPlayer.setDataSource(context, path);
    }

    public void setIsBrandBlog(boolean isBrandBlogVideo) {
        this.isBrandBlogVideo = isBrandBlogVideo;
    }

    private void invalidateExecutor() {
        if (singleExecutor == null || singleExecutor.isShutdown()) {
            singleExecutor = Executors.newSingleThreadExecutor();
        }
    }

    public void setDataSourceAsync(@NonNull String path, Callback callback) {
        invalidateExecutor();
        singleExecutor.execute(() -> {
            try {
                setDataSource(path);
                if (callback != null) {
                    callback.onAsyncFinished();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        });
    }

    public void prepare(@Nullable MediaPlayer.OnPreparedListener listener)
            throws IOException, IllegalStateException {
        mediaPlayer.setOnPreparedListener(listener);
        mediaPlayer.prepare();
    }

    public void prepareAsync(@Nullable MediaPlayer.OnPreparedListener listener)
            throws IllegalStateException {
        if (mediaPlayer == null) return;
        mediaPlayer.setOnPreparedListener(listener);
        mediaPlayer.prepareAsync();
    }

    public boolean isPlaying() {
        return mediaPlayer != null && mediaPlayer.isPlaying();
    }

    public void pause() {
        if (mediaPlayer == null) return;
        mediaPlayer.pause();
    }

    public void setLooping(boolean looping) {
        if (mediaPlayer == null) return;
        mediaPlayer.setLooping(looping);
    }

    public void start() {
        if (mediaPlayer == null) return;
        mediaPlayer.start();

    }

    public void stop() {
        if (mediaPlayer == null) return;
        mediaPlayer.stop();
    }

    public void reset() {
        if (mediaPlayer == null) return;
        mediaPlayer.reset();
    }

    public void releaseAsync(Callback callback) {
        if (singleExecutor != null && !singleExecutor.isShutdown()) {
            singleExecutor.execute(() -> {
                release();
                singleExecutor.shutdownNow();
                if (callback != null) {
                    callback.onAsyncFinished();
                }
            });
        }
    }

    public void release() {
        if (mediaPlayer == null) return;
        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer = null;
    }

    public void setOnCompletionListener(MediaPlayer.OnCompletionListener listener) {
        if (mediaPlayer != null) {
            mediaPlayer.setOnCompletionListener(listener);
        }
    }

    public interface Callback {
        void onAsyncFinished();
    }
}