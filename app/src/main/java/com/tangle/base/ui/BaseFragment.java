package com.tangle.base.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.tangle.R;
import com.tangle.base.ui.interfaces.AutoCloseable;
import com.tangle.base.ui.interfaces.FragmentOperations;
import com.tangle.utils.PlatformUtils;

public abstract class BaseFragment extends Fragment implements AutoCloseable {
    protected final String TAG = this.getClass().getSimpleName();

    private ProgressDialog progressDialog;
    protected FragmentOperations fragmentOperations;
    private Handler animationHandler;

    @TargetApi(23)
    @Override
    public final void onAttach(Context context) {
        super.onAttach(context);
        onAttachToContext(context);
    }

    @SuppressWarnings("deprecation")
    @Override
    public final void onAttach(Activity activity) {
        super.onAttach(activity);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            onAttachToContext(activity);
        }
    }

    protected void onAttachToContext(Context context) {
        fragmentOperations = (FragmentOperations) context;
    }

    protected void clearBackStack() {
        getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        view.setClickable(true);
        animationHandler = new Handler();
        progressDialog = new ProgressDialog(getContext());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setMessage("Wait");
        progressDialog.setIndeterminate(true);
        if (!hasCustomToolbar()) {
            View backButton = view.findViewById(R.id.btn_back);
            if (backButton != null)
                backButton.setOnClickListener(v -> getActivity().onBackPressed());
        }
    }

    public boolean onBackPressed() {
        return false;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        animationHandler.removeCallbacksAndMessages(null);
        PlatformUtils.Keyboard.hideKeyboard(getActivity());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        fragmentOperations = null;
    }

    public void showLoading() {
        progressDialog.show();
    }

    public void hideLoading() {
        progressDialog.dismiss();
    }

    protected void switchFragment(Fragment fragment, boolean addToBackStack) {
        fragmentOperations.switchFragment(fragment, addToBackStack);
    }
    protected void switchFragmentWithPopLast(Fragment fragment) {
        fragmentOperations.switchFragmentWithPopLast(fragment);
    }

    protected void addFragment(Fragment fragment, boolean addToBackStack) {
        fragmentOperations.addFragment(fragment, addToBackStack);
    }

    protected void addFragment(Fragment fragment) {
        fragmentOperations.addFragment(fragment, true);
    }

    protected void safePost(Runnable runnable) {
        if (animationHandler != null) {
            animationHandler.post(runnable);
        }
    }

    protected boolean hasCustomToolbar() {
        return false;
    }

    protected void initToolbar(String title, boolean showBack, Integer rightIcoResId) {
        if (rightIcoResId != null) {
            ImageButton rightBtnIco = getView().findViewById(R.id.btn_right_ico);
            rightBtnIco.setVisibility(View.VISIBLE);
            rightBtnIco.setImageResource(rightIcoResId);
        }
        initToolbar(title, showBack);
    }

    public void initToolbar(String title, boolean showBack) {
        getView().findViewById(R.id.btn_back).setVisibility(showBack ? View.VISIBLE : View.INVISIBLE);
        TextView toolbarTitleTextView = getView().findViewById(R.id.txt_toolbar_title);
        if (!hasCustomToolbar() && toolbarTitleTextView != null && !TextUtils.isEmpty(title)) {
            toolbarTitleTextView.setVisibility(View.VISIBLE);
            toolbarTitleTextView.setText(title);
        }
    }

    public void showAPIError(String errorText) {
        if (!TextUtils.isEmpty(errorText)) {
            Toast.makeText(getActivity(), errorText, Toast.LENGTH_SHORT).show();
        }
    }

    public class ResultActions {
        public static final int ACTION_DELETE_OWN_PHOTO = 1;
        public static final int ACTION_SET_PHOTO_AS_AVATAR = 2;
        public static final int ACTION_OPEN_ADD_PHOTOS_VIEW = 3;
    }

    public void closeFragmentWithResult(int actionId, Object resultObj, Class parentFragment) {
        getActivity().onBackPressed();
        ((BaseFragment) fragmentOperations.findFragment(parentFragment)).handlerAction(actionId, resultObj);
    }

    public void handlerAction(int action, Object resultObj) {
    }

    @Override
    public void close() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }
}