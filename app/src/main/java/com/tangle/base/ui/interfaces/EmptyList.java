package com.tangle.base.ui.interfaces;

public interface EmptyList {

    void showEmptyListError();

    void hideEmptyListError();
}
