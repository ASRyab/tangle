package com.tangle.base.ui.dialogs;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.managers.EventManager;
import com.tangle.utils.RxUtils;

public class ShareFreeDialog extends BottomSheetDialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_share_free, container, false);
        view.findViewById(R.id.btn_share).setOnClickListener(__ -> onShareClick());
        return view;
    }

    private void onShareClick() {
        FragmentActivity activity = getActivity();
        if (activity == null) {
            dismiss();
            return;
        }
        ApplicationLoader.getApplicationInstance().getUserManager().currentUser().subscribe(data -> {
            EventManager.shareEvent(activity, data.id);
            dismiss();
        }, RxUtils.getEmptyErrorConsumer(ShareFreeDialog.class.getSimpleName()));
    }
}
