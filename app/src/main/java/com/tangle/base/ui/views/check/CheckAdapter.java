package com.tangle.base.ui.views.check;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tangle.R;

import java.util.ArrayList;
import java.util.List;

public abstract class CheckAdapter<T> extends RecyclerView.Adapter<CheckAdapter.CheckViewHolder> {

    private List<T> dataList = new ArrayList<>();
    private int selectedIndex;
    private OnItemCheckListener<T> listener;

    protected abstract CheckAdapter.CheckViewHolder getViewHolder(ViewGroup parent, int viewType);

    @Override
    public final CheckAdapter.CheckViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return getViewHolder(parent, viewType);
    }

    @Override
    public final void onBindViewHolder(CheckAdapter.CheckViewHolder holder, int position) {
        holder.bindInternal(position);
    }

    @Override
    public final int getItemCount() {
        return dataList == null ? 0 : dataList.size();
    }

    public abstract class CheckViewHolder extends RecyclerView.ViewHolder {

        protected ImageView checkImageView;
        protected TextView labelTextView;

        public CheckViewHolder(View itemView) {
            super(itemView);
            checkImageView = itemView.findViewById(R.id.img_check);
            labelTextView = itemView.findViewById(R.id.txt_label);
        }

        private void bindInternal(int index) {
            checkImageView.setSelected(selectedIndex == index);
            T item = dataList.get(index);
            itemView.setOnClickListener(v -> {
                if (listener != null) {
                    listener.onItemChecked(item);
                }
            });
            bind(item);
        }

        protected abstract void bind(T item);
    }

    protected void setIndexChecked(int index) {
        this.selectedIndex = index;
        notifyDataSetChanged();
    }

    public void setItemChecked(T item) {
        int index = dataList.indexOf(item);
        if (index > -1) {
            setIndexChecked(index);
        }
    }

    public void switchData(List<T> dataList) {
        this.dataList = dataList;
        notifyDataSetChanged();
    }

    public void setItemCheckListener(OnItemCheckListener<T> listener) {
        this.listener = listener;
    }

    public interface OnItemCheckListener<T> {
        void onItemChecked(T item);
    }

}
