package com.tangle.base.ui.views;

import com.tangle.base.ui.SecondaryScreenFragment;

public abstract class ActivitiesControl extends SecondaryScreenFragment {

    private boolean isOpened;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (!isOpened) {
            isOpened = isVisibleToUser;
        }
        if (isOpened && !isVisibleToUser) {
            isOpened = false;
            markItemsAsRead();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (isOpened) {
            markItemsAsRead();
        }
    }

    public abstract void markItemsAsRead();
}
