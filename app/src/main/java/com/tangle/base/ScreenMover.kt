package com.tangle.base

import com.tangle.base.ui.SecondaryScreenFragment
import com.tangle.screen.main.ScreenJump

interface ScreenMover {
    fun moveToScreen(fragment: SecondaryScreenFragment, screenJump: ScreenJump)

    fun moveToScreen(fragment: SecondaryScreenFragment)
}