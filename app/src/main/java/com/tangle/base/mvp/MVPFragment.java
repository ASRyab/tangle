package com.tangle.base.mvp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;

import com.tangle.base.ui.BaseFragment;
import com.tangle.base.ui.listeners.KeyboardListener;

import java.util.Collections;
import java.util.List;

/**
 * Presenter should be created at class constructor. All arguments (like context, bundle values)
 * should be passed through lifecycle overriden methods (onCreate(), onViewCreated())
 */
public abstract class MVPFragment extends BaseFragment {

    public abstract LifecyclePresenter getPresenter();

    public List<LifecyclePresenter> getPresenters() {
        return Collections.EMPTY_LIST;
    }

    public void onPostViewCreated(View view, @Nullable Bundle savedInstanceState) {
    }

    @Override
    protected void onAttachToContext(Context context) {
        super.onAttachToContext(context);
        if (getPresenters() != null) {
            for (LifecyclePresenter lifecyclePresenter : getPresenters()) {
                lifecyclePresenter.onAttach(context);
            }
        }
        if (getPresenter() != null) {
            getPresenter().onAttach(context);
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (isNeedListenKeyboardState()) {
            setKeyboardListener((KeyboardListener) getActivity());
        }
        if (getPresenters() != null) {
            for (LifecyclePresenter lifecyclePresenter : getPresenters()) {
                lifecyclePresenter.onCreate();
            }
        }
        if (getPresenter() != null) {
            getPresenter().onCreate();
        }
    }

    @Override
    public final void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onPostViewCreated(view, savedInstanceState);
        if (getPresenters() != null) {
            for (LifecyclePresenter lifecyclePresenter : getPresenters()) {
                lifecyclePresenter.attachToView(this, savedInstanceState);
            }
        }
        if (getPresenter() != null) {
            getPresenter().attachToView(this, savedInstanceState);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getPresenters() != null) {
            for (LifecyclePresenter lifecyclePresenter : getPresenters()) {
                lifecyclePresenter.onActivityCreated(savedInstanceState);
            }
        }
        if (getPresenter() != null) {
            getPresenter().onActivityCreated(savedInstanceState);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (isNeedListenKeyboardState()) {
            LocalBroadcastManager.getInstance(getActivity()).registerReceiver(showReceiver,
                    new IntentFilter(KeyboardListener.ACTION_SHOW_KEYBOARD));
            LocalBroadcastManager.getInstance(getActivity()).registerReceiver(hideReceiver,
                    new IntentFilter(KeyboardListener.ACTION_HIDE_KEYBOARD));
        }
        if (getPresenters() != null) {
            for (LifecyclePresenter lifecyclePresenter : getPresenters()) {
                lifecyclePresenter.onStart();
            }
        }
        if (getPresenter() != null) {
            getPresenter().onStart();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getPresenters() != null) {
            for (LifecyclePresenter lifecyclePresenter : getPresenters()) {
                lifecyclePresenter.onResume();
            }
        }
        if (getPresenter() != null) {
            getPresenter().onResume();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getPresenters() != null) {
            for (LifecyclePresenter lifecyclePresenter : getPresenters()) {
                lifecyclePresenter.onPause();
            }
        }
        if (getPresenter() != null) {
            getPresenter().onPause();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (isNeedListenKeyboardState()) {
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(showReceiver);
            LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(hideReceiver);
        }
        if (getPresenters() != null) {
            for (LifecyclePresenter lifecyclePresenter : getPresenters()) {
                lifecyclePresenter.onStop();
            }
        }
        if (getPresenter() != null) {
            getPresenter().onStop();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (getPresenters() != null) {
            for (LifecyclePresenter lifecyclePresenter : getPresenters()) {
                lifecyclePresenter.onDestroyView();
            }
        }
        if (getPresenter() != null) {
            getPresenter().onDestroyView();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (getPresenters() != null) {
            for (LifecyclePresenter lifecyclePresenter : getPresenters()) {
                lifecyclePresenter.onDestroy();
            }
        }
        if (getPresenter() != null) {
            getPresenter().onDestroy();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (getPresenters() != null) {
            for (LifecyclePresenter lifecyclePresenter : getPresenters()) {
                lifecyclePresenter.onSaveInstanceState(outState);
            }
        }
        if (getPresenter() != null) {
            getPresenter().onSaveInstanceState(outState);
        }
    }

    private KeyboardListener keyboardListener;

    public void setKeyboardListener(KeyboardListener keyboardListener) {
        this.keyboardListener = keyboardListener;
    }

    public void attachKeyboardListeners() {
        keyboardListener.attachKeyboardListeners();
    }

    private BroadcastReceiver showReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            actionShowKeyboard();
        }
    };

    public void actionHideKeyboard() {
    }

    private BroadcastReceiver hideReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            actionHideKeyboard();
        }
    };

    public void actionShowKeyboard() {
    }

    public boolean isNeedListenKeyboardState() {
        return false;
    }
}