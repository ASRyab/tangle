package com.tangle.base;

import android.annotation.SuppressLint;

import com.tangle.api.utils.ErrorResponse;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.funnel.FunnelRedirectException;

import java.util.NoSuchElementException;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.Single;
import io.reactivex.functions.Consumer;

import static com.tangle.base.RedirectManager.CheckResult.GO_TO_FUNNEL_GENDER;
import static com.tangle.base.RedirectManager.CheckResult.NO_REDIRECT;

public class RedirectManager {


    @SuppressLint("CheckResult")
    public static void checkFunnel(Throwable throwable, Consumer<CheckResult> action) {
        Observable.just(throwable)
                .flatMap(res -> {
                    if (res instanceof ErrorResponse) {
                        return getNetworkRedirectObs(res);
                    } else
                        return getLocalRedirectObs(res);
                })
                .subscribe(action::accept
                        , throwable1 -> action.accept(NO_REDIRECT));
    }


    public static ObservableTransformer<ProfileData, ProfileData> composeFunnelCheck() {
        return upstream -> upstream
                .filter(data -> !data.getIndustry().equals("0"))
                .singleOrError()
                .onErrorResumeNext(throwable -> {
                            if (throwable instanceof NoSuchElementException) {
                                return Single.error(new FunnelRedirectException());
                            } else return Single.error(throwable);
                        }
                ).toObservable();

    }

    private static Observable<CheckResult> getNetworkRedirectObs(Throwable res) {
        return Observable.just(res).cast(ErrorResponse.class)
                .map(ErrorResponse::getMeta)
                .map(meta ->
                {
                    if (meta != null && meta.getCode() == 302 && "funnel".equals(meta.getRedirect())) {
                        return GO_TO_FUNNEL_GENDER;
                    } else return NO_REDIRECT;
                });

    }

    private static Observable<CheckResult> getLocalRedirectObs(Throwable res) {

        if (res instanceof FunnelRedirectException) {
            return Observable.just(CheckResult.GO_TO_FUNNEL_AGE);
        } else
            return Observable.just(NO_REDIRECT);
    }

    public enum CheckResult {
        GO_TO_FUNNEL_GENDER,
        GO_TO_FUNNEL_AGE,
        NO_REDIRECT
    }
}
