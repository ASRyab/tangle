package com.tangle.base;

import com.tangle.base.ui.interfaces.FragmentOperations;

public interface SecondaryScreenFragmentOperations extends FragmentOperations {

    void moveToMenu(int index);

    void clearStack();
}