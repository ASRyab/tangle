package com.tangle.base.repository;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class ItemsListRepository<T> implements ObservableRepository<T> {

    protected List<T> allItems = new CopyOnWriteArrayList<>();
    protected final PublishSubject<List<T>> updatesPublisher = PublishSubject.create();

    public ItemsListRepository() {
    }

    @Override
    public Observable<T> addObserve(T item) {
        return Observable.just(item)
                .filter(n -> !allItems.contains(n))
                .doOnNext(itemResult -> allItems.add(itemResult))
                .doOnNext(t -> updatesPublisher.onNext(Collections.singletonList(t)));
    }

    @Override
    public Observable<List<T>> addAllObserve(List<T> items) {
        return Observable.just(items)
                .flatMapIterable(items1 -> items1)
                .filter(item -> !allItems.contains(item))
                .toList().toObservable()
                .doOnNext(itemsResult -> allItems.addAll(itemsResult))
                .doOnNext(updatesPublisher::onNext);
    }

    @Override
    public Observable<List<T>> getAll() {
        return Observable.just(allItems);
    }

    @Override //todo should return all items
    public Observable<List<T>> getUpdates() {
        return updatesPublisher;
    }

    @Override
    public void add(T item) {
        addObserve(item).subscribe(t -> {}, Throwable::printStackTrace);
    }

    @Override
    public void addAll(List<T> items) {
        addAllObserve(items).subscribe(t -> {}, Throwable::printStackTrace);
    }

    @Override
    public void deleteAll() {
        allItems.clear();
    }


}
