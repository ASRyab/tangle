package com.tangle.base.repository;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;

public abstract class NotificationBaseRepository<T> {

    protected PublishSubject<T> notificationObservable;

    public NotificationBaseRepository() {
        notificationObservable = PublishSubject.create();
    }

    public abstract void addEmmit(T notification);

    public abstract Disposable subscribe(Consumer observer);
}
