package com.tangle.base.repository;

import com.tangle.model.pojos.DictionaryItem;

public interface RepositoriesHolder {
    ObservableRepository<DictionaryItem> getInterestsRepository();

    ObservableRepository<DictionaryItem> getCareerLevelsRepository();

    ObservableRepository<DictionaryItem> getIndustriesRepository();

    ObservableRepository<String> getCitiesRepository();

    ObservableRepository<String> getRegionsRepository();
}