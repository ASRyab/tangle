package com.tangle.base.repository;


import java.util.List;

import io.reactivex.Observable;

public interface ObservableRepository<T> {

    Observable<T> addObserve(T item);

    Observable<List<T>> addAllObserve(List<T> items);

    Observable<List<T>> getAll();

    Observable<List<T>> getUpdates();

    void deleteAll();

    void add(T item);

    void addAll(List<T> items);
}