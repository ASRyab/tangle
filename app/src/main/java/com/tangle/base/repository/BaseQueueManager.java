package com.tangle.base.repository;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseQueueManager<T> {
    protected List<String> cashList = new ArrayList<>();
    protected Map<String, T> allItems = new HashMap<>();

    @NonNull
    protected List<String> getAskIds(List<String> eventIds) {
        List<String> askList = new ArrayList<>();
        for (String id : eventIds) {
            if (!containsInCache(id) && !cashList.contains(id)) {
                askList.add(id);
            }
        }
        cashList.addAll(askList);
        return askList;
    }

    public void deleteAll() {
        cashList.clear();
        allItems.clear();
    }

    protected boolean containsInCache(String id) {
        return allItems.containsKey(id);
    }
}
