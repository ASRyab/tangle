package com.tangle.base;

import android.text.TextUtils;

import com.tangle.ApplicationLoader;
import com.tangle.api.rpc.rpc_response.invite.InviteData;
import com.tangle.api.rx_tasks.payment.BuyTickets;
import com.tangle.api.rx_tasks.payment.InviteHolds;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.LifecyclePresenter;
import com.tangle.base.mvp.State;
import com.tangle.managers.DialogManager;
import com.tangle.managers.EventManager;
import com.tangle.model.event.EventInfo;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.screen.activities.main.ActivitiesFragment;
import com.tangle.screen.events.popup.CongratsViewType;
import com.tangle.screen.main.ScreenJump;
import com.tangle.utils.RxUtils;

import io.reactivex.Observable;

import static com.tangle.model.event.EventInfo.EventTicketStatus.EventTicketStatusReserved;

public class PrePaymentPresenter<P extends PrePaymentView> extends LifecyclePresenter<P> {

    protected final DialogManager dialogManager;

    public PrePaymentPresenter(DialogManager dialogManager) {
        this.dialogManager = dialogManager;
    }

    public void onPPSuccess(String eventId, String userId) {
        updateData(userId, eventId);
        getView().goToSuccess(eventId, userId
                , (TextUtils.isEmpty(userId) ? CongratsViewType.BOUGHT_EVENT_TICKET : CongratsViewType.SEND_EVENT_TICKET));
    }

    private void updateData(String userId, String eventId) {
        ApplicationLoader.getApplicationInstance().getUserManager().loadCurrentUser();
        ApplicationLoader.getApplicationInstance().getInviteManager().forceLoadNew(0);

        if (!TextUtils.isEmpty(userId)) {
            ApplicationLoader.getApplicationInstance().getUserManager().updateUserInfo(userId);
        }
        ApplicationLoader.getApplicationInstance().getEventManager().forceLoadEvent(eventId);
    }

    public void onPPCanceled() {
        dialogManager.showExpiredDialog();
    }

    private void clickBook(String eventId, int count) {
        monitor(new BuyTickets(eventId, "", count)
                        .getDataTask()
                        .compose(RxUtils.withLoading(getView()))
                        .subscribe(response -> getView().goToPP(response)
                                , error -> getView().showAPIError(ErrorResponse.getServerMessage(error)))
                , State.DESTROY_VIEW);
    }

    private void clickInvite(EventInfo event, String profileId, InviteHolds holds) {
        monitor(new BuyTickets(event.eventId, profileId, holds)
                        .getDataTask()
                        .compose(RxUtils.withLoading(getView()))
                        .flatMap(response -> {
                            if (response.getEventId() == null || response.getInvoiceId() == null) {
                                return Observable.error(new Exception("User already has own ticket in hold"));
                            } else return Observable.just(response);
                        })
                        .subscribe(response -> getView().goToPP(response)
                                , error -> getView().showAPIError(ErrorResponse.getServerMessage(error)))
                , State.DESTROY_VIEW);
    }

    public void onBookClicked(EventInfo event) {
        checkUserInWhiteList(() -> {
            if (EventTicketStatusReserved.equals(event.getEventTicketStatus())) {
                dialogManager.showBookingOwnReserve((v, dialog) -> {
                    chargeTicket(event);
                    countOfTickets(event);
                });
            } else if (checkHasInvite(event)) {
                dialogManager.showNonAnsweredInvite((v, dialog) -> {
                            refuseAllInvites(event);
                            countOfTickets(event);
                        }
                        , (v, dialog) -> goToInvites());
            } else {
                countOfTickets(event);
            }
        });
    }

    private void refuseAllInvites(EventInfo event) {
        monitor(ApplicationLoader.getApplicationInstance().getInviteManager().refuseAllForEvent(event, getView())
                        .subscribe(RxUtils.getEmptyDataConsumer()
                                , RxUtils.getEmptyErrorConsumer(TAG, "chargeTicket"))
                , State.DESTROY_VIEW);
    }

    private void goToInvites() {
        getView().moveToScreen(new ActivitiesFragment(), ScreenJump.INVITE);
    }

    private boolean checkHasInvite(EventInfo event) {
        return ApplicationLoader.getApplicationInstance().getInviteManager().getAll().flatMapIterable(inviteData -> inviteData)
                .filter(inviteData -> inviteData.eventId.equals(event.eventId))
                .filter(inviteData -> InviteData.NOT_RESPOND_TO_INVITE_INDEX == inviteData.status)
                .filter(inviteData -> inviteData.expireDate > 0)
                .filter(inviteData -> ApplicationLoader.getApplicationInstance().getUserManager().isCurrentUser(inviteData.to))
                .toList().map(inviteData -> inviteData.size() > 0).blockingGet();
    }

    private void countOfTickets(EventInfo event) {
        if (event.maxBookTickets > 1) {
            dialogManager.showTicketsCountDialog(event, count -> clickBook(event.eventId, count));
        } else {
            clickBook(event.eventId, 1);
        }
    }

    public void onInviteClicked(EventInfo event, ProfileData user) {
        checkUserInWhiteList(() -> {
            if (checkHasInvite(event)) {
                dialogManager.showNonAnsweredInvite((v, dialog) -> {
                            refuseAllInvites(event);
                            checkTicketStatus(event, user);
                        }
                        , (v, dialog) -> goToInvites());
            } else {
                checkTicketStatus(event, user);
            }
        });
    }

    private void checkTicketStatus(EventInfo event, ProfileData user) {
        switch (event.getEventTicketStatus()) {
            case EventTicketStatusNone:
                dialogManager.showInviteOwnPurchase(
                        (v, dialog) -> clickInvite(event, user.id, InviteHolds.HOLD_INVOICES)
                        , (v, dialog) -> clickInvite(event, user.id, InviteHolds.CHARGE_OWN_INVOICE));
                break;
            case EventTicketStatusBought:
                clickInvite(event, user.id, InviteHolds.HOLD_INVOICES);
                break;
            case EventTicketStatusReserved:
                dialogManager.showInviteOwnReserve((v, dialog) -> {
                    chargeTicket(event);
                    clickInvite(event, user.id, InviteHolds.HOLD_INVOICES);
                });
                break;
        }
    }

    public void chargeTicket(EventInfo event) {
        monitor(EventManager.chargeTicket(event, getView())
                        .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "chargeTicket"))
                , State.DESTROY_VIEW);
    }

    void checkUserInWhiteList(Runnable action) {
        monitor(ApplicationLoader.getApplicationInstance().getUserManager().checkUserInWhiteList()
                        .subscribe(isInWhiteList -> showDialogIfInWhiteList(action, isInWhiteList), RxUtils.getEmptyErrorConsumer(TAG)),
                State.DESTROY_VIEW);
    }

    private void showDialogIfInWhiteList(Runnable action, Boolean isInWhiteList) {
        if (isInWhiteList) {
            dialogManager.showRemindLocation((v, dialog) -> action.run());
        } else {
            action.run();
        }
    }
}
