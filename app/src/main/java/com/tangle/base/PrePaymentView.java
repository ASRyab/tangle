package com.tangle.base;

import com.tangle.base.ui.interfaces.LoadingView;
import com.tangle.model.payment.PaymentData;
import com.tangle.screen.events.popup.CongratsViewType;

public interface PrePaymentView extends LoadingView, ScreenMover {
    void goToPP(PaymentData data);

    void goToSuccess(String eventId, String userId, CongratsViewType type);

    PrePaymentPresenter getPrePaymentPresenter();
}
