package com.tangle.base;

import com.tangle.base.ui.interfaces.LocationCheckableView;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.TicketStatus;

public interface FreePaymentView extends PrePaymentView, LocationCheckableView {
    void freeTicketSuccess(TicketStatus status, EventInfo eventInfo);
}
