package com.tangle.base;

import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.payment.RequestTicket;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.base.mvp.State;
import com.tangle.managers.DialogManager;
import com.tangle.model.event.EventInfo;
import com.tangle.utils.RxUtils;

public class FreePaymentPresenter extends PrePaymentPresenter<FreePaymentView> {
    public FreePaymentPresenter(DialogManager dialogManager) {
        super(dialogManager);
    }

    public void onFreeClicked(EventInfo event) {
        checkUserInWhiteList(() -> {
            if (event.maxBookTickets > 1) {
                dialogManager.showFreeTicketsCountDialog(event, count -> freeBook(event, count));
            } else {
                freeBook(event, 1);
            }
        });
    }

    private void freeBook(EventInfo eventInfo, int count) {
        monitor(new RequestTicket(eventInfo.eventId, count)
                        .getDataTask()
                        .doOnNext(response -> ApplicationLoader.getApplicationInstance().getEventManager()
                                .forceLoadNewEvent().subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "freeBook")))
                        .compose(RxUtils.withLoading(getView()))
                        .subscribe(response -> getView().freeTicketSuccess(response.freeTicketStatus, eventInfo)
                                , error -> getView().showAPIError(ErrorResponse.getServerMessage(error)))
                , State.DESTROY_VIEW);
    }
}
