package com.tangle.base.implementation.adapters;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.model.pojos.DictionaryItem;

import java.util.ArrayList;
import java.util.List;

public class InterestsAdapter extends RecyclerView.Adapter<InterestsAdapter.InterestViewHolder> {

    private static final int MAX_SHOW_ITEM_ON_SCREEN = 7;
    private static final int MORE_INDICATOR_ID = -1;

    private List<DictionaryItem> interests;
    private List<DictionaryItem> interestsWithMoreIndicator = new ArrayList<>();
    private SparseArray<DictionaryItem> selectedInterests = new SparseArray<>();
    private boolean selectAvailable = false;
    private boolean showMoreIndicator = false;
    private int textColor = Color.parseColor("#c5c5c5");

    public InterestsAdapter() {
    }

    public InterestsAdapter(boolean showMoreIndicator) {
        this.showMoreIndicator = showMoreIndicator;
    }

    @Override
    public InterestViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new InterestViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_funnel_interest, parent, false));
    }

    @Override
    public void onBindViewHolder(InterestViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        List<DictionaryItem> interestsList = getInterestsList();
        return interestsList == null ? 0 : interestsList.size();
    }

    private List<DictionaryItem> getInterestsList() {
        if (showMoreIndicator) {
            return interestsWithMoreIndicator;
        } else {
            return interests;
        }
    }

    public void switchInterests(List<DictionaryItem> interests) {
        this.interests = interests;
        if (showMoreIndicator) {
            int maxShowCount = interests.size() < MAX_SHOW_ITEM_ON_SCREEN ? interests.size() : MAX_SHOW_ITEM_ON_SCREEN;
            interestsWithMoreIndicator.clear();
            interestsWithMoreIndicator.addAll(interests.subList(0, maxShowCount));
            selectedInterests.clear();
            if (maxShowCount >= MAX_SHOW_ITEM_ON_SCREEN) {
                interestsWithMoreIndicator.add(new DictionaryItem(MORE_INDICATOR_ID, ApplicationLoader.getContext().getString(R.string.own_profile_read_more)));
            }
        }
        if (!selectAvailable) {
            for (int i = 0; i < interests.size(); i++) {
                selectedInterests.put(i, interests.get(i));
            }
        }
        notifyDataSetChanged();
    }

    public List<DictionaryItem> getSelectedInterests() {
        List<DictionaryItem> list = new ArrayList<>();
        for (int i = 0; i < selectedInterests.size(); i++) {
            int key = selectedInterests.keyAt(i);
            list.add(selectedInterests.get(key));
        }
        return list;
    }

    public void setSelectedInterests(List<DictionaryItem> interests) {
        if (interests == null || interests.isEmpty()) return;
        for (DictionaryItem in : interests) {
            int indexOf = this.interests.indexOf(in);
            if (indexOf > -1) {
                selectedInterests.put(indexOf, in);
            }
        }
        notifyDataSetChanged();
    }

    class InterestViewHolder extends RecyclerView.ViewHolder {
        private DictionaryItem interest;
        private int index;

        void bind(int index) {
            this.index = index;
            this.interest = getInterestsList().get(index);
            Button btn = (Button) itemView;
            btn.setText(interest.name);
            if (isMoreIndicator(interest)) {
                itemView.setSelected(true);
                itemView.setClickable(true);
                btn.setTextColor(Color.WHITE);
            } else {
                boolean selected = (selectedInterests.get(index) != null);
                btn.setTextColor(selected ? ContextCompat.getColor(itemView.getContext(), R.color.colorPrimary) : textColor);
                itemView.setSelected(selected);
                itemView.setClickable(selectAvailable);
            }
        }

        private boolean isMoreIndicator(DictionaryItem item) {
            return item.id == MORE_INDICATOR_ID;
        }

        InterestViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(v -> {
                if (isMoreIndicator(interest)) {
                    interestsWithMoreIndicator.clear();
                    interestsWithMoreIndicator.addAll(interests.subList(interestsWithMoreIndicator.size(), interests.size()));
                } else {
                    if (selectedInterests.get(index) == null) {
                        selectedInterests.put(index, interest);
                    } else {
                        selectedInterests.remove(index);
                    }
                }
                notifyDataSetChanged();
            });
        }
    }

    public boolean isSelectAvailable() {
        return selectAvailable;
    }

    public void setSelectAvailable(boolean selectAvailable) {
        if (this.selectAvailable != selectAvailable) {
            this.selectAvailable = selectAvailable;
            notifyDataSetChanged();
        }
    }

    public void setShowMoreIndicator(boolean showMoreIndicator) {
        if (this.showMoreIndicator != showMoreIndicator) {
            this.showMoreIndicator = showMoreIndicator;
            notifyDataSetChanged();
        }

    }
}