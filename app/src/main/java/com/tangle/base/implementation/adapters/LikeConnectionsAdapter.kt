package com.tangle.base.implementation.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.bumptech.glide.request.RequestOptions
import com.tangle.R
import com.tangle.model.profile_data.ProfileData
import com.tangle.utils.glide.GlideApp
import com.tangle.utils.glide.RequestOptionsFactory
import java.util.*

class LikeConnectionsAdapter(val context: Context?, private val userPhotoRequestOptions: RequestOptions, private var gridItemClickListener: GridItemClickListener?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val likedYouItems = ArrayList<ProfileData>()
    private val likedOtherItems = ArrayList<ProfileData>()

    var likedYouItemsLimitedSize = 0
    var likedOtherItemsLimitedSize = 0


    fun setLikedItems(likedYouItems: List<ProfileData>, likedOtherItems: List<ProfileData>) {
        this.likedYouItems.clear()
        this.likedOtherItems.clear()
        this.likedYouItems.addAll(likedYouItems)
        this.likedOtherItems.addAll(likedOtherItems)

        likedYouItemsLimitedSize = getLimitedListSize(likedYouItems)
        likedOtherItemsLimitedSize = getLimitedListSize(likedOtherItems)
    }

    private fun getLimitedListSize(list: List<ProfileData>): Int {
        return when {
            list.isEmpty() -> 0
            list.size > MAX_LIST_SIZE -> MAX_LIST_SIZE + 1
            else -> list.size + 1
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView: View
        return if (viewType == TYPE_LIKED_ME_HEADER || viewType == TYPE_LIKED_OTHER_HEADER) {
            itemView = View.inflate(context, R.layout.item_likes_with_counter, null)
            LikesWithCounterViewHolder(itemView)
        } else {
            itemView = View.inflate(context, R.layout.item_like_people, null)
            LikedYouItemViewHolder(itemView)
        }
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if (viewHolder is LikesWithCounterViewHolder) {
            val isOutgoingLikeHeader = position > likedYouItemsLimitedSize - 1
            viewHolder.text.setText(if (isOutgoingLikeHeader) R.string.outgoing_likes else R.string.incoming_likes)
            val itemsCount = if (isOutgoingLikeHeader) likedOtherItems.size else likedYouItems.size
            if (itemsCount > MAX_LIST_SIZE) {
                viewHolder.counter.visibility = View.VISIBLE
                viewHolder.counter.text = itemsCount.toString()
                viewHolder.itemView.setOnClickListener {
                    if (isOutgoingLikeHeader) {
                        gridItemClickListener?.onOutgoingLikesClick()
                    } else gridItemClickListener?.onIncomingLikesClick()
                }
            } else {
                viewHolder.counter.visibility = View.GONE
            }
        } else {
            val holder = viewHolder as LikedYouItemViewHolder
            val isLikedOther = position > likedYouItemsLimitedSize
            val pos = if (isLikedOther) position - likedYouItemsLimitedSize - 1 else position - 1
            val item = if (isLikedOther) likedOtherItems[pos] else likedYouItems[pos]
            val imgUserPhoto = holder.imgUserPhoto
            GlideApp.with(imgUserPhoto)
                    .load(item.primaryPhoto.normal)
                    .placeholder(RequestOptionsFactory.getNonAvatar(item))
                    .apply(userPhotoRequestOptions)
                    .into(imgUserPhoto)
            if (item.isNew && !isLikedOther) {
                holder.isNewIndicator.visibility = View.VISIBLE
                holder.imgMutualLikeIndicator.visibility = View.INVISIBLE
                holder.imgLikeIndicator.visibility = View.INVISIBLE
            } else {
                holder.isNewIndicator.visibility = View.INVISIBLE
            }

            holder.txtUserName.text = item.login
            holder.txtUserAgeAndLocation.text = context?.getString(R.string.format_age_country, item.age, item.geo.city)
            val onClickListener = View.OnClickListener {
                gridItemClickListener?.onItemClick(item)
            }
            holder.parentLayout.setOnClickListener(onClickListener)
        }
    }


    override fun getItemCount(): Int {
        return likedYouItemsLimitedSize + likedOtherItemsLimitedSize
    }


    override fun getItemViewType(position: Int): Int {
        return when {
            likedYouItemsLimitedSize > 0 && position == 0 -> TYPE_LIKED_ME_HEADER
            likedYouItemsLimitedSize > 0 && position in 0 until likedYouItemsLimitedSize -> TYPE_LIKED_ME
            likedOtherItemsLimitedSize > 0 && position == likedYouItemsLimitedSize -> TYPE_LIKED_OTHER_HEADER
            likedOtherItemsLimitedSize > 0 && position in likedYouItemsLimitedSize until (likedOtherItemsLimitedSize + likedYouItemsLimitedSize) -> TYPE_LIKED_OTHER
            else -> TYPE_NONE
        }
    }


    interface GridItemClickListener {
        fun onItemClick(item: ProfileData)
        fun onIncomingLikesClick()
        fun onOutgoingLikesClick()
    }

    companion object {
        private const val MAX_LIST_SIZE: Int = 6

        const val TYPE_NONE = 1
        const val TYPE_LIKED_ME_HEADER = 2
        const val TYPE_LIKED_ME = 3
        const val TYPE_LIKED_OTHER_HEADER = 4
        const val TYPE_LIKED_OTHER = 5
    }


}

class LikedYouItemViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
    val parentLayout: LinearLayout
    val imgUserPhoto: ImageView
    val imgMutualLikeIndicator: ImageView
    val imgCheckIndicator: ImageView
    val imgLikeIndicator: ImageView
    val isNewIndicator: TextView
    val txtUserName: TextView
    val txtUserAgeAndLocation: TextView

    init {
        parentLayout = itemView.findViewById(R.id.parent_layout)
        imgUserPhoto = itemView.findViewById(R.id.img_view_user_photo)
        imgMutualLikeIndicator = itemView.findViewById(R.id.img_view_is_mutual_like)
        imgLikeIndicator = itemView.findViewById(R.id.txt_view_is_like)
        imgCheckIndicator = itemView.findViewById(R.id.img_view_check_indicator)
        isNewIndicator = itemView.findViewById(R.id.txt_view_is_new)
        txtUserName = itemView.findViewById(R.id.txt_user_name)
        txtUserAgeAndLocation = itemView.findViewById(R.id.txt_user_age_location)
    }
}

class LikesWithCounterViewHolder internal constructor(v: View) : RecyclerView.ViewHolder(v) {
    var counter: TextView = v.findViewById(R.id.txt_likes_amount)
    var text: TextView = v.findViewById(R.id.txt_likes_title)

}