package com.tangle.base.implementation.adapters;

import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.request.RequestOptions;
import com.tangle.ApplicationLoader;
import com.tangle.R;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.ArrayList;
import java.util.List;

public class LikePeopleGridAdapter extends RecyclerView.Adapter {
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_PROGRESS = 2;
    private static final int TYPE_HEADER = 3;

    private List<ProfileData> profileItems = new ArrayList<>();
    private RequestOptions userPhotoRequestOptions;

    private boolean isCheckedClick;
    private boolean loading;

    private int visibleThreshold = 1;
    private int lastVisibleItem, totalItemCount;
    private boolean shouldShowYouLikeIndicator = true;

    private GridItemClickListener gridItemClickListener;
    private OnLoadMoreListener onLoadMoreListener;

    private String headerText = null;
    private boolean hasHeaderText = false;

    public LikePeopleGridAdapter(GridItemClickListener gridItemClickListener, boolean isCheckedClick) {
        this.gridItemClickListener = gridItemClickListener;
        this.isCheckedClick = isCheckedClick;
        this.userPhotoRequestOptions = RequestOptionsFactory.getSmallRoundedCornersOptions(ApplicationLoader.getContext());
    }

    public void addProfileList(List<ProfileData> profileItems) {
        this.profileItems.addAll(profileItems);
        notifyDataSetChanged();
    }

    public void setShouldShowYouLikeIndicator(boolean shouldShowYouLikeIndicator) {
        this.shouldShowYouLikeIndicator = shouldShowYouLikeIndicator;
    }

    public void setProfileItems(List<ProfileData> profileItems) {
        this.profileItems.clear();
        this.profileItems.addAll(profileItems);
        notifyDataSetChanged();
    }

    public void loaderHide() {
        if (onLoadMoreListener != null) {
            hideProgress();
        }
        notifyDataSetChanged();
        if (onLoadMoreListener != null) {
            setLoaded();
        }
    }

    public void showProgress() {
        //todo it crashed app must be fix
//        this.profileItems.add(null);
//        notifyItemInserted(profileItems.size() - 1);
    }

    public void hideProgress() {
        //todo it crashed app must be fix
//        this.profileItems.remove(profileItems.size() - 1);
//        notifyItemInserted(profileItems.size());
    }

    public void setLoadMoreListener(RecyclerView recyclerView, OnLoadMoreListener loadMoreListener) {
        this.onLoadMoreListener = loadMoreListener;
        final GridLayoutManager gridLayoutManager = (GridLayoutManager) recyclerView.getLayoutManager();
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                return isPositionFooter(position) ? gridLayoutManager.getSpanCount() : 1;
            }
        });
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                totalItemCount = gridLayoutManager.getItemCount();
                lastVisibleItem = gridLayoutManager.findLastVisibleItemPosition();
                if (!loading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    loading = true;
                }
            }
        });
    }

    public void setHeaderText(String headerText) {
        this.headerText = headerText;
        hasHeaderText = headerText != null && !headerText.isEmpty();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case TYPE_PROGRESS: {
                itemView = View.inflate(parent.getContext(), R.layout.item_load_more, null);
                return new LoadMoreViewHolder(itemView);
            }
            case TYPE_HEADER: {
                itemView = View.inflate(parent.getContext(), R.layout.item_list_header, null);
                return new HeaderViewHolder(itemView);
            }
            default:
                itemView = View.inflate(parent.getContext(), R.layout.item_like_people, null);
                return new LikePeopleGridAdapter.LikedYouItemViewHolder(itemView);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof HeaderViewHolder) {
            ((HeaderViewHolder) viewHolder).text.setText(headerText);
        } else if (viewHolder instanceof LoadMoreViewHolder) {
            ((LoadMoreViewHolder) viewHolder).progressBar.setIndeterminate(true);
        } else {
            LikedYouItemViewHolder holder = (LikedYouItemViewHolder) viewHolder;
            final ProfileData item = profileItems.get(hasHeaderText ? position - 1 : position);
            ImageView imgUserPhoto = holder.imgUserPhoto;
            GlideApp.with(imgUserPhoto)
                    .load(item.primaryPhoto.normal)
                    .placeholder(RequestOptionsFactory.getNonAvatar(item))
                    .apply(userPhotoRequestOptions)
                    .into(imgUserPhoto);
            if (item.isNew) {
                holder.isNewIndicator.setVisibility(View.VISIBLE);
                holder.imgMutualLikeIndicator.setVisibility(View.INVISIBLE);
                holder.imgLikeIndicator.setVisibility(View.INVISIBLE);
            } else {
                if (item.isMatchedUser) {
                    holder.imgMutualLikeIndicator.setVisibility(View.VISIBLE);
                    holder.imgLikeIndicator.setVisibility(View.INVISIBLE);
                } else if (item.isYouLikedUser && shouldShowYouLikeIndicator) {
                    holder.imgLikeIndicator.setVisibility(View.VISIBLE);
                    holder.imgMutualLikeIndicator.setVisibility(View.INVISIBLE);
                } else {
                    holder.imgMutualLikeIndicator.setVisibility(View.INVISIBLE);
                    holder.imgLikeIndicator.setVisibility(View.INVISIBLE);
                }
                holder.isNewIndicator.setVisibility(View.INVISIBLE);
            }
            holder.txtUserName.setText(item.login);
            holder.txtUserAgeAndLocation.setText(holder.txtUserAgeAndLocation.getContext().getString(R.string.format_age_country, item.age, item.geo.city));
            View.OnClickListener onClickListener = v -> {
                if (gridItemClickListener != null) {
                    gridItemClickListener.onItemClick(item);
                    if (isCheckedClick) {
                        holder.imgCheckIndicator.setVisibility(View.VISIBLE);
                        gridItemClickListener = null;
                    }
                }
            };
            holder.parentLayout.setOnClickListener(onClickListener);
        }
    }

    public void updateProfile(ProfileData data) {
        int index = profileItems.indexOf(data);
        profileItems.set(index, data);
        notifyItemChanged(index);
    }

    public class LikedYouItemViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout parentLayout;
        private ImageView imgUserPhoto, imgMutualLikeIndicator, imgCheckIndicator, imgLikeIndicator;
        private TextView isNewIndicator, txtUserName, txtUserAgeAndLocation;

        LikedYouItemViewHolder(View view) {
            super(view);
            parentLayout = itemView.findViewById(R.id.parent_layout);
            imgUserPhoto = itemView.findViewById(R.id.img_view_user_photo);
            imgMutualLikeIndicator = itemView.findViewById(R.id.img_view_is_mutual_like);
            imgLikeIndicator = itemView.findViewById(R.id.txt_view_is_like);
            imgCheckIndicator = itemView.findViewById(R.id.img_view_check_indicator);
            isNewIndicator = itemView.findViewById(R.id.txt_view_is_new);
            txtUserName = itemView.findViewById(R.id.txt_user_name);
            txtUserAgeAndLocation = itemView.findViewById(R.id.txt_user_age_location);
        }
    }

    public class LoadMoreViewHolder extends RecyclerView.ViewHolder {
        ProgressBar progressBar;

        LoadMoreViewHolder(View v) {
            super(v);
            progressBar = v.findViewById(R.id.progress_bar);
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        TextView text;
        HeaderViewHolder(View v) {
            super(v);
            text = v.findViewById(R.id.tvHeader);
        }
    }

    public void setLoaded() {
        loading = false;
    }

    public boolean isPositionFooter(int position) {
        return getItemViewType(position) == TYPE_PROGRESS;
    }

    @Override
    public int getItemCount() {
        return profileItems.size() + (hasHeaderText ? 1 : 0);
    }

    @Override
    public int getItemViewType(int position) {
        int listPosition = hasHeaderText ? position - 1 : position;
        return hasHeaderText && position == 0 ? TYPE_HEADER : profileItems.get(listPosition) != null ? TYPE_ITEM : TYPE_PROGRESS;
    }

    public interface OnLoadMoreListener {
        void onLoadMore();
    }

    public interface GridItemClickListener {
        void onItemClick(ProfileData item);
    }
}