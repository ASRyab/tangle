package com.tangle.base.implementation.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tangle.R;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;
import com.tangle.screen.events.EventClickListener;
import com.tangle.utils.EventUtils;
import com.tangle.utils.Patterns;
import com.tangle.utils.glide.GlideApp;
import com.tangle.utils.glide.RequestOptionsFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EventsAdapter extends RecyclerView.Adapter<EventsAdapter.EventViewHolder> {
    private EventClickListener eventClickListener;
    private List<EventInfo> events = new ArrayList<>();

    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new EventViewHolder(LayoutInflater.from(parent.getContext()).inflate(getLayoutId(), parent, false));
    }

    public int getLayoutId() {
        return R.layout.item_event;
    }

    @Override
    public void onBindViewHolder(EventViewHolder holder, int position) {
        holder.bind(events.get(position));
    }

    @Override
    public int getItemCount() {
        return events == null ? 0 : events.size();
    }

    public void switchEvents(List<EventInfo> events) {
        this.events = events;
        notifyDataSetChanged();
    }

    class EventViewHolder extends RecyclerView.ViewHolder {
        EventInfo event;

        TextView titleTextView;
        TextView dateTextView;
        TextView timeTextView;
        TextView tvTicketStatus;
        ImageView previewImageView;

        int primaryTextColor, secondaryTextColor;

        EventViewHolder(View itemView) {
            super(itemView);
            titleTextView = itemView.findViewById(R.id.tvTitle);
            tvTicketStatus = itemView.findViewById(R.id.tvTicketStatus);
            dateTextView = itemView.findViewById(R.id.txt_date);
            timeTextView = itemView.findViewById(R.id.tvTime);
            previewImageView = itemView.findViewById(R.id.img_preview);
            Context context = itemView.getContext().getApplicationContext();
            primaryTextColor = ContextCompat.getColor(context, R.color.colorTextPrimary);
            secondaryTextColor = ContextCompat.getColor(context, R.color.colorTextSecondary);
            itemView.setOnClickListener(v -> {
                if (eventClickListener != null) {
                    eventClickListener.onEventClicked(event);
                }
            });
        }

        void bind(EventInfo event) {
            this.event = event;
            titleTextView.setText(event.title);
            setDate();
            setPicture();
            EventUtils.updateEventStatus(tvTicketStatus, event);
        }

        private void setPicture() {
            List<Media> actualMediaList = event.getActualMediaList();
            if (actualMediaList != null && !actualMediaList.isEmpty()) {
                Media media = event.getActualMediaList().get(0);
                previewImageView.setVisibility(View.VISIBLE);
                GlideApp.with(previewImageView)
                        .load(media.type == Media.Type.PHOTO ? media.photoThumbnail2xUrl : media.videoImagePreviewUrl)
                        .apply(RequestOptionsFactory.getSmallRoundedCornersOptions(itemView.getContext()))
                        .into(previewImageView);
            } else {
                previewImageView.setVisibility(View.INVISIBLE);
            }
        }

        private void setDate() {
            Date launchDate = event.eventStartDate;
            if (DateUtils.isToday(launchDate.getTime())) {
                dateTextView.setBackgroundResource(R.drawable.own_profile_date_background);
                dateTextView.setTextColor(primaryTextColor);
                dateTextView.setText(itemView.getResources().getString(R.string.own_profile_today));
            } else {
                dateTextView.setBackgroundResource(0);
                dateTextView.setBackground(null);
                dateTextView.setTextColor(secondaryTextColor);
                dateTextView.setText(String.format("%s,", Patterns.DATE_FORMAT_SHORT.format(launchDate)));
            }
            timeTextView.setText(Patterns.TIME_FORMAT.format(launchDate));
        }
    }

    public void setEventClickListener(EventClickListener eventClickListener) {
        this.eventClickListener = eventClickListener;
    }
}