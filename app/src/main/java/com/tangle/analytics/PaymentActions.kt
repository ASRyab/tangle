package com.tangle.analytics

enum class SimpleActions(val firebaseAction: String) {
    REGISTRATION_COMPLETE("registration_complete"),

    REGISTRATION_CLICK_SIGNUP_OK("registration_click_signup_ok"),//*
    REGISTRATION_CLICK_SIGNIN_OK("registration_click_signin_ok"),//*

    LIKEBOOK_CLICK_LIKE("likebook_click_like"),//*
    LIKEBOOK_CLICK_DISLIKE("likebook_click_dislike"),//*
    LIKEBOOK_CLICK_INVITE("likebook_click_invite"),//*

    EVENT_CLICK_DETAILS("event_click_details"),//*
    EVENT_CLICK_MAP("event_click_map"),//*
    EVENT_CLICK_PHOTOBIG("event_click_photobig"),//*
    EVENT_CLICK_PARTICIPANTS("event_click_participants"),//*
    EVENT_CLICK_BOOK("event_click_book"),//*

    INVITES_CLICK_CONFIRM("invites_click_confirm"),
    INVITES_CLICK_REFUSE("invites_click_refuse"),

    MATCH_START_INVITE("match_start_invite"),//*
    MATCH_CLICK_INVITE("match_click_invite"),//*
    MATCH_CLICK_INVITE_SHORT("match_click_invite_short"),//*

    PAYMENT_CLICK_VISIT("payment_click_visit"),
    PAYMENT_TYPE_CARDNUMB("payment_type_cardnumb"),
    PAYMENT_SUCCESS("payment_success")
}

enum class PaymentActions(val openPP: String, val onTryPP: String, val success: String) {
    LIKEBOOK("likebook_visit_pp_invite", "likebook_try_invite", "likebook_paid_invite"),

    EVENT_INVITE("event_visit_pp_invite", "event_try_invite", "event_paid_invite"),
    EVENT_BOOK("event_visit_pp_book", "event_try_book", "event_paid_book"),

    PROFILE_INVITE("profile_visit_pp_invite", "profile_try_invite", "profile_paid_invite"),
    PROFILE_BOOK("profile_visit_pp_book", "profile_try_book", "profile_paid_book"),

    MATCH_INVITE("match_visit_pp_invite", "match_try_invite", "match_paid_invite"),
    MATCH_INVITE_SHORT("match_visit_pp_invite_short", "match_try_invite_short", "match_paid_invite_short"),
    NONE("", "", "");
}

enum class PaymentPoints {
    OPEN_PP, ON_TRY_PP, SUCCESS
}