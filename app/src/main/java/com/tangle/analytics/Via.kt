package com.tangle.analytics

enum class Via {
    LIKEBOOK_INVITE,
    MATCH_INVITE,
    MATCH_INVITE_SHORT,
    EVENT_INVITE,
    EVENT_BOOK,
    PROFILE_INVITE,
    PROFILE_BOOK,
    NONE;
}