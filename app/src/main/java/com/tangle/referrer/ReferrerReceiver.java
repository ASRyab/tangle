package com.tangle.referrer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.tangle.ApplicationLoader;

public class ReferrerReceiver extends BroadcastReceiver {

    private static final String KEY_REFERRER = "referrer";
    private static final String KEY_TEST = "test";

    @Override
    public void onReceive(Context context, Intent intent) {
        ApplicationLoader app = (ApplicationLoader) context.getApplicationContext();
        String referrer = intent.getStringExtra(KEY_REFERRER);
        boolean isTest = intent.getBooleanExtra(KEY_TEST, false);
        app.getTrackingManager().onBroadcastReferrerReceived(referrer, isTest);
    }
}
