package com.tangle.referrer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.android.installreferrer.api.InstallReferrerClient;
import com.android.installreferrer.api.InstallReferrerStateListener;
import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.auth.track_install.TrackInstall;
import com.tangle.utils.Debug;
import com.tangle.utils.PreferenceManager;
import com.tangle.utils.RxUtils;

import java.net.URLDecoder;
import java.util.concurrent.atomic.AtomicInteger;

import static com.android.installreferrer.api.InstallReferrerClient.InstallReferrerResponse;
import static com.android.installreferrer.api.InstallReferrerClient.newBuilder;

@SuppressLint("CheckResult")
public class TrackingManager {

    private static final String TAG = TrackingManager.class.getSimpleName();

    private static final String DEFAULT_REFERRER = "utm_source=google-play&utm_medium=organic";
    private static final String VALUE_UNKNOWN = "?";
    private static final int MAX_TRY_COUNT = 3;

    private String mainReferrer = VALUE_UNKNOWN;
    private String secondaryReferrer = VALUE_UNKNOWN;
    private String adId = VALUE_UNKNOWN;
    private boolean dataRequested = false;


    private final ApplicationLoader application;
    private final PreferenceManager preferenceManager;
    private final InstallReferrerClient referrerClient;
    private boolean isBranchAnswered;

    public TrackingManager(Context context) {
        application = (ApplicationLoader) context.getApplicationContext();
        preferenceManager = application.getPreferenceManager();
        referrerClient = newBuilder(context).build();
    }

    public void trackInstall() {
        isBranchAnswered = true;
        if (!preferenceManager.isInstallTracked() && !dataRequested) {
            requestTrackingData();
        }
    }

    public void onBroadcastReferrerReceived(@Nullable String broadcastReferrer, boolean isTestReferrer) {
        Debug.logD(TAG, "referrer from broadcast = " + broadcastReferrer);
        if (!preferenceManager.isInstallTracked() && !hasValidReferrer()) {
            this.secondaryReferrer = broadcastReferrer;
            if (TextUtils.isEmpty(preferenceManager.getKeyReferrer())) {
                preferenceManager.setKeyReferrer(broadcastReferrer);
            }
            tryToTrackInstall();
        } else if (isTestReferrer) {
            trackForceReferrer(broadcastReferrer);
        }
    }

    private void requestTrackingData() {
        dataRequested = true;
        requestAdId();
        requestReferrer();
    }

    private void requestAdId() {
        if (TextUtils.isEmpty(preferenceManager.getAdId())) {
            new AdIdTask(this).execute();
        } else {
            this.adId = preferenceManager.getAdId();
        }
    }

    private void onAdIdReceived(@Nullable String adid) {
        if (VALUE_UNKNOWN.equals(this.adId)) {
            Debug.logD(TAG, "onAdIdReceived: ");
            TrackInstall trackInstall = new TrackInstall(adid, null);
            trackInstall.getDataTask().subscribe(data -> {
                this.adId = adid;
                preferenceManager.setAdId(adid);
                tryToTrackInstall();
            }, RxUtils.getEmptyErrorConsumer("onAdIdReceived"));
        }
    }

    static class AdIdTask extends AsyncTask<Void, Void, String> {

        TrackingManager trackingManager;

        AdIdTask(TrackingManager trackingManager) {
            this.trackingManager = trackingManager;
        }

        @Override
        protected String doInBackground(Void... voids) {
            String adid = null;
            try {
                AdvertisingIdClient.Info info = AdvertisingIdClient.getAdvertisingIdInfo(trackingManager.application);
                adid = info.getId();
            } catch (Exception e) {
                Debug.logE(TAG, "AdIdTask: " + e.toString());
            }
            return adid;
        }

        @Override
        protected void onPostExecute(String adid) {
            super.onPostExecute(adid);
            trackingManager.onAdIdReceived(adid);
        }
    }


    private void requestReferrer() {
        if (!TextUtils.isEmpty(preferenceManager.getKeyReferrer())) {
            secondaryReferrer = preferenceManager.getKeyReferrer();
        }
        requestReferrerFromPlayStore(new AtomicInteger(0));
    }

    private void requestReferrerFromPlayStore(final AtomicInteger tryCount) {
        if (tryCount.incrementAndGet() <= MAX_TRY_COUNT) {
            referrerClient.startConnection(new InstallReferrerStateListener() {
                @Override
                public void onInstallReferrerSetupFinished(int responseCode) {
                    switch (responseCode) {
                        case InstallReferrerResponse.OK:
                            try {
                                String referrer = referrerClient.getInstallReferrer().getInstallReferrer();
                                closeReferrerConnection(referrer);
                            } catch (RemoteException e) {
                                requestReferrerFromPlayStore(tryCount);
                            }
                            break;

                        case InstallReferrerResponse.SERVICE_UNAVAILABLE:
                            requestReferrerFromPlayStore(tryCount);
                            break;

                        case InstallReferrerResponse.FEATURE_NOT_SUPPORTED:
                            onReferrerReceived(null);
                            break;
                    }
                }

                @Override
                public void onInstallReferrerServiceDisconnected() {
                    requestReferrerFromPlayStore(tryCount);
                }
            });
        } else {
            closeReferrerConnection(null);
        }
    }

    private void closeReferrerConnection(@Nullable String referrer) {
        if (referrerClient.isReady()) {
            referrerClient.endConnection();
        }
        onReferrerReceived(referrer);
    }

    private void onReferrerReceived(@Nullable String referrer) {
        if (VALUE_UNKNOWN.equals(this.mainReferrer)) {
            this.mainReferrer = referrer;
            tryToTrackInstall();
        }
    }

    private boolean hasValidAdId() {
        return !VALUE_UNKNOWN.equals(adId);
    }

    private void tryToTrackInstall() {
        if (!preferenceManager.isInstallTracked() && hasValidReferrer() && isBranchAnswered && hasValidAdId()) {
            trackInstall(adId, mainReferrer, secondaryReferrer);
        }
    }

    private void trackInstall(@Nullable String adid, @Nullable String mainReferrer, @Nullable String secondaryReferrer) {
        String referrer;
        if (!TextUtils.isEmpty(mainReferrer)) {
            referrer = mainReferrer;
        } else if (!TextUtils.isEmpty(secondaryReferrer)) {
            referrer = secondaryReferrer;
        } else {
            referrer = DEFAULT_REFERRER;
        }
        preferenceManager.setKeyReferrer(referrer);
        referrer = decodeReferrer(referrer);
        Debug.logD(TAG, "incoming referrer " + referrer);
        TrackInstall model = new TrackInstall(adid, referrer);
        model.getDataTask().subscribe(__ -> preferenceManager.setInstallTracked(true)
                , RxUtils.getEmptyErrorConsumer("trackInstall"));
    }

    private String decodeReferrer(@NonNull String referrer) {
        try {
            return URLDecoder.decode(referrer, "UTF-8");
        } catch (Exception e) {
            return referrer;
        }
    }

    private boolean hasValidReferrer() {
        return !VALUE_UNKNOWN.equals(mainReferrer) && (mainReferrer != null || !VALUE_UNKNOWN.equals(secondaryReferrer));
    }

    private void trackForceReferrer(@Nullable String mainReferrer) {
        preferenceManager.setInstallTracked(false);
        trackInstall(preferenceManager.getAdId(), mainReferrer, null);
    }

    public void trackBranchInstall(String query) {
        if (TextUtils.isEmpty(query)) {
            trackInstall();
        } else {
            trackForceReferrer(query);
        }
    }
}