package com.tangle.db.photo

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.tangle.db.DataSource
import com.tangle.model.profile_data.Photo
import io.reactivex.Flowable

@Dao
interface PhotoDao : DataSource<Photo> {

    @Query("SELECT * FROM Photo")
    override fun getAll(): List<Photo>

    @Query("SELECT * FROM Photo")
    override fun getAllFlowable(): Flowable<List<Photo>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override fun insert(user: Photo)

    @Query("DELETE FROM Photo")
    override fun deleteAll()

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    override fun insertAll(photos: List<Photo>)
}