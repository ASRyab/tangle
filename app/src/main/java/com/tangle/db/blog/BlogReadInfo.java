package com.tangle.db.blog;


import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Objects;

@Entity(tableName = "BlogReadInfo")
public class BlogReadInfo {
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "postId")
    private String postId;

    @ColumnInfo(name = "isRead")
    private boolean isRead;

    public BlogReadInfo(@NonNull String postId, boolean isRead) {
        this.postId = postId;
        this.isRead = isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    @NonNull
    public String getPostId() {
        return postId;
    }

    public boolean isRead() {
        return isRead;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BlogReadInfo that = (BlogReadInfo) o;
        return Objects.equals(postId, that.postId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(postId);
    }
}