package com.tangle.db.blog;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.tangle.db.DataSource;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface BlogDao extends DataSource<BlogReadInfo> {
    @Query("SELECT * FROM BlogReadInfo")
    List<BlogReadInfo> getAll();

    @Query("SELECT * FROM BlogReadInfo")
    Flowable<List<BlogReadInfo>> getAllFlowable();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(BlogReadInfo user);

    @Query("DELETE FROM BlogReadInfo")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<BlogReadInfo> users);

    @Query("DELETE FROM BlogReadInfo WHERE postId = :postId ")
    void deleteById(String postId);
}
