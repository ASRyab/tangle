package com.tangle.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context

import com.tangle.api.rpc.rpc_response.invite.InviteData
import com.tangle.db.blog.BlogDao
import com.tangle.db.blog.BlogReadInfo
import com.tangle.db.invite.InviteDao
import com.tangle.db.likes.LikeDao
import com.tangle.db.likes.LikeUser
import com.tangle.db.messages.MessagesDao
import com.tangle.db.photo.PhotoDao
import com.tangle.model.chat.MailMessagePhoenix
import com.tangle.model.profile_data.Photo

@Database(entities = [MailMessagePhoenix::class, LikeUser::class, InviteData::class, BlogReadInfo::class, Photo::class], version = 3)
abstract class AppDatabase : RoomDatabase() {

    abstract fun messagesDao(): MessagesDao

    abstract fun likeDao(): LikeDao

    abstract fun photoDao(): PhotoDao

    abstract fun inviteDao(): InviteDao

    abstract fun blogDao(): BlogDao

    companion object {
        private var INSTANCE: AppDatabase? = null
        @JvmStatic
        fun getInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    if (INSTANCE == null) {
                        INSTANCE = Room.databaseBuilder(context.applicationContext,
                                AppDatabase::class.java, "Tangle-base.db")
                                .fallbackToDestructiveMigration()
                                .build()
                    }
                }
            }
            return INSTANCE
        }
    }
}
