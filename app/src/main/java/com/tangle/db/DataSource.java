package com.tangle.db;

import java.util.List;

import io.reactivex.Flowable;

public interface DataSource<T> {
    List<T> getAll();

    Flowable<List<T>> getAllFlowable();

    void insertAll(List<T> items);

    void insert(T item);

    void deleteAll();
}