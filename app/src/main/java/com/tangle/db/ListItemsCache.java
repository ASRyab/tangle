package com.tangle.db;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.disposables.Disposables;
import io.reactivex.functions.Function;

public class ListItemsCache<T> implements CollectionCache<T, List<T>> {

    private final Set<ItemsChangeListener<T>> listeners = Collections.newSetFromMap(new ConcurrentHashMap<ItemsChangeListener<T>, Boolean>());
    private final Map<String, T> cache = Collections.synchronizedMap(new HashMap<>());
    private Function<T, String> keyFunction;

    public ListItemsCache(Function<T, String> keyFunction) {
        this.keyFunction = keyFunction;
    }

    @Override
    public Flowable<List<T>> getItems() {
        return Flowable.create(emitter -> {
            ItemsChangeListener<T> listener = emitter::onNext;
            if (!emitter.isCancelled()) {
                addListener(listener);
                emitter.setDisposable(Disposables.fromAction(() -> removeListener(listener)));
                emitter.onNext(new ArrayList<>(cache.values()));
            }
        }, BackpressureStrategy.MISSING);
    }

    @Override
    public void insertItems(List<T> items) {
        for (T item : items) {
            cache.put(getKey(item), item);
        }
        notifyListeners(items);
    }

    @Override
    public int size() {
        return cache.size();
    }

    @Override
    public void clear() {
        cache.clear();
    }

    public boolean contains(String key) {
        return cache.containsKey(key);
    }

    public T getByKey(String key) {
        return cache.get(key);
    }

    private String getKey(T item) {
        try {
            return keyFunction.apply(item);
        } catch (Exception e) {
            return null;
        }
    }

    private void notifyListeners(List<T> items) {
        for (ItemsChangeListener<T> listener : listeners) {
            listener.onItemsChanged(items);
        }
    }

    private void addListener(ItemsChangeListener<T> listener) {
        listeners.add(listener);
    }

    private void removeListener(ItemsChangeListener<T> listener) {
        listeners.remove(listener);
    }

    private interface ItemsChangeListener<T> {
        void onItemsChanged(List<T> values);
    }
}
