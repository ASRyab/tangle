package com.tangle.db.invite;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.tangle.api.rpc.rpc_response.invite.InviteData;
import com.tangle.db.DataSource;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface InviteDao extends DataSource<InviteData> {
    @Query("SELECT * FROM InviteData WHERE inviteId = :inviteId LIMIT 1")
    Flowable<InviteData> loadInviteById(String inviteId);

    @Query("SELECT * FROM InviteData")
    List<InviteData> getAll();

    @Query("SELECT * FROM InviteData")
    Flowable<List<InviteData>> getAllFlowable();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(InviteData user);

    @Query("DELETE FROM InviteData")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<InviteData> users);
}
