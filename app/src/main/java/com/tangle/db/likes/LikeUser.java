package com.tangle.db.likes;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "LikeUser")
public class LikeUser {

    @ColumnInfo(name = "isRead")
    private boolean isRead;

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "senderId")
    private String senderId;

    @ColumnInfo(name = "lastUserActivity")
    private String lastUserActivity;

    @ColumnInfo(name = "matched")
    private boolean isMatch;

    @Ignore
    public LikeUser() {
    }

    public boolean isMatch() {
        return isMatch;
    }

    public void setMatch(boolean match) {
        isMatch = match;
    }

    public String getLastUserActivity() {
        return lastUserActivity;
    }
    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    @NonNull
    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(@NonNull String senderId) {
        this.senderId = senderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LikeUser)) return false;

        LikeUser that = (LikeUser) o;
        return getSenderId().equals(that.getSenderId());

    }

    @Override
    public int hashCode() {
        return getSenderId().hashCode();
    }

    public LikeUser(@NonNull String senderId, String lastUserActivity, boolean isMatch) {
        this.senderId = senderId;
        this.lastUserActivity = lastUserActivity;
        this.isMatch = isMatch;
    }

    public void setLastUserActivity(String lastUserActivity) {
        this.lastUserActivity = lastUserActivity;
    }
}
