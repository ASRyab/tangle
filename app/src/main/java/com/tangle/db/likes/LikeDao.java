package com.tangle.db.likes;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.tangle.db.DataSource;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface LikeDao extends DataSource<LikeUser> {

    @Query("SELECT * FROM LikeUser")
    List<LikeUser> getAll();

    @Query("SELECT * FROM LikeUser")
    Flowable<List<LikeUser>> getAllFlowable();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(LikeUser user);

    @Query("DELETE FROM LikeUser")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<LikeUser> users);
}
