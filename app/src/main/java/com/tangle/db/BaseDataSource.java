package com.tangle.db;

import com.tangle.utils.RxUtils;

import io.reactivex.Flowable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class BaseDataSource {
    private static final String TAG = BaseDataSource.class.getSimpleName();

    protected Disposable doThis(Flowable f) {
        return f.subscribeOn(Schedulers.io()).unsubscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(RxUtils.getEmptyDataConsumer(), RxUtils.getEmptyErrorConsumer(TAG, "doThis"));
    }
}