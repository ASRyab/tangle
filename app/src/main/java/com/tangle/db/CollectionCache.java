package com.tangle.db;

import java.util.Collection;

import io.reactivex.Flowable;

public interface CollectionCache<T, C extends Collection<T>> {

    Flowable<C> getItems();

    void insertItems(C items);

    int size();

    void clear();
}
