package com.tangle.db.messages;

import com.tangle.db.BaseDataSource;
import com.tangle.model.chat.MailMessagePhoenix;

import java.util.List;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.Observable;

public class MessagesDataSource extends BaseDataSource implements MessagesDao {
    private final MessagesDao messagesDao;

    public MessagesDataSource(MessagesDao messagesDao) {
        this.messagesDao = messagesDao;
    }

    @Override
    public List<MailMessagePhoenix> getAll() {
        return messagesDao.getAll();
    }

    @Override
    public Flowable<List<MailMessagePhoenix>> getAllFlowable() {
        return messagesDao.getAllFlowable();
    }

    @Override
    public void insert(MailMessagePhoenix message) {
        doThis(insertObservable(message).toFlowable(BackpressureStrategy.BUFFER));
    }

    public Observable<MailMessagePhoenix> insertObservable(MailMessagePhoenix message) {
        return Observable.fromCallable(() -> {
            messagesDao.insert(message);
            return message;
        });
    }

    @Override
    public void deleteAll() {
        messagesDao.deleteAll();
    }

    @Override
    public void insertAll(List<MailMessagePhoenix> messages) {
        doThis(insertAllObservable(messages).toFlowable(BackpressureStrategy.BUFFER));
    }

    public Observable<List<MailMessagePhoenix>> insertAllObservable(List<MailMessagePhoenix> messages) {
        return Observable.fromCallable(() -> {
            messagesDao.insertAll(messages);
            return messages;
        });
    }
}