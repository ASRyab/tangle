package com.tangle.db.messages;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.tangle.db.DataSource;
import com.tangle.model.chat.MailMessagePhoenix;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface MessagesDao extends DataSource<MailMessagePhoenix> {
    @Query("SELECT * FROM MailMessagePhoenix")
    List<MailMessagePhoenix> getAll();

    @Query("SELECT * FROM MailMessagePhoenix")
    Flowable<List<MailMessagePhoenix>> getAllFlowable();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(MailMessagePhoenix message);

    @Query("DELETE FROM MailMessagePhoenix")
    void deleteAll();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<MailMessagePhoenix> messages);
}