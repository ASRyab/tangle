package com.tangle.api;

import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.text.TextUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.itkacher.okhttpprofiler.OkHttpProfilerInterceptor;
import com.tangle.ApplicationLoader;
import com.tangle.api.deserializers.DateTypeDeserializer;
import com.tangle.api.deserializers.ProfileDeserializer;
import com.tangle.api.requests.ApiActivities;
import com.tangle.api.requests.ApiAuth;
import com.tangle.api.requests.ApiBlog;
import com.tangle.api.requests.ApiChat;
import com.tangle.api.requests.ApiDictionary;
import com.tangle.api.requests.ApiEvents;
import com.tangle.api.requests.ApiFunnel;
import com.tangle.api.requests.ApiLikes;
import com.tangle.api.requests.ApiMedia;
import com.tangle.api.requests.ApiPayment;
import com.tangle.api.requests.ApiProfile;
import com.tangle.api.rpc.SocketManager;
import com.tangle.api.utils.Config;
import com.tangle.api.utils.DeviceUtils;
import com.tangle.api.utils.PhoenixUtils;
import com.tangle.api.utils.RxErrorHandlingCallAdapterFactory;
import com.tangle.model.auth.AuthDataResponse;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.utils.ConvertingUtils;
import com.tangle.utils.PreferenceManager;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Api {
    private final String TAG = this.getClass().getSimpleName();

    private static final int CONNECTION_TIMEOUT = 20;
    final private static String AUTHORIZATION_HEADER = "Authorization";
    final private static String AUTHORIZATION_PREFIX = "Bearer ";

    private static Api instance;
    private Retrofit retrofit;
    private Gson gson;
    private ServerSession session;
    private SocketManager socketManager;
    private OkHttpClient okHttpClient;
    private OkHttpClient.Builder clientBuilder;

    private Api() {
    }

    public synchronized static Api getInst() {
        if (instance == null) {
            instance = new Api();
            instance.build();
        }
        return instance;
    }

    private void build() {
        initSession();
        resetRetrofit();
        socketManager = new SocketManager.Builder()
                .setSocketPort(Config.DEFAULT_SOCKET_PORT)
                .setOkHttpClient(clientBuilder.build())
                .setServerUrl(Config.getBaseUrl())
                .create();


    }

    public void resetRetrofit() {
        clientBuilder = getClientBuilderWithCookieAction();
        clientBuilder.readTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);
        clientBuilder.connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS);

        okHttpClient = clientBuilder.build();
        retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxErrorHandlingCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .baseUrl(Config.getBaseUrl())
                .client(okHttpClient)
                .build();
    }

    public synchronized Gson getGson() {
        if (gson == null) {
            GsonBuilder builder = new GsonBuilder()
                    .setDateFormat(ConvertingUtils.DATE_FORMAT_YMD_HMS);
            builder.registerTypeAdapter(ProfileData.class, new ProfileDeserializer());
            builder.registerTypeAdapter(Date.class, new DateTypeDeserializer());
            builder.setLenient();
            gson = builder
                    .create();
        }
        return gson;
    }

    public ApiAuth auth() {
        return retrofit.create(ApiAuth.class);
    }

    public ApiBlog blog() {
        return retrofit.create(ApiBlog.class);
    }

    public ApiFunnel funnel() {
        return retrofit.create(ApiFunnel.class);
    }

    public ApiLikes likes() {
        return retrofit.create(ApiLikes.class);
    }

    public ApiEvents event() {
        return retrofit.create(ApiEvents.class);
    }

    public ApiChat chat() {
        return retrofit.create(ApiChat.class);
    }

    public ApiProfile profile() {
        return retrofit.create(ApiProfile.class);
    }

    public ApiDictionary dictionary() {
        return retrofit.create(ApiDictionary.class);
    }

    public ApiActivities activities() {
        return retrofit.create(ApiActivities.class);
    }

    public ApiPayment payment() {
        return retrofit.create(ApiPayment.class);
    }

    public ApiMedia media() {
        return retrofit.create(ApiMedia.class);
    }

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public OkHttpClient.Builder getClientBuilderWithCookieAction() {
        final OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();

        clientBuilder.addInterceptor(chain -> {
            Request.Builder builder = chain.request().newBuilder()
                    .addHeader("Accept-Language", Locale.getDefault().getLanguage() + ";q=1")
                    .addHeader("App-Key", getAppKey())
                    .addHeader("X-Requested-With", "XMLHttpRequest")
                    .addHeader("Content-Type", "application/x-www-form-urlencoded")
                    .addHeader("App-Marker", "hand3856be45")
                    .addHeader("App-DeviceId", session.getDeviceIDHex());
            String ip = ApplicationLoader.getApplicationInstance().getPreferenceManager().getEmulateIP();
            if (!TextUtils.isEmpty(ip)) {
                builder.addHeader("Cookie", "ip_address=" + ip); //this option for emulating other country app usage
            }

            String userAgent = getUserAgent();
            builder.addHeader("User-Agent", userAgent);
            if (!TextUtils.isEmpty(session.getAccessToken())) {
                builder.addHeader(AUTHORIZATION_HEADER, AUTHORIZATION_PREFIX + session.getAccessToken());
            }
            Request request = builder.build();
            return chain.proceed(request);
        });
        if (Config.isLogging()) {
            clientBuilder.addInterceptor(new OkHttpProfilerInterceptor());
            clientBuilder.addNetworkInterceptor(new StethoInterceptor());
        }
        return clientBuilder;
    }

    private static String getAppKey() {
        Calendar calendar = Calendar.getInstance();
        long now = calendar.getTimeInMillis();
        long utc = (now / 1000);
        String utcString = String.valueOf(utc);
        StringBuilder apiKey = new StringBuilder();
        StringBuilder key = new StringBuilder();
        key.append(utcString);
        apiKey.append(PhoenixUtils.getMDFive(key.toString()));
        apiKey.append(Long.toHexString(utc));
        return apiKey.toString();
    }

    public String getUserAgent() {
        StringBuilder builder = new StringBuilder();
        try {
            Context context = ApplicationLoader.getContext();
            String versionName = context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
            builder.append(context.getPackageName()).append("/").append(versionName);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        builder.append(" ").append(getUserAgentString());
        return builder.toString();
    }


    protected String getUserAgentString() {
        try {
            Context context = ApplicationLoader.getContext();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                return WebSettings.getDefaultUserAgent(context);
            } else {
                WebSettings webSettings = new WebView(context).getSettings();
                return webSettings.getUserAgentString();
            }
        } catch (Exception e) {
            e.printStackTrace();
            String userAgent = System.getProperty("http.agent");
            if (userAgent != null) {
                return userAgent;
            } else {
                return "Mozilla/5.0 (Linux; Android 4.2.2; ALCATEL ONE TOUCH 7041X Build/JDQ39) AppleWebKit/537.36 (KHTML, like Gecko) " +
                        "Chrome/35.0.1916.141 Mobile Safari/537.36"; //if there isn't any other way to obtain user agent
            }
        }
    }

    private ServerSession createServerSession() {
        return createServerSession(null);
    }

    public void createSessionWithAuthData(AuthDataResponse authData) {
        session = createServerSession(authData);
        saveSession();
        connectToWebSocket();
    }

    private void saveSession() {
        PreferenceManager preferenceManager = ApplicationLoader.getApplicationInstance().getPreferenceManager();
        preferenceManager.saveSession(session);
    }


    private ServerSession createServerSession(AuthDataResponse authData) {
        Context context = ApplicationLoader.getContext();
        ServerSession result;
        String deviceId = DeviceUtils.createDeviceId();
        String deviceIDMultisite = DeviceUtils.getDeviceIdCRC32(context);
        String deviceIdHex = DeviceUtils.getDeviceIdHex(context);
        String appVersion = DeviceUtils.getPackageInfo(context, DeviceUtils.PackageInfoField.APP_VERSION);
        String bundleId = context.getPackageName();

        if (authData == null) {
            result = new ServerSession(deviceId, deviceIDMultisite, deviceIdHex, appVersion, bundleId);
        } else {
            result = new ServerSession(deviceId, deviceIDMultisite, deviceIdHex, appVersion, bundleId, authData);
        }

        return result;
    }

    private void initSession() {
        if (session == null) {
            this.session = ApplicationLoader.getApplicationInstance().getPreferenceManager().loadSession();
        }
        if (session == null) {
            this.session = createServerSession();
        }
    }

    public ServerSession getSession() {
        return session;
    }

    public synchronized void clearSession() {
        session = null;
        ApplicationLoader.getApplicationInstance().getPreferenceManager().clearSession();
        initSession();
    }

    public boolean isSessionValid() {
        return session != null && session.isAlive();
    }

    public void connectToWebSocket() {
        socketManager.connectSocket(session);
    }

    public void refreshSession(AuthDataResponse authData) {
        session.updateAccessToken(authData.getAccessToken());
        session.setLastRequest(System.currentTimeMillis());
        if (TextUtils.isEmpty(session.getTokenType())) {
            session.updateTokenType(authData.getTokenType());
        }
        saveSession();
        connectToWebSocket();
    }

    public SocketManager getSocketManager() {
        return socketManager;
    }
}