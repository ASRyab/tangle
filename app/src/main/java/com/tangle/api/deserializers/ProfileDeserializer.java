package com.tangle.api.deserializers;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.tangle.model.pojos.Gender;
import com.tangle.model.profile_data.Photo;
import com.tangle.model.profile_data.ProfileData;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class ProfileDeserializer implements JsonDeserializer<ProfileData> {
    private static Gson gson;

    @Override
    public ProfileData deserialize(JsonElement profileElement, Type type, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        if (profileElement.isJsonNull() || ((JsonObject) profileElement).size() == 0) {
            return null;
        }
        if (gson == null) {
            gson = new Gson();
        }
        JsonObject profileObject = profileElement.getAsJsonObject();
        JsonElement photosObject = profileObject.get("photos");
        ProfileData profileData;
        if (photosObject != null && !photosObject.isJsonNull()) {
            if (photosObject.isJsonArray()) {
                profileData = gson.fromJson(profileElement, ProfileData.class);
                List<Photo> photos = profileData.photos;
                if (photos != null && !profileData.photos.isEmpty()) {
                    for (Photo photo : photos) {
                        if (photo.isPrimary == 1) profileData.primaryPhoto = photo;
                    }
                }
            } else {
                JsonObject photoElement = photosObject.getAsJsonObject();
                Photo photo = new Photo();
                photo.avatar = (photoElement.get("url").getAsString());
                profileObject.remove("photos");
                profileData = gson.fromJson(profileObject, ProfileData.class);
                profileData.photos = (Collections.emptyList());
                boolean hasPhoto = photoElement.get("has").getAsBoolean();
                if (hasPhoto) {
                    profileData.primaryPhoto = (photo);
                    profileData.photo_count
                            = (photoElement.getAsJsonPrimitive("count").getAsInt());
                    profileData.photos = (Collections.singletonList(photo));
                }
                profileData.age = (profileObject.getAsJsonPrimitive("age").getAsInt());
            }
        } else {
            profileData = gson.fromJson(profileElement, ProfileData.class);
            profileData.photos = (Collections.emptyList());
        }


        if (profileData.gender == null && profileElement.getAsJsonObject().has("gender_key")) {
            JsonElement genderElement = profileElement.getAsJsonObject().get("gender_key");
            profileData.gender = genderElement.getAsString().equals("1") ? Gender.MALE : Gender.FEMALE;
        }

        checkPrimaryPhoto(profileData);

        if (TextUtils.isEmpty(profileData.login) && !TextUtils.isEmpty(profileData.getNOTUSEDName())) {
            profileData.login = profileData.getNOTUSEDName();
        }
        if (profileData.photo_count == 0 && profileData.getNOTUSEDPhotoCount() != 0) {
            profileData.photo_count = profileData.getNOTUSEDPhotoCount();
        }

        return profileData;
    }

    private void checkPrimaryPhoto(ProfileData profileData) {
        if (profileData.primaryPhoto == null) {
            Photo photo = new Photo();
            photo.avatar = "";
            photo.normal = "";
            profileData.primaryPhoto = photo;
        }
    }
}