package com.tangle.api.rx_tasks.msg;

import android.text.TextUtils;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.chat.ChatHistoryData;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class ChatHistory extends ApiTask<ChatHistoryData> {
    private Integer offset = 0;
    private String userId;
    private HistoryType historyType = HistoryType.UNDEFINED;

    public ChatHistory(Integer offset, HistoryType historyType, String userId) {
        this(offset, historyType);
        this.userId = userId;
    }

    public ChatHistory(Integer offset, HistoryType historyType) {
        this(offset);
        this.historyType = historyType;
    }

    public ChatHistory(Integer offset,  String userId) {
        this(offset);
        this.userId = userId;
    }

    public ChatHistory(Integer offset) {
        this();
        this.offset = offset;
    }

    public ChatHistory() {
        super();
    }

    @Override
    protected Observable<BaseModelResponse<ChatHistoryData>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("offset", offset.toString());
        if (!TextUtils.isEmpty(userId)) {
            map.put("userId", userId);
        }
        switch (historyType) {
            case INCOMING:
                return Api.getInst().chat().getPrivateChatList(map);
            case OUTGOING:
                return Api.getInst().chat().getOutgoingChatList(map);
            case UNDEFINED:
            default:
                return Api.getInst().chat().getUnsortedChatList(map);
        }
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}