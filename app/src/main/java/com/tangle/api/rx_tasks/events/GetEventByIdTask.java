package com.tangle.api.rx_tasks.events;

import com.tangle.api.rx_tasks.ObservableTask;
import com.tangle.model.event.EventInfo;

import io.reactivex.Observable;

public class GetEventByIdTask extends ObservableTask<EventInfo> {
    private String eventId;

    public GetEventByIdTask(String eventId) {
        super(true);
        this.eventId = eventId;
    }

    @Override
    protected Observable<EventInfo> getObservableTask() {
        return new GetEventById(eventId).getDataTask();
    }
}