package com.tangle.api.rx_tasks.dictionary;

import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.ObservableTask;
import com.tangle.base.repository.ObservableRepository;
import com.tangle.model.pojos.DictionaryItem;

import java.util.List;

import io.reactivex.Observable;

public class GetInterestsModel extends ObservableTask<List<DictionaryItem>> {

    private ObservableRepository<DictionaryItem> repository;

    public GetInterestsModel() {
        super(false);
        repository = ApplicationLoader.getApplicationInstance().getRepositoriesHolder().getInterestsRepository();
    }

    private Observable<List<DictionaryItem>> getFromServer() {
        return new GetDictionaryInterests().getDataTask().doOnNext(interests -> repository.addAll(interests));
    }

    private Observable<List<DictionaryItem>> getFromRepository() {
        return repository.getAll();
    }

    @Override
    protected Observable<List<DictionaryItem>> getObservableTask() {
        return getFromRepository().flatMap(list -> {
            if (list.isEmpty()) {
                return getFromServer();
            } else {
                return Observable.just(list);
            }
        });
    }
}
