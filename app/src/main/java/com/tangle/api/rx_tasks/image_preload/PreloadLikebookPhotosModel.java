package com.tangle.api.rx_tasks.image_preload;

import com.tangle.api.rx_tasks.ObservableTask;
import com.tangle.model.like_or_not.LikeOrNotUser;
import com.tangle.model.profile_data.Photo;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class PreloadLikebookPhotosModel extends ObservableTask<String> {

    private List<LikeOrNotUser> users;

    public PreloadLikebookPhotosModel(List<LikeOrNotUser> users) {
        super(false);
        this.users = users;
    }

    @Override
    protected Observable<String> getObservableTask() {
        return new PreloadImageModel(convert(users)).getTask();
    }

    private List<String> convert(List<LikeOrNotUser> users) {
        List<String> urls = new ArrayList<>();
        for (LikeOrNotUser u : users) {
            List<String> userUrls = fetchPhotos(u);
            urls.addAll(userUrls);
        }
        return urls;
    }

    private List<String> fetchPhotos(LikeOrNotUser user) {
        List<String> urls = new ArrayList<>();
        for (Photo photo : user.photos) {
            urls.add(photo.normal);
        }
        return urls;
    }
}
