package com.tangle.api.rx_tasks.activities.liked_you;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.activities.liked_you.LikedYouResponse;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;

public class LikedYou extends ApiTask<LikedYouResponse> {

    private final Integer limit;
    private final Integer offset;

    public LikedYou(@NonNull int limit, @NonNull int offset) {
        super();
        this.limit = limit;
        this.offset = offset;
    }

    @Override
    protected Observable<BaseModelResponse<LikedYouResponse>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("limit", limit.toString());
        map.put("offset", offset.toString());
        return Api.getInst().activities().getLikedYouList(map);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}