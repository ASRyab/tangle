package com.tangle.api.rx_tasks.events;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.event.EventInfo;

import java.util.List;

import io.reactivex.Observable;

public class GetEventsByIds extends ApiTask<List<EventInfo>> {
    private List<String> eventIds;

    public GetEventsByIds(List<String> eventIds) {
        super();
        this.eventIds = eventIds;
    }

    @Override
    protected Observable<BaseModelResponse<List<EventInfo>>> getObservableTask() {
        return Api.getInst().event().getEventsByIds(eventIds);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}