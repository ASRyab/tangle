package com.tangle.api.rx_tasks.msg;

public enum HistoryType {
    UNDEFINED("undefined"),
    INCOMING("incoming"),
    OUTGOING("outgoing");

    private final String key;

    HistoryType(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }
}