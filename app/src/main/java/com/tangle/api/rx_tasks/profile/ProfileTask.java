package com.tangle.api.rx_tasks.profile;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.profile_data.ProfileData;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import io.reactivex.Observable;

public class ProfileTask extends ApiTask<ProfileData> {
    private String userId = "";

    public ProfileTask() {
        this("");
    }

    public ProfileTask(String id) {
        super();
        this.userId = id;
    }

    private String getArgsJson() {
        String json = "";
        try {
            json = createGraphObject().toString();
            json = json.replaceAll("null", "\"\"");
            json = URLEncoder.encode(json, "UTF8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return json;
    }

    private JsonElement createGraphObject() {
        JsonElement json = new Gson().toJsonTree(new Object());
        JsonObject jsonObject = json.getAsJsonObject();
        jsonObject.addProperty("id", userId);
        jsonObject.addProperty("gender", "");
        jsonObject.addProperty("age", "");
//        JsonObject jsonObject1 = new JsonObject();
//        jsonObject1.addProperty("resized", "");
//        jsonObject1.addProperty("attributes", "");
//        jsonObject1.addProperty("avatar", "");
//        jsonObject1.addProperty("createdAt", "");
//        jsonObject1.addProperty("id", "");
//        jsonObject1.addProperty("is_primary", "");// but its bool
//        jsonObject1.addProperty("normal", "");
//        jsonObject1.addProperty("resizeType", "large");
        jsonObject.addProperty("photos", "");
        jsonObject.addProperty("careerLevel", "");
        jsonObject.addProperty("geo", "");
        jsonObject.addProperty("location", "");
        jsonObject.addProperty("looking", "");
        jsonObject.addProperty("sexual_orientation", "");
        jsonObject.addProperty("description", "");
        jsonObject.addProperty("login", "");
        jsonObject.addProperty("email", "");
        jsonObject.addProperty("birthday", "");
        jsonObject.addProperty("gender_key", "");
        jsonObject.addProperty("industry", "");
        jsonObject.addProperty("futureEventsList", "");
        jsonObject.addProperty("pastEventsList", "");
        jsonObject.addProperty("name", "");
        jsonObject.addProperty("photo", "");
        jsonObject.addProperty("lookingForGender", "");
        jsonObject.addProperty("primaryPhoto", "");
        jsonObject.addProperty("about", "");
        jsonObject.addProperty("description", "");
        jsonObject.addProperty("photo_count", "");
        jsonObject.addProperty("interests", "");
        jsonObject.addProperty("isYouLikedUser", "");
        jsonObject.addProperty("isTargetCountry", "");
        jsonObject.addProperty("isMatchedUser", "");
        jsonObject.addProperty("isLikedMeUser", "");
        jsonObject.addProperty("isYouSkippedUser", "");
        jsonObject.addProperty("isUserCanBeLiked", "");
        jsonObject.addProperty("matchedEvent", "");
        jsonObject.addProperty("blockedUser", "");
        jsonObject.addProperty("blockedByUser", "");
        jsonObject.addProperty("eventsForInvite", "");
        jsonObject.addProperty("isTester", "");
        return json;
    }

    @Override
    protected Observable<BaseModelResponse<ProfileData>> getObservableTask() {
        String jsonData = getArgsJson();
        return TextUtils.isEmpty(userId)
                ? Api.getInst().profile().getOwnInfo(jsonData)
                : Api.getInst().profile().getOtherUserInfo(userId, jsonData);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}