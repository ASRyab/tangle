package com.tangle.api.rx_tasks.profile;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.EmptyDataResponse;
import com.tangle.utils.CollectionUtils;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class ChangePassword extends ApiTask<EmptyDataResponse> {

    private String newPass;
    private String oldPass;

    public ChangePassword(String oldPass, String newPass) {
        this.oldPass = oldPass;
        this.newPass = newPass;
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }

    @Override
    protected Observable<BaseModelResponse<EmptyDataResponse>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        CollectionUtils.putStringSafe(map, "AccountPasswordForm[oldPassword]", oldPass);
        CollectionUtils.putStringSafe(map, "AccountPasswordForm[newPassword]", newPass);
        return Api.getInst().profile().updatePassword(map);
    }
}