package com.tangle.api.rx_tasks.profile.update;

import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.ObservableTask;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.managers.UserManager;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.model.profile_data.ProfileUpdateData;
import com.tangle.screen.likebook.filter.AgeRange;
import com.tangle.utils.PreferenceManager;

import io.reactivex.Observable;
import io.reactivex.functions.Consumer;

public class ProfileUpdateModel extends ObservableTask<ProfileUpdateData> {
    private final ProfileData profile;

    public ProfileUpdateModel(ProfileData profile) {
        super(false);
        this.profile = profile;
    }

    private boolean isCityError(Throwable throwable) {
        return (throwable instanceof ErrorResponse) && "city not valid".equals(((ErrorResponse) throwable).getMeta().getFirstMessageByKey("0"));
    }

    @Override
    protected Observable<ProfileUpdateData> getObservableTask() {
        return new ProfileUpdate(profile)
                .getDataTask()
                .onErrorResumeNext(throwable -> isCityError(throwable) ? Observable.just(ProfileUpdateData.convertFromProfile(profile))
                        : Observable.error(throwable))
                .doOnNext(this::saveUpdateProfile);
    }

    private void saveUpdateProfile(ProfileUpdateData profileUpdateData) {
        PreferenceManager preferenceManager = ApplicationLoader.getApplicationInstance().getPreferenceManager();
        if (preferenceManager.getFilterAgeRange() == null && profileUpdateData.age != null) {
            preferenceManager.saveFilterAgeRange(AgeRange.getByAge(profileUpdateData.age));
        }
    }

    public Observable<ProfileData> saveName() {
        return saveProfileData(profileData -> profileData.login = profile.login);
    }

    public Observable<ProfileData> saveAge() {
        return saveProfileData(profileData -> profileData.age = profile.age);
    }

    private Observable<ProfileData> saveProfileData(Consumer<ProfileData> mapper) {
        UserManager manager = ApplicationLoader.getApplicationInstance().getUserManager();
        return getTask()
                .flatMap(profileData -> manager.currentUser())
                .doOnNext(mapper)
                .doOnNext(manager::setCurrentUser);
    }

    public Observable<ProfileData> saveCareer() {
        return saveProfileData(profileData -> profileData.setCareerLevel(profile.getCareerLevel()));
    }

    public Observable<ProfileData> saveGeo() {
        return saveProfileData(profileData -> {
            profileData.geo.city = profile.geo.city;
            profileData.geo.region = profile.geo.region;
        });
    }

    public Observable<ProfileData> saveIndustry() {
        return saveProfileData(profileData -> profileData.setIndustry(profile.getIndustry()));
    }

    public Observable<ProfileData> saveInterests() {
        return saveProfileData(profileData -> profileData.interests = profile.interests);
    }

    public Observable<ProfileData> saveLookingFor() {
        return saveProfileData(profileData -> profileData.looking.gender = profile.looking.gender);
    }
}