package com.tangle.api.rx_tasks.auth.logout;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.EmptyDataResponse;

import io.reactivex.Observable;

public class Logout extends ApiTask<EmptyDataResponse> {

    @Override
    protected boolean isNeedSign() {
        return false;
    }

    @Override
    protected Observable<BaseModelResponse<EmptyDataResponse>> getObservableTask() {
        return Api.getInst().auth().logout();
    }
}