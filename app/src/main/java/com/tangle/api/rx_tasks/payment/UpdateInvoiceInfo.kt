package com.tangle.api.rx_tasks.payment

import com.tangle.api.Api
import com.tangle.api.rx_tasks.ApiTask
import com.tangle.model.BaseModelResponse
import com.tangle.model.payment.InvoiceInfo
import io.reactivex.Observable
import java.util.*

class UpdateInvoiceInfo
(private val invoiceId: String) : ApiTask<List<InvoiceInfo>>(true) {


    override fun getObservableTask(): Observable<BaseModelResponse<List<InvoiceInfo>>> {
        val map = HashMap<String, String>()
        map["invoiceId"] = invoiceId
        return Api.getInst().payment().updateInvoiceInfo(map)
    }

    override fun isNeedSign(): Boolean {
        return true
    }
}


