package com.tangle.api.rx_tasks.funnel.geo_regions;

import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.ObservableTask;
import com.tangle.base.repository.ObservableRepository;

import java.util.List;

import io.reactivex.Observable;

public class GeoRegionsModel extends ObservableTask<List<String>> {

    private String country, city;
    private ObservableRepository<String> repository;

    public GeoRegionsModel(String country, String city) {
        super(false);
        repository = ApplicationLoader.getApplicationInstance().getRepositoriesHolder().getRegionsRepository();
        this.country = country;
        this.city = city;
    }

    @Override
    protected Observable<List<String>> getObservableTask() {
        return getFromRepository().flatMap(list -> {
            if (list.isEmpty()) {
                return getFromServer();
            } else {
                return Observable.just(list);
            }
        });
    }

    private Observable<List<String>> getFromServer() {
        return new GeoRegions(country, city).getDataTask()
                .doOnNext(regions -> repository.addAll(regions));
    }

    private Observable<List<String>> getFromRepository() {
        return repository.getAll();
    }
}
