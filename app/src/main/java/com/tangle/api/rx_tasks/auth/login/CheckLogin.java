package com.tangle.api.rx_tasks.auth.login;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.EmptyDataResponse;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class CheckLogin extends ApiTask<EmptyDataResponse> {
    private String email;
    private String password;

    public CheckLogin(String email, String password) {
        this.email = email;
        this.password = password;
    }

    @Override
    protected boolean isNeedSign() {
        return false;
    }

    @Override
    protected Observable<BaseModelResponse<EmptyDataResponse>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("LoginForm[scenario]", "loginByEmailOrMob");
        map.put("LoginForm[email]", email);
        map.put("LoginForm[password]", password);
        return Api.getInst().auth().checkLogin(map);
    }
}
