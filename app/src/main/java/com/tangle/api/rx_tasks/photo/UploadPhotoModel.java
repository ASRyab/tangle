package com.tangle.api.rx_tasks.photo;

import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.ObservableTask;
import com.tangle.model.UploadPhotoData;
import com.tangle.utils.photo_compressor.Compressor;

import java.io.File;
import java.util.Calendar;

import io.reactivex.Flowable;
import io.reactivex.Observable;

public class UploadPhotoModel extends ObservableTask<UploadPhotoData> {
    private final File photoFile;
    private final boolean isChatImg;

    public UploadPhotoModel(File photoFile, boolean isChatImg) {
        super(true);
        this.photoFile = photoFile;
        this.isChatImg = isChatImg;
    }

    @Override
    protected Observable<UploadPhotoData> getObservableTask() {
        if (photoFile == null) {
            return Observable.empty();
        } else {
            return getCompressObservable().toObservable().flatMap(photo -> new UploadPhoto(photo, isChatImg).getDataTask());
        }
    }

    private Flowable<File> getCompressObservable() {
        long time = Calendar.getInstance().get(Calendar.SECOND);
        return new Compressor(ApplicationLoader.getContext()).setMaxHeight(1920).setMaxWidth(1080)
                .compressToFileAsFlowable(photoFile, time + photoFile.getName());
    }
}
