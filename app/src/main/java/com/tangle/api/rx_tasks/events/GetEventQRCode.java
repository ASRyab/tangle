package com.tangle.api.rx_tasks.events;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

public class GetEventQRCode extends ApiTask<List<String>> {
    private String eventId;

    public GetEventQRCode(String eventId) {
        super();
        this.eventId = eventId;
    }

    @Override
    protected Observable<BaseModelResponse<List<String>>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("eventId", eventId);
        return Api.getInst().event().getEventQRCode(map);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}