package com.tangle.api.rx_tasks.dictionary;

import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.ObservableTask;
import com.tangle.base.repository.ObservableRepository;
import com.tangle.model.pojos.DictionaryItem;

import java.util.List;

import io.reactivex.Observable;

public class GetCareersModel extends ObservableTask<List<DictionaryItem>> {

    private ObservableRepository<DictionaryItem> repository;

    public GetCareersModel() {
        super(false);
        repository = ApplicationLoader.getApplicationInstance().getRepositoriesHolder().getCareerLevelsRepository();
    }

    private Observable<List<DictionaryItem>> getFromServer() {
        return new GetDictionaryCareers().getDataTask().doOnNext(careers -> repository.addAll(careers));
    }

    private Observable<List<DictionaryItem>> getFromRepository() {
        return repository.getAll();
    }

    @Override
    protected Observable<List<DictionaryItem>> getObservableTask() {
        return getFromRepository().flatMap(list -> {
            if (list.isEmpty()) {
                return getFromServer();
            } else {
                return Observable.just(list);
            }
        });
    }

}
