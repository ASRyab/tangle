package com.tangle.api.rx_tasks.photo;

import com.tangle.ApplicationLoader;
import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.profile_data.PhotoDelete;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class DeletePhoto extends ApiTask<PhotoDelete> {
    private final String photoId;

    public DeletePhoto(String photoId) {
        super();
        this.photoId = photoId;
    }

    @Override
    protected Observable<BaseModelResponse<PhotoDelete>> getObservableTask() {
        PhotoRequestParamObj photoRequestParamObj = new PhotoRequestParamObj();
        photoRequestParamObj.file = photoId;
        Map<String, String> map = new HashMap<>();
        map.put("data", Api.getInst().getGson().toJson(photoRequestParamObj));
        map.put("useDeferredDeleteMethod", "0");
        return Api.getInst().profile().deletePhoto(map)
                .doOnNext(__ -> ApplicationLoader.getApplicationInstance().getPhotoManager()
                        .removeByUser(photoId));
    }

    @Override
    protected boolean isNeedSign() {
        return false;
    }
}