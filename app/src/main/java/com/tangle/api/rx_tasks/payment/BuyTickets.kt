package com.tangle.api.rx_tasks.payment

import android.text.TextUtils
import com.tangle.api.Api
import com.tangle.api.rx_tasks.ApiTask
import com.tangle.model.BaseModelResponse
import com.tangle.model.payment.PaymentData
import io.reactivex.Observable
import java.util.*

class BuyTickets
private constructor (private val eventId: String, private val userId: String?, private val ticketsCount: Int, var inviteHolds: InviteHolds?)
    : ApiTask<PaymentData>(true) {

    constructor(eventId: String, userId: String, ticketsCount: Int) : this(eventId, userId, ticketsCount, null)
    constructor(eventId: String, userId: String, inviteHolds: InviteHolds) : this(eventId, userId, 0, inviteHolds)

    override fun getObservableTask(): Observable<BaseModelResponse<PaymentData>> {
        val map = HashMap<String, String>()
        map["eventId"] = eventId
        inviteHolds?.let {
            map["option"]=it.code.toString()
        }
        return if (userId != null && !TextUtils.isEmpty(userId)) {
            map["userId"] = userId
            Api.getInst().payment().invite(map)
        } else  {
            map["ticketsCount"] = ticketsCount.toString()
            Api.getInst().payment().book(map)
        }
    }

    override fun isNeedSign(): Boolean {
        return true
    }
}

enum class InviteHolds(val code: kotlin.Int) {
    CHARGE_OWN_INVOICE(1), HOLD_INVOICES(2)
}

