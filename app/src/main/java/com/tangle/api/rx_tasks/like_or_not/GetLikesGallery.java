package com.tangle.api.rx_tasks.like_or_not;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.like_or_not.LikeOrNotGalleryData;
import com.tangle.screen.likebook.filter.AgeRange;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class GetLikesGallery extends ApiTask<LikeOrNotGalleryData> {

    private int offset;
    private AgeRange ageRange;

    public GetLikesGallery(int offset, AgeRange ageRange) {
        this.offset = offset;
        this.ageRange = ageRange;
    }

    @Override
    protected Observable<BaseModelResponse<LikeOrNotGalleryData>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("offset", "0"); //todo need refactor after smoke test
        map.put("ageFrom", Integer.toString(ageRange.from));
        map.put("ageTo", Integer.toString(ageRange.to));
        return Api.getInst().likes().getLikesGallery(map);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}
