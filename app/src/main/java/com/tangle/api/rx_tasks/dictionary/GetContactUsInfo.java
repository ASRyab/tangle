package com.tangle.api.rx_tasks.dictionary;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.contact_us.ContactUsData;

import io.reactivex.Observable;

public class GetContactUsInfo extends ApiTask<ContactUsData> {
    @Override
    protected boolean isNeedSign() {
        return true;
    }

    @Override
    protected Observable<BaseModelResponse<ContactUsData>> getObservableTask() {
        return Api.getInst().dictionary().getContactUsInfo();
    }
}
