package com.tangle.api.rx_tasks.profile;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.TargetCountry;

import io.reactivex.Observable;

public class GetTargetCountry extends ApiTask<TargetCountry> {

    @Override
    protected boolean isNeedSign() {
        return false;
    }

    @Override
    protected Observable<BaseModelResponse<TargetCountry>> getObservableTask() {
        return Api.getInst().profile().getTargetCountry();
    }
}