package com.tangle.api.rx_tasks.auth.refresh_token;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.auth.AuthDataResponse;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class RefreshToken extends ApiTask<AuthDataResponse> {

    private String token;

    public RefreshToken(String token) {
        super();
        this.token = token;
    }


    @Override
    protected Observable<BaseModelResponse<AuthDataResponse>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("key", token);
        return Api.getInst().auth().refreshToken(map);
    }

    @Override
    protected boolean isNeedSign() {
        return false;
    }
}