package com.tangle.api.rx_tasks.image_preload;

import com.tangle.api.rx_tasks.ObservableTask;
import com.tangle.model.event.EventInfo;
import com.tangle.model.event.Media;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;

public class PreloadEventsPhotosModel extends ObservableTask<String> {

    private List<EventInfo> events;

    public PreloadEventsPhotosModel(List<EventInfo> events) {
        super(false);
        this.events = events;
    }

    @Override
    protected Observable<String> getObservableTask() {
        return new PreloadImageModel(convert(events)).getTask();
    }

    private List<String> convert(List<EventInfo> events) {
        List<String> urls = new ArrayList<>();
        for (EventInfo e : events) {
            List<String> userUrls = fetchPhotos(e);
            urls.addAll(userUrls);
        }
        return urls;
    }

    private List<String> fetchPhotos(EventInfo event) {
        List<String> urls = new ArrayList<>();
        for (Media m : event.getActualMediaList()) {
            urls.add(m.photoThumbnail2xUrl);
        }
        return urls;
    }
}
