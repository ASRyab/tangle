package com.tangle.api.rx_tasks.events;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.event.EventInfo;

import io.reactivex.Observable;

public class GetEventById extends ApiTask<EventInfo> {
    private String eventId;

    public GetEventById(String eventId) {
        super();
        this.eventId = eventId;
    }

    @Override
    protected Observable<BaseModelResponse<EventInfo>> getObservableTask() {
        return Api.getInst().event().getEventById(eventId);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}