package com.tangle.api.rx_tasks.profile;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.blocked_users.BlockedUsersResponse;

import io.reactivex.Observable;

public class GetBlockedUsers extends ApiTask<BlockedUsersResponse> {


    public GetBlockedUsers() {
    }

    @Override
    protected Observable<BaseModelResponse<BlockedUsersResponse>> getObservableTask() {
        return Api.getInst().profile().getBlockedUsers();
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}
