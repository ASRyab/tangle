package com.tangle.api.rx_tasks.auth.refresh_token;

import android.text.TextUtils;

import com.tangle.ApplicationLoader;
import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.api.rx_tasks.ObservableTask;
import com.tangle.model.auth.AuthDataResponse;

import io.reactivex.Observable;

public class RefreshTokenModel extends ObservableTask<AuthDataResponse> {
    private final ApiTask<AuthDataResponse> refreshTokenObservable;

    public RefreshTokenModel() {
        super(false);
        String token = Api.getInst().getSession().getRefreshToken();
        String refreshToken = !TextUtils.isEmpty(token) ? token : ApplicationLoader.getApplicationInstance().getPreferenceManager().getRefreshToken();
        this.refreshTokenObservable = new RefreshToken(refreshToken);
    }

    public static Observable<AuthDataResponse> authRefresh(AuthDataResponse authDataResponse) {
        return new RefreshToken(authDataResponse.getRefreshToken())
                        .getDataTask()
                        .map(response -> {
                            authDataResponse.setAccessToken(response.getAccessToken());
                            return authDataResponse;
                        })
                        .onErrorReturn(throwable -> authDataResponse);
    }

    @Override
    protected Observable<AuthDataResponse> getObservableTask() {
        return refreshTokenObservable.getDataTask().doOnNext(this::storeDataToPref);
    }

    private void storeDataToPref(AuthDataResponse authData) {
        Api.getInst().refreshSession(authData);
    }
}