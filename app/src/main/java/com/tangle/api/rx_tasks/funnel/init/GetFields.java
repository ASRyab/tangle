package com.tangle.api.rx_tasks.funnel.init;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.funnel.FunnelData;

import io.reactivex.Observable;

public class GetFields extends ApiTask<FunnelData> {

    @Override
    protected boolean isNeedSign() {
        return true;
    }

    @Override
    protected Observable<BaseModelResponse<FunnelData>> getObservableTask() {
        return Api.getInst().funnel().initFunnelFields();
    }
}
