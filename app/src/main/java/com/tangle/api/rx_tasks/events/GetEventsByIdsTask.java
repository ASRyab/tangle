package com.tangle.api.rx_tasks.events;

import com.tangle.api.rx_tasks.ObservableTask;
import com.tangle.model.event.EventInfo;

import java.util.List;

import io.reactivex.Observable;

public class GetEventsByIdsTask extends ObservableTask<List<EventInfo>> {
    private List<String> eventIds;

    public GetEventsByIdsTask(List<String> eventIds) {
        super(true);
        this.eventIds = eventIds;
    }

    private Observable<List<EventInfo>> getFromServer() {
        return new GetEventsByIds(eventIds).getDataTask();
    }

    @Override
    protected Observable<List<EventInfo>> getObservableTask() {
        return getFromServer();
    }
}