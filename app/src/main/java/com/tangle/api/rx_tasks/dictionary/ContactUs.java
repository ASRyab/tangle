package com.tangle.api.rx_tasks.dictionary;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class ContactUs extends ApiTask {
    private final String categoryId;
    private final String subjectId;
    private final String message;

    public ContactUs(String categoryId, String subjectId, String message) {
        this.categoryId = categoryId;
        this.subjectId = subjectId;
        this.message = message;
    }

    @Override
    protected Observable<BaseModelResponse> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("categoryId", categoryId);
        map.put("subjectId", subjectId);
        map.put("message", message);
        return Api.getInst().dictionary().contactUs(map);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}