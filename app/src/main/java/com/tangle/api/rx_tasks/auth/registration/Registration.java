package com.tangle.api.rx_tasks.auth.registration;


import android.text.TextUtils;

import com.tangle.BuildConfig;
import com.tangle.api.Api;
import com.tangle.api.ServerSession;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.auth.AuthDataResponse;
import com.tangle.utils.NetworkUtils;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class Registration extends ApiTask<AuthDataResponse> {

    private String name;
    private String email;
    private String password;
    private Date birthDay;

    public Registration(String name,
                        String email,
                        String password,
                        Date birthDay) {
        super();
        this.name       = name;
        this.email      = email;
        this.password   = password;
        this.birthDay   = birthDay;
    }


    @Override
    protected Observable<BaseModelResponse<AuthDataResponse>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("UserForm[login]", name);
        map.put("UserForm[email]", email);
        map.put("UserForm[password]", password);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(birthDay);
        map.put("UserForm[year]", String.valueOf(calendar.get(Calendar.YEAR)));
        map.put("UserForm[month]", String.valueOf(calendar.get(Calendar.MONTH) + 1));
        map.put("UserForm[day]", String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)));
        map.put("UserForm[termsConsent]", "1");
        map.put("UserForm[policyConsent]", "1");

        map.put("LoginForm[scenario]", "register");
        ServerSession session = Api.getInst().getSession();
        String deviceId = session.getDeviceId();
        if (!TextUtils.isEmpty(deviceId)) {
            map.put("udid", deviceId);
        }
        map.put("deviceIdHex", session.getDeviceIDHex());
        map.put("bundleId", BuildConfig.APPLICATION_ID);
        map.put("os", "android");
        map.put("appVersion", BuildConfig.VERSION_NAME);
        map.put("macAddress", NetworkUtils.getMACAddress());
        return Api.getInst().auth().userRegistration(map);
    }

    @Override
    protected boolean isNeedSign() {
        return false;
    }
}