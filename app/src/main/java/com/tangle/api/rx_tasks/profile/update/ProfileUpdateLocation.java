package com.tangle.api.rx_tasks.profile.update;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.profile_data.ProfileUpdateData;
import com.tangle.utils.CollectionUtils;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class ProfileUpdateLocation extends ApiTask<ProfileUpdateData> {

    private String location;
    private String countryCode;

    public ProfileUpdateLocation(String location, String countryCode) {
        super(false);
        this.location = location;
        this.countryCode = countryCode;
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }

    @Override
    protected Observable<BaseModelResponse<ProfileUpdateData>> getObservableTask() {

        Map<String, String> map = new HashMap<>();
        CollectionUtils.putStringSafe(map, "location", location);
        CollectionUtils.putStringSafe(map, "country_code", countryCode);

        return Api.getInst().profile().update(map);
    }
}
