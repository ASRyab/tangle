package com.tangle.api.rx_tasks.auth.remind_password;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.auth.RemindPasswordDataResponse;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class RemindPassword extends ApiTask<RemindPasswordDataResponse> {

    private String email;

    public RemindPassword(String email) {
        super();
        this.email = email;
    }

    @Override
    protected Observable<BaseModelResponse<RemindPasswordDataResponse>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("RecoveryForm[email]", email);
        return Api.getInst().auth().remindPassword(map);
    }

    @Override
    protected boolean isNeedSign() {
        return false;
    }
}