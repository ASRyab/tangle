package com.tangle.api.rx_tasks.profile.update;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.model.profile_data.ProfileUpdateData;
import com.tangle.utils.CollectionUtils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import io.reactivex.Observable;

public class ProfileUpdate extends ApiTask<ProfileUpdateData> {

    private final ProfileData profile;

    public ProfileUpdate(ProfileData profile) {
        super();
        this.profile = profile;
    }

    @Override
    protected Observable<BaseModelResponse<ProfileUpdateData>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        CollectionUtils.putStringSafe(map, "login", profile.login);
        CollectionUtils.putStringSafe(map, "scenario", "jobRequired");
        CollectionUtils.putStringSafe(map, "careerLevel", profile.getCareerLevel());
        CollectionUtils.putStringSafe(map, "industry", profile.getIndustry());
        CollectionUtils.putStringSafe(map, "description", profile.description);
        CollectionUtils.putStringSafe(map, "birthday", profile.birthday);
        if (profile.geo != null
                //todo fixing on SS
//                && Config.isProduction
                ) {
            CollectionUtils.putStringSafe(map, "city", profile.geo.city);
            CollectionUtils.putStringSafe(map, "country", profile.geo.country_code);
            CollectionUtils.putStringSafe(map, "cityArea", profile.geo.region);
            CollectionUtils.putStringSafe(map, "region", profile.geo.region);
        }
        CollectionUtils.putStringSafe(map, "lookingForGender", profile.lookingForGender);
        CollectionUtils.putStringSafe(map, "sexualOrientation", profile.sexual_orientation.getName());
        CollectionUtils.putStringSafe(map, "sexual_orientation", profile.sexual_orientation.getName());
        if (profile.interests != null) {
            Iterator<Map.Entry<String, Boolean>> iterator = profile.interests.entrySet().iterator();
            int i = 0;
            while (iterator.hasNext()) {
                map.put("interest[" + i + "]", iterator.next().getKey());
                i++;
            }
        }
        return Api.getInst().profile().update(map);
    }

    @Override
    protected boolean isNeedSign() {
        return false;
    }
}