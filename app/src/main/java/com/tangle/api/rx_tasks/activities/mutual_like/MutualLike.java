package com.tangle.api.rx_tasks.activities.mutual_like;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.activities.liked_you.LikedYouResponse;

import io.reactivex.Observable;
//not used. USE LIKED YOU
public class MutualLike extends ApiTask<LikedYouResponse> {

    public MutualLike() {
        super();
    }

    @Override
    protected Observable<BaseModelResponse<LikedYouResponse>> getObservableTask() {
        return Api.getInst().activities().getMatchesList();
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}