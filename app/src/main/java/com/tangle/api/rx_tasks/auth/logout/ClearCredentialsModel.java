package com.tangle.api.rx_tasks.auth.logout;

import com.tangle.ApplicationLoader;
import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ObservableTask;

import io.reactivex.Observable;

public class ClearCredentialsModel extends ObservableTask<Boolean> {

    public ClearCredentialsModel() {
        super(false);
    }

    @Override
    protected Observable<Boolean> getObservableTask() {
        return Observable.fromCallable(() -> {
            Api.getInst().clearSession();
            cleanManagers();
            ApplicationLoader.getApplicationInstance().getPreferenceManager().saveTermsAccepted(false);
            return true;
        });
    }


    private void cleanManagers() {
        ApplicationLoader instance = ApplicationLoader.getApplicationInstance();
        instance.getUserManager().deleteAll();
        instance.getEventManager().deleteAll();
        instance.getPrivateChatMsgsManager().deleteAll();
        instance.getLikeManager().deleteAll();
        instance.getInviteManager().deleteAll();
        instance.getBlogInfoManager().deleteAll();
    }
}
