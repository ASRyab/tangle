package com.tangle.api.rx_tasks.payment;

import android.os.Environment;

import com.tangle.api.Api;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;
import retrofit2.Response;

public class TicketDownload {
    private String eventId, userId;

    public TicketDownload(String eventId, String userId) {
        this.eventId = eventId;
        this.userId = userId;
    }

    public Observable<File> download() {
        Map<String, String> map = new HashMap<>();
        map.put("eventId", eventId);
        map.put("userId", userId);
        return Api.getInst().payment().ticketDownload(map).subscribeOn(Schedulers.io()).flatMap(this::saveFile);
    }

    private Observable<File> saveFile(Response<ResponseBody> response) {
        return Observable.fromCallable(() -> {
            // Get headers of response
            String header = response.headers().get("Content-Disposition");
            // Get filename
            String fileName = eventId + "_" + header.replace("attachment; filename=", "").replace("\"", "");
            // Create file in global directory
            File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsoluteFile(), fileName);
            BufferedSink sink = Okio.buffer(Okio.sink(file));
            // Access body of response
            sink.writeAll(response.body().source());
            sink.close();
            return file;
        });
    }
}