package com.tangle.api.rx_tasks.dictionary;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.pojos.DictionaryItem;

import java.util.List;

import io.reactivex.Observable;

public class GetDictionaryIndustries extends ApiTask<List<DictionaryItem>> {

    @Override
    protected Observable<BaseModelResponse<List<DictionaryItem>>> getObservableTask() {
        return Api.getInst().dictionary().getDictionaryIndustries();
    }

    @Override
    protected boolean isNeedSign() {
        return false;
    }
}
