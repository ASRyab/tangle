package com.tangle.api.rx_tasks.funnel.geo_regions;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

public class GeoRegions extends ApiTask<List<String>> {

    private String country, city;

    public GeoRegions(String country, String city) {
        super();
        this.country = country;
        this.city = city;
    }

    @Override
    protected boolean isNeedSign() {
        return false;
    }

    @Override
    protected Observable<BaseModelResponse<List<String>>> getObservableTask() {
        Map<String, String> params = new HashMap<>();
        params.put("city", city);
        params.put("country", country);
        return Api.getInst().dictionary().getRegions(params);
    }
}
