package com.tangle.api.rx_tasks;

import android.text.TextUtils;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.auth.refresh_token.RefreshTokenModel;
import com.tangle.model.BaseModelResponse;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.subjects.AsyncSubject;

public abstract class ApiTask<R> extends ObservableTask<BaseModelResponse<R>> {

    final private static long EXPIRED_ACCESS_TOKEN_TIME = TimeUnit.MINUTES.toMillis(15);

    public static AsyncSubject asyncSubject = null;

    public ApiTask(boolean executeOnNewThread) {
        super(executeOnNewThread);
    }

    public ApiTask() {
        super(true);
    }

    protected abstract boolean isNeedSign();

    private synchronized AsyncSubject getAsyncSubject() {
        if (asyncSubject == null || asyncSubject.hasComplete() || asyncSubject.hasThrowable()) {
            asyncSubject = AsyncSubject.create();
            if (TextUtils.isEmpty(Api.getInst().getSession().getRefreshToken())) {
                asyncSubject.onError(new IllegalArgumentException("RefreshToken is null"));
            } else if (isAccessTokenNeedRefresh()) {
                new RefreshTokenModel().getTask().subscribe(authData -> asyncSubject.onComplete(), asyncSubject::onError);
            } else {
                asyncSubject.onComplete();
            }
        }
        return asyncSubject;
    }

    @Override
    public Observable<BaseModelResponse<R>> getTask() {
        if (isNeedSign()) {
            //todo must be better
            return Observable.just(1).flatMap(__ -> getAsyncSubject().concatWith(super.getTask())).takeLast(1);
        } else {
            return super.getTask();
        }
    }

    public Observable<R> getDataTask() {
        return getTask().map(BaseModelResponse::getData);
    }

    protected boolean isAccessTokenNeedRefresh() {
        String accessToken = Api.getInst().getSession().getAccessToken();
        long refreshTokenLastUpdateTime = Api.getInst().getSession().getLastRequest();
        return (!TextUtils.isEmpty(accessToken) && refreshTokenLastUpdateTime < System.currentTimeMillis() - EXPIRED_ACCESS_TOKEN_TIME);
    }
}