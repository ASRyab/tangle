package com.tangle.api.rx_tasks.photo;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.UploadPhotoData;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UploadPhoto extends ApiTask<UploadPhotoData> {
    private File photoFile;
    private boolean isChatImg;

    public UploadPhoto(File photoFile, boolean isChatImg) {
        this.photoFile = photoFile;
        this.isChatImg = isChatImg;
    }

    @Override
    protected Observable<BaseModelResponse<UploadPhotoData>> getObservableTask() {
        RequestBody reqFile = RequestBody.create(MediaType.parse("image/jpeg"), photoFile);
        MultipartBody.Part body = MultipartBody.Part.createFormData("PhotoUploadForm[file]", photoFile.getName(), reqFile);
        if(isChatImg) {
            Map<String, RequestBody> map = new HashMap<>();
            map.put("via", createPartFromString("message_upload"));
            map.put("attachToUser", createPartFromString("false"));
            return Api.getInst().profile().uploadPhotoWithPartMap(map, body);
        } else {
            return Api.getInst().profile().uploadPhoto(body);
        }
    }

    private RequestBody createPartFromString(String valueString) {
        if (valueString == null) {
            return RequestBody.create(MultipartBody.FORM, "");
        }
        return RequestBody.create(MultipartBody.FORM, valueString);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}