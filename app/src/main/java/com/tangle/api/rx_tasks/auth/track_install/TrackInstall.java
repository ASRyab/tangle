package com.tangle.api.rx_tasks.auth.track_install;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;

import com.tangle.ApplicationLoader;
import com.tangle.api.Api;
import com.tangle.api.ServerSession;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.auth.TrackInstallDataResponse;
import com.tangle.utils.CollectionUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

import io.reactivex.Observable;

public class TrackInstall extends ApiTask<TrackInstallDataResponse> {

    private String referrer, adId;

    public TrackInstall(String adId, String referrer) {
        super();
        this.adId = adId;
        this.referrer = referrer;
    }

    @Override
    protected boolean isNeedSign() {
        return false;
    }

    @Override
    protected Observable<BaseModelResponse<TrackInstallDataResponse>> getObservableTask() {
        ServerSession session = Api.getInst().getSession();
        HashMap<String, String> params = new HashMap<>();
        Context application = ApplicationLoader.getContext();
        try {
            PackageInfo pInfo = application.getPackageManager().getPackageInfo(application.getPackageName(), 0);
            CollectionUtils.putStringSafe(params, "appVersion", pInfo.versionName);
            CollectionUtils.putStringSafe(params, "bundleId", pInfo.packageName);
            CollectionUtils.putStringSafe(params, "firstInstallTime", String.valueOf(pInfo.firstInstallTime));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        CollectionUtils.putStringSafe(params, "os", "android");
        CollectionUtils.putStringSafe(params, "deviceType", "android");

        String deviceId = session.getDeviceId();
        CollectionUtils.putStringSafe(params, "deviceId", deviceId);
        CollectionUtils.putStringSafe(params, "deviceIdHex", session.getDeviceIDHex());
        CollectionUtils.putStringSafe(params, "internalDeviceIdentifier", Settings.Secure.getString(application.getContentResolver(), Settings.Secure.ANDROID_ID));
        CollectionUtils.putStringSafe(params, "idfa", adId);
        try {
            CollectionUtils.putStringSafe(params, "osVersion", URLEncoder.encode(Build.VERSION.RELEASE, "UTF-8"));
            if (!TextUtils.isEmpty(referrer)) {
                CollectionUtils.putStringSafe(params, "referrer", URLEncoder.encode(referrer, "UTF-8"));
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Api.getInst().auth().trackInstall(params);
    }
}