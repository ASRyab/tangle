package com.tangle.api.rx_tasks.payment;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.payment.PaymentActionResult;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class CloseInvoice extends ApiTask<PaymentActionResult> {

    private String invoiceId;

    public CloseInvoice(String invoiceId) {
        super(true);
        this.invoiceId = invoiceId;
    }

    @Override
    protected Observable<BaseModelResponse<PaymentActionResult>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("invoiceId", invoiceId);
        return Api.getInst().payment().close(map);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}