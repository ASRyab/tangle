package com.tangle.api.rx_tasks.profile

import com.tangle.ApplicationLoader
import com.tangle.api.rx_tasks.ObservableTask
import com.tangle.model.profile_data.ProfileData
import com.tangle.screen.likebook.filter.AgeRange
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class OwnProfileModel() : ObservableTask<ProfileData>(false) {

    constructor(isLogin: Boolean) : this() {
        this.isLogin = isLogin
    }

    private var isLogin: Boolean = false

    override fun getObservableTask(): Observable<ProfileData> {
        val observable = ProfileTask().dataTask
                .doOnNext { this.saveProfile(it) }

        return if (isLogin) observable else observable
                .doOnNext { ApplicationLoader.getApplicationInstance().photoManager.onReceiveNewPhotos(it.photos) }
                .observeOn(Schedulers.io())
                .doOnNext { ApplicationLoader.getApplicationInstance().photoManager.saveNewPhoto(it.photos) }
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun saveProfile(profileData: ProfileData) {
        val preferenceManager = ApplicationLoader.getApplicationInstance().preferenceManager
        preferenceManager.saveCurrentUserId(profileData.id)
        if (preferenceManager.filterAgeRange == null) {
            preferenceManager.saveFilterAgeRange(AgeRange.getByAge(profileData.age))
        }
        ApplicationLoader.getApplicationInstance().userManager.setCurrentUser(profileData)
    }
}