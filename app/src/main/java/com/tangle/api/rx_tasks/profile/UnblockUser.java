package com.tangle.api.rx_tasks.profile;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.profile_data.BlockUserData;

import io.reactivex.Observable;

public class UnblockUser extends ApiTask<BlockUserData> {

    public String userId;

    public UnblockUser(String userId) {
        this.userId = userId;
    }

    @Override
    protected Observable<BaseModelResponse<BlockUserData>> getObservableTask() {
        return Api.getInst().profile().unblockUser(userId);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}
