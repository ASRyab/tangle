package com.tangle.api.rx_tasks.payment

import com.tangle.api.Api
import com.tangle.api.rx_tasks.ApiTask
import com.tangle.model.BaseModelResponse
import com.tangle.model.payment.PaymentActionResult
import io.reactivex.Observable
import java.util.*

class ChargeTicket(private val eventId: String) : ApiTask<PaymentActionResult>(true) {

    override fun getObservableTask(): Observable<BaseModelResponse<PaymentActionResult>> {
        val map = HashMap<String, String>()
        map["eventId"] = eventId
        return Api.getInst().payment().charge(map)
    }

    override fun isNeedSign(): Boolean {
        return true
    }
}