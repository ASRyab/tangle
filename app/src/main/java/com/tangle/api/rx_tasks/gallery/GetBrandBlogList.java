package com.tangle.api.rx_tasks.gallery;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.blog.BlogEntity;

import java.util.List;

import io.reactivex.Observable;

public class GetBrandBlogList extends ApiTask<List<BlogEntity>> {

    @Override
    protected boolean isNeedSign() {
        return true;
    }

    @Override
    protected Observable<BaseModelResponse<List<BlogEntity>>> getObservableTask() {
        return Api.getInst().blog().getBlogList();
    }
}