package com.tangle.api.rx_tasks.like_or_not;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.like_or_not.LikeOrNotGalleryData;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class SkipUser extends ApiTask<LikeOrNotGalleryData> {

    public String skippedUserId;

    public SkipUser(String skippedUserId) {
        this.skippedUserId = skippedUserId;
    }

    @Override
    protected Observable<BaseModelResponse<LikeOrNotGalleryData>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("toUserId", skippedUserId);
        return Api.getInst().likes().skipUserLike(map);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}
