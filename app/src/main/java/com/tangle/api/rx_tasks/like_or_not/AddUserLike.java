package com.tangle.api.rx_tasks.like_or_not;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.like_or_not.UserLikedData;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class AddUserLike extends ApiTask<UserLikedData> {

    private String addedUserId;

    public AddUserLike(String addedUserId) {
        this.addedUserId = addedUserId;
    }

    @Override
    protected Observable<BaseModelResponse<UserLikedData>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("toUserId", addedUserId);
        return Api.getInst().likes().addUserLike(map);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}
