package com.tangle.api.rx_tasks.photo;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.profile_data.ChangePrimaryPhoto;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class SetPrimaryPhoto extends ApiTask<ChangePrimaryPhoto> {
    private final String photoId;

    public SetPrimaryPhoto(String photoId) {
        super();
        this.photoId = photoId;
    }

    @Override
    protected Observable<BaseModelResponse<ChangePrimaryPhoto>> getObservableTask() {
        PhotoRequestParamObj photoRequestParamObj = new PhotoRequestParamObj();
        photoRequestParamObj.file = photoId;
        Map<String, String> map = new HashMap<>();
        map.put("data", Api.getInst().getGson().toJson(photoRequestParamObj));
        return Api.getInst().profile().changePrimaryPhoto(map);
    }

    @Override
    protected boolean isNeedSign() {
        return false;
    }
}