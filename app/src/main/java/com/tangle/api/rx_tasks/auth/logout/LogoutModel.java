package com.tangle.api.rx_tasks.auth.logout;

import com.tangle.api.rx_tasks.ObservableTask;
import com.tangle.model.EmptyDataResponse;

import io.reactivex.Observable;

public class LogoutModel extends ObservableTask {

    public LogoutModel() {
        super(false);
    }

    @Override
    protected Observable<EmptyDataResponse> getObservableTask() {
        Observable<EmptyDataResponse> logoutObservable = new Logout().getDataTask();
        Observable clearObservable = new ClearCredentialsModel().getTask();
        return Observable.concat(logoutObservable, clearObservable);
    }
}