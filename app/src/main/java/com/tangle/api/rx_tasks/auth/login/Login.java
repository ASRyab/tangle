package com.tangle.api.rx_tasks.auth.login;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.auth.AuthDataResponse;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class Login extends ApiTask<AuthDataResponse> {

    private String email;
    private String password;

    public Login(String email, String password) {
        super();
        this.email = email;
        this.password = password;
    }

    @Override
    protected Observable<BaseModelResponse<AuthDataResponse>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("LoginForm[email]", email);
        map.put("LoginForm[password]", password);
        map.put("deviceIdHex", Api.getInst().getSession().getDeviceIDHex());
        return Api.getInst().auth().userLogin(map);
    }

    @Override
    protected boolean isNeedSign() {
        return false;
    }
}