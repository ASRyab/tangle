package com.tangle.api.rx_tasks.funnel.init;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.funnel.FunnelData;

import java.util.HashMap;

import io.reactivex.Observable;

public class PostFields extends ApiTask<FunnelData> {

    private FunnelData data;

    public PostFields(FunnelData data) {
        this.data = data;
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }

    @Override
    protected Observable<BaseModelResponse<FunnelData>> getObservableTask() {
        HashMap<String, String> map = new HashMap<>();
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        map.put("data", gson.toJson(data));
        return Api.getInst().funnel().unlockFunnel(map);
    }
}
