package com.tangle.api.rx_tasks.funnel.geo_cities;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.funnel.CitiesDataResponse;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;

public class GeoCities extends ApiTask<CitiesDataResponse> {

    private String country;
    private String state;

    public GeoCities(@NonNull String country, @NonNull String state) {
        super();
        this.country = country;
        this.state = state;
    }


    @Override
    protected Observable<BaseModelResponse<CitiesDataResponse>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("country", country);
        map.put("state", state);
        return Api.getInst().dictionary().getCities(map);
    }

    @Override
    protected boolean isNeedSign() {
        return false;
    }
}