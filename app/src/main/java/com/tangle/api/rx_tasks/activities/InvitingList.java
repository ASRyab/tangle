package com.tangle.api.rx_tasks.activities;

import com.tangle.api.Api;
import com.tangle.api.rpc.RPCResponse;
import com.tangle.api.rpc.rpc_actions.invite.InvitesFindAllAction;
import com.tangle.api.rpc.rpc_response.invite.InviteData;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class InvitingList extends ApiTask<List<InviteData>> {

    private static final Integer LIMIT = 20;
    private final Integer offset;

    public InvitingList(int offset) {
        this.offset = offset;
    }

    public Observable<List<InviteData>> getFromServerSocket() {
        return Api.getInst().getSocketManager().executeRPCAction(new InvitesFindAllAction())
                .map(RPCResponse::getResult)
                .subscribeOn(Schedulers.io());
    }


    @Override
    protected Observable<BaseModelResponse<List<InviteData>>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("limit", LIMIT.toString());
        map.put("offset", offset.toString());
        return Api.getInst().activities().getInvites(map);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}