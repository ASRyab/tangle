package com.tangle.api.rx_tasks.payment;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.payment.InvoiceInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;

public class PaymentHistory extends ApiTask<List<InvoiceInfo>> {
    private final Integer limit;
    private final Integer offset;

    public PaymentHistory(@NonNull int limit, @NonNull int offset) {
        super();
        this.limit = limit;
        this.offset = offset;
    }

    @Override
    protected Observable<BaseModelResponse<List<InvoiceInfo>>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("limit", limit.toString());
        map.put("offset", offset.toString());
        return Api.getInst().payment().paymentHistory(map);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}