package com.tangle.api.rx_tasks.payment;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.payment.TicketResponse;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

//deprecated
public class AddTicket extends ApiTask<TicketResponse> {

    private final boolean isCheck;
    private String eventId, forUser;
    private boolean withTicket;

    public AddTicket(String eventId, String forUser, boolean withTicket, boolean isCheck) {
        super(true);
        this.eventId = eventId;
        this.forUser = forUser;
        this.withTicket = withTicket;
        this.isCheck = isCheck;
    }

    @Override
    protected Observable<BaseModelResponse<TicketResponse>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("eventId", eventId);
        map.put("withTicket", String.valueOf(withTicket ? 1 : 0));
        map.put("check", String.valueOf(isCheck ? 1 : 0));
        if (forUser != null) {
            map.put("userId", forUser);
        }
        return Api.getInst().payment().addTicket(map);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}
