package com.tangle.api.rx_tasks.payment

import com.tangle.ApplicationLoader
import com.tangle.api.Api
import com.tangle.api.rpc.rpc_response.invite.InviteData
import com.tangle.api.rx_tasks.ApiTask
import com.tangle.model.BaseModelResponse
import com.tangle.model.payment.PaymentActionResult
import io.reactivex.Observable
import java.util.*

class Respond(private val inviteData: InviteData, private val ticketAction: Int) : ApiTask<PaymentActionResult>(true) {

    override fun getObservableTask(): Observable<BaseModelResponse<PaymentActionResult>> {
        val map = HashMap<String, String>()
        map["inviteId"] = inviteData.inviteId
        map["status"] = ticketAction.toString()
        return Api.getInst().payment().respond(map)
                .doOnNext {
                    inviteData.status = when (ticketAction) {
                        TICKET_STATUS_ACCEPT -> InviteData.ACCEPT_INVITE_INDEX
                        TICKET_STATUS_REJECT -> InviteData.USER_REJECT_SENDING_INVITE_INDEX
                        else -> InviteData.REJECT_INVITE_INDEX
                    }
                    inviteData.isRead = true
                    ApplicationLoader.getApplicationInstance().inviteManager.setInvite(inviteData)
                }
    }

    override fun isNeedSign(): Boolean {
        return true
    }

    companion object {
        const val TICKET_STATUS_ACCEPT = 1
        const val TICKET_STATUS_DECLINE = 2
        const val TICKET_STATUS_REJECT = 3
    }
}