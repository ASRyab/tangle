package com.tangle.api.rx_tasks.funnel.geo_cities;

import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.ObservableTask;
import com.tangle.base.repository.ObservableRepository;

import java.util.List;

import io.reactivex.Observable;

public class GeoCitiesModel extends ObservableTask<List<String>> {

    private String country, state;
    private ObservableRepository<String> repository;

    public GeoCitiesModel(String country, String state) {
        super(false);
        repository = ApplicationLoader.getApplicationInstance().getRepositoriesHolder().getCitiesRepository();
        this.country = country;
        this.state = state;
    }

    @Override
    protected Observable<List<String>> getObservableTask() {
        return getFromRepository().flatMap(list -> {
            if (list.isEmpty()) {
                return getFromServer();
            } else {
                return Observable.just(list);
            }
        });
    }

    private Observable<List<String>> getFromServer() {
        return new GeoCities(country, state).getDataTask()
                .map(response -> response.cities)
                .doOnNext(cities -> repository.addAll(cities));
    }

    private Observable<List<String>> getFromRepository() {
        return repository.getAll();
    }
}