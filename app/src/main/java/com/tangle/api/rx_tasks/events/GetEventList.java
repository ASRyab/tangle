package com.tangle.api.rx_tasks.events;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.event.EventInfo;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

public class GetEventList extends ApiTask<List<EventInfo>> {

    @Override
    protected Observable<BaseModelResponse<List<EventInfo>>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("offset", "0");
        map.put("limit", "1000");
        return Api.getInst().event().getEventsList(map, Arrays.asList(0, 1));
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}