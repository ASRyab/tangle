package com.tangle.api.rx_tasks.image_preload;

import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.bumptech.glide.Priority;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.tangle.ApplicationLoader;
import com.tangle.api.rx_tasks.ObservableTask;
import com.tangle.utils.glide.GlideApp;

import java.util.Iterator;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.PublishSubject;

public class PreloadImageModel extends ObservableTask<String> {

    protected final PublishSubject<String> updatesPublisher = PublishSubject.create();
    private List<String> urls;

    public PreloadImageModel(List<String> urls) {
        super(false);
        this.urls = urls;
    }

    @Override
    protected Observable<String> getObservableTask() {
        Iterator<String> iterator = urls.iterator();
        while (iterator.hasNext()) {
            String url = iterator.next();
            if (!TextUtils.isEmpty(url)) {
                preloadImage(url, !iterator.hasNext());
            }
        }
        return updatesPublisher;
    }

    private void preloadImage(String url, boolean isLast) {
        GlideApp.with(ApplicationLoader.getContext())
                .load(url)
                .apply(RequestOptions.priorityOf(getPriority()))
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        updatesPublisher.onNext(url);
                        if (isLast) {
                            updatesPublisher.onComplete();
                        }
                    }
                });
    }

    protected Priority getPriority() {
        return Priority.LOW;
    }
}
