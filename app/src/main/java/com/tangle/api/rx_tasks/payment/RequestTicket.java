package com.tangle.api.rx_tasks.payment;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.event.TicketStatusResponse;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class RequestTicket extends ApiTask<TicketStatusResponse> {

    private String eventId;
    private int ticketsCount;

    public RequestTicket(String eventId, int ticketsCount) {
        super(true);
        this.eventId = eventId;
        this.ticketsCount = ticketsCount;
    }

    @Override
    protected Observable<BaseModelResponse<TicketStatusResponse>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("eventId", eventId);
        map.put("ticketsCount", String.valueOf(ticketsCount));
        return Api.getInst().payment().request(map);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}
