package com.tangle.api.rx_tasks.profile;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.profile_data.EmailUpdateData;
import com.tangle.utils.CollectionUtils;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

public class ChangeEmail extends ApiTask<EmailUpdateData> {

    private String newMail;
    private String pass;

    public ChangeEmail(String newMail, String pass) {
        this.newMail = newMail;
        this.pass = pass;
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }

    @Override
    protected Observable<BaseModelResponse<EmailUpdateData>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        CollectionUtils.putStringSafe(map, "AccountEmailForm[email]", newMail);
        CollectionUtils.putStringSafe(map, "AccountEmailForm[password]", pass);
        return Api.getInst().profile().updateMail(map);
    }
}