package com.tangle.api.rx_tasks.activities.liked_other

import com.tangle.api.Api
import com.tangle.api.rx_tasks.ApiTask
import com.tangle.model.BaseModelResponse
import com.tangle.model.activities.liked_you.LikedYouResponse
import io.reactivex.Observable
import java.util.*

class LikedOther(private val limit: Int, private val offset: Int) : ApiTask<LikedYouResponse>() {

    override fun getObservableTask(): Observable<BaseModelResponse<LikedYouResponse>> {
        val map = HashMap<String, String>()
        map["limit"] = limit.toString()
        map["offset"] = offset.toString()
        return Api.getInst().activities().getLikedOtherList(map)
    }

    override fun isNeedSign(): Boolean {
        return true
    }
}