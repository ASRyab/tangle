package com.tangle.api.rx_tasks.payment;

import com.tangle.api.Api;
import com.tangle.api.rx_tasks.ApiTask;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.payment.PaymentActionResult;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;

//deprecated
public class UpdateInvoice extends ApiTask<PaymentActionResult> {

    private String invoiceId;
    private boolean status;

    public UpdateInvoice(String invoiceId, boolean isCharged) {
        super(true);
        this.invoiceId = invoiceId;
        this.status = isCharged;
    }

    @Override
    protected Observable<BaseModelResponse<PaymentActionResult>> getObservableTask() {
        Map<String, String> map = new HashMap<>();
        map.put("invoiceId", invoiceId);
        map.put("status", String.valueOf(status ? 1 : 4));
        return Api.getInst().payment().updateInvoice(map);
    }

    @Override
    protected boolean isNeedSign() {
        return true;
    }
}
