package com.tangle.api;

import android.text.TextUtils;

import com.google.gson.annotations.Expose;
import com.tangle.model.auth.AuthDataResponse;

final public class ServerSession {

    @Expose
    private final String deviceId;
    private final String deviceIDMultisite;
    private final String deviceIDHex;
    @Expose
    private final String appVersion;
    @Expose
    private final String bundleId;
    @Expose
    private String login;
    @Expose
    private String accessToken;
    @Expose
    private String refreshToken;
    @Expose
    private String tokenType;
    @Expose
    private long lastRequest;

    public ServerSession(String deviceId, String deviceIDMultisite, String deviceIDHex, String appVersion, String bundleId) {
        this.deviceId = deviceId;
        this.deviceIDMultisite = deviceIDMultisite;
        this.deviceIDHex = deviceIDHex;
        this.appVersion = appVersion;
        this.bundleId = bundleId;
    }

    public ServerSession(String deviceId, String deviceIDMultisite, String deviceIDHex, String appVersion, String bundleId, AuthDataResponse authData){
        this(deviceId, deviceIDMultisite, deviceIDHex, appVersion, bundleId);
        this.accessToken = authData.getAccessToken();
        this.refreshToken = authData.getRefreshToken();
        this.tokenType = authData.getTokenType();
        this.lastRequest = System.currentTimeMillis();
    }

    public String getDeviceId() {
        return deviceId;
    }

    public String getDeviceIDMultisite() {
        return deviceIDMultisite;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public String getBundleId() {
        return bundleId;
    }

    public String getLogin() {
        return login;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getRefreshToken() {
        return refreshToken;
    }

    public void updateRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void updateTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

    public long getLastRequest() {
        return lastRequest;
    }

    public void setLastRequest(long lastRequest) {
        this.lastRequest = lastRequest;
    }

    public boolean isAlive() {
        //todo never empty
        return !TextUtils.isEmpty(tokenType)
                && !TextUtils.isEmpty(accessToken)
                && !TextUtils.isEmpty(refreshToken);
    }

    public void updateAccessToken(String newAccessToken) {
        this.accessToken = newAccessToken;
    }

    public void resetSession(){
        accessToken = null;
        refreshToken = null;
        tokenType = null;
    }

    @Override
    public String toString() {
        return "{" +
                "refreshToken='" + refreshToken + '\'' +
                ", accessToken='" + accessToken + '\'' +
                ", deviceId='" + deviceId + '\'' +
                ", tokenType='" + tokenType + '\'' +
                ", lastRequest=" + lastRequest +
                '}';
    }

    public String getDeviceIDHex() {
        return deviceIDHex;
    }
}
