package com.tangle.api.requests;

import com.tangle.api.utils.Config;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.event.TicketStatusResponse;
import com.tangle.model.payment.InvoiceInfo;
import com.tangle.model.payment.PaymentActionResult;
import com.tangle.model.payment.PaymentData;
import com.tangle.model.payment.TicketResponse;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;

public interface ApiPayment {
    //deprecated
    @GET(Config.BASE_PATH + "/mobApp/tickets/add")
    Observable<BaseModelResponse<TicketResponse>> addTicket(@QueryMap Map<String, String> map);

    @GET(Config.BASE_PATH + "/mobApp/credits/history")
    Observable<BaseModelResponse<List<InvoiceInfo>>> paymentHistory(@QueryMap Map<String, String> map);

    @POST(Config.BASE_PATH + "/mobApp/tickets/book")
    Observable<BaseModelResponse<PaymentData>> book(@QueryMap Map<String, String> map);

    @POST(Config.BASE_PATH + "/mobApp/tickets/request")
    Observable<BaseModelResponse<TicketStatusResponse>> request(@QueryMap Map<String, String> map);

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/mobApp/tickets/invite")
    Observable<BaseModelResponse<PaymentData>> invite(@FieldMap final Map<String, String> dataMap);

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/mobApp/tickets/respond")
    Observable<BaseModelResponse<PaymentActionResult>> respond(@FieldMap final Map<String, String> dataMap);

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/mobApp/tickets/updateInvoice")
    Observable<BaseModelResponse<PaymentActionResult>> updateInvoice(@FieldMap final Map<String, String> dataMap);

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/mobApp/tickets/charge")
    Observable<BaseModelResponse<PaymentActionResult>> charge(@FieldMap final Map<String, String> dataMap);

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/mobApp/tickets/close")
    Observable<BaseModelResponse<PaymentActionResult>> close(@FieldMap final Map<String, String> dataMap);

    @GET(Config.BASE_PATH + "/mobApp/events/ticketPDF")
    @Streaming
    Observable<Response<ResponseBody>> ticketDownload(@QueryMap Map<String, String> map);

    @GET(Config.BASE_PATH + "/mobApp/invoice/info")
    Observable<BaseModelResponse<List<InvoiceInfo>>> updateInvoiceInfo(@QueryMap Map<String, String> map);
}