package com.tangle.api.requests;

import com.tangle.api.utils.Config;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.funnel.FunnelData;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiFunnel {

    @GET(Config.BASE_PATH + "/funnel")
    Observable<BaseModelResponse<FunnelData>> initFunnelFields();

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/funnel")
    Observable<BaseModelResponse<FunnelData>> unlockFunnel(@FieldMap Map<String, String> data);

}