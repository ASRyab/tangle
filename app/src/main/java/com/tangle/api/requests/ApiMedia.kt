package com.tangle.api.requests

import com.tangle.api.utils.Config
import com.tangle.model.BaseModelResponse
import com.tangle.model.media.MediaResponse
import io.reactivex.Single
import retrofit2.http.GET


interface ApiMedia {
    @GET(Config.BASE_PATH + "/mobApp/appMedia/list")
    fun getMediaUrls(): Single<BaseModelResponse<MediaResponse>>
}