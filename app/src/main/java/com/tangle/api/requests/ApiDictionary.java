package com.tangle.api.requests;

import com.tangle.api.utils.Config;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.contact_us.ContactUsData;
import com.tangle.model.funnel.CitiesDataResponse;
import com.tangle.model.pojos.DictionaryItem;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface ApiDictionary {

    @GET(Config.BASE_PATH + "/mobApp/dictionary/interest")
    Observable<BaseModelResponse<List<DictionaryItem>>> getDictionaryInterests();

    @GET(Config.BASE_PATH + "/mobApp/dictionary/industry")
    Observable<BaseModelResponse<List<DictionaryItem>>> getDictionaryIndustries();

    @GET(Config.BASE_PATH + "/mobApp/dictionary/careerLevel")
    Observable<BaseModelResponse<List<DictionaryItem>>> getDictionaryCareerLevels();

    @GET(Config.BASE_PATH + "/geo/cities")
    Observable<BaseModelResponse<CitiesDataResponse>> getCities(@QueryMap final Map<String, String> geoCitiesPartMap);

    @GET(Config.BASE_PATH + "/geo/suggestLocation")
    Observable<BaseModelResponse<List<String>>> getRegions(@QueryMap final Map<String, String> geoCitiesPartMap);

    @GET(Config.BASE_PATH + "/site/contactUs")
    Observable<BaseModelResponse<ContactUsData>> getContactUsInfo();

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/site/contactUs")
    Observable<BaseModelResponse> contactUs(@FieldMap final Map<String, String> contactUsMap);

}
