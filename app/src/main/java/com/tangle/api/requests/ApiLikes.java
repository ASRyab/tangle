package com.tangle.api.requests;

import com.tangle.api.utils.Config;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.like_or_not.LikeOrNotGalleryData;
import com.tangle.model.like_or_not.UserLikedData;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiLikes {

    @GET(Config.BASE_PATH + "/userLikes/getUserLikeGallery")
    Observable<BaseModelResponse<LikeOrNotGalleryData>> getLikesGallery(@QueryMap Map<String, String> map);

    @GET(Config.BASE_PATH + "/userLikes/addUserLike")
    Observable<BaseModelResponse<UserLikedData>> addUserLike(@QueryMap Map<String, String> map);

    @GET(Config.BASE_PATH + "/userLikes/skipUserLike")
    Observable<BaseModelResponse<LikeOrNotGalleryData>> skipUserLike(@QueryMap Map<String, String> map);

}
