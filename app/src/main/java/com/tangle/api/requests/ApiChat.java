package com.tangle.api.requests;

import com.tangle.api.utils.Config;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.chat.ChatHistoryData;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiChat {
    @GET(Config.BASE_PATH + "/mobApp/chat/incoming")
    Observable<BaseModelResponse<ChatHistoryData>> getPrivateChatList(@QueryMap final Map<String, String> dataMap);

    @GET(Config.BASE_PATH + "/mobApp/chat/outgoing")
    Observable<BaseModelResponse<ChatHistoryData>> getOutgoingChatList(@QueryMap final Map<String, String> dataMap);

    @GET(Config.BASE_PATH + "/mobApp/chat")
    Observable<BaseModelResponse<ChatHistoryData>> getUnsortedChatList(@QueryMap final Map<String, String> dataMap);
}