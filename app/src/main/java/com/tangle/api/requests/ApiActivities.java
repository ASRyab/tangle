package com.tangle.api.requests;

import com.tangle.api.rpc.rpc_response.invite.InviteData;
import com.tangle.api.utils.Config;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.activities.liked_you.LikedYouResponse;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

public interface ApiActivities {
    @GET(Config.BASE_PATH + "/userLikes/getUsersLikedMe")
    Observable<BaseModelResponse<LikedYouResponse>> getLikedYouList(@QueryMap final Map<String, String> whoLikedYouListPartMap);

   @GET(Config.BASE_PATH + "/userLikes/getUsersYouLiked")
    Observable<BaseModelResponse<LikedYouResponse>> getLikedOtherList(@QueryMap final Map<String, String> whoLikedYouListPartMap);

    @GET(Config.BASE_PATH + "/userLikes/getMatches")
    Observable<BaseModelResponse<LikedYouResponse>> getMatchesList();

    @GET(Config.BASE_PATH + "/mobApp/invite/findAll")
    Observable<BaseModelResponse<List<InviteData>>> getInvites(@QueryMap final Map<String, String> map);
}