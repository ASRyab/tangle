package com.tangle.api.requests;

import com.tangle.api.utils.Config;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.blog.BlogEntity;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.GET;

public interface ApiBlog {
    @GET(Config.BASE_PATH + "/mobApp/blog/list")
    Observable<BaseModelResponse<List<BlogEntity>>> getBlogList();
}
