package com.tangle.api.requests;

import com.tangle.api.utils.Config;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.EmptyDataResponse;
import com.tangle.model.auth.AuthDataResponse;
import com.tangle.model.auth.RemindPasswordDataResponse;
import com.tangle.model.auth.TrackInstallDataResponse;

import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

public interface ApiAuth {

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/track/installAndroid")
    Observable<BaseModelResponse<TrackInstallDataResponse>> trackInstall(@FieldMap final Map<String, String> trackInstallPartMap);

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/site/login")
    Observable<BaseModelResponse<AuthDataResponse>> userLogin(@FieldMap final Map<String, String> authorizationPartMap);

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/site/checkLogin")
    Observable<BaseModelResponse<EmptyDataResponse>> checkLogin(@FieldMap final Map<String, String> authorizationPartMap);

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/user/register")
    Observable<BaseModelResponse<AuthDataResponse>> userRegistration(@FieldMap final Map<String, String> registrationPartMap);

    @GET(Config.BASE_PATH + "/site/refreshToken")
    Observable<BaseModelResponse<AuthDataResponse>> refreshToken(@QueryMap final Map<String, String> refreshTokenPartMap);

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/account/remindPassword")
    Observable<BaseModelResponse<RemindPasswordDataResponse>> remindPassword(@FieldMap final Map<String, String> refreshTokenPartMap);

    @GET(Config.BASE_PATH + "/site/logout")
    Observable<BaseModelResponse<EmptyDataResponse>> logout();
}