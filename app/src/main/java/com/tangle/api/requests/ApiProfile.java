package com.tangle.api.requests;

import com.tangle.api.utils.Config;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.EmptyDataResponse;
import com.tangle.model.TargetCountry;
import com.tangle.model.UploadPhotoData;
import com.tangle.model.blocked_users.BlockedUsersResponse;
import com.tangle.model.profile_data.BlockUserData;
import com.tangle.model.profile_data.ChangePrimaryPhoto;
import com.tangle.model.profile_data.EmailUpdateData;
import com.tangle.model.profile_data.PhotoDelete;
import com.tangle.model.profile_data.ProfileData;
import com.tangle.model.profile_data.ProfileUpdateData;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;

public interface ApiProfile {

    @Multipart
    @POST(Config.BASE_PATH + "/photo/upload")
    Observable<BaseModelResponse<UploadPhotoData>> uploadPhoto(@Part MultipartBody.Part photo);

    @Multipart
    @POST(Config.BASE_PATH + "/photo/upload")
    Observable<BaseModelResponse<UploadPhotoData>> uploadPhotoWithPartMap(@PartMap() Map<String, RequestBody> partMap,
                                                                          @Part MultipartBody.Part photo);

    @GET(Config.BASE_PATH + "/data/fetch/args/{jsonData}")
    Observable<BaseModelResponse<ProfileData>> getOwnInfo(@Path(value = "jsonData", encoded = true) String jsonData);

    @GET(Config.BASE_PATH + "/data/fetch/userId/{id}/args/{jsonData}")
    Observable<BaseModelResponse<ProfileData>> getOtherUserInfo(@Path("id") String id, @Path(value = "jsonData", encoded = true) String jsonData);

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/profile/update")
    Observable<BaseModelResponse<ProfileUpdateData>> update(@FieldMap final Map<String, String> profileUpdatePartMap);

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/account/requestEmailUpdate")
    Observable<BaseModelResponse<EmailUpdateData>> updateMail(@FieldMap final Map<String, String> profileUpdatePartMap);

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/account/updatePassword")
    Observable<BaseModelResponse<EmptyDataResponse>> updatePassword(@FieldMap final Map<String, String> profileUpdatePartMap);

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/photo/delete")
    Observable<BaseModelResponse<PhotoDelete>> deletePhoto(@FieldMap final Map<String, String> photoDeletePartMap);

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/photo/changeprimary")
    Observable<BaseModelResponse<ChangePrimaryPhoto>> changePrimaryPhoto(@FieldMap final Map<String, String> changePrimaryPhotoPartMap);

    @GET(Config.BASE_PATH + "/blocked/block/id/{id}")
    Observable<BaseModelResponse<BlockUserData>> blockUser(@Path("id") String id);

    @GET(Config.BASE_PATH + "/blocked/unblock/id/{id}")
    Observable<BaseModelResponse<BlockUserData>> unblockUser(@Path("id") String id);

    @GET(Config.BASE_PATH + "/blocked")
    Observable<BaseModelResponse<BlockedUsersResponse>> getBlockedUsers();

    @GET(Config.BASE_PATH + "/mobApp/account/isTargetCountry")
    Observable<BaseModelResponse<TargetCountry>> getTargetCountry();

}