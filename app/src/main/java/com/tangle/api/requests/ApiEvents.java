package com.tangle.api.requests;

import com.tangle.api.utils.Config;
import com.tangle.model.BaseModelResponse;
import com.tangle.model.event.EventInfo;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public interface ApiEvents {
    @GET(Config.BASE_PATH + "/mobApp/events/list")
    Observable<BaseModelResponse<List<EventInfo>>> getEventsList(@QueryMap final Map<String, String> trackInstallPartMap, @Query("types[]") List<Integer> types);

    @GET(Config.BASE_PATH + "/mobApp/events/getEventsByIds")
    Observable<BaseModelResponse<List<EventInfo>>> getEventsByIds(@Query("ids[]") List<String> eventIds);

    @GET(Config.BASE_PATH + "/mobApp/events/getEvent")
    Observable<BaseModelResponse<EventInfo>> getEventById(@Query("eventId") String eventId);

    @FormUrlEncoded
    @POST(Config.BASE_PATH + "/mobApp/tickets/generate")
    Observable<BaseModelResponse<List<String>>> getEventQRCode(@FieldMap final Map<String, String> dataMap);
}