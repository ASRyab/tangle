package com.tangle.api.utils;

import com.tangle.ApplicationLoader;
import com.tangle.BuildConfig;
import com.tangle.R;
import com.tangle.utils.Debug;

public final class Config {
    public static final String TEST_USER_AUTOLOGIN = "";

    public static boolean isLogging() {
        return Debug.isEnabled;
    }

    public static boolean isTracking() {
        return !(BuildConfig.DEBUG || ApplicationLoader.getContext().getResources().getBoolean(R.bool.is_tracking_off));
    }

    public static String getBaseUrl() {
        return ApplicationLoader.getContext().getString(R.string.server_url);
    }

    public final static int DEFAULT_SOCKET_PORT = 443;
    public final static String BASE_PATH = "/api/v1";

    public static String getBasePaymentUrl() {
        return ApplicationLoader.getContext().getString(R.string.server_payment_url);
    }

    public final static String BASE_PAYMENT_PATH = getBasePaymentUrl() + "/pay/app";

    public final static boolean IS_ALL_GRAPH = true;

    public static final String PRIVACY_POLICY_URL = getBasePaymentUrl() + "/staticPage/privacypolicy";
    public static final String TERMS_OF_USE_URL = getBasePaymentUrl() + "/staticPage/terms";

    public static final String FLURRY_DEV_KEY = "5YBXN4FQCFHXWMPTVJS3";


}