package com.tangle.api.rpc.rpc_actions.chat;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.tangle.api.rpc.RPCAction;
import com.tangle.api.rpc.RPCResponse;
import com.tangle.model.chat.MailMessagePhoenix;

public class PrivateChatSendMsgAction extends RPCAction<RPCResponse<MailMessagePhoenix>> {
    public RPCResponse<MailMessagePhoenix> response;

    public PrivateChatSendMsgAction(String toId, String message, MsgType msgType) {
        super("deventsPrivateChat", createParams(toId, message, msgType));
    }

    private static JsonObject createParams(String toId, String message,  MsgType msgType) {
        JsonObject params = new JsonObject();
        params.add("toId", new JsonPrimitive(toId));
        params.add("subject", new JsonPrimitive(""));
        params.add("message", new JsonPrimitive(message.trim()));
        params.add("messageType", new JsonPrimitive(msgType.value));
        params.add("copyPasteDetected", new JsonPrimitive(false));
        return params;
    }

    @Override
    public RPCResponse<MailMessagePhoenix> getResponse() {
        return response;
    }

    @Override
    public void setResponse(RPCResponse<MailMessagePhoenix> response) {
        this.response = response;
    }
}