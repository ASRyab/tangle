package com.tangle.api.rpc;

import android.os.Handler;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.tangle.ApplicationLoader;
import com.tangle.api.Api;
import com.tangle.api.ServerSession;
import com.tangle.api.rpc.core.koushikdutta.async.http.socketio.Acknowledge;
import com.tangle.api.rpc.core.koushikdutta.async.http.socketio.ConnectCallback;
import com.tangle.api.rpc.core.koushikdutta.async.http.socketio.EventCallback;
import com.tangle.api.rpc.core.koushikdutta.async.http.socketio.SocketIOClient;
import com.tangle.api.rpc.rpc_response.invite.InviteSocketMsg;
import com.tangle.api.utils.ErrorResponse;
import com.tangle.managers.PrivateChatMsgsManager;
import com.tangle.model.chat.MailMessagePhoenix;
import com.tangle.utils.Debug;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;

public class SocketManager {
    private final static String TAG = SocketManager.class.getSimpleName();

    final private static int RAND_MAX = 32767;
    final private static String RPC = "rpc";
    final private static String AUTH = "auth";
    final private static String MSG = "msg";
    final private static String ROOMS = "rooms";

    private final String serverUrl;
    private final int socketPort;
    private final OkHttpClient okHttpClient;
    private SocketIOClient socketIOClient;
    private AtomicInteger messageId = new AtomicInteger(0);
    private static Gson gson;
    private Hashtable<Integer, RPCAction> rpcActions = new Hashtable<Integer, RPCAction>();

    private SocketManager(Builder builder) {
        this.serverUrl = builder.serverUrl;
        this.socketPort = builder.socketPort;
        this.okHttpClient = builder.okHttpClient;
    }

    private Gson getGson() {
        if (gson == null) {
            gson = Api.getInst().getGson();
        }
        return gson;
    }

    public synchronized void connectSocket(ServerSession session) {
        if (session == null) return;
        if (!session.isAlive()) return;
        if (isConnected()) {
            destroy();
        }
        SocketIOClient.setReconnect(true);
        StringBuilder query = new StringBuilder();
        Random rand = new Random();
        query.append("/?t=").append(rand.nextInt(RAND_MAX));
        query.append("&key=").append(session.getRefreshToken());
        query.append("&id=").append(session.getAccessToken());
        query.append("&platform=androidApp");
        String url = serverUrl + ":" + socketPort + query.toString();
        SocketIOClient.connect(url, okHttpClient, new ConnectCallback() {
            @Override
            public void onConnectCompleted(Exception ex, SocketIOClient client) {
                if (ex == null) {
                    Debug.logD(TAG, "socket connected");
                } else {
                    Debug.logD(TAG, "socket error");
                    ex.printStackTrace();
                    return;
                }
                if (isConnected()) {
                    destroy();
                }
                socketIOClient = client;
                socketIOClient.addListener(RPC, rpcEventCallback);
                socketIOClient.addListener(MSG, msgEventCallback);
                socketIOClient.addListener(MSG, invEventCallback);
                socketIOClient.addListener(ROOMS, roomsEventCallback);
                socketIOClient.addListener(AUTH, authEventCallback);
            }
        }, new Handler());
    }

    public <T extends RPCResponse> Observable<T> executeRPCAction(RPCAction<T> rpcAction) {
        //todo need implement same with REST
        if (!isConnected()) {
            Api.getInst().connectToWebSocket();
        }
        return Observable.create(new ObservableOnSubscribe<T>() {
            @Override
            public void subscribe(ObservableEmitter<T> emitter) throws Exception {
                if (isConnected()) {
                    try {
                        rpcAction.setId(messageId.incrementAndGet());
                        String args = getGson().toJson(rpcAction);
                        JSONObject argsObj = new JSONObject(args);
                        JSONArray array = new JSONArray();
                        array.put(argsObj);
                        rpcAction.setEmitter(emitter);
                        rpcActions.put(rpcAction.getId(), rpcAction);
                        socketIOClient.emit(RPC, array);
                    } catch (JSONException exception) {
                        emitter.onError(exception);
                        exception.printStackTrace();
                    }
                } else {
                    emitter.onError(ErrorResponse.networkError((new IOException())));
                }
            }
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public void destroy() {
        if (isConnected()) {
            SocketIOClient.setReconnect(false);
            socketIOClient.removeListener(RPC, rpcEventCallback);
            socketIOClient.removeListener(MSG, msgEventCallback);
            socketIOClient.removeListener(ROOMS, roomsEventCallback);
            socketIOClient.addListener(AUTH, authEventCallback);
            socketIOClient.disconnect();
            socketIOClient = null;
        }
    }

    public boolean isConnected() {
        Debug.logD(TAG, "isConnected : socketIOClient : " + socketIOClient);
        return socketIOClient != null && socketIOClient.isConnected();
    }

    public interface SocketManagerListener {
        void onConnect();
    }

    private ArrayList<SocketManagerListener> subscribeListeners = new ArrayList<>();

    public void subscribeOn(SocketManagerListener socketManagerListener) {
        if (isConnected()) {
            socketManagerListener.onConnect();
        } else {
            subscribeListeners.add(socketManagerListener);
        }
    }

    private EventCallback rpcEventCallback = new EventCallback() {
        @Override
        public void onEvent(String event, JSONArray argument, Acknowledge acknowledge) {
            try {
                JSONObject arg = argument.getJSONObject(0);
                if (arg.has("id")) {
                    int id = arg.getInt("id");
                    if (rpcActions.containsKey(id)) {
                        RPCResponse response;
                        RPCAction action = rpcActions.get(id);
                        ObservableEmitter emitter = action.getEmitter();
                        response = parseResponseData(arg.toString(), action);
                        action.setResponse(response);
                        if (action.isSuccess()) {
                            emitter.onNext(response);
                        } else {
                            emitter.onError(ErrorResponse.rpcError(response.getError()));
                        }
                        rpcActions.remove(id);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    };

    private EventCallback authEventCallback = new EventCallback() {
        @Override
        public void onEvent(String event, JSONArray argument, Acknowledge acknowledge) {
            for (SocketManagerListener subscriber : subscribeListeners) {
                subscriber.onConnect();
            }
            subscribeListeners.clear();
        }
    };

    private EventCallback roomsEventCallback = new EventCallback() {
        @Override
        public void onEvent(String event, JSONArray argument, Acknowledge acknowledge) {
            try {
                String type = argument.getString(0);
                String responseBody = argument.getString(1);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    protected RPCResponse parseResponseData(String responseBody, RPCAction action) {
        Type dataType = null;
        Class<?> clazz = action.getClass();
        while (!Object.class.equals(clazz) && clazz != null) {
            if (clazz == null) break;
            if (!(clazz.getGenericSuperclass() instanceof ParameterizedType))//to avoid attempts to parse not parametrized response
                return new RPCResponse();
            Class superClass = clazz.getSuperclass();
            if ((superClass.equals(RPCAction.class)
                    && ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments().length > 0)) {
                dataType = ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments()[0];
                break;
            }
            clazz = clazz.getSuperclass();
        }

        RPCResponse rpcResponse = null;
        try {
            rpcResponse = getGson().fromJson(responseBody, dataType);
        } catch (Exception e) {
            action.getEmitter().onError(ErrorResponse.parseError(e));
            e.printStackTrace();
        }
        if (rpcResponse == null) {
            rpcResponse = new RPCResponse();
        }
        return rpcResponse;
    }

    private EventCallback msgEventCallback = (event, argument, acknowledge) -> {
        int incomeMsgsCount = argument.length();
        if (incomeMsgsCount == 0) {
            return;
        }
        Gson gson = Api.getInst().getGson();
        PrivateChatMsgsManager privateChatMsgsManager = ApplicationLoader.getApplicationInstance().getPrivateChatMsgsManager();
        for (int i = 0; i < incomeMsgsCount; i++) {
            MailMessagePhoenix mailMessagePhoenix = null;
            try {
                mailMessagePhoenix = gson.fromJson(argument.getJSONObject(i).toString(), MailMessagePhoenix.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (mailMessagePhoenix != null) {
                mailMessagePhoenix.read = false;
                privateChatMsgsManager.addMessage(mailMessagePhoenix);
            }
        }
    };

    private EventCallback invEventCallback = (event, argument, acknowledge) -> {
        int incomeMsgsCount = argument.length();
        if (incomeMsgsCount == 0) {
            return;
        }
        Gson gson = Api.getInst().getGson();
        for (int i = 0; i < incomeMsgsCount; i++) {
            InviteSocketMsg inviteSocketMsg = null;
            try {
                inviteSocketMsg = gson.fromJson(argument.getJSONObject(i).toString(), InviteSocketMsg.class);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (inviteSocketMsg != null) {
                ApplicationLoader.getApplicationInstance().getInviteManager().forceLoadNew(0);
            }
        }
    };

    public final static class Builder {
        private String serverUrl;
        private int socketPort;
        private OkHttpClient okHttpClient;
        boolean isSocketPortSet;

        public Builder setSocketPort(int socketPort) {
            this.socketPort = socketPort;
            isSocketPortSet = true;
            return this;
        }

        public Builder setOkHttpClient(OkHttpClient okHttpClient) {
            this.okHttpClient = okHttpClient;
            return this;
        }

        public Builder setServerUrl(String serverUrl) {
            this.serverUrl = serverUrl;
            return this;
        }

        public SocketManager create() {
            if (okHttpClient == null
                    || TextUtils.isEmpty(serverUrl)
                    || !isSocketPortSet) {
                throw new IllegalArgumentException("All fields in Builder are mandatory");
            }
            return new SocketManager(this);
        }
    }
}