package com.tangle.api.rpc.core.koushikdutta.async.http.socketio;

public interface DisconnectCallback {
    void onDisconnect(Exception e);
}
