package com.tangle.api.rpc.rpc_actions;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.tangle.api.rpc.RPCAction;
import com.tangle.api.rpc.RPCResponse;
import com.tangle.api.rpc.RPCResult;
import com.tangle.model.profile_data.ProfileData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

//old RoomVCardRequestAction2
public class GetUsersAction extends RPCAction<RPCResponse<RPCResult<Map<String, ProfileData>>>> {

    public RPCResponse<RPCResult<Map<String, ProfileData>>> response;

    public GetUsersAction(List<String> userIds) {
        super("vcard", createParams(userIds));
    }

    public GetUsersAction(String id) {
        super("vcard", createParams(Collections.singletonList(id)));
    }

    private static JsonObject createParams(List<String> userIds) {
        final List<String> userList = new ArrayList<>(userIds);
        JsonObject params = new JsonObject();
        JsonArray ids = new JsonArray();
        for (String id : userList) {
            JsonPrimitive element = new JsonPrimitive(id);
            ids.add(element);
        }
        params.add("items", ids);
        return params;
    }

    @Override
    public RPCResponse<RPCResult<Map<String, ProfileData>>> getResponse() {
        return response;
    }

    @Override
    public void setResponse(RPCResponse<RPCResult<Map<String, ProfileData>>> response) {
        this.response = response;
    }
}