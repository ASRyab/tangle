package com.tangle.api.rpc.rpc_actions.invite;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.tangle.api.rpc.RPCAction;
import com.tangle.api.rpc.RPCResponse;
import com.tangle.api.rpc.rpc_response.invite.InviteData;

import java.util.List;

public class InvitesFindAllByEventIdAction extends RPCAction<RPCResponse<List<InviteData>>> {
    public RPCResponse<List<InviteData>> response;

    public InvitesFindAllByEventIdAction(String eventId) {
        super("deventsInvitesFindAllByEventId", createParams(eventId));
    }

    private static JsonObject createParams(String eventId) {
        JsonObject params = new JsonObject();
        params.add("eventId", new JsonPrimitive(eventId));
        return params;
    }

    @Override
    public RPCResponse<List<InviteData>> getResponse() {
        return response;
    }

    @Override
    public void setResponse(RPCResponse<List<InviteData>> response) {
        this.response = response;
    }
}