package com.tangle.api.rpc.core.koushikdutta.async.http.socketio;

import org.json.JSONArray;

public interface Acknowledge {
    void acknowledge(JSONArray arguments);
}
