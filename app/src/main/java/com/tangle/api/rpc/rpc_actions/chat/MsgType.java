package com.tangle.api.rpc.rpc_actions.chat;

public enum MsgType {
    CHAT("chat"),
    IMG_MSG("imbImage"),
    SYSTEM_MSG("systemNotification");

    public String value;

    MsgType(String value) {
        this.value = value;
    }
}