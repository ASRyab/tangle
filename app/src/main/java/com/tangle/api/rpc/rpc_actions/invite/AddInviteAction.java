package com.tangle.api.rpc.rpc_actions.invite;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.tangle.api.rpc.RPCAction;
import com.tangle.api.rpc.RPCResponse;
import com.tangle.api.rpc.rpc_response.invite.InviteData;

public class AddInviteAction extends RPCAction<RPCResponse<InviteData>> {
    public RPCResponse<InviteData> response;

    public AddInviteAction(String toUserId, String eventId) {
        super("deventsInvitesAdd", createParams(toUserId, eventId));
    }

    private static JsonObject createParams(String toUserId, String eventId) {
        JsonObject params = new JsonObject();
        params.add("toUserId", new JsonPrimitive(toUserId));
        params.add("eventId", new JsonPrimitive(eventId));
        return params;
    }

    @Override
    public RPCResponse<InviteData> getResponse() {
        return response;
    }

    @Override
    public void setResponse(RPCResponse<InviteData> response) {
        this.response = response;
    }
}