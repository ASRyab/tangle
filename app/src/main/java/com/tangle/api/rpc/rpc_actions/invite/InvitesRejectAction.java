package com.tangle.api.rpc.rpc_actions.invite;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.tangle.api.rpc.RPCAction;
import com.tangle.api.rpc.RPCResponse;
import com.tangle.api.rpc.rpc_response.invite.InviteData;

public class InvitesRejectAction extends RPCAction<RPCResponse<InviteData>> {
    public RPCResponse<InviteData> response;

    public InvitesRejectAction(String inviteId) {
        super("deventsInvitesReject", createParams(inviteId));
    }

    private static JsonObject createParams(String inviteId) {
        JsonObject params = new JsonObject();
        params.add("inviteId", new JsonPrimitive(inviteId));
        return params;
    }

    @Override
    public RPCResponse<InviteData> getResponse() {
        return response;
    }

    @Override
    public void setResponse(RPCResponse<InviteData> response) {
        this.response = response;
    }
}