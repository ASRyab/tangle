package com.tangle.api.rpc.core.koushikdutta.async.http.socketio;

public interface ErrorCallback {
    void onError(String error);
}
