package com.tangle.api.rpc.core.koushikdutta.async.http.socketio;

public interface ReconnectCallback {
    public void onReconnect();
}