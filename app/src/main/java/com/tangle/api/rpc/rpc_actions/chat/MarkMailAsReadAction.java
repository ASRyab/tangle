package com.tangle.api.rpc.rpc_actions.chat;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.tangle.api.rpc.RPCAction;
import com.tangle.api.rpc.RPCResponse;

public class MarkMailAsReadAction extends RPCAction<RPCResponse<Object>> {
    public RPCResponse<Object> response;

    public MarkMailAsReadAction(String userId) {
        super("markAsRead", createParams(userId));
    }

    private static JsonObject createParams(String userId) {
        JsonObject params = new JsonObject();
        params.add("toId", new JsonPrimitive(userId));
        return params;
    }

    @Override
    public RPCResponse<Object> getResponse() {
        return response;
    }

    @Override
    public void setResponse(RPCResponse<Object> response) {
        this.response = response;
    }
}