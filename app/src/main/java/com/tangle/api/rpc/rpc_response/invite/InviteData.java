package com.tangle.api.rpc.rpc_response.invite;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;
import com.tangle.utils.ConvertingUtils;

import java.io.Serializable;
import java.util.Date;


@Entity(tableName = "InviteData")
public class InviteData implements Serializable, Comparable<InviteData> {
    /**
     * EventInviteStatusUndefined = 0,
     * EventInviteStatusPending = 1,   // еще не ответил на инвайт
     * EventInviteStatusAccepted = 2,  // подтвердил инвайт
     * EventInviteStatusDeclined = 3,  // отменил инвайт
     * EventInviteStatusRefused = 4,   // пользователь отменил отправленный инвайт
     */
    public static final int EXPIRED_INVITE = 0;
    public static final int NOT_RESPOND_TO_INVITE_INDEX = 1;
    public static final int ACCEPT_INVITE_INDEX = 2;
    public static final int REJECT_INVITE_INDEX = 3;
    public static final int USER_REJECT_SENDING_INVITE_INDEX = 4;

    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "inviteId")
    public String inviteId;
    @ColumnInfo(name = "eventId")
    public String eventId;
    @ColumnInfo(name = "from")
    public String from;
    @ColumnInfo(name = "to")
    public String to;
    @ColumnInfo(name = "withTicket")
    public boolean withTicket;
    @ColumnInfo(name = "expireDate")
    public int expireDate;
    @ColumnInfo(name = "status")
    public Integer status;
    @ColumnInfo(name = "isRead")
    public boolean isRead;
    @SerializedName("unread")
    @Ignore// NOT USE NOW
    private boolean unreadServer;
    @ColumnInfo(name = "createdAt")
    public String createdAt;

    public Date getCreatedDate() {
        return ConvertingUtils.getDateFromString(createdAt, ConvertingUtils.DATE_FORMAT_YMD_HMS);
    }

    @Override
    public int compareTo(@NonNull InviteData o) {
        int status = this.status.compareTo(o.status);
        int date = this.getCreatedDate().compareTo(o.getCreatedDate());
        return ((status == 0) ? date : status);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InviteData)) {
            return false;
        }
        InviteData rpcResponse = (InviteData) o;
        return inviteId != null && inviteId.equals(rpcResponse.inviteId) && status.equals(rpcResponse.status);
    }
}