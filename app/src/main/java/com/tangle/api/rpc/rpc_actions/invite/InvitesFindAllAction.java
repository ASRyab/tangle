package com.tangle.api.rpc.rpc_actions.invite;

import com.google.gson.JsonObject;
import com.tangle.api.rpc.RPCAction;
import com.tangle.api.rpc.RPCResponse;
import com.tangle.api.rpc.rpc_response.invite.InviteData;

import java.util.List;

public class InvitesFindAllAction extends RPCAction<RPCResponse<List<InviteData>>> {
    public RPCResponse<List<InviteData>> response;

    public InvitesFindAllAction() {
        super("deventsInvitesFindAll", new JsonObject());
    }

    @Override
    public RPCResponse<List<InviteData>> getResponse() {
        return response;
    }

    @Override
    public void setResponse(RPCResponse<List<InviteData>> response) {
        this.response = response;
    }
}