package com.tangle.api.rpc.rpc_response.invite;

import java.io.Serializable;

public class InviteSocketMsg implements Serializable {
    public String action;
    public String eventId;
    public String fromUserId;
    public String type;

}