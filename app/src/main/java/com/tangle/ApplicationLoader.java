package com.tangle;

import android.content.Context;
import android.content.IntentFilter;
import android.support.multidex.MultiDexApplication;

import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.danikula.videocache.HttpProxyCacheServer;
import com.facebook.stetho.Stetho;
import com.flurry.android.FlurryAgent;
import com.tangle.api.utils.Config;
import com.tangle.base.repository.ItemsListRepository;
import com.tangle.base.repository.ObservableRepository;
import com.tangle.base.repository.RepositoriesHolder;
import com.tangle.db.AppDatabase;
import com.tangle.db.messages.MessagesDataSource;
import com.tangle.managers.AnalyticManager;
import com.tangle.managers.BlogInfoManager;
import com.tangle.managers.DataPollingManager;
import com.tangle.managers.EventManager;
import com.tangle.managers.InviteManager;
import com.tangle.managers.LikeManager;
import com.tangle.managers.MediaManager;
import com.tangle.managers.NavigationManager;
import com.tangle.managers.NotificationViewManager;
import com.tangle.managers.PhotoManager;
import com.tangle.managers.PrivateChatMsgsManager;
import com.tangle.managers.UserManager;
import com.tangle.model.pojos.DictionaryItem;
import com.tangle.referrer.TrackingManager;
import com.tangle.utils.ADBReceiver;
import com.tangle.utils.Debug;
import com.tangle.utils.PreferenceManager;

import java.util.Map;

import io.branch.referral.Branch;

public class ApplicationLoader extends MultiDexApplication {
    private static final String TAG = ApplicationLoader.class.getSimpleName();
    private static ApplicationLoader applicationInstance;

    private RepositoriesHolder repositoriesHolder;
    private HttpProxyCacheServer proxy;
    private NotificationViewManager notificationViewManager;
    private UserManager userManager;
    private EventManager eventManager;
    private PrivateChatMsgsManager privateChatManager;
    private LikeManager likeManager;
    private InviteManager inviteManager;
    private PhotoManager photoManager;
    private BlogInfoManager blogInfoManager;
    private DataPollingManager dataPollingManager;
    private TrackingManager trackingManager;
    private NavigationManager navigationManager;
    private AnalyticManager analyticManager;
    private MediaManager mediaManager;

    @Override
    public void onCreate() {
        super.onCreate();
        applicationInstance = this;
        //Must be top
        Debug.INSTANCE.initDebug(!isLiveServer());
        initStetho();
        initTracking();
        initRepository();
        initCachingProxy();
        initManagers();
        initAdbReceiver();
    }

    public boolean isLiveServer() {
        String serverUrl = getString(R.string.server_payment_url);
        return serverUrl.equals(getString(R.string.server_url_live));
    }

    private void initAdbReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ADBReceiver.ACTION_CHANGE_IP_ADDRESS);
        intentFilter.addAction(ADBReceiver.ACTION_DO_AUTO_LOGIN);
        intentFilter.addAction(ADBReceiver.ACTION_OPEN_USER);
        intentFilter.addAction(ADBReceiver.ACTION_SET_LOGGING);
        registerReceiver(new ADBReceiver(), intentFilter);
    }

    private void initTracking() {
        if (Config.isTracking()) {
            initAppsFlyer();
            Branch.getAutoInstance(this);
        } else {
            Branch.enableLogging();
            Branch.getAutoTestInstance(this, true);
        }

        if (!BuildConfig.DEBUG) {
            new FlurryAgent.Builder()
                    .withLogEnabled(true)
                    .build(this, Config.FLURRY_DEV_KEY);
        }
    }

    private void initAppsFlyer() {
        AppsFlyerLib appsFlyerLib = AppsFlyerLib.getInstance();
        AppsFlyerConversionListener listener = new AppsFlyerConversionListener() {

            @Override
            public void onInstallConversionDataLoaded(Map<String, String> map) {
                Debug.logD(TAG, "onInstallConversionDataLoaded: " + map);
            }

            @Override
            public void onInstallConversionFailure(String s) {
                Debug.logD(TAG, "onInstallConversionFailure: " + s);
            }

            @Override
            public void onAppOpenAttribution(Map<String, String> map) {
            }

            @Override
            public void onAttributionFailure(String s) {
            }
        };
        appsFlyerLib.init(getString(R.string.apps_flyer_key), listener, getApplicationContext());
        appsFlyerLib.startTracking(this);
    }

    private void initManagers() {
        userManager = new UserManager();
        eventManager = new EventManager();
        dataPollingManager = new DataPollingManager();
        notificationViewManager = new NotificationViewManager();
    }

    public PrivateChatMsgsManager getPrivateChatMsgsManager() {
        PrivateChatMsgsManager localInstance = privateChatManager;
        if (localInstance == null) {
            synchronized (PrivateChatMsgsManager.class) {
                localInstance = privateChatManager;
                if (localInstance == null) {
                    MessagesDataSource source = new MessagesDataSource(AppDatabase.getInstance(this).messagesDao());
                    privateChatManager = localInstance = new PrivateChatMsgsManager(source);
                }
            }
        }
        return localInstance;
    }

    public UserManager getUserManager() {
        return userManager;
    }

    public EventManager getEventManager() {
        return eventManager;
    }

    public NotificationViewManager getNotificationViewManager() {
        return notificationViewManager;
    }


    public HttpProxyCacheServer getCacheProxy() {
        return proxy;
    }

    private void initRepository() {
        ObservableRepository<DictionaryItem> interestsRepository = new ItemsListRepository<>();
        ObservableRepository<DictionaryItem> careersRepository = new ItemsListRepository<>();
        ObservableRepository<DictionaryItem> industriesRepository = new ItemsListRepository<>();
        ObservableRepository<String> citiesRepository = new ItemsListRepository<>();
        ObservableRepository<String> regionsRepository = new ItemsListRepository<>();
        repositoriesHolder = new RepositoriesHolder() {
            @Override
            public ObservableRepository<DictionaryItem> getInterestsRepository() {
                return interestsRepository;
            }

            @Override
            public ObservableRepository<DictionaryItem> getCareerLevelsRepository() {
                return careersRepository;
            }

            @Override
            public ObservableRepository<DictionaryItem> getIndustriesRepository() {
                return industriesRepository;
            }

            @Override
            public ObservableRepository<String> getCitiesRepository() {
                return citiesRepository;
            }

            @Override
            public ObservableRepository<String> getRegionsRepository() {
                return regionsRepository;
            }
        };
    }

    private void initCachingProxy() {
        proxy = new HttpProxyCacheServer.Builder(this)
                .maxCacheSize(1024 * 1024 * 128)
                .build();
    }

    private void initStetho() {
        if (Config.isLogging()) {
            Stetho.initializeWithDefaults(this);
        }
    }

    public static Context getContext() {
        return applicationInstance.getApplicationContext();
    }

    public static ApplicationLoader getApplicationInstance() {
        return applicationInstance;
    }

    public RepositoriesHolder getRepositoriesHolder() {
        return repositoriesHolder;
    }

    public PreferenceManager getPreferenceManager() {
        return PreferenceManager.getInstance(this);
    }

    public LikeManager getLikeManager() {
        LikeManager localInstance = likeManager;
        if (localInstance == null) {
            synchronized (LikeManager.class) {
                localInstance = likeManager;
                if (localInstance == null) {
                    likeManager = localInstance = new LikeManager(AppDatabase.getInstance(this).likeDao());
                }
            }
        }
        return localInstance;
    }

    public InviteManager getInviteManager() {
        InviteManager localInstance = inviteManager;
        if (localInstance == null) {
            synchronized (InviteManager.class) {
                localInstance = inviteManager;
                if (localInstance == null) {
                    inviteManager = localInstance = new InviteManager(AppDatabase.getInstance(this).inviteDao());
                }
            }
        }
        return localInstance;
    }

    public PhotoManager getPhotoManager() {
        PhotoManager localInstance = photoManager;
        if (localInstance == null) {
            synchronized (PhotoManager.class) {
                localInstance = photoManager;
                if (localInstance == null) {
                    photoManager = localInstance = new PhotoManager(AppDatabase.getInstance(this).photoDao());
                }
            }
        }
        return localInstance;
    }

    public BlogInfoManager getBlogInfoManager() {
        BlogInfoManager localInstance = blogInfoManager;
        if (localInstance == null) {
            synchronized (BlogInfoManager.class) {
                localInstance = blogInfoManager;
                if (localInstance == null) {
                    blogInfoManager = localInstance = new BlogInfoManager(AppDatabase.getInstance(this).blogDao());
                }
            }
        }
        return localInstance;
    }

    public MediaManager getMediaManager() {
        MediaManager localInstance = mediaManager;
        if (localInstance == null) {
            synchronized (MediaManager.class) {
                localInstance = mediaManager;
                if (localInstance == null) {
                    mediaManager = localInstance = new MediaManager();
                }
            }
        }
        return localInstance;
    }

    public DataPollingManager getDataPollingManager() {
        return dataPollingManager;
    }

    public TrackingManager getTrackingManager() {
        TrackingManager localInstance = trackingManager;
        if (localInstance == null) {
            synchronized (TrackingManager.class) {
                localInstance = trackingManager;
                if (localInstance == null) {
                    trackingManager = localInstance = new TrackingManager(this);
                }
            }
        }
        return localInstance;
    }

    public NavigationManager getNavigationManager() {
        NavigationManager localInstance = navigationManager;
        if (localInstance == null) {
            synchronized (NavigationManager.class) {
                localInstance = navigationManager;
                if (localInstance == null) {
                    navigationManager = localInstance = new NavigationManager();
                }
            }
        }
        return localInstance;
    }

    public AnalyticManager getAnalyticManager() {
        AnalyticManager localInstance = analyticManager;
        if (localInstance == null) {
            synchronized (AnalyticManager.class) {
                localInstance = analyticManager;
                if (localInstance == null) {
                    analyticManager = localInstance = new AnalyticManager();
                }
            }
        }
        return localInstance;
    }
}